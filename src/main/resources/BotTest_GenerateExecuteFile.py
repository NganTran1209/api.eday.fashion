import os, sys, json
import codecs
import ast

def generateExecutingFile(dataFolder, executeFilePath, lsTestcases, runningUser, tsName, runningOS, timeout, realOS, runingLocation, screenRes, mobileMode, mobileRes, deviceName, jobName, runningDate, browserName, isRecordVideo, projectName, resultName, runDescription, projectId, runningName):
    
    #prepare json data
    data = {}
    browserData = {}
    data['ResultName'] = resultName
    data['RunDescription'] = runDescription
    data['ProjectName'] = projectName
    data['ProjectId'] = projectId
    data['Testsuite'] = tsName
    data['Server'] = {
        "Ip":"http://127.0.0.1",
        "Port":4732
    }

    data['Device'] = {
        'DeviceName': deviceName.replace("~"," "),
        'Resolution': mobileRes
    }
    data['RunningUser'] = runningUser
    data['Timeout'] = timeout
    data['TestResult']={}
    data['Testcases']=[]
    typeWeb = ""
    if mobileMode == 'true':
        typeWeb = "mobile"
    elif mobileMode == 'false':
        typeWeb = "desktop"
    
    data['Mode'] = {
        'Name': "web",
        'Type': typeWeb
    }

    data['Environment'] = {
        'BrowserName': browserName,
        'OS': runningOS,
        'Name': realOS.replace("~"," "),
        'Resolution': screenRes.replace("~"," "),
        'Location': runingLocation.replace("~"," "),
        'TimeOut': timeout,
        'Milestone': runningName
    }

    data['JobName'] = jobName

    data['ExecuteDate'] = runningDate

    browserData = {
                "ClassName": "AC_UseBrowser",
                "Params": [
                    {
                        "Name": "value",
                        "Value": browserName
                    }
                ],
                "Result": {
                },
                "Language": {
                    "en": {
                        "ActionName": "use browser",
                        "StepName": "use browser"
                    }
                }
            }

    for ind,vas in enumerate(lsTestcases):
        jsFile = dataFolder + "/Testcases/"+vas+".json"
        with codecs.open(jsFile, encoding='utf-8') as f:
            jsData = json.load(f)
            jsData["Testscripts"].insert(0,browserData)
            indexChild = 0

            while indexChild < len(jsData["Testscripts"]):
                if jsData["Testscripts"][indexChild]["ClassName"] == "AC_ExecuteTestcase":
                    openSubTestcase = dataFolder + "/Testcases/" + jsData["Testscripts"][indexChild]["Params"][0]["Value"] + ".json"
                    with codecs.open(openSubTestcase,encoding='utf-8') as flats:
                        dataSubTestcase = json.load(flats)
                    jsData["Testscripts"] = append_List(jsData["Testscripts"], indexChild, dataSubTestcase["Testscripts"] )
                    #indexChild += 1
                    
                # if action is  AC_CheckData
                appendScript = AC_CheckData(jsData["Testscripts"],indexChild,dataFolder )
                if (len(appendScript) > 0):
                    jsData["Testscripts"] = append_List(jsData["Testscripts"], indexChild, appendScript)
                    indexChild += len(appendScript) -1

                # if action is  AC_UseTestData
                appendScript = AC_UseTestData(jsData["Testscripts"],indexChild,dataFolder )
                if (len(appendScript) > 0):
                    jsData["Testscripts"] = append_List(jsData["Testscripts"], indexChild, appendScript)
                    indexChild += len(appendScript) -1
                indexChild += 1
            for i in jsData["Testscripts"]:
                i["Result"] = {}
        data['Testcases'].append({
            "Name":jsData["Testcase"],
            "TestcaseProcedure": jsData["TestcaseProcedure"],
            "ExpectedOutput": jsData["ExpectedOutput"],
            "TestScripts":jsData["Testscripts"]
        })
    with codecs.open(executeFilePath, 'w', encoding='utf-8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)

# generate for action ExecuteTestcase        
def recursion_ExecuteTestcase(subIndexChild, caseChild, childCase, dataValid, dataFolder):
    for ind,dExTe in enumerate(dataValid):
        if dExTe['ClassName'] == 'AC_ExecuteTestcase':
            if subIndexChild == 0:
                subIndexChild = ind
            subparam = dExTe["Params"]
            for i in subparam:
                subValue = i["Value"]
                openSubTestcase = dataFolder + "/Testcases/" + subValue + ".json"
                with codecs.open(openSubTestcase,encoding='utf-8') as flats:
                    dataSubTestcase = json.load(flats)
                    indexSub = 0
                    caseSub = 0
                    dataSubTestcase["Testscripts"] = recursion_ExecuteTestcase(indexSub, caseSub, childCase, dataSubTestcase["Testscripts"], dataFolder)
                for subExte in dataSubTestcase["Testscripts"]:
                    subExte["Result"] = {}
                subDictData = []
                subDictData.extend(dataValid[:subIndexChild])
                subDictData.extend(dataSubTestcase["Testscripts"])
                subDictData.extend(dataValid[subIndexChild+1:])
                dataValid = subDictData
                subItem = 0
                for ind,valse in enumerate(dataValid):
                    if valse['ClassName'] == "AC_ExecuteTestcase":
                        if subItem == 0:
                            subIndexChild = ind
                        subItem = 1
                    if valse['ClassName'] == "AC_CheckData":
                        caseChild = ind
                    if val['ClassName'] == "AC_UseTestData":
                        childCase = ind
        if dExTe['ClassName'] == 'AC_CheckData':
            if caseChild == 0:
                caseChild = ind
            param = dExTe["Params"]
            for p in param:
                if p["Name"] == "Screen":
                    layoutName = p["Value"]
                if p["Name"] == "Casenum":
                    casenum = p ["Value"]
            openFileData = dataFolder + "/TestDatas/" + layoutName + ".json"
            with codecs.open(openFileData,encoding='utf-8') as flats:
                dataCase = json.load(flats)
            layoutName = dataCase["layoutName"]
            listCheckControl = []
            for l in dataCase["listCases"]:
                if int(l["casenum"]) == int(casenum):
                    for k in l["listControl"]:
                        checkControl = {
                            "ClassName": "AC_CheckControlValue",
                            "Params": [
                                {
                                    "Name": "Screen",
                                    "Value": layoutName
                                },
                                {
                                    "Name": "Item",
                                    "Value": k["Name"]
                                },
                                {
                                    "Name": "Operator",
                                    "Value": "equal"
                                },
                                {
                                    "Name": "Expected",
                                    "Value": k["Value"]
                                }
                            ],
                            "Result": {
                            },
                            "Language": {
                                "en": {
                                    "ActionName": "check control " + k["Name"],
                                    "StepName": "check control value"
                                }
                            }
                        }
                        listCheckControl.append(checkControl)
            dictData = []
            dictData.extend(dataValid[:caseChild])
            dictData.extend(listCheckControl)
            dictData.extend(dataValid[caseChild+1:])
            dataValid = dictData
            item1 = 0
            for ind,val in enumerate(dataValid):
                if val['ClassName'] == "AC_CheckData":
                    if item1 == 0:
                        caseChild = ind
                    item1 = 1
                if val['ClassName'] == "AC_ExecuteTestcase":
                    subIndexChild = ind
                if val['ClassName'] == "AC_UseTestData":
                    childCase = ind
        if dExTe['ClassName'] == 'AC_UseTestData':
            if childCase == 0:
                childCase = ind
            param = dExTe["Params"]
            for p in param:
                if p["Name"] == "Screen":
                    layoutName = p["Value"]
                if p["Name"] == "Casenum":
                    casenum = p ["Value"]
            openFileData = dataFolder + "/TestDatas/" + layoutName + ".json"
            with codecs.open(openFileData,encoding='utf-8') as flats:
                dataCase = json.load(flats)
            layoutName = dataCase["layoutName"]
            listCheckControl = []
            for l in dataCase["listCases"]:
                if int(l["casenum"]) == int(casenum):
                    for k in l["listControl"]:
                        controlType = get_controlType(layoutName, k["Name"], dataFolder)
                        if controlType == "text":
                            inputAction = {
                                "ClassName": "AC_Enter",
                                "Language": {
                                    "en": {
                                        "StepName": "enter " + k["Name"],
                                        "ActionName": "enter"
                                    }
                                },
                                "Params": [
                                    {
                                        "Name": "Screen",
                                        "Value": layoutName
                                    },
                                    {
                                        "Name": "Item",
                                        "Value": k["Name"]
                                    },
                                    {
                                        "Name": "Value",
                                        "Value": k["Value"]
                                    }
                                ]
                            }
                        elif controlType == "checkbox" or controlType == "radio":
                            inputAction = {
                                "ClassName": "AC_Click",
                                "Language": {
                                    "en": {
                                        "StepName": "click " + k["Name"],
                                        "ActionName": "click"
                                    }
                                },
                                "Params": [
                                    {
                                        "Name": "Screen",
                                        "Value": layoutName
                                    },
                                    {
                                        "Name": "Item",
                                        "Value": k["Name"]
                                    }
                                ]
                            }
                        elif controlType == "select":
                            inputAction = {
                                "ClassName": "AC_Select",
                                "Language": {
                                    "en": {
                                        "StepName": "select " + k["Name"],
                                        "ActionName": "select"
                                    }
                                },
                                "Params": [
                                    {
                                        "Name": "Screen",
                                        "Value": layoutName
                                    },
                                    {
                                        "Name": "Item",
                                        "Value": k["Name"]
                                    },
                                    {
                                        "Name": "Value",
                                        "Value": k["Value"]
                                    }
                                ]
                            }
                        elif controlType == "input":
                            inputAction = {
                                "ClassName": "AC_Enter",
                                "Language": {
                                    "en": {
                                        "StepName": "enter " + k["Name"],
                                        "ActionName": "enter"
                                    }
                                },
                                "Params": [
                                    {
                                        "Name": "Screen",
                                        "Value": layoutName
                                    },
                                    {
                                        "Name": "Item",
                                        "Value": k["Name"]
                                    },
                                    {
                                        "Name": "Value",
                                        "Value": k["Value"]
                                    }
                                ]
                            }
                        listCheckControl.append(inputAction)
            dictData = []
            dictData.extend(dataValid[:childCase])
            dictData.extend(listCheckControl)
            dictData.extend(dataValid[childCase+1:])
            dataValid = dictData
            item1 = 0
            for ind,val in enumerate(dataValid):
                if val['ClassName'] == "AC_UseTestData":
                    if item1 == 0:
                        childCase = ind
                    item1 = 1
                if val['ClassName'] == "AC_ExecuteTestcase":
                    subIndexChild = ind
                if val['ClassName'] == "AC_CheckData":
                    caseChild = ind
        dExTe["Result"] = {}
    return dataValid
def condition_useTestData(controlType, layoutName, controlName, value):
    inputAction = ""
    if controlType == "text" or controlType == "password":
        inputAction = {
            "ClassName": "AC_Enter",
            "Language": {
                "en": {
                    "StepName": "enter " + controlName,
                    "ActionName": "enter"
                }
            },
            "Params": [
                {
                    "Name": "Screen",
                    "Value": layoutName
                },
                {
                    "Name": "Item",
                    "Value": controlName
                },
                {
                    "Name": "Value",
                    "Value": value
                }
            ]
        }
    elif controlType == "checkbox" or controlType == "radio":
        inputAction = {
            "ClassName": "AC_Click",
            "Language": {
                "en": {
                    "StepName": "click " + controlName,
                    "ActionName": "click"
                }
            },
            "Params": [
                {
                    "Name": "Screen",
                    "Value": layoutName
                },
                {
                    "Name": "Item",
                    "Value": controlName
                }
            ]
        }
    elif controlType == "select":
        inputAction = {
            "ClassName": "AC_Select",
            "Language": {
                "en": {
                    "StepName": "select " + controlName,
                    "ActionName": "select"
                }
            },
            "Params": [
                {
                    "Name": "Screen",
                    "Value": layoutName
                },
                {
                    "Name": "Item",
                    "Value": controlName
                },
                {
                    "Name": "Value",
                    "Value": value
                }
            ]
        }
    elif controlType == "input":
        inputAction = {
            "ClassName": "AC_Enter",
            "Language": {
                "en": {
                    "StepName": "enter " + controlName,
                    "ActionName": "enter"
                }
            },
            "Params": [
                {
                    "Name": "Screen",
                    "Value": layoutName
                },
                {
                    "Name": "Item",
                    "Value": controlName
                },
                {
                    "Name": "Value",
                    "Value": value
                }
            ]
        }
    return inputAction
def get_controlType(layoutName, controlName, dataFolder):
    openFileData = dataFolder + "/Layouts/" + layoutName + ".json"
    with codecs.open(openFileData,encoding='utf-8') as flats:
        dataLayouts = json.load(flats)
    for i in dataLayouts["Controls"]:
        if i["Name"] == controlName:
            for k in i["Attributes"]:
                if k["Name"] == "type":
                    return k["Value"]
def append_List(currentScript, startIndex, appendScript ):
    dictData = []
    dictData.extend(currentScript[:startIndex])
    dictData.extend(appendScript)
    dictData.extend(currentScript[startIndex+1:])
    return dictData
def AC_CheckData(currentScript, index,dataFolder):
    listCheckControl = []
    if currentScript[index]["ClassName"] == "AC_CheckData":
        layoutName = currentScript[index]["Params"][0]["Value"]
        casenum = currentScript[index]["Params"][1]["Value"]
        openFileData = dataFolder + "/TestDatas/" + layoutName + ".json"

        # read testdata file
        with codecs.open(openFileData,encoding='utf-8') as flats:
            dataCase = json.load(flats)
        # transform testdata into test script
        for l in dataCase["listCases"]:
            if int(l["casenum"]) == int(casenum):
                for k in l["listControl"]:
                    checkControl = {
                        "ClassName": "AC_CheckControlValue",
                        "Params": [
                            {
                                "Name": "Screen",
                                "Value": layoutName
                            },
                            {
                                "Name": "Item",
                                "Value": k["Name"]
                            },
                            {
                                "Name": "Operator",
                                "Value": "equal"
                            },
                            {
                                "Name": "Expected",
                                "Value": k["Value"]
                            }
                        ],
                        "Result": {
                        },
                        "Language": {
                            "en": {
                                "ActionName": "check control " + k["Name"],
                                "StepName": "check control value"
                            }
                        }
                    }
                    listCheckControl.append(checkControl)
                return listCheckControl
    return listCheckControl

def AC_UseTestData(currentScript, index,dataFolder):
    listCheckControl = []
    if currentScript[index]["ClassName"] == "AC_UseTestData":
        layoutName = currentScript[index]["Params"][0]["Value"]
        casenum = currentScript[index]["Params"][1]["Value"]
        # get testdata file name
        openFileData = dataFolder + "/TestDatas/" + layoutName + ".json"

        # read testdata file
        with codecs.open(openFileData,encoding='utf-8') as flats:
            dataCase = json.load(flats)

        # transform testdata into test script
        for l in dataCase["listCases"]:
            if int(l["casenum"]) == int(casenum):
                for k in l["listControl"]:
                    controlType = get_controlType(layoutName, k["Name"], dataFolder)
                    inputAction = condition_useTestData(controlType, layoutName, k["Name"], k["Value"])
                    
                    if inputAction != "":
                        listCheckControl.append(inputAction)
                return listCheckControl
    return listCheckControl

def Execute(args):
    #getting paramenters value
    runningUser = args[1]
    tsName = args[2]
    tsName = tsName.replace("~"," ")
    lsTestcases = args[3].replace("~"," ")
    lsTestcases = lsTestcases.split(";")
    runningDate = args[4].replace("~","_")
    runningOS = args[5]
    timeout = args[6]
    resourceFolder = args[7]
    realOS = args[8]
    realOS = realOS.replace("~"," ")
    runingLocation = args[9]
    runingLocation = runingLocation.replace("~"," ")
    screenRes = args[10]
    screenRes = screenRes.replace("~"," ")
    mobileMode = args[11]
    if mobileMode == "true":
        mobileRes = args[12]
        deviceName = args[13]
        deviceName = deviceName.replace("~"," ")
        jobName = args[14]
        browserName = args[15]
        isRecordVideo = args[16]
        projectName = args[17]
        runDescription = args[18]
        projectId = args[19]
        runningName = args[20]
    else:
        mobileRes = ""
        deviceName = ""
        jobName = args[12]
        browserName = args[13]
        isRecordVideo = args[14]
        projectName = args[15]
        runDescription = args[16]
        projectId = args[17]
        runningName = args[18]
        
    dataFolder = resourceFolder + "/" + tsName
    resultFolder = dataFolder + "/Results/"
    executeFile = resultFolder+"/Execute_"+runningUser+"_"+runningDate+".json"
    resultName = "Result_"+runningUser+"_"+runningDate
    
    #create executeReportFile
    generateExecutingFile(dataFolder, executeFile, lsTestcases, runningUser, tsName, runningOS, timeout, realOS, runingLocation, screenRes, mobileMode, mobileRes, deviceName, jobName, runningDate, browserName, isRecordVideo, projectName, resultName, runDescription, projectId, runningName)
    

#python ./BotTest_GenerateExecuteFile.py "runningUser" "TestsuiteName" "Testcase1 Testcase2" "20181023_100000" "MacOS"
#python ./BotTest_GenerateExecuteFile.py "MrA" "TestsuiteName" "Testcase1 Testcase2" "20181023_100000" "MacOS"


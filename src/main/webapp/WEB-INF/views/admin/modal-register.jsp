<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="modal fade formUser" tabindex="1" role="dialog" id="modalRegister">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h4 class="modal-title mt-0">Create User</h4>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="registerUser" id="registerUser" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
	       	<input type="hidden" name="token" id="token" value="${dto.getToken()}"/>
	       	<div class="row item form-group bad">
			    <label for="firstName" class="col-md-4 control-label text-left">First Name<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa firstName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="firstName" required="required" name="firstName" placeholder="Enter first name">
			      	 </div>
			    </div>
		  	</div>
		  		<div class="row item form-group bad">
			    <label for="lastName" class="col-md-4 control-label text-left">Last Name<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa lastName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="lastName" required="required" name="lastName" placeholder="Enter last name">
			      	 </div>
			    </div>
		  	</div>
	       	<div class="row item form-group bad emailName">
			    <label for="inputEmail3" class="col-md-4 control-label text-left">Email<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa userName" data-toggle="tooltip"></i>
			      		<input type="email" class="form-control" id="inputEmail" required="required" name="username" placeholder="Enter email" value="${dto.getEmail()}">
			      	 </div>
			    </div>
		  	</div>
		  	<div class="row item form-group bad">
			    <label for="inputPassword3" class="col-md-4 control-label text-left">Password<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<div class="input-icon right"><i class="fa passWord"></i>
			      		<input type="password" class="form-control" id="inputPassword" name="password" required="required" placeholder="Enter password">
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad passConfirm">
			    <label for="passconfirmation" class="col-md-4 control-label text-left">Password Confirmation<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<div class="input-icon right"><i class="fa confirmPass"></i>
			      		<input type="password" class="form-control" id="passconfirmation" required="required" name="passconfirmation" placeholder="Enter password Confirmation">
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad companyCheck" >
			    <label for="passconfirmation" class="col-md-4 control-label text-left">Your Company Name<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<div class="input-icon right"><i class="fa checkCompany"></i>
			      		<input class="form-control" type="text" name="company" required="required" id="company" placeholder="Your comapany name">
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad">
			    <label for="country" class="col-md-4 control-label text-left">Country<span class="required">*</span></label>
			    <div class="col-md-8">
			      	<select name="country" required="required" id="country" class="form-control">
                        <option label="Country" value="">Country</option>
                        <option label="Afghanistan" value="AF">Afghanistan</option>
                        <option label="Aland Islands" value="AX">Aland Islands</option>
                        <option label="Albania" value="AL">Albania</option>
                        <option label="Algeria" value="DZ">Algeria</option>
                        <option label="Andorra" value="AD">Andorra</option>
                        <option label="Angola" value="AO">Angola</option>
                        <option label="Anguilla" value="AI">Anguilla</option>
                        <option label="Antarctica" value="AQ">Antarctica</option>
                        <option label="Antigua and Barbuda" value="AG">Antigua and Barbuda</option>
                        <option label="Argentina" value="AR">Argentina</option>
                        <option label="Armenia" value="AM">Armenia</option>
                        <option label="Aruba" value="AW">Aruba</option>
                        <option label="Australia" value="AU">Australia</option>
                        <option label="Austria" value="AT">Austria</option>
                        <option label="Azerbaijan" value="AZ">Azerbaijan</option>
                        <option label="Bahamas" value="BS">Bahamas</option>
                        <option label="Bahrain" value="BH">Bahrain</option>
                        <option label="Bangladesh" value="BD">Bangladesh</option>
                        <option label="Barbados" value="BB">Barbados</option>
                        <option label="Belarus" value="BY">Belarus</option>
                        <option label="Belgium" value="BE">Belgium</option>
                        <option label="Belize" value="BZ">Belize</option>
                        <option label="Benin" value="BJ">Benin</option>
                        <option label="Bermuda" value="BM">Bermuda</option>
                        <option label="Bhutan" value="BT">Bhutan</option>
                        <option label="Bolivia, Plurinational State of" value="BO">Bolivia, Plurinational State of</option>
                        <option label="Bonaire, Sint Eustatius and Saba" value="BQ">Bonaire, Sint Eustatius and Saba</option>
                        <option label="Bosnia and Herzegovina" value="BA">Bosnia and Herzegovina</option>
                        <option label="Botswana" value="BW">Botswana</option>
                        <option label="Bouvet Island" value="BV">Bouvet Island</option>
                        <option label="Brazil" value="BR">Brazil</option>
                        <option label="British Indian Ocean Territory" value="IO">British Indian Ocean Territory</option>
                        <option label="Brunei Darussalam" value="BN">Brunei Darussalam
                        </option>
                        <option label="Bulgaria" value="BG">Bulgaria</option>
                        <option label="Burkina Faso" value="BF">Burkina Faso</option>
                        <option label="Burundi" value="BI">Burundi</option>
                        <option label="Cambodia" value="KH">Cambodia</option>
                        <option label="Cameroon" value="CM">Cameroon</option>
                        <option label="Canada" value="CA">Canada</option>
                        <option label="Cape Verde" value="CV">Cape Verde</option>
                        <option label="Cayman Islands" value="KY">Cayman Islands</option>
                        <option label="Central African Republic" value="CF">Central African Republic</option>
                        <option label="Chad" value="TD">Chad</option>
                        <option label="Chile" value="CL">Chile</option>
                        <option label="China" value="CN">China</option>
                        <option label="Chinese Taipei" value="TW">Chinese Taipei</option>
                        <option label="Christmas Island" value="CX">Christmas Island
                        </option>
                        <option label="Cocos (Keeling) Islands" value="CC">Cocos (Keeling) Islands</option>
                        <option label="Colombia" value="CO">Colombia</option>
                        <option label="Comoros" value="KM">Comoros</option>
                        <option label="Congo" value="CG">Congo</option>
                        <option label="Congo, the Democratic Republic of the"  value="CD"> Congo, the Democratic Republic of the</option>
                        <option label="Cook Islands" value="CK">Cook Islands</option>
                        <option label="Costa Rica" value="CR">Costa Rica</option>
                        <option label="Cote d'Ivoire" value="CI">Cote d'Ivoire</option>
                        <option label="Croatia" value="HR">Croatia</option>
                        <option label="Cuba" value="CU">Cuba</option>
                        <option label="Curaçao" value="CW">Curaçao</option>
                        <option label="Cyprus" value="CY">Cyprus</option>
                        <option label="Czech Republic" value="CZ">Czech Republic</option>
                        <option label="Denmark" value="DK">Denmark</option>
                        <option label="Djibouti" value="DJ">Djibouti</option>
                        <option label="Dominica" value="DM">Dominica</option>
                        <option label="Dominican Republic" value="DO">Dominican Republic
                        </option>
                        <option label="Ecuador" value="EC">Ecuador</option>
                        <option label="Egypt" value="EG">Egypt</option>
                        <option label="El Salvador" value="SV">El Salvador</option>
                        <option label="Equatorial Guinea" value="GQ">Equatorial Guinea
                        </option>
                        <option label="Eritrea" value="ER">Eritrea</option>
                        <option label="Estonia" value="EE">Estonia</option>
                        <option label="Ethiopia" value="ET">Ethiopia</option>
                        <option label="Falkland Islands (Malvinas)" value="FK">Falkland Islands (Malvinas)</option>
                        <option label="Faroe Islands" value="FO">Faroe Islands</option>
                        <option label="Fiji" value="FJ">Fiji</option>
                        <option label="Finland" value="FI">Finland</option>
                        <option label="France" value="FR">France</option>
                        <option label="French Guiana" value="GF">French Guiana</option>
                        <option label="French Polynesia" value="PF">French Polynesia
                        </option>
                        <option label="French Southern Territories" value="TF">French Southern Territories</option>
                        <option label="Gabon" value="GA">Gabon</option>
                        <option label="Gambia" value="GM">Gambia</option>
                        <option label="Georgia" value="GE">Georgia</option>
                        <option label="Germany" value="DE">Germany</option>
                        <option label="Ghana" value="GH">Ghana</option>
                        <option label="Gibraltar" value="GI">Gibraltar</option>
                        <option label="Greece" value="GR">Greece</option>
                        <option label="Greenland" value="GL">Greenland</option>
                        <option label="Grenada" value="GD">Grenada</option>
                        <option label="Guadeloupe" value="GP">Guadeloupe</option>
                        <option label="Guam" value="GU">Guam</option>
                        <option label="Guatemala" value="GT">Guatemala</option>
                        <option label="Guernsey" value="GG">Guernsey</option>
                        <option label="Guinea" value="GN">Guinea</option>
                        <option label="Guinea-Bissau" value="GW">Guinea-Bissau</option>
                        <option label="Guyana" value="GY">Guyana</option>
                        <option label="Haiti" value="HT">Haiti</option>
                        <option label="Heard Island and McDonald Islands" value="HM">Heard Island and McDonald Islands</option>
                        <option label="Holy See (Vatican City State)" value="VA">Holy See (Vatican City State)</option>
                        <option label="Honduras" value="HN">Honduras</option>
                        <option label="Hungary" value="HU">Hungary</option>
                        <option label="Iceland" value="IS">Iceland</option>
                        <option label="India" value="IN">India</option>
                        <option label="Indonesia" value="ID">Indonesia</option>
                        <option label="Iran, Islamic Republic of" value="IR">Iran, Islamic Republic of</option>
                        <option label="Iraq" value="IQ">Iraq</option>
                        <option label="Ireland" value="IE">Ireland</option>
                        <option label="Isle of Man" value="IM">Isle of Man</option>
                        <option label="Israel" value="IL">Israel</option>
                        <option label="Italy" value="IT">Italy</option>
                        <option label="Jamaica" value="JM">Jamaica</option>
                        <option label="Japan" value="JP">Japan</option>
                        <option label="Jersey" value="JE">Jersey</option>
                        <option label="Jordan" value="JO">Jordan</option>
                        <option label="Kazakhstan" value="KZ">Kazakhstan</option>
                        <option label="Kenya" value="KE">Kenya</option>
                        <option label="Kiribati" value="KI">Kiribati</option>
                        <option label="Korea" value="KP">Korea</option>
                        <option label="Kuwait" value="KW">Kuwait</option>
                        <option label="Kyrgyzstan" value="KG">Kyrgyzstan</option>
                        <option label="Lao People's Democratic Republic" value="LA">Lao People's Democratic Republic</option>
                        <option label="Latvia" value="LV">Latvia</option>
                        <option label="Lebanon" value="LB">Lebanon</option>
                        <option label="Lesotho" value="LS">Lesotho</option>
                        <option label="Liberia" value="LR">Liberia</option>
                        <option label="Libyan Arab Jamahiriya" value="LY">Libyan Arab Jamahiriya</option>
                        <option label="Liechtenstein" value="LI">Liechtenstein</option>
                        <option label="Lithuania" value="LT">Lithuania</option>
                        <option label="Luxembourg" value="LU">Luxembourg</option>
                        <option label="Macao" value="MO">Macao</option>
                        <option label="Macedonia, the former Yugoslav Republic of" value="MK">Macedonia, the former Yugoslav Republic of</option>
                        <option label="Madagascar" value="MG">Madagascar</option>
                        <option label="Malawi" value="MW">Malawi</option>
                        <option label="Malaysia" value="MY">Malaysia</option>
                        <option label="Maldives" value="MV">Maldives</option>
                        <option label="Mali" value="ML">Mali</option>
                        <option label="Malta" value="MT">Malta</option>
                        <option label="Martinique" value="MQ">Martinique</option>
                        <option label="Mauritania" value="MR">Mauritania</option>
                        <option label="Mauritius" value="MU">Mauritius</option>
                        <option label="Mayotte" value="YT">Mayotte</option>
                        <option label="Mexico" value="MX">Mexico</option>
                        <option label="Moldova, Republic of" value="MD">Moldova, Republic of
                        </option>
                        <option label="Monaco" value="MC">Monaco</option>
                        <option label="Mongolia" value="MN">Mongolia</option>
                        <option label="Montenegro" value="ME">Montenegro</option>
                        <option label="Montserrat" value="MS">Montserrat</option>
                        <option label="Morocco" value="MA">Morocco</option>
                        <option label="Mozambique" value="MZ">Mozambique</option>
                        <option label="Myanmar" value="MM">Myanmar</option>
                        <option label="Namibia" value="NA">Namibia</option>
                        <option label="Nauru" value="NR">Nauru</option>
                        <option label="Nepal" value="NP">Nepal</option>
                        <option label="Netherlands" value="NL">Netherlands</option>
                        <option label="New Caledonia" value="NC">New Caledonia</option>
                        <option label="New Zealand" value="NZ">New Zealand</option>
                        <option label="Nicaragua" value="NI">Nicaragua</option>
                        <option label="Niger" value="NE">Niger</option>
                        <option label="Nigeria" value="NG">Nigeria</option>
                        <option label="Niue" value="NU">Niue</option>
                        <option label="Norfolk Island" value="NF">Norfolk Island</option>
                        <option label="Norway" value="NO">Norway</option>
                        <option label="Oman" value="OM">Oman</option>
                        <option label="Pakistan" value="PK">Pakistan</option>
                        <option label="Palestinian Territory, Occupied" value="PS"> Palestinian Territory, Occupied</option>
                        <option label="Panama" value="PA">Panama</option>
                        <option label="Papua New Guinea" value="PG">Papua New Guinea
                        </option>
                        <option label="Paraguay" value="PY">Paraguay</option>
                        <option label="Peru" value="PE">Peru</option>
                        <option label="Philippines" value="PH">Philippines</option>
                        <option label="Pitcairn" value="PN">Pitcairn</option>
                        <option label="Poland" value="PL">Poland</option>
                        <option label="Portugal" value="PT">Portugal</option>
                        <option label="Puerto Rico" value="PR">Puerto Rico</option>
                        <option label="Qatar" value="QA">Qatar</option>
                        <option label="Reunion" value="RE">Reunion</option>
                        <option label="Romania" value="RO">Romania</option>
                        <option label="Russian Federation" value="RU">Russian Federation
                        </option>
                        <option label="Rwanda" value="RW">Rwanda</option>
                        <option label="Saint Barthélemy" value="BL">Saint Barthélemy
                        </option>
                        <option label="Saint Helena, Ascension and Tristan da Cunha" value="SH"
                           >Saint Helena, Ascension and Tristan da Cunha</option>
                        <option label="Saint Kitts and Nevis" value="KN">Saint Kitts and Nevis</option>
                        <option label="Saint Lucia" value="LC">Saint Lucia</option>
                        <option label="Saint Maarten (Dutch part)" value="SX">Saint Maarten (Dutch part)</option>
                        <option label="Saint Martin (French part)" value="MF">Saint Martin (French part)</option>
                        <option label="Saint Pierre and Miquelon" value="PM">Saint Pierre and Miquelon</option>
                        <option label="Saint Vincent and the Grenadines" value="VC">Saint Vincent and the Grenadines</option>
                        <option label="Samoa" value="WS">Samoa</option>
                        <option label="San Marino" value="SM">San Marino</option>
                        <option label="Sao Tome and Principe" value="ST">Sao Tome and Principe</option>
                        <option label="Saudi Arabia" value="SA">Saudi Arabia</option>
                        <option label="Senegal" value="SN">Senegal</option>
                        <option label="Serbia" value="RS">Serbia</option>
                        <option label="Seychelles" value="SC">Seychelles</option>
                        <option label="Sierra Leone" value="SL">Sierra Leone</option>
                        <option label="Singapore" value="SG">Singapore</option>
                        <option label="Slovakia" value="SK">Slovakia</option>
                        <option label="Slovenia" value="SI">Slovenia</option>
                        <option label="Solomon Islands" value="SB">Solomon Islands</option>
                        <option label="Somalia" value="SO">Somalia</option>
                        <option label="South Africa" value="ZA">South Africa</option>
                        <option label="South Georgia and the South Sandwich Islands" value="GS"
                           >South Georgia and the South Sandwich Islands</option>
                        <option label="South Korea" value="KR">South Korea</option>
                        <option label="South Sudan" value="SS">South Sudan</option>
                        <option label="Spain" value="ES">Spain</option>
                        <option label="Sri Lanka" value="LK">Sri Lanka</option>
                        <option label="Sudan" value="SD">Sudan</option>
                        <option label="Suriname" value="SR">Suriname</option>
                        <option label="Svalbard and Jan Mayen" value="SJ">Svalbard and Jan Mayen</option>
                        <option label="Swaziland" value="SZ">Swaziland</option>
                        <option label="Sweden" value="SE">Sweden</option>
                        <option label="Switzerland" value="CH">Switzerland</option>
                        <option label="Syrian Arab Republic" value="SY">Syrian Arab Republic
                        </option>
                        <option label="Tajikistan" value="TJ">Tajikistan</option>
                        <option label="Tanzania, United Republic of" value="TZ">Tanzania, United Republic of</option>
                        <option label="Thailand" value="TH">Thailand</option>
                        <option label="Timor-Leste" value="TL">Timor-Leste</option>
                        <option label="Togo" value="TG">Togo</option>
                        <option label="Tokelau" value="TK">Tokelau</option>
                        <option label="Tonga" value="TO">Tonga</option>
                        <option label="Trinidad and Tobago" value="TT">Trinidad and Tobago
                        </option>
                        <option label="Tunisia" value="TN">Tunisia</option>
                        <option label="Turkey" value="TR">Turkey</option>
                        <option label="Turkmenistan" value="TM">Turkmenistan</option>
                        <option label="Turks and Caicos Islands" value="TC">Turks and Caicos Islands</option>
                        <option label="Tuvalu" value="TV">Tuvalu</option>
                        <option label="Uganda" value="UG">Uganda</option>
                        <option label="Ukraine" value="UA">Ukraine</option>
                        <option label="United Arab Emirates" value="AE">United Arab Emirates
                        </option>
                        <option label="United Kingdom" value="GB">United Kingdom</option>
                        <option label="United States" value="US">United States</option>
                        <option label="Uruguay" value="UY">Uruguay</option>
                        <option label="Uzbekistan" value="UZ">Uzbekistan</option>
                        <option label="Vanuatu" value="VU">Vanuatu</option>
                        <option label="Venezuela, Bolivarian Republic of" value="VE">
                            Venezuela, Bolivarian Republic of</option>
                        <option label="Viet Nam" value="VN">Viet Nam</option>
                        <option label="Virgin Islands, British" value="VG">Virgin Islands, British</option>
                        <option label="Wallis and Futuna" value="WF">Wallis and Futuna
                        </option>
                        <option label="Western Sahara" value="EH">Western Sahara</option>
                        <option label="Yemen" value="YE">Yemen</option>
                        <option label="Zambia" value="ZM">Zambia</option>
                        <option label="Zimbabwe" value="ZW">Zimbabwe</option>
                    </select>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad">
			    <label for="language" class="col-md-4 control-label text-left">Language<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<select id="language" required="required" name="language" class="form-control">
                       <option value="English">English</option>
                       <option value="Japanese">Japanese</option>
                    </select>
                </div>
			 </div>
	  		 <div class="row item form-group bad">
			    <label for="businessRoles" class="col-md-4 control-label text-left">Business Role<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<select id="businessRoles" required="required" name="businessRoles" class="form-control">
                       <option value="">Business Role</option>
                       <option value="Director">Director</option>
                       <option value="Vice President">Vice President</option>
                       <option value="Chief Executive Officer">Chief Executive Officer</option>
                       <option value="Chief Operating Officer" >Chief Operating Officer</option>
                       <option value="Chief Financial Officer">Chief Financial Officer</option>
                       <option value="Chief Technology officer">Chief Technology officer</option>
                       <option value="Chief Marketing Officer">Chief Marketing Officer</option>
                       <option value="Chief Product Officer">Chief Product Officer</option>
                       <option value="Member of the Board">Member of the Board</option>
                       <option value="Business/Technical Executive">Business/Technical Executive</option>
                       <option value="Manager">Manager</option>
                       <option value="RPA Engineer/Developer">RPA Engineer/Developer</option>
                       <option value="Other">Other</option>
                   </select>
			    </div>
			 </div>
			 <div class=" item bad checkbox-requied form-group bad fix-top-checkbox-forminline">
			    <div class="col-md-8 pull-right">
				    <div class="checkbox checkbox-green form-check form-check-inline mr-2 input-icon right">
				    	<i class="fa checkAgree"></i>
				    	<input required="required" type="checkbox" id="checkboxTerms" name="checkboxTerms">
	                    <label for="checkboxTerms">
                            By proceeding, you acknowledge that you have read and accepted the A-People
                            <a href="#" data-aura-rendered-by="459:0">Terms and Conditions</a>, as well as Bottest’s
                            <a href="#" target="_blank">Terms</a>
                            and
                            <a href="#" target="_blank" >Privacy policy</a>
	                    </label>
				    </div> 
			    </div>
		    </div>
		    <div class=" item bad checkbox-requied form-group bad fix-top-checkbox-forminline">
			    <div class="col-md-8 pull-right">
			    	<div class="checkbox checkbox-green form-check form-check-inline mr-2 input-icon right">
			    		<i class="fa checkboxAgree"></i>
						<input type="checkbox" name="agrre" id="agrre" required="required">
						<label for="agrre">
	                   	 I agree to Bottest sending me communications regarding its products and
	                           services.
	                   </label>
					</div>
			 	</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class=" text-center form-group">
		        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" onclick="signUp();" id="btn-signup"><i class="ti-check mr-2"></i>Rigister</button>
		        <button data-dismiss="modal" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>
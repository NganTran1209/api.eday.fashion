<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="state-overview">
	<div class="row">
		<div class="col-lg-3 col-sm-6">
			<div class="overview-panel purple">
				<div class="symbol">
					<i class="icon-people usr-clr"></i>
				</div>
				<div class="value white">
					<p class="sbold addr-font-h1 counter" id="numbUser">0</p>
					<p>Users</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6">
			<div class="overview-panel deepPink-bgcolor">
				<div class="symbol">
					<i class=" icon-screen-desktop"></i>
				</div>
				<div class="value white">
					<p class="sbold addr-font-h1 counter" id="numbComponent">0</p>
					<p>Component</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6">
			<div class="overview-panel orange">
				<div class="symbol">
					<i class=" ti-desktop"></i>
				</div>
				<div class="value white">
					<p class="sbold addr-font-h1 counter" id="numbInterface">0</p>
					<p>Interface</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6">
			<div class="overview-panel blue-bgcolor">
				<div class="symbol">
					<i class="icon-folder-alt"></i> 
				</div>
				<div class="value white">
					<p class="sbold addr-font-h1 counter" id="numbProject">0</p>
					<p>Project</p>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="${context}/assets/js/management/admin/dashboard-admin.js"></script>
<script src="${context}/assets/js/management/countUp.js"></script>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="clearfix"></div>
<div class="w-100">
	<div class="card card-box card-topline-green" id="listCompProject"> 
		<div class="card-head card-head-icon">
           	<header><i class="icon-screen-desktop"></i> Component List</header>
           	<div class="tools">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
           	</div> 
          </div>
          <div class="card-body " style="">
          	<div class="form-group">
         		<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" data-toggle="modal" data-target="#modalEditComponent" onclick="callModalAddComponent();">
         			 <i class="fa fa-plus"></i> Creat new component
       			 </button>
         	</div> 
          		<div class="table-scrollable">
                    <table class="table table-hover table-checkable order-column full-width table-common" id="listComponentProject">
                        <thead>
                            <tr>
                                <th class="center"> ID</th>
                                <th class="center"> Group Code </th> 
                                <th class="center"> Value</th>
                                <th class="center"> Name </th>
                                <th class="center"> Default </th>
                                <th class="center"> Action </th>
                            </tr>
                        </thead>
                        <tbody id="tableComponentManagement">
						</tbody>
                  </table>
               </div>
          </div>
      </div>
</div>

<jsp:include page="/WEB-INF/views/admin/modal-edit-component.jsp"></jsp:include>
<script src="${context}/assets/js/management/admin/component-management.js"></script>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="clearfix"></div>
<div class="w-100">
	<div class="card card-box card-topline-green" id="listProject"> 
		<div class="card-head card-head-icon">
           	<header><i class="icon-people"></i> User List</header>
           	<div class="tools">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
           	</div> 
          </div>
          <div class="card-body " style="">
          	<div class="form-group">
         		<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" data-toggle="modal" data-target="#modalRegister">
         			 <i class="fa fa-plus"></i> Creat new user
       			 </button>
         	</div> 
          		<div class="table-scrollable">
                    <table class="table table-hover table-checkable order-column full-width table-common" id="listUserProject">
                        <thead>
                            <tr>
                            	<th width="10%"></th>
                                <th class="center" width="5%"> User Name </th>
                                <th class="center" width="20%"> Full Name </th> 
                                <th class="center"width="10%"> Company </th>
                                <th class="center" width="10%"> Email </th>
                                <th class="center" width="10%"> Country </th>
                                <th class="center" width="10%"> Business Role </th>
                                <th class="center" width="10%"> Language </th>
                                <th class="center" width="5%"> Role </th>
                                <th class="center" width="5%"> Active </th>
                                <th class="center" width="10%"> Action </th>
                            </tr>
                        </thead>
                        <tbody id="tableUserManagement">
						</tbody>
                  </table>
               </div>
          </div>
      </div>
</div>

<jsp:include page="/WEB-INF/views/admin/modal-register.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/admin/modal-edit-user.jsp"></jsp:include>
<script src="${context}/assets/js/management/admin/user-management.js"></script>
<script src="${context}/assets/js/management/signup.js"></script>
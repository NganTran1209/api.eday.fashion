<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="modal fade formUser" tabindex="1" role="dialog" id="modalEditComponent">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h4 class="modal-title mt-0">Edit Component</h4>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="formEditComponent" id="formEditComponent" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
        	<input type="hidden" id="idComponent" value="">
        	<div class="row item form-group bad" id="itemIdInterfaceLock">
			    <label for="editItemValue" class="col-md-4 control-label text-left">ID</label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa lastName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="idComponentLock" required="required" name="idComponentLock" disabled>
			      	 </div>
			    </div>
		  	</div>
	       	<div class="row item form-group bad">
			    <label for="editGroupCode" class="col-md-4 control-label text-left">Group Code<span class="required">*</span></label>
		    	<div class="col-md-8">
			      	 <select id="editGroupCode" class="form-control" name="editGroupCode">
					    <option value="RUNNING_OS">RUNNING_OS</option>
					    <option value="OS">OS</option>
					    <option value="LOCATION">LOCATION</option>
					    <option value="BROWSER">BROWSER </option>
					    <option value="RESOLUTION">RESOLUTION </option>
					    <option value="DEVICE">DEVICE</option>
					    <option value="BOTTESTTOOL">BOTTESTTOOL</option>
					</select>
			    </div>
		  	</div>
	  		<div class="row item form-group bad">
			    <label for="editItemValue" class="col-md-4 control-label text-left">Value<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa lastName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="editItemValue" required="required" name="editItemValue" placeholder="Enter component value">
			      	 </div>
			    </div>
		  	</div>
		 	 <div class="row item form-group bad companyCheck" >
			    <label for="editItemName" class="col-md-4 control-label text-left">Name<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<div class="input-icon right"><i class="fa checkCompany"></i>
			      		<input class="form-control" type="text" name="editItemName" required="required" id="editItemName" placeholder="Your component name">
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad">
			    <label for="editItemValue" class="col-md-4 control-label text-left">Default<span class="required">*</span></label>
		    	<div class="col-md-8 checkCompponent">
			      	<input type="checkbox" id="defaultComponent" name="defaultComponent" value="defaultComponent" class="form-check-input checkDefault" >
			      	<span class="checkmark"></span>
			    </div>
		  	</div>
		    <div class="clearfix"></div>
		    <div class=" text-center form-group">
		        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" id="editComponent" onclick="saveComponent();"><i class="ti-check mr-2"></i>Save</button>
		        <button data-dismiss="modal" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>
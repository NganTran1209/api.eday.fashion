<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="clearfix"></div>
<div class="w-100">
	<div class="card card-box card-topline-green" id="listSoftware"> 
		<div class="card-head card-head-icon">
           	<header><i class="ti-desktop"></i> Software List</header>
           	<div class="tools">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
           	</div> 
          </div>
          <div class="card-body " style="">
          	<div class="form-group">
         		<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" data-toggle="modal" data-target="#modalUploadSoftware" >
         			 <i class="fa fa-plus">&nbsp;&nbsp;</i>  Upload Sorfware
       			 </button>
         	</div> 
          		<div class="table-scrollable">
                    <table class="table table-hover table-checkable order-column full-width table-common" id="software">
                        <thead>
                            <tr>
                            	<th class="center"> Id </th> 
                                <th class="center"> Type </th> 
                                <th class="center"> Version</th>
                                <th class="center"> File name </th>
                                <th class="center"> Upload Date </th>
                                <th class="center"> Requirement </th>
                                <th class="center"> Release Notes </th>
                      			<th class="center"> Action </th> 
                            </tr>
                        </thead>
                        <tbody id="tableSoftware">
						</tbody>
                  </table>
               </div>
          </div>
      </div>
</div>
<jsp:include page="/WEB-INF/views/admin/modal-upload-software.jsp"></jsp:include>
<script src="${context}/assets/js/management/admin/software-management.js"></script>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddTestcaseToStone" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width:70%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0"><spring:message code="label.AddTestcaseToMilestone"></spring:message></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormTestCase()" id="btnCloseTestcase" >×</button>
			</div>
			<input id="milestoneNameText" hidden />
			<div class="modal-body ">
				<div class="form-row">
					<div class="col-md-2 form-group">
						<label><spring:message code="label.DefinedTestcase"></spring:message></label>
						<select class="form-control" id="milestonePredifinition" >
							<option></option>
							<option value="checked"><spring:message code="label.Predifined"></spring:message></option>
							<option value="unchecked"><spring:message code="label.Not-Yet"></spring:message></option>
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label><spring:message code="label.FilterTestsuite"></spring:message></label>
						<select class="form-control" id="filterTestsuite" >
							<option></option>
							<option value="default"><spring:message code="label.Defaultselect"></spring:message></option>
						</select>
					</div>
					<div class="col-md-2 form-group ml-auto">
						<label><spring:message code="label.Status"></spring:message></label>
						<select class="form-control" id="filterStatus">
							<option value=""></option>
							<option value="PASS"><spring:message code="label.PASS"></spring:message></option>
							<option value="FAILED"><spring:message code="label.FAILED"></spring:message></option>
							<option value="DEGRADE"><spring:message code="label.DEGRADE"></spring:message></option>
							<option value="NOT-RUN">- ( <spring:message code="label.NotRun"></spring:message> )</option>
						</select>
					</div>
				</div>
				<div class="w-100">
					<div class="table-scrollable modal-table-scrollable">
	                   <table class="table table-hover table-common table-add-testcase-milestone fixed_header">
	                       <thead class="bg-success">
	                           <tr class="tr-bottest-hd bg-success w-100 d-flex">
	                           		<th class="border-bottom-0 border-top-0" width="5%"></th>
									<th class="border-bottom-0 border-top-0" width="25%"><spring:message code="label.Testsuite"></spring:message></th>
									<th class="border-bottom-0 border-top-0" width="25%"><spring:message code="label.Testcase"></spring:message></th>
									<th class="border-bottom-0 border-top-0" width="25%"><spring:message code="label.Description"></spring:message></th>
									<th class="border-bottom-0 border-top-0" width="20%"><spring:message code="label.Status"></spring:message></th>
								</tr>
	                       </thead>
	                       <tbody id="listBodyDetailTestcaseMilestone">
	                       
	                       </tbody>
	                   </table>
	               </div>
	               <div class="preloader loading-important position-absolute progress-loadMainContent" id="loadTableDetailMilestone" style="display: none;">
					   <div class="loader" style="top:30%">
					        <div class="loader__figure"></div>
					        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
					    </div>
					</div>
				</div>
			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" onclick="saveMilestoneTestcaseToTable()" id="saveMilestoneTcToTable"><i class="ti-check mr-2"></i> <spring:message code="label.UpdateToMilestone"></spring:message></button>
				<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="resetFormTestCase()" data-dismiss="modal"><i class="ti-close mr-2"></i><spring:message code="label.Close"></spring:message></button>
			</div>
		</div>
	</div>
</div>

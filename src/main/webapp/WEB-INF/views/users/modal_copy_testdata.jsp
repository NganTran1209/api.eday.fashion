<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalCopyData" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleData" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Copy TestData</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_layout" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <div class="row item form-group">
				    	<label class="control-label col-md-4 col-sm-12 col-xs-4" for="name">
				        	Testsuite
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				        	<select id="testsuiteListss" class="form-control"></select>
					    </div>
					</div>
					
				    <div class="row item form-group">
				    	<p class="control-label col-md-7 col-sm-12 col-xs-4" id="messageErrors" style="color:red;"></p>
				    </div>
				    <input id ="nameTestDataOld" value="" type="hidden">
				    <input id="testsuiteId" value="" type="hidden">
				    <input id="projectId" value="${projectId}" type="hidden">
				    <input id="idTestData" value="" name="testDataId" hidden>
				    <div class="row form-group justify-content-center">
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" id="saveTestDataCopy" onclick="copyTestData();"><i class="ti-check mr-2"></i> Save</button>
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" data-dismiss="modal"><i class="ti-close mr-2"></i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	
	
	$("#testsuiteListss").on('change.select2', function(){
		var testsuiteName = $("#testsuiteListss").val();
		loadListScreenLayout(testsuiteName, "#listLayouts", false);
	})
</script>

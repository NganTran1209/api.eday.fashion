<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>
<div class="row box-main-setting-tabs changeAddIssue" id="drawSetting">
	<div class="col-md-4 form-group">
		<div class="card card-box card-topline-green h-100" id="projectInfoBox">
			<div class="card-head card-head-icon">
             	<header>
             		<i class="icon-info"></i> <spring:message code="label.ProjectInfo"></spring:message>
             	</header>
            	<div class="tools d-flex">
            		<a class=" icon-note btn-color mr-3 btn-edit-project" href="javascript:;" onclick="getProject();"></a> 
					<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
				</div>
           </div>
           <div class="card-body collapse-height ">
	           	<div id="infoProjectId" hidden></div>
           		<div class="w-100">
	           		<div class="infoProjectLabel"><spring:message code="label.ProjectName"></spring:message></div>
	           		<div id="infoProjectName" class="font-italic text-muted"></div>
	           	</div>
	           	<div class="w-100">
	           		<div class="infoProjectLabel"><spring:message code="label.ProjectType"></spring:message></div>
	           		<div id="infoProjectType" class="font-italic text-muted"></div>
	           	</div>
	           	<div class="w-100">
	           		<div class="infoProjectLabel"><spring:message code="label.Description"></spring:message></div>
	           		<div id="infoDesciptionType" class="font-italic text-muted"></div>
	           	</div>
	           	<div class="w-100">
	           		<div class="infoProjectLabel"><spring:message code="label.Status"></spring:message></div>
	           		<div id="infoStatusType" class="font-italic text-muted"></div>
	           	</div>
	           	<div class="w-100">
	           		<div class="infoProjectLabel"><spring:message code="label.DailyReportTime"></spring:message></div>
	           		<div id="infoReportTime" class="font-italic text-muted"></div>
	           	</div>
	           	<div class="w-100">
	           		<div class="infoProjectLabel"><spring:message code="label.TimeZone"></spring:message></div>
	           		<div id="infoReportTimeZone" class="font-italic text-muted"></div>
	           	</div>
           </div> 
        </div>
	</div>
	<div class="col-md-4 form-group">
		<div class="card card-box card-topline-green h-100" id="memberBox">
			<div class="card-head card-head-icon">
             	<header><i class="icon-people"></i> <spring:message code="label.Members"></spring:message></header>
            	<div class="tools">
					<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
             	</div>
           </div> 
           <div class="card-body collapse-height body-member-project">
           		<div class="w-100 form-group">
		   			<div class="regencyMember"><spring:message code="label.ProjectAdmin"></spring:message></div>
		   			<div class="role-admin"></div>
		        </div>
	           	<div class="w-100 form-group">
				   <div class="regencyMember"><spring:message code="label.Manager"></spring:message></div>
		           <div class="role-manager"></div>
		        </div>
	           	<div class="w-100 form-group">
				    <div class="regencyMember"><spring:message code="label.Developer"></spring:message></div>
				    <div class="role-developer"></div>
		        </div>
	           	<div class="w-100 form-group">
				     <div class="regencyMember"><spring:message code="label.Tester"></spring:message></div>
		           	  <div class="role-tester"></div>
		        </div>
	           	<div class="w-100 form-group">
	           		<div class="regencyMember"><spring:message code="label.Customer"></spring:message></div>
		           	<div class="role-customer"></div>
	           	</div> 
	           	<div class="w-100 form-group" id="user-add-invited">
		        </div>
	           <div class="w-100 d-inline-block text-center">
	          		<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success button-sm" onclick="showModalInvitedMember();"><i class="icon-user-follow mr-2"></i><spring:message code="label.InvitedMember"></spring:message></button> 
	      	   </div>
	      </div>
		</div>
	</div>
	<div class="col-md-4 form-group">
		<div class="card card-box card-topline-green h-100" id="contentTasks">
			<div class="card-head card-head-icon">
             	<header>
             		<spring:message code="label.Definition"></spring:message>
             	</header>
          	</div> 
			<div id="contentCode">
	          </div>
       </div>
	</div>
	
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>

<div id="modalEditMemberSetting" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Edit Member</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormEditRole()">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="EditMemberModal" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <div class="item form-group">
				       <label for="emailTo">
				        	Role Project<span class="required">*</span>
				        </label>
				        <div class="input-icon right">
			            	<select name="roleProjectEdit" id="roleProjectEdit" class="form-control">
			            		<option value="Manager">Manager</option>
			            		<option value="Developer">Developer</option>
			            		<option value="Tester">Tester</option>
			            		<option value="Customer">Customer</option>
			            	</select>
			        	</div>
				    </div>
				    <div class="form-group justify-content-center text-center">
				        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" onclick="updateRole();">
				        	<i class="ti-check mr-2"></i> Save
			        	</button>
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="resetFormEditRole();" data-dismiss="modal" >
				        	<i class="ti-close">&nbsp;</i>   Close
				        </button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="${context}/assets/js/management/overview-project.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#calSetting").addClass("active show");
	var projectId = ${projectId}; 
	var detailProject = ${settingValue};
	var listBug=${listBug};
 	var taskList = ${taskList};
 	var productTypeList = ${productTypeList};
	$("#infoProjectId").text(detailProject.id);
	$("#infoProjectName").text(detailProject.name);
	$("#infoProjectType").text(detailProject.type);
	$("#infoDesciptionType").text(detailProject.description);
	$("#infoReportTime").text(detailProject.reportTime);
	$("#infoReportTimeZone").text(detailProject.reportTimeZone);
	$("#infoStatusType").text(detailProject.status);
</script>
<jsp:include page="/WEB-INF/views/users/modal_add_new_project.jsp"></jsp:include>
<script src="${context}/assets/js/management/userprojects.js"></script>
<script src="${context}/assets/js/management/project-component.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_add_invite_member.jsp"></jsp:include>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div id="modalActionAPI" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModal">
	<div class="modal-dialog modal-70">
		<!-- Modal content-->
		<form class="" id="form-modal-api" action="javascript:void(0)" accept-charset="UTF-8" method="post">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<h4 class="modal-title mt-0">Action API</h4>
					<button type="button" class="close" data-dismiss="modal">×</button>
				</div>
				<div class="modal-body form-action-element p-0">
			        <div class="col-md-12 box-input-request p-3">
			        	<div class="item bad d-flex">
				                <div class="input-group-btn search-panel">
				                    <select class="form-control select-patch" name="Method" id="apiMethod" >
				                    	<option value="get">GET</option>
				                    	<option value="post">POST</option>
				                    	<!-- <option value="put">PUT</option>
				                    	<option value="delete">DELETE</option> -->
				                    </select>
				                </div>
				                <div class="form-group mb-0 w-100">
					                <div class="input-group input-icon right w-100 h-100">
						                <i class="fa mt-1" data-original-title=""></i>
					                	<input type="text" class="form-control input-patch" name="urlrequestApi" id="apiUrl" placeholder="Search term..." required="required" >
				                	</div>
			                	</div>
			                	<input type="text" class="form-control input-patch" name="outputVariable" id="outputVariable"   value="#Result" placeholder="Output variable...">
			            </div>
			        </div>
					<div class="borderBox shadow-none border-0 light card-box p-0 m-0 tabsBorderCustom" style="box-shadow: none;">
					    <div class="borderBox-title tabbable-line mb-0">
					        <ul class="nav nav-tabs float-left">
					            <li class="nav-item">
					                <a href="#paramsContent" data-toggle="tab" class="text-capitalize border-success active show" id="" name="Params">Params</a>
					            </li>
					            <li class="nav-item">
					                <a href="#authContent" data-toggle="tab" class="text-capitalize border-success" id="" name="Authorization">Authorization <span class="dote-auth ml-1"></span></a>
					            </li>
					            <li class="nav-item">
					                <a href="#headersContent" data-toggle="tab" class="text-capitalize border-success" id="" name="Header"> Headers</a>
					            </li>
					            <li class="nav-item">
					                <a href="#bodyContent" data-toggle="tab" class="text-capitalize border-success"  id="" name="Body">Body</a>
					            </li>
					        </ul>
					    </div>
					    <div class="borderBox-body">
					        <div class="tab-content">
					            <div class="tab-pane active show" id="paramsContent">
					                <jsp:include page="/WEB-INF/views/users/API_content_params.jsp"></jsp:include> 
					            </div>
					            <div class="tab-pane" id="authContent">
					            	<jsp:include page="/WEB-INF/views/users/API_content_authorization.jsp"></jsp:include> 
					            </div>
					            <div class="tab-pane" id="headersContent">
					                <jsp:include page="/WEB-INF/views/users/API_content_headers.jsp"></jsp:include>
					            </div>
					            <div class="tab-pane" id="bodyContent">
					                <jsp:include page="/WEB-INF/views/users/API_content_body.jsp"></jsp:include>
					            </div>
				            </div>
			            </div>
				    </div>
				</div> 
				<div class="modal-footer border-top justify-content-center mb-0" style="border-top: 1px solid #dee2e6!important;">
			        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" id="testActionAPI"><i class="ti-search mr-1"></i>Test</button> 
			        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success btn-save-send-material" id="saveAPI"><i class="ti-check mr-1"></i>Save</button>
			        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" data-dismiss="modal"><i class="ti-close mr-1"></i>Close</button>
		      	</div>
		      	<p style="color: #1dcb8b; text-align: center;" id="notificationSave"></p>
		      	<div class="collapse" id="collapseResponseTest">
			      	<!-- <div class="title-content-action text-muted font-weight-normal m-0">
		               	<div>Response</div>
		               	<div class="box-add-row-action">
							<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom" id="closeResponseAPI"><i class="ti-close mr-1"></i>Close</button>
						</div>
		            </div>
			      	<div class="content-response">
		           		<textarea rows="10" class="form-control border-bottom bg-white" placeholder="Hit Send to get a response" readonly="readonly" id="resposeTextArea"></textarea>
		           	</div> -->
		           	
		           	<div class="borderBox shadow-none border-0 light card-box p-0 m-0 tabsBorderCustom" style="box-shadow: none;">
					    <div class="borderBox-title tabbable-line mb-0">
					        <ul class="nav nav-tabs float-left">
					            <li class="nav-item">
					                <a href="#responseBody" data-toggle="tab" class="text-capitalize border-success active show" id="" name="Body">Body</a>
					            </li>
					            <li class="nav-item">
					                <a href="#responseHeader" data-toggle="tab" class="text-capitalize border-success" id="" name="Headers">Headers <span class="dote-auth ml-1"></span></a>
					            </li>
					            
					        </ul>
					        <ul class="nav nav-tabs float-right" style="padding: 6px">
					       		<li class="nav-item" style="margin-right: 10px">
									<p id="responseStatusCode"></p>
								</li>
					       		<li class="nav-item">
									<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom" id="closeResponseAPI"><i class="ti-close mr-1"></i>Close</button>
								</li>
					        </ul>
					    </div>
					    <div class="borderBox-body">
					        <div class="tab-content">
					            <div class="tab-pane active show" id="responseBody">
					                <div class="content-response">
						           		<textarea rows="10" class="form-control border-bottom bg-white" placeholder="" readonly="readonly" id="resposeTextArea"></textarea>
						           	</div>
					            </div>
					            <div class="tab-pane" id="responseHeader">
					            	<table class="table" style="margin-left: 10px;">
									  <thead>
									    <tr>
									      <th scope="col">Key</th>
									      <th scope="col">Value</th>
									    </tr>
									  </thead>
									  <tbody id="responseHeaderTBody">
									    
									  </tbody>
									</table>
					            </div>
				            </div>
			            </div>
				    </div>
	           	</div>
	           
			</div>
		</form>
	</div>
</div>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="borderBox shadow-none border-0 light card-box p-0 mb-3 tabsBorderCustom " style="border-radius:10px;overflow:hidden;height: 50px;margin-bottom: 10px;">
	<div class="borderBox-title tabbable-line mb-0" style="min-height: 50px">
	     <ul class="nav nav-tabs float-left">
	     	 <li class="nav-item" id="calSummaryTabs">
	             <a href="${context}/project/${projectId}/overview" class="text-uppercase border-success active show " id="tabsSummary"><spring:message code="label.Summary"></spring:message>  </a>
	         </li>
	         <li class="nav-item" id="calProjectMilestone">
	             <a href="${context}/project/${projectId}/milestones" class="text-uppercase border-success" id="calMilestonesTab">  <spring:message code="label.Milestone"></spring:message></a>
	         </li>
	         <%-- <li class="nav-item">
	             <a href="${context}/project/${projectId}/testsuites" class="text-uppercase border-success" id="callTestsuiteTabs"> <spring:message code="label.Testsuite"></spring:message></a>
	         </li>
	         <li class="nav-item">
	             <a href="${context}/project/${projectId}/testcases" class="text-uppercase border-success" id="callAutomationTestcaseTab"><spring:message code="label.AutoTc"></spring:message></a>
	         </li>
	         <li class="nav-item">
	             <a href="${context}/project/${projectId}/testcaseManual" class="text-uppercase border-success" id="callManualTestcaseTab"><spring:message code="label.ManualTc"></spring:message></a>
	         </li>
	         <li class="nav-item">
	             <a href="${context}/project/${projectId}/screenDesign" class="text-uppercase border-success" id="callScreenDesignTab"><spring:message code="label.ScreenDesign"></spring:message></a>
	         </li>
	         <li class="nav-item">
	             <a href="${context}/project/${projectId}/testData" class="text-uppercase border-success" id="callTestDataTab"><spring:message code="label.TestData"></spring:message></a>
	         </li> --%>
	         <li class="nav-item">
	             <a href="${context}/project/${projectId}/issue" class="text-uppercase border-success" id="callIssueTab"> <spring:message code="label.Issue"></spring:message></a>
	         </li>
	         <%-- <li class="nav-item" >
	             <a href="${context}/project/${projectId}/schedules" class="text-uppercase border-success"id="calSchedulerTabs">  <spring:message code="label.Schedule"></spring:message></a>
	         </li>
	         <li class="nav-item" >
	             <a href="${context}/project/${projectId}/runnings" class="text-uppercase border-success" id="calRunningTabs"> <spring:message code="label.Result"></spring:message> </a>
	         </li> --%>
	         <li class="nav-item" id="callSettingProjectTabs">
	             <a href="${context}/project/${projectId}/setting" class="text-uppercase border-success" id="calSetting"><spring:message code="label.Setting"></spring:message>  </a>
	         </li>
	         
	     </ul>
	</div> 
</div>
<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success stickyButtonOnScreen" onclick="addIssueAllScreen();"> 
   	<i class='ti-plus mr-2'></i> <spring:message code="label.Issue"></spring:message>
</button>
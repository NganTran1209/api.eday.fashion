<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>
<div class="w-100 changeAddIssue" id="box-schedule-init">
	<div class="card card-box card-topline-green w-100">
		<div class="card-head card-head-icon">
			<header>
				<i class="ti-flag-alt-2"></i> <spring:message code="label.Milestone"></spring:message> 
			</header>
			<div class="tools d-flex">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
			</div>
		</div>
		<div class="card-body collapse-height">
			<div class="form-group">
				<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" onclick="creatMileStone();">
			      	<i class="fa fa-plus">&nbsp;</i>  <spring:message code="label.Createnewmilestone"></spring:message>
				</button>
			</div>
			<div class="position-relative row">
				<div class="preloader progress-loadMainContent" id="loadlistMilestone" style="display: none;">
				    <div class="loader" style="top:30%">
				        <div class="loader__figure"></div>
				        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
				    </div>
				</div>
					<%-- <div class="col-md-12 mb-2">
			         <div class="card card-box card-topline-green">
			             <div class="card-head card-head-icon">
			             	<div class="col-md-3">
			             		<header><spring:message code="label.Issue"></spring:message></header>
			             	</div>
			                 
			                 <div class="col-md-9 legend headerLe w-100 text-right mr-3" id="legendIssue"></div>
			             </div>
			             <div class="card-body no-padding height-9">
			             	<div class="text-danger" style="text-align: center;" id="issueCircleEmpty"></div>
			                 <div class="row" id="progressIssueMilestone">
			                 	
			                </div>
			            </div>
			        </div>
			    </div> --%>
				<div class="col-md-12 px-2">
					<div class="row m-0" id="listMilestone"></div>
				</div>
			
			</div>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
	
<script src="${context}/assets/js/management/milestone.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#calMilestonesTab").addClass("active show");
	
	$("#loadlistMilestone").show();
	$("#listMilestone").empty();
	
	var listMileStone = ${milestoneLs};
	var projectId = ${projectId};
	var listBug=${listBug};
 	var taskList = ${taskList};
	drawProjectMilestone(listMileStone, projectId);
	
	$("#loadlistMilestone").hide();
</script>

<jsp:include page="/WEB-INF/views/users/modal_add_testcase_to_milestone.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_add_milestone.jsp"></jsp:include>
<script src="${context}/assets/js/management/modal_add_testcase_to_milestone.js"></script>

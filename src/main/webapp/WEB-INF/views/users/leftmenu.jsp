<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<!-- start sidebar menu -->
	<div class="sidebar-container">
		<div class="sidemenu-container navbar-collapse collapse fixed-menu">
              <div id="remove-scroll wewqe">
                  <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200">
                      <li class="sidebar-toggler-wrapper hide">
                          <div class="sidebar-toggler">
                              <span></span>
                          </div>
                      </li>
                      <li class="sidebar-user-panel">
                          <div class="user-panel">
                              <div class="row">
                                         <div class="sidebar-userpic">
                                             <img src="${context}/assets/img/dp.jpg" class="img-responsive" alt=""> </div>
                                     </div>
                                     <div class="profile-usertitle">
                                         <div class="sidebar-userpic-name" id="nameUser"></div>
                                         <div class="profile-usertitle-job" id="role"></div>
                                     </div>
                                     <div class="sidebar-userpic-btn">
                                     <spring:message code="label.Profile" var="Profile" ></spring:message>
						        <a class="tooltips" href="#" data-placement="top" data-original-title="${Profile}" data-toggle="modal" data-target="#modalEditMember">
						        	<i class="material-icons"><spring:message code="label.person_outline" ></spring:message></i>
						        </a>
						        <!--<spring:message code="label.Mail" var="Mail" ></spring:message>
						        <a class="tooltips" href="email_inbox.html" data-placement="top" data-original-title="${Mail}">
						        	<i class="material-icons"><spring:message code="label.mail_outline" ></spring:message></i>
						        </a>
						        <spring:message code="label.Chat" var="Chat" ></spring:message>
						        <a class="tooltips" href="chat.html" data-placement="top" data-original-title="${Chat}">
						        	<i class="material-icons"><spring:message code="label.chat"></spring:message></i>
						        </a>-->
						        <spring:message code="label.Logout" var="Logout" ></spring:message>
						        <a class="tooltips" href="login.html" data-placement="top" data-original-title="${Logout}">
						        	<i class="material-icons"><spring:message code="label.input"></spring:message></i>
						        </a>
						    </div>
                          </div>
                      </li>
		           	<c:if test="${userDetails.role eq 'ROLE_ADMIN' }">
		           		<li class="menu-heading">
		                	<span>--<spring:message code="label.Management"></spring:message></span>
		                </li>
		           	</c:if>
                  </ul>
                  <ul class="sidemenu  page-header-fixed sidemenu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                  		<c:if test="${userDetails.role eq 'ROLE_ADMIN' }">
	                       <li class="nav-item start">
	                       		<span class="nav-link nav-toggle">
	                       			<i class="material-icons"><spring:message code="label.dashboard"></spring:message></i>
	                       			<a class="title" href="${context}/admin"><spring:message code="label.Dashboard"></spring:message></a>
	                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
	                     		</span>
							</li>
							<li class="nav-item start">
	                       		<span class="nav-link nav-toggle">
	                       			<i class="material-icons"><spring:message code="label.group"></spring:message></i>
	                       			<a class="title" href="${context}/admin/user"><spring:message code="label.UserManagement"></spring:message></a>
	                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
	                     		</span>
							</li>
							<li class="nav-item start">
	                       		<span class="nav-link nav-toggle">
	                       			<i class="material-icons">desktop_mac</i>
	                       			<a class="title" href="${context}/admin/component"><spring:message code="label.ComponentManagement"></spring:message></a>
	                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
	                     		</span>
							</li>
							<li class="nav-item start">
	                       		<span class="nav-link nav-toggle">
	                       			<i class="material-icons">dvr</i> 
	                       			<a class="title" href="${context}/admin/interface"><spring:message code="label.InterfaceManagement"></spring:message></a>
	                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
	                     		</span>
							</li>
							<li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">computer</i> 
                       			<a class="title" href="${context}/admin/software">Software Management</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						    </li>
					</c:if>
					 	<li class="nav-item start">
	                     	<span class="nav-link nav-toggle">
	                     		<i class="icon-cloud-download"></i> 
	                     		<a class="title" href="${context}/downloadSoftware"><spring:message code="label.DownloadSoftware"></spring:message></a>
	                     		<span class="arrow" style="display:none" id="arrow_version test"></span>
	                   		</span>
						</li>
						<li class="menu-heading">
		                	<span>-- <spring:message code="label.Project"></spring:message></span>
		                </li>
		                 <li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class=" ti-layout-grid2-alt"></i> 
                       			<a class="title" href="${context}/project"><spring:message code="label.ProjectManagement"></spring:message></a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
                  </ul>
                  <ul class="sidemenu sidemenuListProject  page-header-fixed sidemenu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                  
                  </ul>
              </div>
             </div>
         </div>
         <!-- end sidebar menu --> 
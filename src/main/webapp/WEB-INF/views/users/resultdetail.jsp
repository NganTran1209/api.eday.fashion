<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="card card-box card-topline-green">
	<div class="card-body">
		<div class="w-100">
			<ul class="stats-overview title-count-header list-unstyled">
				<li>
					<span class="value text-success" id="testcaseRun">0</span> <span class="name"> Total run testcases </span> 
				</li>
				<li class="hidden-phone">
					<span class="value text-success" id="testcasePass">0</span> <span class="name"> Total passed cases </span>
				</li>
				<li class="hidden-phone">
					<span class="value text-danger" id="testcaseFail">0</span> <span class="name"> Total failure cases </span> 
				</li>
			</ul>
 
		</div>
		<div class="row">
			<div class="col-md-4 form-group">
				<div class="card card-box rounded-1 border-success card-topline-green" id="testsuiteBox">
	                <div class="card-head border-success ">
	                    <header>Testsuite</header>
	                    <div class="tools">
	                   		<a class="bottest-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
	                    </div>
	                </div>
	                <div class="card-body no-padding height-9" style="">
	                    <div class="w-100">
	              			<div class="noti-information notification-menu">
							    <div class="slimScrollDiv h-auto" style="position: relative; overflow: hidden; width: auto;min-height:125px;">
							        <div class="x_content x_content-result-details" id="box-testsuite">
				                        
				                    </div>
							    </div>
							</div>
		                </div>
		            </div>
				</div>
			</div>
			<div class="col-md-4 form-group">
				<div class="card card-box rounded-1 border-success card-topline-green"  id="modeBox">
	                <div class="card-head border-success ">
	                    <header>Mode</header>
	                    <div class="tools">
	                   		<a class="bottest-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
	                    </div>
	                </div>
	                <div class="card-body no-padding height-9" style="">
	                    <div class="w-100">
	              			<div class="noti-information notification-menu">
							    <div class="slimScrollDiv h-auto" style="position: relative; overflow: hidden; width: auto;min-height:125px;">
							        <div class="x_content x_content-result-details" id="box-mode">
				                        
				                    </div>
							    </div>
							</div>
		                </div>
	                </div>
	            </div>
			</div>
			<div class="col-md-4 form-group">
				<div class="card card-box rounded-1 border-success card-topline-green"  id="environmentBox"> 
	                <div class="card-head border-success ">
	                    <header>Environment</header> 
	                    <div class="tools">
	                   		<a class="bottest-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
	                    </div>
	                </div>
	                <div class="card-body no-padding height-9" style="">
	                    <div class="w-100">
	              			<div class="noti-information notification-menu">
							    <div class="slimScrollDiv h-auto" style="position: relative; overflow: hidden; width: auto;min-height:125px;">
							        <div class="x_content x_content-result-details" id="box-environment">
				                       
			                        </div>
			                    </div>
						    </div>
						</div> 
	                </div>
	            
	            </div> 
	       </div>
		</div>
		<!--  <div id="bodyDetailResuft">
			<div class="col-md-12 p-0 form-group">
	           <div class="scroller scroller-left"><i class="ti-arrow-circle-left"></i></div>
	           <div class="scroller scroller-right"><i class="ti-arrow-circle-right"></i></div>
	           <div class="wrapper">
	               <ul class="nav nav-tabs navTabs-results list"></ul>
	           </div>
	        </div>
	        <div class="col-md-12 tab-content TabsContent-results">
	            
			</div>
		</div> -->
		<div id="bodyDetailResuft">
			<div class="col-md-12 p-0 form-group">
	           <button class="scroller scroller-left border-0"><i class="ti-arrow-circle-left"></i></button>
	           <button class="scroller scroller-right border-0"><i class="ti-arrow-circle-right"></i></button>
	           <div class="wrapper">
	               <ul class="nav nav-tabs navTabs-results list"></ul>
	           </div>
	        </div>
	        <div class="col-md-12 tab-content TabsContent-results">
	            
			</div>
		</div>
	</div>
</div>
<div class='video-wrapper'>
    <div class='video'>
        <video id="player" width="700" height="400" src=''  controls autoplay style="width:700px;outline: none;"></video>
    </div>
</div>
<script>

var testsuiteId = "${testsuiteId}";
var glProjectId = "${projectId}";
var resultDetail = ${resultDetail}; 
var glTestsuiteId= "${testsuiteId}";
var testCaseName = "${testCaseName}";
$( document ).ready(function() {
	projectId = glProjectId;
	checkBreadcrumb();
	drawResultDetail(resultDetail);
	countNumberInfo(resultDetail);
});
</script>  
<script src="${context}/assets/js/management/resultdetail.js"></script>
<script src="${context}/assets/js/management/pictureEyes.js"></script>
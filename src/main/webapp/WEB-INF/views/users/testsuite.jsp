<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="card card-box card-topline-green">
			<div class="card-body">
				<div class="w-100">
					<ul class="stats-overview title-count-header list-unstyled">
						<li>
							<span class="value text-info" id="totalTestcaseId">0</span> <span class="name">  <spring:message code="label.Totaltestcases"></spring:message></span> 
						</li>
						
						<li class="hidden-phone">
							<span class="value text-success" id="testcasepass">0</span> <span class="name"><spring:message code="label.Totaltestcasespassed"></spring:message>  </span>
						</li>
						<li class="hidden-phone">
							<span class="value text-danger" id="testcasefail">0</span> <span class="name"> <spring:message code="label.Totaltestcasesfailed"></spring:message> </span> 
						</li>
						<li class="hidden-phone">
							<span class="value text-degrade" id="degrateBug">0</span> <span class="name"> <spring:message code="label.DegradeBug"></spring:message> </span> 
						</li>
						<li class="hidden-phone">
							<span class="value" id="testcasenottest">0</span><span class="name"> <spring:message code="label.Totaltestcasesnottested"></spring:message> </span> 
						</li>
						<li class="hidden-phone">
							<span class="value text-warning" id="testcasePercent">0</span><span class="name"> <spring:message code="label.Passedrate"></spring:message></span> 
						</li>
					</ul>

				</div>
				<div class="borderBox shadow-none border-0 light card-box p-0 m-0 tabsBorderCustom" style="box-shadow: none;">
				    <div class="borderBox-title tabbable-line" id="tabs-viewTestsuite">
				        <ul class="nav nav-tabs float-left">
				            <li class="nav-item">
				                <a href="#Testcases" data-toggle="tab" class="text-uppercase border-success active show" id="testcaseTabs">
				                
				                	<spring:message code="label.AutomationTestcases"></spring:message> <span class="badge badge-success numbTestcase label-success shawdow-0 d-inline">0</span>
				                </a>
				            </li>
				            <li class="nav-item">
				                <a href="#manualTestcases" data-toggle="tab" class="text-uppercase border-success" id="manualTestcaseTabs">
				                	<spring:message code="label.ManualTestcases"></spring:message> <span class="badge badge-success label-success shawdow-0 d-inline" id="numbTestcaseManual">0</span>
				                </a>
				            </li>
				            <li class="nav-item">
				                <a href="#Layout" data-toggle="tab" class="text-uppercase border-success" id="layoutTabs"> 
				                	<spring:message code="label.ScreenDesign"></spring:message> <span class="badge badge-success label-success shawdow-0 d-inline" id="numbLayout">0</span>
			                	</a>
				            </li>
				            <li class="nav-item">
				                <a href="#testData" data-toggle="tab" class="text-uppercase border-success"  id="testDataTabs"> 
				                	<spring:message code="label.TestDatas"></spring:message> <span class="badge badge-success label-success shawdow-0 d-inline" id="numbTestData">0</span>
			                	</a>
				            </li>
				            <li class="nav-item" id="schedulerTabs">
				                <a href="#Scheduler" data-toggle="tab" class="text-uppercase border-success" id="calSchedulerTabs"> 
				                	<spring:message code="label.Schedules"></spring:message> <span class="badge badge-success label-success shawdow-0 d-inline" id="numbScheduler">0</span>
			                	</a>
				            </li>
				            <li class="nav-item">
				                <a href="#Results" data-toggle="tab" class="text-uppercase border-success" id="calRunningTabs"> 
				                	<spring:message code="label.AutomationResults"></spring:message> <span class="badge badge-success label-success shawdow-0 d-inline" id="numbResuft">0</span>
				                </a>
				            </li>
				        </ul>
				    </div>
				    <div class="borderBox-body">
				        <div class="tab-content">
				            <div class="tab-pane active show" id="Testcases">
				                <jsp:include page="/WEB-INF/views/users/testcase.jsp"></jsp:include>
				            </div>
				            <div class="tab-pane" id="manualTestcases">
				            	<jsp:include page="/WEB-INF/views/users/manual-testcase.jsp"></jsp:include>
				            </div>
				            <div class="tab-pane" id="Layout">
				                <jsp:include page="/WEB-INF/views/users/layout.jsp"></jsp:include>
				            </div>
				            <div class="tab-pane " id="testData">
				                <jsp:include page="/WEB-INF/views/users/testData.jsp"></jsp:include>
			                </div>
				            <div class="tab-pane " id="Scheduler">
				                <jsp:include page="/WEB-INF/views/users/schedule.jsp"></jsp:include> 
				            </div>
				            <div class="tab-pane " id="Results">
				                <jsp:include page="/WEB-INF/views/users/result.jsp"></jsp:include> 
				            </div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_Detail_Layout.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_add_new_testcase.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_Manual_Testcase_Detail.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_Update_Stt_Manual_Testcase_Detail.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_add_new_layout.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_add_new_screen.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_add_new_screen.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_action_API.jsp"></jsp:include> 

<jsp:include page="/WEB-INF/views/users/modal_copy_testdata.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_copy_testcase.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_coypy_testcase_manual.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_copy_layout.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_run_testcases.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_submit_generateTC.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_run_testcases.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_add_step_testscript.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_choose_screen_new_testData.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_upload_testData.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_run_scheduler.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_add_milestone.jsp"></jsp:include> 
<c:choose>
<c:when test="${ sessionScope.lang == 'ja'}">
<script src="${context}/assets/actions_ja.js" ></script>
</c:when>
<c:otherwise>
<script src="${context}/assets/actions.js" ></script>
</c:otherwise>
</c:choose>
<script>
	$(function(){
		  var hash = window.location.hash;
		  hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		  $('.nav-tabs a').click(function (e) {
		    $(this).tab('show');
		    var scrollmem = $('body').scrollTop();
		    window.location.hash = this.hash;
		    $('html,body').scrollTop(scrollmem);
		  });
	});
</script>
<script src="${context}/assets/js/management/testsuite.js"></script>
<script src="${context}/assets/js/management/testcase_detail.js"></script>
<script src="${context}/assets/js/management/modal_action_api.js"></script>
<script src="${context}/assets/js/management/manualTestcase_detail.js"></script>
<script src="${context}/assets/js/management/testscript-modal.js"></script>
<script src="${context}/assets/js/management/layouts.js"></script>
<script src="${context}/assets/js/management/layout.js"></script>
<script src="${context}/assets/js/management/testData.js"></script>
<script src="${context}/assets/js/management/testcases.js"></script>
<script src="${context}/assets/js/management/testcases_v2.js"></script>
<script src="${context}/assets/js/management/pictureEyes.js"></script>
<script src="${context}/assets/js/management/generateUTC.js"></script>
<script src="${context}/assets/js/management/scheduler.js"></script>

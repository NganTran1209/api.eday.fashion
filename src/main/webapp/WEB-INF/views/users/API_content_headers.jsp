<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<a class="title-content-action bg-white w-100">
	<div>Headers</div>
	<div class="box-add-row-action">
		<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success button-sm button-sm-custom" id="addRowHeaders"><i class="ti-plus"></i></button>
	</div>	
</a>
<table class="table table-bordered table-hover table-params m-0 " id="tableHeadersParams">
	<thead>
		<tr>
			<th class="border-bottom-0"></th>
			<th class="border-bottom-0">Key</th>
			<th class="border-bottom-0">Value</th>
			<th class="border-bottom-0">Description</th>
		</tr>
	</thead>
	<tbody>
<!-- 		<tr>
			<td class="text-center">
				<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>
			</td>
			<td>
				<input type="text" name="actionKeyHd" class="form-control" placeholder="Key" value="Content-Type" name="Key">
			</td>
			<td>
				<input type="text" name="actionValueHd" class="form-control"  placeholder="Value" value="application/x-www-form-urlencoded" name="Value">
			</td>
			<td>
				<input type="text" name="actionDescriptionHd" class="form-control"  placeholder="Description" name="Description">
			</td>
		</tr>
 -->
	</tbody>
</table>
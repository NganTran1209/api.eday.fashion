<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddStepTestscript" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Add Step</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="invitedMemberModal" action="javascript:void(0)" accept-charset="UTF-8" method="post">
					<input type="hidden" id="stepId">
				    <div class="item form-group bad ">
				        <label for="stepName">
				        	Step Name<span class="required">*</span>:
				        </label>
				        <div class="input-icon right"><i class="fa"></i>
			            	<input class="form-control" placeholder="Please enter" required="required" type="text" name="name" id="stepName">
			        	</div>
				    </div>
				   <div class="item form-group bad ">
				        <label for="stepAction">
				        	Action<span class="required">*</span>:
			        	</label>
				        <div class="input-icon right"><i class="fa"></i> 
				        	<select class="select2_single form-control list-action-modal" tabindex="1" aria-hidden="true">
								<option value="">Choose Action</option>
								<option value="AC_Open">open</option>
								<option value="AC_Enter">enter</option>
								<option value="AC_Click">click</option>
								<option value="AC_TakeScreenshot">take screenshot</option>
								<option value="AC_CloseBrowser">close browser</option>
								<option value="AC_SetVariable">set variable</option>
								<option value="AC_If">if</option>
								<option value="AC_Loop">loop</option>
								<option value="AC_EndLoop">end loop</option>
								<option value="AC_GetRangeValue">get range value</option>
								<option value="AC_UseBrowser">use browser</option>
								<option value="AC_Sleep">sleep</option>
								<option value="AC_Select">select</option>
								<option value="AC_Set">set</option>
								<option value="AC_Get">get</option>
								<option value="AC_CheckValue">check value</option>
								<option value="AC_SetJavascriptVariable">set javascript variable</option>
								<option value="AC_ExecuteJavascript">execute javascript</option>
								<option value="AC_GetJavascriptVariable">get javascript variable</option>
								<option value="AC_EndIf">end if</option>
								<option value="AC_ExecuteTestcase">execute testcase</option>
								<option value="AC_TakeFullScreen">take full screen</option>
								<option value="AC_GetCellColor">get cell color</option>
								<option value="AC_CheckCellColor">check cell color</option>
								<option value="AC_GetCellValue">get cell value</option>
								<option value="AC_CheckCellValue">check cell value</option>
								<option value="AC_GetRow">get row</option>
								<option value="AC_GetColumn">get column</option>
								<option value="AC_GetCsvFile">get csv file</option>
								<option value="AC_GetTableData">get table data</option>
								<option value="AC_SetControlValue">set control value</option>
								<option value="AC_GetControlValue">get control value</option>
								<option value="AC_CheckControlValue">check control value</option>
								<option value="AC_CompareImage">compare image</option>
								<option value="AC_TakeScreenshotWithResult">take screenshot with result</option>
								<option value="AC_ConnectDatabase">connect database</option>
								<option value="AC_ExecuteQuery">execute query</option>
								<option value="AC_CheckControlAttribute" selected="selected">check control attribute</option>
								<option value="AC_WriteToCSV">write to csv</option>
								<option value="AC_CheckValueFormat">check value format</option></select>
						</div>
				    </div>
				    <div class="item form-group bad ">
				        <label></label>
				        <div class="input-icon right"><i class="fa"></i>
			            	<input class="form-control" placeholder="Please enter" required="required" type="text" name="" id="">
			        	</div>
				    </div>
				    <div class="form-group justify-content-center">
				        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" onclick="">
				        	<i class="icon-user-follow"></i> Invinted
			        	</button>
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="" data-dismiss="modal" >
				        	<i class="ti-close"></i> Close
				        </button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>

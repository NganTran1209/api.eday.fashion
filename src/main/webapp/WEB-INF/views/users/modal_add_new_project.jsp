
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddNewProject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0"><spring:message code="label.AddNewProject" ></spring:message></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormInvite();" >×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_project" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <input type="hidden" name="id" id="idProject">
				    <div class="row item form-group bad newProjectName">
				        <label class="control-label col-md-3 col-sm-12 col-xs-3 text-left">
				        	<spring:message code="label.ProjectName" ></spring:message><span class="required">*</span>
				        </label>
				        <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				        	<div class="input-icon right"><i class="fa projectName"></i>
				        		<spring:message code="label.Pleaseenter"  var="Pleaseenter"></spring:message>
				            	<input class="form-control" placeholder="${Pleaseenter}" required="required" type="text" name="nameProject" id="nameProject" title="" alt="">
				        	</div>
				        </div>
				    </div>
				    <div class="row item form-group bad">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.ProjectType"></spring:message>
				        </label>
				         <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <select class="form-control" name="type" id="typeProject">
				            	<option><spring:message code="label.PublicProject"></spring:message></option>
				            	<option><spring:message code="label.PrivateProject"></spring:message></option>
				            </select> 
				        </div> 
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.Description" ></spring:message>
				        </label>
				       <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				      		<spring:message code="label.Pleaseenter"  var="Pleaseenter"></spring:message>
				            <textarea class="form-control" rows="3" placeholder="${Pleaseenter}" name="description" id="descriptionProject"></textarea>
				        </div>
				    </div>
				    <div class="row item form-group itemStatusProject " hidden>
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.Status" ></spring:message>
				        </label>
				        <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <select class="form-control" name="status" id="statusProject">
				            	<option value="Open">Open</option>
				            	<option value="Close">Close</option>
				            	<option value="Pending">Pending</option>
				            </select>
				        </div>
				    </div>
				    <div class="row item">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Product Type
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
			       			<input class="form-control ui-autocomplete-input" placeholder="${Pleaseenter}" type="text" name="productTypes" id="productType" title="" alt="">
				        </div>
				        <div class="form-group col-md-6">
					        <div class="row">
					        	<label class="col-md-6 col-sm-12 col-xs-9 text-right pt-2">
						        	Product Code
						        </label>
						        <div class="col-md-6 col-sm-12 col-xs-12 text-left">
						        	<div class="input-icon right"><i class="fa projectName"></i>
						       			<input class="form-control" placeholder="${Pleaseenter}" type="text" name="productCodes" id="productCode" title="" alt="">
						       		</div>
						       	</div>
					        </div>
				       </div>
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Product Name
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
							<input class="form-control" placeholder="${Pleaseenter}" type="text" name="productNames" id="productName" title="" alt="">
				        </div>
				        
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Standard Unit
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
							<select class="form-control" name="type" id="standardUnit">
				            	<option>m2</option>
				            	<option>con</option>
				            </select> 
				        </div>
				        <label class="col-md-3 col-sm-12 col-xs-9 text-right pt-2">
				        	Standard Size
				        </label>
				        <div class="col-md-3 col-sm-12 col-xs-12 text-left">
				        	<input class="form-control" placeholder="${Pleaseenter}" type="text" name="standardSizes" id="standardSize" title="" alt="">
				       	</div>
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Plan Start Date
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
				       		<div class="input-icon right"><i class="fa projectName"></i>
				       			<input type="text" id="planStDa" name="planStDa" value="" class="form-control date" placeholder="mm/dd/yyyy"/>
							</div>
				        </div>
				        <label class="col-md-3 col-sm-12 col-xs-9 text-right pt-2">
				        	Plan End Date
				        </label>
				        <div class="col-md-3 col-sm-12 col-xs-12 text-left">
				        	<div class="input-icon right"><i class="fa projectName"></i>
				       			<input type="text" id="planEnDa" name="planEnDa" value="" class="form-control date" placeholder="mm/dd/yyyy"/>
				       		</div>
				       	</div>
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Estimated Duration
				        </label>
				       <div class="col-md-3 col-sm-12 col-xs-12 text-left">
				       		<input class="form-control" placeholder="${Pleaseenter}" type="text" name="estimatedDurations" id="estimatedDuration" title="" alt="">
				       	</div>
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Estimated Productivity
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
				       		<input class="form-control" placeholder="${Pleaseenter}" type="text" name="estimatedProductivitys" id="estimatedProductivity" title="" alt="">
				        </div>
				        <label class="col-md-3 col-sm-12 col-xs-9 text-right pt-2">
				        	Production Unit
				        </label>
				        <div class="col-md-3 col-sm-12 col-xs-12 text-left">
				       		<input class="form-control" placeholder="${Pleaseenter}" type="text" name="productionUnits" id="productionUnit" title="" alt="">
				       	</div>
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	Estimated Price (VND)
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
				       		<input class="form-control" placeholder="${Pleaseenter}" type="text" name="estimatedPrices" id="estimatedPrice" title="" alt="">
							
				        </div>
				        <label class="col-md-3 col-sm-12 col-xs-9 text-right pt-2">
				        	Estimated Cost (VND)
				        </label>
				        <div class="col-md-3 col-sm-12 col-xs-12 text-left">
				       		<input class="form-control" placeholder="${Pleaseenter}" type="text" name="estimatedCosts" id="estimatedCost" title="" alt="">
				       	</div>
				    </div>
				    
				    <div class="row item form-group">
				    	<label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	StandardGAP
				        </label>
				       <div class="col-md-3 col-sm-3 col-xs-9 text-left">
				       		<input class="form-control" placeholder="${Pleaseenter}" type="text" name="standardGAPs" id="standardGAP" title="" alt="">
							
				        </div>
				    </div>
				    
				    <div class="row item form-group bad newProjectName">
				        <label class="control-label col-md-3 col-sm-12 col-xs-3 text-left">
				        	Files
				        </label>
				        <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				        	<div class="right">
				        		<input type="file" id="fileNameProject" name="fileNameProject" multiple hidden> 
								<label for="fileNameProject" class="label label-success label-history w-auto d-inline-block" id="chooseIdProject" style=""><spring:message code="label.Choosefile"></spring:message></label>
								<span class="ml-2 my-atuo">(<spring:message code="label.Maximumsize"></spring:message>: 100 MB)</span>
				        	</div>
				        	<div class="listFilesProject mt-2"></div>
				        </div>
				    </div>
				    
				    <div class="row form-group justify-content-center">
				        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" onclick="validateAddInvited();" id="svCreateProject">
				        	<i class="ti-check mr-2"></i><spring:message code="label.Save" ></spring:message>
			        	</button>
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="resetFormInvite();" data-dismiss="modal" >
				        	<i class="ti-close mr-2"></i><spring:message code="label.Close" ></spring:message>
				        </button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$('#planStDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#planEnDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#actualStDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#actualEnDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
</script>



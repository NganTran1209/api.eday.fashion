<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="newTask" hidden class="w-100">
	<div class="w-100 form-group" id="backIssueButton">
			<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListIssue('true')">
				<i class="icon-arrow-left mr-2"></i> Back
			</button>
	</div>	
	<div id="title-new"><spring:message code="label.NewIssue"></spring:message></div>
	<form action="javascript:void(0)" class="newTask form-horizontal" id="new_Issue" accept-charset="UTF-8" method="post" >
		<div class="box tabular">
			<div class="all_attributes col-md-11 mx-auto">
				<p id="idTask" class="hidden"></p>
				<div class="p_ckeditor row">
					<div class="col-md-2">
						<label class =""><spring:message code="label.Tracker"></spring:message> <span class="required"> *</span></label>
					</div>
					<div class="col-md-2 mb-3">
						<select class="form-control" name="tracker" id="trackerTask"></select>
					</div>
					<div class="col-auto">
						<label>Testsuite</label>
					</div>
					<div class="col-md-2 mb-3">
						<select class="form-control" name="testsuiteIssue" id="testsuiteIssue"></select>
					</div>
					<div class="col-auto">
						<label>Milestone</label>
					</div>
					<div class="col-md-2 mb-3">
						<select  name="milestoneIssue" id="milestoneIssue"  class="form-control"></select>
					</div>
					<div id="errorMilestone" class="col-12" style="color:red;"></div>
				</div>
				<div class="row mb-3">
					<div class="col-md-2 ">
						<label class =""><spring:message code="label.Subject"></spring:message> <span class="required"> *</span></label>
					</div>
					<div class="p_ckeditor col-md-10">
						<input maxlength="255" type="text" value="" name="subjectTask" id="subjectTask" class="form-control">
						<span id="errorSubject" style="color:red;"></span>
					</div>
				</div>
				<div class="row mb-3" id="descriptionNew">
					<div class="col-md-2">
						<label class ="">Description</label>
					</div>
					<div class="col-md-10">
						<textarea class="form-control" rows="5" placeholder="Please enter" name="descriptionTask" id="descriptionTask"></textarea>
					</div>
				</div>
				<div class="row" id="descriptionEdit" hidden>
					<div class="col-md-2">
						<label class ="">Description</label>
					</div>
					<div class="col-md-10">
						<p class="icon icon-edit" onclick="displayDescription();" id="edit-Description">Edit</p>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-2">
						<label class ="">Tools & Materials</label>
					</div>
					<div class="col-md-10">
						<div class="table-wrapper">
				            <div class="table-title">
				                <div class="row">
				                    <div class="col-sm-12">
				                        <button type="button" class="btn btn-info add-new" id="add"><i class="fa fa-plus"></i> Add New</button>
				                    </div>
				                </div>
				            </div>
				            <table class="table table-bordered" id="makeEditable">
				                <thead>
				                    <tr>
				                        <th>Name</th>
				                        <th>Quantity</th>
				                        <th>Price</th>
				                    </tr>
				                </thead>
				                <tbody id="planToolMaterial">
				                	
				                </tbody>
				            </table>
			        	</div>
		        	</div>
				</div>
				
				<div id="attributes" class="attributes">
					<div class="splitcontent row">
						<div class="splitcontentleft col-md-6">
							<div class="row mb-3">
								<div class="col-md-4">
									<label >Status <span class="required"> *</span></label>
								</div>
								<div class="col-md-7">
									<select name="status" id="statusTask"  class="form-control"></select>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4">
									<label><spring:message code="label.Priority"></spring:message> <span class="required"> *</span></label>
								</div>
								<div class="col-md-7">
									<select name="priority" id="priorityTask"  class="form-control"></select>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4">
									<label class =""><spring:message code="label.Assigneeto"></spring:message> </label>
								</div>
								<div class="col-md-7">
									<select name="assignee" id="assigneeTask"  class="form-control">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4">
									<label class ="">Category</label>
								</div>
								<div class="col-md-7">
									<select name="category" id="categoryTask"  class="form-control"></select>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4">
									<label class ="mx-2 w-auto" style="white-space: nowrap;"><spring:message code="label.Parenttask"></spring:message> </label>
								</div>
								<div class="col-md-7">
									<input size="10" type="text" name="parentTask" id="parentTask" class="ui-autocomplete-input autocomplete form-control" autocomplete="off">
									<span id="errorParentTask" style="color:red;"></span>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-8 ml-auto">
									<p class="err_duplicate_issue" style="color:red"></p>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4">
									<label class =""><spring:message code="label.Watchers"></spring:message></label>
								</div>
								<div class="dropdown show-watchers col-md-6">
									<a class="icon icon-add  tag-watchers dropdown-toggle pt-1" data-toggle="dropdown" ><spring:message code="label.Searchforwatcherstoadd"></spring:message></a>
									<ul class="dropdown-menu py-3" id="watchers">
									</ul>
									<div class='choose-watcher my-2'></div>
								</div>
								
							</div>
						</div>
						<div class="splitcontentright col-md-6">
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
									<label class =""> <spring:message code="label.Done"></spring:message></label>
								</div>
								<div class="col-md-6">
									<select name="assignee" id="donePercent" class="form-control">
										<option>0%</option>
										<option>10%</option>
										<option>20%</option>
										<option>30%</option>
										<option>40%</option>
										<option>50%</option>
										<option>60%</option>
										<option>70%</option>
										<option>80%</option>
										<option>90%</option>
										<option>100%</option>
									</select>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
									<label class =""><spring:message code="label.PlanStartDate"></spring:message></label>
								</div>
								<div class="col-md-6">
									<input size="15" value="" type="date" name="planStartDate" id="planStartDateTask" class=" form-control">
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
									<label class =""><spring:message code="label.PlanEndDate"></spring:message> </label>
								</div>
								<div class="col-md-6">
									<input size="15" value="" type="date" name="planEndDate" id="planEndDateTask" class=" form-control">
									<span id="errorPlanDate" style="color:red;"></span>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
								<label class =""><spring:message code="label.ActualStartDate"></spring:message></label>
								</div>
								<div class="col-md-6">
								<input size="15" value="" type="date" name="actualStartDate" id="actualStartDateTask" class=" form-control">
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
									<label class =""><spring:message code="label.ActualEndDate"></spring:message> </label>
								</div>
								<div class="col-md-6">
									<input size="15" value="" type="date" name="actualEndDate" id="actualEndDateTask" class="form-control">
									<span id="errorAcutalDate" style="color:red;"></span>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
									<label class =""><spring:message code="label.EstimatedTimes"></spring:message> </label>
								</div>
								<div class="col-md-6 input-group">
									<input size="10" value="" type="text" name="estimatedTime" id="estimatedTimeTask" class="form-control mr-2">
									<spring:message code="label.Hours"></spring:message>
								</div>
							</div> 
							<div class="row mb-3">
								<div class="col-md-4 ml-auto">
									<label class =""><spring:message code="label.ActualTimes"></spring:message> </label>
								</div>
								<div class="col-md-6 input-group">
									<input size="10" value="" type="text" name="actualTime" id="actualTimeTask" class="form-control mr-2">
									<spring:message code="label.Hours"></spring:message>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row mb-3" id="noteEdit">
					<div class="col-md-2">
						<label class =""> <spring:message code="label.Notes"></spring:message></label>
					</div>
					<div class="col-md-10">
						<textarea class="form-control" rows="3" placeholder="Please enter" name="notesTask" 
						id="notesTask"></textarea>
					</div>
				</div>
				
				<div class="row mb-3">
					<div class="col-md-2">
					<label class ="mr-5">Files </label>
					</div>
					<div class="col-md-7">
						<div class="w-100 d-flex">
							<input type="file" id="fileName" name="fileName" multiple hidden> 
							<label for="fileName" class="label label-success label-history w-auto d-inline-block" id="chooseId" style=""><spring:message code="label.Choosefile"></spring:message></label>
							<span class="ml-2 my-atuo">(<spring:message code="label.Maximumsize"></spring:message>: 100 MB)</span>
						</div>
						<div class="listFiles mt-2"></div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="footer-new-task text-center" id="saveIssueButton">
			<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask('issue')" id="create-task"><i class="ti-check mr-2"></i>Save</button>
		</div>
	</form>
</div>
<style>
	.table_issue, .th_month, .value_month td{
	  border: 1px solid #C4C4C4;
	  border-collapse: collapse;
	}
	.th_month th{
	  padding: 10px;
	  text-align: center;
	}
	.th_month {
		color: #FFFFFF;
		background-color: #1DCB8B;
	}
	.value_month input{
		border-color: white !important;
		width: 100%;
		max-width: 100% !important;
		padding-top: 8px;
		padding-bottom: 8px;
	}
</style>

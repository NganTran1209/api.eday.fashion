<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="preloader preloader-main">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting" ></spring:message><br> Bottest.io</p>
    </div>
</div>
<div class="col-md-12" id="modalDetailSoftware">
	<div class="row bg-success title-software">
	<h3 class="modal-title mt-3 "><spring:message code="label.DownloadSoftware"></spring:message></h3>  
	</div>
       <c:forEach items="${softwareVersionNews}" var="item">
	    <div class="row  infoSoftware">
			   <div class="softwarerow d-block">
				   <h4>${item.type}</h4>
				   <div class=" d-flex ">
					   <label for="editItemValue" class="col-md-2 ">Version</label>
			    	   	<p class="mb-2">${item.version}</p>
			       </div>   
		    	   <div class="d-flex">
					   <label for="editItemValue" class="col-md-2">File name</label>
			    	   	<p class="mb-2">${item.fileName}</p>
		    	   </div>
		    	   <div class="d-flex ">
					   <label for="editItemValue" class="col-md-2 ">Upload Date</label>
			    	   <p class="mb-2">${item.upDate}</p>
		    	   </div>
		    	   <div class=" d-flex">
					   <label for="editItemValue" class="col-md-2">Requirement</label>
					   <div>${item.requirement}</div>
		    	   </div>
		    	   <div class="d-flex">
					   <label for="editItemValue" class="col-md-2">Release Notes</label>
				       	<div>${item.releaseNote}</div>
		    	   </div>
		    	   <div class="d-flex ml-3">
			    	   <div class="downloadExtention">
			    		 	<a  href="${context}/VersionNewS?filename=${item.fileName}" class="mdl-button
			    		 	 mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm  d-flex"
			    		 	  id="downloadExtention"  download><i class="ti-download mt-2">&nbsp; </i>Download</a> 
			    	   </div>
	    	   	    </div>  
		    	</div>
		   </div>
	 </c:forEach>
</div>
<script>
var authoRole = "${userDetails.getAuthorities()}";
var username = "${userDetails.getUsername()}";
$( "body" ).removeClass("sidemenu-closed");
$(window).on('load', function(){ 
	$(".preloader").fadeOut("slow");
	$(".preloader").css("z-index"," 1040");
});
</script>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="clearfix"></div>
<div class="card card-box card-topline-green" id="detailAuto" hidden>
	<div class="card-body">
		<div class="w-100" id="detailsTestCase">
			<div class="w-100 form-group">
				<div class="col-md-12 form-group">
					<div class="col-md-12 mb-5">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backPageIssue();">
							<i class="icon-arrow-left"></i> <spring:message code="label.Back"></spring:message>
						</button>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<div class="mb-3">
								<i class="fa fa-circle my-auto mr-2"></i>
							 	<span><spring:message code="label.TestcaseName"></spring:message>:</span> 
							 	<span id="titleTestcase" class="text-success font-weight-bold"></span>
							</div>
							<div>
								<span class="mb-3 d-flex" id="discription"> <i class="fa fa-circle my-auto mr-3"></i><spring:message code="label.Description"></spring:message>:</span>
				            	<textarea class="form-control" rows="8" id="testcaseDescription"></textarea>
							</div>
						</div>
						<div class="col-md-6 form-group">
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Status"></spring:message>: </label>
								<div class="col-md-8">
									<select class="form-control assignTo " id="statusIssueDetail">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.AssignTo"></spring:message>: </label>
								<div class="col-md-8">
									<select class="form-control assignTo" id="filterAssignTos">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.PlanStartDate"></spring:message>: </label>
								<div class="col-md-8">
									<input id="planStarDa" class="form-control date" value="" type="date"/>
								</div>
								
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.PlanEndDate"></spring:message>: </label>
								<div class="col-md-8">
									<input id="planEndDa" class="form-control date" value="" type="date"/>
								</div>
								
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Time"></spring:message>: </label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-6">
											<spring:message code="label.EstimatedTimes" var ="estimatedTimes"></spring:message>
											<spring:message code="label.Enterestimatedtimes" var="enterestimatedtimes"></spring:message>
											<input id="estimatedTimeDetail" data-toggle="tooltip" data-placement="top" data-original-title="${estimatedTimes}" placeholder="${enterestimatedtimes}" class="form-control tooltip-primary" value=""/>
										</div>
										<div class="col-md-6">
											<spring:message code="label.ActualTimes" var="actualTimes"></spring:message>
											<input id="actualTimeDetail" data-toggle="tooltip" data-placement="top" data-original-title="${actualTimes}" placeholder="Enter Actual times" class="form-control tooltip-primary" value=""/>
										</div>
										
									</div>
								</div>
							</div>
							<div class="d-flex">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Status"></spring:message> : </label>
								<div class="col-md-8">
									<select class="form-control" id="categoryIssueDetail">
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
			<div class="position-relative w-100">
				
				<div class="preloader progress-loadMainContent" id="loadDetailTestCase">
				    <div class="loader">
				        <div class="loader__figure"></div>
				        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
				    </div>
				</div>
				
				<div class="col-md-12 form-group pb-3">
					<ol id="sortable"></ol>
					<input id="testsuiteNameT" hidden/>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group text-center">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backPageIssue();"><i class="icon-arrow-left mr-2"></i> <spring:message code="label.Back"></spring:message></button>
					    <button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" id="addStep"><i class="ti-plus mr-2"></i><spring:message code="label.AddStep"></spring:message></button>
					    <button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" id="saveStep" onclick="saveStepTask();" data-loading-text="
					    	<i class='fa fa-spinner fa-spin '></i> Processing save ..."> <i class='ti-check mr-2'></i> <spring:message code="label.Save"></spring:message>
					    </button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="screenTask" class="w-100 changeAddIssue">
	<div class="card card-box card-topline-green" id="listTask"> 
         <div class="card-body " style="">
         	<div class="col-md-12 form-group">
         		<div class="row">
         			<div class="col-md-12">
         				<div class="row mb-3" >
         					<spring:message code="label.Selecttracker" var = "selecttracker"></spring:message>
         					<spring:message code="label.SelectTestsuite" var="selectTestsuite"></spring:message>
	         			 	<spring:message code="label.SelectMilestone" var="selectMilestone"></spring:message>
	         			 	<spring:message code="label.SelectassignTo" var = "selectassignTo"></spring:message>
         					<spring:message code="label.Selectstatus" var = "selectstatus"></spring:message>
	         			 	<div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${selecttracker}"><select class="form-control" multiple="multiple" id="trackerF"></select></div>
                         	<div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${selectTestsuite}"><select class="form-control" multiple="multiple" id="testsuiteT"></select></div>
                         	<div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${selectMilestone}"><select class="form-control" multiple="multiple" id="milestoneT"></select></div>
	                        <div class="col-md-2 assignFile" data-toggle="tooltip" data-placement="top" data-original-title="${selectassignTo}"><select class="form-control " multiple="multiple" id="assigneeF"></select></div>
	                        <div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${selectstatus}"><select class="form-control" multiple="multiple" id="statusF"></select></div>
	         				<div class="col-md-2">
		         				<button class="btnPlay mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2 w-100" onclick="createFormNewTask()" id="new_task">
		         					<i class="ti-plus text-white mr-2"></i><spring:message code="label.NewIssue"></spring:message>
		         				</button>
	         				</div>
         				</div>
         				
         				<div class="row">
	         			 	<spring:message code="label.SelectPriority" var = "selectPriority"></spring:message>
	         			 	<spring:message code="label.PlanEndDate" var = "planEndDate"></spring:message>
	         			 	<spring:message code="label.PlanStartDate" var = "planStartDate"></spring:message>
	         			 	<spring:message code="label.EnterKeyword" var="enterDescription"></spring:message>
	         			 	<spring:message code="label.SelectCategory" var="selectCategory"></spring:message>
	         			 	<div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${planStartDate}"><input type="text" value="" class="form-control colorPlace" id="planStartDateF" placeholder="${planStartDate}"></div>
	                        <div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${planEndDate}"><input type="text"  value="" class="form-control colorPlace" id="planEndDateF" placeholder="${planEndDate}" ></div>
	                        <div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${selectPriority}"><select class="form-control" multiple="multiple" id="priorityF"></select></div>
         					<div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${selectCategory}"><select class="form-control" multiple="multiple" id="categoryF"></select></div>
         					<div class="col-md-2" data-toggle="tooltip" data-placement="top" data-original-title="${enterDescription}"><input type="text"  value="" class="form-control colorPlace" id="filterDescriptionTask" placeholder="${enterDescription}" ></div>
         					<div class="col-md-2">
		         				<button id="filTask" class="btnPlay mdl-button mdl-js-button mdl-js-ripple-effect btn-success w-100" onclick="filterTask();">
									<i class="ti-search text-white"> </i> <spring:message code="label.Search"></spring:message>
								</button>
	         				</div>
         				</div>
         			</div>
         		</div>
         	</div>
         	
       		<div class="row">
	       		<div class="col-2"><strong>Total Issue: </strong><span id="totalIssue"></span></div>
	       		<div class="col-3"><strong>Total plan hours: </strong><span id="totalTimePlan"></span> </div>
	       		<div class="col-3"><strong>Total spent hours: </strong><span id="totalTime"></span> </div>
       		</div>
          	<div class="table-scrollable">
                 <table class="table table-hover table-checkable order-column full-width table-common" id="taskList">
                     <thead>
                         <tr>
                         	 <th class=""> # </th> 
							<th class=""><spring:message code="label.Tracker"></spring:message></th>
							<th class="">   
								<div class="mb-1"><a style="font-weight: 700;" ><spring:message code="label.Testsuite"></spring:message></a></div>
   								<div> <a style="font-weight: 700;"><spring:message code="label.Milestone"></spring:message> </a></div>
							</th>
							<th class=""> 
								<div class="mb-1"><a style="font-weight: 700;"><spring:message code="label.Status"></spring:message> </a></div>
   								<div> <a style="font-weight: 700;"><spring:message code="label.Priority"></spring:message> </a></div>
							</th>
							<th class=""><spring:message code="label.Subject"></spring:message></th>
							<th class=""><spring:message code="label.Assigneeto"></spring:message></th>
							<th class=""> 
								<div class="mb-1"><a style="font-weight: 700;"><spring:message code="label.PlanStartDate"></spring:message> </a></div>
   								<div> <a style="font-weight: 700;"><spring:message code="label.ActualStartDate"></spring:message> </a></div>
							</th>
							<th class="">   
								<div class="mb-1"><a style="font-weight: 700;"><spring:message code="label.PlanEndDate"></spring:message></a></div>
   								<div> <a style="font-weight: 700;"><spring:message code="label.ActualEndDate"></spring:message> </a></div>
							</th>
							<th class="">   
								<div class="mb-1"><a style="font-weight: 700;"><spring:message code="label.EstimatedTimes"></spring:message></a></div>
   								<div> <a style="font-weight: 700;"><spring:message code="label.ActualTimes"></spring:message> </a></div>
							</th>
							<th class="">  <spring:message code="label.Done"></spring:message></th>
							<th class="center"> Action </th>
                         </tr>
                     </thead>
                     <tbody id="tableTask">
					</tbody>
               </table>
             </div>
          </div>
      </div>
</div>

<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>

<script src="${context}/assets/js/management/tasks.js" ></script>
<c:choose>
<c:when test="${ sessionScope.lang == 'ja'}">
<script src="${context}/assets/actions_ja.js" ></script>
</c:when>
<c:otherwise>
<script src="${context}/assets/actions.js" ></script>
</c:otherwise>
</c:choose>
<script>
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});
</script>

<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callIssueTab").addClass("active show");
	$('[data-toggle="tooltip"]').tooltip();
	var projectId = '${projectId}';
	var context = '${context}';
	var taskList = ${taskList};
	var listBug = ${listBug};
	var manualTcLs = ${testcaseMas};
	
	var trackerSS = '${trackerSS}';
	var assigneeSS = '${assigneeSS}';
	var statusSS = '${statusSS}';
	var prioritySS = '${prioritySS}';
	var categorySS = '${categorySS}';
	var planStartSS = "${planStartSS}";
	var planEndSS = "${planEndSS}";
	var testsuiteTSS = '${testsuiteTSS}';
	var miletoneTSS = '${milestoneTSS}';
	var descriptionSS = "${descriptionSession}";
	var userP = "${userP}";
	var checkSession = true;
	
	var trackerParse = "";
	if(trackerSS != ""){
		trackerParse = JSON.parse(trackerSS);
	}
	
	var assigneeParse = "";
	if(assigneeSS != ""){
		assigneeParse = JSON.parse(assigneeSS);
	}
	
	var statusParse = "";
	if(statusSS != ""){
		statusParse = JSON.parse(statusSS);
	}
	
	var priorityParse = "";
	if(prioritySS != ""){
		priorityParse = JSON.parse(prioritySS);
	}
	var categoryParse = "";
	if(categorySS != ""){
		categoryParse = JSON.parse(categorySS);
	}
	
	var testsuiteTParse = "";
	if(testsuiteTSS != ""){
		testsuiteTParse = JSON.parse(testsuiteTSS);
	}
	
	var milestoneTParse = "";
	if(miletoneTSS != ""){
		milestoneTParse = JSON.parse(miletoneTSS);
	}
	
	$('#planStartDateF').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	
	$('#planEndDateF').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	
	
	infProjectCom(function(projectCo){
		var trackerValue = [];
		var statusValue = [];
		var priorityValue = [];
		var categoryValue = [];
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Tracker"){
				trackerValue.push({ "id" :projectCo[i]["itemName"] ,"text" : projectCo[i]["itemName"] });
			} else if(projectCo[i]["groupCode"] == "Status"){
				statusValue.push({ "id" :projectCo[i]["itemName"] ,"text" : projectCo[i]["itemName"] });
			} else if(projectCo[i]["groupCode"] == "Priority"){
				priorityValue.push({ "id" :projectCo[i]["itemName"] ,"text" : projectCo[i]["itemName"] });
			} 
			else if(projectCo[i]["groupCode"] == "Category"){
				categoryValue.push({ "id" :projectCo[i]["itemName"] ,"text" : projectCo[i]["itemName"] });
			} 
		}
		trackerValue.push({ "id" :"Testcase" ,"text" : "Testcase" });
		statusValue.push({ "id" :"Open" ,"text" : "Open" });
		$("#trackerF").select2({
			placeholder: "Select tracker",
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: trackerValue,
			tags: true,
			dropdownParent: ""
		});
		$("#trackerF").select2({placeholder: getLang(currentLocale,"Select tracker"), theme: 'bootstrap'});
		$("#trackerF").val(trackerParse);
		$("#trackerF").trigger('change');
		
		$("#statusF").select2({
			placeholder: "Select status",
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: statusValue,
			tags: true,
			dropdownParent: ""
		});
		$("#statusF").select2({placeholder: getLang(currentLocale,"Select status"), theme: 'bootstrap'});
		$("#statusF").val(statusParse);
		$("#statusF").trigger('change');
		
		$("#priorityF").select2({
			placeholder: "Select Priority",
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: priorityValue,
			tags: true,
			dropdownParent: ""
		});
		$("#priorityF").select2({placeholder: getLang(currentLocale,"Select Priority"), theme: 'bootstrap'});
		$("#priorityF").val(priorityParse);
		$("#priorityF").trigger('change');
		
		$("#categoryF").select2({
			placeholder: "Select Category",
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: categoryValue,
			tags: true,
			dropdownParent: ""
		});
		$("#categoryF").select2({placeholder: getLang(currentLocale,"Select Category"), theme: 'bootstrap'});
		$("#categoryF").val(categoryParse);
		$("#categoryF").trigger('change');
	});
	
	function duplicateUser(arr){
		var duplicateIds = [];
		var valueDupl = arr
	    .map(e => e['lastname'] + e['firstname'])
	    .map((e, i, final) => final.indexOf(e) !== i && i)
	    .filter(obj => arr[obj])
	    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
		 $.each(valueDupl, function(index, value){
			 $.each(arr, function(indexArr, valueArr){
				 var name = valueArr["lastname"] + valueArr["firstname"]; 
				 if(name == value){
					 duplicateIds.push(valueArr);
				 }
			 })
		 })
		 
		 return duplicateIds ;
	}
	getUserProject(function(userProjectList){
		var duplicateUserTask = duplicateUser(userProjectList);
		var assigneValue = [];
		for(var i = 0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null && userProjectList[i]["firstname"] != null){
				var count = 0 ;
				if(duplicateUserTask.length > 0){
					$.each(duplicateUserTask, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+"()"+userProjectList[i]["username"]+")"+'</option>');
							assigneValue.push({"id":userProjectList[i]["username"],"text":userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"] +"("+userProjectList[i]["username"]+")"});
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+'</option>');
					assigneValue.push({"id":userProjectList[i]["username"],"text":userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]});
				}
			}
		}
		
		$("#assigneeF").select2({
			placeholder: getLang(currentLocale,"Select assign To"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: assigneValue,
			tags: true,
			dropdownParent: ""
		});
		$("#assigneeF").select2({placeholder: getLang(currentLocale,"Select assign To"), theme: 'bootstrap'});
		$("#assigneeF").val(assigneeParse);
		$("#assigneeF").trigger('change');
	});
	
	getListTestsuite("true", function(testsuitels){
		
		$("#testsuiteT").select2({
			placeholder: getLang(currentLocale,"Select Testsuite"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: testsuitels,
			dropdownParent: ""
		});
		$("#testsuiteT").select2({placeholder: getLang(currentLocale,"Select Testsuite"), theme: 'bootstrap'});
		$("#testsuiteT").val(testsuiteTParse);
		$("#testsuiteT").trigger('change');
		
	});
	
	getAllMilestoneInIssue(function(milestoneLs){
		var milestoneVal = [];
		$.each( milestoneLs, function(index, valuesItemTestsuite) { 
			milestoneVal.push({"id":valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"],"text":valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]});
			
		});
		
		$("#milestoneT").select2({
			placeholder: getLang(currentLocale,"Select Milestone"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: milestoneVal,
			dropdownParent: ""
		});
		$("#milestoneT").select2({placeholder: getLang(currentLocale,"Select Milestone"), theme: 'bootstrap'});
		$("#milestoneT").val(milestoneTParse);
		$("#milestoneT").trigger('change');
	});
	
	$("#planStartDateF").val(planStartSS);
	$("#planEndDateF").val(planEndSS);
	$("#filterDescriptionTask").val(descriptionSS);
	
	if((trackerSS != "" && trackerSS != "[]")|| (assigneeSS != "" && assigneeSS != "[]")  || 
		(statusSS != "" && statusSS != "[]")|| (prioritySS != "" && prioritySS != "[]") || (categorySS != "" && categorySS != "[]") || planStartSS != "" || planEndSS != ""||
		(testsuiteTSS != "" && testsuiteTSS != "[]") || (miletoneTSS != "" && miletoneTSS != "[]") || descriptionSS != ""){
		
	} else {
		if(userP == "PROJECT_ADMIN" || userP == "Manager"){
			assigneeParse = [];
			
		} else {
			assigneeParse = ["${namePrincipal}"];
			
		}
		
		trackerParse = [];
		statusParse = [];
		priorityParse = [];
		categoryParse = [];
		planStartSS = "";
		planEndSS = "";
		descriptionSS = "";
		testsuiteTParse = [];
		milestoneTParse = [];
	}
	drawTaskFilter(trackerParse, assigneeParse, statusParse, priorityParse,categoryParse, planStartSS, planEndSS, testsuiteTParse, milestoneTParse, descriptionSS);
</script>
<jsp:include page="/WEB-INF/views/users/modal_Update_Stt_Manual_Testcase_Detail.jsp"></jsp:include>
<script src="${context}/assets/js/management/manualTestcase_detail.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_Manual_Testcase_Detail.jsp"></jsp:include>
<script src="${context}/assets/js/management/testcase_detail.js"></script>
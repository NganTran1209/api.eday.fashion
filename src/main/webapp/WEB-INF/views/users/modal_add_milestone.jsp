<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
var projectId="${projectId}";
</script>
<div id="modalAddMilestone" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0"></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormMilestone();">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="createprojectMilestone" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <input id="editmile" type="hidden" name="editmile">
				    <div class="item form-group bad milestoneNameElm">
				       <label for="milestoneName">
				        	Milestone Name<span class="required">*</span>
				        </label>
				        <div class="input-icon right"><i class="fa milestoneNameValid"></i>
			            	<input class="form-control" placeholder="Please enter" required="required" type="text" name="milestoneName" id="milestoneName">
			        	</div>
				    </div>
				    <div class="form-group">
				         <label for="descriptionMile">
								Description
				        </label>
				       <textarea class="form-control" rows="3" placeholder="Please enter" name="descriptionMile" id="descriptionMile"></textarea>
				    </div>
				    <div class="form-group status-show" hidden>
				         <label for="statusMile">
								Status
				        </label>
				       <select class="form-control" name="statusMile" id="statusMile" >
				       		<option value="Open">Open</option>
				       		<option value="Close">Close</option>
				       		<option value="Pending">Pending</option>
				       </select>
				    </div>
				    <div class="form-group">
				       <label for="milestoneName">
				        	Start Date<span class="required">*</span>
				        </label>
				        <input type="text" id="createDate" class="form-control" placeholder="Date" value="" name="createDate">
				    </div>
				    <div class="form-group">
				       <label for="milestoneName">
				        	End Date<span class="required">*</span>
				        </label>
				        <input type="text" id="endDate" class="form-control" placeholder="Date" value="" name="endDate">
				    </div>
				    <div class="form-group justify-content-center">
				        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" onclick="sendMileStone();" id="saveMilestone">
				        	<i class="ti-check"> </i> Save
			        	</button>
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="resetFormMilestone();" data-dismiss="modal" >
				        	<i class="ti-close"> </i> Close
				        </button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>



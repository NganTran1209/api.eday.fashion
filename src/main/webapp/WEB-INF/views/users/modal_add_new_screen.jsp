<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddNewScreen" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Add New Screen</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_screen" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <div class="row item form-group bad">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4" for="name">
				        	Screen Name<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				            <input class="form-control" placeholder="" required="required" type="text" name="screenName" id="screen_name">
				        </div>
				    </div>
				    <div class="row item form-group">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4" for="name">
				        	Description<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				            <textarea class="form-control" placeholder="" required="required" rows="3" name="screenDescription" id="screen_description"></textarea>
				        </div>
				    </div>
				    <div class="row form-group justify-content-center">
				        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" onclick = "addNewScreen();"><i class="ti-check"></i> Save</button>
				        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" data-dismiss="modal"><i class="ti-close"></i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
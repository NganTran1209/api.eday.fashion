<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="w-100">
    <div class="card card-box card-topline-green" id="">
        <div class="card-head card-head-icon">
            <header><i class="icon-people"></i><spring:message code="label.InvitedToJoinProject"></spring:message></header>
            <div class="tools">
                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"
                    onclick="collsapseCardBody(this)"></a>
            </div>
        </div>
        <div class="card-body " style="">
            <div id="">
                <ul class="docListWindow small-slimscroll-style list-unstyled d-block"
                    style="width: auto;height: auto;" id="listInvitedToJoin">
                   
                </ul>
            </div>
        </div>
    </div>
</div>
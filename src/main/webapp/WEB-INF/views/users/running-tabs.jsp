<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>
<div class="card card-box card-topline-green changeAddIssue" id="runningBox">
	
    <div class="card-body">
    	<div class="col-md-12 form-group">
			<div class="row">
				<div class="col-md-2 form-group ml-auto">
					<select class="form-control" multiple="multiple" id="filterTestsuiteRunning">
					</select>
				</div>
				<div class="col-md-2 form-group">
					<select class="form-control" multiple="multiple" id="filterExecuteBy">
					</select>
				</div>
				<div class="col-md-2 form-group">
					<select class="form-control" multiple="multiple" id="filterMileStone">
					</select>
				</div>
				<div class="col-md-2 form-group">
					<select class="form-control" id="filterStatus">
						<option value=""></option>
						<option value="PASS"><spring:message code="label.PASS"></spring:message></option>
						<option value="FAILED"><spring:message code="label.FAILED"></spring:message></option>
						<option value="DEGRADE"><spring:message code="label.DEGRADE"></spring:message></option>
					</select>
				</div>
				<button id="filRunning" class="btnPlay mdl-button mdl-js-button mdl-js-ripple-effect btn-success" onclick="filterRunning();">
					<i class="ti-search text-white"> </i> Search
				</button>
			</div>
		</div>
		<div class="w-100 position-relative">
			<div class="preloader progress-loadMainContent" id="loadlistScheduler" style="display: none;">
			    <div class="loader" style="top:30%">
			        <div class="loader__figure"></div>
			        <p class="loader__label text-center">Please waiting!<br> Bottest.io</p>
			    </div>
			</div>
		</div>
		<div class="w-100" id="box-schedule-init">
			<div id="listRunningJobSchedule"></div>
			<div class="w-100 text-center text-danger" id="alert-empty-result"></div>
		</div>
	</div>
</div> 
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
<script src="${context}/assets/js/management/scheduler.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#calRunningTabs").addClass("active show");
	var listRunning = ${listRunning};
	var projectId = ${projectId};
	var testsuiteSession = '${testsuiteSession}';
	var statusSession = "${statusSession}";
	var executeBySession = '${executeBySession}';
	var milestoneSS = '${milestoneSS}';
	var jobScheduleId = '${jobScheduleId}';
	var checkSession = true;
	var listBug=${listBug};
 	var taskList = ${taskList};
	
	var testsuiteParse = "";
	if(testsuiteSession != ""){
		testsuiteParse = JSON.parse(testsuiteSession);
	}
	
	var executeByParse = "";
	if(executeBySession != ""){
		executeByParse = JSON.parse(executeBySession);
	}
	
	var milestoneParse = "";
	if(milestoneSS != ""){
		milestoneParse = JSON.parse(milestoneSS);
	}
	
	getListTestsuite("true", function(testsuiteLs){
		
		$("#filterTestsuiteRunning").select2({
			placeholder: getLang(currentLocale,"Select Testsuite"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: testsuiteLs,
			dropdownParent: ""
		});
		$("#filterTestsuiteRunning").select2({placeholder: getLang(currentLocale,"Select Testsuite"), theme: 'bootstrap'});
		$("#filterTestsuiteRunning").val(testsuiteParse);
		$("#filterTestsuiteRunning").trigger('change');
		
		$("#filterStatus").select2({
			placeholder: getLang(currentLocale,"Select status"),
			width:"100%",	
			minimumResultsForSearch: Infinity,
			allowClear: true,
			dropdownParent: ""
		});
		$("#filterStatus").select2({placeholder: getLang(currentLocale,"Select status"),allowClear: true ,theme: 'bootstrap'});
		$("#filterStatus").val(statusSession);
		$("#filterStatus").trigger('change')
	});
	
	getExecuteBy(function(listExecuteBy){
		$("#filterExecuteBy").select2({
			placeholder: getLang(currentLocale,"Select Execute By"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: listExecuteBy,
			dropdownParent: ""
		});
		$("#filterExecuteBy").select2({placeholder: getLang(currentLocale,"Select Execute By"), theme: 'bootstrap'});
		$("#filterExecuteBy").val(executeByParse);
		$("#filterExecuteBy").trigger('change');
	});
	
	getAllMilestoneSchedule("true", function(listMilestone){
		$("#filterMileStone").select2({
			placeholder: getLang(currentLocale,"Select Milestone"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: listMilestone,
			dropdownParent: ""
		});
		$("#filterMileStone").select2({placeholder: getLang(currentLocale,"Select Milestone"), theme: 'bootstrap'});
		$("#filterMileStone").val(milestoneParse);
		$("#filterMileStone").trigger('change');
	});
	
	if((testsuiteSession != "" && testsuiteSession != "[]") || statusSession != "" || (executeBySession != "" && executeBySession != "[]") || (milestoneSS != "" && milestoneSS != "[]")){
		drawRunningByFilter(testsuiteParse, statusSession, executeByParse, milestoneParse);
	} else {
		if(listRunning.length > 0){
			drawListScheduler(listRunning, "#listRunningJobSchedule"); 
		}else{
			$("#alert-empty-result").html(getLang(currentLocale,"The Result is empty!!!"));
		}
	}
	
	if (jobScheduleId != "") {
		var y = $(window).scrollTop();
		var elementTop = $('div[job-scheduler-id="1665"]').offset().top
		window.scroll(0, elementTop - y);
	}
	
	
	
</script>
<jsp:include page="/WEB-INF/views/users/modal_copy_link.jsp"></jsp:include>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalUpdateSttDetailManualTestcase" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg" style="max-width:70%" >
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Add New Last Result</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body position-relative">
				<div class="preloader loading-important position-absolute" id="loaderModalManualTCS" style="display: none;">
				   <div class="loader" style="top:30%">
				        <div class="loader__figure"></div>
				        <p class="loader__label text-center">Please waiting!<br> Bottest.io</p>
				    </div>
				</div>
				<div class="borderBox shadow-none border-0 light card-box py-0 px-2 mb-3 tabsBorderCustom " style="border-radius:10px;overflow:hidden;height: 50px;margin-bottom: 10px;">
					<div class="borderBox-title tabbable-line mb-0" style="min-height: 50px">
					     <ul class="nav nav-tabs float-left">
					     	 <li class="nav-item">
					             <a href="#detailResultManul" id="resultTab" data-toggle="tab" class="text-uppercase border-success active show" > Result Detail </a>
					         </li>
					         <li class="nav-item">
					             <a href="#uploadImgResult" data-toggle="tab" class="text-uppercase border-success " > Files </a>
					         </li>     
					     </ul>
					</div> 
				</div>
				<form class="col-md-12 form-horizontal form-label-left form-addNew-common" id="formAddLastResultManual" action="javascript:void(0)" accept-charset="UTF-8" method="post" novalidate="novalidate">
					<div class="tab-content">
						<div class="tab-pane active show" id="detailResultManul">
							<div class="row item form-group des">
								<div class="col-md-4">
									<div class="w-100 d-flex border-bottom" id="manualIdResult" name="manualIdResult">
										<label class="control-label font-weight-bold">ID:</label>
										<label class="control-label ml-1 font-weight-bold" id="idResult" name="idResult"></label>
									</div>
									<div class="w-100 input-group">
										<label class="w-100">Milestone</label>
										<select class="form-control" id="mileStoneId" name="mileStoneId">
										    <option value="">Please choose</option>
										</select>
										<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success button-sm-custom ml-2" id="addNewMilestone" onclick="createMileStone();" style="height:34px;padding: 0 9px;">
											<i class="ti-plus"></i>
										</button>
									</div>
									<div class="w-100">
										<label class="w-100">OS</label>
										<select class="form-control" id="manualOS" name="manualOS">
										   <c:forEach items="${lsOs}" var="item">
											    <c:choose>
													<c:when test="${item.defaultC eq  true }">
												    	<option value="${item.itemValue}" selected>${item.itemName}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.itemValue}">${item.itemName}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div class="w-100">
										<label class="w-100">Browser</label>
										<select class="form-control" id="manualBrowser" name="manualBrowser">
										    <c:forEach items="${lsBrowser}" var="item">
											    <c:choose>
													<c:when test="${item.defaultC eq  true }">
												    	<option value="${item.itemValue}" selected>${item.itemName}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.itemValue}">${item.itemName}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div class="w-100">
										<label class="w-100">Screen Resolution</label>
										<select class="form-control" id="manualResolution" name="manualResolution">
										    <c:forEach items="${lsResolution}" var="item">
											    <c:choose>
													<c:when test="${item.defaultC eq  true }">
												    	<option value="${item.itemValue}" selected>${item.itemName}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.itemValue}">${item.itemName}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div class="w-100">
										<label class="w-100">Location</label>
										<select class="form-control" id="manualLocation" name="manualLocation">
										    <c:forEach items="${lsLocation}" var="item">
											    <c:choose>
													<c:when test="${item.defaultC eq  true }">
												    	<option value="${item.itemValue}" selected>${item.itemName}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.itemValue}">${item.itemName}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div class="w-100">
										<div class="checkbox checkbox-green form-check form-check-inline mt-3 checkbox-fix-top  pl-2">
											<input type="checkbox" name="mobileMode" id="mobileModeResult" name="mobileModeResult">
											<label for="mobileModeResult" class="mt-0">Mobile Mode</label>
										</div>
									</div>
									<div class="w-100">	
										<label class="w-100">Device Name</label>
										<select class="form-control" id="manualDeviceName" name="manualDeviceName">
										   <c:forEach items="${lsDevice}" var="item">
											    <c:choose>
													<c:when test="${item.defaultC eq  true }">
												    	<option value="${item.itemValue}" selected>${item.itemName}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.itemValue}">${item.itemName}</option>
													</c:otherwise>
												</c:choose>
										  </c:forEach>
										</select>
									</div>
									<div class="w-100">
										<label class="w-100">Status Running</label>
										<select class="form-control" id="manualStatus" name="manualStatus">
										    <option value="PASS">PASS</option>
										    <option value="FAILED">FAILED</option>
										    <option value="DEGRADE">DEGRADE</option>
										</select>
									</div>
									<div class="w-100">
										<label class="w-100">Run User</label>
										<select class="form-control" id="manualRunningUser" name="manualRunningUser">
										    
										</select>
									</div>
								</div>
								<div class="col-md-8 elementValidate">
									<div class="w-100 form-group mb-2 mt-1 bad">
										<label class="w-100">Run Time</label>
										<input type="text" id="datetimeLastRun" name="datetimeLastRun" class="form-control" placeholder="Date" value="">
									</div>	
									<div class="w-100 form-group mb-2 bad">
											<label class="w-100">Run Description</label>
											<textarea rows="3" class="form-control" placeholder="Please enter" style="height: 100px;" id="manualDescription" name="manualDescription"></textarea>
									</div>
									<div class="w-100 item form-group mb-0 bad">
										<label class="w-100">Expected Output<span class="required">*</span></label>
										<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
											<textarea rows="8" class="form-control" placeholder="Please enter" id="manualExpectedOutput" name="manualExpectedOutput"></textarea>
										</div>
									</div>
									<div class="w-100 item form-group mb-0 bad">
										<label class="w-100">Actual Output<span class="required">*</span></label>
										<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
											<textarea rows="7" class="form-control" placeholder="Please enter" id="manualActualOutput" name="manualActualOutput"></textarea>
										</div>
									</div>
								</div>
							</div>
							<input id="testcaseNameS" name="testcaseNameS" hidden value="">
							<input id="testsuiteId" name="testsuiteId" hidden>
							<div class="row form-group justify-content-center">
						    	<button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" id="addNewResultManualTcs" onclick="saveResultManuals()"><i class="ti-check mr-2"></i> Save</button>
						        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" id="updateNewResultManualTcs" style="display:none" onclick="editResultManuals()"><i class="ti-check mr-2"></i> Update</button>
						        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2" id="newIssueTcManual" style="display:none" onclick="addIssue(this);"><i class="ti-plus mr-2"></i><spring:message code="label.LogBug"></spring:message></button>
						        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default" data-dismiss="modal" id="closeModalResultManuals"><i class="ti-close mr-2"></i> Close</button>
						    </div>
						</div>
						<div class="tab-pane" id="uploadImgResult">
							<div class="row">
								<div class="ml-2 upload-btn-wrapper">
								  	<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success button-sm" type="button" id="uploadImages">Upload File</button>
								  	<input id="uploadFiles" type="file" name="myfile" multiple="true">
								</div>
								<div class="col-md-12 mx-3px">
									<div class="row position-relative">
										<div class="preloader progress-loadMainContent" id="loadUploadImage">
										    <div class="loader" style="top:30%">
										        <div class="loader__figure"></div>
										        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
										    </div>
										</div>
									</div>
									<div class="row mt-3 img-result mb-3" id="loadFiles">
										 
									</div>
					        	 	
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>

<div id="modalImg" class="slideshow-container w-100 h-100" style="z-index: 99999999;">
					                	
					                	
</div>

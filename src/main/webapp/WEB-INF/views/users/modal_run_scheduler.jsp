<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
var projectId="${projectId}";
</script>
<div class="modal fade bs-example-modal-sm" id="modalRunScheduler" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
 	<div class="modal-dialog modal-lg" style="min-width:64%">
 	<form name="runTestCasesScheduler" id="runTestCasesScheduler">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Run Scheduler</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body" id="modalBody">
				<input id="jobNameScheduler" value="" hidden>
				<input id="statusScheduler" value="" hidden>
				<input id="queueIdScheduler" value="" hidden>
				<input id="buildNumberScheduler" value="" hidden>
				<input id="idJobScheduler" value="" hidden>
				<input id="jsonDataScheduler" value="" hidden>
				<input id="startDateScheduler" value="" hidden>
				<input id="createDateScheduler" value="" hidden>
				<input id="updateDateScheduler" value="" hidden>
			    
				<div class="row mb-2">
					<div class="col-md-2 col-xs-12">
						Project Name
					</div>
					<div class="col-md-4 col-xs-12 ">
						<p id="projectIdInModalScheduler">${currentProject.getName()}</p>
					</div>
					<div class="col-md-2 col-xs-12">
						Testsuite Name
					</div>
					<div class="col-md-4 col-xs-12 ">
						<p id="testsuiteNameInModalScheduler" style="display:none"></p>
						
						<select id="testsuiteRunScheduler" name ="testsuiteRunScheduler" class="form-control" tabindex="1">
						</select>
					</div>
				</div>
				<div class="row item mb-2 bad">
					<div class="col-md-2 col-xs-12">
						<h5>Milestone Name<span class="text-danger">*</span></h5>
					</div>
					<div class="col-md-10 col-xs-12">
						<div class="input-icon right"><i class="fa excuteName"></i>
							<select id="executeNameScheduler" class="form-control" name="executeName">
							</select>
						</div>
					</div>
				</div>
				<input id="messageError" hidden value="" style="color:red">
			    
				<div class="row">
					<div class="col-md-2 col-xs-12">
						<h5>OS</h5>
					</div>
					<div class="col-md-4 col-xs-12 mb-2">
						<select id="slOsScheduler" class="form-control" name="runningOS">
							<c:forEach items="${lsOs}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-md-2 col-xs-12 mb-2">
						<h5>Run Location</h5>
					</div>
					<div class="col-md-4 col-xs-12">
						<select id="sladdressScheduler" class="form-control" name="runningLocation">
							<c:forEach items="${lsLocation}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12 mb-2">
						<h5>Browser</h5>
					</div>
					<div class="col-md-4 col-xs-12 mb-2">
						<select id="slbrowserScheduler" class="form-control" name = "browser">
							<c:forEach items="${lsBrowser}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-md-2 col-xs-12 mb-2 text-nowrap">
						<h5>Screen Resolution</h5>
					</div>
					<div class="col-md-4 col-xs-12 mb-2">
						<select id="slscreenScheduler" class="form-control" name="screenRes">
							<c:forEach items="${lsResolution}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12 mb-2">
						<h5>Timeout(ms)</h5>
					</div>
					<div class="col-md-4 col-xs-12">
						<input class="form-control allownumericwithoutdecimal" type="text" id="txtTimeoutScheduler" name="timeout" value="300">
					</div>
					<div class="col-md-2 col-xs-12">
						<h5>Select Devices</h5>
					</div>
					<div class="col-md-4 col-xs-12 ">
						<select id="devicesSelectionScheduler" name ="deviceName"
							class="form-control" tabindex="1">
							<c:forEach items="${lsDevice}" var="item">
								    <c:choose>
										<c:when test="${item.defaultC eq  true }">
									    	<option value="${item.itemValue}" selected>${item.itemName}</option>
										</c:when>
										<c:otherwise>
											<option value="${item.itemValue}">${item.itemName}</option>
										</c:otherwise>
									</c:choose>
							</c:forEach>
						</select>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12 mb-2">
						<h5>Mobile Mode</h5>
					</div>
					<div class="col-md-4 col-xs-12 mb-2">
						<div class="checkbox checkbox-green form-check form-check-inline mr-2 pl-2">
							<input type="checkbox" name="mobileMode" id="mobileModeScheduler">
							<label for="mobileModeScheduler"></label>
						</div>
					</div>
					<div class="col-md-2 col-xs-12 mb-2">
						<h5>Record Video</h5>
					</div>
					<div class="col-md-4 col-xs-12 mb-2">
						<div class="checkbox checkbox-green form-check form-check-innline mr-2 pl-2">
							<input type="checkbox" name="isRecordVideo" id="videoModeScheduler">
							<label for="videoModeScheduler"></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="row mb-2" id="testcasesScheduler">
							<div class="col-md-4 col-xs-12">
								Select Testcases
							</div>
							<div class="col-md-8 col-xs-8 fix-top-checkbox-forminline">
								<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0 border border-bottom-0 py-2">
									<input type="checkbox" name="runAsSchedulerAll" id="runAsSchedulerAll">
									<label for="runAsSchedulerAll" class="mb-0">Select all</label>
								</div>	
								<div id="lsTestcasesScheduler" class="selectTestcaseCheckbox" value="">
														
								</div>
								<div class="btn-group" style="width: 100%;display:none">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row item mb-2 bad">
							<div class="col-md-4 col-xs-4">
								<h5>Run Description</h5> 
							</div>
							<div class="col-md-8 col-xs-8">
								<div class="input-icon right"><i class="fa excuteName"></i>
									<textarea class="form-control" rows="7" id="runDescriptionScheduler" name="runDescription" placeholder="Description of software version or more information"></textarea>
								</div>
							</div>
						</div>				
					</div> 
				</div>
				<div class="row item mb-2 bad" id="box-runScheduler">
					<div class="col-md-2 col-xs-2">
						<div class="w-100 mt-1" style="white-space: nowrap;">Run as schedule</div>
					</div>
					<div class="col-md-10 col-xs-10"> 
						<div id="drawCronTab">
							<div id="corntab1" class="cronTab mb-3"></div> 
						</div>
						<div class="example-text" id="example1b-val"></div>
<!-- 						<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm" id="addTimeScheduler" style="display: none;min-width: 40px">
	         			 <i class="ti-plus"></i>
	       			 </button> -->
					</div>
				</div>
				
				
			</div>
			<div class="modal-footer" id="modalFooter">
				
				<button type="button" id="btnRunScheduler" onclick="saveSheduler();" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2">
					<i class="ti-check"> </i>Save
				</button>
				<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default"
					data-dismiss="modal" id="btnExit">
					<i class="ti-close"> </i> Close
				</button>
			</div>
		</div>
		</form>
	</div>
</div>
<script>
	
	$( document ).ready(function() {
		hideModalRunScheduler();
	});
	
	$("#runAsScheduler").click(function(){
		if($("#runAsScheduler").prop("checked") == true){
			$("#selectTimeRun").show();
		}else{
			$("#selectTimeRun").hide();
		}
		
	});
	$(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
	    $(this).val($(this).val().replace(/[^\d].+/, ""));
	    if ((event.which < 48 || event.which > 57)) {
	         event.preventDefault();
	     }
	 });
	
</script>


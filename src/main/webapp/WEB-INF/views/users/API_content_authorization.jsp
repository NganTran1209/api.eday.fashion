<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="col-md-12">
	<div class="row">
		<div class="col-md-4 py-3 form-type-request">
			<div class="w-100 form-group">
				<small class="control-label text-muted text-uppercase font-weight-bold">TYPE</small>
				<select class="form-control" id="selectTypeAuth" name="Type">
					<option value="none">None</option>
					<option value="basicAuth">Basic Auth</option>
					<option value="apiKey">API Key</option>
					<option value="bearerToken">Bearer Token</option>
					<option value="oauth2" style="display: none;">OAuth 2.0</option>
					<option value="awsSignature">AWS Singature</option>
				</select>
			</div>
			<small class="w-100 text-muted description-auth-type form-group">The authorization header will be automatically generated when you send the request.</small>
			<div class="w-100 form-group" id="elementRequest" style="display:none">
				<small class="control-label">Add authorization data to</small>
				<select class="form-control" name="authorizationData">
					<option value="basicAuth">Request URl</option>
					<option value="apiKey">Request Header</option>
				</select>
			</div>
			<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-warning button-sm btn-preview-request">Preview Request</button>
		</div>
		<div class="col-md-8 d-flex py-3">
			<div class="my-auto w-100 form-element-bg" id="form-basic-auth" style="display:none" >
				<div class="form-group row">
				    <label for="api_auth_basic_username" class="col-md-3 col-form-label">Username</label>
				    <div class="col-md-9">
				      <input type="text" class="form-control" id="api_auth_basic_username" placeholder="Username" name="Key" >
				    </div>
				</div>
			  	<div class="row">
				    <label for="api_auth_basic_username" class="col-md-3 col-form-label">Password</label>
				    <div class="col-md-9">
				      <input type="password" class="form-control" id="api_auth_basic_password" placeholder="Password" name="Value">
				    </div>
			  	</div>
		  	</div>
		  	<div class="my-auto w-100 form-element-bg" id="form-api-key" style="display:none">
				<div class="form-group row">
				    <label for="api_auth_apiKey_Key" class="col-md-3 col-form-label">Key</label>
				    <div class="col-md-9">
				      <input type="text" class="form-control" id="api_auth_apiKey_Key" placeholder="Key" name="Key">
				    </div>
				</div>
			  	<div class="form-group row">
				    <label for="api_auth_apiKey_Value" class="col-md-3 col-form-label">Value</label>
				    <div class="col-md-9">
				      <input type="password" class="form-control" id="api_auth_apiKey_Value" placeholder="Value" name="Value">
				    </div>
			  	</div>
			  	<div class="row">
				    <label for="selectAddTo" class="col-md-3 col-form-label">Add to</label>
				    <div class="col-md-9">
				    	<select class="form-control" name="AddTo" id="api_auth_apiKey_AddTo">
							<option value="header">Header</option>
							<option value="queryParams">Query Params</option>
						</select>
				    </div>
			  	</div>
		  	</div>
		  	<div class="my-auto w-100 form-element-bg" id="form-bearer-token" style="display:none">
				<div class="form-group row">
				    <label for="api_auth_bearerToken_Token" class="col-md-3 col-form-label">Token</label>
				    <div class="col-md-9">
				      <input type="text" class="form-control" id="api_auth_bearerToken_Token" placeholder="Token" name="Key">
				    </div>
				</div>
		  	</div>
		  	<div class="my-auto w-100 form-element-bg" id="form-oauth2" style="display:none">
				<div class="form-group row">
				    <label for="oauth2AccessToken" class="col-md-3 col-form-label">Access Token</label>
				    <div class="col-md-6">
				      <input type="text" class="form-control" id="api_auth_oauth2_AccessToken" placeholder="Access Token">
				    </div>
				    <!-- <div class="col-md-3">
				    	<select class="form-control">
				    		<option value="header">Available Tokens</option>
							<option value="">1</option>
							<option value="">2</option>
						</select> 
				    </div>-->
				</div>
				<!-- <div class="form-group row">
					<div class="col-md-9 offset-md-3">
						<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-warning button-sm">Get New Access Token</button>
					</div>
				</div> -->
		  	</div>
		  	<div class="my-auto w-100 form-element-bg" id="form-aws" style="display:none">
				<div class="form-group row">
				    <label for="api_auth_s3_AccessKey" class="col-md-3 col-form-label">AccessKey</label>
				    <div class="col-md-9">
				      <input type="text" class="form-control" id="api_auth_s3_AccessKey" placeholder="AccessKey Key">
				    </div>
				</div>
			  	<div class="form-group row">
				    <label for="api_auth_s3_SecretKey" class="col-md-3 col-form-label">SecretKey</label>
				    <div class="col-md-9">
				      <input type="text" class="form-control" id="api_auth_s3_SecretKey" placeholder="Secret Key">
				    </div>
			  	</div>
			  	<div class="w-100 mt-3">
			  		<a class="awsAdvanced-link collapsed" data-toggle="collapse" href="#awsAdvanced" role="button" aria-expanded="false" aria-controls="awsAdvanced">
			  			ADVANCED
		  			</a>
			  	</div>
			  	<div class="collapse" id="awsAdvanced">
			  		<small class="w-100 text-muted">These are advanced configuration options. They are optional. Postman will auto generate values for some fields if left blank.</small>
			  		<div class="form-group row">
					    <label for="api_auth_s3_AWSRegion" class="col-md-3 col-form-label">AWS Region</label>
					    <div class="col-md-9">
					      <input type="text" class="form-control" id="awsregion" name="api_auth_s3_AWSRegion" placeholder="e.g. us-east-1">
					    </div>
					</div>
				  	<div class="form-group row">
					    <label for="api_auth_s3_ServiceName" class="col-md-3 col-form-label">Service Name</label>
					    <div class="col-md-9">
					      <input type="text" class="form-control" id="api_auth_s3_ServiceName" name="serviceName" placeholder="e.g. s3 ">
					    </div>
				  	</div>
				  	<div class="form-group row">
					    <label for="api_auth_s3_SessionToken" class="col-md-3 col-form-label">Session Token</label>
					    <div class="col-md-9">
					      <input type="text" class="form-control" id="api_auth_s3_SessionToken" name="sessionToken" placeholder="Session Token">
					    </div>
				  	</div>
			  	</div>
		  	</div>
		</div>
	</div>
</div>
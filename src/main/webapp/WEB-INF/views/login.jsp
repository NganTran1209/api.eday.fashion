<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="body-container" id="body-container-form">
	<div class="d-flex w-100 justify-content-center flex-row h-100 my-auto">
		<div class="bg-dote-animation">
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"><i class="fa fa-bug"></i></div>
			<div class="particle"></div>
		</div>
    	<div class="d-flex w-100 justify-content-center flex-row h-100 my-auto">
	        <div class="body-screen col-md-8 d-flex">
				<div class="w-100 bg-white my-auto" style="min-height: auto">
					<div class="row m-0">
						<div class="text-center col-md-6 px-0">
							<div class="col-backdrop">
								<div class="logo-form">
									<img alt="" src="${context}/assets/img/logo.png" width="250" height="auto" class="mx-auto">
								</div>
								<h1 class="text-uppercase text-light font-weight-bold title-backdrop">
									Automation Test & RPA Platform
								</h1>
								<div class="text-center text-uppercase text-light">Manage Your Application Quality Automatically</div>
							</div>
						</div>
						<div class="w-100 col-md-6 d-flex flex-column justify-content-center py-5">
							<div class="w-100" id="loginForm" style="min-height: auto">
								<div class="d-flex justify-content-center position-relative h-100" >
									<div class="col-md-10">
										<h2 class="text-center text-success">Login Account!</h2>
										<form class="form-horizontal" name="login" action="javascript:void(0)" accept-charset="UTF-8" method="post" id="loginFormSubmit">
											<div class="modal-body"> 
												<div class="row item form-group bad">
												    <label for="username" class="col-md-12 control-label text-left">Email</label>
											    	<div class="col-md-12">
											    		<div class="input-icon right"> 
												    		<i class="fa"></i>
												      		<input type="email" name="username" id="username" placeholder="Enter email" class="form-control" required="required" title="" alt="">
												      	</div>
												    </div> 
											  	</div>  
											  	<div class="row item form-group bad">
												    <label for="password" class="col-md-12 control-label text-left">Password</label>
											    	<div class="col-md-12">
											    		<div class="input-icon right">
												    		<i class="fa"></i>
													      	<input type="password" name="password" id="password" placeholder="Enter password" class="form-control" required="required" title="" alt="">
												      	</div>
												    </div>
											  	</div>
											  	<div class="form-group row">
											    	<div class="col-md-12 text-right">
												      	<a href="#" class="text-muted" id="forgotPass">Forgot Password?</a>
												    </div>
											  	</div>
											</div>
											<div class="modal-footer text-center justify-content-center">
										        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn btn-success" onclick="validateLogin();"  id="btnLoginAcc">Login</button>
										       <button onclick="window.location.href='${context}';" type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn btn-default">Back</button>
									      	</div>
								      	</form>
							      	</div>
								</div>
								<div class="text-center text-muted text-sigup w-100">
									Don't have an account? <a class="ml-1" href="${context}/signup">Sign Up</a>
								</div>
							</div>
							<div class="w-100 sendMailForm py-5" style="min-height: auto;display:none">
								<div class="bg-white col-md-10 m-auto h-100" > 
									<h1 class="text-center text-success">Forgot Password</h1>
									<form class="form-horizontal" name="findYourEmail" action="javascript:void(0)" accept-charset="UTF-8" method="post" id="findYourEmail">
										<div class="modal-body"> 
											<div class="row item form-group bad">
											    <label for="username" class="col-md-12 control-label text-left">Email</label>
										    	<div class="col-md-12">
										    		<div class="input-icon right">  
											    		<i class="fa"></i>
											      		<input type="email" name="findEmail" id="findEmail" placeholder="Enter email" class="form-control" required="required" title="" >
											      	</div>
											    </div> 
										  	</div>  
										</div>
										<div class="modal-footer text-center justify-content-center">
									        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect btn btn-success" onclick="validateSendEmail()" id="btnSendMailFPass">Send Mail</button>
									        <button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn btn-default" id="backLogin">Close</button>
								      	</div>
							      	</form>
							    </div>
						   </div>
				      	</div>
			      	</div>
			    </div>
			</div>
	    </div>
	</div>
</div>
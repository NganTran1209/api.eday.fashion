<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<footer>
	<div class="w-100" id="contactus">
	    <div class="footer-container w-100"> 
	    	<div class="container section-container contact-us">
		    	<h1 class="text-center nomargin home_sub_heading">Contact us</h1>
		        <div class="line-bottom"></div>
		    </div>
	        <div class="info-footer col-md-12 form-group">
	            <div class="container">
	                <div class="row">
	                    <div class="col-md-4 info-footer-content">
	                        <p class="footer-nav-title caps-heading-12">QUICK LINKS<i class="fa fa-angle-down footer-drop" aria-hidden="true"></i></p>
	                        <ul class="footer-nav show navigation">
	                            <li>
	                                <a href="#">Support</a>
	                            </li>
	                            <li class="nav-item">
	                                <a href="#features">Features</a>
	                            </li>
	                            <li class="nav-item">
	                                <a href="#pricing">Pricing</a>
	                            </li>
	                            <li class="nav-item">
	                                <a href="#contactus">Contact us</a>
	                            </li>
	                        </ul>
	                    <div class=" info-footer-content">
	                        <p class="footer-nav-title caps-heading-12">CONNECT WITH US<i class="fa fa-angle-down footer-drop" aria-hidden="true"></i></p>
	                        <ul class="footer-nav social-connect-nav">
	                            <li>
	                                <a href="https://www.facebook.com/Bottest-915648588775699/" target="_blank">
	                                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="https://www.youtube.com/channel/UCaXwtIP2Do2hmSj-LhKNPxQ" target="_blank">
	                                    <i class="fa fa-youtube-square" aria-hidden="true"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" target="_blank">
	                                    <i class="fa fa-twitter-square" aria-hidden="true"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" target="_blank">
	                                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
	                                </a>
	                            </li>
	                        </ul>
	                        <div>
	                            <a href="mailto:support@bottest.io">
	                                <i class="icon-mail"></i>
	                                <span>support@bottest.io</span>
	                            </a>
	                        </div> 
	                    </div></div>
	                    
	                    <div class="col-md-8">
					        <div class="col-md-10 col-center-block">
					          <form class="form-horizontal" action="" method="post">
						          <fieldset>
						            <p class="footer-nav-title caps-heading-12 text-center w-100">CONNECT WITH US<i class="fa fa-angle-down footer-drop" aria-hidden="true"></i></p>
						    
						            <!-- Name input-->
						            <div class="form-group">
						              <label class="col-md-3 control-label text-left" for="name">Name</label>
						              <div class="col-md-9">
						                <input id="name" name="name" type="text" placeholder="Your name" class="form-control">
						              </div>
						            </div>
						    
						            <!-- Email input-->
						            <div class="form-group">
						              <label class="col-md-3 control-label text-left" for="email">Your E-mail</label>
						              <div class="col-md-9">
						                <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
						              </div>
						            </div>
						    
						            <!-- Message body -->
						            <div class="form-group">
						              <label class="col-md-3 control-label text-left" for="message">Your message</label>
						              <div class="col-md-9">
						                <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
						              </div>
						            </div>
						    
						            <!-- Form actions -->
						            <div class="form-group">
						              <div class="col-md-12 text-right">
						                <button type="submit" class="btn btn-primary btn-lg">Send</button>
						              </div>
						            </div>
						          </fieldset> 
					          </form>
					        </div>
					      </div>
	                </div>    
	            </div>
	        </div>
	    
	        <div class="footer-links l-page clearfix-lg">
	            <div class="row">
	                <div class="col-md-12 seperator-line"></div>
	            </div>
	        </div>
	    </div>
	    <div class="footer-copyrights col-md-12">
	        <div class="l-page clearfix-lg">
	            <div class="copyrights-nav">
	                <a data-toggle="modal" data-target="#modal-terms" style="margin-right: 10px">Terms of Service</a>
	                <a data-toggle="modal" data-target="#PrivacyPolicy" style="margin-right: 10px">Privacy Policy</a>
	            </div>
	            <p class="footer-copyrights-text">Copyright © Paracel Technology solutions  All Rights Reserved</p>
	        </div>
	    </div>
	</div>
</footer>
<div class="modal" tabindex="-1" role="dialog" id="modal-terms">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="imgcontainer">
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      <img alt="apple" width="300px" height="100px" src="https://www.bottest.io/assets/logo-dark-e7813169b51815b137fe462d2f5db42e0fe747d74fa694714a35575e07fbf71c.png" />
	    </div>
        
      </div>
      <div class="modal-body">
        <form class=" action="#">
		    <div class="content-terms">
		        <h3 class="title-term">Privacy Policy</h3>
		        <h4 class="title">General</h4>
		        <p class="content">This website ‘testsigma.com and testsigma.io’ (here in after referred to as “Website” or “Website”), is owned by Testsigma Technologies Private Limited, operating in the name and style of Testsigma. Please read these Terms of Service carefully before accessing or using our website. You can review the most current version of the Terms of Service at any time on this page.<br>
		        Testsigma offers this website, including all information, tools and services available from this Website to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here. By accessing or using any part of the Website, you agree to be bound by this Agreement, Privacy Policy as well as rules, guidelines, policies, terms, and conditions applicable to any service / module that is provided by or through this Website, including the Terms and Policies on the Website relating to standalone services that shall be deemed to be incorporated into this Terms of Service and shall be considered as part and parcel of this Terms of Service (collectively “Terms”). If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. We reserve the right to update, change or replace any part of these Terms by posting updates and/or changes to our website. It is solely your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.
		        </p><h4 class="title">Terms</h4>
		        <p class="content">For the purposes of these Terms, as amended, wherever the context so requires “You” or “User” shall mean any natural or legal person who has agreed to become a user of the services provided by Testsigma, or who browses or accesses this Website. <br>
		        “Agreement” shall mean and refer to this Terms, including any amendments that may be incorporated into it and other Policies available on our Website. <br>
		        “Fees” shall mean consideration payable for Subscription. <br>
		        “Intellectual Property Rights” shall mean all intellectual property rights and all analogous rights subsisting under the laws of each and every jurisdiction throughout the world and intellectual property of the rights concerned including all extensions and renewals of such rights, whether or not such rights are registered or capable of registration, including, without limitation, copyrights, trademarks, trade names, service marks, service names, patents, designs and all other proprietary rights of whatsoever description whether or not protected and whether or not capable of protection.<br>
		        “Malicious Code” shall mean code, files, scripts, or programs intended to do harm, including, for example, viruses, worms, time bombs and Trojan horses. Additionally, any file, script, program, browser plug-in, browser helper or extension, or any robot or application designed to scrape and collect data or automate the entry of data shall also be deemed Malicious Code. <br>
		        “Product / Application” shall mean and refer to the automation testing solutions, and any version of the software being made available through the Website and the portal www.testsigma.io. <br>
		        “Services” shall mean all services being made available by and through the Website, including without limitation, Subscription to the Product. <br>
		        “Subscribe / Subscription” shall mean the access being granted to Use of the Application.<br>
		        “Third Party” shall mean and refer to any natural or legal persons apart from the User and Testsigma. <br>
		        “Use” shall mean directly or indirectly activating the processing capabilities of the Software, to load, execute access, employ the Software, or display information resulting from such capabilities, including but not limited to employing the proprietary information made available to the customer.<br>
		        “We”, “Us”, “Our” shall mean Testsigma Technologies Private Limited.<br>
		        </p>
		        <h4 class="title">Limited License</h4>
		        <p class="content">
		        Subject to Terms, the User is hereby granted a non-exclusive, non-transferable license to use the Application and other proprietary information. Testsigma (“Licensor”) may modify the Terms at any time. Your continued use of the Testsigma Product and/or data derived there from shall constitute your acceptance of such revised Terms. <br>
		        The Licensor may at any time assign, transfer or novate all or any part of the Terms (including its rights and obligations) to any other person without the consent of the licensee.<br>
		        The Licensee acknowledges the proprietary nature of the Application, its architecture, logic, data sources and processes and shall do everything within its control to protect its confidentiality.<br>
		        Licensee shall not have the right to change, alter, copy, amend, reverse engineer, decompile, disassemble, publish, disclose, display or make available, in whole or in part, or otherwise use the Software in any manner whatsoever, except as specified herein.<br>
		        Testsigma, its affiliates, and licensors shall retain all right, title, know-how, and other proprietary and intellectual property rights in the Application. The Licensee does not acquire any rights in the software, except as specified and granted in this Agreement, and shall not remove any proprietary, copyright, patent, trademark, design right, trade secret or any other proprietary rights legends from the software. Licensee shall not modify, resell, distribute, or create derivative works based on the software or any part thereof. <br>
		        </p>
		        <h4 class="title">Reservation of Rights</h4>
		        <p class="content">
		        No other rights are granted except as expressly set forth in the Terms. Subscription shall not amount to sale and does not convey any rights or ownership in, or to, the Services or any underlying software. We own all right, title, and interest, including all intellectual property rights, in and to the Services and the underlying software and any and all updates, upgrades, modifications, enhancements, content, improvements or derivative works thereof, and in any idea, know-how, and programs developed by Us or Our licensors during the course of performance of the Services.
		        </p>
        	</div>
    	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="PrivacyPolicy">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="imgcontainer">
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      <img alt="apple" width="300px" height="100px" src="https://www.bottest.io/assets/logo-dark-e7813169b51815b137fe462d2f5db42e0fe747d74fa694714a35575e07fbf71c.png" />
	    </div>
        
      </div>
      <div class="modal-body">
        <form class="" action="#">
		
		    <div class="content-terms">
		        <h3 class="title-term">Privacy Policy</h3>
		        <h4 class="title">General</h4>
		        <p class="content">This website ‘testsigma.com and testsigma.io’ (here in after referred to as “Website” or “Website”), is owned by Testsigma Technologies Private Limited, operating in the name and style of Testsigma. Please read these Terms of Service carefully before accessing or using our website. You can review the most current version of the Terms of Service at any time on this page.<br>
		        Testsigma offers this website, including all information, tools and services available from this Website to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here. By accessing or using any part of the Website, you agree to be bound by this Agreement, Privacy Policy as well as rules, guidelines, policies, terms, and conditions applicable to any service / module that is provided by or through this Website, including the Terms and Policies on the Website relating to standalone services that shall be deemed to be incorporated into this Terms of Service and shall be considered as part and parcel of this Terms of Service (collectively “Terms”). If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. We reserve the right to update, change or replace any part of these Terms by posting updates and/or changes to our website. It is solely your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.
		        </p><h4 class="title">Terms</h4>
		        <p class="content">For the purposes of these Terms, as amended, wherever the context so requires “You” or “User” shall mean any natural or legal person who has agreed to become a user of the services provided by Testsigma, or who browses or accesses this Website. <br>
		        “Agreement” shall mean and refer to this Terms, including any amendments that may be incorporated into it and other Policies available on our Website. <br>
		        “Fees” shall mean consideration payable for Subscription. <br>
		        “Intellectual Property Rights” shall mean all intellectual property rights and all analogous rights subsisting under the laws of each and every jurisdiction throughout the world and intellectual property of the rights concerned including all extensions and renewals of such rights, whether or not such rights are registered or capable of registration, including, without limitation, copyrights, trademarks, trade names, service marks, service names, patents, designs and all other proprietary rights of whatsoever description whether or not protected and whether or not capable of protection.<br>
		        “Malicious Code” shall mean code, files, scripts, or programs intended to do harm, including, for example, viruses, worms, time bombs and Trojan horses. Additionally, any file, script, program, browser plug-in, browser helper or extension, or any robot or application designed to scrape and collect data or automate the entry of data shall also be deemed Malicious Code. <br>
		        “Product / Application” shall mean and refer to the automation testing solutions, and any version of the software being made available through the Website and the portal www.testsigma.io. <br>
		        “Services” shall mean all services being made available by and through the Website, including without limitation, Subscription to the Product. <br>
		        “Subscribe / Subscription” shall mean the access being granted to Use of the Application.<br>
		        “Third Party” shall mean and refer to any natural or legal persons apart from the User and Testsigma. <br>
		        “Use” shall mean directly or indirectly activating the processing capabilities of the Software, to load, execute access, employ the Software, or display information resulting from such capabilities, including but not limited to employing the proprietary information made available to the customer.<br>
		        “We”, “Us”, “Our” shall mean Testsigma Technologies Private Limited.<br>
		        </p>
		        <h4 class="title">Limited License</h4>
		        <p class="content">
		        Subject to Terms, the User is hereby granted a non-exclusive, non-transferable license to use the Application and other proprietary information. Testsigma (“Licensor”) may modify the Terms at any time. Your continued use of the Testsigma Product and/or data derived there from shall constitute your acceptance of such revised Terms. <br>
		        The Licensor may at any time assign, transfer or novate all or any part of the Terms (including its rights and obligations) to any other person without the consent of the licensee.<br>
		        The Licensee acknowledges the proprietary nature of the Application, its architecture, logic, data sources and processes and shall do everything within its control to protect its confidentiality.<br>
		        Licensee shall not have the right to change, alter, copy, amend, reverse engineer, decompile, disassemble, publish, disclose, display or make available, in whole or in part, or otherwise use the Software in any manner whatsoever, except as specified herein.<br>
		        Testsigma, its affiliates, and licensors shall retain all right, title, know-how, and other proprietary and intellectual property rights in the Application. The Licensee does not acquire any rights in the software, except as specified and granted in this Agreement, and shall not remove any proprietary, copyright, patent, trademark, design right, trade secret or any other proprietary rights legends from the software. Licensee shall not modify, resell, distribute, or create derivative works based on the software or any part thereof. <br>
		        </p>
		        <h4 class="title">Reservation of Rights</h4>
		        <p class="content">
		        No other rights are granted except as expressly set forth in the Terms. Subscription shall not amount to sale and does not convey any rights or ownership in, or to, the Services or any underlying software. We own all right, title, and interest, including all intellectual property rights, in and to the Services and the underlying software and any and all updates, upgrades, modifications, enhancements, content, improvements or derivative works thereof, and in any idea, know-how, and programs developed by Us or Our licensors during the course of performance of the Services.
		        </p>
	        </div>
	    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

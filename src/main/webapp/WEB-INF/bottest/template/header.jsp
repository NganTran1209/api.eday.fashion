<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!-- loader -->
<div class="preloader preloader-main" style="display: none;">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting" ></spring:message><br> Bottest.io</p>
    </div>
</div>
<!-- start header -->
 <div class="page-header navbar navbar-fixed-top">
     <div class="page-header-inner ">
         <!-- logo start --> 
         <div class="page-logo">
	         <c:if test="${userDetails.getAuthorities() == '[ROLE_ADMIN]'}">
	         		<a href="${context}/admin">
		             	<img alt="" src="${context}/assets/img/logo.png" class="logo-default">
		             	<img alt="" src="${context}/assets/img/logo-toggle.png" class="logo-responsive-toggler">
	             	</a> 
	         	</c:if>
	         	<c:if test="${userDetails.getAuthorities() == '[ROLE_USER]'}">
	         		<a href="${context}/project">
		             	<img alt="" src="${context}/assets/img/logo.png" class="logo-default">
		             	<img alt="" src="${context}/assets/img/logo-toggle.png" class="logo-responsive-toggler">
	             	</a> 
	         	</c:if>
         </div>   
         <!-- logo end -->
         <div class="top-menu">
                 
             <ul class="nav navbar-nav pull-right flex-row">
                 <!-- start notification dropdown --> 
	         	  <li class="d-flex dropdown dropdown-extended" id="header-content">
	         	   		<a href="${context}/downloadSoftware" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm">
		             	  <i class="ti-download"> </i>   <spring:message code="label.DownloadSoftware"></spring:message>
	             		</a>
                  </li> 
                  <li class="d-flex dropdown dropdown-extended" id="header-content"> 
                  	<button  onclick="syncProject()" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm" id="reloadProject">
	         			 <i class="ti-reload"> </i>  <spring:message code="label.synproject" ></spring:message>
	       			 </button> 
                  </li> 
                 
                 <!-- start running dropdown --> 
				<li class="dropdown dropdown-extended dropdown-inbox my-auto" id="header_inbox_bar_running" style="display:none">
                     <a href="javascript:;" class="dropdown-toggle btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                         <i id="runningPlayIcon" class="fa fa-play fa-spin w-auto"></i>
                         <span class="badge headerBadgeColor2" id="runningLabel1" >2</span>
                     </a>
                     <ul class="dropdown-menu animated slideInDown" style="right: 10px;">
                         <li class="external">
                             <h3><span class="bold"><spring:message code="label.Running" ></spring:message></span></h3> 
                             <span class="notification-label cyan-bgcolor" id="runningLabel2" >2</span>
                         </li>
                         <li>
                             <ul class="list-group list-group-flush dropdown-menu-list small-slimscroll-style runningMess" data-handle-color="#637283">
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>
								  <li><a href="#" class="py-2"><i class="fa fa-caret-right my-auto"></i> Test get javascript variable action</a></li>   
							 </ul>
                             <div class="dropdown-menu-footer">
                             </div>
                         </li>
                     </ul>
                     <ul class="dropdown-menu dropdown-menu-default animated jello alert-bubbles w-auto" id="dropdown-alert-running">
                         <li id="messageNotifications"> 
                         </li>
                     </ul>
                 </li>
				<!-- start manage user dropdown -->
				<li class="dropdown dropdown-user">
                     <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                         <img alt="" class="img-circle " src="${context}/assets/img/dp.jpg" />
                         <span class="username username-hide-on-mobile" hidden>${userDetails.getUsername()}</span>
                          <span class="username-header"></span>
                         <i class="fa fa-angle-down"></i>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default animated jello">
                     	<li>
                             <a href="#" data-toggle="modal" data-target="#modalEditMember">
                                 <i class="icon-info"></i> <spring:message code="label.Profile" ></spring:message>
                             </a>
                         </li>
                         <li>
                             <a href="#">
                                 <i class="icon-directions"></i> <spring:message code="label.Help" ></spring:message>
                             </a>
                         </li>
                         <li>
                             <a href="${context}/logout">
                                 <i class="icon-logout"></i>  <spring:message code="label.LogOut" ></spring:message> </a>
                         </li>
                     </ul>
                     
                 </li>
                
             </ul>
              <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            		 <span></span>
        		 </a>
         </div>
		 <ul class="nav navbar-nav navbar-left flex-row in page-title-breadcrumb mb-0">
				<li>
					<a href="#" class="menu-toggler sidebar-toggler nav-item mr-2"><i class="icon-menu"></i></a>
					
				</li>
				<li class="projectNameItem" style="margin: auto;font-size: 16px">
					<c:if test="${projectId ne 0  }">
						<a href="${context}/project/${projectId}" id="projectName" class="ml-1 text-success text-align-center"> ${currentProject.getName()}</a> 
					</c:if>
				</li>
				<!-- <li class="breadcrumb-child projectNameItem">
					<div class="nav-item pl-0 pr-3 d-flex">
						<button class="mr-2 back-history pl-5 position-relative" onclick="goBack();"><i class="icon-arrow-left-circle"></i></button> 
						<spring:message code="label.ProjectName" ></spring:message> <a>:</a><a href="${context}/project/${currentProject.getId()}" id="projectName" class="ml-1 back-history text-success"> ${currentProject.getName()}</a> 
					</div>
				</li>
				<li class="d-flex breadcrumb-child hidden projectNameItem"><div class="d-flex"><i class="ti-angle-right my-auto"></i></div></li>
				<li class="breadcrumb-child testsuiteNameItem">
					<div class="nav-item px-3">
						<spring:message code="label.TestsuiteName" ></spring:message><a>:</a><a href="${context}/project/${currentProject.getId()}/${testsuiteId}" id="testSuiteName"> ${testsuiteId}</a>
					</div>
				</li> -->
			</ul>
         <!-- start mobile menu -->
        <!-- end mobile menu -->
         <!-- start header menu --> 
     </div>
 </div>
<script>
var glProjectId = "${projectId}";
var glTestsuiteId= "${testsuiteId}";
var authoRole = "${userDetails.getAuthorities()}";
var username = "${userDetails.getUsername()}";
</script>
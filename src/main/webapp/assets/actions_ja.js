var listActions = [
    {
       
        "data": {
            "id": 2,
            "name": "URLを開く",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
             "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.245Z",
            "updated_at": "2018-11-13T03:27:49.245Z",
            "description": "{0}を開く",
            "test_class": "AC_Open",
            "type": "in"
        },
        "params": [
            {
                "id": 2,
                "name": "Url",
                "test_action_id": 2,
                "param_type": "string",
            }
        ]
    },
    {
        "data": {
            "id": 3,
            "name": "項目に値を入力",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
            "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.260Z",
            "updated_at": "2018-11-13T03:27:49.260Z",
            "description": "{1}に{3}の入力",
            "test_class": "AC_Enter",
            "type": "in"

        },
        "params": [
            {
                "id": 3,
                "name": "Screen",
                "test_action_id": 3,
                "param_type": "layout"
            },
            {
                "id": 4,
                "name": "Item",
                "test_action_id": 3,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 3,
                "param_type": "number"
            },
            {
                "id": 5,
                "name": "Value",
                "test_action_id": 3,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 4,
            "name": "クリック",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
             "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.260Z",
            "updated_at": "2018-11-13T03:27:49.260Z",
            "description": "{1}をクリック",
            "test_class": "AC_Click",
            "type": "in"
        },
        "params": [
            {
                "id": 6,
                "name": "Screen",
                "test_action_id": 4,
                "param_type": "layout"
            },
            {
                "id": 7,
                "name": "Item",
                "test_action_id": 4,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 4,
                "param_type": "number"
            },
        ]
    },
    {
        "data": {
            "id": 5,
            "name": "スクリーンショット取得",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
             "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.260Z",
            "updated_at": "2018-11-13T03:27:49.260Z",
            "description": "スクリーンショットを撮る",
            "test_class": "AC_TakeScreenshot",
            "type": "out"
        },
        "params": []
    },
    {
        "data": {
            "id": 6,
            "name": "ブラウザを閉じる",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-14T04:20:26.188Z",
            "updated_at": "2018-11-14T04:20:26.188Z",
            "description": "ブラウザを閉じる",
            "test_class": "AC_CloseBrowser",
            "type": "in"
        },
        "params": []
    },
    {
        "data": {
            "id": 7,
            "name": "変数をセット",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2018-11-14T05:21:17.991Z",
            "updated_at": "2018-11-14T05:21:17.991Z",
            "description": "{0}に{2}を設定",
            "test_class": "AC_SetVariable",
            "type": "in"
        },
       "params": [
            {
                "id": 8,
                "name": "Name",
                "test_action_id": 7,
                "param_type": "string"
            },
            {
                "id": 9,
                "name": "Variable Type",
                "test_action_id": 7,
                "param_type": "list=Set Value,Set Password"
            },
            {
                "id": 10,
                "name": "Value",
                "test_action_id": 7,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 8,
            "name": "if",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2018-11-21T09:57:36.490Z",
            "updated_at": "2018-11-21T09:57:36.490Z",
            "description": "チェック条件：{0} ",
            "test_class": "AC_If",
            "type": "in",
        },
        "params":[
            {
                "id": 11,
                "name": "Condition",
                "test_action_id": 8,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 9,
            "name": "end if",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2018-11-21T10:01:06.144Z",
            "updated_at": "2018-12-19T04:32:04.819Z",
            "description": "分岐終了",
            "test_class": "AC_EndIf",
            "type": "in"
        },
        "params": []
    },
    {
        "data": {
            "id": 10,
            "name": "loop",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2018-11-21T10:01:31.494Z",
            "updated_at": "2018-11-21T10:01:31.494Z",
            "description": "ループ{0}回",
            "test_class": "AC_Loop",
            "type": "in"
        },
        "params": [
            {
                "id": 12,
                "name": "Times",
                "test_action_id": 10,
                "param_type": "string"
            },
            {
                "id": 13,
                "name": "Variable",
                "test_action_id": 10,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 11,
            "name": "end loop",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-21T10:02:14.483Z",
            "updated_at": "2018-11-21T10:02:14.483Z",
            "description": "ループ終了",
            "test_class": "AC_EndLoop",
            "type": "in"
        },
        "params": []
    },
    {
        "data": {
            "id": 12,
            "name": "Excel range値取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-23T08:36:33.369Z",
            "updated_at": "2018-11-23T08:36:33.369Z",
            "description": "[{0}] [{1}]ファイルから{2}の範囲を読み取む",
            "test_class": "AC_GetRangeValue",
            "type":"in"
        },
        "params":[
            {
                "id": 14,
                "name": "File name",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 15,
                "name": "Sheet name",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 16,
                "name": "Range",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 17,
                "name": "Result",
                "test_action_id": 12,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 25,
            "name": "Excel cell値取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T03:59:53.626Z",
            "updated_at": "2019-03-22T03:59:53.626Z",
            "description": "[{0}] [{1}]ファイルから{2}セルの値を取得",
            "test_class": "AC_GetCellValue",
            "type": "in"
        },
        "params": [
        	{
                "id": 44,
                "name": "File name",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 45,
                "name": "Sheet name",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 46,
                "name": "Row",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 47,
                "name": "Column",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 48,
                "name": "Result",
                "test_action_id": 25,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 26,
            "name": "Excel cell値チェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:02:06.804Z",
            "updated_at": "2019-03-22T04:02:06.804Z",
            "description": "[{0}] [{1}]ファイルの{2}セルの{3}等しい値をチェック",
            "test_class": "AC_CheckCellValue",
            "type": "out"
        },
        "params": [
        	{
                "id": 49,
                "name": "File name",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 50,
                "name": "Sheet name",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 51,
                "name": "Row",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 52,
                "name": "Column",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 53,
                "name": "Result",
                "test_action_id": 26,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 49,
            "name": "Excel cell値設定",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-17T05:33:37.762Z",
            "updated_at": "2019-06-17T05:33:37.762Z",
            "description": "[{0}] [{1}]ファイルの{2}セルに{3}を書き込む",
            "test_class": "AC_SetCellValue",
            "type": "in"
        },
         "params": [
            {
                "id": 109,
                "name": "File name",
                "test_action_id": 49,
                "param_type": "filepath"
            },
            {
                "id": 110,
                "name": "Sheet name",
                "test_action_id": 49,
                "param_type": "string"
            },
            {
                "id": 111,
                "name": "Row",
                "test_action_id": 49,
                "param_type": "string"
            },
            {
                "id": 112,
                "name": "Column",
                "test_action_id": 49,
                "param_type": "string"
            },
            {
                "id": 113,
                "name": "Value",
                "test_action_id": 49,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 56,
            "name": "Excel range値設定",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-03T07:08:49.176Z",
            "updated_at": "2019-07-03T07:08:49.176Z",
            "description": "エクセル：{3}を[{0}] [{1}]ファイルの{2}に書き込む",
            "test_class": "AC_SetRangeValue",
            "type": "in"
        },
         "params": [
            {
                "id": 125,
                "name": "File name",
                "test_action_id": 56,
                "param_type": "filepath"
            },
            {
                "id": 126,
                "name": "Sheet name",
                "test_action_id": 56,
                "param_type": "string"
            },
            {
                "id": 127,
                "name": "Row",
                "test_action_id": 56,
                "param_type": "string"
            },
            {
                "id": 128,
                "name": "Column",
                "test_action_id": 56,
                "param_type": "string"
            },
            {
                "id": 129,
                "name": "Value",
                "test_action_id": 56,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 13,
            "name": "スリープ",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-04T04:51:21.144Z",
            "updated_at": "2018-12-04T04:51:21.144Z",
            "description": "{0}ミリ秒待つ",
            "test_class": "AC_Sleep",
            "type": "in"
        },
        "params": [
            {
                "id": 18,
                "name": "Miliseconds",
                "test_action_id": 13,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 14,
            "name": "選択",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-04T09:08:56.670Z",
            "updated_at": "2018-12-04T09:08:56.670Z",
            "description": "{1}ドロップダウンで{3}を選択",
            "test_class": "AC_Select",
            "type": "in"
        },
       "params": [
            {
                "id": 19,
                "name": "Screen",
                "test_action_id": 14,
                "param_type": "layout"
            },
            {
                "id": 20,
                "name": "Item",
                "test_action_id": 14,
                "param_type": "control"
            },
              {
                "id": 114,
                "name": "Index",
                "test_action_id": 14,
                "param_type": "number"
            },
            {
                "id": 21,
                "name": "Value",
                "test_action_id": 14,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 15,
            "name": "チェックボックスON/OFF設定",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:13:10.937Z",
            "updated_at": "2018-12-11T07:13:10.937Z",
            "description": "{1}に{3}を設定",
            "test_class": "AC_SetCheckboxValue",
            "type": "in"
        },
        "params": [
            {
                "id": 22,
                "name": "Screen",
                "test_action_id": 15,
                "param_type": "layout"
            },
            {
                "id": 23,
                "name": "Item",
                "test_action_id": 15,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 15,
                "param_type": "number"
            },
            {
                "id": 24,
                "name": "Value",
                "test_action_id": 15,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 16,
            "name": "チェックボックス値取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:14:35.685Z",
            "updated_at": "2018-12-11T07:14:35.685Z",
            "description": "{1}から{3}値を取得",
            "test_class": "AC_GetCheckboxValue",
            "type": "in"
        },
       "params": [
            {
                "id": 25,
                "name": "Screen",
                "test_action_id": 16,
                "param_type": "layout"
            },
            {
                "id": 26,
                "name": "Item",
                "test_action_id": 16,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 16,
                "param_type": "number"
            },
            {
                "id": 27,
                "name": "Variable",
                "test_action_id": 16,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 17,
            "name": "値チェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:16:02.792Z",
            "updated_at": "2018-12-11T07:16:02.792Z",
            "description": "{0} {1} {2}のチェック",
            "test_class": "AC_CheckValue",
            "type": "out"
        },
        "params": [
            {
                "id": 28,
                "name": "Variable",
                "test_action_id": 17,
                "param_type": "string"
            },
            {
                "id": 95,
                "name": "Operator",
                "test_action_id": 17,
                "param_type": "list=equal,greater than,less than,greater than or equal,less than or equal,contain,incontain,wildcard"
            },
            {
                "id": 96,
                "name": "Expected",
                "test_action_id": 17,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 18,
            "name": " Javaスクリプト変数設定",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:18:10.242Z",
            "updated_at": "2018-12-11T07:18:10.242Z",
            "description": "{1} jsに{0}を設定",
            "test_class": "AC_SetJavascriptVariable",
            "type": "in"
        },
        "params": [
            {
                "id": 30,
                "name": "Variable",
                "test_action_id": 18,
                "param_type": "string"
            },
            {
                "id": 31,
                "name": "Value",
                "test_action_id": 18,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 19,
            "name": " Javaスクリプト実行",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:18:49.209Z",
            "updated_at": "2018-12-11T07:18:49.209Z",
            "description": " ジャバスクリプトを実行",
            "test_class": "AC_ExecuteJavascript",
            "type": "in"
        },
        "params": [
            {
                "id": 32,
                "name": "Script",
                "test_action_id": 19,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 20,
            "name": " Javaスクリプト変数取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:19:24.755Z",
            "updated_at": "2018-12-11T07:19:24.755Z",
            "description": "{0} jsを{1}に取得",
            "test_class": "AC_GetJavascriptVariable",
            "type": "in"
        },
        "params": [
            {
                "id": 33,
                "name": "Variable",
                "test_action_id": 20,
                "param_type": "string"
            },
            {
                "id": 34,
                "name": "Result",
                "test_action_id": 20,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 21,
            "name": "テストケース実行",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-02-12T08:30:13.553Z",
            "updated_at": "2019-02-12T08:32:31.246Z",
            "description": "{0}テストケースを実行",
            "test_class": "AC_ExecuteTestcase",
            "type": "in"
        },
        "params": [
            {
                "id": 35,
                "name": "Testcase",
                "test_action_id": 21,
                "param_type": "testcase"
            }
        ]
    },
    {
        "data": {
            "id": 22,
            "name": "全スクリーンショット取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-03-14T03:43:11.472Z",
            "updated_at": "2019-03-14T03:43:28.785Z",
            "description": "全スクリーンショットを撮る",
            "test_class": "AC_TakeFullScreen",
            "type": "out"
        },
        "params": []
    },
    {
        "data": {
            "id": 29,
            "name": "CSVファイル読込み",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:17:28.264Z",
            "updated_at": "2019-03-22T04:17:28.264Z",
            "description": "{0}ファイルを{1}に読み込む",
            "test_class": "AC_GetCsvFile",
            "type": "in"
        },
        "params": [
            {
                "id": 60,
                "name": "File name",
                "test_action_id": 29,
                "param_type": "string"
            },
            {
                "id": 61,
                "name": "Result",
                "test_action_id": 29,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 42,
            "name": "CSV書き込み",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-05-15T08:47:15.109Z",
            "updated_at": "2019-05-15T08:47:15.109Z",
            "description": "{0}を{1}に書き込む",
            "test_class": "AC_WriteToCSV",
            "type": "in"
        },
        "params": [
            {
                "id": 85,
                "name": "Data",
                "test_action_id": 42,
                "param_type": "string"
            },
            {
                "id": 86,
                "name": "File name",
                "test_action_id": 42,
                "param_type": "filepath"
            },
            {
                "id": 87,
                "name": "Append",
                "test_action_id": 42,
                "param_type": "list=TRUE,FALSE"
            }
        ]
    },
    {
        "data": {
            "id": 30,
            "name": "コントロール値設定",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-03-22T04:19:39.917Z",
            "updated_at": "2019-03-22T04:32:26.549Z",
            "description": "{3}を{1}アイテムに設定",
            "test_class": "AC_SetControlValue",
            "type": "in"
        },
        "params": [
            {
                "id": 62,
                "name": "Screen",
                "test_action_id": 30,
                "param_type": "layout"
            },
            {
                "id": 63,
                "name": "Item",
                "test_action_id": 30,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 30,
                "param_type": "number"
            },
            {
                "id": 64,
                "name": "Value",
                "test_action_id": 30,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 32,
            "name": "コントロール値取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-03-22T04:22:02.176Z",
            "updated_at": "2019-03-22T04:34:10.953Z",
            "description": "{1}アイテムの値を{3}に取得",
            "test_class": "AC_GetControlValue",
            "type": "in"
        },
        "params": [
            {
                "id": 65,
                "name": "Screen",
                "test_action_id": 32,
                "param_type": "layout"
            },
            {
                "id": 66,
                "name": "Item",
                "test_action_id": 32,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 32,
                "param_type": "number"
            },
            {
                "id": 67,
                "name": "Return Variable",
                "test_action_id": 32,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 33,
            "name": "コントロール値をチェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:35:28.402Z",
            "updated_at": "2019-03-22T04:35:28.402Z",
            "description": "スクリーンショットで{1} {3} {4}の値をチェック",
            "test_class": "AC_CheckControlValue",
            "type": "out"
        },
         "params": [
            {
                "id": 68,
                "name": "Screen",
                "test_action_id": 33,
                "param_type": "layout"
            },
            {
                "id": 69,
                "name": "Item",
                "test_action_id": 33,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 33,
                "param_type": "number"
            },
            {
                "id": 93,
                "name": "Operator",
                "test_action_id": 33,
                "param_type": "list=equal,greater than,less than,greater than or equal,less than or equal,contain,incontain,wildcard"
            },
            {
                "id": 94,
                "name": "Expected",
                "test_action_id": 33,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 34,
            "name": "画像比較",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
           "delete_flag": "",
            "created_at": "2019-03-22T04:37:10.227Z",
            "updated_at": "2019-03-22T04:37:10.227Z",
            "description": "画像比較：{0}と{1}",
            "test_class": "AC_CompareImage",
            "type": "out"
        },
        "params": [
            {
                "id": 71,
                "name": "Recorded image",
                "test_action_id": 34,
                "param_type": "string"
            },
            {
                "id": 72,
                "name": "Expected image",
                "test_action_id": 34,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 35,
            "name": "変数にスクリーンショットを撮る",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:39:17.429Z",
            "updated_at": "2019-03-22T04:39:17.429Z",
            "description": "{0}にスクリーンショットを撮る",
            "test_class": "AC_TakeScreenshotWithResult",
            "type": "out"
        },
        "params": [
            {
                "id": 73,
                "name": "Image name",
                "test_action_id": 35,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 36,
            "name": "データベース結合",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:40:30.989Z",
            "updated_at": "2019-03-22T04:40:30.989Z",
            "description": "データベース{2}に接続",
            "test_class": "AC_ConnectDatabase",
            "type": "in"
        },
        "params": [
            {
                "id": 74,
                "name": "Connection name",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 75,
                "name": "Type sql",
                "test_action_id": 36,
                "param_type": "list=sqlserver,mysql,oracle,mongodb"
            },
            {
                "id": 76,
                "name": "Server string",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 77,
                "name": "Database name",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 78,
                "name": "Username",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 79,
                "name": "Password",
                "test_action_id": 36,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 37,
            "name": "クエリ実行",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:43:04.940Z",
            "updated_at": "2019-03-22T04:43:04.940Z",
            "description": "問い合わせを実行",
            "test_class": "AC_ExecuteQuery",
            "type": "in"
        },
        "params": [
            {
                "id": 80,
                "name": "Connection name",
                "test_action_id": 37,
                "param_type": "string"
            },
            {
                "id": 80,
                "name": "Type",
                "test_action_id": 37,
                "param_type": "list=text,file"
            },
            {
                "id": 81,
                "name": "Query string",
                "test_action_id": 37,
                "param_type": "string"
            },
            {
                "id": 82,
                "name": "Result",
                "test_action_id": 37,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 38,
            "name": "コントロール属性チェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-04-23T06:41:57.082Z",
            "updated_at": "2019-04-23T07:18:11.838Z",
            "description": "{1}の{3}属性が{4}に等しいことをチェック",
            "test_class": "AC_CheckControlAttribute",
            "type": "out"
        },
        "params": [
            {
                "id": 83,
                "name": "Screen",
                "test_action_id": 38,
                "param_type": "layout"
            },
            {
                "id": 84,
                "name": "Item",
                "test_action_id": 38,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 38,
                "param_type": "number"
            },
            {
                "id": 85,
                "name": "Attribute",
                "test_action_id": 38,
                "param_type": "string"
            },
            {
                "id": 86,
                "name": "Value",
                "test_action_id": 38,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 43,
            "name": "フォーマットチェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-05-22T04:06:27.102Z",
            "updated_at": "2019-05-22T04:06:27.102Z",
            "description": "{1}は{0}形式がある？",
            "test_class": "AC_CheckValueFormat",
            "type": "out"
        },
        "params": [
            {
                "id": 87,
                "name": "Format",
                "test_action_id": 43,
                "param_type": "string"
            },
            {
                "id": 88,
                "name": "Value or Variable",
                "test_action_id": 43,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 44,
            "name": "スクリーンテーブル取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-05-24T03:47:15.115Z",
            "updated_at": "2019-05-24T03:47:15.115Z",
            "description": "{1}テーブルの値を{3}に取得",
            "test_class": "AC_GetTableData",
            "type": "in"
        },
       "params": [
            {
                "id": 90,
                "name": "Screen",
                "test_action_id": 44,
                "param_type": "layout"
            },
            {
                "id": 91,
                "name": "Item",
                "test_action_id": 44,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 44,
                "param_type": "number"
            },
            {
                "id": 92,
                "name": "Value",
                "test_action_id": 44,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 45,
            "name": "ftp接続",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-12T10:47:03.493Z",
            "updated_at": "2019-06-12T10:47:03.493Z",
            "description": "FTPサーバー接続：{0}",
            "test_class": "AC_ConnectFTP",
            "type": "in"
        },
        "params": [
            {
                "id": 93,
                "name": "Connect name",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 94,
                "name": "Host",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 95,
                "name": "Port",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 96,
                "name": "Username",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 97,
                "name": "Password",
                "test_action_id": 45,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 46,
            "name": "ftpファイルダウンロード",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-06-12T10:49:45.418Z",
            "updated_at": "2019-06-25T08:48:19.566Z",
            "description": "ftp：{1}で{2}をダウンロード",
            "test_class": "AC_DowloadFTPFile",
            "type": "in"
        },
       "params": [
        {
            "id": 98,
            "name": "Connect name",
            "test_action_id": 46,
            "param_type": "string"
        },
        {
            "id": 99,
            "name": "Remote Path",
            "test_action_id": 46,
            "param_type": "string"
        },
        {
            "id": 100,
            "name": "Local path",
            "test_action_id": 46,
            "param_type": "filepath"
        },
        {
            "id": 101,
            "name": "File name",
            "test_action_id": 46,
            "param_type": "string"
        }
        ]
    },
    {
        "data": {
            "id": 47,
            "name": "fptフォルダダウンロード",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-06-12T10:53:54.282Z",
            "updated_at": "2019-06-25T08:48:50.359Z",
            "description": "{0}ftpダウンロード",
            "test_class": "AC_DowloadFTPFiles",
            "type": "in"
        },
        "params": [
            {
                "id": 102,
                "name": "Connect name",
                "test_action_id": 47,
                "param_type": "string"
            },
            {
                "id": 103,
                "name": "Remote path",
                "test_action_id": 47,
                "param_type": "string"
            },
            {
                "id": 104,
                "name": "Local path",
                "test_action_id": 47,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 48,
            "name": "fptファイルアップロード",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-06-12T10:55:31.687Z",
            "updated_at": "2019-06-25T08:49:15.394Z",
            "description": "{1}ftpアップロード",
            "test_class": "AC_UploadFTPFile",
            "type": "in"
        },
        "params": [
            {
                "id": 105,
                "name": "Connect name",
                "test_action_id": 48,
                "param_type": "string"
            },
            {
                "id": 106,
                "name": "Remote path",
                "test_action_id": 48,
                "param_type": "string"
            },
            {
                "id": 107,
                "name": "Local path",
                "test_action_id": 48,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 60,
            "name": "fptフォルダ取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-05T10:13:34.816Z",
            "updated_at": "2019-07-05T10:13:34.816Z",
            "description": "ftp：{0}のフォルダーを取得",
            "test_class": "AC_GetFtpFolders",
            "type": "in"
        },
       "params": [
        {
            "id": 135,
            "name": "Connect name",
            "test_action_id": 60,
            "param_type": "string"
        },
        {
            "id": 136,
            "name": "Remote path",
            "test_action_id": 60,
            "param_type": "string"
        },
        {
            "id": 137,
            "name": "Result",
            "test_action_id": 60,
            "param_type": "string"
        }
        ]
    },
    {
        "data": {
            "id": 66,
            "name": "fptフォルダ作成",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
           "delete_flag": "",
            "created_at": "2019-08-02T04:14:17.469Z",
            "updated_at": "2019-08-02T04:14:17.469Z",
            "description": "ftpで{0}を作成 ",
            "test_class": "AC_CreateFolderFTP",
            "type": "in"
        },
        "params": [
            {
                "id": 154,
                "name": "Connect name",
                "test_action_id": 66,
                "param_type": "string"
            },
            {
                "id": 155,
                "name": "Dirname",
                "test_action_id": 66,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 67,
            "name": "fpt削除",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-02T04:15:20.026Z",
            "updated_at": "2019-08-02T04:15:20.026Z",
            "description": "ftpで{0}を削除",
            "test_class": "AC_DeleteInFTP",
            "type": "in"
        },
	   "params": [
        {
            "id": 156,
            "name": "Connect name",
            "test_action_id": 67,
            "param_type": "string"
        },
        {
            "id": 157,
            "name": "Dirname",
            "test_action_id": 67,
            "param_type": "string"
        }
	        ]
	    },
    {
        "data": {
            "id": 73,
            "name": "fptファイル存在チェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
           "delete_flag": "",
            "created_at": "2019-08-06T08:12:27.691Z",
            "updated_at": "2019-08-06T08:12:27.691Z",
            "description": "ftp {0}に{1}タイプが存在することをチェック",
            "test_class": "AC_CheckFileExistFTP",
            "type": "out"
        },
       "params": [
        {
            "id": 166,
            "name": "Connect name",
            "test_action_id": 73,
            "param_type": "string"
        },
        {
            "id": 167,
            "name": "Remote path",
            "test_action_id": 73,
            "param_type": "string"
        },
        {
            "id": 168,
            "name": "Type file",
            "test_action_id": 73,
            "param_type": "string"
        },
        {
            "id": 169,
            "name": "Result",
            "test_action_id": 73,
            "param_type": "string"
        }
        ]
    },
    {
        "data": {
            "id": 50,
            "name": "コントロールイメージキャプチャ",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-17T05:52:16.946Z",
            "updated_at": "2019-06-17T05:52:16.946Z",
            "description": "{1}の画像をキャプチャ",
            "test_class": "AC_CaptureControl",
            "type": "out"
        },
        "params": [
            {
                "id": 113,
                "name": "Screen",
                "test_action_id": 50,
                "param_type": "layout"
            },
            {
                "id": 114,
                "name": "Item",
                "test_action_id": 50,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 50,
                "param_type": "number"
            },
            {
                "id": 115,
                "name": "File name",
                "test_action_id": 50,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 51,
            "name": "エレメント属性取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-18T03:44:09.216Z",
            "updated_at": "2019-06-18T03:44:09.216Z",
            "description": "{1}で{3}を取得",
            "test_class": "AC_GetAttribute",
            "type": "in"
        },
         "params": [
            {
                "id": 116,
                "name": "Screen",
                "test_action_id": 51,
                "param_type": "layout"
            },
            {
                "id": 117,
                "name": "Item",
                "test_action_id": 51,
                "param_type": "control"
            },
            {
    			"id": 114,
                "name": "Index",
                "test_action_id": 51,
                "param_type": "number"
            },
            {
                "id": 118,
                "name": "Attribute",
                "test_action_id": 51,
                "param_type": "string"
            },
            {
                "id": 119,
                "name": "Value",
                "test_action_id": 51,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 52,
            "name": "while",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-02T03:56:41.625Z",
            "updated_at": "2019-07-02T03:56:41.625Z",
            "description": "条件{0}のループ",
            "test_class": "AC_While",
            "type": "in"
        },
         "params": [
            {
                "id": 120,
                "name": "Condition",
                "test_action_id": 52,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 53,
            "name": "end while",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-02T03:58:57.381Z",
            "updated_at": "2019-07-02T03:58:57.381Z",
            "description": "ループ終了",
            "test_class": "AC_EndWhile",
            "type": "in"
        },
        "params": []
    },
    {
        "data": {
            "id": 54,
            "name": "ファイルリストを取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-02T04:48:44.804Z",
            "updated_at": "2019-07-02T04:48:44.804Z",
            "description": "{0}の{1}ファイルを取得",
            "test_class": "AC_GetFilesInFolder",
            "type": "in"
        },
         "params": [
            {
                "id": 121,
                "name": "Path",
                "test_action_id": 54,
                "param_type": "filepath"
            },
            {
                "id": 122,
                "name": "Type",
                "test_action_id": 54,
                "param_type": "string"
            },
            {
                "id": 123,
                "name": "Result",
                "test_action_id": 54,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 55,
            "name": "キーを送信",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-03T04:22:49.289Z",
            "updated_at": "2019-07-03T04:22:49.289Z",
            "description": "{0}キーを送信",
            "test_class": "AC_Type",
            "type": "in"
        },
        "params": [
            {
                "id": 124,
                "name": "Value",
                "test_action_id": 55,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 61,
            "name": "get control by",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-09T03:43:15.004Z",
            "updated_at": "2019-07-09T03:43:15.004Z",
            "description": "コントロールの定義{0}",
            "test_class": "AC_GetControlBy",
            "type": "in"
        },
        "params": [
            {
                "id": 137,
                "name": "Control name",
                "test_action_id": 61,
                "param_type": "string"
            },
            {
                "id": 138,
                "name": "Detect by",
                "test_action_id": 61,
                "param_type": "list=id,href,xpath,class,name,text,css"
            },
            {
                "id": 139,
                "name": "Value",
                "test_action_id": 61,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 62,
            "name": "email送信者定義",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-15T02:36:37.915Z",
            "updated_at": "2019-07-15T02:36:37.915Z",
            "description": "送信者の定義：{2}",
            "test_class": "AC_EmailFromContact",
            "type": "in"
        },
        "params": [
            {
                "id": 141,
                "name": "Connect email",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 142,
                "name": "Smtp server",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 143,
                "name": "Port",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 144,
                "name": "Email",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 145,
                "name": "Password",
                "test_action_id": 62,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 63,
            "name": "email送信",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-15T02:38:51.348Z",
            "updated_at": "2019-07-15T02:38:51.348Z",
            "description": "{1}にメールを送信",
            "test_class": "AC_SendEmail",
            "type": "in"
        },
        "params": [
        	{
                "id": 147,
                "name": "Connect email",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 148,
                "name": "To",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 149,
                "name": "Subject",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 150,
                "name": "Content",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 151,
                "name": "Attatch file",
                "test_action_id": 63,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 64,
            "name": "ローカルフォルダ取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-16T08:40:06.075Z",
            "updated_at": "2019-07-16T08:40:06.075Z",
            "description": "{0}の全フォルダーを取得",
            "test_class": "AC_GetFolders",
            "type": "in"
        },
        "params": [
            {
                "id": 153,
                "name": "Path",
                "test_action_id": 64,
                "param_type": "string"
            },
            {
                "id": 154,
                "name": "Result",
                "test_action_id": 64,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 65,
            "name": "ファイルコピー",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-31T08:47:24.470Z",
            "updated_at": "2019-07-31T08:47:24.470Z",
            "description": "{0}ファイルを{1}にコピー",
            "test_class": "AC_CopyFile",
            "type": "in"
        },
        "params": [
            {
                "id": 155,
                "name": "File path",
                "test_action_id": 65,
                "param_type": "filepath"
            },
            {
                "id": 156,
                "name": "New file",
                "test_action_id": 65,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 68,
            "name": "フォルダー内全ファイル取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-02T07:33:59.014Z",
            "updated_at": "2019-08-02T07:33:59.014Z",
            "description": "{0}の全ファイルを取得",
            "test_class": "AC_GetAllFileInFolder",
            "type": "in"
        },
        "params": [
            {
                "id": 157,
                "name": "Path",
                "test_action_id": 68,
                "param_type": "filepath"
            },
            {
                "id": 158,
                "name": "Result",
                "test_action_id": 68,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 69,
            "name": "ファイル削除",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-02T09:28:06.476Z",
            "updated_at": "2019-08-02T09:28:06.476Z",
            "description": "{0}を削除",
            "test_class": "AC_DeleteFile",
            "type": "in"
        },
         "params": [
            {
                "id": 159,
                "name": "File path",
                "test_action_id": 69,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 70,
            "name": "フォルダ移動",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-05T06:30:43.705Z",
            "updated_at": "2019-08-05T06:30:43.705Z",
            "description": "{0}を{1}に移動",
            "test_class": "AC_MoveFolder",
            "type": "in"
        },
        "params": [
            {
                "id": 160,
                "name": "Folder path",
                "test_action_id": 70,
                "param_type": "string"
            },
            {
                "id": 161,
                "name": "New directory",
                "test_action_id": 70,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 73,
            "name": "テストデータ使用",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "{2}ファイルを{1}にアップロード",
            "test_class": "AC_UseTestData",
            "type": "in"
            
        },
       "params": [
            {
                "id": 166,
                "name": "Screen",
                "test_action_id": 73,
                "param_type": "testdata"
            },
            {
                "id": 167,
                "name": "Casenum",
                "test_action_id": 73,
                "param_type": "casenum"
            }
        ]
    },
    {
        "data": {
            "id": 74,
            "name": "テストデータチェック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "{0}の{1}テストデータをチェック",
            "test_class": "AC_CheckData",
            "type": "in"
        },
       "params": [
            {
                "id": 168,
                "name": "Screen",
                "test_action_id": 74,
                "param_type": "testdata"
            },
            {
                "id": 169,
                "name": "Casenum",
                "test_action_id": 74,
                "param_type": "casenum"
            }
        ]
    },
    {
        "data": {
            "id": 75,
            "name": "Restfull API",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "{6}のAPI {0} {1}テストデータを呼び出す",
            "test_class": "AC_API",
            "type": "in"
        },
         "params": [
            {
            	"id":170,
            	"test_action_id": 75,
                "name": "Method",
                "param_type": "list=POST,GET,PUT,UPDATE,DELETE"
            },
            {
                "id":180,
                "test_action_id": 75,
            	"name": "url",
                "param_type": "string"
            },
            {
                "name": "Params",
                "param_type":"key-value"
            },
            {
                "name": "Authorization",
                "param_type":"auth",
            },
            {
                "name": "Header",
                "param_type":"header",
            },
            {
                "name": "Body",
                "param_type":"body"
            },
            //Return to a json value into #variableName
            {
                "name": "output",
                "param_type": "Variable"
            }
           
        ]
},
    {
        "data": {
            "id": 76,
            "name": "右クリック",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "click on {1}",
            "test_class": "AC_RightClick",
            "type": "in",
            
        },
        "params": [
            {
                "id": 168,
                "name": "Screen",
                "test_action_id": 76,
                "param_type": "layout"
            },
            {
                "id": 169,
                "name": "Item",
                "test_action_id": 76,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 38,
                "param_type": "number"
            }
        ]
    },
    {
        "data": {
            "id": 77,
            "name": "find elements by",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-09T03:43:15.004Z",
            "updated_at": "2019-07-09T03:43:15.004Z",
            "description": "find elements {0} by {3}",
            "test_class": "AC_FindElementsBy",
            "type": "in",
            
        },
        "params": [
            {
                "id": 137,
                "name": "Return name",
                "test_action_id": 77,
                "param_type": "string"
            },
            {
                "id": 137,
                "name": "From parent",
                "test_action_id": 77,
                "param_type": "string"
            },
            {
                "id": 137,
                "name": "Index",
                "test_action_id": 77,
                "param_type": "string"
            },
            {
                "id": 139,
                "name": "Css Selector",
                "test_action_id": 77,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 12,
            "name": "Excel column取得",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-23T08:36:33.369Z",
            "updated_at": "2018-11-23T08:36:33.369Z",
            "description": "read {2} range from file [{0}][{1}]",
            "test_class": "AC_GetColumn",
            "type":"in",
            
        },
        "params": [
            {
                "id": 14,
                "name": "File name",
                "test_action_id": 12,
                "param_type": "filepath"
            },
            {
                "id": 15,
                "name": "Sheet name",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 16,
                "name": "Range",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 17,
                "name": "Result",
                "test_action_id": 12,
                "param_type": "string"
            }
        ]
    }
]

var actionHelp = {"AC_Open": {
    "document": {
        " Url": "URLアドレス、例：https://www.google.com.vn/。"
        },
    "picture": "/assets/img/AC_Open.PNG"
    },
"AC_Enter": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Index":"",
        "Value": "値、変数または式。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_Enter.PNG"
},
"AC_Click": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
    },
    "picture": "/assets/img/AC_Click.PNG"
},
"AC_TakeScreenshot": {
    "document": "画面を撮る。",
    "picture": "/assets/img/AC_TakeScreenshot_ja.PNG"
},
"AC_CloseBrowser": {
    "document": "ブラウザーを閉じる。",
    "picture": "/assets/img/AC_CloseBrowser_ja.PNG"
},
"AC_SetVariable": {
    "document": {
        "Name": "変数名は#で始まります。例：#username。",
        "Variable Type": "値の設定、パスワードの設定の2種類があります。　テスト形の値は、値の設定し、パスワード形の値はパスポートを設定します。",
        "Value": "値、変数または式。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_SetVariable.PNG"
},
"AC_If": {
    "document": {
        "Condition": "boolean値、boolean変数、boolean式です。　Ifアクションは常にend ifアクションに伴います。",
        "「変数」": " 「＃で始まり、例#username。」 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_If_ja.PNG"
},
"AC_EndIf": {
    "document": " Ifアクションは常にend ifアクションに伴います。",
    "picture": "/assets/img/AC_If_ja.PNG"
},
"AC_Loop": {
    "document": {
        "Times": "タイムは、整数値、整数変数、または整数結果の式です。　 アクションループは常にアクションエンドループに伴います。",
        "Variable": "n番目の反復を表す変数を返します。例えば、#n。　 変数を使用してループに入力することができます。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_Loop_ja.PNG"
},
"AC_EndLoop": {
    "document": "エンドループアクションはいつもループアクションに伴います。",
    "picture": "/assets/img/AC_Loop_ja.PNG"
},
"AC_GetRangeValue": {
    "document": {
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルxlsx、xlsの2つの形式のいずれかに属します。　 テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。　 ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。",
        "Sheet name": "名シートはデータを取るシート名。例えば：Sheet1。",
        "Range": "範囲名。例えば：A2:B11。",
        "Result": "Resultは返された結果を含む変数です。例えば：#result。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_GetRangeValue.PNG"
},
"AC_GetCellValue": {
    "document": {
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルはxlsx、xlsの2つの形式のいずれかに属します。　 テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。　 ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。",
        "Sheet name": "シート名はデータを取るシート名。例えば：Sheet1。",
        "Row": "行はシートの行数です。",
        "Column": "列はシートの列番号です。",
        "Value": "値は、返された結果を含む変数です。例えば：#result。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_GetCellValue.PNG"
},
"AC_CheckCellValue": {
    "document": {
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルはxlsx、xlsの2つの形式のいずれかに属します。　 テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。 　ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。",
        "Sheet name": "シート名はデータを取るシート名です。例えば：Sheet1。",
        "Row": "行はシートの行数です。",
        "Column": "列はシートの列数です。",
        "Value": "Valueは列と行から取得した結果と比べる値です。 値、変数、式である可能性があります。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_CheckCellValue.PNG"
},
"AC_SetCellValue": {
    "document": {
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルはxlsx、xlsの2つの形式のいずれかに属します。　テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。　 ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。",
        "Sheet name": "シート名はデータを取るシート名です。例えば：Sheet1。",
        "Row": "行はシートの行数です。",
        "Column": "列はシートの列数です。",
        "Value": "Valueは、列と行から取得した結果との比較する値です。 値、変数または式である可能性があります。",
        "「変数」": " 「＃で始まり、例#username。」 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_SetCellValue.PNG"
},
"AC_SetRangeValue": {
    "document": {
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルはxlsx、xlsの2つの形式のいずれかに属します。　 テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。　 ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。",
        "Sheet name": "シート名はデータを取るシートの名。例えば：Sheet1。",
        "Row": "行はシートの行数です。",
        "Column": "列はシートの列数です。",
        "Value": "Valueは、列と行から取得した結果との比較する値です。　値、変数または式である可能性があります。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_SetRangeValue.PNG"
},
"AC_Sleep": {
    "document": {
        "Miliseconds": "次の段階はしばらく待った後で実施されます。"
    },
    "picture": "/assets/img/AC_Sleep.PNG"
},
"AC_Select": {
    "document": { 
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Value": "Valueはリストにある値、形式または変数です。",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」 ",
		"「表現」": " 「${で始まり、 }で終わり」 ",
    },
    "picture": "/assets/img/AC_Select.PNG"
},
"AC_SetCheckboxValue": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Index":"",
        "Value": "チェックボックスを選択する場合、値はTrue / On、そうでない場合はFalse / Off。"
    },
    "picture": "/assets/img/AC_SetCheckboxValue.PNG"
},
"AC_GetCheckboxValue": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Variable": "返された結果を含む変数。",
        "Index": "",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_GetCheckboxValue.PNG"
},
"AC_CheckValue": {
    "document": {
        "Variable": "Variableは値、変数または形式です。",
        "Operator": "比較したいオペレーター。",
        "Expected": "Expectedは値、変数または形式。　Value = expectedの場合、Trueを返す。そうでない場合はfalseを返します。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_CheckValue_ja.PNG"
},
"AC_SetJavascriptVariable": {
    "document": {
        "Variable": " javascriptの変数。例えば：#value。",
        "Value ": "javascriptの変数に値、変数または形式。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_SetJavascriptVariable.PNG"
},
"AC_ExecuteJavascript": {
    "document": {
        "Script": "javascriptの文 "
    },
    "picture": "/assets/img/AC_ExecuteJavascript.PNG"
},
"AC_GetJavascriptVariable": {
    "document": {
        "Variable": "Variableはデータがあった変数。",
        "Result": "Resultは返された結果を含む変数です。例えば：#data。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",      
    },
    "picture": "/assets/img/AC_GetJavascriptVariable_ja.PNG"
},
"AC_ExecuteTestcase": {
    "document": {
        "Testcase": "作成されたテストケース。"
    },
    "picture": "/assets/img/AC_ExecuteTestcase.PNG"
},
"AC_TakeFullScreen": {
    "document": "すべてのテスト画面を取る。",
    "picture": "/assets/img/AC_TakeFullScreen_ja.PNG"
},
"AC_GetCsvFile": {
    "document": {
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルはxlsx、xlsの2つの形式のいずれかに属します。　テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。　 ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。 ",
        "Result": "Resultは返された結果を含む変数です, 例えば：#result。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」。 ",   
    },
    "picture": "/assets/img/AC_GetCsvFile.PNG"
},
"AC_WriteToCSV": {
    "document": {
        "Data": "データはファイルに入る必要がある値、変数または形式。",
        "File name": "ファイル名はエクセルファイルのパスです。　エクセルファイルはxlsx、xlsの2つの形式のいずれかに属します。　テストスイートのテストデータにアップロードされたファイルの場合は#testData/'filename'の形式でファイル名を記入します。　 ファイルが今回のテスト結果である場合#testResult/'filename'の形式で記入します。 ",
        "Append": "ファイルにデータを入力したい場合、AppendはTrueで、そうでない場合はFalseである。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",    
    },
    "picture": "/assets/img/AC_WriteToCSV_ja.PNG"
},
"AC_SetControlValue": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Value": "値、変数または形式。",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",     
    },
    "picture": "/assets/img/AC_SetControlValue.PNG"
},
"AC_GetControlValue": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        " Value": "Value は返された結果を含む変数。例えば：#data。",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",       
    },
    "picture": "/assets/img/AC_GetControlValue.PNG"
},
"AC_CheckControlValue": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Operator ": "比較したいオペレーター。",
        "Expected ": "Expectedは値、変数または形式。value = expectedの場合、Trueを返す、そうでない場合はfalseを返します。　",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",  
    },
    "picture": "/assets/img/AC_CheckControlValue.PNG"
},
"AC_CompareImage": {
    "document": {
        "Recorded image": "同じサイズの２つの画像を比較します。　画像のパスはRecord imageとExpected imageの位置に入力されます。"
    },
    "picture": "/assets/img/AC_CompareImage.PNG"
},
"AC_TakeScreenshotWithResult": {
    "document": {
        "Image name": "画像名は画像ファイルと画像ファイル名のパスです。例えば：C:\Paracel\img\Redmine\login.PNG。　テストスイートのテストデータに画像ファイルをアップロードしたい場合、#testData/'nameimage' の形式でファイル名を入力します。例えば：#testData/login.PNG"
    },
    "picture": "/assets/img/AC_TakeScreenshotWithResult.PNG"
},
"AC_ConnectDatabase": {
    "document": {
        "Connection name": "名を付けるデータベース、　例えば：#connectData。",
        "Type sql": " sqの種類、例えば：mysql。",
        "Server string": "データベースに応じるサーバー。",
        "Database name": "接続の必要があるデータベース名。",
        "Username": "データベースに応じるユーザー。",
        "Password": "データベースに応じるパスワード。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_ConnectDatabase.PNG"
},
"AC_ExecuteQuery": {
    "document": {
        "Connection name": "名を付けるデータベース、例えば：#connectData。",
        "Query string": "query文、例えば：Select * From customer。",
        "Result": "Resultは返された結果を含む変数です。例えば：#listdata。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_ExecuteQuery.PNG"
},
"AC_CheckControlAttribute": {
    "document": {
        "Screen": "SCREEN DESIGNに作成があった画面。",
        "Item": "画面のアイテム。",
        "Attribute": "Attributeは、アイテムに応じるオープンhtmlタグに配置された属性です。例えば：name。",
        "Value ": "attributeの値と比較する値、変数または形式。",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_CheckControlAttribute.PNG"
},
"AC_CheckValueFormat": {
    "document": {
        "Format": "比較する値のフォーマット。例えば：dd/mm/yyyy。",
        "Value or Variable": "上記のフォーマットと比較する値、変数または形式。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_CheckValueFormat.PNG"
},
"AC_GetTableData": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "Value": "値は返された結果を含む変数です。例えば：#data。",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_GetTableData.PNG"
},
"AC_ConnectFTP": {
    "document": {
        "Connect name": "名を付けるFtp。例えば：ftpconnect。",
        "Host": "ホストの住所。",
        "Port": "ポート。",
        "Username": "ユーザー名。",
        " Password": "パスワード。"
    },
    "picture": "/assets/img/AC_ConnectFTP.PNG"
},
"AC_DowloadFTPFile": {
    "document": {
        "Connect name": "名を付けるFtp。",
        "Remote Path": "ftpサーバーにフォルダーのパス。",
        "Local path": "ダウンロードファイルを含むフォルダーのパス。",
        "File name": "ダウンロードの必要があるファイル名。"
    },
    "picture": "/assets/img/AC_DowloadFTPFile.PNG"
},
"AC_DowloadFTPFiles": {
    "document": {
        "Connect name": "名を付けるFtp",
        "Remote path": "ftpサーバーにフォルダーのパス。",
        "Local path": "ダウンロードファイルを含むフォルダーのパス。"
    },
    "picture": "/assets/img/AC_DowloadFTPFiles.PNG"
},
"AC_UploadFTPFile": {
    "document": {
        "Connect name": "名を付けるFtp。",
        "Remote path": "ftpサーバーにフォルダーのパス。",
        "Local path": "ダウンロードファイルを含むフォルダーのパス。"
    },
    "picture": "/assets/img/AC_UploadFTPFile_ja.PNG"
},
"AC_GetFtpFolders": {
    "document": {
        "Connect name": "名を付けるFtp。",
        "Remote path": "ftpサーバーにフォルダーのパス。",
        "Result": "Resultは返された結果を含む変数。例えば：#listdata。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_GetFtpFolders_ja.PNG"
},
"AC_CreateFolderFTP": {
    "document": {
        "Connect name": "名を付けるFtp。",
        "Dirname": "新規フォルダー名。"
    },
    "picture": "/assets/img/AC_CreateFolderFTP_ja.PNG"
},
"AC_DeleteInFTP": {
    "document": {
        "Connect name": "名を付けるFtp。",
        "Dirname": "削除の必要があるフォルダーへのパス。"
    },
    "picture": "/assets/img/AC_DeleteInFTP_ja.PNG"
},
"AC_CheckFileExistFTP": {
    "document": {
        "Connect name": "名を付けるFtp。",
        "Remote path": "ftpサーバーにフォルダーのパス。",
        "Type file": "ファイルの種類。例えば： xlsx。",
        "Result": "Resultは返された結果を含む変数。例えば： #listdata。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_CheckFileExistFTP_ja.PNG"
},
"AC_CaptureControl": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Item": "画面のアイテム。",
        "File name": "ファイル名は画像ファイルのパスです。　テストスイートのテストデータに画像ファイルをアップロードしたい場合、#testDataの形式でファイルを入力します。",
        "Index":""
    },
    "picture": "/assets/img/AC_CaptureControl.PNG"
},
"AC_GetAttribute": {
    "document": {
        "Screen": "SCREEN DESIGNに作成があった画面。",
        "Item": "画面のアイテム。",
        "Attribute": "Attributeは、アイテムに応じるオープンhtmlタグに配置された属性です。",
        "Value": "Valueは返された結果を含む変数です。",
        "Index":"",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_GetAttribute.PNG"
},
"AC_While": {
    "document": {
        "Condition": "n番目の反復を表す変数を返します。例えば、#n。  変数を使用してループに入力することができます。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_While_ja.PNG"
},
"AC_EndWhile": {
	"document":"エンドループアクションはいつもループアクションに伴います。",
    "picture": "/assets/img/AC_While_ja.PNG"
},
"AC_GetFilesInFolder": {
    "document": {
        "Path": "ファイルのパス。",
        "Type": "取る必要があるファイルの種類。",
        "Result": "Resultは返された結果を含む変数。例えば：#listdata。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」 ",
    },
    "picture": "/assets/img/AC_GetFilesInFolder.PNG"
},
"AC_Type": {
    "document": {
    	"Value" : "1つ以上のキーをブラウザに送信します"
    },
    "picture": "/assets/img/AC_Type.PNG"
},
"AC_GetControlBy": {
    "document": {
        "Item name": "新規アイテム名。",
        "Detect by": "Detect byはアイテムに応じるオープンHTMLタグに配置される属性です。",
        " Value": "属性の値、変数または式。",
        "「変数」": " 「＃で始まり、例#username。」 。",
		"「表現」": " 「${で始まり、 }で終わり」 。",
    },
    "picture": "/assets/img/AC_GetControlBy.PNG"
},
"AC_EmailFromContact": {
    "document": {
        "Connect email": "名を付けるメールアカウント。 例えば：#emailconnect。",
        "Smtp server": "smtpサーバーアドレス。",
        "Port": "ポート。",
        "Email": "メールアドレス。",
        "Password": "パスワード。"
    },
    "picture": "/assets/img/AC_EmailFromContact.PNG"
},
"AC_SendEmail": {
    "document": {
        "Connect email": "名を付けるメールアカウント。 例えば：#emailconnect。",
        "From": "送る人のメールアドレス。",
        "To": "受ける人のメールアドレス。",
        "Subject": "メールの件名。",
        "Content": "メールの内容。",
        "Attatch file": "パスは送るファイルを含めます。"
    },
    "picture": "/assets/img/AC_SendEmail_ja.PNG"
},
"AC_GetFolders": {
    "document": {
        "Path": "パスはコピーの必要があるフォルダーのパスです。",
        "Result": "Resultは返された結果を含む変数です。 例えば#listdataです。",
        "「変数」": " 「＃で始まり、例#username。」。 ",
		"「表現」": " 「${で始まり、 }で終わり」。 ",
    },
    "picture": "/assets/img/AC_GetFolders.PNG"
},
"AC_CopyFile": {
    "document": {
        "File path": "ファイルパスはコピーの必要があるファイルのパスです。　 テストスイートのテストデータにアップロードされたファイルの場合、#testData/'filename'のフォーマット通りにファイル名を記入します。　 ファイルは今回のテスト結果の場合、#testResult/'filename'フォーマット通りに記入します。",
        "New file": "新規ファイルアドレス。"
    },
    "picture": "/assets/img/AC_CopyFile.PNG"
},
"AC_GetAllFileInFolder": {
    "document": {
        "Path": "パスは取るファイルを含むフォルダーのパスです。　テストスイートのテストデータにアップロードされたフォルダーの場合、#testDataのフォーマット通りにファイル名を記入します。　 フォルダーは今回のテスト結果の場合、#testResultフォーマット通りに記入します。",
        "Result": "Resultは返された結果を含む変数です。 例えば#listdataです。",
        "「変数」": " 「＃で始まり、例#username。」 ",
		"「表現」": " 「${で始まり、 }で終わり」 ",
    },
    "picture": "/assets/img/AC_GetAllFileInFolder.PNG"
},
"AC_DeleteFile": {
    "document": {
        "File path": "ファイルパスは削除の必要があるファイルのパスです。　 テストスイートのテストデータにアップロードされたファイルの場合、#testData/'filename'の形式でファイル名を記入します。　ファイルは今回のテスト結果の場合、#testResult/'filename'の形式で記入します。"
    },
    "picture": "/assets/img/AC_DeleteFile.PNG"
},
"AC_MoveFolder": {
    "document": {
        "Folder path": "フォルダーーパスは、削除されたフォルダーーのパスです。",
        "New directory": "新規フォルダー追加。"
    },
    "picture": "/assets/img/AC_MoveFolder.PNG"
},
"AC_UseTestData": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Casenum ": "テストケースの番号は作成された 。"
    },
    "picture": "/assets/img/AC_UseTestData.PNG"
},
"AC_CheckData": {
    "document": {
        "Screen": "SCREEN DESIGNで作成があった画面。",
        "Casenum": "テストケースの番号は作成された。"
    },
    "picture": "/assets/img/AC_CheckData.PNG"
},
"AC_API": {
    "document": " ",
    "picture": "/assets/img/AC_API.PNG"
},
"AC_RightClick":{
	"document": {
		"Screen":"",
		"Item":"",
		"Index":""
	},
	"picture": "/assets/img/AC_RightClick.PNG"
},
"AC_FindElementsBy":{
	"document": {
		"Return name":"",
		"From parent":"",
		"Index":"",
		"Css Selector":"https://www.w3schools.com/cssref/css_selectors.asp"
	},
	"picture": "/assets/img/AC_FindElementsBy.PNG"
},
"AC_GetColumn":{
	"document": {
		"Return name":"",
		"From parent":"",
		"Index":"",
		"Css Selector":"<a href='https://www.w3schools.com/cssref/css_selectors.asp' target='_blank'>https://www.w3schools.com/cssref/css_selectors.asp</a>"
	},
	"picture": "/assets/img/AC_FindElementsBy.PNG"
}
};


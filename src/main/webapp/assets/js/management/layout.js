var glCurrentLayout = "";

// func count Layout
jQuery(document).ready(function() {
	'use strict';
	validateAddLayout();
	validateAddLayoutDetail();
});


var tableItemList=["controlName","type","id","name", "class","xpath","href","textContent","initvalue","dataformat","readonly","disabled","maxlength","minlength","required","background-color","color","text-align", "errorAttributeName", "errorAttributeValue", "noteLayout"];
var conditionList=["id","name", "class","href","textContent","xpath"];

function getCurrentLayout(layoutName){
	
	return glCurrentLayout;
}



function getControlByName(controlName){
	var currentLayout = getCurrentLayout(null);
	var controls = currentLayout.Controls;
	for(var j = 0; j < controls.length; j++){
		if (controls[j].Name == controlName){
			return controls[j ]
		}
	}
	return null;
}


//close detail layout
function closeDetailLayout(){
	if(existEdit == true){
		swal({
	        title: getLang(currentLocale,"Save change in CONTROL?"),
	        text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
	        type: "warning", 
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: getLang(currentLocale,"Save"),
	        cancelButtonText: getLang(currentLocale,"Dont's Save"),
	        closeOnConfirm: true
	    },function(isConfirm){
	    	if(isConfirm) {
	    		saveCurrentLayout(event); 
	    	}	
	    	window.location.href= jscontext +"/project/" + projectId +"/screenDesign";
	    		
		});
		existEdit = false;
	}else{
		window.location.href= jscontext +"/project/" + projectId +"/screenDesign";

	}
}

//call modal create detail 
function callModalDetail(){
  $(".callDetailModal").click(function () {
      $("#updateControl").removeClass("hidden");
      $('#modalDetailLayout').modal('show');
      getAttrControlName = $(this).attr("data-name");
      if(getAttrControlName != undefined){
    	  var controlName = $(this).attr("data-name");
      }else{
    	  var controlName = $(this).parent().closest(".callDetailModal").attr("data-name");
      }
      
      openEditControlModal(controlName);
  });
}

// func clear Attr
function modalhideclearAttr(){
	$('#modalGenerateTestcase').on('shown.bs.modal', function () {
		  $('#btnGenerate').removeAttr("data-name");
		  $('#btnGenerate').removeAttr("data-numb");
		})
}

// func reset Form of modal_add_new_Layout
function resetFormLayout() {
	$('#modalAddNewLayout').on('hidden.bs.modal', function() {
		document.getElementById("new_layout").reset();
	    var $alertas = $('#new_layout');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

//func reset Form of modal_add_new_Layout
function resetFormDeitalLayout() {
	$('#modalDetailLayout').on('hidden.bs.modal', function() {
		document.getElementById("bodyLayoutDetails").reset();
	    var $alertas = $('#bodyLayoutDetails');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

//func validate add Layout
var validateAddLayout = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#new_layout');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    $.validator.addMethod("checkExist",function(value, element, regexp) {
    	var $alertas = $('#new_layout');
    	var nameLayout = $("#layout_name").val();
    	var checkBoolean = true;
    	if(JSON.stringify(layoutls) != "{}" ){
	    	$.each(layoutls, function(ind,vals){
	    		if(vals.layoutName == nameLayout){
	    			checkBoolean = false;
	    		} 
	    	});
    	}else{
    		return checkBoolean;
    	}
    	return checkBoolean;
    });
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	layoutName: {
                minlength: 2,
                maxlength:50, 
                required: {
                	depends:function(){
		            $(this).val($.trim($(this).val()));
		            return true;
                	}
                },
                regex:"^[a-zA-Z0-9\\_]{1,50}$",
                checkExist: true
            },
            layoutDescription: {
                required: true,
                minlength: 5,
            },
           
        },     
        messages: {
        	layoutName:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 2 characters!"),
        		regex: getLang(currentLocale,"Cannot special characters!"),
        		checkExist: getLang(currentLocale,"Layout Name is exist. Please enter another name!!!")
			},
        	layoutDescription:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	} ,
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();

        }
        
    });
}

var validateAddLayoutDetail = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var formDetail = $('#bodyLayoutDetails');
    var error2 = $('.alert-danger', formDetail);
    var success2 = $('.alert-success', formDetail);
    var layouts = layoutls;
    
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    
    formDetail.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        
        rules: {
        	controlName: {
                minlength: 1,
                maxlength: 50,
                required: true,
                regex:"^[a-zA-Z0-9.\-_]*$"
            },
            type: {
                required: true,
            },
            errorAttributeName :{
            	
            }
        },
        
        messages: {
        	controlName:{
        		maxlength: getLang(currentLocale,"Please enter at less 50 characters!"),
        		required:getLang(currentLocale,"Cannot be blank!"),
        		regex: getLang(currentLocale,"Cannot special characters!"),
        	},
        	type: getLang(currentLocale,"Cannot be blank!")
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            $("#hiddenDataName").attr("data-name",$("#valControlName").val());
            var hiddenDataName = $("#hiddenDataName");
            var itemNameLayout= $('#titleLayout').text();
        	var layouts = layoutls;
	    	for(var i = 0; i < layouts.length; i++){
				if(layouts[i]["Name"] == itemNameLayout){
					var currentLayout = layouts[i];
					var controls = currentLayout.Controls;
					for(var j = 0; j < controls.length; j++){
						var AttributesArr = controls[j]['Attributes'];
						for(var m = 0; m < AttributesArr.length; m ++){
							var controlName = $("#valControlName").val();
							if(AttributesArr[m]['Name'] == "controlName" && AttributesArr[m]['Value'] == controlName){
									var $alertas = $('#bodyLayoutDetails');
									error2.show();
									success2.hide();
									$alertas.validate().resetForm();
									$alertas.find('.form-group').removeClass('has-success');
							        $alertas.find('.controllNameVal').addClass('has-error');
							        $alertas.find('.controlNameIcon').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Control Name is exist. Please enter another name!!!"));
							        return false;
							}
						}
					}
				}
	    	}
	    	
    	    var $alertas = $('#bodyLayoutDetails');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
        
    });
}

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}

// func show modal confirm delete layout
function showConfirmMessageDeleteLayout(isThis) {
	 var items = $(isThis).attr("data-name");
	 var idLayout = $(isThis).attr("data-id");
	 var testsuiteName = $(isThis).attr("testsuite-name");
   swal({
       title: getLang(currentLocale,"Are you sure delete Layout:") + items +"?",
       text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
       cancelButtonText: getLang(currentLocale,"Cancel"),
       customClass:"modalDelLayout",
       closeOnConfirm: false
   }, function () {
	   $(".modalDelLayout .confirm").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	   $(".modalDelLayout .confirm").prop("disabled", true);
	   deleteLayout(items, idLayout, testsuiteName);
	   
   });
}



//delete layout
function deleteLayout(items, idLayout, testsuiteName){
	var layoutNames = items;
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/deleteLayout/"+idLayout,
		type : "POST",
		data : layoutNames,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#bodyLayoutRecord").empty();
				$("#loadingScreenDesign").show();
				$(".modalDelLayout .confirm").html(getLang(currentLocale,"Yes, delete it!"));
			    $(".modalDelLayout .confirm").prop("disabled", false);

				var testsuiteName = $("#filterTestsuite").val();
				var createBy = $("#filterCreateBy").val();
				var createDate = $("#filterCreateDate").val();
				var fileName = $("#filterFileName").val();
				drawLayoutByFilter(testsuiteName, createBy, createDate, fileName);
//				drawTestsuiteReport();
				existEdit = true;
			} else {
			}
			swal({
		        title: getLang(currentLocale,"Delete Layout Success!!!"),
		        type: "success",
		        timer: 2000,
		        showConfirmButton: false
		    });
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}





//generate item layout
function generateItemLayout(controls) {
  controls.forEach(function (tl, i) {
      var newControlType = "label"; 
      var textValue="";
      for (var j =0; j < tl.Attributes.length ; j++){
    	  if (tl.Attributes[j]["Name"] == "type"){
    		  newControlType = tl.Attributes[j]["Value"];

    	  }
    	  if (tl.Attributes[j]["Name"] == "textContent"){
    		  textValue = tl.Attributes[j]["Value"];

    	  }
      }
      newControlName = tl.Name;
      var typeControlLayout;
      if(newControlType == 'textarea'){
          typeControlLayout = '<textarea class="form-control" value="'+textValue+'">'+textValue+'</textarea>';
      }
      else if(newControlType == 'a'){
          typeControlLayout = '<a href="#">'+textValue+'</a>';
      }
      else if(newControlType == 'submit'){
          typeControlLayout = '<button type="submit" class="btn btn-submit">Submit</button>';
      }
      else if(newControlType == 'button'){
          typeControlLayout = '<button type="button" class="btn btn-submit">Button</button>';
      }
      else if(newControlType == 'checkbox'){
          typeControlLayout = '<input type="checkbox" id="'+textValue+'"><label for="'+textValue+'" class="labelCheckbox-layout">'+textValue+'</label>';
      }
      else if(newControlType == 'radio'){
          typeControlLayout = '<input type="radio" id="'+textValue+'"> <label for="'+textValue+'" class="labelCheckbox-layout">'+textValue+'</label>';
      }
      else if(newControlType == 'text'){
          typeControlLayout = '<input type="text" class="form-control" value="'+textValue+'">';
      }
      else if(newControlType == 'input'){
          typeControlLayout = '<input type="text" class="form-control" value="'+textValue+'">';
      }
      else if(newControlType == 'text'){
          typeControlLayout = '<input type="text" class="form-control" value="'+textValue+'">';
      }
      else if(newControlType == 'password'){
          typeControlLayout = '<input type="password" class="form-control" value="'+textValue+'">';
      }
      else if(newControlType == 'label'){
          typeControlLayout = '<div class="iconType callDetailModal" onclick="callModalDetail(event)">&#60;label&#62; '+textValue+' &#60;/label&#62;</div>';
      }
      else if(newControlType == 'div'){
          typeControlLayout = '<div class="iconType callDetailModal" onclick="callModalDetail(event)">&#60;div&#62;'+textValue+'&#60;/div&#62;</div>';
      }
      else if(newControlType == 'span'){
          typeControlLayout = '<div class="iconType callDetailModal" onclick="callModalDetail(event)">&#60;span&#62;'+textValue+'&#60;/span&#62;</div>';
      }
      else if(newControlType == 'h1'){
          typeControlLayout = '<div class="iconType callDetailModal" onclick="callModalDetail(event)">&#60;h1&#62;'+textValue+'&#60;/h1&#62;</div>';
      }
      else if(newControlType == 'h2'){
          typeControlLayout = '<div class="iconType callDetailModal" onclick="callModalDetail(event)">&#60;h2&#62;'+textValue+'&#60;/h2&#62;</div>';
      }
      else if(newControlType == 'h3'){
          typeControlLayout = '<div class="iconType callDetailModal" data-name="'+newControlName+'" data-number="'+i+'">&#60;h3&#62;'+textValue+'&#60;/h3&#62;</div>';
      }
      else if(newControlType == 'h4'){ 
          typeControlLayout = '<div class="iconType callDetailModal" data-name="'+newControlName+'" data-number="'+i+'">&#60;h4&#62;'+textValue+'&#60;/h4&#62;</div>';
      }
      else if(newControlType == 'h5'){
          typeControlLayout = '<div class="iconType callDetailModal" data-name="'+newControlName+'" data-number="'+i+'">&#60;h5&#62;'+textValue+'&#60;/h5&#62;</div>';
      }
      else if(newControlType == 'h6'){
          typeControlLayout = '<div class="iconType callDetailModal" data-name="'+newControlName+'" data-number="'+i+'">&#60;h6&#62;'+textValue+'&#60;/h6&#62;</div>';
      }
      else if(newControlType == 'img'){
          typeControlLayout = '<div class="iconType callDetailModal" data-name="'+newControlName+'" data-number="'+i+'"><i class="ti-image"></i></div>';
      }
      else{
          typeControlLayout = '<div class="iconType callDetailModal" data-name="'+newControlName+'" data-number="'+i+'"><i class="icon-arrow-left"></i>' +newControlType+ '<i class="icon-arrow-right"></i></div>';
      }
      var htmlItemControlLayout = 	'<div class="col-controlName item form-group listJsons panel panel-default" id="divaddjson'+i+'">'
							  		+'<div class="row step panel-heading">'
							  		+'<div class="btnRemoveRecode" onclick="deleteColJson(\''+'divaddjson'+i+'\',\''+tl.Name+'\')">'
							  		+'<i class="ti-trash"></i>'
							  		+'</div>'
							  		+'<div class="col-md-12 col-sm-12 col-xs-12 text-left main-box-ControlName callDetailModal" data-name="'+tl.Name+'" data-number="'+i+'">'
							  		+'<div class="iconType callDetailModal" onclick="callModalDetail(event)">'
							  		+ typeControlLayout
							  		+'</div>'
							      	+'<a class="toggle-clicks collapsed " >'
									+'<div class="NameControl text-truncate">'
									+tl.Name
									+'</div>'
									+'</a>'
									+'<div class="card__circle"></div>'
									+'<div class="card__circle"></div>' 
									+'</div>'
									+'</div>'
									+'<div class="clearfix"></div>'
									+'</div>';
      $("#accordion").append(htmlItemControlLayout);
  });

};

// func load detail body
// load a controll to modal
function openEditControlModal(controlName) {
  var theControl = getControlByName(controlName);
  $("#bodyLayoutDetails [name="+ tableItemList[0] + "]").val(controlName);
  if(theControl.Name != ""){
	  $("#bodyLayoutDetails [name="+ tableItemList[0] + "]").attr("readonly",true);
  } else {
	  $("#bodyLayoutDetails [name="+ tableItemList[0] + "]").attr("readonly",false);
  }
  
  var attributes = theControl["Attributes"];
  for(var i = 0; i < attributes.length; i ++){
		if ($("#bodyLayoutDetails [name="+ attributes[i]['Name'] + "]").attr("type") == "checkbox"){
			if(attributes[i]['Value'] == "true"){
				$("#bodyLayoutDetails [name="+ attributes[i]['Name'] + "]").prop("checked",attributes[i]['Value']) ;
			}
		}else{
			$("#bodyLayoutDetails [name="+ attributes[i]['Name'] + "]").val(attributes[i]['Value'] );
		}
  }
}

function clickattbtn(id) {
  var htmlloader = `<div class="loadding col-md-8 col-center-block">
                  <img src="https://thumbs.gfycat.com/UnitedSmartBinturong-max-1mb.gif" class="img-responsive" alt="Image" style="width: 25px; margin: 0 auto;">
              </div>`;
  $("#" + id + " .recodinput .appendAttr").append(htmlloader);
  initItemLayout(id);
  $('#' + id + ' .recodinput .loadding').fadeToggle("slow", function () {
      $(this).remove();
  });
}
function controlItem() {
	$("#bodyLayoutDetails [name="+ tableItemList[0] + "]").attr("readonly",false);
  $("#saveNewControl").removeClass("hidden");
  $("#bodyLayoutDetails")[0].reset();
  $('#modalDetailLayout').modal({
		backdrop: 'static'
	});
  $('body').addClass("modal-open");
  
}

// Save the controll from Modal to current layout
function saveNewControls(IsthisAddControl){
  if ($('#bodyLayoutDetails').valid() != true){
		return;
  }
	existEdit = true;
	var valueAttrName = $("#errorAttributeName").val();
	var valueAttrValue = $("#errorAttributeValue").val();
	if(valueAttrName != null && valueAttrName != "") {
		if(valueAttrValue == ""){
    		var $alertas = $('#bodyLayoutDetails');
    		$alertas.find('.AttrValue').addClass('has-error');
    		$alertas.find('.itemAttrValue').addClass('fa-warning').attr("data-original-title",getLang(currentLocale,"Cannot be blank!"));
    		$(".itemAttrValue").tooltip({'container': 'body'});
    		return false;
    	} else {
    		var $alertas = $('#bodyLayoutDetails');
    		$alertas.find('.AttrValue').removeClass('has-error');
    		$alertas.find('.itemAttrValue').removeClass('fa-warning').removeAttr("data-original-title");
    		 var currentLayout = getCurrentLayout();
    		  var countnum = $('.listJsons.panel.panel-default').length;
    		  countnum++;
    		//  initItemLayout(countnum);
    		  updateControls();
    		  $('#modalDetailLayout').modal("hide");
    		  var $alertas = $('#bodyLayoutDetails');
    		  $alertas.find('.form-group').removeClass('has-success');
    		  $alertas.find('.fa').removeClass('fa-check');
    		  $('body').remove("modal-open");
    		  modalLayoutHidden();

    		  generateGridLayout( currentLayout);
    	}
  } else {
	  var currentLayout = getCurrentLayout();
	  var countnum = $('.listJsons.panel.panel-default').length;
	  countnum++;
	//  initItemLayout(countnum);
	  updateControls();
	  $('#modalDetailLayout').modal("hide");
	  var $alertas = $('#bodyLayoutDetails');
	//  $alertas.validate().resetForm();
	  $alertas.find('.form-group').removeClass('has-success');
	  $alertas.find('.fa').removeClass('fa-check');
	  $('body').remove("modal-open");
	  modalLayoutHidden();

	  generateGridLayout( currentLayout);
  }
  
}

function createControlFromModal(){
	var attributes = [];
	var conditions =[];
	for (var item = 1; item < tableItemList.length ; item++){
		if ($("#bodyLayoutDetails [name="+ tableItemList[item] + "]").attr("type") == "checkbox"){
			attributes.push ({"Name": tableItemList[item], "Value": $("#bodyLayoutDetails [name="+ tableItemList[item] + "]").prop("checked").toString()});
		}else{
			attributes.push ({"Name": tableItemList[item], "Value": $("#bodyLayoutDetails [name="+ tableItemList[item] + "]").val()});
		}
	}
	
	for (var j =0; j < attributes.length; j++){
		if ((conditionList.indexOf( attributes[j]["Name"])>=0) && (attributes[j]["Value"] >"" )){
			conditions.push(attributes[j]);
			//console.log(attributes[j]);
		}
	}
	var controlName = $("#bodyLayoutDetails [name="+ tableItemList[0] + "]").val();
	
	var newControl = {
			"Name": controlName,
    	    "Condition": conditions,
    	    "Attributes": attributes	
		}
	
	
	return newControl;

}

// update or create new control from modal
function updateControls(){
		if ($('#bodyLayoutDetails').valid() != true){
			return;
		}
		var currentLayout = getCurrentLayout(null);
		var controls = currentLayout.Controls
		var newControl = createControlFromModal();

		
		var isExist =false;
		var controlName = $("#bodyLayoutDetails [name="+ tableItemList[0] + "]").val();
		for (var j = 0; j < currentLayout.Controls.length; j++){
			
			// create new controll
			if (currentLayout.Controls[j].Name == controlName){
				currentLayout.Controls[j] = newControl;
				j = currentLayout.Controls.length;
				isExist = true;
			}
		}
		
		// create new controll if not exist
		if (!isExist){
			
			controls.push(newControl);
		}
		
		
		for (var j = 0; j < currentLayout.Controls.length; j++){
			if(currentLayout.Controls[j]["Name"] == ""){
				currentLayout.Controls.splice(j,1);
			}
		}
		
		existEdit = true;
		var valueAttrName = $("#errorAttributeName").val();
    	var valueAttrValue = $("#errorAttributeValue").val();
    	if(valueAttrName != null && valueAttrName != "") {
    		if(valueAttrValue == ""){
        		var $alertas = $('#bodyLayoutDetails');
        		$alertas.find('.AttrValue').addClass('has-error');
        		$alertas.find('.itemAttrValue').addClass('fa-warning').attr("data-original-title",getLang(currentLocale,"Cannot be blank!"));
        		$(".itemAttrValue").tooltip({'container': 'body'});
        		return false;
        	} else {
        		var $alertas = $('#bodyLayoutDetails');
        		$alertas.find('.AttrValue').removeClass('has-error');
        		$alertas.find('.itemAttrValue').removeClass('fa-warning').removeAttr("data-original-title");
        		$('#modalDetailLayout').modal("hide");
        		$("#accordion").empty();
        		generateItemLayout(controls);
        		generateGridLayout( currentLayout);
        		callModalDetail(controls);
        	}
    	} else {
    		var $alertas = $('#bodyLayoutDetails');
    		$alertas.find('.AttrValue').removeClass('has-error');
    		$alertas.find('.itemAttrValue').removeClass('fa-warning').removeAttr("data-original-title");
    		$('#modalDetailLayout').modal("hide");
    		$("#accordion").empty();
    		generateItemLayout(controls);
    		generateGridLayout( currentLayout);
    		callModalDetail(controls);
    	}
    	
    	var $alertas = $('#bodyLayoutDetails');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
}


// modal layout hidden
function modalLayoutHidden(){
  
  $('#modalDetailLayout').on('hide.bs.modal', function () {
      $(this).removeAttr("data-num");
      $(this).removeAttr("data-namearr");
      $('form#bodyLayoutDetails')[0].reset();
      $('form#bodyLayoutDetails .icheckbox_flat-green').removeClass("checked");
      $('form#bodyLayoutDetails .icheckbox_flat-green input[type="checkbox"]').prop('checked', false);
      $("#updateControl").addClass("hidden");
      $("#saveNewControl").addClass("hidden");
      $("#saveNewControlToLayout").addClass("hidden");
  });
}
function hasClass(target, className) {
  return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}






//func update tab View List
$('#btnUpdateViewGrid').click(function(event){
	 saveCurrentLayout(event)
	});





// load list detail layout
// this function read a layout and draw it as a grid
function generateGridLayout(theLayout){
	if (theLayout == null){
		theLayout = getCurrentLayout();
	}
	var controls = theLayout["Controls"];
	if (controls != null) {
		$("#bodyDetailLayoutList").empty();
		$.each( controls, function(indexCtrs, valuesCtrs) {
			var AttributesAttr = valuesCtrs['Attributes'];
			var htmlTrTable =   '<tr>'
				+'<td class="box-action-btn text-center">'
				+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-info btn-copy-testSuites callDetailModal" href="javascript:void(0);" data-name="'+valuesCtrs["Name"]+'" data-numb="'+indexCtrs+'" onclick="" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,'Edit Control')+'"><i class="icon-pencil"></i></a>'
				+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default  btn-delete-testSuites btn-deleteLayout" href="javascript:void(0);" data-name="'+valuesCtrs["Name"]+'" data-numb="'+indexCtrs+'" onclick="deleteControlInLayout(\'' + valuesCtrs["Name"] +'\')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,'Delete Control')+'"><i class="fa fa-trash-o"></i></a>'
				+'</td>'
				+'<td>'+valuesCtrs['Name']+'</td>'
				if(AttributesAttr !== '' || AttributesAttr !== undefined){
					for(var i = 1; i < tableItemList.length; i++){
						var j = 0;
						for (;j < AttributesAttr.length; j++  ){
							if(AttributesAttr[j]['Name'] == tableItemList[i]) {
								htmlTrTable  = htmlTrTable + '<td>'+AttributesAttr[j]['Value']+'</td>';
								j = AttributesAttr.length + 9999;
							}
						}
						if(j >=( AttributesAttr.length + 9999)) {
							// nothing todo
						} else{
							htmlTrTable  = htmlTrTable + '<td></td>';
						}
					}
				}
				htmlTrTable  = htmlTrTable +'</tr>';
				$("#bodyDetailLayoutList").append(htmlTrTable);	
		});
	} else {
    	$("#sortable").empty();
        initItemLayout();
		$("#detailsLayout").slideToggle();
	}
	callModalDetail();
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
}

// func delete Atrribute of View list
function deleteControlInLayout(controllName) {
	var currentLayout = getCurrentLayout();
	for (var j = 0; j < currentLayout.Controls.length; j++){
		if (currentLayout.Controls[j].Name == controllName ){
			currentLayout.Controls.splice(j,1);
			j = currentLayout.Controls.length;
		}
	}
	callModalDetail();
	$('.tooltip').remove();
	generateGridLayout( currentLayout);
	existEdit = true;
}


$("#fileImportID")[0].onchange = onSelectedFileChange;
function onSelectedFileChange() {
	var uploadFiles = document.getElementById("fileImportID").files;
	if(uploadFiles[0].type == "application/vnd.ms-excel"){
		for (i = 0; i < uploadFiles.length; i++){
			processFiles(uploadFiles[i]);
			$("#errorImportScreen").text("");
		}
	    return;
	    
	} else {
		$("#errorImportScreen").text("Please select a csv file type!");
	}
}

function processFiles(file) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var output = document.getElementById("fileImportID");
        var texto = e.target.result;
        csvToJSON(texto);
    };
    reader.readAsText(file);
}

function csvToJSON(csv) {
    var result = [];
    var csvContent = csv2array(csv,",");
    var NewControls =[];
    
//    check header have duplicated column
	for (var j = 0; j < csvContent[0].length -1 ; j++){

		var index = tableItemList.indexOf( csvContent[0][j]);
		if (index >= 0){
			if (0 < csvContent[0].indexOf( csvContent[0][j] , j + 1) ){
				alert(" duplicate column:" + csvContent[0][j]);
				return;
			}
		}			
	}
    
    for (var k=1; k < csvContent.length; k++){
    	var attributes =[];
    	var controlName;
    	for (var m =0; m < csvContent[0].length; m++){
    		var index = tableItemList.indexOf(csvContent[0][m])
    		if (index > 0){
    			attributes.push({"Name": csvContent[0][m] , "Value":csvContent[k][m] })
    		}else if (index == 0){	//controlName 
    			controlName = csvContent[k][m];
    		}
    	}
    	// controlName is exist mean controll is valid
    	if (controlName.length > 0){
    		NewControls.push({
      		  "Name": controlName,
    	      "Condition": [
    	        {
    	          "Name": "",
    	          "Value": ""
    	        }
    	      ],
    	      "Attributes": attributes
    		});
    	}
    	
    }
    
    var currentLayout = getCurrentLayout();
    currentLayout["Controls"].push(...NewControls);

  	$("#accordion").empty();
  	generateItemLayout(currentLayout["Controls"]);
	generateGridLayout(currentLayout);
	callModalDetail();
}


function csv2array(data, delimeter) {
	  // Retrieve the delimeter
	  if (delimeter == undefined) 
	    delimeter = ',';
	  if (delimeter && delimeter.length > 1)
	    delimeter = ',';

	  // initialize variables
	  var newline = '\n';
	  var eof = '';
	  var i = 0;
	  var c = data.charAt(i);
	  var row = 0;
	  var col = 0;
	  var array = new Array();

	  while (c != eof) {
	    // skip whitespaces
	    while (c == ' ' || c == '\t' || c == '\r') {
	      c = data.charAt(++i); // read next char
	    }
	    
	    // get value
	    var value = "";
	    if (c == '\"') {
	      // value enclosed by double-quotes
	      c = data.charAt(++i);
	      
	      do {
	        if (c != '\"') {
	          // read a regular character and go to the next character
	          value += c;
	          c = data.charAt(++i);
	        }
	        
	        if (c == '\"') {
	          // check for escaped double-quote
	          var cnext = data.charAt(i+1);
	          if (cnext == '\"') {
	            // this is an escaped double-quote. 
	            // Add a double-quote to the value, and move two characters ahead.
	            value += '\"';
	            i += 2;
	            c = data.charAt(i);
	          }
	        }
	      }
	      while (c != eof && c != '\"');
	      
	      if (c == eof) {
	        throw "Unexpected end of data, double-quote expected";
	      }

	      c = data.charAt(++i);
	    }
	    else {
	      // value without quotes
	      while (c != eof && c != delimeter && c!= newline && c != ' ' && c != '\t' && c != '\r') {
	        value += c;
	        c = data.charAt(++i);
	      }
	    }

	    // add the value to the array
	    if (array.length <= row) 
	      array.push(new Array());
	    array[row].push(value);
	    
	    // skip whitespaces
	    while (c == ' ' || c == '\t' || c == '\r') {
	      c = data.charAt(++i);
	    }

	    // go to the next row or column
	    if (c == delimeter) {
	      // to the next column
	      col++;
	    }
	    else if (c == newline) {
	      // to the next row
	      col = 0;
	      row++;
	    }
	    else if (c != eof) {
	      // unexpected character
	      throw "Delimiter expected after character " + i;
	    }
	    
	    // go to the next character
	    c = data.charAt(++i);
	  }  
	  
	  return array;
	}

function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}


//-------------------------------------------------------------------layout-------------------------------------------------
//layout db
//func add layout
function addNewlayout(){
	 $("#errorImportScreen").text("");
	if ($('#new_layout').valid() !=true){
		return;
	}
	$("#saveNewLayout").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveNewLayout").prop("disabled", true);
	var testsuiteName = $("#lsTestsuite option:selected").text();
	var layoutName = $('#layout_name').val();
	var layoutDescription = $('#layout_description').val();
	var attrs=[];
	for (var i = 1; i < tableItemList.length; i ++){
		attrs.push({"Name": tableItemList[i] , "Value":"" })
	}
	var controls =[{
		"Name": "",
	      "Condition": [
	        {
	          "Name": "",
	          "Value": ""
	        }
	      ],
	      "Attributes": attrs
	      
	}];
	var layout = 
	{
		"Name":layoutName,
		"Description":layoutDescription,
		"Controls": controls
	}
	
	var screenDesign = {
			"projectId": projectId,
			"testsuiteName": testsuiteName,
			"layoutName": layoutName,
			"description": layoutDescription,
			"layoutContent": JSON.stringify(layout),
			"createBy": "",
			"createDate": new Date().toISOString()
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/saveLayout",
		type : "POST",
		data : JSON.stringify(screenDesign),
		contentType : "application/json; charset=UTF-8",
		success : function(result) {
			if (!(result.errormessage) ) {
				resetFormLayout();
				$("#modalAddNewLayout").modal("hide");
				$("#bodyLayoutRecord").empty();
				$("#loadingScreenDesign").show();
//				var testsuiteName = $("#filterTestsuite").val();
//				var createBy = $("#filterCreateBy").val();
//				var createDate = $("#filterCreateDate").val();
//				drawLayoutByFilter(testsuiteName, createBy, createDate);
//				drawTestsuiteReport();
				existEdit = true;
				$("#saveNewLayout").html("<i class='ti-check'></i> Save");
				$("#saveNewLayout").prop("disabled", false);
				document.getElementById("new_layout").reset();
	    	    var $alertas = $('#new_layout');
	    	    $alertas.validate().resetForm();
	    	    window.location.href = jscontext+"/project/"+projectId+"/screenDesign";
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//===================Layout Details==============================
//call get detail layout
function generateDetailLayout(isThis,layoutName){
	var testsuiteName = $(isThis).attr("testsuite-name");
	$("#bodyListLayout").hide();
	$("#sortable").empty();
	$("#titleLayout").html(layoutName);
	$("#btnGenerateItemDetails").attr("data-name",layoutName);
	$("#btnExportCSVFile").attr("data-name",layoutName);
	
	$("#btnGenerateItemDetails").attr("testsuite-name",testsuiteName);
	$("#btnExportCSVFile").attr("testsuite-name",testsuiteName);
	
	$("#indexScreenLayout").val($(isThis).attr("data-numb"));
	$("#btnExportCSVFile").attr("data-number",$(isThis).attr("data-numb"));
	
	getCurrentLayoutByName(testsuiteName, layoutName, function (theLayoutData){
		$("#testsuiteName").val(testsuiteName);
		generateIconLayout(theLayoutData);

		$('#imageLayoutDetail').empty();
		// loadImage for layout
		getImage(testsuiteName);
		
		$("#detailsLayout").slideToggle();

	})
	
	

}

function getCurrentLayoutByName(testsuiteName, layoutName, callbackF){
	
    if (layoutName == null || layoutName =="" || layoutName == undefined){
    	layoutName = $('#titleLayout').text();
    }else{
    }
	$.ajax({
		url : jscontext +"/project/" + projectId +"/"+ testsuiteName + "/getLayoutByName/"+ layoutName,
		type : "get",
		data : "",
		contentType : "application/json", 
		async: false,
		success : function(result) {
			
			if (!(result.errormessage) ) {
				glCurrentLayout = JSON.parse(result.data.layoutContent);
				callbackF(glCurrentLayout);
				
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
    
}

function generateIconLayout(theLayoutData){
	var controls = theLayoutData["Controls"];
	$("#accordion").empty();
	if (controls != null || controls.length > 0) {
		generateItemLayout(controls);
 		callModalDetail();
        modalLayoutHidden();
    }
}

//func getImage
function getImage(testsuiteName){
	var itemNameLayout= $('#titleLayout').text();
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName + "/" + itemNameLayout +"/getImage",
		type : "GET",
		contentType : "application/json",
		async: false,
		success : function(result) {
			if(result!=null && result.errormessage ==null && result.data.length>0 ){
				var html = '';
				$('#imageLayoutDetail').empty();
				drowImages(result.data, testsuiteName);
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func update tab View Gird
$('#btnUpdateViewIcon').off('click').click(function(event){
	$('#btnUpdateViewIcon').html("<i class='fa fa-circle-o-notch fa-spin'></i>" +getLang(currentLocale,"Please waiting!"));
	 saveCurrentLayout(event);
});

function saveCurrentLayout(event, testsuiteName){
	var indexScreen = $("#indexScreenLayout").val();
	var testsuiteName = $("#testsuiteName").val();
	var currentLayout = getCurrentLayout();
	
	var screenDesign = {
			"id": indexScreen,
			"projectId": projectId,
			"testsuiteName": testsuiteName,
			"layoutName": "",
			"description": "",
			"layoutContent": JSON.stringify(currentLayout),
			"createBy": "",
			"createDate": new Date().toISOString()
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/saveLayout/"+indexScreen,
		type : "POST",
		data : JSON.stringify(screenDesign),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: getLang(currentLocale,"Update Layout Success!!!"),
			        type: "success",
			        timer: 5000,
			        showConfirmButton: false
			    });
				$('#btnUpdateViewIcon').html(getLang(currentLocale,"Save Layout"));
				existEdit = false;
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});	
	
}

//func delete element of Json attribute
function deleteColJson(id, controllName) {
  console.log(id);
  $('#' + id).fadeToggle("slow", function () {
      $('#' + id).remove();
  });
  var currentLayout = getCurrentLayout();
  for (var j = 0; j < currentLayout.Controls.length; j++){
	if (currentLayout.Controls[j].Name == controllName ){
		currentLayout.Controls.splice(j,1);
			j = currentLayout.Controls.length;
	}
  }
  
  existEdit = true;
}

//func add image if you want to add new image 
$("#fileID")[0].onchange = onSelectedFileImage;
function onSelectedFileImage() {
	uploadImage(document.getElementById("fileID").files);
}


//func delete Image when not updated
function deleteTheImage(eId, imagename){
  $(eId).remove();
  for (var j =0 ; j < listImage.length ; j++){
	  if( listImage[j].name ==imagename){
			 listImage.splice(j,1);
			 j=  listImage.length;
		 } 
  }
  
  existEdit = true;
}

//func uploadImage
function uploadImage(listimage) {
	$("#uploadImages").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	var itemNameLayout= $('#titleLayout').text();
	var testsuiteName = $("#testsuiteName").val();
	if(listimage.length != 0){
		var formData = new FormData();
		for(var i = 0; i < listimage.length; i++){
			formData.append("file", listimage[i]);
		}
		$.ajax({
			type : "POST",
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			url :  jscontext+"/project/"+projectId+"/"+testsuiteName +"/"+itemNameLayout + "/uploadImage",
			data : formData,
			dataType : 'json',
			timeout : 600000,
			success : function(result) {
				if (!(result.errormessage) ) {		
					getImage(testsuiteName);
					//drowImages(result.data);
					swal({
					        title: getLang(currentLocale,"Upload Image Success!!!"),
					        type: "success",
					        timer: 1000,
					        showConfirmButton: false
					    });
					$("#uploadImages").html(getLang(currentLocale,"Upload Image"));
					existEdit = true;
				}
				
			},
			error : function(xhr,e) {
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			}
		});
	}
};


function drowImages(data, testsuiteName){
	 for(var index=0; index < data.length; index ++) {
		html = '<li class="form-group img-testcase">'
				+'<button class="closeImg" data-name="'+data[index]+'"> X</button>'
				+'<div class="containers">'
				+'<img class="img-responsive" src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/downloadLayout/'+data[index]+'">'
				+'</div>'
				+'</li>';
        $('#imageLayoutDetail').append(html);
	}
	$(function(){
		viewfulImgLayout();
	});	
	$(".closeImg").click(function() {
		var nameImg = $(this).attr("data-name");
		showConfirmMessageImage(nameImg);	
	});
}

//func show modal confirm delete image
function showConfirmMessageImage(imageName) {
  swal({
      title: getLang(currentLocale,"Are you sure to delete Image"),
      text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
      cancelButtonText: getLang(currentLocale,"Cancel"),
      closeOnConfirm: false
  }, function () {
	   deleteImage(imageName);
	   
  });
}

//delete image
function deleteImage(imageName){
	var testsuiteId = $("#testsuiteName").val();
	var itemNameLayout= $('#titleLayout').text();
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteId+"/"+itemNameLayout+"/deleteImage",
		type : "POST",
		data : imageName,
		async : false,
		contentType : "application/json",
		success : function(result) {
				$("#imageLayoutDetail").empty();
				getImage(testsuiteId);
				swal({
			        title: getLang(currentLocale,"Delete Image Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				existEdit = true;
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func export file CSV from file Json
function exportCSVFile(isThis) {
	
	var itemNameLayout = $(isThis).attr("data-name");
	var indexLayout = $(isThis).attr("data-number");
	var testsuiteName = $(isThis).attr("testsuite-name");
	$.each(layoutls, function (ind, vals) {
		if (vals["layoutName"] == itemNameLayout && vals["testsuiteName"] == testsuiteName) {
			var fileTitle = itemNameLayout;
			var controls = JSON.parse(vals.layoutContent)["Controls"];
			var jsonObject = tableItemList.map(function (value){
				return "\"" + value + "\"";
			})
			for(var j = 0; j < controls.length; j++){
		var attributes = controls[j]["Attributes"];
		jsonObject += "\n\""  + controls[j].Name + "\",";
		for(i = 1; i < tableItemList.length; i++){
			var k = 0;
			for(k=0; k < attributes.length; k++){
				
				if(attributes[k]["Name"] == tableItemList[i]){
					if(attributes[k]["Name"] == "xpath") {
						jsonObject += "\"" + attributes[k]["Value"].split('"').join("'") + "\"";
					} else{
						jsonObject += "\"" + attributes[k]["Value"] + "\"";
					}
					k = attributes.length + 9999;
				}
			}
			//if (k >= attributes.length + 9999  ){
				jsonObject += ","
			//}
		}
		$("#btnExportCSVFile").attr("data-number",ind);
	}
	
    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([jsonObject], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
		}
	});

    
}

function getAllMilestoneInLayout(callbackF){
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getAllMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				callbackF(result.data);
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: " Can get data!!!",
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}




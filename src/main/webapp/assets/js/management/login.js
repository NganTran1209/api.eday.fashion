$( document ).ready(function() {
	$("#forgotPass").click(function(){
		$("#loginForm").slideToggle();
		$(".sendMailForm").slideToggle();
	});
	$("#backLogin").click(function(){
		$("#findEmail").val("");
		$("#loginForm").slideToggle();
		$(".sendMailForm").slideToggle();
	});
});



//func validate login home page
var validateLogin = function() {
  // for more info visit the official plugin documentation: 
  // http://docs.jquery.com/Plugins/Validation

  var form2 = $('#loginFormSubmit');
  var error2 = $('.alert-danger', form2);
  var success2 = $('.alert-success', form2);
  $.validator.addMethod(
          "regex",
          function(value, element, regexp) {
              var re = new RegExp(regexp);
              return this.optional(element) || re.test(value);
          }
  );
  form2.validate({
      errorElement: 'span', //default input error message container
      errorClass: 'help-block help-block-error', // default input error message class
      focusInvalid: true, // do not focus the last invalid input
      ignore: "",  // validate all fields including form hidden input
      rules: {
    	  username: {
              required: true,
              email: true,
              maxlength: 50,
              minlength: 5,
              regex:/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          },
          password: {
              required: true,
              minlength: 5
          },
          
      },
      
      messages: {
      	username:{
      		required:getLang(currentLocale,"Cannot be blank!"),
//      		regex: "Email is invalid!!!"
			},
			password:{
				required:getLang(currentLocale,"Cannot be blank!")
			},
        },

      invalidHandler: function (event, validator) { //display error alert on form submit              
          success2.hide();
          error2.show();
      },

      errorPlacement: function (error, element) { // render error placement for each input type
          var icon = $(element).parent('.input-icon').children('i');
          icon.removeClass('fa-check').addClass("fa-warning");  
          icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
         
      },

      highlight: function (element) { // hightlight error inputs
          $(element)
              .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
          tooltipValidation();
      },
      success: function (label, element) {
      	
          var icon = $(element).parent('.input-icon').children('i');
          $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
          icon.removeClass("fa-warning").addClass("fa-check");
          tooltipValidation();
      },

      submitHandler: function (form) {
          success2.show();
          error2.hide();
          signin();
  	    var $alertas = $('#loginFormSubmit');
  	    $alertas.validate().resetForm();
  	    $alertas.find('.form-group').removeClass('has-success');
  	    $alertas.find('.fa').removeClass('fa-check');
      }
      
  });
  return {
  //main function to initiate the module
      init: function () {

          handleValidation1();
          handleValidation2();
      }
  }
}

//func validate send email
var validateSendEmail = function() {
  // for more info visit the official plugin documentation: 
  // http://docs.jquery.com/Plugins/Validation

  var form2 = $('#findYourEmail');
  var error2 = $('.alert-danger', form2);
  var success2 = $('.alert-success', form2);
  $.validator.addMethod(
          "regex",
          function(value, element, regexp) {
              var re = new RegExp(regexp);
              return this.optional(element) || re.test(value);
          }
  );
  form2.validate({
      errorElement: 'span', //default input error message container
      errorClass: 'help-block help-block-error', // default input error message class
      focusInvalid: true, // do not focus the last invalid input
      ignore: "",  // validate all fields including form hidden input
      rules: {
    	  findEmail: {
              required: true,
              email: true,
              maxlength: 50,
              minlength: 5,
              regex:/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          }
      },
      
      messages: {
    	  findEmail:{
      		required:getLang(currentLocale,"Cannot be blank!"),
			},
        },

      invalidHandler: function (event, validator) { //display error alert on form submit              
          success2.hide();
          error2.show();
      },

      errorPlacement: function (error, element) { // render error placement for each input type
          var icon = $(element).parent('.input-icon').children('i');
          icon.removeClass('fa-check').addClass("fa-warning");  
          icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
         
      },

      highlight: function (element) { // hightlight error inputs
          $(element)
              .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
          tooltipValidation();
      },
      success: function (label, element) {
      	
          var icon = $(element).parent('.input-icon').children('i');
          $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
          icon.removeClass("fa-warning").addClass("fa-check");
          tooltipValidation();
      },

      submitHandler: function (form) {
          success2.show();
          error2.hide();
          sendMaiForgotPass();
  	    var $alertas = $('#findYourEmail');
  	    $alertas.validate().resetForm();
  	    $alertas.find('.form-group').removeClass('has-success');
  	    $alertas.find('.fa').removeClass('fa-check');
      }
      
  });
  return {
  //main function to initiate the module
      init: function () {

          handleValidation1();
          handleValidation2();
      }
  }
}

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}


function signin(){
	$("#btnLoginAcc").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#btnLoginAcc").prop("disabled", true);
	var loginData = {
			username:$("input[name='username']").val(),
			password:$("input[name='password']").val()
	};
	$.ajax({
		url : jscontext+"/auth/signin",
		type : "POST",
		data : JSON.stringify( loginData),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalLogin").modal("hide");
				$.ajax({
					url:jscontext+"/auth/me",
					type:"GET",
					headers:{
						"Authorization":"Bearer " + result.data.token
					},
					success: function(){
						if(window.location.href.split("/").length <= 5){
							window.location= jscontext+"/project";
						} else {
							if((window.location.href).includes("/login")){
								window.location= jscontext+"/project";
							} else{
								location.reload();
							}
						}
						
						
					}
				})
			} else {
			}
		},
		error : function(e) {
			console.log("ERROR: ", e);
			$("#btnLoginAcc").html("Login");
			$("#btnLoginAcc").prop("disabled", false);
			swal({
		        title:getLang(currentLocale,"Email and password are not correct!"),
		        text: getLang(currentLocale,"Please enter again!"),
		        type: "warning",
		        timer: 2500,
		        showConfirmButton: false
		    });
		},
		done : function(e) {
			console.log("DONE");
			$("#btnLoginAcc").html("Login");
			$("#btnLoginAcc").prop("disabled", false);
			swal({
		        title: getLang(currentLocale,"Email and password are not correct!"),
		        text: getLang(currentLocale,"Please enter again!"),
		        type: "warning",
		        timer: 2500,
		        showConfirmButton: false
		    });
			
			window.location= jscontext + "/login";
		}
	});
}


// func send email reset password
function sendMaiForgotPass(){
	$("#btnSendMailFPass").html("<i class='fa fa-circle-o-notch fa-spin'></i>"+ getLang(currentLocale,"Please waiting!"));
	$("#btnSendMailFPass").prop("disabled", true);
	var emailName = {
		"email": $("#findEmail").val()
	}
	$.ajax({
		url : jscontext+"/sendMailForgotPass",
		type : "POST",
		data : JSON.stringify(emailName),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#btnSendMailFPass").html('Send Mail');
				$("#btnSendMailFPass").prop("disabled", false);
				swal({
			        title: getLang(currentLocale,"Your request has been processed.")+"\n"
			             + getLang(currentLocale,"Please check your email to take the next step!"),
			        type: "success",
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: false
			    });
			}
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
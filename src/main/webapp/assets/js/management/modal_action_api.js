var testDatas = "";
$(document).ready(function(){
	validateFormApi();
	getDataFromTestData();
});
jQuery.validator.setDefaults({
	  debug: true,
	  success: "valid"
	});
function deleteRowTable(){
	$(".btn-deleteRowTable").click(function(){
		$(this).parent().closest("tr").remove();
		
	});
}

$("#selectTypeAuth").on("change",function(){
	var valueTypeAuth = $(this).val();
	if (valueTypeAuth == "none"){
		$(".form-element-bg").hide();
		$("#form-basic-auth").hide();
		$("#elementRequest").hide();
		$(".btn-preview-request").hide();
		
	}else if(valueTypeAuth == "basicAuth"){
		
		$(".form-element-bg").hide();
		$("#form-basic-auth").show();
		$("#elementRequest").hide();
		$(".btn-preview-request").show();
		
	}else if(valueTypeAuth == "apiKey"){
		
		$(".form-element-bg").hide();
		$("#form-api-key").show();
		$("#elementRequest").hide();
		$(".btn-preview-request").hide();
		
	}else if(valueTypeAuth == "bearerToken"){
		
		$(".form-element-bg").hide();
		$("#form-bearer-token").show();
		$("#elementRequest").hide();
		$(".btn-preview-request").show();
		
	}else if(valueTypeAuth == "oauth2"){
		
		$(".form-element-bg").hide();
		$("#form-oauth2").show();
		$("#elementRequest").show();
		$(".btn-preview-request").show();
		
	}else if (valueTypeAuth == "awsSignature"){
		
		$(".form-element-bg").hide();
		$("#form-aws").show();
		$("#elementRequest").hide();
		$(".btn-preview-request").show();
		
	}
 });
 
function getDataFromTestData() {
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+glTestsuiteId+"/getTestData",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				testDatas = result.data;
 			}
      },
      error : function(request,error)
      {
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
//				swal({
//					title: getLang(currentLocale,"Can not get list testdata!!!"),
//			        text: getLang(currentLocale,"Please sync project!!!"),
//			        type: "warning", 
//			        timer: 2000,
//			        showConfirmButton: false
//			    });
			}
      }
    });
}
 
$("#bodyRequestNone").click(function(){
	$(".bodyOfTypeRequest").hide();
	$("#requestBodyNone").show();
	$("#addRowMultiLine").hide();
});

$("#bodyRequestFormData").click(function(){
	$(".bodyOfTypeRequest").hide();
	$("#requestFormData").show();
	$("#addRowMultiLine").show();
});

$("#bodyRequestRaw").click(function(){
	$(".bodyOfTypeRequest").hide();
	$("#api_body_requestRaw").show();
	$("#addRowMultiLine").hide();
});

$("#bodyRequestBinary").click(function(){
	$(".bodyOfTypeRequest").hide();
	$("#requestBodyBinary").show();
	displaySelectFile();
	$("#addRowMultiLine").hide();
});

function displaySelectFile(){
	var divSelect = $("#requestBodyBinary").find(".binaryClass");
	$(divSelect).empty();
	var html = '<select name="binaryType" class=" form-control">'
	$.each(testDatas, function(index, values){
		if (values.layoutData == false){
			html += '<option value="'+values.filename+'">'+values.filename+'</option>'
		}
	});
	html += '</select>';
	$(divSelect).append(html);
}

$("#fileUploadRequestBody").on("change", function(){
	if($(this).val() != null){
		var files = $('#fileUploadRequestBody')[0].files;
		var nameFile = $(this).parents('.upload-btn-wrapper').find('.nameFileUpload');
		var html =""
		for (var i =0; i < files.length; i++){
			html +=  '<i class="icon-doc mr-1"></i>'+files[i].name +'<br>';
		}
		nameFile.html(html);
		$(this).hide();
		$("#btnUploadRequestBody").hide();
		$("#resetUploadFileRsBody").show();
	}
});

$("#resetUploadFileRsBody").click(function(){
	$("#fileUploadRequestBody").val("");
	$("#fileUploadRequestBody").show();
	$("#resetUploadFileRsBody").hide();
	$(this).closest(".upload-btn-wrapper").find(".nameFileUpload").text("");
	$("#btnUploadRequestBody").show();
});


$("#addRowQueryParams").click(function(){
	addOneRowParams("","","");
});

function addOneRowParams(key,value,description){
	var htmlRowText ='<tr>'
		+'<td class="text-center">'
		+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>'
		+'</td>'
		+'<td>'
		+'<input type="text" name="Key"  class="form-control" placeholder="'+getLang(currentLocale,'Key')+'">'
		+'</td>'
		+'<td>'
		+'<input type="text" name="Value" class="form-control" placeholder="'+getLang(currentLocale,'Value')+'">'
		+'</td>'
		+'<td>'
		+'<input type="text" name="Description" class="form-control" placeholder="'+getLang(currentLocale,'Description')+'">'
		+'</td>'
		+'</tr>';
	$("table.queryParamsTable tbody").append(htmlRowText);
	var lastRow = $("table.queryParamsTable tbody tr").last();
	lastRow.find('input[name="Key"]').val(key);
	lastRow.find('input[name="Value"]').val(value);
	lastRow.find('input[name="Description"]').val(description);
	deleteRowTable();
}

$("#addRowHeaders").click(function(){
	addOneRowHead("","","");
	deleteRowTable();
});
//
//$("#bodyRqFormData").click(function(){
//	
//}); 

function addOneRowHead(key, value,description){
	var htmlRowText ='<tr>'
		+'<td class="text-center">'
		+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>'
		+'</td>'
		+'<td>'
		+'<input type="text"  class="form-control" placeholder="'+getLang(currentLocale,'Key')+'" name="Key">'
		+'</td>'
		+'<td>'
		+'<input type="text"  class="form-control" placeholder="'+getLang(currentLocale,'Value')+'" name="Value">'
		+'</td>'
		+'<td>'
		+'<input type="text"  class="form-control" placeholder="'+getLang(currentLocale,'Description')+'">'
		+'</td>'
		+'</tr>';
	$("#tableHeadersParams tbody").append(htmlRowText);
	var lastRow = $("#tableHeadersParams tbody tr").last();
	lastRow.find('input[name="Key"]').val(key);
	lastRow.find('input[name="Value"]').val(value);
	lastRow.find('input[name="Description"]').val(description);
	deleteRowTable();
}
//
//$("#addRowText").click(function(){
//	var htmlRowText ='<tr>'
//					+'<td class="text-center" width="6%">'
//					+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>'
//					+'</td>'
//					+'<td width="32%">'
//					+'<input type="text" name="actionKey1" class="form-control" placeholder="Key">'
//					+'</td>'
//					+'<td width="32%">'
//					+'<input type="text" name="actionValue1" class="form-control" placeholder="Value">'
//					+'</td>'
//					+'<td width="32%">'
//					+'<input type="text" name="actionDescription1" class="form-control" placeholder="Description">'
//					+'</td>'
//					+'</tr>';
//	$("#requestFormData table tbody").append(htmlRowText);
//	deleteRowTable();
//});
//
//$("#addRowFile").click(function(){
//	var htmlRowText ='<tr>'
//					+'<td class="text-center" width="6%">'
//					+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>'
//					+'</td>'
//					+'<td width="32%">'
//					+'<input type="text" name="actionKey1" class="form-control" placeholder="Key">'
//					+'</td>'
//					+'<td width="32%">'
//					+'<input type="file" name="actionValue1" class="form-control" >'
//					+'</td>'
//					+'<td width="32%">'
//					+'<input type="text" name="actionDescription1" class="form-control" placeholder="Description">'
//					+'</td>'
//					+'</tr>';
//	$("#requestFormData table tbody").append(htmlRowText);
//	deleteRowTable();
//});

$("#addRowMultiLine").click(function(){
		addRowToBody("","text","","");
});

function addRowToBody(key,type, value,description){
	var htmlRowText ='<tr >'
					+'<td class="text-center" width="6%">'
					+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>'
					+'</td>'
					+'<td width="25%">'
					+'<input type="text" name="Key" class="form-control" placeholder="'+getLang(currentLocale,"Key")+'">'
					+'</td>'
					+ '<td width="19%">'
					+'<select name="Type" class=" form-control selectChooseType">'
					+'<option value="file">File path</option>'
					+'<option value="text">'+getLang(currentLocale,"Text value")+'</option>' 
					+'</select>'
					+'</td>'
					+'<td width="25%" class="valueChooseType">'
					+'<textarea name="Value" class="form-control"  placeholder='+getLang(currentLocale,"Value")+'></textarea>'
					+'</td>'
					+'<td width="25%">'
					+'<input type="text" name="Description" class="form-control" placeholder='+getLang(currentLocale,"Description")+'>'
					+'</td>'
					+'</tr>';
	$("#requestFormData table tbody").append(htmlRowText);
	var lastRow = $("#requestFormData table tbody tr").last();
	lastRow.find("input[name='Key']").val(key);
	lastRow.find("select").val(type);
	lastRow.find("textarea[name='Value']").val(value);
	lastRow.find("text[name='Description']").val(description);
	changeTypeSelect(key,type, value, description);
	deleteRowTable();
}

function changeTypeSelect(key, type, value, description) {
	if(type != "text" && type != "") {
		var trCloset = $("#requestFormData table tbody tr").last();
		var tdFind = trCloset.find(".valueChooseType");
		
		$(tdFind).empty();
		var html = '<select name="formDataType" class=" form-control">'
		$.each(testDatas, function(index, values){
			if (values.layoutData == false){
				html += '<option value="'+values.filename+'">'+values.filename+'</option>'
			}
		});
		html += '</select>';
		$(tdFind).append(html);
		var lastRow = $("#requestFormData table tbody tr").last();
		lastRow.find("input[name='Key']").val(key);
		lastRow.find("select").val(type);
		lastRow.find("select[name='formDataType']").val(value);
		lastRow.find("text[name='Description']").val(description);
	}
	$(".selectChooseType").change(function(){
		var trCloset = $(this).closest("tr");
		var chooseType = $(trCloset).find(".selectChooseType").val();
		var tdFind = $(trCloset).find(".valueChooseType");
		if (chooseType == "text") {
			$(tdFind).empty();
			$(tdFind).append('<textarea name="Value" class="form-control"  placeholder='+getLang(currentLocale,"Value")+'></textarea>');
		} else {
			$(tdFind).empty();
			var html = '<select name="formDataType" class=" form-control">'
			$.each(testDatas, function(index, values){
				if (values.layoutData == false){
					html += '<option value="'+values.filename+'">'+values.filename+'</option>'
				}
			});
			html += '</select>';
			$(tdFind).append(html);
		}
		
	});

}

$("#testActionAPI").click(function(){
	$("#testActionAPI").html("<i class='ti-search mr-1'></i>"+getLang(currentLocale,"TESTING"));
	$("#testActionAPI").prop('disabled', true);
	$('#collapseResponseTest').collapse("show");
	
	var apiScript = new apiScriptSample();
	apiScript.Params[0]["Value"] = $("#apiMethod").val();
	apiScript.Params[1]["Value"] = $("#apiUrl").val();
	var varOuput = $("#outputVariable").val();
	if(!varOuput.includes("#")){
		varOuput = "#"+$("#outputVariable").val();
	}
	apiScript.Params[6]["Value"]=varOuput;
	saveParams(apiScript);
	saveAuthorization(apiScript);
	saveHeader(apiScript);
	saveBody(apiScript);

	var dataRequestRest = {
	"path" : $(".username").text() +"/"+ glProjectId +"/" + glTestsuiteId,
	"data": apiScript,
};
	
	$.ajax({
	url : jscontext+"/rest",
	type : "POST",
	data : JSON.stringify(dataRequestRest),
	contentType : "application/json",
	success : function(result) {
		$("#testActionAPI").html("<i class='ti-search mr-1'></i>"+getLang(currentLocale,"TEST"));
		$("#testActionAPI").prop('disabled', false);
		$("#resposeTextArea").val(result.data.responseBody);
		
		var obj = JSON.parse(result.data.responseHeaders);
		$("#responseHeaderTBody").empty();
		for (var key in obj) {
		    if (obj.hasOwnProperty(key)) {
		    	var htmlRowText ='<tr><td>'+ key +'</td><td>'+ obj[key]+'</td></tr>';
		    	$("#responseHeaderTBody").append(htmlRowText);
		    }
		}
		$("#responseStatusCode").html(getLang(currentLocale,"Status Code")+"  :" + result.data.statusCode);
	},
	error : function(request,error)
      {	
		$("#testActionAPI").html("<i class='ti-search mr-1'></i>"+getLang(currentLocale,"TEST"));
		$("#testActionAPI").prop('disabled', false);
			console.log("Error: "+error);
			
      }
    });
	
});

$("#closeResponseAPI").click(function(){
	$('#collapseResponseTest').collapse("hide");
});

$("#saveAPI").click(function(){
	$("#notificationSave").text("");
	saveAPI2Json();
});

$("#apiUrl").off('change').change(function(){
	$("#notificationSave").text("");
});

function saveAPI2Json(){
	if ($('#form-modal-api').valid() !=true){
		return;
	}
	
	var apiScript = new apiScriptSample();
	apiScript.Params[0]["Value"] = $("#apiMethod").val();
	apiScript.Params[1]["Value"] = $("#apiUrl").val();
	var varOuput = $("#outputVariable").val();
	if(!varOuput.includes("#")){
		varOuput = "#"+$("#outputVariable").val();
	}
	apiScript.Params[6]["Value"] = varOuput;
	saveParams(apiScript);
	saveAuthorization(apiScript);
	saveHeader(apiScript);
	saveBody(apiScript);
	console.log(apiScript);
	var textareaId = "#" + $("#modalActionAPI").attr("fromtextAreaId");
	$(textareaId).val(JSON.stringify(apiScript,undefined, 4));
	$("#notificationSave").text(getLang(currentLocale,"Save Success!!!"));
	return apiScript;
}

function saveParams(apiScript){
	var rows = $("table.queryParamsTable tbody tr");
	apiScript["Params"][2]["Value"]=[];
	for (var i =0; i < rows.length; i++){
		var theRow = $(rows[i])
		if (theRow.find('input[name="Key"]').val().trim().length >0){
			apiScript["Params"][2]["Value"].push( {
				"Key" : theRow.find('input[name="Key"]').val(),
				"Value": theRow.find('input[name="Value"]').val(),
				"Description": theRow.find('input[name="Description"]').val()
			});			
		}
	}
	
}

function saveAuthorization(apiScript){
	var type = $("#selectTypeAuth").val();
	apiScript["Params"][3]["Value"]["Type"]= type;
	
	if (type =="none"){
		apiScript["Params"][3]["Value"]["Value"]={};
		
	}else if(type =="basicAuth"){
		apiScript["Params"][3]["Value"]["Value"]={"username": $("#api_auth_basic_username").val(),
												"password":$("#api_auth_basic_password").val() };
	}else if(type =="apiKey"){
		apiScript["Params"][3]["Value"]["Value"]={"key": $("#api_auth_apiKey_Key").val(),
				"value":$("#api_auth_apiKey_Value").val(), "addTo": $("#api_auth_apiKey_AddTo").val() };
		
	}else if(type =="bearerToken"){
		apiScript["Params"][3]["Value"]["Value"]={"token": $("#api_auth_bearerToken_Token").val() };
		
	}else if(type =="oauth2"){
		apiScript["Params"][3]["Value"]["Value"]={"accessToken": $("#api_auth_oauth2_AccessToken").val()};
		
	}else if(type =="awsSignature"){
		//TODO:
		apiScript["Params"][3]["Value"]["Value"]={
				"AccessKey": $("#api_auth_s3_AccessKey").val(),
				"SecretKey":$("#api_auth_s3_SecretKey").val(), 
				"AWSRegion": $("#awsregion").val(),
				"ServiceName": $("#api_auth_s3_ServiceName").val(),
				"SessionToken": $("#api_auth_s3_SessionToken").val()};		
	}

	
}


function saveHeader(apiScript){
	var rows = $("#tableHeadersParams tbody tr");
	apiScript["Params"][4]["Value"]=[];
	for (var i =0; i < rows.length; i++){
		var theRow = $(rows[i])
		if (theRow.find('input[name="Key"]').val().trim().length > 0) {
			apiScript["Params"][4]["Value"].push( {
				"Key" : theRow.find('input[name="Key"]').val(),
				"Value": theRow.find('input[name="Value"]').val(),
				"Description": theRow.find('input[name="Description"]').val()
			});
		}
	}
	
}

function saveBody(apiScript){
	var bodyType = $("#modalActionAPI input[name='bodyRequest'][type='radio']:checked").val();
	apiScript["Params"][5]["Value"]["Type"] = bodyType;
	apiScript["Params"][5]["Value"]["Value"]=[];
	if (bodyType=="none"){
		apiScript["Params"][5]["Value"]["Value"]="";
	}else if (bodyType=="form-data") {
		var rows = $("#requestFormData table tbody tr");
		var  value=[];
		for (var i =0; i < rows.length; i++){
			var theRow = $(rows[i]);
			if (theRow.find("input[name='Key']").val()){
				if(theRow.find("select").val() == "text") {
					value.push( {
						"Key" : theRow.find("input[name='Key']").val(),
						"Type": theRow.find("select").val(),
						"Value":theRow.find("textarea[name='Value']").val(),
						"Description":theRow.find("text[name='Description']").val()
					});
				} else {
					value.push( {
						"Key" : theRow.find("input[name='Key']").val(),
						"Type": theRow.find("select").val(),
						"Value":theRow.find("select[name='formDataType']").val(),
						"Description":theRow.find("text[name='Description']").val()
					});
				}
				
			}
		}
		apiScript["Params"][5]["Value"]["Value"]= value;
	} else if (bodyType=="raw") {
		apiScript["Params"][5]["Value"]["Value"]= isJson($("#api_body_requestRaw").find("textarea").val());
	} else if (bodyType == "binary") {
		apiScript["Params"][5]["Value"]["Value"] = $("select[name='binaryType']").val();
	}

}

$('#modalActionAPI').on('hidden.bs.modal', function () {
	var $alertas = $('#form-modal-api');
    $alertas.find('.form-group').removeClass('has-error');
    $alertas.find('.fa').removeClass('fa-warning');
    $alertas.find('.fa').removeClass('fa-check');
	$("#api_body_requestRaw").find("textarea").val("");
	
	$("#selectTypeAuth").val("none");
	$("#form-basic-auth").hide();
	$("#form-api-key").hide();
	$("#form-bearer-token").hide();
	$("#form-oauth2").hide();
	$("#form-aws").hide();
	
	$("#api_auth_basic_username").val("");
	$("#api_auth_basic_password").val("");
	$("#api_auth_apiKey_Key").val("");
	$("#api_auth_apiKey_Value").val("");
	$("#api_auth_bearerToken_Token").val("");
	$("#api_auth_oauth2_AccessToken").val("");
	$("#api_auth_s3_AccessKey").val("");
	$("#api_auth_s3_SecretKey").val("");
	$("#api_auth_s3_AWSRegion").val("");
	$("#api_auth_s3_ServiceName").val("");
	$("#api_auth_s3_SessionToken").val("");
	$("#notificationSave").text("");
	$("#resposeTextArea").val("");
	$('#collapseResponseTest').collapse("hide");
});

function bindingDataToAPIModal(apiScript){
	if (!apiScript || !apiScript["Params"]){
		apiScript = new apiScriptSample();
	}
	$("#apiMethod").val(apiScript.Params[0].Value);
	$("#apiUrl").val(apiScript.Params[1].Value);
	$("#outputVariable").val(apiScript.Params[6]["Value"]);
	bindingParamsToModal(apiScript);
	bindingAuthorizationToModal(apiScript);
	bindingHeaderToModal(apiScript);
	bindingBodyToModal(apiScript);
	$("#modalActionAPI").modal("show");
}

function bindingParamsToModal(apiScript){
	var params={};
	$("table.queryParamsTable tbody").html("");
	if (apiScript && apiScript["Params"]){
		for(var i= 0 ; i < apiScript["Params"].length ; i++){
			if (apiScript["Params"][i]["Name"]=="Params"){
				params = apiScript["Params"][i];
				for(var j= 0 ; j < params["Value"].length ; j++){
					addOneRowParams(params["Value"][j]["Key"],params["Value"][j]["Value"],params["Value"][j]["Description"]);
				}
				i=apiScript["Params"].length;
			}
		}		
	}

	addOneRowParams("","","");
}


function bindingAuthorizationToModal(apiScript){
	
	var type = apiScript["Params"][3]["Value"]["Type"];
	
	checkDisplayAuthorization(type);
	
	if (type =="none") {
		console.log("none");
		
	} else if (type =="basicAuth") {
		$("#api_auth_basic_username").val(apiScript["Params"][3]["Value"]["Value"]["username"]);
		$("#api_auth_basic_password").val(apiScript["Params"][3]["Value"]["Value"]["password"]);
	} else if (type =="apiKey") {
		 $("#api_auth_apiKey_Key").val(apiScript["Params"][3]["Value"]["Value"]["key"]);
		 $("#api_auth_apiKey_Value").val(apiScript["Params"][3]["Value"]["Value"]["value"]);
		 $("#api_auth_apiKey_AddTo").val(apiScript["Params"][3]["Value"]["Value"]["addTo"]);
		
	} else if (type =="bearerToken") {
		$("#api_auth_bearerToken_Token").val(apiScript["Params"][3]["Value"]["Value"]["token"]);
		
	} else if (type =="oauth2") {
		$("#api_auth_oauth2_AccessToken").val(apiScript["Params"][3]["Value"]["Value"]["accessToken"]);
		
	} else if (type =="awsSignature") {
		$("#api_auth_s3_AccessKey").val(apiScript["Params"][3]["Value"]["Value"]["AccessKey"]);
		$("#api_auth_s3_SecretKey").val(apiScript["Params"][3]["Value"]["Value"]["SecretKey"]);
		$("#awsregion").val(apiScript["Params"][3]["Value"]["Value"]["AWSRegion"]);
		$("#api_auth_s3_ServiceName").val(apiScript["Params"][3]["Value"]["Value"]["ServiceName"]);
		$("#api_auth_s3_SessionToken").val(apiScript["Params"][3]["Value"]["Value"]["SessionToken"]);
			
	}
	
	var authorization;
	if (authorization && authorization["Value"]&& authorization["Value"]["Type"]){
		$("#selectTypeAuth").val(authorization["Value"]["Type"]);
		$("#selectTypeAuth").trigger("change");
	}

	
//
//	for(var i= 0 ; i < params["Value"].length ; i++){
//		addOneRowParams(params["Value"]["Key"],params["Value"]["Value"],params["Value"]["Description"]);
//	}
}

function checkDisplayAuthorization(type) {
	if (type == "none") {
		$("#selectTypeAuth").val("none");
		$("#form-basic-auth").hide();
		$("#form-api-key").hide();
		$("#form-bearer-token").hide();
		$("#form-oauth2").hide();
		$("#form-aws").hide();
	} else if (type == "basicAuth") {
		$("#selectTypeAuth").val("basicAuth");
		$("#form-basic-auth").show();
		$("#form-api-key").hide();
		$("#form-bearer-token").hide();
		$("#form-oauth2").hide();
		$("#form-aws").hide();
	} else if (type == "apiKey") {
		$("#selectTypeAuth").val("apiKey");
		$("#form-basic-auth").hide();
		$("#form-api-key").show();
		$("#form-bearer-token").hide();
		$("#form-oauth2").hide();
		$("#form-aws").hide();
	} else if (type == "bearerToken") {
		$("#selectTypeAuth").val("bearerToken");
		$("#form-basic-auth").hide();
		$("#form-api-key").hide();
		$("#form-bearer-token").show();
		$("#form-oauth2").hide();
		$("#form-aws").hide();
	} else if (type == "oauth2") {
		$("#selectTypeAuth").val("oauth2");
		$("#form-basic-auth").hide();
		$("#form-api-key").hide();
		$("#form-bearer-token").hide();
		$("#form-oauth2").show();
		$("#form-aws").hide();
	} else if (type == "awsSignature") {
		$("#selectTypeAuth").val("awsSignature");
		$("#form-basic-auth").hide();
		$("#form-api-key").hide();
		$("#form-bearer-token").hide();
		$("#form-oauth2").hide();
		$("#form-aws").show();
	}
}

function bindingHeaderToModal(apiScript){
	var headers;
	$("#tableHeadersParams tbody").html("");
	if (apiScript && apiScript["Params"]){
		for(var i= 0 ; i < apiScript["Params"].length ; i++){
			if (apiScript["Params"][i]["Name"]=="Header"){
				headers = apiScript["Params"][i];
				i=apiScript["Params"].length;
				for(var j= 0 ; j < headers["Value"].length ; j++){
					addOneRowHead(headers["Value"][j]["Key"],headers["Value"][j]["Value"],headers["Value"][j]["Description"]);
				}
			}
		}		
	}
	//params["Value"].push({"Key":"","Value":"","Description":""});


	addOneRowHead("","","");
	
}
function bindingBodyToModal(apiScript){
	$("#requestFormData table tbody").html("");
	var bodyType = apiScript["Params"][5]["Value"]["Type"];
	
	$("#modalActionAPI input[name='bodyRequest'][type='radio'][value='"+ bodyType+"']").prop("checked",true);
	checkDisplayTabBody(bodyType);
	
	if (bodyType=="none"){
		apiScript["Params"][5]["Value"]["Value"]="";
	} else if (bodyType=="form-data") {
		
		var  value= apiScript["Params"][5]["Value"]["Value"];
		for (var i =0; i < value.length; i++){
			if (value[i]["Key"]){
				addRowToBody(value[i]["Key"], value[i]["Type"], value[i]["Value"], value[i]["Description"]);
			}
		}
		
	} else if (bodyType=="raw") {
		$("#api_body_requestRaw").find("textarea").val(JSON.stringify(apiScript["Params"][5]["Value"]["Value"], undefined, 4));
	} else if (bodyType == "binary") {
		$("select[name='binaryType']").val(apiScript["Params"][5]["Value"]["Value"]);
	}
	
	addRowToBody("","text","","");
	
}

function checkDisplayTabBody(bodyType){
	if (bodyType == "none") {
		$("#requestBodyNone").show();
		$("#requestFormData").hide();
		$("#api_body_requestRaw").hide();
		$("#requestBodyBinary").hide();
	} else if (bodyType == "form-data") {
		$("#requestBodyNone").hide();
		$("#requestFormData").show();
		$("#api_body_requestRaw").hide();
		$("#requestBodyBinary").hide();
	} else if (bodyType == "raw") {
		$("#requestBodyNone").hide();
		$("#requestFormData").hide();
		$("#api_body_requestRaw").show();
		$("#requestBodyBinary").hide();
	} else if (bodyType == "binary") {
		$("#requestBodyNone").hide();
		$("#requestFormData").hide();
		$("#api_body_requestRaw").hide();
		$("#requestBodyBinary").show();
	}
}

var apiScriptSample= function() {
	
	return {
        "Params": [
            {
                "Name": "Method",
                "Value": "get"
            },
            {
                "Name": "url",
                "Value": ""
            },
            {
                "Name": "Params",
                "Value": [
//                    {
//                        "Key": "",
//                        "Value": ""
//                    }
                ]
            },
            {
                "Name": "Authorization",
                "Value": {
                	  "Type": "none",
                	  "Value": []
//                    "Type": "bearerToken",
//                    "Value": [
//                        {
//                            "Key": "token",
//                            "Value": ""
//                        }
//                    ]
                    // "Type": "Basic", value: [ {"key":"username", "value":"aaaaaaaaaaaaaaaaaa"}
                    // {"key":"password", "value":"password123"}]
                }
            },
            {
                "Name": "Header",
                "Value": [
//                    {
//                        "Key": "",
//                        "Value": ""
//                    },
                ]
            },
            {
                "Name": "Body",
                "Value": {
                    "Type": "none",
                    "Value": {},
                    // "Type": "formData", value: [ {"key":"username", "type": "file", "value":"filepath"}
                    // {"key":"password", "type": "text" "value":"password123"}],
                    // "Type": "raw", value: "string json here",
                }
            },
            //Return to a json value into #variableName
            {
                "Name": "output",
                "Value": ""
            }
        ]
	}
}


//func validate form api
var validateFormApi = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#form-modal-api');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        rules: {
        	urlrequestApi: {
        		required: true,
//        		url: true
            },
        },     
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            //tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
    	    var $alertas = $('#form-modal-api');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
    	    return true;
        }
        
    });
}
$( document ).ready(function() {
	$( "#callProjectList" ).click(function() {
		location.reload();// Display project information after joined the project
		});
	if(productTypeList != ""){
		var lsProductType = [];
		$.each(productTypeList, function(ind, vals){
			var itemIssue = {
				value: vals.farmProduceSeedName ,
			    label: vals.farmProduceSeedName,
			    desc: vals.farmProduceSeedName,
			}
			lsProductType.push(itemIssue);
		});
		autocompleteProductType(lsProductType);
	}
});

function autocompleteProductType(lsProductType){
	$( "#productType" ).autocomplete({
	      minLength: 0, 
	      source: lsProductType,
	      appendTo : "#modalAddNewProject .modal-body",
	      focus: function( event, ui ) {
	         $( "#productType" ).val( ui.item.label );
	            return false;
	      },
	      select: function( event, ui ) {
	         $( "#productType" ).val( ui.item.label );
	         return false;
	      }
	   })
				
	   .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	      .append( "<a>" + item.value + "</a>" )
	      .appendTo( ul );
	   };
}

$("#nameProject").on("change", function(){
	$("#productName").val($("#nameProject").val());
});

var projects = [];
var listImagesProject =[];
var listImgEditProject = [];
//func validation add new project
var validateAddInvited = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#new_project');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }
    );
    $.validator.addMethod("checkExistProductCode",function(value, element, regexp) {
    	var $alertas = $('#new_project');
    	var productCode = $("#productCode").val();
    	var checkBoolean = true;
    	if(lsProject.length > 0){
    		$.each(lsProject, function(ind,vals){
	    		if(vals.productCode == productCode){
	    			checkBoolean = false;
	    		} 
	    	});
    	} else {
    		return checkBoolean;
    	}
    	
    	return checkBoolean;
    });
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	nameProject: {
            	required:true,
            	minlength: 1,
            },
            productCodes: {
            	required: true,
            	checkExistProductCode: true,
            },
            planStDa: {
            	required: true,
            },
            planEnDa: {
            	required: true,
            }
        },
        
        messages: {
        	name: {
        		required:getLang(currentLocale,"Cannot be blank!"),
        	},
        	productCodes: {
        		required:getLang(currentLocale,"Cannot be blank!"),
        		checkExistProductCode: "Product Code already exists!"
        	},
        	planStDa: {
            	required: getLang(currentLocale,"Cannot be blank!"),
            },
            planEnDa: {
            	required: getLang(currentLocale,"Cannot be blank!"),
            }
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            
            //func check projectId is exist
            var newProjectName = $("#projectId").val();
            for(var i = 0; i < projects.length; i++){
				if(projects[i]["projectId"] == newProjectName){
					var $alertas = $('#new_project');
					error2.show();
					success2.hide();
					$alertas.validate().resetForm();
					$alertas.find('.form-group').removeClass('has-success');
			        $alertas.find('.newProjectName').addClass('has-error');
			        $alertas.find('.fa.projectName').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Project Id is exist. Please enter another name!!!"));
			        return false;
				}
	    	}
            addNewProject();
    	    var $alertas = $('#new_project');
    	    $alertas.validate().resetForm();
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
    });
    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}

function showModalAddNewProject(){
	
	$('#modalAddNewProject').modal({
		backdrop: 'static'
	});
}

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
}

//func reset Form of modal addnewProject
function resetFormInvite() {
	$('#modalAddNewProject').on('hidden.bs.modal', function() {
		document.getElementById("new_project").reset();
	    var $alertas = $('#new_project');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	    if(listImagesProject.length > 0){
	    	$.ajax({
	    		url : jscontext+"/project"+"/deleteFileStore",
	    		type : "POST",
	    		data : JSON.stringify(listImagesProject),
	    		contentType : "application/json",
	    		success : function(result) {
	    			if (!(result.errormessage) ) {
	    				$(".listFilesProject").empty();
	    				listImagesProject = [];
	    			} 
	    		},
	    		error : function(request, e) {
	    			console.log("ERROR: ", e);
	    			var statusCode = request.status;
	    			if (statusCode == 403) {
	    				window.location= jscontext + "/login";
	    			}
	    		},
	    		done : function(e) {
	    			console.log("DONE");
	    		}
	    	});
	    }
	    $(".listFilesProject").empty();
	    listImagesProject = [];
	});
}

function getRandomColor() { 
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}
$("#fileNameProject")[0].onchange = onSelectedFileResultProject;
function onSelectedFileResultProject(){
	uploadFileProject($("#fileNameProject")[0].files, "project");
}

function uploadFileProject(fileList, status){
	$("#chooseIdProject").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	if(fileList.length != 0){
		var formData = new FormData();
		for(var i = 0; i < fileList.length; i++){
			formData.append("file", fileList[i]);
		}
		
		$.ajax({
			type : "POST",
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			url :  jscontext+"/project"+ "/uploadFile",
			data : formData,
			dataType : 'json',
			timeout : 600000,
			success : function(result) {
				if (!(result.errormessage) ) {	
					var fileRe = JSON.parse(result.data);
					if(listImgEditProject.length != 0){
						listImagesProject = listImgEditProject;
						var datas = fileRe;
						$.each(datas, function(ind, vas){
							listImagesProject.push(vas);
						});
						
					} else {
						listImagesProject = fileRe;
					}
					swal({
					        title: getLang(currentLocale,"Upload File Success!!!"),
					        type: "success",
					        timer: 1000,
					        showConfirmButton: false
					    });
					$("#fileNameProject").val("");
					$("#chooseIdProject").html(getLang(currentLocale,"Choose file"));
					drawListFileProject(listImagesProject);
				}
				
			},
			error : function(request, e) {
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					$("#chooseIdProject").html(getLang(currentLocale,"Choose file"));
				}
			}
		});
	}
}

function drawListFileProject(listImages){
	if(listImages.length > 0){
		$(".listFilesProject").empty();
		var html = "";
		$.each(listImages, function(ind, vals){
			html += '<a class="icon icon-attachment mb-2" id="fileOlProject'+ind+'">'+vals.name+'<i class="fa fa-trash-o ml-3"  onclick="closeFileInProject(\''+ind+'\')"></i></a>';
			
		});
		$(".listFilesProject").append(html);
		
	}
}

function addNewProject(){
	$("#svCreateProject").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#svCreateProject").prop("disabled", true);
	$(".preloader").fadeIn("slow");
	$(".preloader").css("z-index"," 1051");
	
	var project ={
			id:$("#idProject").val(),
			name:$("#nameProject").val(),
			description:$("#descriptionProject").val(),
			type:$("#typeProject").val(),
			reportTime:$("#reportTime").val(),
			reportTimeZone:$("#reportTimeZone").val(),
			scmType:$("#scmType").val(),
			scmUrl:$("#scmUrl").val(),
			scmUsername:$("#scmUsername").val(),
			scmPassword:$("#scmPassword").val(),
			color:getRandomColor(),
			planStartDate: ($("#planStDa").val() != ""? moment($("#planStDa").val()).format("YYYY-MM-DDTHH:mm:ss") : ""),
			planEndDate: ($("#planEnDa").val() != ""? moment($("#planEnDa").val()).format("YYYY-MM-DDTHH:mm:ss") : ""),
			standardSize: $("#standardSize").val(),
			standardUnit: $("#standardUnit").val(),
			estimatedProductivity: $("#estimatedProductivity").val(),
			estimatedPrice: $("#estimatedPrice").val(),
			estimatedCost: $("#estimatedCost").val(),
			estimatedDuration: $("#estimatedDuration").val(),
			actualQuantity: $("#actualQuantity").val(),
			actualProductivity: $("#actualProductivity").val(),
			productType: $("#productType").val(),
			standardGAP: $("#standardGAP").val(),
			actualPrice: $("#actualPrice").val(),
			productionUnit: $("#productionUnit").val(),
			image: (listImagesProject.length > 0 ? JSON.stringify(listImagesProject): ""),
			productCode: $("#productCode").val(),
			productName: $("#productName").val()
	}
	if($("#idProject").val()==""){
		project["status"] = "Open";
		$.ajax({
			url : jscontext+"/project/create",
			type : "POST",
			data : JSON.stringify(project),
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					var infoPro = result.data;
					var idMax = result.data.id;
					$(".listFilesProject").empty();
					listImagesProject = [];
					$("#modalAddNewProject").modal("hide");
//					loadGraphic();
					getProjects("true", function(project){
					});
					$("#new_project")[0].reset();
					$("#svCreateProject").prop("disabled", false);
					$("#svCreateProject").html("<i class='ti-check'></i> "+getLang(currentLocale,"Save"));
					$("#svCreateProject").prop("disabled", false);
					swal({
				        title: getLang(currentLocale,"Create Project Success!!!"),
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
					createGroupCodeAtProjectNew(idMax);
				} else {
					$(".preloader").fadeOut("slow");
					swal({
				        title: result.errormessage,	
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
					$("#svCreateProject").prop("disabled", false);
					$("#svCreateProject").html("<i class='ti-check'></i> "+getLang(currentLocale,"Save"));
				}
			},
			error : function(request,error)
		      {	
					$(".preloader").fadeOut("slow");
		          //alert("Request: Can not save");
		          
		          var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					} else {
						$("#svCreateProject").html("<i class='ti-check'></i> "+getLang(currentLocale,"Save"));
						$("#svCreateProject").prop("disabled", false);
						swal({
					        title: getLang(currentLocale,"Can not save!"),	
					        type: "warning",
					        timer: 3000,
					        showConfirmButton: false
					    });
					}
		      },
			done : function(e) {
				$(".preloader").fadeOut("slow");
				//console.log("DONE");
				 swal({
				        title: getLang(currentLocale,"Can not save!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
			}
		});
	} else{
		project["status"] = $("#statusProject").val();
		$.ajax({
			url : jscontext+"/project/"+glProjectId+"/editProject",
			type : "POST",
			data : JSON.stringify(project),
			contentType : "application/json",
			success : function(result) {
			
				if (!(result.errormessage) ) {
					getInfoProject(result.data);
					$("#modalAddNewProject").modal("hide");
					$("#new_project")[0].reset();
					$("#svCreateProject").prop("disabled", false);
					$("#svCreateProject").html("<i class='ti-check'></i> Save");
					$("#svCreateProject").prop("disabled", false);
					swal({
				        title: getLang(currentLocale,"Update Project Success!!!"),
				        type: "success",
				        timer: 2000,
				       
				        
				        showConfirmButton: false
				    });
				} 
				$(".preloader").fadeOut("slow");
			},
			error : function(request,error)
		      {	
					$(".preloader").fadeOut("slow");
		          //alert("Request: Can not save");
		          
		          var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					} else {
						swal({
					        title: getLang(currentLocale,"Can not save!"),	
					        type: "warning",
					        timer: 3000,
					        showConfirmButton: false
					    });
						$("#svCreateProject").html("<i class='ti-check'></i> Save");
						$("#svCreateProject").prop("disabled", false);
					}
		      },
			done : function(e) {
				$(".preloader").fadeOut("slow");
				//console.log("DONE");
				 swal({
				        title: getLang(currentLocale,"Can not save!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
			}
		});
	}
}

function getInfoProject(infProject){
	detailProject = infProject;
	$("#infoProjectId").text(infProject.id);
	$("#infoProjectName").text(infProject.name);
	$("#infoProjectType").text(infProject.type);
	$("#infoDesciptionType").text(infProject.description);
	$("#infoReportTime").text(infProject.reportTime);
	$("#infoReportTimeZone").text(infProject.reportTimeZone);
	$("#infoStatusType").text(infProject.status);
}

function invitedToJoinProject(){
	$.ajax({
		url : jscontext+"/project/infInvited",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var results = result.data;
				listInvitedToJoinProject(results);
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function listInvitedToJoinProject(inviteds){
	$("#listInvitedToJoin").html("");
	for(var key in inviteds){
	   var invalidDate = new Date(inviteds[key]["createDate"]);
	   var createDate = invalidDate.getFullYear() + "-" + (invalidDate.getMonth() + 1) + '-'+ invalidDate.getDate();
	   var  invited ='<li class="d-flex justify-content-start">'
         +'<div class="prog-avatar mr-3 my-auto">'
         +'<img src="'+jscontext+'/assets/img/user/user1.jpg" alt="" width="40" height="40">'
         +'</div>'
         +'<div class="details">'
         +'<div class="title d-flex " id="email">'
         +'<b class="mr-2 ">'+getLang(currentLocale,"Email")+' : </b>'+inviteds[key]["inviterEmail"] 	
         +'</div>'
         +'<span id="projectName">' 
         +'<a class="title" href="/bottest/project/'+inviteds[key]["projectId"]+'/overview"><i class="icon-folder-alt mr-2"></i></a>' +inviteds[key]["projectName"]
         +'<span class="px-3">|</span>'
         +'</span>'
         +'<span id="roleProject">'
         +'<b class="mr-2">'+getLang(currentLocale,"Role Project")+': </b>'+inviteds[key]["projectRole"]
         +'<span class="px-3">|</span>'
         +'</span>'
         +'<span id="userName">'
         +'<b class="mr-2">'+getLang(currentLocale,"Name")+': </b>'+inviteds[key]["inviterName"]
         +'<span class="px-3">|</span>'
         +'</span>'
         +'<span>'
         +'<b class="mr-2">'+getLang(currentLocale,"Date Invited")+': </b>'+createDate
         +'</span>'
         +'</div>';
	   	 if(inviteds[key]["status"] == 0){
		   		invited +='<div class="d-flex my-auto ml-auto py-3">'
		         +'<a class="check-invited mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab mr-3" '
		         +' data-toggle="tooltip" data-placement="top" title="" id="agreeInvid'+inviteds[key]["id"]+'" onclick="joinInvite('+inviteds[key]["id"]+');">'
		         +'<i class="fa fa-check mt-2" style="font-size: 18px;"></i>'
		         +'</a>'
		         +'<a class="delete-invited mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default "'
		         +' data-toggle="tooltip" data-placement="top" title="" id="delInvid'+inviteds[key]["id"]+'" onclick="deleteInvited('+inviteds[key]["id"]+')" >'
		         +'<i class="fa fa-trash-o"></i></a>'
		         +'</div>'
		         +'</li>';
		   	  $("#listInvitedToJoin").append(invited);
	   	 }  
	}		
}

function joinInvite(idJoin){
	$("#agreeInvid"+idJoin).css('background-color', '#1dcb8b');
		$.ajax({
			url : jscontext+"/project"+"/createUserProject",
			type : "POST",
			data: JSON.stringify(idJoin),
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					swal({
				        title: getLang(currentLocale,"Join Success!!!"),
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
					invitedToJoinProject();
				} 
			},
			error : function(request,error)
		      {	
					$(".preloader").fadeOut("slow");
		          //alert("Request: Can not save");
		         
		          var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					} else {
						 swal({
						        title: getLang(currentLocale,"Can not save!"),	
						        type: "warning",
						        timer: 3000,
						        showConfirmButton: false
						    });
					}
		      },
			done : function(e) {
				$(".preloader").fadeOut("slow");
				//console.log("DONE");
				 swal({
				        title: getLang(currentLocale,"Can not save!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
			}		
		});
}

function deleteInvited(idJoin){
	$.ajax({
		url : jscontext+"/project/"+"deleteInvited",
		type : "POST",
		data: JSON.stringify(idJoin),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: getLang(currentLocale,"Delete Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				invitedToJoinProject();
			} 
		},
		error : function(request,error)
	      {	
				$(".preloader").fadeOut("slow");
	          //alert("Request: Can not save");
	          
	          var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can not delete!"),	
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
				}
	      },
		done : function(e) {
			$(".preloader").fadeOut("slow");
			//console.log("DONE");
			 swal({
			        title: getLang(currentLocale,"Can not detele!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
		}		
	});
}

function createGroupCodeAtProjectNew(projectNew){
	var arrTracker = ["Root","Risk","Solution","Task","DailyTask","Request","Q&A"];
	var arrStatus = [
		{
			"key":"New",
			"value":"#e74c3c"
		},
		{
			"key":"In Progress",
			"value":"#3498db"
		},
		{
			"key":"Resolved",
			"value":"#2ecc71"
		},
		{
			"key":"Closed",
			"value":"#2f3542"
		},
		{
			"key": "Re-Opened",
			"value": "#ffa502"
		},
		{
			"key": "Feedback",
			"value": "#7a338c"
		}
	];
	var arrPriority = ["Low", "Normal","High","Urgent","Immediate"];
	var arrCategory = ["API-Web"];
	var tasks = [];

	$.each(arrTracker, function(index, value){
		tasks.push({
			"id" :"",
			"groupCode": "Tracker",
			"itemName" : value,
			"itemValue": value,
			"projectId":projectNew,
			"status":1,
		});
	});
	$.each(arrStatus, function(index, value){
		tasks.push({
			"id" :"",
			"groupCode": "Status",
			"itemName" : value.key,
			"itemValue": value.key,
			"projectId":projectNew,
			"status":1,
			"colorCode":value.value
		});
	});
	$.each(arrPriority, function(index, value){
		tasks.push({
			"id" :"",
			"groupCode": "Priority",
			"itemName" : value,
			"itemValue": value,
			"projectId":projectNew,
			"status":1,
		});
	});
	$.each(arrCategory, function(index, value){
		tasks.push({
			"id" :"",
			"groupCode": "Category",
			"itemName" : value,
			"itemValue": value,
			"projectId":projectNew,
			"status":1,
		});
	});
	$.ajax({
		url : jscontext+"/createProjectComponent",
		type : "POST",
		data : JSON.stringify(tasks),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
			} 
		},
		error : function(request,error)
	      {	
				$(".preloader").fadeOut("slow");
	          //alert("Request: Can not save");
	          
	          var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can not save!"),	
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
				}
	      },
		done : function(e) {
			$(".preloader").fadeOut("slow");
			//console.log("DONE");
			 swal({
			        title: getLang(currentLocale,"Can not save!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
		}		
	});
}

function backSetting(){
	$("#newTask").hide();
	$("#drawSetting").show();
}

var testsuite="";
var itemNameResuft;
var itemNumberResuft;

var lsTestscript, lsActions;
var lsActionsAry = listActions;
var parentaddstep;
var dem = 0;
var testcasedatest = 0;
var listStr = [];
var listJsonResult = [];
var countPass = 0; 
var countFail = 0;
var existEdit = false;
var lsDataUser = "";
var listTestcase;
$(document).ready(function() {
	getAllTestcasesByStatus();
//	getAllUsers();
	drawTestsuiteReport();
	getAllTestcases();
	
//	getTestsuiteTreels();
	var hash = window.location.hash;
	if(hash=="#calRunningTabs"){
		$("#calRunningTabs").trigger("click");
	}
});


function getRunningJobInTestsuite(){
	$.ajax({
		url: jscontext + "/project/" +glProjectId +"/" + glTestsuiteId + "/getRunningJobInTestsuite",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			var listRunningSche = result.data;
			if(listRunningSche.length > 0){
				drawListScheduler(listRunningSche, "#listRunningJobSchedule");
			}else{
				$("#listRunningJobSchedule").html("<div class='w-100 text-center text-danger my-auto'>"+getLang(currentLocale,"The Result is empty!!!")+"</div>")
			}
			
			$(".progress-loadMainContent").fadeOut("slow");
			},
		error : function(request,error)
		    {
				//alert("Request111: "+error);
				
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
						title: getLang(currentLocale,"Could not load data"),
				        text: getLang(currentLocale,"You can reload the page again!"),
				        type: "warning", 
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		    }
	});
}
function getAllUsers(){
	$.ajax({
		url : jscontext+"/project/"+glProjectId+"/"+glTestsuiteId+"/getUsers",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				lsDataUser = result.data;
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

//$("#calRunningTabs").click(function(){
//	if($("#calRunningTabs").hasClass("active show") == false){
////		loadListResuft(testsuite);
//		$(".progress-loadMainContent").show();
//		getRunningJobInTestsuite();
//		
//	}
//	return false;
//});

//
//$("#manualTestcaseTabs").click(function(){
//	if($("#manualTestcaseTabs").hasClass("active show") == false){
//		getAllTestcasesManual();
//		getAllTestcasesByStatusManual();
//	}
//})

//$("#testDataTabs").click(function(){
//	if($("#testDataTabs").hasClass("active show") == false){
//		loadListTestData();
//		loadListScreenLayout();
//	}
//});

//$("#testcaseTabs").click(function(){
//	if($("#testcaseTabs").hasClass("active show") == false){
//		$("#tableListTestcase .progress-loadMainContent").show();
//		$("#detailsTestCase").hide();
//		$("#tableListTestcase").show();
//		
//		
//		$("#bodyTestcaseRecord").empty();
//		getAllTestcases();
//	}
//});

function getTestsuiteTreels(){
	$.ajax({
	url : jscontext + "/project/" +glProjectId +"/" + glTestsuiteId + "/getTree",
	type : "GET",
	data : "",
	contentType : "application/json",
	success : function(result) {
			testsuite = JSON.parse(result.data);
//			loadListTestcase(testsuite);
//			modalhideclearAttr();
//			$(".progress-loadMainContent").fadeOut("slow");
		},
		error : function(request,error)
	    {
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
//				swal({
//					title: getLang(currentLocale,"Could not load data"),
//			        text: "Please sync project!!!",
//			        type: "warning", 
//			        timer: 2000,
//			        showConfirmButton: false
//			    });
			}
			
	    }
	});
}


$("#layoutTabs").click(function(){
	if($("#layoutTabs").hasClass("active show") == false && $("#bodyLayoutRecord .itemLayout").length < 1){
		$(".progress-loadMainContent").show();
		$("#bodyLayoutRecord").empty();
		$("#bodyListLayout").show();
		$("#detailsLayout").hide(); 
		getAllLayouts();
//		getLayoutTree();
	}
});

//close detail testcase
function closeStepTestcase(){
	if(existEdit == true){
		swal({
	        title: getLang(currentLocale,"Save change in TESTCASE ?"),
	        text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
	        type: "warning", 
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: getLang(currentLocale,"Save"),
	        cancelButtonText: getLang(currentLocale,"Dont's Save"),
	        closeOnConfirm: true
	    },function(isConfirm){
	    	if(isConfirm) {
	    		saveStep();
	    	}
	    	$("#detailsTestCase").slideToggle();
	    	$("#tableListTestcase").slideToggle(); 
	    	$("#sortable").empty();
//	    	getTestsuiteTreels();
//	    	getAllTestcases();
	    	//loadListTestcase(testsuite);

		});
		existEdit = false;
	}else{
		$("#detailsTestCase").slideToggle();
    	$("#tableListTestcase").slideToggle(); 
    	$("#sortable").empty();
//    	getTestsuiteTreels();
//    	getAllTestcases();
    	//loadListTestcase(testsuite);

	}
}






 




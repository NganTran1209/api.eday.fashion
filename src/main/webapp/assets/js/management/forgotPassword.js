var validateResetPass = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#resetPassword');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
            password: {
                required: true,
                regex:"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,30})$"
            },
            confirmPass:{
            	required: true,
            	regex:"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,30})$"
            },
        },
        
        messages: {
			password:{
				required:getLang(currentLocale,"Cannot be blank!"),
				regex: getLang(currentLocale,"Password must contain at least six characters, at least one number and uppercase letters"),
			},
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
			
            //func check Name Layout is exist
            var password = $("#password").val();
            var passconfirmation = $("#confirmPass").val();
				if(passconfirmation != password){
					var $alertas = $('#resetPassword');
					error2.show();
					success2.hide();
					$alertas.validate().resetForm();
					$alertas.find('.form-group').removeClass('has-success');
			        $alertas.find('.passConfirm').addClass('has-error');
			        $alertas.find('.confirmPassWord').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Your password and confirmation password do not match!!!"));
			        $alertas.find('.fa.password').removeClass('fa-warning').removeClass('fa-check');	     
			        return false;
				}
			// func sign up
			resetPass();
    	    var $alertas = $('#resetPassword');
    	    $alertas.validate().resetForm();
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
        
    });
    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}
function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}

//func create new pass when you forgot password
function resetPass(){
	var passwordDTO = {
		"token": $("#token").val(),
		"newPassword": $("#password").val(),
		"newConfirmPass": $("#confirmPass").val(),
		"@type" : "ChangePassDTO"
	}
	$.ajax({
		url : jscontext+"/forgotPass",
		type : "POST",
		data : JSON.stringify(passwordDTO),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title:getLang(currentLocale, "You have successfully set a new password!!!"),
			        type: "success",
			        confirmButtonColor: "#25c16fde",
			        confirmButtonText: getLang(currentLocale,"Back to Login Page"),
			        closeOnConfirm: false
			    }, function () {
			    		self.location.href = jscontext+"/login";
				});
			}
			
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
	var duplicateUserTaskDetail = [];
$( document ).ready(function() {
	var listIssue = [];
	$.each(taskList, function(ind, vals){
		var itemIssue = {
			value: vals.tracker +" # " + vals.id + ": ",
		    label: vals.id,
		    desc: vals.subject,
		}
		listIssue.push(itemIssue);
	});
	autocompleteParentTask(listIssue);
	
	var lsBug = [];
	$.each(listBug, function(ind, vals){
		var itemIssue = {
			value: getLang(currentLocale,"Result Id")+": "+vals.id + " - ",
		    label: vals.id,
		    desc: vals.testcase,
		}
		lsBug.push(itemIssue);
	});
//	autocompleteBugId(lsBug);
});

function issueDetailById(task){
	getListBottestUser();
	getTaskHistory(task.id);
	var planStartDate = (task.planStartDate != undefined ? converDate(task.planStartDate): "");
	var planEndDate = (task.planEndDate != undefined ? converDate(task.planEndDate): "");
	var actualStartDate =(task.actualStartDate != undefined ? converDate(task.actualStartDate): "");
	var actualEndDate = (task.actualEndDate != undefined ? converDate(task.actualEndDate): "");
	
	var dateAu = new Date(task.createDate);
	var dateCreate = converDate(task.createDate) + " " + dateAu.getHours() +":"+ dateAu.getMinutes()+":" + dateAu.getSeconds();
	
	if(numberDraw == 1){
		$("#previousIssue").hasClass("disabled");
		$("#previousIssue").removeAttr("href");
	} else {
		$("#previousIssue").attr("href", jscontext +"/project/"+projectId+"/issue/"+ taskPreId);
	}
	
	if(numberDraw == lengthTask){
		$("#nextIssue").hasClass("disabled");
		$("#nextIssue").removeAttr("href");
	} else {
		$("#nextIssue").attr("href", jscontext +"/project/"+projectId+"/issue/"+ taskNextId);
	}
	
	$("#numberChange").text(numberDraw);
	$("#numberTotal").text(lengthTask);
	
	$("#subject").text(task.subject);
	$("#assignee").text(task.assignee ? (task.lastname != null &&  task.firstname != null ? task.lastname + " " +task.firstname  : task.assignee )  : "" );
	$("#planStartDate").text(planStartDate);
	$("#planEndDate").text(planEndDate);
	$("#actualStartDate").text(actualStartDate);
	$("#actualEndDate").text(actualEndDate);
	$("#estimatedTime").text(task.estimatedTime);
	$("#actualTime").text(task.actualTime);
	$("#description").html(task.description);
	$("#title-detail-task").text(task.tracker);
	$("#category").text(task.category);
	$("#priority").text(task.priority);
	$("#status").text(task.status);
	$("#title-detail-task").append(' # '+ task.id);
	$("#taskIdentity").val(task.id);
	$("#percentDone").text(task.done);
	$("#testsuite").text(task.testsuite);
	$("#milestone").text(task.milestone);
	var createBy = '';
	$.each(listUsers,function(index, value){
		if(value["username"] == task.createBy){
			var count = 0 ;
			if(duplicateUserTaskDetail.length > 0){
				$.each(duplicateUserTaskDetail, function(indexDup,valueDup){
					if(valueDup.username == task.createBy){
						createBy = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] +"( " +task.createBy+" )" : task.createBy); 
						count ++;
					}					
				})
			} 
			
			if(count == 0 ){
				createBy = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] :task.createBy); 
			}
			
		}
	})
	
	$("#author").text("Add By " + (createBy != "" ? createBy :  task.createBy)  +" about " + dateCreate);
	getSubTaskInDetail(task.id, function(data){
		if(data.length > 0){
			var html = "";
			$("#detailSubTask").empty();
			var assignName = '';
			$.each(data, function(ind,vals){
				$.each(listUsers,function(index, value){
					if(value["username"] == vals.assignee){
						var count = 0 ;
						if(duplicateUserTaskDetail.length > 0){
							$.each(duplicateUserTaskDetail, function(indexDup,valueDup){
								if(valueDup.username == vals.assignee){
									assignName = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] +"( " +task.createBy+" )" : vals.assignee); 
									count ++;
								}					
							})
						} 
						
						if(count == 0 ){
							assignName = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] : vals.assignee); 
						}
						
					}
				})
				
				html +='<tr class="">'
					+'<td ><a href="'+jscontext +"/project/"+projectId+"/issue/"+ vals["id"]+'">Feature #'+vals.id+':</a> '+vals.subject+'</td>'
					+'<td width="20%" >'+vals.status+'</td>'
					+'<td width="20%">'+(assignName != "" ? assignName : vals.assignee)+'</td>'
					+'<td rowspan="2">'
						+ vals.done
					+'</td>'
				+'</tr>'
			});
			$("#detailSubTask").append(html);
		}
	});
	
	if(task.parentTask != undefined){
		getParent(task.parentTask, function(data){
			if(data.length > 0){
				var html = "";
				$.each(data, function(ind,vals){
					html += '<a href="'+jscontext +"/project/"+projectId+"/issue/"+ vals["id"]+'">Feature #'+vals.id+':</a> '+vals.subject+'';
				})
				$("#parentTest").append(html);
			}
		});
	}

	var fileP = (task.fileName ? JSON.parse(task.fileName): "");
	if(fileP.length > 0){
		$("#fileTask").empty();
		var html = "";
		$.each(fileP, function(ind, vals){
			var commas = [...vals.Name].filter(l => l === '.').length;
			
			var extensionFile = (vals.Name).split(".")[commas];
			var fileName = vals.Id + "." + extensionFile;
			html += '<a class="icon icon-attachment">'+vals.Name+'</a><a class="icon-only icon-download" href="'+context+'/project/'+projectId+'/'+ task.id +'/getFileIssue/'+fileName+'" download></a>';
		});
		$("#fileTask").append(html);
	}
	
}

function duplicateUser(arr){
	var duplicateIds = [];
	var valueDupl = arr
    .map(e => e['lastname'] + e['firstname'])
    .map((e, i, final) => final.indexOf(e) !== i && i)
    .filter(obj=> arr[obj])
    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
	 $.each(valueDupl, function(index, value){
		 $.each(arr, function(indexArr, valueArr){
			 var name = valueArr["lastname"] + valueArr["firstname"]; 
			 if(name == value){
				 duplicateIds.push(valueArr);
			 }
		 })
	 })
	 
	 return duplicateIds ;
}

var listUsers = '';
function getListBottestUser(){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getUsers",
		type : "get",
		data : "",
		contentType : "application/json", 
		async : false,
		success : function(result) {
			if (!(result.errormessage) ) {
				listUsers = result.data;
				duplicateUserTaskDetail = duplicateUser(listUsers);
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
var listMilestone;
function editIssue(){
	$("html, body").animate({ scrollTop: $(document).height() }, "slow");
	$("#backIssueButton").html("");
	var saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'issue\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	detailItemIssue(task);
}

$("#milestoneIssue").on("change", function(){
	var milestoneName = $("#milestoneIssue").val();
	$.each(listMilestone, function(index,value){
		if(value.mileStoneName == milestoneName){
			$("#planEndDateTask").val(converDate(value["endDate"]));
		}
	});
	
});

function getTaskHistory(id){
	$.ajax({
		type:"GET",
		url: jscontext +"/project/"+projectId+"/tasks/"+id+"/tasksHistory", 
		success : function(result) {
			if(!(result.errormessage) ){	
				$("#contentUpDate").html("");
				var taskHistory = result.data;
				for(var i = 0 ; i < taskHistory.length ; i++){
					var taskVal = taskHistory[i].contentTask;
					var char = taskVal.lastIndexOf(",");
					var contentTaskHtr = taskVal.slice(0,char) + taskVal.slice(char+1);
					var dateUpdate = new Date(taskHistory[i].updateDate).toISOString();
					var dateU = dateUpdate.split("T")[0] +" "+ (dateUpdate.split("T")[1]).split(".")[0];
					var updateBy = '';
					$.each(listUsers,function(index, value){
						if(value["username"] == taskHistory[i].updateBy){
							var count = 0 ;
							if(duplicateUserTaskDetail.length > 0){
								$.each(duplicateUserTaskDetail, function(indexDup,valueDup){
									if(valueDup.username == taskHistory[i].updateBy){
										updateBy = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] +"( " +task.createBy+" )" : taskHistory[i].updateBy); 
										count ++;
									}					
								})
							} 
							
							if(count == 0 ){
								updateBy = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] :taskHistory[i].updateBy); 
							}
							
						}
					})
					var html = '<p class="taskChange">Update By <strong>' +(updateBy != '' ? updateBy : taskHistory[i].updateBy)+ "&nbsp</strong>on " +dateU+'</p>';
					var jsonTask = "";
					try{
						jsonTask = JSON.parse(contentTaskHtr);
					}catch(e){
						
					}
					if(jsonTask != ""){
						for(var key in jsonTask){
							if(key == "Description"){
								html += '<div class="ml-5"><div><strong>'+ key +'</strong> </div>' + jsonTask[key] +'</div>';
							} else {
								html += '<ul class="details"><li class="d-flex"><strong>'+ key +'&nbsp</strong> ' + jsonTask[key] +'</li></ul>';	
							}
							
						}
						$("#contentUpDate"). append(html);
					}
				}
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function addSubTask(status){
	var idTask = $("#idTask").text("");
	listImages = [];
	$(".newTask").trigger("reset");
	$(".listFiles").empty();
	descriptionTask.data.set("");
	
	$("#errorSubject").text("")
	$("#errorPlanDate").text("");
	$("#errorAcutalDate").text("");
	$("#errorMilestone").text("");
	
	$('#dtTask').hide();
	var backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backDetailIssue()">'
			+'<i class="icon-arrow-left"></i> Back'
			+'</button>';
	$("#backIssueButton").html(backButton);
	var saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'issue\', \'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	$("#noteEdit").attr("hidden", true);
	$("#title-new").text('New Issue');
	if(status =="true"){
		$("#parentTask").val($("#taskIdentity").val());
	}
	loadDataInPage();
	$('#trackerTask option[value="'+task.tracker+'"]').prop("selected",true);
	if(task.tracker == "Bug"){
		$("#bugId").attr("disabled", false);
	} else {
		$("#bugId").attr("disabled", true);
	}
	$('#statusTask option[value="'+task.tracker+'"]').prop("selected",true);
	$('#categoryTask option[value="'+task.category+'"]').prop("selected",true);
	$('#priorityTask option[value="Normal"]').prop("selected",true);
	
	getUserProject(function(userProjectList){
		var valueDuplUser = duplicateUser(userProjectList);
		$("#assigneeTask").empty();
		$("#watchers").empty();
		$("#assigneeTask").append('<option value = ""></option>');
		for(var i =0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null && userProjectList[i]["firstname"] != null ){
				var count = 0;
				if(valueDuplUser.length > 0){
					$.each(valueDuplUser, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							$("#watchers").append('<li name="'+userProjectList[i]["username"]+'"><input class="mx-2 my-2 ckb-watcher"  type="checkbox"'
									+'value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"] +" "+userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")" +'</li>');
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"] +"("+userProjectList[i]["username"]+")" +'</option>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#watchers").append('<li name="'+userProjectList[i]["username"]+'"><input class="mx-2 my-2 ckb-watcher"  type="checkbox"'
							+'value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"] +" "+userProjectList[i]["firstname"] +'</li>');
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"] +'</option>');
				}	
			}
		}
	});
	if(status == "true"){
		$('#testsuiteIssue option[value="'+task.testsuite+'"]').prop("selected",true);
	}
	
}

function getSubTaskInDetail(id, callbackF){
	$.ajax({
		type:"GET",
		url: jscontext +"/project/"+projectId+"/tasks/getSubTaskInDetail/"+id, 
		success : function(result) {
			if(!(result.errormessage) ){
				callbackF(result.data);
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getParent(id, callbackF){
	$.ajax({
		type:"GET",
		url: jscontext +"/project/"+projectId+"/tasks/getParent/"+id, 
		success : function(result) {
			if(!(result.errormessage) ){
				callbackF(result.data);
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function backDetailIssue(){
	$("#dtTask").show();
	$("#newTask").hide();
}

function converDate(dateTask){
	var date = new Date(dateTask);
	var timeConver = date.getFullYear() + '-' + (((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1)))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate()));
	return timeConver;
}
var testsuitels =[];
$( document ).ready(function() {
	drawParentTaskInTestcase(taskList);
});

function showModalAddNewTestsuite(){
	$('#modalAddNewTestsuite').modal({
		backdrop: 'static'
	});
}

function getProjectTestSuites(){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			$("#loader-testsuite").fadeOut("slow");
			if (!(result.errormessage) ) {
				var testsuitels = result.data;
				loadListRs(testsuitels);
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			//console.log("DONE");
		}
	});
}

function getAllTestcaseToCSV() {
	$("#exportAllTestcase").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#exportAllTestcase").prop("disabled", true);
	var listTitle = "Testsuites,Test Cases,Test Case Procedure,Expected Output,Last Run \n";
	var count = 0;
	var listDatas = "";
//	$.each( testsuitels, function(indexs, valuesItemTestsuite) {
		$.ajax({
			url : jscontext +"/project/" + projectId + "/getTestsuitesContent",
			type : "get",
			data : "",
			contentType : "application/json", 
			success : function(result) {
				
				if (!(result.errormessage) ) {
					window.location.href = jscontext +"/project/" + projectId+"/downloadTestcaseFromTestsuiteExport";
				    $("#exportAllTestcase").html("<i class='ti-export text-white mr-2'></i>"+getLang(currentLocale,"Export"));
					$("#exportAllTestcase").prop("disabled", false);
					
				} else {
				}
			},
			error : function(request, e) {
				console.log("ERROR: ", e);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});
//	});
}
function loadListRs(testsuitels){
	$("#bodyTestsuiteRecord").html("");
	var testsuiteString = JSON.stringify(testsuitels);
	if(testsuiteString != '{}'){
		var index = 0;
		for(var key in testsuitels){
			var totalTestcase = 0;
		    var testcasePass = 0;
		    var testcasetFail = 0;
		    var testcaseNotTest = 0;
		    var testcaseTested = 0;
		    var testcaseDegrade = 0;
		    var percentTestPass = 0;
		    var percentTestCase = 0;
		    var percentNotTest = 0;
		    var percentTestFail = 0;
		    var percentDegrade = 0;
		    var status="";
			index += 1;
			testcasePass = testsuitels[key]["testcasePass"];
			testcasetFail = testsuitels[key]["testcaseFail"];
			testcaseNotTest = testsuitels[key]["notRun"];
			testcaseDegrade = testsuitels[key]["countDegrade"];
			totalTestcase = testcasePass + testcasetFail + testcaseNotTest + testcaseDegrade ;
			if(totalTestcase != 0){
				percentTestPass = (testcasePass / totalTestcase *100).toFixed(0);
				percentTestFail = (testcasetFail/ totalTestcase*100).toFixed(0) ;
				percentDegrade = (testcaseDegrade/ totalTestcase*100).toFixed(0);
				percentNotTest = (testcaseNotTest/totalTestcase*100).toFixed(0);
			} 
				info ='<table class="table table-borderless table-result-action">'
					  +'<tbody>'
					  +'<tr>'
					  +'<td>'+getLang(currentLocale,"Total Testcases")+':</td>'
					  +'<td width="20%">'+ totalTestcase+'</td>'
					  +'<td width="20%">'+ (totalTestcase != 0 ? "100%": "0%")+'</td>'
					  +'</tr>'
					  +'<tr class="text-success">'
					  +'<td onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"PASS"+'\');"><a href="#">- '+getLang(currentLocale, "Pass")+':</a></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"PASS"+'\');"><a href="#">'+ testcasePass+'</a></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"PASS"+'\');"><a href="#">'+ percentTestPass +'%</a></td>'
					  +'<td rowspan="2">'
					  +'<div class="actionListTable box-row-reverse-action align-items-end">'
					  +'<div class="actionListTable box-row-reverse-action align-items-end">'
						+'<button class="btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect  mdl-button--mini-fab btn-default btn-success" id="btnPlay_'+index+'" data-id="'+index+'" data-name="'+testsuitels[key]['testsuite']+'" data-toggle="modal"  data-target="#modalRun" onclick="getAllNameTestCase(\''+testsuitels[key]['testsuite']+'\');" data-placement="top" title='+getLang(currentLocale,"Run")+'>'
						+'<i class="fa fa-play"></i>'
						+'</button>'
					  +'</div>'
					  +'</td>'
					  +'</tr>'
					  +'<tr class="text-danger">'
					  +'<td onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"FAILED"+'\');"><span class="ahoverFail">- '+getLang(currentLocale, "Fail")+':</span></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"FAILED"+'\');"><span class="ahoverFail">'+ testcasetFail+'</span></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\',bodyTestsuiteRecord \''+testsuitels[key]['testsuite']+'\', \''+"FAILED"+'\');"><span class="ahoverFail">'+ percentTestFail+'%</span></td>'
					  +'</tr>'
					  +'<tr>'
					  +'<tr class="text-degrade">'
					  +'<td onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"DEGRADE"+'\');"><span class="ahoverDegrade">- '+getLang(currentLocale,"Degrade")+':</span></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"DEGRADE"+'\');"><span class="ahoverDegrade">'+ testcaseDegrade+'</span></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"DEGRADE"+'\');"><span class="ahoverDegrade">'+ percentDegrade+'%</span></td>'
					  +'<td rowspan="2">'
					  +'<div class="actionListTable box-row-reverse-action align-items-end">'
						
						+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites" href="javascript:void(0);" data-name="'+testsuitels[key]['testsuite']+'" onclick="showConfirmMessageTestSuite(this)" data-toggle="tooltip" data-placement="top" title='+getLang(currentLocale," Delete")+'>'
						+'<i class="fa fa-trash-o"></i>'
						+'</a>'
					  +'</div>'
					  +'</td>'
					  +'</tr>'
					  +'<td onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"NOT-RUN"+'\');"><span class="ahoverNotRun">- '+getLang(currentLocale,"Not Run")+':</span></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"NOT-RUN"+'\');"><span class="ahoverNotRun">'+ testcaseNotTest +'</span></td>'
					  +'<td width="20%" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"NOT-RUN"+'\');"><span class="ahoverNotRun">'+ percentNotTest+'%</span></td>'
					  +'</tr>'
					  +'</tbody>'
					  +'</table>';

			var htmlRecordTestsuites = 	'<tr>'
				+'<td width="25%"><div class="d-flex"><i class="fa fa-caret-right fa-1x mt-1 mr-2"></i> <p style="color: #1dcb8b;">'+testsuitels[key]['testsuite']+'</p></div>'
				+ '<small class="text-muted font-italic pl-2">'+(testsuitels[key]['descriptionTestsuite'] != undefined || testsuitels[key]['descriptionTestsuite'] != null ? testsuitels[key]['descriptionTestsuite'] : "")+' </small>'
				+'</td>'
				+'<td width="50%">'
				+'<div class="d-flex flex-row">'
					+'<div class="col-md-12 form-group">';
			statistical(htmlRecordTestsuites, projectId, testsuitels[key]['testsuite'], function(jsonText){
htmlRecordTestsuites += '<div class="col-md-3">'
						  + '<div class="">'
						  	+ '<a class="btn" href="#" style="border-radius: 21px;background: #6399dc;padding: 3px 12px;color: #fff;margin-bottom: 3px;" onclick="redirectFilterTcAuto(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\', \''+"FAILED"+'\');">'+getLang(currentLocale,"Automation Testcase")+': '+jsonText['automation']+'</a>'
						  + '</div>'
						  + '<div class="">'
						  	+ '<a class="btn" href="#" style="border-radius: 21px;background: #cca42e;padding: 3px 12px;color: #fff;margin-bottom: 3px;" onclick="redirectFilterTcMan(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\');">'+getLang(currentLocale,"Manual Testcase")+': '+jsonText["manual"]+'</a>'
						  + '</div>'
						  + '<div class="">'
						  	+ '<a class="btn" href="#" style="border-radius: 21px;background: #c39ac7;padding: 3px 12px;color: #fff;margin-bottom: 3px;" onclick="redirectFilterScreen(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\');">'+getLang(currentLocale,"Screen Design")+': '+jsonText["layout"]+'</a>'
						  + '</div>'
						  + '<div class="">'
						  	+ '<a class="btn" href="#" style="border-radius: 21px;background: #c391a8;padding: 3px 12px;color: #fff;margin-bottom: 3px;" onclick="redirectFilterTestData(\''+projectId+'\', \''+testsuitels[key]['testsuite']+'\');">'+getLang(currentLocale,"TestData")+': '+jsonText["testdata"]+'</a>'
						  + '</div>'
						  + '<div class="">'
						  	+ '<a class="btn" href="#" style="border-radius: 21px;background: #8b758e;padding: 3px 12px;color: #fff;margin-bottom: 3px;" onclick="window.location.href='+"'"+jscontext+"/project/"+projectId+"/schedules"+"'"+'">'+getLang(currentLocale,"Schedule")+': '+jsonText["schedule"]+'</a>'
						  + '</div>'
					  + '</div>'
					  +'<div class="col-md-9"></div>';
			});
				          
htmlRecordTestsuites +='</div>'
				+'</div>'
				+'</td>'
				+'<td>'+status 
					+'<div class="d-flex flex-row">'
					+ info
					+'</div>'
				+'</td>'
				+'<tr>';
			$("#bodyTestsuiteRecord").append(htmlRecordTestsuites);		
		}		

		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
	}else{
		var htmlRecordTestsuites = 	'<tr><td colspan="3" class="text-center text-danger">'+getLang(currentLocale,"The Testsuite is empty!!!")+'</td></tr>';
		$("#bodyTestsuiteRecord").append(htmlRecordTestsuites);
	}
}

function redirectFilterTcAuto(projectId, testsuiteName, status){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterTcAuto/"+testsuiteName+"/"+status,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/testcases";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}

function redirectFilterTcMan(projectId, testsuiteName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterTcMan/"+testsuiteName,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/testcaseManual";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}

function redirectFilterScreen(projectId, testsuiteName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterScreen/"+testsuiteName,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/screenDesign";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}

function redirectFilterTestData(projectId, testsuiteName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterTestData/"+testsuiteName,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/testData";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}

function statistical(htmlRecordTestsuites,projectId, testsuiteName, callbackF){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/getTestcaseTested",
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				var jsonText = result.data;
				
				callbackF(jsonText);
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
						title: getLang(currentLocale,"Could not load data"),
				        text: getLang(currentLocale,"Please sync project!!!"),
				        type: "warning", 
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}

function showConfirmMessageTestSuite(isThis) {
	 var item = $(isThis).attr("data-name");
	
   swal({
       title: getLang(currentLocale,"Are you sure delete TestSuite")+" :" + item +"?",
       text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
       cancelButtonText: getLang(currentLocale,"Cancel"),
       customClass:"modalDelLayout",
       closeOnConfirm: false
   }, function () {
	   $(".modalDelLayout .confirm").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	   $(".modalDelLayout .confirm").prop("disabled", true);
	   deleteTestSuite(item);
   });
}

//func validation add TestSuite
var validateAddTestSuite = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#new_testsuite');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	testsuiteName: {
        		minlength: 2,
        		maxlength: 50,
                required: {
                	depends:function(){
		            $(this).val($.trim($(this).val()));
		            return true;
                	}
                },
                regex:"^[a-zA-Z0-9.\-_]*$"
            },
            testsuiteDescription: {
                minlength: 5,
            },
        },
        
        messages: {
        	testsuiteName:{
        		maxlength: getLang(currentLocale,"Please enter at less 50 characters!"),
        		minlength: getLang(currentLocale,"Please enter at least 2 characters!"),
        		required:getLang(currentLocale,"Cannot be blank!"),
        		regex: getLang(currentLocale,"not contain characters special,white space")
        	},
        	testsuiteDescription: getLang(currentLocale,"Please enter at least 5 characters!")
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            //func check Name Test suite is exist
            var newNametestSuite = $("#testsuite_name").val();
            for(var i = 0; i < testsuitels.length; i++){
				if(testsuitels[i]["Testsuite"] == newNametestSuite){
					var $alertas = $('#new_testsuite');
					error2.show();
					success2.hide();
					$alertas.validate().resetForm();
					$alertas.find('.form-group').removeClass('has-success');
			        $alertas.find('.addNewTS').addClass('has-error');
			        $alertas.find('.fa.testsuiteName').addClass('fa-warning').removeClass('fa-check').attr("data-original-title","TestSuite Name is exist. Please enter another name!!!");
			        $alertas.find('.fa.testSuiteDes').removeClass('fa-warning').removeClass('fa-check');
			        return false;
				}
	    	}
            addNewTestSuite();
    	    var $alertas = $('#new_testsuite');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
    });
    
    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}
	
// func add testsuite
function addNewTestSuite(){
	$("#addNewTestsuite").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#addNewTestsuite").prop("disabled", true);
	var testSuiteName = $('#testsuite_name').val();
	var testSuiteDescription = $('#testsuite_description').val();
	var testSuite = {
		"Testsuite":testSuiteName, 
		"Description":testSuiteDescription,
	}
	$.ajax({
		url : jscontext +"/project/" + projectId +"/createTestSuite",
		type : "POST",
		data : JSON.stringify(testSuite),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalAddNewTestsuite").modal("hide");
				$("#loader-testsuite").fadeIn("slow");
				$("#bodyTestsuiteRecord").empty();
				
				getProjectTestSuites();
				
				$("#addNewTestsuite").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
				$("#addNewTestsuite").prop("disabled", false);
				document.getElementById("new_testsuite").reset();
	    	    var $alertas = $('#new_testsuite');
	    	    $alertas.validate().resetForm();
//					Layout.init();
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func edit description Test Suite
function editDescriptionTestSuite(){
	var variableProjectId = $("#variableProject").val();
	var description = $("#testsuite_edit_description").val();
	var variableTestsuite = $("#variableTestsuites").val();
	$.ajax({
		url : jscontext +"/project/" + projectId +"/"+variableTestsuite+"/editDescriptionTestSuite",
		type : "POST",
		data : description,
		contentType : "application/json", 
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalEditTestsuite").modal("hide");
				$("#bodyTestsuiteRecord").empty();
				getProjectTestSuites();
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//delete testsuite
function deleteTestSuite(item){
	var testsuiteId = item
	$.ajax({
		url : jscontext +"/project/" + projectId +"/"+testsuiteId+"/deleteTestSuite",
		type : "POST",
		data : testsuiteId,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#bodyTestsuiteRecord").empty();
				getProjectTestSuites();
				Layout.init();
			} else {
			}
			swal({
		        title: getLang(currentLocale,"Delete TestSuite Success!!!"),
		        type: "success",
		        timer: 2000,
		        showConfirmButton: false
		    });
			$(".modalDelLayout .confirm").html(getLang(currentLocale,"Yes, delete it!"));
		    $(".modalDelLayout .confirm").prop("disabled", false);
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

// func get Name TestCase
function getNameTCInTestsuiteScreen(isThis){
	var testSuiteName = $(isThis).attr("data-name");
	$("#testsuiteNameInModal").text(testSuiteName);  
}


//func reset Form of new testsuite
function resetFormTestsuite() {
	$('#modalAddNewTestsuite').on('hidden.bs.modal', function() {
		document.getElementById("new_testsuite").reset();
	    var $alertas = $('#new_testsuite');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

////Sync project with scm server
//function syncProject(e){
//	$.ajax({
//		url : jscontext + "/project/"+ $('#hiddenProjectId').val()+"/syncProject",
//		type : "POST",
//		data : "",
//		contentType : "application/json",
//		success : function(result) {
//			if (!(result.errormessage) ) {
//				loadListRs(JSON.parse(result.data));
//			}else {
//				alert("Sync project fail:" + result.errormessage);
//			}
//      },
//      error : function(request,error)
//      {
//    	  alert("Sync project fail:" + error);
//      }
//	
//	});
//}

function backTestsuites(){
	$("#newTask").hide();
	$("#drawTestsuites").show();
}


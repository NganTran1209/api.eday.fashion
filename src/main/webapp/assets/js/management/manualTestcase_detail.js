var listMileStone ;
var listImages =[];
var duplicateUsers ;
jQuery(document).ready(function() {
	'use strict';
	getListBottestUserMal();
	validateAddManualTestcase();
	validateAddLastResult();
	hidenModalManual();
	validateAddMilestone();
	
	$("#filterStatusTestcaseManual").select2({
		placeholder: getLang(currentLocale,"Execute Status"),
		width:"100%",
		minimumResultsForSearch: Infinity,
		allowClear: true,
		dropdownParent: ""
	});
	
	drawBugInTestcase(manualTcLs);
	drawParentTaskInTestcase(taskList);

});

$(document).on("click", function() {
//	$('#modalImg').hide();
//	 $(".close-image-result").show();
});

$("#listTestcaseManual").sortable({
	start: function(event, ui) {
   	 	ui.item.startPos = ui.item.index();
	},
	update: function(event, ui) {
		var indexOld = ui.item.startPos + 1;
        console.log("Old: "+ indexOld);
		var indexNew = ui.item.index() + 1;
        console.log("New: " +indexNew);
        var productOrder = $(this).sortable('toArray');
        var lsTestcaseS = [];
        $.each(productOrder, function(ind, val){
        	var projectPro = {
        			"projectId": projectId,
        			"testsuite": "",
        			"testcase": val
        	}
        	var automationTestcase = {
        			"projectProgressIdentity": projectPro,
        		    "type": "manual",
        		    "orderId": ind+1
        	}
        	lsTestcaseS.push(automationTestcase);
        });
        
        $.ajax({
    		url : jscontext +"/project/" + projectId +"/orderIdTestcase",
    		type : "POST",
    		data : JSON.stringify(lsTestcaseS),
    		contentType : "application/json", 
    		success : function(result) {
    			
    			if (!(result.errormessage) ) {
    				getAllTestcaseManualByProject();
    				
    				var testsuiteName = $("#filterTestsuite").val();
    				var executeBySl = $("#filterExecuteBy").val();
    				var statusTestcase = $("#filterStatusTestcaseManual").val();
    				var fromDate = $("#filterFromDate").val();
    				var toDate = $("#filterToDate").val();
    				var filterAssignTo = $("#filterAssignTo").val();
    				var description = $("#filterDescription").val();
    				var statusIssue = $("#filterTestcaseStatus").val();
    				var categoryIssue = $("#filterTestcaseCategory").val();
    				var milestoneTestcase = $("#filterTestcaseMilestone").val();
    				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue,categoryIssue,milestoneTestcase);
    			} else {
    			}
    		},
    		error : function(request, e) {
    			console.log("ERROR: ", e);
    			
    			var statusCode = request.status;
    			if (statusCode == 403) {
    				window.location= jscontext + "/login";
    			} else {
    				swal({
    					title: getLang(currentLocale,"Could not load get All Testcase"),
    			        text: getLang(currentLocale,"You can reload the page again!"),
    			        type: "warning", 
    			        timer: 2000,
    			        showConfirmButton: false
    			    });
    			}
    		},
    		done : function(e) {
    			console.log("DONE");
    		}
    	});
    }
});

$("#listTestcaseManual").disableSelection();

var listFileInput=[];

var tstManual="";
function getAllTestcasesManual(){
	$("#listTestcaseManual").empty();
	$("#loadingManualTestcase").show();
	$.ajax({
		url : jscontext +"/project/" + projectId+"/"+ glTestsuiteId +"/getTestcasesManual",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				tstManual = result.data;
				loadListTestcaseManual(tstManual);
				
			} else {
			}
		},
		error : function(request,e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get All Testcase Manual"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
var listTestcaseSttmanual;
function getAllTestcasesByStatusManual(){
	$.ajax({
		url : jscontext +"/project/" + projectId+"/"+ glTestsuiteId +"/getAllTestcasesStatusManual",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				listTestcaseSttmanual = result.data;
			} else {
			}
		},
		error : function(request,e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get All Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


function loadListTestcaseManual(tstManual){
	var numbSTT = 0;
	if(tstManual.length == 0){
		var htmlRecordTestsuites =	'<tr><td colspan="6" class="text-center text-danger">'+getLang(currentLocale,"The testcase manual is empty!!!")+'</td></tr>'
		$("#listTestcaseManual").html(htmlRecordTestsuites);
		
	}else{
		$.each(tstManual, function(index, values){
			var litsResult = values["results"];
			var testcaseName = values.projectProgressIdentity["testcase"].split("_");
			var numberTestcase = testcaseName[testcaseName.length-1];
			drawTestcaseManual(index, values, numberTestcase, litsResult);
		});
		showTooltipBtn();
	}
	$("#loadingManualTestcase").fadeOut("slow");
}

function showTooltipBtn(){
	 $(".item-tooltip").hover(function(){
		 	$('.item-tooltip').tooltip('hide');
		    $(this).tooltip('show');
	 }, function(){
	    	$('.item-tooltip').tooltip('hide');
	 });
}

$("#testsuiteListManual").change(function() {
	var testsuiteChoose = $("#testsuiteListManual option:selected").text();
	var lstTestcaseChoose;
	$.ajax({
		url : jscontext +"/project/" + projectId+"/"+ testsuiteChoose +"/getTestcasesManual",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				lstTestcaseChoose = result.data;
				
				var testcaseNumber = lstTestcaseChoose.length + 1
				$("#testcase_new_copyMan").val(testsuiteChoose + "_man_"+ testcaseNumber);
			} else {
			}
		},
		error : function(request,e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get All Testcase Manual"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
});



$('#modalCopyTestcasesManual').on('hidden.bs.modal', function () {
	$("#messageError").text("");
	})

function hidenModalManual(){
	$('#modalDetailManualTestcase').on('hidden.bs.modal', function () {
		$("#testsuiteMan").attr("disabled", false);
		$("#formGetDataMenualTestcase").trigger("reset");
		$("#testcaseName").prop("readonly", false);
		$("#testcaseName").val("");
		$("#testcaseDesMal").val("");
		$("#expOutput").val("");
		$("#tcProcedure").val("");
		$("#preCondition").val("");
		$('.elementValidate .form-group').removeClass('has-success');
		$('.elementValidate .form-group').removeClass('has-error');
		$('.elementValidate .input-icon i').removeClass('fa-warning').removeClass('fa-check');
		$("#orderIdMan").val("");
	})
}





function getDateTimePicker(){
	var dateNow = new Date();
	var date = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + '-'+ dateNow.getDate();
	$("#createDate").val(date);
	$("#endDate").val($("#createDate").val());
	var createDate = $('#createDate').val();
	$('#createDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false
	});
	$('#createDate').bootstrapMaterialDatePicker('setMinDate', date).on('change', function(e, date) {
		if((new Date($("#createDate").val())).getTime() > new Date($("#endDate").val()).getTime()){
			$("#endDate").val($("#createDate").val());
		} else {
			$("#endDate").val($('#endDate').val());
		}
		
		$('#endDate').bootstrapMaterialDatePicker
		({
			format: 'YYYY-MM-DD',
			lang: 'en',
			weekStart: 1,
			nowButton : false,
			time: false
		});
		if(createDate > $('#createDate').val()){
			$('#endDate').bootstrapMaterialDatePicker('setMinDate', $('#createDate').val());
		}else{
			createDate = $('#createDate').val();
			$('#endDate').bootstrapMaterialDatePicker('setMinDate', $('#endDate').val());
		}
	});
	
	$('#endDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : false,
		time: false
	});
	$('#endDate').bootstrapMaterialDatePicker('setMinDate', $('#endDate').val());
}

function hidenModalMilestone(){
	$('#modalAddMilestone').on('hidden.bs.modal', function () {
		$("#milestoneName").val("");
		$("#descriptionMile").val("");
		getDateTimePicker();
		$("#milestoneName").prop("readonly", false);
	})
}


$('#modalDetailManualTestcase').on('hidden.bs.modal', function () {
	$("#testcaseName").prop("readonly", false);
	$("#testcaseName").val("");
	$("#testcaseDesMal").val("");
	$("#expOutput").val("");
	$("#tcProcedure").val("");
	$("#preCondition").val("");
	$("#orderIdMan").val("");
});

$('#modalUpdateSttDetailManualTestcase').on('hidden.bs.modal', function () {
	$("#manualDescription").val("");
	$("#manualExpectedOutput").val("");
	$("#manualActualOutput").val("");
	$("#loadFiles").empty();
	$("#modalImg").empty();
	$('.elementValidate .form-group').removeClass('has-success');
	$('.elementValidate .form-group').removeClass('has-error');
	$('.elementValidate .input-icon i').removeClass('fa-warning').removeClass('fa-check');
	listImages = [];
	$("#newIssueTcManual").attr("hidden", true);
});

//func validate add manual testcase
var validateAddManualTestcase = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#formGetDataMenualTestcase');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	testcaseName: {
                minlength: 1,
                maxlength:50, 
                required: true,
//                regex:"^[A-Za-z0-9](?!.*?\s$)[A-Za-z0-9\s]{1,50}$",
//                exist:true
            },
            testcaseDescription: {
                required: true,
                minlength: 5,
            },
            tcProcedure: {
                required: true,
                minlength: 5,
            },
            expOutput: {
                required: true,
                minlength: 5,
            },
           
        },     
        messages: {
        	testcaseName:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 1 characters!"),
//        		regex: "not contain characters special,white space!",
        		exist: getLang(currentLocale,"Testcase Name is exist. Please enter another name!")
			},
			testcaseDescription:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	},
        	tcProcedure:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	},
        	expOutput:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	},
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            
            //func check Name Layout is exist
            
    	    var $alertas = $('#new_layout');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
    	    return true;
        }
        
    });
}


var validateAddLastResult = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#formAddLastResultManual');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	manualExpectedOutput: {
                required: true,
                minlength: 5,
            },
            manualActualOutput: {
                required: true,
                minlength: 5,
            },
        },     
        messages: {
        	manualExpectedOutput:{
        		required: getLang(currentLocale,"Cannot be blank"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	},
        	manualActualOutput:{
        		required: getLang(currentLocale,"Cannot be blank"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	},
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
    	    var $alertas = $('#new_layout');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
    	    return true;
        }
        
    });
}

function createMileStone(){
	getDateTimePicker();
	$("#modalAddMilestone .modal-title").text(getLang(currentLocale,"Create Milestone"));
	$('#modalAddMilestone').modal({
		backdrop: 'static'
	});
}

function sendMileStone(){
	if ($('#createprojectMilestone').valid() !=true){
		return;
	}
	$("#saveMilestone").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveMilestone").prop("disabled", true);
	var milestoneIdentity = {
			"projectId": projectId,
			"mileStoneName": $("#milestoneName").val()
	}
	var milestoneProject = {
			"projectMilestoneIdentity": milestoneIdentity,
			"description": $("#descriptionMile").val(),
			"startDate": new Date($("#createDate").val()).toISOString(),
			"endDate": new Date($("#endDate").val()).toISOString(),
			"status": "add"
	}
	$.ajax({
		url : jscontext+"/project/"+glProjectId+"/createProjectMilestone",
		type : "POST",
		data : JSON.stringify(milestoneProject), 
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				$("#modalAddMilestone").modal("hide");
				swal({
			        title: "Create MileStone Success!!!",
			        type: "success",
			        timer: 1000,
			        showConfirmButton: false
			    });
				resetFormMilestone();
				//draw milestone
				getAllMilestoneMan(function(){
					$("#manualDescription").empty();
					$("#mileStoneId").on('change', function(){
						var mileStoneName = $("#mileStoneId").val();
						$.each(listMileStone, function(index, values){
							if(mileStoneName == values.projectMilestoneIdentity.mileStoneName){
								$("#manualDescription").val(values["description"]);
							}
						});
					});
				});
				$("#saveMilestone").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
				$("#saveMilestone").prop("disabled", false);
				$("#milestoneName").val("");
				$("descriptionMile").val("");
				$("#loadFiles").empty();
				$("#modalImg").empty();
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: " Can not save!!!",
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
	
	
}




function closeFile(i){
	$.each(listImages, function(index, vals){
		if(i == index){
			showConfirmMessageFileResultManual(vals, i);
		}
	});
}

function showConfirmMessageFileResultManual(file, i) {
  swal({
      title: getLang(currentLocale,"Are you sure delete File"),
      text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
      closeOnConfirm: false
  }, function () {
	   deleteFileResultManual(file, i);
	   
  });
}

function deleteFileResultManual(file, i){
	listImages.splice(i,1);
	var extensionFile = file.Name.split(".")[1];
	var fileName = file.Id + "." + extensionFile;
	$.ajax({
		url : jscontext + "/project/" + projectId + "/" + $("#testsuiteId").val() + "/deleteFileResultManual",
		type : "POST",
		data: fileName,
		async : false,
		contentType : "application/json",
		success : function(result) {
				swal({
			        title: getLang(currentLocale,"Delete File Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				$("#loadFiles").empty();
				$("#modalImg").empty();
				drawFilesResult(listImages, $("#testsuiteId").val());
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

var validateAddMilestone = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#createprojectMilestone');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod("checkExist",function(value, element, regexp) {
    	var $alertas = $('#createprojectMilestone');
    	var newNameMileStone = $("#milestoneName").val();
    	if(JSON.stringify(listMileStone) != "{}" ){
	    	for(var i in listMileStone){
	        	if(newNameMileStone == i){
			        return false;
	        	}else{
	        		return true;
	        	}
	        }
    	}else{
    		return true;
    	}
    });
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	milestoneName: {
        		required: true,
        		checkExist:true,
        		regex: /^[a-zA-Z0-9\\_]*$/,
        		maxlength: 50,
            }
        },     
        messages: {
        	milestoneName:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		checkExist: "MileStone Name is exist. Please enter another name!!!",
        		regex: getLang(currentLocale,"Cannot special characters!"),
        		maxlength:getLang(currentLocale,"Please enter at less 50 characters!"),
			},
        },
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            //tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
	    	 success2.show();
	         error2.hide();
			var $alertas = $('#createprojectMilestone');
		    var newNameMileStone = $("#milestoneName").val();
		    $alertas.find('.form-group').removeClass('has-success');
		    $alertas.find('.fa').removeClass('fa-check');
		    return true;
        }
        
    });
}
var getProjectProgress = function (glProjectId, glTestsuiteId, glTestcaseId){
	$.ajax({
		url : jscontext + "/project/" + glProjectId + "/" + glTestsuiteId + "/" + glTestcaseId + "/getProjectProgress",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			$("#manualExpectedOutput").val(result.data.expectedOutput);
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


function resetFormMilestone() {
	$('#modalAddMilestone').on('hidden.bs.modal', function() {
		document.getElementById("createprojectMilestone").reset();
	    var $alertas = $('#createprojectMilestone');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}


//----------------------------------------------------------------------------------------------------------------------------------------------
//manual again
function getAllTestcaseManualByProject(){
	drawBugInTestcase(manualTcLs);
	$.ajax({
		url : jscontext+"/project/"+ projectId + "/getAllTestcaseManualByProject",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			manualTcLs= result.data;
//			drawTestcaseManualByProjectId(manualTcLs);
		},
		error : function(request,error)
	      {	
				console.log("Error: "+error);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
	      }
	});
}

function duplicateUserManual(arr){
	var duplicateIds = [];
	var valueDupl = arr
    .map(e => e['lastname'] + e['firstname'])
    .map((e, i, final) => final.indexOf(e) !== i && i)
    .filter(obj=> arr[obj])
    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
	 $.each(valueDupl, function(index, value){
		 $.each(arr, function(indexArr, valueArr){
			 var name = valueArr["lastname"] + valueArr["firstname"]; 
			 if(name == value){
				 duplicateIds.push(valueArr);
			 }
		 })
	 })
	 
	 return duplicateIds ;
}

function drawTestcaseManualByProjectId(manualTcLs){

	$('[data-toggle="tooltip"]').tooltip();
	if(manualTcLs.length == 0){
		$("#totalTcManual").text(0);
		var htmlRecordTestsuites =	'<tr><td colspan="6" class="text-center text-danger">'+getLang(currentLocale,"The testcase manual is empty!!!")+'</td></tr>'
		$("#listTestcaseManual").html(htmlRecordTestsuites);
		
	}else{
		$("#totalTcManual").text(manualTcLs.length);
		$("#listTestcaseManual").empty();
		$.each(manualTcLs, function(index, values){
//			var litsResult = values["results"];
			var testcaseName = values.testcase.split("_");
			var numberTestcase = testcaseName[testcaseName.length-1];
			var linkTestcaseDetail = jscontext + "/project/"+projectId+"/testcaseManual/"+values.testcase;
			var html = '<tr id="'+values.testcase+'">'
				+'<td class="text-center" width="3%" style="cursor: move;">'
				+'<div class="mb-2">'+ numberTestcase+'</div>'
				+'<div class="actionListTable mr-1">'
				+	'<button class="btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success"  id="btnAdd_'+index+'"  data-id="'+index+'" data-name="'+values.testcase+'" testsuite-name="'+values.testsuite+'" onclick="showModalAddNewTestcaseByOrder(\''+values.orderId+'\');">'
				+	'<i class="ti-plus text-white"></i>'
				+	'</button>'
				+	'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites item-tooltip" data-original-title="'+getLang(currentLocale,"Copy Manual Testcase")+'"role="tooltip" href="javascript:void(0);" assign-to="'+values.assignTo+'" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" onclick="funcCopyTestcaseManual(this)"><i class="icon-docs"></i></a>'
				+	'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites item-tooltip" data-original-title="'+getLang(currentLocale,"Delete Testcase")+'" role="tooltip" href="javascript:void(0);" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" onclick="showConfirmMessagesManual(this)"><i class="fa fa-trash-o"></i></a>'
				+	'<a class="btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab" style="background: #0288D1;cursor: move;" id="btnAdd_'+index+'"  data-id="'+index+'" data-name="'+values.testcase+'" testsuite-name="'+values.testsuite+'" onclick="">'
				+	'<i class="ti-move text-white"></i>'
				+	'</a>'
				+'</div>'
				+'</td>'
				+'<td  width="14%" style="cursor: move;">'
				+	'<div class="w-100 d-flex justify-content-between">'
				+		'<a href="javascript:void(0);" class="itemNameTestcase nameManualTestcase mr-3" onclick="getDetailManualTestcase(\''+values.testcase+'\',\''+false+'\')">'+values.testcase+'</a>'
				+		'<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkResultManual(\''+linkTestcaseDetail+'\', \''+"true"+'\')"><i class="fa fa-link"></i></button>'
				+	'</div>'
				+	'<textarea class="font-bold form-control" readonly="readonly" rows="3">'+values.description+'</textarea>'
				+	'<div>'+getLang(currentLocale,"Pre-Condition")+'</div>'
				+	'<textarea class="text-muted font-italic form-control" readonly="readonly" rows="5">'+values.preCondition+'</textarea>';
				if((values.assignTo != null && values.assignTo != "") || (values.statusIssue != null && values.statusIssue != "")){
					html += '<div class="form-group" style="border: 1px solid #1dcb8b;padding: 5px 10px;">';
					var count = 0 ;
					if(duplicateUsers.length > 0){
						$.each(duplicateUsers, function(index,value){
							if(value.username == values.assignTo ){
								html+= '<div style="font-size: 14px">'+(values.assignTo ? "- " + (values.lastname != null && values.firstname != null ? values.lastname + " " +values.firstname +"( "+values.assignTo+" )" : values.assignTo ) : "")+'</div>';
								count ++;
							}					
						})
					} 
					
					if(count == 0 ){
						html+= '<div>'+(values.assignTo ? "- " + (values.lastname != null && values.firstname != null ? values.lastname + " " +values.firstname : values.assignTo ) : "")+'</div>';
					}
						
					html+= '<div>'+(values.statusIssue ? "- " + values.statusIssue : "")+'</div>';
					+'</div>';
				}
				html 				+='</td>'
				+'<td onclick="" width="25%" style="cursor: move;">'
				+	'<div class="scroll-content-manualTestCase">'
				+	'<textarea class="text-muted font-italic form-control" readonly="readonly" rows="12">'+values.testcaseProcedure+'</textarea>'
				+	'</div>'
				+'</td>'
				+'<td width="25%">'
				+	'<div class="scroll-content-manualTestCase">'
				+	'<textarea class="text-muted font-italic form-control" readonly="readonly" rows="12">'+values.expectedOutputTc+'</textarea>'
				+	'</div>'
				+'</td>'
				if(values.testCaseName != null && values.status != "delete"){
					html += '<td width="33%" id="loadResultManual_'+index+'" style="cursor: move;">'
					html += drawLastResults(values,index, "true", "false");
					html += '</td>';
				} else {
					html += '<td width="33%" style="cursor: move;">'
						+	'<div class="theLastResult" rows="12">'
//						+		'<div class="w-50">'
//						+			'<div class="title-content-last-result">'+getLang(currentLocale,"Actual Output")+'</div>'
//						+			'<div class="scroll-content-manualTestCase">'
//						+				'<textarea class="text-muted font-italic form-control" readonly="readonly" rows="9">'+values.actual_output+'</textarea>'
//						+			'</div>'
//						+		'</div>'
//						+		'<div class="pl-3 w-50">'
//						+			'<div class="title-content-last-result">'+getLang(currentLocale,"Last Status")+'</div>'
//						+		'</div>'
						+	'</div>'
						+   '<div class="box-btn-action-manual-result">'
						+		'<div class="">'
						+		 	'<button onclick="addNewLastResult(this)" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 item-tooltip" data-original-title="'+getLang(currentLocale,"Add New Result")+'" role="tooltip">'
				        +			'<i class="ti-plus"></i>'
				       	+		 	'</button>'
						+		'</div>'
						+   '</td>';
				}
//		   html += '<td width="4%">'
//				+	'<div class="actionListTable">'
//				+		'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites item-tooltip" data-original-title="'+getLang(currentLocale,"Copy Manual Testcase")+'"role="tooltip" data-target="#modalCopyTestcasesManual" data-toggle="modal" href="javascript:void(0);" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" onclick="funcCopyTestcaseManual(this)"><i class="icon-docs"></i></a>'
//				+		'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites item-tooltip" data-original-title="'+getLang(currentLocale,"Delete Testcase")+'" role="tooltip" href="javascript:void(0);" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" onclick="showConfirmMessagesManual(this)"><i class="fa fa-trash-o"></i></a>'
//				+	'</div>'
//				+'</td>'
			html		+='</tr>';
			if(values.statusTc == "add"){
				$("#listTestcaseManual").append(html);
			}
			
		});
	}
	
}
var itemInResult = "";
function drawLastResults(values, index, sttCheck, sttUpdate){
	
	var inxlitsResult = index;
	var lengthListResult = 0;
	var html =	'<div class="d-flex theLastResult mb-3">'
		+		'<div class="w-50">'
		+			'<div class="title-content-last-result">'+getLang(currentLocale,"Actual Output")+'</div>'
		+			'<div class="scroll-content-manualTestCase">'
		+ 			'<textarea class="text-muted font-italic form-control" readonly="readonly" rows="11">'+values.actualOutput+'</textarea>'
		+			'</div>'
		+		'</div>'
		+		'<div class="pl-3 w-50">'
		+			'<div class="title-content-last-result"><strong>'+getLang(currentLocale,"Last Status")+'</strong></div>';
	
	var dateData = values.executeDate;
	var testsuiteName = values.testSuite
	var testcaseName = values.testCaseName;
	var testcaseResult = values.testCaseResult;
	var runningUser = "";
	$.each(listUsersMal, function(indexUser, valueUser){
		if(valueUser["username"] == values.runningUser){
			var count = 0 ;
			if(duplicateUsers.length > 0){
				$.each(duplicateUsers, function(index,value){
					if(value.username == values.runningUser ){
						runningUser = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] + " ( "+values.runningUser+" ) " : "");
						count ++;
					}					
				})
			} 
			if(count == 0 ){
				runningUser = (valueUser["lastname"] != null && valueUser["firstname"] != null ? valueUser["lastname"] + " " + valueUser["firstname"] : "");
			}	
		}
	})
	itemInResult = values;
	var dateStart = new Date(dateData);
	dateStart = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate();
	html += '<div>';
	var linkResult = jscontext + "/project/"+projectId+"/"+testsuiteName+"/testcaseManual/"+testcaseName+"/resultDetail/"+values.id;
	if(testcaseResult == "FAILED"){
		html += '<a class="label label-danger label-history w-auto d-inline-block mr-2" href="#" onclick="updateLastResultModal(\''+testcaseName+'\',\''+"false"+'\', \''+sttCheck+'\');">'+testcaseResult+'</a><button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkResultManual(\''+linkResult+'\',\''+"false"+'\')"><i class="fa fa-link"></i></button>'
			 + '<div class="w-100 mt-1">- <strong>'+getLang(currentLocale,"Result Id")+':</strong> '+values.id+'</div>';
	} else if(testcaseResult == "DEGRADE"){
		html += '<a class="label label-degrade label-history w-auto d-inline-block mr-2" href="#" onclick="updateLastResultModal(\''+testcaseName+'\',\''+"false"+'\', \''+sttCheck+'\');">'+testcaseResult+'</a><button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkResultManual(\''+linkResult+'\',\''+"false"+'\')"><i class="fa fa-link"></i></button>'
			 +  '<div class="w-100 mt-1">- <strong>'+getLang(currentLocale,"Result Id")+':</strong> '+values.id+'</div>';
	} else {
		html += '<a class="label label-success label-history w-auto d-inline-block mr-2" href="#" onclick="updateLastResultModal(\''+testcaseName+'\',\''+"false"+'\', \''+sttCheck+'\');">'+testcaseResult+'</a><button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkResultManual(\''+linkResult+'\',\''+"false"+'\')"><i class="fa fa-link"></i></button>'
	}
	html +=		'<div class="w-100 mt-1" style="white-space: nowrap;">- '+dateStart+'</div><div class="w-100" style="word-break: break-all;">- '+runningUser+'</div>'
		 +'</div>';
	html += '<div id="issueManual_'+values.id+'" class="title-content-last-result">'
	
	if(testcaseResult == "FAILED" || testcaseResult == "DEGRADE"){
		getStatusIssue(projectId, values.id, function(taskData){
			if(taskData == null || taskData == undefined){
				if(sttUpdate == "true"){
					$("#issueManual_"+values.id+"").html("");
				} else {
					$("#issueManual_"+values.id+"").html('<button class="btn btn-success label-history mt-2 mb-5" onclick="fucntionAddNewIssue(\''+ projectId+'\', \''+ values.id + '\');"><i class="ti-plus text-white"></i>'+getLang(currentLocale,"Log Bug")+'</button>');
				}
				
			} else {
				if(sttUpdate == "true"){
					$("#issueManual_"+values.id+"").html("");
				} else {
					if(taskData.assignee != ""){
						$.each(listUsersMal, function(indexUser, valueUser){
							if(valueUser["username"] == taskData.assignee){
								var count = 0 ;
								if(duplicateUsers.length > 0){
									$.each(duplicateUsers, function(index,value){
										if(value.username == taskData.assignee ){
											$("#issueManual_"+values.id+"").html('<strong>Issue</strong> <div class="w-100" style="word-break: break-all;">- <a href="'+jscontext + "/project/"+projectId+"/issue/"+taskData.id+'"> '+taskData.status+'</a><div>- '+value["lastname"]+" "+value["firstname"] +' ( '+taskData.assignee+' ) '+'</div></div>');
											count ++;
										}					
									})
								} 
								if(count == 0 ){
									$("#issueManual_"+values.id+"").html('<strong>Issue</strong> <div class="w-100" style="word-break: break-all;">- <a href="'+jscontext + "/project/"+projectId+"/issue/"+taskData.id+'"> '+taskData.status+'</a><div>- '+valueUser["lastname"]+" "+valueUser["firstname"]+'</div></div>');
								}	
							}
						})
					} else{
						$("#issueManual_"+values.id+"").html('<strong>Issue</strong> <div class="w-100" style="word-break: break-all;">- <a href="'+jscontext + "/project/"+projectId+"/issue/"+taskData.id+'"> '+taskData.status+'</a></div>');
					}
					
				}
				
			}
			
		});
	}
	html +='</div>';
	var lstFile = JSON.parse(values["files"]);
	if(lstFile.length > 0 ){
		html += '<div id="modalImg_'+index+'" class="slideshow-container w-100 h-100">'
		$.each(lstFile , function(ite, valueItem){
			var commas = [...valueItem["Name"]].filter(l => l === '.').length;
			var fileContent = valueItem["Name"].split(".")[commas];
			var fileName = valueItem["Id"]+"." + fileContent;
			if(valueItem["Type"].includes("image")){
				html += '<div class="text-center w-100 h-100 divImage" id="divImage_'+index+'_'+ite+'" hidden>'
					+'<span class="close" style="color: white; background: gray;" onclick="closeModalShowImage();">&times;</span>'
					+'<img alt="" id="imageShow_'+index+'_'+ite+'" src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" style="height: 100%;">'
					+'</div>';
			} 
			
	 	          
		});
		html += '</div>';
	}
	
	
	$.each(lstFile , function(ite, valueItem){
		var commas = [...valueItem["Name"]].filter(l => l === '.').length;
		var fileContent = valueItem["Name"].split(".")[commas];
		var fileName = valueItem["Id"]+"." + fileContent;
		if(valueItem["Type"].includes("image")){
			
			html += '<div>- <a href="#" onclick="showImage(\''+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'\', \''+index+'\',\''+ite+'\');" style="text-decoration: underline;">'+valueItem["Name"]+'</a></div>'
		} else if(valueItem["Type"].includes("video")){
			html += '<div>- <a href="#" style="text-decoration: underline;" onclick="showVideo(\''+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'\', \''+valueItem.Type+'\');">'+valueItem["Name"]+'</a></div>';
		} else {
			html += '<div>- <a href="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" style="text-decoration: underline;">'+valueItem["Name"]+'</a></div>';
		}
		
 	          
	});
	
	
	html +='</div></div>';
	
	html +=  '<div class="box-btn-action-manual-result mt-5" id="">';
	html += '<div class="">'
		+		 	'<button onclick="addNewLastResult(this)" testsuite-name="'+testsuiteName+'" data-name="'+testcaseName+'"  class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2 item-tooltip" data-original-title="'+getLang(currentLocale,"Add New Result")+'" data-toggle="tooltip" data-placement="top">'
	    +			'<i class="ti-plus"></i>'
	   	+		 	'</button>';
	   	if(sttUpdate == "true"){
	   	    html +='<button hidden id="updateLast_'+testcaseName+'" onclick="updateLastResultModal(\''+testcaseName+'\',\''+"true"+'\', \''+sttCheck+'\');" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2 item-tooltip" data-original-title="'+getLang(currentLocale,"Edit The Result")+'" data-toggle="tooltip" data-placement="top">';
	   	} else {
	   		html +='<button id="updateLast_'+testcaseName+'" onclick="updateLastResultModal(\''+testcaseName+'\',\''+"true"+'\',\''+sttCheck+'\');" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2 item-tooltip" data-original-title="'+getLang(currentLocale,"Edit The Result")+'" data-toggle="tooltip" data-placement="top">';
	   	}		 	
	html    +=			 '<i class="ti-pencil-alt"></i>'
	   	+		 	'</button>';
	   	
	   
	html +=			'<button id="preResult_'+inxlitsResult+'" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2 item-tooltip" data-original-title="'+getLang(currentLocale,"Older result")+'" data-toggle="tooltip" data-placement="top" onclick="preResultManual(\''+ testsuiteName +'\',\''+ testcaseName +'\',\''+ values.id +'\', \''+ inxlitsResult +'\')">'
	+				'<i class="icon-arrow-left"></i>'
	+			'</button>';
   	
	
	html += '<button id="nextResult_'+inxlitsResult+'" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2 item-tooltip" data-original-title="'+getLang(currentLocale,"Next The Result")+'" data-toggle="tooltip" data-placement="top" onclick="nextManual(\''+ testsuiteName +'\',\''+ testcaseName +'\',\''+ values.id +'\', \''+ inxlitsResult +'\')">'
		 +				'<i class="icon-arrow-right"></i>' 
		 +			'</button>'	
	html +=		'</div>'
	 + 	'</div>';
	return html;
}

var listUsersMal = '';
function getListBottestUserMal(){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getUsers",
		type : "get",
		data : "",
		contentType : "application/json", 
		async : false,
		success : function(result) {
			if (!(result.errormessage) ) {
				listUsersMal = result.data;	
				duplicateUsers = duplicateUserManual(listUsersMal);
			} 
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

var scrollTop = "";
function showImage(srcStr, itemTc, itemImg){
	scrollTop = $("html,body").scrollTop();
	var modalHtml = $("#modalImg_"+itemTc+"").html();
	modalHtml = modalHtml.replace(/divImage/g,"divImages");
	$("#modalShowImage").modal("show");
	$("#bodyShowImg").attr("hidden", false);
	$("#showVideo").attr("hidden", true);
	$("#modalImge").empty();
	$("#modalImge").append(modalHtml);
	$("#modalImge").append('<a class="prev" onclick="plusSlidesImg(-1, 0)">&#10094;</a><a class="next" onclick="plusSlidesImg(1, 0)">&#10095;</a>');
	var modal = document.getElementById("modalImge");
	
	modal.style.display = "block";
	$("#divImages_"+itemTc+"_"+itemImg+"").attr("hidden", false);

}

function showVideo(srcStr, typeStr){
	scrollTop = $("html,body").scrollTop();
	$("#modalShowImage").modal("show");
	$("#bodyShowImg").attr("hidden", true);
	$("#showVideo").attr("hidden", false);
	$("#showVideo").empty();
	var html = '<div class="text-center w-100 h-100" style="margin: auto;"><span class="close" style="color: white; background: gray;z-index:1;" onclick="closeModalShowImage();">&times;</span><video controls class="w-100 h-100">'
			+'<source id="" src="'+srcStr+'" type="'+typeStr+'">'
		+'</video></div>'
		$("#showVideo").append(html);
}

var slideIndex = [1];
var slideIds = ["divImages"];

function plusSlidesImg(n, no) {
	showSlidesImg(slideIndex[no] += n, no);
}

function showSlidesImg(n, no) {
	var i;
	var x = $("."+slideIds[no]+"");
	if (n > x.length) { slideIndex[no] = 1 }
	if (n < 1) { slideIndex[no] = x.length }
	for (i = 0; i < x.length; i++) {
		$(x[i]).attr("hidden", true);
	}
	$(x[slideIndex[no] - 1]).attr("hidden", false);
}

function closeModalShowImage(){
	$("#modalShowImage").modal("hide");
	$('html, body' ).scrollTop(scrollTop);
	scrollTop = "";
}

function copyLinkResultManual(link, status){
	$('#modalCopyLink').modal({
		backdrop: 'static'
	});
	if(status != "true"){
		$("#titleFormCopy").text(getLang(currentLocale,'Copy link result manual'));
	} else {
		$("#titleFormCopy").text(getLang(currentLocale,'Copy link detail testcase manual'));
	}
	
	$("#copyItem").val(window.location.origin + link);
	
}

function copyLink(){
	var copyText = document.getElementById("copyItem");
	copyText.select();
	copyText.setSelectionRange(0, 99999);
	document.execCommand("copy");
	$("#modalCopyLink").modal('hide');
}

function resetFormCopy(){
	$('#modalCopyLink').on('hidden.bs.modal', function() {
		document.getElementById("copyLink").reset();
	    var $alertas = $('#copyLink');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

function addNewManualTestcase(){
	$("#modalDetailManualTestcase .modal-title").text(getLang(currentLocale,"Add New Manual Testcase"));
	
	$("#testsuiteMan").empty();
	getListTestsuite("false", function(testsuitels){
			
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteMan").append(htmlOptions);
		
	});
	infProjectCom(function(projectCo){
		$("#statustaskTc").empty();
		$("#categorytaskTc").empty();
		var html = "";
		var htmlCategory = "";
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Status"){
				html += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			} else if(projectCo[i]["groupCode"] == "Category"){
				htmlCategory += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			}
		}
		$("#statustaskTc").append(html);
		$("#categorytaskTc").append(htmlCategory);
	});
	
	$("#userBottestAss").empty();
	getAllUsersByProjectId("false", function(lsDataUser){
		var html = '';
		$.each(lsDataUser, function(index, vals){
			if( vals["lastname"] != null && vals["firstname"] != null ){
				var count = 0;
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == vals["username"] ){
							html +=  '<option value="'+vals["username"]+'">'+ vals["lastname"]+" "+vals["firstname"]+' ( '+vals["username"]+')'+'</option>';
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					 html +=  '<option value="'+vals["username"]+'">'+ vals["lastname"]+" "+vals["firstname"]+'</option>';
				}
			}
		});
		$("#userBottestAss").append(html);
	});
	
	$("#btnSaveNewManualTsc").show();
	$("#btnUpdateNewManualTsc").hide();
	$('#modalDetailManualTestcase').modal({
		backdrop: 'static'
	});
	
	getAllMilestoneInIssue(function(milestones){
		$("#milestoneManulTC").empty();
		var htmlOptions = "";
		var number = 0;
		$.each(milestones,function(index, value){
			var dateStart = new Date(value["startDate"]);
			var dateEnd = new Date(value["endDate"]);
			var dateStartStr = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate()+" "+dateStart.getHours()+":"+dateStart.getMinutes()+":"+dateStart.getSeconds();
			var dateEndStr = dateEnd.getFullYear() + "-" + (dateEnd.getMonth() + 1) + '-'+ dateEnd.getDate()+" "+dateEnd.getHours()+":"+dateEnd.getMinutes()+":"+dateEnd.getSeconds();
			var dateNow = new Date().getTime();
			if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){
				if(number == 0){
					htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" selected>'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
					number ++;
				} else{
					htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
				}
			} else{
				htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
			}
		})
		$("#milestoneManulTC").append(htmlOptions);
	})
}

function getListTestsuite(status, callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				if(status == "true"){
					tsByProjectId = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.testsuite;
						obTs["text"] = values.testsuite;
						tsByProjectId.push(obTs);
					});
					callbackF(tsByProjectId);
				} else {
					callbackF(result.data);
				}
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getAllUsersByProjectId(status, callbackF){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getUsers",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				if(status == "true"){
					var lsUser = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						if(values.lastname != null  && values.firstname != null){
							obTs["id"] = values.username;
							var count = 0;
							if(duplicateUsers.length > 0){
								$.each(duplicateUsers, function(index,value){
									if(value.username ==values.username){
										obTs["text"] =  values.lastname  +" "+values.firstname +" ( " + values.username + " ) ";	
										count ++;
									}					
								})
							} 
							
							if(count == 0 ){
								obTs["text"] =  values.lastname  +" "+values.firstname;	
							}
							lsUser.push(obTs);
						}
					});
					callbackF(lsUser);
				} else {
					callbackF(result.data);
				}
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

function saveNewManualTsc(){
	
	if ($('#formGetDataMenualTestcase').valid() !=true){
		return;
	}
	$("#btnSaveNewManualTsc").html("<i class='fa fa-circle-o-notch fa-spin'></i>"+getLang(currentLocale,"Please waiting!"));
	$("#btnSaveNewManualTsc").prop("disabled", true);
	var orderId = ($("#orderIdMan").val() ? parseInt($("#orderIdMan").val())+1: 0);
	var glTestsuiteId = $("#testsuiteMan option:selected").text();
	var projectPro = {
			"projectId": glProjectId,
			"testsuite": glTestsuiteId,
			"testcase": ""
	}
	var manualTestcase = {
			"projectProgressIdentity": projectPro,
		    "description": $("#testcaseDesMal").val(),
		    "expectedOutput": $("#expOutput").val(),
		    "testcaseProcedure": $("#tcProcedure").val(),
		    "updateDate": new Date().toISOString(),
		    "testscript": "",
		    "createDate": new Date().toISOString(),
		    "updateUser": glProjectId,
		    "assignTo": $("#userBottestAss").val(),
		    "preCondition": $("#preCondition").val(),
		    "status": "add",
		    "type": "manual",
		    "statusIssue": $("#statustaskTc option:selected").text(),
		    "planStartDate": $("#planStarDa").val(),
		    "planEndDate": $("#planEndDa").val(),
		    "estimatedTime": $("#estimatedTime").val(),
		    "actualTime": $("#actualTime").val(),
		    "category": $("#categorytaskTc option:selected").text(),
		    "orderId": orderId,
		    "milestone": $("#milestoneManulTC option:selected").val(),
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+glTestsuiteId+"/saveTestCaseManual",
		type : "POST",
		data : JSON.stringify(manualTestcase),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalDetailManualTestcase").modal("hide");
				
				swal({
				    type: 'success',
				    title: getLang(currentLocale,"Create Testcase")+" "+ result.data +" "+getLang(currentLocale,"Success"),
				    timer: 2000,
			        showConfirmButton: false
				});
				
				//draw testcase manual
				getAllTestcaseManualByProject();
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue,categoryIssue,milestoneTestcase);
//				getAllTestcasesByStatusManual();
//				drawTestsuiteReport();
				$("#btnSaveNewManualTsc").html("<i class='ti-check'></i> "+getLang(currentLocale,"Save"));
				$("#btnSaveNewManualTsc").prop("disabled", false);
				
				$("#testcaseDesMal").val("");
				$("#expOutput").val("");
				$("#tcProcedure").val("");
				$("#preCondition").val("");
				$("#orderIdMan").val("");
			} else {
			}
		},
		error : function(e) {
			swal({
		        title: getLang(currentLocale,"Can not save!!!"),
		        type: "warning",
		        timer: 2000,
		        showConfirmButton: false
		    });
//			window.location= jscontext + "/login";
		}
	});
}

//edit testcase manual
function getDetailManualTestcase(index, status){
	$("#modalDetailManualTestcase .modal-title").text(getLang(currentLocale,"Update Manual Testcase"));
	var valItem = "";
	$.each(manualTcLs, function(ind,vals){
		if(vals.testcase == index){
			valItem = vals;
		}
	});
	$("#modalDetailManualTestcase .modal-title").text(getLang(currentLocale,"Update Manual Testcase"));
	$("#btnSaveNewManualTsc").hide();
	$("#btnUpdateNewManualTsc").show();
	$("#checkScreen").val(status);
	
	$("#testsuiteMan").empty();
	getListTestsuite("false", function(testsuitels){
			
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteMan").append(htmlOptions);
		$('#testsuiteMan option[value="'+valItem["testsuite"]+'"]').prop("selected",true);
		$("#testsuiteMan").attr("disabled", true);
	});
	
	
	$("#testcaseDesMal").val(valItem["description"]);
	
	infProjectCom(function(projectCo){
		$("#statustaskTc").empty();
		$("#categorytaskTc").empty();
		var html = "";
		var htmlCategory = "";
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Status"){
				html += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			} else if(projectCo[i]["groupCode"] == "Category"){
				htmlCategory += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			}
		}
		$("#statustaskTc").append(html);
		$("#categorytaskTc").append(htmlCategory);
		$('#statustaskTc option[value="'+valItem.statusIssue+'"]').prop("selected",true);
		$('#categorytaskTc option[value="'+valItem.category+'"]').prop("selected",true);
	});
	
	$("#userBottestAss").empty();
	getAllUsersByProjectId("false", function(lsDataUser){
		var html =  '';
		$.each(lsDataUser, function(index, vals){
			if(vals["lastname"] != null && vals["firstname"] != null ){
				var count = 0; 
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == vals["username"]){
							html += '<option value="'+vals["username"]+'">'+vals["lastname"] +" "+vals["firstname"] +" ( "+vals["username"]+" ) "+'</option>';	
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					html += '<option value="'+vals["username"]+'">'+vals["lastname"] +" "+vals["firstname"]+'</option>';	
				}
			} 
		});
		$("#userBottestAss").append(html);
		$('#userBottestAss option[value="'+valItem.assignTo+'"]').prop("selected",true);
	});
	
	$("#preCondition").val((valItem.preCondition ? valItem.preCondition: ""));
	$("#tcProcedure").val((valItem.testcaseProcedure ? valItem.testcaseProcedure: ""));
	$("#expOutput").val((valItem.expectedOutputTc ? valItem.expectedOutputTc: ""));
	$("#dateCreateStart").val((valItem.createDate ? valItem.createDate: ""))
	$("#manualTestcaseName").val((valItem.testcase? valItem.testcase: ""));
	$("#planStarDa").val((valItem.planStartDate? new Date(valItem.planStartDate).toISOString().split("T")[0]: ""));
	$("#planEndDa").val((valItem.planEndDate? new Date(valItem.planEndDate).toISOString().split("T")[0]:""));
	$("#estimatedTime").val((valItem.estimatedTime ? valItem.estimatedTime :""));
	$("#actualTime").val((valItem.actualTime ? valItem.actualTime : ""));
	
	$('#modalDetailManualTestcase').modal({
		backdrop: 'static'
	});
	
	getAllMilestoneInIssue(function(milestones){
		$("#milestoneManulTC").empty();
		var htmlOptions = "";
		var number = 0;
		$.each(milestones,function(index, value){
			htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
		})
		$("#milestoneManulTC").append(htmlOptions);
		$('#milestoneManulTC option[value="'+valItem.milestoneTc+'"]').attr('selected','selected');
	})	
}

function updateManualTsc(){
	if ($('#formGetDataMenualTestcase').valid() !=true){
		return;
	}
	$("#testcaseName").prop("readonly", false);
	$("#btnUpdateNewManualTsc").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#btnUpdateNewManualTsc").prop("disabled", true);
	var glTestsuiteId = $("#testsuiteMan option:selected").text();
	var projectPro = {
			"projectId": projectId,
			"testsuite": glTestsuiteId,
			"testcase": $("#manualTestcaseName").val()
	}
	var dateCreate;
	try{
		dateCreate = new Date($("#dateCreateStart").val()).toISOString();
	} catch(err){
		dateCreate = $("#dateCreateStart").val();
	}
	var manualTestcase = {
			"projectProgressIdentity": projectPro,
		    "description": $("#testcaseDesMal").val(),
		    "expectedOutput": $("#expOutput").val(),
		    "testcaseProcedure": $("#tcProcedure").val(),
		    "updateDate": new Date().toISOString(),
		    "testscript": "",
		    "createDate": dateCreate,
		    "updateUser": projectId,
		    "assignTo": $("#userBottestAss").val(),
		    "preCondition": $("#preCondition").val(),
		    "status": "add",
		    "type": "manual",
		    "statusIssue": $("#statustaskTc option:selected").text(),
		    "planStartDate": $("#planStarDa").val(),
		    "planEndDate": $("#planEndDa").val(),
		    "estimatedTime": $("#estimatedTime").val(),
		    "actualTime": $("#actualTime").val(),
		    "category": $("#categorytaskTc option:selected").text(),
		    "milestone": $("#milestoneManulTC option:selected").val(),
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+glTestsuiteId+"/editTestcaseManual",
		type : "POST",
		data : JSON.stringify(manualTestcase),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#testsuiteMan").attr("disabled", false);
				$("#modalDetailManualTestcase").modal("hide");
				swal({
				    type: 'success',
				    title: "Edit Testcase "+ $("#manualTestcaseName").val() + " Success",
				    timer: 2000,
			        showConfirmButton: false
				});
				//update manLs
				getAllTestcaseManualByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue, milestoneTestcase);
//				getAllTestcasesByStatusManual();
				$("#btnUpdateNewManualTsc").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
				$("#btnUpdateNewManualTsc").prop("disabled", false);
				if($("#checkScreen").val() == "true"){
					//load lai issue
					drawTaskFilter(trackerParse, assigneeParse, statusParse, priorityParse,categoryParse, planStartSS, planEndSS, testsuiteTParse, milestoneTParse, descriptionSS);
				}
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title:  getLang(currentLocale,'Can not save!!!'),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
}

function showConfirmMessagesManual(isThis) {
	 var testcaseName = $(isThis).attr("data-name");
	 var testsuiteName = $(isThis).attr("testsuite-name");
  swal({
      title: getLang(currentLocale,"Are you sure delete TestCase: ") + testcaseName +"?",
      text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
      closeOnConfirm: false
  }, function () {
  	deleteTestCaseManual(testcaseName, testsuiteName);
  	swal({
       title: getLang(currentLocale,"Delete TestCase Success!!!"),
       type: "success",
       timer: 2000,
       showConfirmButton: false
   });
  });
}

function deleteTestCaseManual(testcaseName, testsuiteName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/deleteTestCaseManual",
		type : "POST",
		data : testcaseName,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				getAllTestcaseManualByProject();
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue,milestoneTestcase);
//				drawTestsuiteReport();s
			} else {
			}
			
		},
		error : function(request,e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function funcCopyTestcaseManual(isThis){
	
	$('#modalCopyTestcasesManual').modal({
		backdrop: 'static'
	});
	$("#copyTestcaseIdMan").hide();
	$("#copyTestcaseIdManuals").show();
	$("#testsuiteListManual").empty();
	var testcaseName = $(isThis).attr("data-name");
	var testsuiteName = $(isThis).attr("testsuite-name");
	$("#testcaseNameOldMan").text(testcaseName);
	
	$("#testsuiteListManual").empty();
	getListTestsuite("false", function(testsuitels){
			
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteListManual").append(htmlOptions);
		$('#testsuiteListManual option[value="'+testsuiteName+'"]').prop("selected",true);
	});
	
}

//coppy Testcase
function copyTestCasesManual(){
	
	$("#copyTestcaseIdManuals").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#copyTestcaseIdManuals").prop("disabled", true);

	var testcaseNameOld = $("#testcaseNameOldMan").text();
	
	var testsuiteChoose = $("#testsuiteListManual option:selected").text();
	
	var testCase = "";
	$.each(manualTcLs, function(index, value){
		if(value.testcase == testcaseNameOld){
			testCase =  Object.assign({}, value);
			return false;
		}
	});
	var projectPro = {
			"projectId": projectId,
			"testsuite": testsuiteChoose,
			"testcase": ""
	}
	var manualTestcase = {
			"projectProgressIdentity": projectPro,
		    "description": testCase.description,
		    "expectedOutput": testCase.expectedOutputTc,
		    "testcaseProcedure": testCase.testcaseProcedure,
		    "updateDate": new Date().toISOString(),
		    "testscript": "",
		    "createDate": new Date().toISOString(),
		    "updateUser": projectId,
		    "assignTo": testCase.assignTo,
		    "preCondition": testCase.preCondition,
		    "status": "add",
		    "type": "manual",
		    "statusIssue": "New",
	    	"orderId": 0,
	    	"planStartDate": (testCase.planStartDate ? new Date(testCase.planStartDate).toISOString(): ""),
		    "planEndDate": (testCase.planEndDate ? new Date(testCase.planEndDate).toISOString() : ""),
		    "estimatedTime": (testCase.estimatedTime ? testCase.estimatedTime: ""),
		    "actualTime": (testCase.actualTime ? testCase.actualTime: ""),
		    "category": (testCase.category ? testCase.category: ""),
		    "milestone": (testCase.milestone ? testCase.milestone: "")
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteChoose+"/saveTestCaseManual",
		type : "POST",
		data : JSON.stringify(manualTestcase),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalCopyTestcasesManual").modal("hide");
				swal({
				    type: 'success',
				    title: result.data +" "+ getLang(currentLocale,"testcase was created"),
				    timer: 2000,
			        showConfirmButton: false
				});
				getAllTestcaseManualByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue,categoryIssue,milestoneTestcase);
//				getAllTestcasesManual();
//				getAllTestcasesByStatusManual();
				$("#copyTestcaseIdManuals").html("<i class='ti-check'></i>  "+ getLang(currentLocale,'Save'));
				$("#copyTestcaseIdManuals").prop("disabled", false);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});

}

function addNewLastResult(isThis){
	$("#modalUpdateSttDetailManualTestcase .modal-title").text(getLang(currentLocale,"Add New Result"));	
	
//	$("#loaderModalManualTCS").show();
	$('#modalUpdateSttDetailManualTestcase').modal({
		backdrop: 'static'
	});
	
	$("#resultTab").addClass("active show");
	
	var testcaseName = $(isThis).attr("data-name");
	var testsuiteName = $(isThis).attr("testsuite-name");
	$("#testsuiteId").val(testsuiteName);
	getProjectProgress(projectId, testsuiteName, testcaseName);
	$("#manualExpectedOutput").prop("readonly",false);
	$("#manualExpectedOutput").val("");
	$("#idResult").text("#");
	
	$("#manualRunningUser").empty();
	getAllUsersByProjectId("false", function(lsDataUser){
		var html = '';
		$.each(lsDataUser, function(index, vals){
			if(vals["lastname"] != null && vals["firstname"] != null){
				var count = 0; 
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == vals["username"]){
							html += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+" ( " +vals["username"]+" ) "+'</option>';
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					html += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+'</option>';
				}
			}	
		});
		$("#manualRunningUser").append(html);
		$('#manualRunningUser option[value="'+$(".username.username-hide-on-mobile").text()+'"]').prop("selected",true);
	});
	
	var dateNow = new Date();
	var date = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + '-'+ dateNow.getDate()+" "+dateNow.getHours()+":"+dateNow.getMinutes()+":"+dateNow.getSeconds();
	$("#datetimeLastRun").val(date);
	$('#datetimeLastRun').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD HH:mm:ss',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
	});
	$('#datetimeLastRun').bootstrapMaterialDatePicker('setMinDate', date);
	
	$("#testcaseNameS").val(testcaseName);
	
	$("#addNewResultManualTcs").show();
	$("#updateNewResultManualTcs").hide();
//	$('#modalUpdateSttDetailManualTestcase').on('shown.bs.modal', function (event) {
	getAllMilestoneMan(function(){
		$("#mileStoneId").on('change', function(){
			var mileStoneName = $("#mileStoneId").val();
			$.each(listMileStone, function(index, values){
				if(mileStoneName == values.projectMilestoneIdentity.mileStoneName){
					$("#manualDescription").val(values["description"]);
				}
			});
		});
//		$("#loaderModalManualTCS").fadeOut("slow");
	});
//	});
	if(listImages.length != 0){
		drawFilesResult(listImages, testsuiteName);
	}
}

function getAllMilestoneMan(callbackF){
	
//	getMileStones
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getAllMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				listMileStone = result.data;
				$("#mileStoneId").empty();
				var i = 0;
				$.each(listMileStone, function(index, vals){
					i += 1;
					var html = '<option value="'+vals.projectMilestoneIdentity.mileStoneName+'">'+vals.projectMilestoneIdentity.mileStoneName+'</option>';
					$("#mileStoneId").append(html);
					if(i == 1){
						$("#manualDescription").val(vals.description);
					}
				});
				callbackF();
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: " Can get data!!!",
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}

function saveResultManuals(){
	if ($('#formAddLastResultManual').valid() !=true){
		return;
	}
	$("#addNewResultManualTcs").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#addNewResultManualTcs").prop("disabled", true);
	var testsuiteName = $("#testsuiteId").val();
	var resultInfo = {
			"testSuite": testsuiteName,
			"runningUser": $("#manualRunningUser").val(),
			"executeDate": new Date($('#datetimeLastRun').val()).toISOString(),
			"testCaseName": $("#testcaseNameS").val(),
			"testCaseResult": $("#manualStatus").val(),
			"projectId": projectId,
			"milestone": $("#mileStoneId").val(),
			"os": $("#manualOS").val(),
			"browser": $("#manualBrowser").val(),
			"screenResolution": $("#manualResolution").val(),
			"location": $("#manualLocation").val(),
			"mobileMode": $("#mobileModeResult").prop("checked").toString(),
			"deviceName": $("#manualDeviceName").val(),
			"runningDescription": $("#manualDescription").val(),
			"expectedOutput": $("#manualExpectedOutput").val(),
			"actualOutput": $("#manualActualOutput").val(),
			"testResult":"",
			"id":"",
			"resultName": "",
			"type": "manual",
			"files": JSON.stringify(listImages),
			"status": "add"
	}
	
	$.ajax({
		url : jscontext + "/project/" + projectId + "/" + testsuiteName + "/addResultManual",
		type : "POST",
		data : JSON.stringify(resultInfo),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalUpdateSttDetailManualTestcase").modal("hide");
				swal({
			        title: getLang(currentLocale,"Add Result Success!!!"),
			        type: "success",
			        timer: 1000,
			        showConfirmButton: false
			    });
				getAllTestcaseManualByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue,milestoneTestcase);
				$("#addNewResultManualTcs").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
				$("#addNewResultManualTcs").prop("disabled", false);
				$("#manualDescription").val("");
				$("#manualExpectedOutput").val("");
				$("#manualActualOutput").val("");
				listImages = [];
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not save!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
}


function updateLastResult(idResult, testcaseName, testsuiteName, status, checkissue){
	$("#modalUpdateSttDetailManualTestcase .modal-title").text(getLang(currentLocale,"Update The Result"));
	$.ajax({
		url : jscontext + "/project/" + projectId + "/" + testsuiteName+"/"+ testcaseName + "/getResultInfoTestcaseManual/"+ idResult,
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var resultItem = result.data;
				showLastResultManual(resultItem, status);
	
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not save!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
}

function updateLastResultModal(testcaseName, status , sttCheck){
	var resultItem = "";
	if(sttCheck == "true"){
		$.each(manualTcLs, function(ind,vals){
			if(vals.testcase == testcaseName){
				resultItem = vals;
			}
		});
	} else {
		resultItem = itemInResult;
	}
	
	showLastResultManual(resultItem, status);
}

function showLastResultManual(resultItem, status){
	var testsuiteName = resultItem.testSuite; 
	$("#testsuiteId").val(testsuiteName);
	
	$("#loaderModalManualTCS").show();
	$("#modalUpdateSttDetailManualTestcase").modal("show");
	
	$("#resultTab").addClass("active show");
	getAllMilestoneMan(function(){
		$("#modalUpdateSttDetailManualTestcase .modal-title").text(getLang(currentLocale,"Update The Result"));
		$("#addNewResultManualTcs").hide();
		$("#updateNewResultManualTcs").show();
		//dung de khi edit thi chi cho xem, ko cho sua
		if (status =="true") {
			$("#updateNewResultManualTcs").attr("hidden",false);
			$("#closeModalResultManuals").attr("hidden",false);
			getStatusIssue(resultItem.projectId, resultItem.id, function(taskData){
				if(taskData == null){
					if(resultItem.testCaseResult == "FAILED"){
						$("#newIssueTcManual").attr("hidden",false);
						$("#newIssueTcManual").show();
						$("#newIssueTcManual").attr("data-id", resultItem["id"]);
						$("#newIssueTcManual").attr("data-testcase", testcaseName);
						$("#newIssueTcManual").attr("data-testsuite", testsuiteName);
					}
					
				}
			});
			
			
		} else {
			$("#updateNewResultManualTcs").attr("hidden",true);
			$("#closeModalResultManuals").attr("hidden",true);
			$("#newIssueTcManual").attr("hidden",true);
		}
		
		$("#idResult").text(resultItem["id"]);
		
		$("#mileStoneId option").prop("selected",false);
		$.each(listMileStone, function(index, values){
			var nameOptionLocation = values.projectMilestoneIdentity.mileStoneName;
			if(nameOptionLocation == resultItem["milestone"]){
				$('#mileStoneId option[value="'+nameOptionLocation+'"]').prop("selected",true);
			}
		});
		
		$("#manualRunningUser option").prop("selected",false);
		$('#manualRunningUser option[value="'+resultItem["os"]+'"]').prop("selected",true);
		
		$('#manualBrowser option[value="'+resultItem["browser"]+'"]').prop("selected",true);
		
		$('#manualResolution option[value="'+resultItem["screenResolution"]+'"]').prop("selected",true);
		
		$('#manualLocation option[value="'+resultItem["location"]+'"]').prop("selected",true);
		
		if(resultItem["mobileMode"] == 'false'){
			$("#mobileModeResult").prop("checked",false);
		}else{
			$("#mobileModeResult").prop("checked",true);
		}
		
		$('#manualDeviceName option[value="'+resultItem["deviceName"]+'"]').prop("selected",true);
		
		$('#manualStatus option[value="'+resultItem["testCaseResult"]+'"]').prop("selected",true);
		
		$("#manualRunningUser").empty();
		getAllUsersByProjectId("false", function(lsDataUser){
			var html = '';
			$.each(lsDataUser, function(index, vals){
				if(vals["lastname"] != null && vals["firstname"] != null){
					var count = 0; 
					if(duplicateUsers.length > 0){
						$.each(duplicateUsers, function(index,value){
							if(value.username == vals["username"]){
								html += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+" ( " +vals["username"]+" ) "+'</option>';
								count ++;
							}					
						})
					} 
					
					if(count == 0 ){
						html += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+'</option>';
					}
				}		
			});
			$("#manualRunningUser").append(html);
			$('#manualRunningUser option[value="'+$(".username.username-hide-on-mobile").text()+'"]').prop("selected",true);
		});
		
		var dateNow = new Date();
		var date = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + '-'+ dateNow.getDate()+" "+dateNow.getHours()+":"+dateNow.getMinutes()+":"+dateNow.getSeconds();
		$("#datetimeLastRun").val(date);
		$('#datetimeLastRun').bootstrapMaterialDatePicker
		({
			format: 'YYYY-MM-DD HH:mm:ss',
			lang: 'en',
			weekStart: 1,
			nowButton : true,
		});
		$('#datetimeLastRun').bootstrapMaterialDatePicker('setMinDate', date);
		
		$("#testcaseNameS").val(resultItem["testCaseName"]);
		
		$("#manualDescription").val(resultItem["runningDescription"]);
//		$("#manualExpectedOutput").val(resultItem["expectedOutput"]);
		$("#manualExpectedOutput").prop("readonly", true);
		$("#manualActualOutput").val(resultItem["actualOutput"]);
		getProjectProgress(projectId, testsuiteName, resultItem["testCaseName"]);
		
		var listFile = JSON.parse(resultItem["files"]);
		listImages = listFile;
		$.each(listFile , function(ite, valueItem){
			
			var html = '<div class="col-md-3 preview p-0" id="imageResult_'+ite+'">';
			var commas = [...valueItem["Name"]].filter(l => l === '.').length;
			var fileContent = valueItem["Name"].split(".")[commas];
			var fileName = valueItem["Id"]+"." + fileContent;
			if(valueItem.Type.includes("image")){
				html += '<div class="component-file-upload-result p-0">'
							+'<img onclick="clickDetailImg(\''+ite+'\');" id="imgResult'+ite+'" src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" alt="" class="img-thumbnail border-0 path-img-result">'
						+'</div>';
			} else if(valueItem.Type.includes("video")){
				html += '<div class="component-file-upload-result p-0">'
					 +'<div class="w-100 h-100"><video controls class="w-100 h-100"><source id="imgResult'+ite+'"  src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" type="'+valueItem.Type+'"></video>'
					+'</div></div>';
			}else{
				html += '<div class="component-file-upload-result">'
							+'<div class="w-100 item-file-upload-result"><i class="fa fa-file"></i>'
								+'<div class="w-100 mb-3 file-name-result"><a href="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" download>'+fileName+'</a></div>'
								+'<a href="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" class="icon-download-file-result" download><i class="ti-cloud-down"></i></a>'
							+'</div>'
						+'</div>';
			}
			            
	         html   += '<div class="close-image-result" onclick="closeFile('+ite+')">'
		        	 	+'<i class="ti-close"></i>'
		                +'</div>'
	                +'</div>';
	                
			$("#loadFiles").append(html);
			
			var imgHtml = '<div class="text-center w-100 h-100 divResult" id="divResult_'+ite+'" hidden>'
					+'<span class="close" onclick="closeImageResult(\''+ite+'\')" id="closeImgFile_'+ite+'">&times;</span>'
						+'<img class="pictureResult" id="pictureResult_'+ite+'" src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" style="height: 100%;display: none;">'
					+'</div>';
			if(valueItem.Type.includes("image")){
				$("#modalImg").append(imgHtml);
			}
			
		});
		$("#modalImg").append('<a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a><a class="next" onclick="plusSlides(1, 0)">&#10095;</a>');
		
		$("#loaderModalManualTCS").fadeOut();
		
	});
	
}

function clickDetailImg(ite){
	var modal = document.getElementById("modalImg");
	var img = document.getElementById("imgResult"+ite);
	var modalImg = document.getElementById("pictureResult_"+ite+"");
	modal.style.display = "block";
	modalImg.style.display = "initial";
	$("#divResult_"+ite+"").attr("hidden", false);
//	modalImg.src = this.src;
}

function closeImageResult(ite){
	var picture = document.getElementsByClassName("pictureResult");
	$.each(picture, function(ind, values){
		values.style.display = "none";
	});
	var divRs = $(".divResult");
	$.each(divRs, function(ind, values){
		$(values).attr("hidden", true);
	});
	$('#modalImg').hide();
}

var slideIndex = [1];
var slideId = ["divResult"];

function plusSlides(n, no) {
	showSlides(slideIndex[no] += n, no);
}

function showSlides(n, no) {
	var i;
	var x = $("."+slideId[no]+"");
	if (n > x.length) { slideIndex[no] = 1 }
	if (n < 1) { slideIndex[no] = x.length }
	for (i = 0; i < x.length; i++) {
		$(x[i]).attr("hidden", true);
		$($(x[i]).find(".pictureResult")).css("display", "none");
	}
	$(x[slideIndex[no] - 1]).attr("hidden", false);
	$($(x[slideIndex[no] - 1]).find(".pictureResult")).css("display","initial");
}

$("#uploadFiles")[0].onchange = onSelectedFileResult;
function onSelectedFileResult(){
	uploadFileResultManual($("#uploadFiles")[0].files);
}
//
//$("#uploadTcFile")[0].onchange = onUploadFileTestcase;
//function onUploadFileTestcase(){
//	var lsFile = $("#uploadTcFile")[0].files;
//	$("#listFileTc").empty();
//	$.each(lsFile, function(ind, val){
//		var nameFile = val.name;
//		var html = '<div class="col-md-4"><a class="icon icon-attachment mb-2">'+nameFile+'<i class="fa fa-trash-o ml-3"></i></a></div>';
//		$("#listFileTc").append(html);
//	});
//}

function uploadFileResultManual(fileList){
	$("#uploadImages").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#loadUploadImage").fadeIn("slow");
	var testcaseName = $("#testcaseNameS").val();
	var testsuiteName = $("#testsuiteId").val();
	if(fileList.length != 0){
		var formData = new FormData();
		for(var i = 0; i < fileList.length; i++){
			formData.append("file", fileList[i]);
		}
		
		$.ajax({
			type : "POST",
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			url :  jscontext+"/project/"+projectId+"/"+testsuiteName +"/"+testcaseName + "/uploadFileToResultManual",
			data : formData,
			dataType : 'json',
			timeout : 600000,
			success : function(result) {
				if (!(result.errormessage) ) {	
					
					if(listImages.length != 0){
						var datas = JSON.parse(result.data);
						$.each(datas, function(ind, vas){
							listImages.push(vas);
						});
						
					} else {
						listImages = JSON.parse(result.data);
					}
					swal({
					        title: getLang(currentLocale,"Upload File Success!!!"),
					        type: "success",
					        timer: 1000,
					        showConfirmButton: false
					    });
					$("#uploadImages").html(getLang(currentLocale,"Upload File"));
					drawFilesResult(listImages, testsuiteName);
					$("#loadUploadImage").fadeOut("slow");
					$("#uploadFiles").val("");
				}
				
			},
			error : function(request, e) {
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			}
		});
	}
}

function drawFilesResult(listFile, testsuiteName){
	$("#loadFiles").empty();
	$("#modalImg").empty();
	$.each(listFile , function(ite, valueItem){
		
		var html = '<div class="col-md-3 preview p-0" id="imageResult_'+ite+'">';
		var commas = [...valueItem["Name"]].filter(l => l === '.').length;
		var fileContent = valueItem["Name"].split(".")[commas];
		var fileName = valueItem["Id"]+"." + fileContent;
		if(valueItem.Type.includes("image")){
			
			html += '<div class="component-file-upload-result p-0">'
						+'<img onclick="clickDetailImg(\''+ite+'\');" id="imgResult'+ite+'" src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" alt="" class="img-thumbnail border-0 path-img-result">'
					+'</div>';
		} else if(valueItem.Type.includes("video")){
			html += '<div class="component-file-upload-result p-0">'
				 +'<div class="w-100 h-100"><video controls class="w-100 h-100"><source id="imgResult'+ite+'"  src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" type="'+valueItem.Type+'"></video>'
				 +'</div></div>';
		}else {
			html += '<div class="component-file-upload-result">'
						+'<div class="w-100 item-file-upload-result"><i class="fa fa-file"></i>'
							+'<div class="w-100 mb-3 file-name-result"><a href="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" download>'+fileName+'</a></div>'
							+'<a href="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" class="icon-download-file-result" download><i class="ti-cloud-down"></i></a>'
						+'</div>'
					+'</div>';
		}
		            
         html   +='<div class="close-image-result" onclick="closeFile('+ite+')">'
            +'<i class="ti-close"></i>'
            +'</div>';
           
		$("#loadFiles").append(html);
		
		var imgHtml = '<div class="text-center w-100 h-100 divResult" id="divResult_'+ite+'" hidden>'
		+'<span class="close" onclick="closeImageResult(\''+ite+'\')" id="closeImgFile_'+ite+'">&times;</span>'
			+'<img class="pictureResult" id="pictureResult_'+ite+'" src="'+jscontext+"/project/"+projectId+"/"+testsuiteName +'/getFileResult/'+fileName+'" style="height: 100%;display: none;">'
		+'</div>';
		if(valueItem.Type.includes("image")){
			$("#modalImg").append(imgHtml);
		}
	});
	$("#modalImg").append('<a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a><a class="next" onclick="plusSlides(1, 0)">&#10095;</a>');
}

function editResultManuals(){
	var testsuiteName = $("#testsuiteId").val();
	if ($('#formAddLastResultManual').valid() !=true){
		return;
	}
	$("#updateNewResultManualTcs").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#updateNewResultManualTcs").prop("disabled", true);
	
	var resultInfo = {
			"testSuite": testsuiteName,
			"runningUser": $("#manualRunningUser").val(),
			"executeDate": new Date($('#datetimeLastRun').val()).toISOString(),
			"testCaseName": $("#testcaseNameS").val(),
			"testCaseResult": $("#manualStatus").val(),
			"projectId": projectId,
			"milestone": $("#mileStoneId").val(),
			"os": $("#manualOS").val(),
			"browser": $("#manualBrowser").val(),
			"screenResolution": $("#manualResolution").val(),
			"location": $("#manualLocation").val(),
			"mobileMode": $("#mobileModeResult").prop("checked").toString(),
			"deviceName": $("#manualDeviceName").val(),
			"runningDescription": $("#manualDescription").val(),
			"expectedOutput": $("#manualExpectedOutput").val(),
			"actualOutput": $("#manualActualOutput").val(),
			"testResult":"",
			"id": $("#idResult").text(),
			"resultName": "",
			"type": "manual",
			"files": JSON.stringify(listImages),
			"status": "add"
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/editResultManual",
		type : "POST",
		data : JSON.stringify(resultInfo),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				
				swal({
			        title: getLang(currentLocale,"Edit Result Success!!!"),
			        type: "success",
			        timer: 1000,
			        showConfirmButton: false
			    });
				getAllTestcaseManualByProject();
				//draw testcase manual
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue,milestoneTestcase);
				$("#updateNewResultManualTcs").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
				$("#updateNewResultManualTcs").prop("disabled", false);
				$("#manualExpectedOutput").prop("readonly", false);

				listImages = [];
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not save!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
	
}

function showConfirmMessagesResultManual(id, testcaseName, testsuiteName) {
	
	  swal({
	      title: getLang(currentLocale,"Are you sure delete Result?"),
	      text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
	      closeOnConfirm: false
	  }, function () {
		  deleteLastResult(id, testcaseName, testsuiteName);
	  });
	}

function deleteLastResult(id, testcaseName, testsuiteName){
	var resultItem = "";
	$.each(manualTcLs, function(ind,vals){
		if(vals.testcase == testcaseName){
			resultItem = vals;
		}
	});
	var listNameFile = [];
	var nameFiles = JSON.parse(resultItem["files"]);
	$.each(nameFiles, function(index, vals){
		var nameF = vals["Id"];
		var extensionF = vals["Name"].split(".")[1];
		var itemL = nameF + "." + extensionF;
		listNameFile.push(itemL);
	});
	var filesStr = JSON.stringify(listNameFile).replace(/"/g,"'");
	var resultInfo = {
			"testCaseName": testcaseName,
			"id": id,
			"files": filesStr
	}
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/deleteResultManual",
		type : "POST",
		data : JSON.stringify(resultInfo),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: getLang(currentLocale,"Delete Result Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				getAllTestcaseManualByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcaseManual").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue,milestoneTestcase);
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not save!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
}

function preResultManual(testsuiteName, testcaseName, id, inlitsResult){
	$("#updateLast_"+testcaseName+"").attr("hidden", true);
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/"+testcaseName+"/previousManualRs/"+id,
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var values = result.data;
				$("#nextResult_"+inlitsResult+"").attr("disabled",false);
				if(values != null){
					$('.item-tooltip').tooltip('hide');
					$("#preResult_"+inlitsResult+"").attr("disabled",false);
					$("#loadResultManual_"+inlitsResult+"").empty();
					var html = drawLastResults(values, inlitsResult, "false", "true");
					$("#loadResultManual_"+inlitsResult+"").html(html);
					showTooltipBtn();
				} else {
					$("#preResult_"+inlitsResult+"").attr("disabled",true);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not load data!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
	

	
}

function nextManual(testsuiteName, testcaseName, id, inlitsResult) {
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/"+testcaseName+"/nextManualRs/"+id,
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var values = result.data;
				$("#preResult_"+inlitsResult+"").attr("disabled",false);
				if(values != null){
					$('.item-tooltip').tooltip('hide');
					$("#nextResult_"+inlitsResult+"").attr("disabled",false);
					$("#loadResultManual_"+inlitsResult+"").empty();
					var booleanChek = "true";
					$.each(manualTcLs, function(ind,val){
						if(values.id == val.id){
							booleanChek= "false";
						}
					});
					
					var html = drawLastResults(values, inlitsResult, "false", booleanChek);
					$("#loadResultManual_"+inlitsResult+"").html(html);
					
					showTooltipBtn();
				} else{
					$("#updateLast_"+testcaseName+"").attr("hidden", false);
					$("#nextResult_"+inlitsResult+"").attr("disabled",true);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not load data!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		}
	});
	
}

function getExecuteBy(callbackF){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getExecuteByManual",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var listExecuteBy = result.data.map(function(obj){
					return {"id": obj, "text":obj}
				});		
			} else {
			}
			callbackF(listExecuteBy);
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


function drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue,milestoneTestcase){
	if(statusTestcase == "NOT-RUN"){
		$("#filterExecuteBy").attr("disabled", true);
		$("#filterFromDate").attr("disabled",true);
		$("#filterToDate").attr("disabled",true);

	} else {
		$("#filterExecuteBy").attr("disabled", false);
		$("#filterFromDate").attr("disabled",false);
		$("#filterToDate").attr("disabled",false);

	}
	
	testsuiteNameParse = testsuiteName;
	executeBySlParse = executeBySl;
	fromDateSS = fromDate;
	toDateSS = toDate;
	assignToParse = filterAssignTo;
	descriptionSS = description;
	statusTestcaseSS = statusTestcase;
	statusIssueParse = statusIssue;
	categoryIssueParse = categoryIssue;
	milestoneParse = milestoneTestcase;
	
	var filterTestcase = {
			"testsuiteName": testsuiteName,
			"executeBy": executeBySl,
			"statusTestcase": statusTestcase,
			"fromDate": fromDate,
			"toDate": toDate,
			"assignTo": filterAssignTo,
			"type": "manual",
			"fileName": description,
			"statusIssue": statusIssue,
			"categoryIssue": categoryIssue,
			"milestone": milestoneTestcase,
	}
	$(".progress-loadMainContent").show();
	$.ajax({
		url : jscontext +"/project/" + projectId +"/filterManualTc",
		type : "post",
		data : JSON.stringify(filterTestcase),
		contentType : "application/json", 
		success : function(result) {
			$("#listTestcaseManual").empty();
			if (!(result.errormessage) && result.data != null) {
				var tcFilter = result.data;
				drawTestcaseManualByProjectId(tcFilter);
			} else {
			}
			$(".progress-loadMainContent").fadeOut("slow");
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function fucntionAddNewIssue(projectId, idLastResult){
	scrollTop = $("html,body").scrollTop();
	$("#tableListManualTestcase").hide();
	var backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListTestcase(this);">'
				+'<i class="icon-arrow-left"></i> Back'
				+'</button>';
	$("#backIssueButton").html(backButton);
	var saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'testcaseManual\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	
	$("#idTask").text("");
	listImages = [];
	$(".newTask").trigger("reset");
	$(".listFiles").empty();
	descriptionTask.data.set("");
	$("#noteEdit").attr("hidden", true);
	
	$("#errorSubject").text("")
	$("#errorPlanDate").text("");
	$("#errorAcutalDate").text("");
	$("#errorMilestone").text("");
	drawSlInIssue(idLastResult);
	
}

function backListTestcase(isThis){
	
	if($(".btnBackDetail").attr("data-id") != undefined){
		updateLastResult($(".btnBackDetail").attr("data-id"), $(".btnBackDetail").attr("data-testcase"), $(".btnBackDetail").attr("data-testsuite"), "true", "false");
	} 
	$(".newTask").trigger("reset");
	descriptionTask.data.set("");
	$("#newTask").hide();
	$("#tableListManualTestcase").show();
	$("html,body").scrollTop(scrollTop);
	scrollTop = "";
}

function drawSlInIssue(idResult){
	
	infProjectCom(function(projectCo){
		$("#trackerTask").empty();
		$("#statusTask").empty();
		$("#priorityTask").empty();
		$("#categoryTask").empty();
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Tracker"){
				$("#trackerTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>');
			} else if(projectCo[i]["groupCode"] == "Status"){
				$("#statusTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Priority"){
				$("#priorityTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Category"){
				$("#categoryTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			}
		}
		$("#trackerTask option[value='Bug']").prop("selected",true);
		$("#priorityTask option[value='Normal']").prop("selected",true);
		if($("#trackerTask").val() == "Bug"){
			$("#bugId").attr("disabled", false);
		}
	});
	
	getListTestsuite("false", function(testsuitels){
		$("#testsuiteIssue").empty();
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteIssue").append(htmlOptions);
		
		
	});
	
	getAllMilestoneInIssue(function(milestoneLs){
		listMilestone = milestoneLs;
		$("#milestoneIssue").empty();
		var htmlOptions = "";
		var number = 0;
		$.each( milestoneLs, function(index, valuesItemTestsuite) { 
			var dateStart = new Date(valuesItemTestsuite["startDate"]);
			var dateEnd = new Date(valuesItemTestsuite["endDate"]);

			var dateNow = new Date().getTime();
			if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){ 
				number += 1;
				if(number == 1){
					htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'" selected>'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
					
					$("#planEndDateTask").val(converDate(valuesItemTestsuite["endDate"]));
					
				} else {
					htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
				}
				
			} else {
				htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
			}
		});
		$("#milestoneIssue").append(htmlOptions);
		
	});
	
	$(".choose-watcher").empty();
	$(".tag-watchers").click(function(){
		$("#watchers li input").prop("checked", false) ;
		$('.choose-watcher :checkbox:checked').each(function(i){
	    	$("#watchers li[name='"+$(this).val()+"'] input").prop("checked", true);
		});
		$(".show-watchers").on('hide.bs.dropdown', function () {
			$(".choose-watcher").empty();
			$('.show-watchers :checkbox:checked').each(function(i){
				var nameWatcher = $(this).val();
				$.each(listUsersMal,function(index, value){
					if(value["username"] == nameWatcher){
						var count = 0 ;
						if(duplicateUsers.length > 0){
							$.each(duplicateUsers, function(indexDup,valueDup){
								if(valueDup.username == nameWatcher){
									$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " "+ value.firstname +"("+ nameWatcher+")"+'</p>');
									count ++;
								}					
							})
						} 
						
						if(count == 0 ){
							$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " " + value.firstname+'</p>');
						}
					}
				})
		    	$("#watcher-"+i).prop( "checked", true );
			});
		});	
	});
	
	$("#bugId").val(idResult);
	var testcaseLs = "";
	$.each(manualTcLs, function(ind,vals){
		if(vals.id == idResult){
			testcaseLs = vals;
		}
	});
	
	var descHtml = getValueDescriptionIssue(testcaseLs).first;
	var subjectHtml = getValueDescriptionIssue(testcaseLs).second;
	
	descriptionTask.data.set(descHtml);
	$("#subjectTask").val(subjectHtml);
	getUserProject(function(userProjectList){
		$("#assigneeTask").empty();
		$("#watchers").empty();
		$("#assigneeTask").append('<option value = ""></option>');
		for(var i =0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null && userProjectList[i]["firstname"] != null){
				var count = 0; 
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'
									 +userProjectList[i]["lastname"] +" "+userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</option>')
							$("#watchers").append('<li class=""><input class="mx-2 my-2 ckb-watcher" name="'+userProjectList[i]["username"]+'"'
									 +'type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'
									 +userProjectList[i]["lastname"] +" "+userProjectList[i]["firstname"] +"( " +userProjectList[i]["username"]+ " )"+'</li>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'
							 +userProjectList[i]["lastname"] +" "+userProjectList[i]["firstname"]+'</option>')
					$("#watchers").append('<li class=""><input class="mx-2 my-2 ckb-watcher" name="'+userProjectList[i]["username"]+'"'
							 +'type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'
							 +userProjectList[i]["lastname"] +" "+userProjectList[i]["firstname"]+'</li>');
				}
			}	
		}
		$('#assigneeTask option[value=""]').prop("selected",true);
		$('#milestoneIssue option[value="'+testcaseLs.milestone+'"]').prop("selected",true);
		$('#testsuiteIssue option[value="'+testcaseLs.testSuite+'"]').prop("selected",true);
	});
}

function getValueDescriptionIssue(testcaseLs){
	
	var descriptionHtml = '- <strong>Milestone</strong>: '+ (testcaseLs.milestone ? testcaseLs.milestone : "") +'<br>'
		+ '- <strong>Os</strong>: ' + (testcaseLs.os ? testcaseLs.os :"") + '<br>'
		+ '- <strong>Browser</strong>: ' + (testcaseLs.browser ? testcaseLs.browser : "") + '<br>'
		+ '- <strong>Screen Resolution</strong>: ' + (testcaseLs.screenResolution ? testcaseLs.screenResolution : "")+ '<br>'
		+ '- <strong>Location</strong>: ' + (testcaseLs.location ? testcaseLs.location : "") + '<br>'
		+ '- <strong>Mobile Mode</strong>: ' + (testcaseLs.mobileMode ? testcaseLs.mobileMode: "") + '<br>'
		+ '- <strong>Device Name</strong>: ' + (testcaseLs.deviceName ? testcaseLs.deviceName : "") + '<br>'
		+ '- <strong>Run Status</strong>: ' + (testcaseLs.testCaseResult ? testcaseLs.testCaseResult: "") + '<br>'
		+ '- <strong>Run User</strong>: ' + (testcaseLs.runningUser ? testcaseLs.runningUser : "") + '<br>'
		+ '- <strong>Run Time</strong>: ' + (testcaseLs.executeDate ? testcaseLs.executeDate : "") + '<br>'
		+ '- <strong>Run Description</strong>: ' + (testcaseLs.runningDescription ? testcaseLs.runningDescription : "") + '<br>'
		+ '- <strong>Pre Condition</strong>: ' + (testcaseLs.preCondition ? testcaseLs.preCondition : "") +'<br>'
		+ '- <strong>Testcase Procedure</strong>: ' + (testcaseLs.testcaseProcedure ? testcaseLs.testcaseProcedure : "") +'<br>'
		+ '- <strong>Expected Output</strong>: ' + (testcaseLs.expectedOutput ? testcaseLs.expectedOutput : "")+ '<br>' 
		+ '- <strong>Actual Output</strong>: ' + (testcaseLs.actualOutput ? testcaseLs.actualOutput : "");
	
	var subjectHtml = testcaseLs.testcase + " ("+ testcaseLs.description+"): " + testcaseLs.testCaseResult;
	
	return {
		first: descriptionHtml,
        second: subjectHtml,
	};
}

function getStatusIssue(projectId, bugId, callbackF){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/tasks/getTaskByBugId/" + bugId,
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var task = result.data;
				callbackF(task);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

function addIssue(isThis){
	var idResult = $(isThis).attr("data-id");
	var testcase = $(isThis).attr("data-testcase");
	var testsuite = $(isThis).attr("data-testsuite");
	$("#tableListManualTestcase").hide();
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	$("#modalUpdateSttDetailManualTestcase").modal("hide");
	$(".btnBackDetail").attr("data-id", idResult);
	$(".btnBackDetail").attr("data-testcase", testcase);
	$(".btnBackDetail").attr("data-testsuite", testsuite);
	
	drawSlInIssue(idResult);
}

function filterManualTestcase(){
	var testsuiteName = $("#filterTestsuite").val();
	var executeBySl = $("#filterExecuteBy").val();
	var statusTestcase = $("#filterStatusTestcaseManual").val();
	var fromDate = $("#filterFromDate").val();
	var toDate = $("#filterToDate").val();
	var filterAssignTo = $("#filterAssignTo").val();
	var description = $("#filterDescription").val();
	var statusIssue = $("#filterTestcaseStatus").val();
	var categoryIssue = $("#filterTestcaseCategory").val();
	var milestoneTestcase = $("#filterTestcaseMilestone").val();
	$("#filterTcMan").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filterTcMan").prop("disabled", true);
	drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue,categoryIssue,milestoneTestcase);
	$("#filterTcMan").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filterTcMan").prop("disabled", false);
}

function showModalAddNewTestcaseByOrder(orderId){
	addNewManualTestcase();
	$("#orderIdMan").val(orderId);
}

$("#filterTestsuite").on('select2:opening', function(){
	checkSession = false;
	$("#filterTestsuite").one('change', function(){
		checkFilter(checkSession);
		
	});
});

$("#filterStatusTestcaseManual").on('select2:opening', function(){
	checkSession = false;
	$("#filterStatusTestcaseManual").one('change', function(){
		$("#filterExecuteBy").attr("disabled", false);
		checkFilter(checkSession);
	});
});

$("#filterExecuteBy").on('select2:opening',function(){
	checkSession = false;
	$("#filterExecuteBy").one('change', function(){
		checkFilter(checkSession);
	});
});

$("#filterAssignTo").on('select2:opening',function(){
	checkSession = false;
	$("#filterAssignTo").one('change', function(){
		checkFilter(checkSession);
	});
});

$("#filterFromDate").on('change', function(){
	$("#filterFromDate").attr("disabled",false);
	checkSession = false;
	checkFilter(checkSession);
});

$("#filterToDate").on('change', function(){
	$("#filterToDate").attr("disabled",false);
	checkSession = false;
	checkFilter(checkSession);
});

$("#filterDescription").on('change', function(){
	checkSession = false;
	checkFilter(checkSession);
});

function checkFilter(checkSession){
	var testsuiteName = $("#filterTestsuite").val();
	var executeBySl = $("#filterExecuteBy").val();
	var statusTestcase = $("#filterStatusTestcaseManual").val();
	var fromDate = $("#filterFromDate").val();
	var toDate = $("#filterToDate").val();
	var filterAssignTo = $("#filterAssignTo").val();
	var description = $("#filterDescription").val();
	var milestoneTestcase = $("#filterTestcaseMilestone").val();
	if(checkSession != true){
		drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description,milestoneTestcase);
	}
}
// func count Layout
jQuery(document).ready(function() {
	'use strict';
});

//func validate Signup
var validateSignUp = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#registerUser');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	firstname:{
        		required: {
        			depends:function(){
	    	            $(this).val($.trim($(this).val()));
	    	            return true;
        			}
        		},
        		maxlength: 50,
                minlength: 1,
        	},
        	lastname:{
        		required: {
        			depends:function(){
	    	            $(this).val($.trim($(this).val()));
	    	            return true;
        			}
        		},
        		maxlength: 50,
                minlength: 1,
        	},
        	username: {
                required: true,
                email: true,
                maxlength: 50,
                minlength: 5,
                regex:/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            },
            password: {
                required: true,
	            minlength: 5,
                regex:"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$"
            },
            passconfirmation:{
            	required: true,
            	regex:"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$",
            	equalTo: "#inputPassword"
            },
            company:{
             	required: true,
                minlength: 1
            },
            country:{
             	required: true,
            },
            businessRoles:{
            	required: true,
            },
            checkboxTerms:{
            	required: true,
            },
            agrre:{
            	required: true,
            }
        },
        
        messages: {
        	firstname: {
        		required:getLang(currentLocale,"Cannot be blank!")
        	},
        	lastname: {
        		required:getLang(currentLocale,"Cannot be blank!")
        	},
        	username:{
        		required:getLang(currentLocale,"Cannot be blank!"),
        		regex: getLang(currentLocale,"Email is invalid!")
			},
			password:{
				required:getLang(currentLocale,"Cannot be blank!"),
				regex: getLang(currentLocale,"Password must contain at least six characters and most fifty characters, at least one number and uppercase letters")
			},
			company: getLang(currentLocale,"Cannot be blank!")
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            
            //func check Name Layout is exist
            var password = $("#inputPassword").val();
            var passconfirmation = $("#passconfirmation").val();
				if(passconfirmation != password){
					var $alertas = $('#registerUser');
					error2.show();
					success2.hide();
					$alertas.validate().resetForm();
					$alertas.find('.form-group').removeClass('has-success');
			        $alertas.find('.passConfirm').addClass('has-error');
			        $alertas.find('.confirmPass').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Your password and confirmation password do not match!!!"));
			        $alertas.find('.fa.passWord').removeClass('fa-warning').removeClass('fa-check');
			        $alertas.find('.fa.checkCompany').removeClass('fa-warning').removeClass('fa-check');
			        $alertas.find('.fa.selectCountry').removeClass('fa-warning').removeClass('fa-check');
			        $alertas.find('.fa.selectRole').removeClass('fa-warning').removeClass('fa-check');
			        $alertas.find('.fa.checkAgree').removeClass('fa-warning').removeClass('fa-check');
			        $alertas.find('.fa.checkboxAgree').removeClass('fa-warning').removeClass('fa-check');
			        return false;
				}
			// func sign up
			signUp();
    	    var $alertas = $('#registerUser');
    	    $alertas.validate().resetForm();
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
    	    return true;
        }
        
    });

}

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}

function signUp(){
	if ($('#registerUser').valid() != true){
		return;
	}
	$("#btn-signup").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#btn-signup").prop("disabled", true);
	var userName = $("#inputEmail").val();
	var password = $("#inputPassword").val();
	var confirmPass = $("#passconfirmation").val();
	var company = $("#company").val();
	var country = $("#country").val();
	var projectName = $("#projectName").val();
	var businessRoles = $("#businessRoles").val();
	var token = $("#token").val();
	if(userName != ''){
		var firstName = $("#inputFirstName").val();
		var lastName = $("#inputLastName").val();
		var language = $("#language").val();
	}else{
		var firstName = "";
		var lastName = "";
		var language = "";
	}
	var signup = {
			"firstname": firstName,
			"lastname": lastName,
			"username": userName,
			"email": userName,
			"password": password,
			"confirmPass": confirmPass,
			"company": company,
			"country": country,
			"position": businessRoles,
			"language": language,
			"token": token,
			"projectName": projectName,
			"@type" : "SignupDTO"
	}
	$.ajax({
		url : jscontext +"/signup",
		type : "POST",
		data : JSON.stringify(signup),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				if(token == null || token == ""){
					swal({	
				        title: getLang(currentLocale,"Register is success!!!"),
				        text: getLang(currentLocale,"Your account is:") +"\n"+ "<br>" + "<br>" + "<p style = color:red; >" + userName + "</p>" + "\n\n" + "<br>"+ "<br>" +
				        		"<p>"+getLang(currentLocale,"Please check your email to confirm your account.")+"</p>",  
				        type: "success",
				        confirmButtonColor: "#DD6B55",
				        confirmButtonText: "Close",
				        closeOnConfirm: false,
				        html:true
					 }, function () {
						 window.location= jscontext + "/login";
					  });
					} else {
					swal({
				        title: getLang(currentLocale,"Register is success!!!")+ "\n" +
				        getLang(currentLocale,"Your account is:")+ "\n"
				        		+ userName,
				        type: "success",
				        confirmButtonColor: "#DD6B55",
				        confirmButtonText: "Close",
				        closeOnConfirm: false
				    }, function () {
						 window.location= jscontext + "/login";
					});
				}
				$("#btn-signup").html("Rigister");
				$("#btn-signup").prop("disabled", false);
				
			} else {
				// check username is exist
				$("#btn-signup").html("Rigister");
				$("#btn-signup").prop("disabled", false);
				var $alertas = $('#registerUser');
				$alertas.validate().resetForm();
				$alertas.find('.form-group').removeClass('has-success');
		        $alertas.find('.emailName').addClass('has-error');
		        $alertas.find('.fa.userName').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Username is exist. Please enter the new username!!!"));
		        $alertas.find('.fa.passWord').removeClass('fa-warning').removeClass('fa-check');
		        $alertas.find('.fa.confirmPass').removeClass('fa-warning').removeClass('fa-check');
		        $alertas.find('.fa.checkCompany').removeClass('fa-warning').removeClass('fa-check');
		        $alertas.find('.fa.selectCountry').removeClass('fa-warning').removeClass('fa-check');
		        $alertas.find('.fa.selectRole').removeClass('fa-warning').removeClass('fa-check');
		        $alertas.find('.fa.checkAgree').removeClass('fa-warning').removeClass('fa-check');
		        $alertas.find('.fa.checkboxAgree').removeClass('fa-warning').removeClass('fa-check');
		        return false;
			}
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});	
}
var getTask = '';
var numbFile = 0;
var descriptionTask = '';
var context = context;
var listImages =[];
var duplicateUsers = [] ;
var userProjectList ; 
var scrollTop = "";
var task;
$( document ).ready(function() {
	ClassicEditor
	.create( document.querySelector( '#descriptionTask' ), {
	} )
	.then( function(editor){
		window.editor = editor;
		descriptionTask = editor;
	} )
	.catch( err => {
		console.error( err.stack );
	} );
	ClassicEditor
	.create( document.querySelector( '#notesTask' ), {
	} )
	.then( function(editor){
		window.editor = editor;
		notesTask = editor;
	} )
	.catch( err => {
		console.error( err.stack );
	} );
	
//	if(listBug != ""){
//		var lsBug = [];
//		$.each(listBug, function(ind, vals){
//			var itemIssue = {
//				value:getLang(currentLocale,"Result Id") +": "+vals.id + " - ",
//			    label: vals.id,
//			    desc: vals.testcase,
//			}
//			lsBug.push(itemIssue);
//		});
//		autocompleteBugId(lsBug);
//	}
	
	$('#estimatedTimeTask').keypress(function(e) {
		if(isNaN(this.value+""+String.fromCharCode(e.charCode))) return false;
	  })
	  .on("cut copy paste",function(e){
		e.preventDefault();
	  });
	
	$('#actualTimeTask').keypress(function(e) {
		if(isNaN(this.value+""+String.fromCharCode(e.charCode))) return false;
	 })
	 .on("cut copy paste",function(e){
		 e.preventDefault();
	 });	
});

function duplicateUser(arr){
	var duplicateIds = [];
	var valueDupl = arr
    .map(e => e['lastname'] + e['firstname'])
    .map((e, i, final) => final.indexOf(e) !== i && i)
    .filter(obj=> arr[obj])
    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
	 $.each(valueDupl, function(index, value){
		 $.each(arr, function(indexArr, valueArr){
			 var name = valueArr["lastname"] + valueArr["firstname"]; 
			 if(name == value){
				 duplicateIds.push(valueArr);
			 }
		 })
	 })
	 
	 return duplicateIds ;
}

//------------------------------------------------------------edit----------------------------------------------------------
function listTask(response) {
	$("#tableTask").empty();
	if(response.length > 0){
		$("#totalIssue").text(response.length);
		
		var listIssue = [];
		var totalTime = 0;
		var totalTimePlan = 0;
		$.each(response, function(indexTask, valueTask){
			
			var itemIssue = {
				value: valueTask.tracker +" # " + valueTask.id + ": ",
			    label: valueTask.id,
			    desc: valueTask.subject,
			}
			listIssue.push(itemIssue);
			var numberTime = new Number(valueTask["actualTime"]);
			totalTime += numberTime;
			var numTimePlan = new Number(valueTask["estimatedTime"]);
			totalTimePlan += numTimePlan;
			
			if(valueTask["tracker"] == "Testcase"){
				var itemTable = '<tr class="odd gradeX" onclick="">';
				var href = "";
				if(valueTask.priority == "automation"){
					itemTable += '<td class="" ><a style="color: #1dcb8b;" onclick="getDetailAutoTc(\''+valueTask.testsuite+'\', \''+valueTask.subject+'\');">'+(valueTask["subject"] ? valueTask.id : "")+'</a></td>';
				} else{
					itemTable += '<td class="" ><a style="color: #1dcb8b;" onclick="getDetailManualTestcase(\''+valueTask["subject"]+'\',\''+true+'\')">'+(valueTask["subject"] ? valueTask.id : "")+'</a></td>';
				}
			} else{
				var itemTable = '<tr class="odd gradeX" id="Issue_'+valueTask["id"]+'">';
				itemTable += '<td class="text-center"><a href="'+jscontext +"/project/"+projectId+"/issue/"+ valueTask["id"]+'">'+(valueTask["id"] ? valueTask["id"] : "")+'</a></td>';
			}
			
			itemTable +='<td class="">'+valueTask["tracker"]+'</td>'
				+'<td class="">'
				+(valueTask["testsuite"] ? '<div class="mb-1"><a >'+valueTask["testsuite"]+'</a></div>' : "")
				+(valueTask["milestone"] ? '<div class="mb-1"><a >'+valueTask["milestone"]+'</a></div>' : "")
				+'</td>'
				+'<td class="">'
				+(valueTask["status"] ? '<div class="mb-1"><a >'+valueTask["status"]+'</a></div>' : "")
				+(valueTask["tracker"] == "Testcase" ? "" : '<div class="mb-1"><a >'+valueTask["priority"]+'</a></div>')
				+'</td>';
			
			if(valueTask["tracker"] == "Testcase"){
				var href = "";
				if(valueTask.priority == "automation"){
					itemTable += '<td class="" ><a style="color: #1dcb8b;" onclick="getDetailAutoTc(\''+valueTask.testsuite+'\', \''+valueTask.subject+'\');">'+valueTask["subject"]+'</a></td>';
				} else{
					itemTable += '<td class="" ><a style="color: #1dcb8b;" onclick="getDetailManualTestcase(\''+valueTask["subject"]+'\',\''+true+'\')">'+valueTask["subject"]+'</a></td>';
				}
				
			} else{
				itemTable += '<td class="" ><a href="'+jscontext +"/project/"+projectId+"/issue/"+ valueTask["id"]+'">'+valueTask["subject"]+'</a></td>';
			}
			itemTable += '<td class="">';
			if(valueTask["assignee"] ){ 
				var count = 0 ;
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == valueTask["assignee"]){
							itemTable += valueTask["lastname"] + valueTask["firstname"] + "(" + valueTask["assignee"] + ")";
							count ++;
						}
					})
				}
				
				if(count == 0 ){
					itemTable += valueTask["lastname"] + valueTask["firstname"] ;
				}
			}
			itemTable +='</td><td class="">';
				if(valueTask["planStartDate"]){
					var planStartDate = (new Date(valueTask["planStartDate"]).toISOString()).split("T")[0] ;
					itemTable += '<div class="mb-1"><a >'+planStartDate+'</a></div>';
				} 
				if(valueTask["actualStartDate"]){
					var actualStartDate = (new Date(valueTask["actualStartDate"]).toISOString()).split("T")[0] ;
					itemTable += '<div class="mb-1"><a >'+actualStartDate+'</a></div>';
				} 
itemTable +=	'</td>'
				+'<td class="">'
				if(valueTask["planEndDate"]){
					var d = (new Date().toISOString()).split("T")[0];
					planEndDate = (new Date(valueTask["planEndDate"]).toISOString()).split("T")[0] ;
					if( new Date(d).getTime() == new Date(planEndDate).getTime() ){
						itemTable += '<div class="mb-1" style="color:gold"><a >'+planEndDate+'</a></div>';
					} else if ( new Date(d).getTime() > new Date(planEndDate).getTime() ){
						itemTable += '<div class="mb-1" style="color:red"><a>'+planEndDate+'</a></div>';
					} else{
						itemTable += '<div class="mb-1"><a >'+planEndDate+'</a></div>';
					}
				} 
				if(valueTask["actualEndDate"]){
					actualEndDate = (new Date(valueTask["actualEndDate"]).toISOString()).split("T")[0] ;
					itemTable += '<div class="mb-1"><a >'+actualEndDate+'</a></div>';
				} 
itemTable +=    '</td>'
				+'<td class="text-right">'
				+(valueTask["estimatedTime"] ? '<div class="mb-1"><a >'+valueTask["estimatedTime"]+'</a></div>' : "")
				+(valueTask["actualTime"] ? '<div class="mb-1"><a >'+valueTask["actualTime"]+'</a></div>' : "")
				+'</td>'
				+'<td class="text-right">'+(valueTask["done"] ? valueTask["done"] : "")+'</td>';
				if(userP == 'PROJECT_ADMIN' || userP == 'Manager'){
					if(valueTask["tracker"] != "Testcase"){
						itemTable += '<td class="">'
							+'<a style="float:right;" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2 mb-2" onclick="editItemTask(\''+valueTask["id"]+'\');"><i class="ti-pencil-alt"></i></a>'
							+'<a style="float:right;" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-copy-testSuites m-0 mr-2" onclick="showConfirmMessagesIssue('+valueTask["id"]+');"><i class="fa fa-trash-o"></i></a></td>';
					} else{
						itemTable += '<td class=""></td>';
					}
				} else {
					if(valueTask["tracker"] != "Testcase"){
						itemTable += '<td class=""><a style="float:right;" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites m-0 mr-2" onclick="editItemTask(\''+valueTask["id"]+'\');"><i class="ti-pencil-alt"></i></a></td>';
					} else {
						itemTable += '<td class=""></td>';
					}
				}
				itemTable +='</tr>';
				
			$("#tableTask").append(itemTable);
		
		});
		$("#totalTime").text(totalTime);
		$("#totalTimePlan").text(totalTimePlan);
	} else {
		var htmlRecordTestsuites =	'<tr><td colspan="14" class="text-center text-danger">'+getLang(currentLocale,"The issue is empty!!!")+'</td></tr>'
		$("#tableTask").html(htmlRecordTestsuites);
		$("#totalIssue").text(0);
		$("#totalTime").text(0);
		$("#totalTimePlan").text(0);
	}
	
	autocompleteParentTask(listIssue);
	
}

function editItemTask(id){
	scrollTop = $("html,body").scrollTop();
	$(".newTask").trigger("reset");
	$("#screenTask").hide();
	var backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListIssue(\'true\')">'
		+'<i class="icon-arrow-left"></i> Back'
		+'</button>';
	$("#backIssueButton").html(backButton);
	var saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'issue\', \'false\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	getIssueById(id, function(data){
		task = data;
//		callBackClickTrackerFunction(task.tracker);
		detailItemIssue(task);
	});
}

function getIssueById(id, callback){
	$.ajax({
		url : jscontext +"/project/" + projectId+"/issues/"+ id,
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				callback(result.data);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function detailItemIssue(task){
	//edit Issue and edit item Issue
	$("#title-new").text(getLang(currentLocale,'Edit Issue'));
	$("#idTask").text(task.id);
	$("#noteEdit").attr("hidden", false);
	$('#donePercent').val(task.done);
	$("#bugId").val(task.bugId);
	infProjectCom(function(projectCo){
		$("#trackerTask").empty();
		$("#statusTask").empty();
		$("#priorityTask").empty();
		$("#categoryTask").empty();
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Tracker"){
				$("#trackerTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>');
			} else if(projectCo[i]["groupCode"] == "Status"){
				$("#statusTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Priority"){
				$("#priorityTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Category"){
				$("#categoryTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			}
		}
		$('#trackerTask option[value="'+task.tracker+'"]').prop("selected",true);
		if(task.tracker == "Bug"){
			$("#bugId").attr("disabled", false);
		} else {
			$("#bugId").attr("disabled", true);
		}
		$('#statusTask option[value="'+task.status+'"]').prop("selected",true);
		$('#categoryTask option[value="'+task.category+'"]').prop("selected",true);
		$('#priorityTask option[value="'+task.priority+'"]').prop("selected",true);
	});
	var lstPlanToolMaterial = (task.planToolMaterial ? JSON.parse(task.planToolMaterial): "");
	if(lstPlanToolMaterial.length > 0){
		
		$("#planToolMaterial").empty();
		$.each(lstPlanToolMaterial, function(ind, vals){
			var html = '<tr>'
				+'<td>'+vals.Material+'</td>'
				+'<td>'+vals.Quantity+'</td>'
				+'<td>'+vals.Price+'</td>'
				+'</tr>';
			$("#planToolMaterial").append(html);
		});
	}
	setEditTable();
	getUserProject(function(userProjectList){
		var valueDuplUser = duplicateUser(userProjectList);
		$("#assigneeTask").empty();
		$("#watchers").empty();
		$("#assigneeTask").append('<option value = ""></option>');
		for(var i =0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null && userProjectList[i]["firstname"] != null){
				var count = 0 ;
				if(valueDuplUser.length > 0){
					$.each(valueDuplUser, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</li>');
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</option>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+'</li>');
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+'</option>');
				}
			}
		}
		$('#assigneeTask option[value="'+task.assignee+'"]').prop("selected",true);
	});
	getListTestsuite("false", function(testsuitels){
		$("#testsuiteIssue").empty();
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteIssue").append(htmlOptions);
		$('#testsuiteIssue option[value="'+task.testsuite+'"]').prop("selected",true);
		
	});
	
	getAllMilestoneInIssue(function(milestoneLs){
		listMilestone = milestoneLs;
		$("#milestoneIssue").empty();
		var htmlOptions = "";
		var number = 0;
		$.each( milestoneLs, function(index, valuesItemTestsuite) {
			if(task.milestone == valuesItemTestsuite.projectMilestoneIdentity.mileStoneName){
				htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'" selected>'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';
			} else {
				htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';
			}
			
			if(task.planEndDate == null){
				number += 1;
				if(number == 1){
					var dateStart = new Date(task.planStartDate);
					var dateEnd = new Date(task.planEndDate);
					if(dateStart.getTime() < dateEnd.getTime()){
						$("#planEndDateTask").val(converDate(valuesItemTestsuite["endDate"]));
					}
					
				}
			}
		});
		$("#milestoneIssue").append(htmlOptions);
	});
	
	$("#subjectTask").val(task.subject);
//	if(task.tracker == "Supply" || task.tracker == "Require" || task.tracker == "Price"){
//		const arrayMonth =  JSON.parse(task.description);
//		$("#month1").val(arrayMonth[0].value);
//		$("#month2").val(arrayMonth[1].value);
//		$("#month3").val(arrayMonth[2].value);
//		$("#month4").val(arrayMonth[3].value);
//		$("#month5").val(arrayMonth[4].value);
//		$("#month6").val(arrayMonth[5].value);
//		$("#month7").val(arrayMonth[6].value);
//		$("#month8").val(arrayMonth[7].value);
//		$("#month9").val(arrayMonth[8].value);
//		$("#month10").val(arrayMonth[9].value);
//		$("#month11").val(arrayMonth[10].value);
//		$("#month12").val(arrayMonth[11].value);
//	} else {
//	$("#descriptionTask").val(task.description);
//	}	
	descriptionTask.setData(task.description ? task.description : "");
	if(task.planStartDate != null){
		$("#planStartDateTask").val(converDate(task.planStartDate));
	}
	if(task.planEndDate != null){
		$("#planEndDateTask").val(converDate(task.planEndDate));
	}
	if(task.actualStartDate != null){
		$("#actualStartDateTask").val(converDate(task.actualStartDate));
	}
	if(task.actualEndDate != null){
		$("#actualEndDateTask").val(converDate(task.actualEndDate));
	}
	
	var listMaterial = (task.planToolMaterial ? JSON.parse(task.planToolMaterial) : "");
	if(listMaterial.length > 0){
		$("#materialBody").empty();
		var htmlMaterial = "";
		$.each(listMaterial, function(i, v){
			htmlMaterial += '<tr>'
						+ '<td>'+v.Material+'</td>'
						+ '<td>'+v.Quantity+'</td>'
						+ '<td>'+v.Price+'</td>'
						+ '</tr>';
		});
		$("#materialBody").append(htmlMaterial);
	} 
	setEditTable();
	$("#estimatedTimeTask").val(task.estimatedTime);
	$("#actualTimeTask").val(task.actualTime);
	$("#parentTask").val(task.parentTask);
	var listFile = (task.fileName ? JSON.parse(task.fileName) : "");
	listImages = listFile;
	drawListFileIssue(listFile);
	
	$(".tag-watchers").click(function(){
		$("#watchers li input").prop("checked", false) ;
		$('.choose-watcher :checkbox:checked').each(function(i){
	    	$("#watchers li[name='"+$(this).val()+"'] input").prop("checked", true);
		});
		$(".show-watchers").on('hide.bs.dropdown', function () {
			$(".choose-watcher").empty();
			$('.show-watchers :checkbox:checked').each(function(i){
				var nameWatcher = $(this).val();
				$.each(listUsers,function(index, value){
					if(value["username"] == nameWatcher){
						var count = 0 ;
						if(duplicateUserTaskDetail.length > 0){
							$.each(duplicateUserTaskDetail, function(indexDup,valueDup){
								if(valueDup.username == nameWatcher){
									$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " "+ value.firstname +"("+ nameWatcher+")"+'</p>');
									count ++;
								}					
							})
						} 
						
						if(count == 0 ){
							$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " " + value.firstname+'</p>');
						}
						
					}
				})
		    	$("#watcher-"+i).prop( "checked", true );
			});
		});	
	});
}

function showConfirmMessagesIssue(id) {
  swal({
      title: getLang(currentLocale,"Are you sure delete Issue ? ") ,
      text: getLang(currentLocale,"You will not be able to recover this issue!"),
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
      closeOnConfirm: false
  }, function () {
	  deleteTask(id);
  });
}
var listMilestone;
function createFormNewTask(){
	scrollTop = $("html,body").scrollTop();
	$("#planToolMaterial").empty();
	$("#screenTask").hide();
	var backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListIssue(\'true\')">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';  
	$("#backIssueButton").html(backButton);
	var saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'issue\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	
	$("#title-new").text(getLang(currentLocale,'New Issue'));
	$("#idTask").text("");
	listImages = [];
	$(".newTask").trigger("reset");
	$(".listFiles").empty();
	//descriptionTask.data.set("");
	$("#noteEdit").attr("hidden", true);
	
	$("#errorSubject").text("")
	$("#errorPlanDate").text("");
	$("#errorAcutalDate").text("");
	$("#errorMilestone").text("");
	$("#errorParentTask").text("");
	
	loadDataInPage();
	getUserProject(function(userProjectList){
		$("#assigneeTask").empty();
		$("#watchers").empty();
		$("#assigneeTask").append('<option value = ""></option>');
		for(var i =0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null &&  userProjectList[i]["firstname"] != null){
				var count = 0 ;
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</li>');
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</option>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+'</li>');
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+'</option>');
				}
			}
		}
		$('#assigneeTask option[value=""]').prop("selected",true);
	});
	$(".choose-watcher").empty();
	setEditTable();
	$(".tag-watchers").click(function(){
		$("#watchers li input").prop("checked", false) ;
		$('.choose-watcher :checkbox:checked').each(function(i){
	    	$("#watchers li[name='"+$(this).val()+"'] input").prop("checked", true);
		});
		$(".show-watchers").on('hide.bs.dropdown', function () {
			$(".choose-watcher").empty();
			$('.show-watchers :checkbox:checked').each(function(i){
				var nameWatcher = $(this).val();
				$.each(userProjectList,function(index, value){
					if(value["username"] == nameWatcher){
						var count = 0 ;
						if(duplicateUsers.length > 0){
							$.each(duplicateUsers, function(indexDup,valueDup){
								if(valueDup.username == nameWatcher){
									$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " "+ value.firstname +"("+ nameWatcher+")"+'</p>');
									count ++;
								}					
							})
						} 
						
						if(count == 0 ){
							$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " " + value.firstname+'</p>');
						}
					}
				})
		    	$("#watcher-"+i).prop( "checked", true );
			});
		});	
	});
}

function callBackClickTrackerFunction(supplyAndRequireAndPrice){
	if(supplyAndRequireAndPrice == "Supply" || supplyAndRequireAndPrice == "Require" || supplyAndRequireAndPrice =="Price" ) {
		document.getElementById("tracker_task").innerHTML = "" +
				"<table style='width: 100%' class='table_issue'>" +
				"<thead><tr class='th_month'>" +
				"<th>Jan</th>" +
				"<th>Feb</th>" +
				"<th>Mar</th>" +
				"<th>Apr</th>" +
				"<th>May</th>" +
				"<th>Jun</th>" +
				"<th>Jul</th>" +
				"<th>Aug</th>" +
				"<th>Sep</th>" +
				"<th>Oct</th>" +
				"<th>Nov</th>" +
				"<th>Dec</th>" +
				"</tr></thead><tbody><tr class='value_month'>" +
				"<td><input type='text' class='month' id='month1'></td>" +
				"<td><input type='text' class='month' id='month2'></td>" +
				"<td><input type='text' class='month' id='month3'></td>" +
				"<td><input type='text' class='month' id='month4'></td>" +
				"<td><input type='text' class='month' id='month5'></td>" +
				"<td><input type='text' class='month' id='month6'></td>" +
				"<td><input type='text' class='month' id='month7'></td>" +
				"<td><input type='text' class='month' id='month8'></td>" +
				"<td><input type='text' class='month' id='month9'></td>" +
				"<td><input type='text' class='month' id='month10'></td>" +
				"<td><input type='text' class='month' id='month11'></td>" +
				"<td><input type='text' class='month' id='month12'></td>" +
				"</tr></tbody></table>"
				
				$('.month').keypress(function(e) {
					if(isNaN(this.value+""+String.fromCharCode(e.charCode))) return false;
				  })
				  .on("cut copy paste",function(e){
					e.preventDefault();
				  });
	} else {
		var html = '<textarea class="form-control" rows="5" placeholder="Please enter" name="descriptionTask" id="descriptionTask"></textarea>'
			+'';
		$("#tracker_task").html(html);
		
		
	}
}

function changeTrackerContent(){
	let supplyAndRequireAndPrice = $("#trackerTask").val();
	callBackClickTrackerFunction(supplyAndRequireAndPrice);
}

function loadDataInPage(){
	//add new issue and add subtask
	infProjectCom(function(projectCo){
		$("#trackerTask").empty();
		$("#statusTask").empty();
		$("#priorityTask").empty();
		$("#categoryTask").empty();
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Tracker"){
				$("#trackerTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Status"){
				$("#statusTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Priority"){
				$("#priorityTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Category"){
				$("#categoryTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			}
		}
		if($("#trackerTask").val() == "Bug"){
			$("#bugId").attr("disabled", false);
		}
		$('#priorityTask option[value="Normal"]').prop("selected",true);
//		changeTrackerContent();
	});	
	
	getListTestsuite("false", function(testsuitels){
		$("#testsuiteIssue").empty();
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteIssue").append(htmlOptions);
		
	});
	
	getAllMilestoneInIssue(function(milestoneLs){
		listMilestone = milestoneLs;
		$("#milestoneIssue").empty();
		var htmlOptions = "";
		var number = 0;
		$.each( milestoneLs, function(index, valuesItemTestsuite) { 
			var dateStart = new Date(valuesItemTestsuite["startDate"]);
			var dateEnd = new Date(valuesItemTestsuite["endDate"]);

			var dateNow = new Date().getTime();
			if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){ 
				number += 1;
				if(number == 1){
					htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'" selected>'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
					
					$("#planEndDateTask").val(converDate(valuesItemTestsuite["endDate"]));
					
				} else {
					htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
				}
				
			} else {
				htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
			}
		});
		$("#milestoneIssue").append(htmlOptions);
		
	});
}

$("#trackerTask").on('change', function(){
	
	if($("#trackerTask").val() == "Bug"){
		$("#bugId").attr("disabled", false);
	} else {
		$("#bugId").attr("disabled", true);
	}
});

$("#milestoneIssue").on("change", function(){
	var milestoneName = $("#milestoneIssue").val();
	$.each(listMilestone, function(index,value){
		if(value.mileStoneName == milestoneName){
			$("#planEndDateTask").val(converDate(value["endDate"]));
		}
	});
	
});

function backListIssue(status){
	if(status == "true"){
		$("#newTask").hide();
		$("#screenTask").show();
		$("html,body").scrollTop(scrollTop);
		scrollTop = "";
	} else {
		$("#detailTask").hide();
		$("#screenTask").show();
		tasksList();
	}
	
}

function getListTestsuite(status, callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				
				
				if(status == "true"){
					tsByProjectId = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.testsuite;
						obTs["text"] = values.testsuite;
						tsByProjectId.push(obTs);
					});
					callbackF(tsByProjectId);
				} else {
					callbackF(result.data);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getAllMilestoneInIssue(callbackF){
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getAllMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				callbackF(result.data);
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: " Can get data!!!",
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}

function infProjectCom(callbackF){
	$.ajax({
		type:"GET",
		url: jscontext +"/project/"+projectId+"/task/getProCo",
		success : function(result) {
			if(!(result.errormessage) ){	
				var projectCo = result.data;
				
				callbackF(projectCo);
			} 
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getUserProject(callbackF){
	$.ajax({
		type:"GET",
		url: jscontext +"/project/"+projectId+"/getUsers",
		async: false,
		success : function(result) {
			if(!(result.errormessage) ){	
				userProjectList = result.data; 
				duplicateUsers = duplicateUser(userProjectList);
				callbackF(userProjectList);
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}



$("#fileName")[0].onchange = onSelectedFileResult;
function onSelectedFileResult(){
	uploadFileIssue($("#fileName")[0].files);
}

function uploadFileIssue(fileList){
	$("#chooseId").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	if(fileList.length != 0){
		var formData = new FormData();
		for(var i = 0; i < fileList.length; i++){
			formData.append("file", fileList[i]);
		}
		
		$.ajax({
			type : "POST",
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			url :  jscontext+"/project/"+projectId+ "/uploadFileToIssue",
			data : formData,
			dataType : 'json',
			timeout : 600000,
			success : function(result) {
				if (!(result.errormessage) ) {	
					var fileRe = JSON.parse(result.data);
					if(listImages.length != 0){
						var datas = fileRe;
						$.each(datas, function(ind, vas){
							listImages.push(vas);
						});
						
					} else {
						listImages = fileRe;
					}
					swal({
					        title: getLang(currentLocale,"Upload File Success!!!"),
					        type: "success",
					        timer: 1000,
					        showConfirmButton: false
					    });
					$("#fileName").val("");
					$("#chooseId").html(getLang(currentLocale,"Choose file"));
					drawListFileIssue(listImages);
				}
				
			},
			error : function(request, e) {
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			}
		});
	}
}

function drawListFileIssue(listImages){
	if(listImages.length > 0){
		$(".listFiles").empty();
		var html = "";
		$.each(listImages, function(ind, vals){
			html += '<a class="icon icon-attachment mb-2" id="fileOl'+ind+'">'+vals.Name+'<i class="fa fa-trash-o ml-3"  onclick="closeFileInTask(\''+ind+'\')"></i></a>';
			
		});
		$(".listFiles").append(html);
		
	}
}

function closeFileInTask(i){
	$.each(listImages, function(index, vals){
		if(i == index){
			showConfirmMessageFileIssue(vals, i);
		}
	});
}

function showConfirmMessageFileIssue(file, i) {
	  swal({
	      title: getLang(currentLocale,"Are you sure delete File"),
	      text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
	      closeOnConfirm: false
	  }, function () {
		  deleteFileIssue(file, i);
		   
	  });
	}

function deleteFileIssue(file, i){
	listImages.splice(i,1);
	var extensionFile = file.Name.split(".")[1];
	var fileName = file.Id + "." + extensionFile;
	$.ajax({
		url : jscontext + "/project/" + projectId + "/deleteFileIssue",
		type : "POST",
		data: fileName,
		async : false,
		contentType : "application/json",
		success : function(result) {
				swal({
			        title: getLang(currentLocale,"Delete File Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				$(".listFiles").empty();
				drawListFileIssue(listImages);
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function newTask(status, statusLoad) {
//	var arrayMonth = [
//		{
//			"month": 1,
//			"value": $("#month1").val()
//		},
//		{
//			"month": 2,
//			"value": $("#month2").val()
//		},
//		{
//			"month": 3,
//			"value": $("#month3").val()
//		},
//		{
//			"month": 4,
//			"value": $("#month4").val()
//		},
//		{
//			"month": 5,
//			"value": $("#month5").val()
//		},
//		{
//			"month": 6,
//			"value": $("#month6").val()
//		},
//		{
//			"month": 7,
//			"value": $("#month7").val()
//		},
//		{
//			"month": 8,
//			"value": $("#month8").val()
//		},
//		{
//			"month": 9,
//			"value": $("#month9").val()
//		},
//		{
//			"month": 10,
//			"value": $("#month10").val()
//		},
//		{
//			"month": 11,
//			"value": $("#month11").val()
//		},
//		{
//			"month": 12,
//			"value": $("#month12").val()
//		}
//	]
	var listTr = $($("#makeEditable").find("tbody")).find("tr");
	var lstDataMaterial = [];
	if(listTr.length > 0){
		for(var i = 0; i < listTr.length; i++){
		    var lstTd = $(listTr[i]).find("td");
		    if($(lstTd[1]).text() != "" && $(lstTd[2]).text() != ""){
		    	var dataTd = {
		    	        "Material": $(lstTd[0]).text(),
		    	        "Quantity": $(lstTd[1]).text(),
		    	        "Price": $(lstTd[2]).text(),
		    	        "Total": parseFloat($(lstTd[1]).text()) * parseFloat($(lstTd[2]).text())
		    	    }
		    	    lstDataMaterial.push(dataTd);
		    }
		    
		}
	}
	
	
	$("#errorSubject").text("")
	$("#errorPlanDate").text("");
	$("#errorAcutalDate").text("");
	$("#errorMilestone").text("");
	$("#errorParentTask").text("");
	var validateCheck = true;
	if($("#subjectTask").val() == ""){
		validateCheck = false;
			$("#errorSubject").text(getLang(currentLocale,"Subject can't be blank"));
	}
	var listWatchers = [];
	$('.choose-watcher :checkbox:checked').each(function(i){
		listWatchers.push($(this).val());
	});
	if($("#planEndDateTask").val() != "" && $("#planStartDateTask").val() != ""){
		var endDate = new Date($("#planEndDateTask").val()).getTime();
		var startDate = new Date($("#planStartDateTask").val()).getTime();
		if(endDate < startDate){
			validateCheck = false;
			$("#errorPlanDate").text(getLang(currentLocale,"Plan End Date must be greater than Plan Start Date"));
		}
	}
	
	if($("#actualStartDateTask").val() != "" && $("#actualEndDateTask").val() != ""){
		var endDate = new Date($("#actualEndDateTask").val()).getTime();
		var startDate = new Date($("#actualStartDateTask").val()).getTime();
		if(endDate < startDate){
			validateCheck = false;
			$("#errorAcutalDate").text(getLang(currentLocale,"Actual End Date must be greater than Actual Start Date"));
		}
	}
	
	if($("#trackerTask").val() != "Root"){
		if($("#parentTask").val() == ""){
			validateCheck = false;
			$("#errorParentTask").text("Parent task can't be blank!!!");
		}
	}
	
	if($("#milestoneIssue").val() == null){
		validateCheck = false;
		$("#errorMilestone").text(getLang(currentLocale,"Milestone not blank, Please create milestone!"));
	}
	
	if(validateCheck == true){
		var idTask = $("#idTask").text();
		var bugId = $("#bugId").val();
		if($("#trackerTask").val() != "Bug"){
			bugId = "";
		}
		$("#create-task").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
		$("#create-task").prop("disabled", true);
		var taskEn = {
				"tracker": $("#trackerTask").val(),
				"subject": $("#subjectTask").val(),
				"description": (descriptionTask.getData()? (descriptionTask.getData()).replace(/"/g,"'"): ""),
				"status": $("#statusTask").val(),
				"priority": $("#priorityTask").val(),
				"assignee": $("#assigneeTask").val(),
				"category": $("#categoryTask").val(),
				"fileName": (listImages.length > 0 ? JSON.stringify(listImages): ""),
				"parentTask": $("#parentTask").val(),
				"planStartDate": ($("#planStartDateTask").val() != "" ? new Date($("#planStartDateTask").val()).toISOString():""),
				"planEndDate": ($("#planEndDateTask").val() != "" ? new Date($("#planEndDateTask").val()).toISOString(): ""),
				"actualStartDate": ($("#actualStartDateTask").val() != "" ? new Date($("#actualStartDateTask").val()).toISOString(): ""),
				"actualEndDate": ($("#actualEndDateTask").val() != "" ? new Date($("#actualEndDateTask").val()).toISOString(): ""),
				"estimatedTime": $("#estimatedTimeTask").val(),
				"actualTime": $("#actualTimeTask").val(),
				"projectId": projectId,
				"bugId": bugId,
				"createBy": "",
				"updateDate": new Date().toISOString(),
				"done": $("#donePercent").val(),
				"notes": "",
				"testsuite": $("#testsuiteIssue").val(),
				"milestone": $("#milestoneIssue").val(),
				"watchers":listWatchers,
				"planToolMaterial": (lstDataMaterial.length > 0 ? JSON.stringify(lstDataMaterial): "")
		}
		taskEn.console
		if(!idTask){
			taskEn["createDate"] = new Date().toISOString();
		} else {
			taskEn["createDate"] = new Date(task.createDate).toISOString();
			taskEn["id"] = idTask;
			taskEn["notes"] = notesTask.getData();
			taskEn["createBy"] = task.createBy;
		}	
		$.ajax({
			type:"POST",
			url: jscontext +"/project/"+projectId+"/tasks/create",
			data: JSON.stringify(taskEn),
			contentType : "application/json",
			success : function(result) {
				if(!(result.errormessage) ){
					var taskInfo = result.data;
					$("#planToolMaterial").empty();
					swal({
						title:getLang(currentLocale,"Save Success!!!"),
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
					$(".newTask").trigger("reset");
					descriptionTask.data.set("");
					
					$("#create-task").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
					$("#create-task").prop("disabled", false);
					
					if(status == "testcase"){
						$("#newTask").hide();
						$("#tableListTestcase").show();
						drawTestcaseByFilter(testsuiteNameParse, executeBySlParse, statusTestcaseSS, fromDateSS, toDateSS, assignToParse, descriptionSS, statusIssueParse,categoryIssueParse,milestoneParse);
					} else if(status == "testcaseManual") {
						$("#newTask").hide();
						$("#tableListManualTestcase").show();
						drawTestcaseByFilter(testsuiteNameParse, executeBySlParse, statusTestcaseSS, fromDateSS, toDateSS, assignToParse, descriptionSS, statusIssueParse,categoryIssueParse, milestoneParse);
					} else if(status == "summary"){
						$("#newTask").hide();
						$("#drawSummary").show();
						window.location.href = jscontext +"/project/"+projectId+"/overview";
					} else if(status == "milestone"){
						$("#newTask").hide();
						$("#box-schedule-init").show();
						getAllMilestone();
					} else if(status == "testsuites"){
						$("#newTask").hide();
						$("#drawTestsuites").show();
						getProjectTestSuites();
					} else if(status == "screenDesign"){
						$("#newTask").hide();
						$("#drawScreenDesign").show();
						if(testsuiteNameParse != "" || createByParse != "" || createDateSS != "" || fileNameSS != ""){
							drawLayoutByFilter(testsuiteNameParse, createByParse, createDateSS, fileNameSS);
						}
					} else if(status == "testdata"){
						$("#newTask").hide();
						$("#drawTestData").show();
						if(testsuiteNameParse != "" || createDateSS != "" || typeParse != "" || fileNameSS != ""){
							drawTestcaseByFilter(testsuiteNameParse, createDateSS, typeParse, fileNameSS);
						}
						
					} else if(status == "detailData"){
						$("#newTask").hide();
						$("#boxTestDataDetail").show();
					} else if(status == "schedule"){
						$("#newTask").hide();
						$("#drawSchedule").show();
						if(testsuiteNameParse != ""){
							drawScheduleByFilter(testsuiteNameParse);
						}
						
					} else if(status == "running"){
						$("#newTask").hide();
						$("#runningBox").show();
						if(testsuiteParse != "" || statusSession != ""||executeByParse != ""||milestoneParse != ""){
							drawRunningByFilter(testsuiteParse, statusSession, executeByParse, milestoneParse);
						}
						
					} else if(status == "setting"){
						$("#newTask").hide();
						$("#drawSetting").show();
					}else {
						if(statusLoad == "true"){
							detailTask(taskInfo.id);
						} else {
							$("#newTask").hide();
							$("#screenTask").show();
							drawTaskFilter(trackerParse, assigneeParse, statusParse, priorityParse,categoryParse, planStartSS, planEndSS, testsuiteTParse, milestoneTParse, descriptionSS);
						}
						
					}
					
					$("#errorSubject").text("")
					$("#errorPlanDate").text("");
					$("#errorAcutalDate").text("");
					$("#errorMilestone").text("");
					$("#errorParentTask").text("");
					$("html,body").scrollTop(scrollTop);
					scrollTop = "";
				}
			},
			error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}else {
					swal({
				        title: getLang(currentLocale,"Can not save!!!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
				$("#planToolMaterial").empty();
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
	
}

function detailTask(id){
	window.location.href = jscontext +"/project/"+projectId+"/issue/"+ id;
}

function tasksList(){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/getAllTasks",
		type:"GET",
		success : function(result) {
			var tasks = result.data;
			taskList = tasks ;
			$("#tableTask").empty();
			listTask(tasks);
		},error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}	
	});
}

function converDate(dateTask){
	var date = new Date(dateTask);
	var timeConver = date.getFullYear() + '-' + (((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1)))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate()));
	return timeConver;
}

function deleteTask(idTask){
	var resultItem = "";
	$.each(taskList, function(ind,vals){
		if(vals.id == idTask){
			resultItem = vals;
		}
	});
	
	var listNameFile = [];
	var nameFiles = (resultItem["fileName"] ? JSON.parse(resultItem["fileName"]): "");
	if(nameFiles.length > 0) {
		$.each(nameFiles, function(index, vals){
			var nameF = vals["Id"];
			var extensionF = vals["Name"].split(".")[1];
			var itemL = nameF + "." + extensionF;
			listNameFile.push(itemL);
		});
	}
	
	var filesStr = (listNameFile.length > 0 ? JSON.stringify(listNameFile).replace(/"/g,"'"): "");
	
	var task = {
			"id": idTask,
			"fileName": filesStr
	}
	$.ajax({
		url : jscontext +"/project/"+projectId+"/deleteIssue",
		type:"POST",
		data: JSON.stringify(task),
		contentType:"application/Json",
		success : function(result) {
			if(!(result.errormessage) ){	
				swal({
			        title: "Delete Success !!!",
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				tasksList();
			}
		},error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}	
	});
}

function drawTaskFilter(trackerF, assigneeF, statusF, priorityF, categoryF, planStartDateF, planEndDateF, testsuiteF, milestoneF, description){
	trackerParse = trackerF;
	assigneeParse = assigneeF;
	statusParse = statusF;
	priorityParse = priorityF;
	categoryParse = categoryF;
	planStartSS = planStartDateF;
	planEndSS = planEndDateF;
	testsuiteTParse = testsuiteF; 
	milestoneTParse = milestoneF;
	descriptionSS = description;
	var filterTask = {
			"tracker": trackerF,
			"assignee": assigneeF,
			"status" : statusF ,
			"priority" : priorityF,
			"category" : categoryF,
			"planStartDate" : planStartDateF,
			"planEndDate" : planEndDateF,
			"testsuite" : testsuiteF,
			"milestone" : milestoneF,
			"description": description
		}
	$.ajax({
		type:"POST",
		url: jscontext +"/project/"+projectId+"/filterTask",
		data: JSON.stringify(filterTask),
		contentType : "application/json", 
		success : function(result) {
			if(!(result.errormessage) ){	
				var filterTask = result.data;
				listTask(filterTask);
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function filterTask(){
	var trackerF = $("#trackerF").val();
	var assigneeF = $("#assigneeF").val();
	var statusF = $("#statusF").val();
	var priorityF = $("#priorityF").val();
	var categoryF = $("#categoryF").val();
	var planStartDateF = $("#planStartDateF").val();
	var planEndDateF = $("#planEndDateF").val();
	var testsuiteF = $("#testsuiteT").val();
	var milestoneF = $("#milestoneT").val();
	var description = $("#filterDescriptionTask").val();
	
	
	$("#filTask").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filTask").prop("disabled", true);
	
	drawTaskFilter(trackerF, assigneeF, statusF, priorityF, categoryF, planStartDateF, planEndDateF, testsuiteF, milestoneF, description);
	
	$("#filTask").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filTask").prop("disabled", false);
}

function checkFilterTask(checkSession){
	var trackerF = $("#trackerF").val();
	var assigneeF = $("#assigneeF").val();
	var statusF = $("#statusF").val();
	var priorityF = $("#priorityF").val();
	var categoryF = $("#categoryF").val();
	var planStartDateF = $("#planStartDateF").val();
	var planEndDateF = $("#planEndDateF").val();
	var testsuiteF = $("#testsuiteT").val();
	var milestoneF = $("#milestoneT").val();
	var description = $("#filterDescriptionTask").val();
	
	if(checkSession != true){
		drawTaskFilter(trackerF, assigneeF, statusF, priorityF,categoryF, planStartDateF, planEndDateF, testsuiteF, milestoneF, description);
	}
}

$("#trackerF").on('select2:opening', function(){
	checkSession = false;
	$("#trackerF").one('change', function(){
		checkFilterTask(checkSession);
		
	});
});

$("#assigneeF").on('select2:opening', function(){
	checkSession = false;
	$("#assigneeF").one('change', function(){
		checkFilterTask(checkSession);
		
	});
});

$("#statusF").on('select2:opening', function(){
	checkSession = false;
	$("#statusF").one('change', function(){
		checkFilterTask(checkSession);
		
	});
});

$("#priorityF").on('select2:opening', function(){
	checkSession = false;
	$("#priorityF").one('change', function(){
		checkFilterTask(checkSession);
		
	});
});

$("#categoryF").on('select2:opening', function(){
	checkSession = false;
	$("#categoryF").one('change', function(){
		checkFilterTask(checkSession);
		
	});
});

$("#planStartDateF").on('change', function(){
	checkSession = false;
	checkFilterTask(checkSession);
});

$("#planEndDateF").on('change', function(){
	checkSession = false;
	checkFilterTask(checkSession);
});

$("#testsuiteT").on('select2:opening', function(){
	checkSession = false;
	$("#testsuiteT").one('change', function(){
		checkFilterTask(checkSession);
		
	});
});

$("#milestoneT").on('select2:opening', function(){
	checkSession = false;
	$("#milestoneT").one('change', function(){
		checkFilterTask(checkSession);
	});
});

$("#filterDescriptionTask").on('change', function(){
	checkSession = false;
	checkFilterTask(checkSession);
	
});


function getDetailAutoTc(testsuiteName, testcaseName){
	scrollTop = $("html,body").scrollTop();
	$('#detailAuto').removeAttr('hidden');
	$("#detailAuto").show();
	$("#screenTask").hide();
	getUserAssignTo("false", function(lsAssignTo){
		var htmlOptions = "";
		$("#filterAssignTos").empty();
		$.each(lsAssignTo, function(ind,vals){
			if(vals["lastname"]  != null && vals["firstname"] != null){
				var count = 0 ;
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == vals["username"]){
							htmlOptions += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+"("+vals["username"]+")" +'</option>';
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					htmlOptions += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"] +'</option>';
				}
			}
		});
		$("#filterAssignTos").append(htmlOptions);
	});
	infProjectCom(function(projectCo){
		$("#statusIssueDetail").empty();
		$("#categoryIssueDetail").empty(); 
		var html = "";
		var htmlCategory = "";
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Status"){
				html += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			} else if(projectCo[i]["groupCode"] == "Category"){
				htmlCategory += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			}
		}
		$("#statusIssueDetail").append(html);
		$("#categoryIssueDetail").append(htmlCategory); 
	});
	$("#testsuiteNameT").val(testsuiteName);
	getDetailTestcase(testsuiteName, testcaseName);
}

function backPageIssue(){
	$("#detailAuto").hide();
	$("#screenTask").show();
	$("html,body").scrollTop(scrollTop);
	scrollTop = "";
	drawTaskFilter(trackerParse, assigneeParse, statusParse, priorityParse,categoryParse, planStartSS, planEndSS, testsuiteTParse, milestoneTParse, descriptionSS);
}

function saveStepTask(){
	var testsuiteName = $("#testsuiteNameT").val();
	saveStep(testsuiteName);
}

function drawBugInTestcase(autoTcLs){
	var lsBug = [];
	$.each(autoTcLs, function(ind, vals){
		if(vals.testCaseResult == "FAILED"){
			var itemIssue = {
					value: getLang(currentLocale,"Result Id")+": "+vals.id + " - ",
				    label: vals.id,
				    desc: vals.testcase,
				}
				lsBug.push(itemIssue);
		}
		
	});
//	autocompleteBugId(lsBug);
}

function drawParentTaskInTestcase(tasks){
	var lsIssue = [];
	$.each(tasks, function(ind, vals){
		var itemIssue = {
			value: vals.tracker +" # " + vals.id + ": ",
		    label: vals.id,
		    desc: vals.subject,
		}
		lsIssue.push(itemIssue);
	});
	
	autocompleteParentTask(lsIssue);
}

function autocompleteParentTask(projects){
    $( "#parentTask" ).autocomplete({
       minLength: 0,
       source: projects,
       focus: function( event, ui ) {
          $( "#parentTask" ).val( ui.item.label );
             return false;
       },
       select: function( event, ui ) {
          $( "#parentTask" ).val( ui.item.label );
          return false;
       }
    })
			
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
       return $( "<li>" )
       .append( "<a>" + item.value + "" + item.desc + "</a>" )
       .appendTo( ul );
    };
}

function autocompleteBugId(projects){
   $( "#bugId" ).autocomplete({
      minLength: 0, 
      source: projects,
      focus: function( event, ui ) {
         $( "#bugId" ).val( ui.item.label );
            return false;
      },
      select: function( event, ui ) {
         $( "#bugId" ).val( ui.item.label );
         return false;
      }
   })
			
   .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
      .append( "<a>" + item.value + "" + item.desc + "</a>" )
      .appendTo( ul );
   };
}
function setEditTable(){
	$("#makeEditable").SetEditable({
	    $addButton: $('#add')
	});
}

var getTestsuiteID = "";
var listMileStone ;

$('#modalRun').on('show.bs.modal', function () {
	for(var i = 1; i <= 100; i ++){
		var htmlOption = '<option value="'+ i +'">'+ i +'</option>';
		$("#testConcurrent").append(htmlOption);
	}
});

function getANameTestCase(isThis){
	getAllMilestonesTcAuto();
	var testsuiteName = $(isThis).attr("testsuite-name");
	$("#testsuiteNameInModal").text(testsuiteName);  
	getTestsuiteID = testsuiteName;
	var items = $(isThis).attr("data-name");
	drawACaseAsCheckBox([items]);
}

function selectAllTestcaseModalRun() {
	$("#runAsTestcase").click(function(){
		if( $(this).prop("checked") == true ){
			$("#lsTestcases input[type='checkbox']").prop("checked",true);
		}else{
			$("#lsTestcases input[type='checkbox']").prop("checked",false);
		}
	});
}

function getAllMilestonesTcAuto(){
	$.ajax({
		url : jscontext+"/project/"+glProjectId+"/getAllMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				listMileStone = result.data;
				$("#executeName").empty();
				$.each(listMileStone, function(index, values){
					var dateStart = new Date(values.startDate);
					var dateEnd = new Date(values.endDate);
					var dateStartStr = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate()+" "+dateStart.getHours()+":"+dateStart.getMinutes()+":"+dateStart.getSeconds();
					var dateEndStr = dateEnd.getFullYear() + "-" + (dateEnd.getMonth() + 1) + '-'+ dateEnd.getDate()+" "+dateEnd.getHours()+":"+dateEnd.getMinutes()+":"+dateEnd.getSeconds();
					var selected ="";
					var dateNow = new Date();
					dateNow.setHours(0);
					dateNow.setMinutes(0);
					dateNow.setSeconds(0);
					dateNow.setMilliseconds(0);
					if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){ 
						selected = "selected";
						$("#runDescription").val(values.description);
					}
					var html = '<option value="'+values.projectMilestoneIdentity.mileStoneName+'" '+ selected +' >'+values.projectMilestoneIdentity.mileStoneName+"("+dateStartStr+","+dateEndStr+")"+'</option>';
					$("#executeName").append(html);
				});
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale," Can get data!!!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}

$("#executeName").on('change', function(){
	var mileStoneName = $("#executeName").val();
	$.each(listMileStone, function(index, values){
		if(mileStoneName == values.projectMilestoneIdentity.mileStoneName){
			$("#runDescription").val(values["description"]);
		}
	});
});

function getAllNameTestCase(testsuiteName){
	getAllMilestonesTcAuto();
	var testsuiteNameInModal = testsuiteName;
	if ((testsuiteName == null) || (testsuiteName == undefined) || (testsuiteName.trim()=="")){
		testsuiteNameInModal = $('#testSuiteName').text();
		getTestsuiteID = testsuiteNameInModal.trim();
	}else{
		getTestsuiteID = testsuiteName;
	}
	
	$("#testsuiteNameInModal").text(testsuiteNameInModal);  

	getListTestCase(getTestsuiteID);
}

var validateRunTestCases = function() {
// for more info visit the official plugin documentation: 
// http://docs.jquery.com/Plugins/Validation

var form2 = $('#runTestCases');
var error2 = $('.alert-danger', form2);
var success2 = $('.alert-success', form2);

	form2.validate({
	  errorElement: 'span', //default input error message container
	  errorClass: 'help-block help-block-error', // default input error message class
	  focusInvalid: true, // do not focus the last invalid input
	  ignore: "",  // validate all fields including form hidden input
	  rules: {
	  	executeName: {
	          minlength: 1,
	          required: true
	      },
	  },
	  
	  messages: {
	  	executeName: getLang(currentLocale,"Cannot be blank!"),
	    },
	
	  invalidHandler: function (event, validator) { //display error alert on form submit              
	      success2.hide();
	      error2.show();
	      App.scrollTo(error2, -200);
	  },
	
	  errorPlacement: function (error, element) { // render error placement for each input type
	      var icon = $(element).parent('.input-icon').children('i');
	      icon.removeClass('fa-check').addClass("fa-warning");  
	      icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
	     
	  },
	
	  highlight: function (element) { // hightlight error inputs
	      $(element)
	          .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	      tooltipValidation();
	  },
	  success: function (label, element) {
	  	
	      var icon = $(element).parent('.input-icon').children('i');
	      $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	      icon.removeClass("fa-warning").addClass("fa-check");
	      tooltipValidation();
	  },
	
	  submitHandler: function (form) {
	      success2.show();
	      error2.hide();
	      //var testsuiteName = $('#testSuiteName').text();
	      runTestSuite();
	      document.getElementById("runTestCases").reset();
	      var $alertas = $('#runTestCases');
	      $alertas.validate().resetForm();
	      $alertas.find('.form-group').removeClass('has-success');
	      $alertas.find('.fa').removeClass('fa-check');
	      $("#modalRun").modal("hide");
	  }
	});
	return {
	//main function to initiate the module
	  init: function () {
	
	      handleValidation1();
	      handleValidation2();
	  }
	}
}

function getListTestCase(getTestsuiteID){
	console.log(getTestsuiteID);
	var testcases;
	$.ajax({
		url : jscontext+"/project/"+glProjectId+"/"+getTestsuiteID+"/getTestcases",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				testcases = result.data;
				console.log(testcases)
				drawTestCaseAsCheckBox(testcases);

			}
			return testcases;
      },
      error : function(request,error)
      {
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
      }
	})
}



function drawTestCaseAsCheckBox(testcases){
	
	$("#lsTestcases").empty();
	$.each(testcases, function(index, value){
		var htmlTestCase = '<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0">' +
								'<input type="checkbox" name="lsTestcases" id="testcase_'+index+'" value="'+value.testcase+'" checked="checked" >' +
								'<label for="testcase_'+index+'">'+ value.testcase + '</label>' +
							'</div>' ;
		if(value.statusTc == "add"){
			$("#lsTestcases").append(htmlTestCase);
		}
		
	});
	selectAllTestcaseModalRun();
}

function drawACaseAsCheckBox(testcase){
	
	$("#lsTestcases").empty();
		var htmlTestCase = '<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0">' +
								'<input type="checkbox" name="lsTestcases" id="testcase" value="'+testcase+'" checked="checked" >' +
								'<label for="testcase">'+ testcase + '</label>' +
							'</div>' ;
	$("#lsTestcases").append(htmlTestCase);
	selectAllTestcaseModalRun();
}

function nameValueToArrayValue(currentValue){
	return currentValue["value"];
}

function runTestSuite(){
	var testConcurrent = $("#testConcurrent").val();
		var executeTestDTO ={
				"runningName":$("#runTestCases [name='executeName']").val(),
				"type": "RUNNING",
				"runningUser":  username,
				"projectId": projectId,
				"testsuite": getTestsuiteID,
				"lsTestcases": $("#runTestCases [name='lsTestcases']").serializeArray().map(function(obj){return obj.value;}).join(";"),
				"runningDate": new Date().toISOString(),
				"runningOS": $("#runTestCases [name='runningOS']").val(),
				"timeout": $("#runTestCases [name='timeout']").val(),
				"runningLocation": $("#runTestCases [name='runningLocation']").val(),
				"screenRes": $("#runTestCases [name='screenRes']").val(),
				"mobileMode": $("#runTestCases [name='mobileMode']").prop("checked").toString(),
				"mobileRes":$("#runTestCases [name='deviceName']").val().toString(),
				"deviceName": $("#runTestCases [name='deviceName']")[0].selectedOptions[0].text,
				"browser": $("#runTestCases [name='browser']").val(),
				"recordVideo": $("#runTestCases [name='isRecordVideo']").prop("checked").toString(),
				"runDescription": $("#runTestCases [name='runDescription']").val(),
				"delaytime": "",
				"scheduleSpec": "",
				"projectName": $("#projectIdInModal").text(),
				"runOnLocal" : $("#runTestCases [name='runOnLocal']").prop("checked").toString()
		}
		
		$.ajax({
			url : jscontext+"/project/"+glProjectId+"/"+getTestsuiteID+"/"+testConcurrent+"/executeTest",
			type : "POST",
			data : JSON.stringify(executeTestDTO), 
			contentType : "application/json", 
			success : function(result) {
				if (!(result.errormessage) ) {
//					$("#calRunningTabs").trigger("click");
					window.location=jscontext + "/project/"+ projectId+"/runnings";
					//loadRunning();
				}
			  },
			  error : function(request,error)
			  {
				  var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					}
			  }
		
		})
}


function hideModalRun(){
	$('#modalRun').on('hidden.bs.modal', function () {
		$("#selectTimeRun").hide();
		$("#runTestCases")[0].reset();
		})
}

//func reset Form of run testsuite
function resetFormRunTestsuite() {
	$("#messageCheck").html("Require connected on local");
	$('#modalRun').on('hidden.bs.modal', function() {
		document.getElementById("runTestCases").reset();
	    var $alertas = $('#runTestCases');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}


$('#runOnLocal').click(function(event){
	
	var runningOs = $("#runTestCases [name='runningOS']").val()
    if($(this).is(':checked')){
    	event.preventDefault();
    	$("#messageCheck").html("Waiting ...");
    	$.ajax({
			url : jscontext+"/node/checkOnLocal?os=" + runningOs,
			type : "GET",
			contentType : "application/json", 
			success : function(result) {
				if (result) {
					$("#runOnLocal").prop( "checked", true);
					$("#messageCheck").html("Success");
				} else {
					$("#runOnLocal").prop( "checked", false);
					$("#messageCheck").html("Local machine not found");
				}
			  },
			  error : function(request,error)
			  {
				  var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					}
			  }
		})
    } else {
    	$("#messageCheck").html("Require connected on local");
    }
});


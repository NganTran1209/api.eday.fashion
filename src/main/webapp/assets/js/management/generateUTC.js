//func validate generateUTC
var validateGenerateUTC = function(isThis) {
	// for more info visit the official plugin documentation:
	// http://docs.jquery.com/Plugins/Validation
	var form2 = $('#generate_Testcase');
	var error2 = $('.alert-danger', form2);
	var success2 = $('.alert-success', form2);

	form2.validate({
		errorElement : 'span', // default input error message container
		errorClass : 'help-block help-block-error', // default input error
													// message class
		focusInvalid : true, // do not focus the last invalid input
		ignore : "", // validate all fields including form hidden input
		rules : {
			
			testcaseDescription : {
				required : true,
				minlength : 1
			},
			urlTest : {
				required : true,
				url: true
			}
		},
		messages : {
			
			testcaseDescription : getLang(currentLocale,"Cannot be blank!"),
			urlTest :{
				required : getLang(currentLocale,"Cannot be blank!"),	
			}
				
				
		},

		invalidHandler : function(event, validator) { // display error alert
														// on form submit
			success2.hide();
			error2.show();
			App.scrollTo(error2, -200);
		},

		errorPlacement : function(error, element) { // render error placement
													// for each input type
			var icon = $(element).parent('.input-icon').children('i');
			icon.removeClass('fa-check').addClass("fa-warning");
			icon.attr("data-original-title", error.text()).tooltip({
				'container' : 'body'
			});

		},

		highlight : function(element) { // hightlight error inputs
			$(element).closest('.form-group').removeClass("has-success")
					.addClass('has-error'); // set error class to the control
											// group
			tooltipValidation();
		},
		success : function(label, element) {

			var icon = $(element).parent('.input-icon').children('i');
			$(element).closest('.form-group').removeClass('has-error')
					.addClass('has-success'); // set success class to the
												// control group
			icon.removeClass("fa-warning").addClass("fa-check");
			tooltipValidation();
		},

		submitHandler : function(form) {
			success2.show();
			error2.hide();
			$("#btnGenerate").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
			$("#btnGenerate").prop("disabled", true);
			// func check Name TestCase is exist
			var newNameTestCase = $("#generateTC_name").val();
//			var testcase = listTestcase;
			var testsuiteName = $(isThis).attr("testsuite-name");
			var layoutFileName = $(isThis).attr("data-name");;
			
			generateTCs(testsuiteName, layoutFileName);

			var $alertas = $('#generate_Testcase');
			$alertas.find('.form-group').removeClass('has-success');
			$alertas.find('.fa').removeClass('fa-check');
		}

	});
	return {
		// main function to initiate the module
		init : function() {

			handleValidation1();
			handleValidation2();
		}
	}
}
function functionGenerateTc(isThis){
	$('#modalGenerateTestcase').modal({
		backdrop: 'static'
	});
	var testsuiteName = $(isThis).attr("testsuite-name");
	var select = document.getElementById("typeSelect");
	select.options.length = 0;
	var currentLayout = getCurrentLayout();
	var htmlOptions="";
	for(var i=0; i< currentLayout.Controls.length; i++){
		for(var j=0; j < currentLayout.Controls[i].Attributes.length; j++){
			var vb = currentLayout.Controls[i].Attributes[j];
			if(vb.Name == "type" && (vb.Value == "submit" || vb.Value == "button" || vb.Value == "a")){
				htmlOptions += ' <option value="'+ currentLayout.Controls[i].Name +'">'+currentLayout.Controls[i].Name+'</option>';
			}
		}
	}
	$(".typeSelect").append(htmlOptions);
	$('#modalGenerateTestcase').on('shown.bs.modal', function () {
		  $('#btnGenerate').attr("data-name",currentLayout["Name"]);
		  $('#btnGenerate').attr("testsuite-name",testsuiteName);
	})
	
	getAllMilestoneInLayout(function(milestones){
			$("#milestoneGen").empty();
			var htmlOptions = "";
			var number = 0;
			$.each(milestones,function(index, value){
				var dateStart = new Date(value["startDate"]);
				var dateEnd = new Date(value["endDate"]);
				
				var dateNow = new Date().getTime();
				if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){
					if(number == 0){
						htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" selected>'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
						number ++;
					} else{
						htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
					}
				} else{
					htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
				}
			})
			$("#milestoneGen").append(htmlOptions);
		})
}
//$('.btnGenerateItems').on('click', function(){
//	
//});
// func reset Form of modal Generate Test Case
function resetFormGenerate() {
	$('#modalGenerateTestcase').on('hidden.bs.modal',function() {
		document.getElementById("generate_Testcase").reset();
		var $alertas = $('#generate_Testcase');
		$("#errorNotification").text("");
		$alertas.validate().resetForm();
		$alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
		$alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

// func Generate Test Case

function generateTCs(testsuiteName, layoutFileName) {
//	$("#modalGenerateTestcase .loading-important").show();
//	var testcaseName = $('#generateTC_name').val();
	var testcaseName = "";
	var testcaseDescription = $('#generateTC_description').val();
	var testsuite = "";
	generateTCsFromLayout(testsuite, testsuiteName, layoutFileName, testcaseName,
			testcaseDescription);
}

$('#modalGenerateTestcase').on('hidden.bs.modal', function () {
	$("#generateTC_name").val("");
	$("#urlTest").val("");
})

function generateFullItemAttributes(layout){
	var tableItemList=["type","id","name", "class","xpath","href","textContent","initvalue","dataformat","readonly","disabled","maxlength","minlength","required","background-color","color","text-align", "errorAttributeName", "errorAttributeValue"];
	$.each(layout["Controls"], function(index,values){
		if(values["Attributes"].length < 19){
			var lsAtr = [];
			$.each(values["Attributes"], function(ind,val){
				lsAtr.push(val["Name"]);
			});
			var dict2 = {};

	        lsAtr.forEach(function(item) {
	            dict2[item] = true;
	        });

	        var result = tableItemList.reduce(function(prev, current) {
	            if (dict2.hasOwnProperty(current) === false) {
	                prev.push(current);
	            }
	            return prev;
	        }, [])

			for(var i = 0; i < result.length; i++){
				values["Attributes"].push({"Name": result[i], "Value": ""});
	        }
	    }
	})
	return layout;
}

var urlTest ='';
var typeControlClick = '';
function generateTCsFromLayout(testsuite, testsuiteName, layoutFileName, testcaseName,
		testcaseDescription) {
	var layout;
	function jsonGenerateTestcase(projectId, testsuiteName,testcaseDescription, testcaseName, ds, dataValid) {
//		testcaseDescription = testcaseName.split("_").join(" ");
		let json = '{';
		json += '"UpdatedBy":';
		json += '"' + projectId + '"';
		json += ',';
		json += '"Testsuite":';
		json += '"' + testsuiteName + '"';
		json += ',';
		json += '"Description":';
		json += '"' + testcaseDescription + '"';
		json += ',';
		json += '"CreatedBy":';
		json += '"' + projectId + '"';
		json += ',';
		json += '"TestcaseProcedure":';
		json += '""';
		json += ',';
		json += '"ExpectedOutput":';
		json += '""';
		json += ',';
		json += '"Testcase":';
		json += '"' + testcaseName + '"';
		json += ',';
		json += '"UpdatedDate":';
		json += '"' + ds + '"';
		json += ',';
		json += '"Testscripts":';
		json += '' + JSON.stringify(dataValid) + '';
		json += ',';
		json += '"CreatedDate":';
		json += '"' + ds + '"';
		json += ',';
		json += '"DeleteFlag":';
		json += 'false';
		json += ',';
		json += '"username":';
		json += '"' + projectId + '"';
		json += '}';
		return json;
	}
	
	// func read Layout
	$.ajax({
		url : jscontext + "/project/" + projectId + "/" + testsuiteName+ "/readFileLayout",
		type : "POST",
		data : layoutFileName,
		contentType : "application/json",
		success : function(result) {
			urlTest = $("#urlTest").val();
			typeControlClick = $("#typeSelect").val();
			layout = JSON.parse(result.data);
			booleanCheck = false;
			$.each(layout["Controls"], function(index,values){
				
				$.each(values["Attributes"], function(ind,val){
					if(val["Name"] != "type" && val["Name"] != "id" && val["Name"] != "name" && val["Name"] != "class" && val["Name"] != "xpath"){
	                    if(val["Value"] != "" && val["Value"] != "false"){
	                        booleanCheck = true;
	                    } else {
	                        if(booleanCheck != true){
	                        	booleanCheck = false;
	                        }
	                    }
	                }
				});
				
		   
			});
			if (booleanCheck == false) {
				$("#btnGenerate").html("<i class='ti-check'></i>"+ getLang(currentLocale,"Generate"));
				$("#btnGenerate").prop("disabled", false);
				$("#errorNotification").text("Haven't data to generate!!!");
				
			} else {
				layout = generateFullItemAttributes(layout);
				let theActionsRequired = [];
				let theActionsInitValue = [];
				let theActionsReadOnly = [];
				let theActionsDisable = [];
				let theActionsTextValue = [];
				let theActionsHref = [];
				let theActionsMinLength = [];
				let theActionsMaxLength = [];
				let theActionsBackgroundColor = [];
				let theActionsFontColor = [];
				let theActionsTextAlign = [];
				let theActionsCheckbox = [];
				let theActionsRadio = [];
				let count = 0;
				let selectCount = 0;
				tmpUrlTest = checkUrlTest(layoutFileName);
				
				var ds = new Date().toISOString();
//				var gmtRe = /GMT([\-\+]?\d{4})/;
//				var tz = gmtRe.exec(d)[1];
//				var ds = d.getFullYear() + "-" + (d.getMonth() + 1) + '-'
//						+ d.getDate() + " " + d.getHours() + ":" + d.getMinutes()
//						+ ":" + d.getSeconds() + " " + tz;
				
				theActionsInitValue = checkInitValue(layoutFileName, layout);
				theActionsReadOnly = checkReadOnly(layoutFileName, layout);
				theActionsDisable = checkDisable(layoutFileName, layout);
				theActionsTextValue = checkTextValue(layoutFileName, layout);
				theActionsHref = checkHref(layoutFileName, layout);
				theActionsMinLength = checkMinlength(layoutFileName, layout);
				theActionsMaxLength = checkMaxlength(layoutFileName, layout);
				theActionsRequired = checkRequired(layoutFileName, layout);
				theActionsBackgroundColor = checkBackgroundColor(layoutFileName, layout);
				theActionsFontColor = checkFontColor(layoutFileName, layout);
				theActionsTextAlign = checkTextAlign(layoutFileName, layout);
				theActionsCheckbox = checkbox(layoutFileName, layout);
				theActionsRadio = checkRadio(layoutFileName, layout);
				
				for (var i = 0; i < layout.Controls.length; i++) {
					let theActionsDataFormatInput = [];
					let theActionsSelectBox = [];
					tmpUrlTest = checkUrlTest(layoutFileName);
//					if (tmpUrlTest.length != 0) {
//						for (var q = 0; q < tmpUrlTest.length; q++) {
							theActionsDataFormatInput.push(tmpUrlTest);
							theActionsSelectBox.push(tmpUrlTest);
//						}
//					}
					theControl = layout.Controls[i];
					//check format data after input
					var values = checkDataFormatInput(layoutFileName, theControl);
					var tmpDataFormatInput = values.first;
					var controlName = values.second;
				    if (tmpDataFormatInput.length > 0) {
				        count += 1;
				        for(var l = 0; l < tmpDataFormatInput.length; l++) {
				            theActionsDataFormatInput.push(tmpDataFormatInput[l]);
				        }
				    }
				    
				    var layoutNameDataFormatInput = 'check dataFormat ' + controlName;
					let jsonCheckDataFormatInput = jsonGenerateTestcase(glProjectId, testsuiteName,
							layoutNameDataFormatInput, layoutNameDataFormatInput, ds,theActionsDataFormatInput);
					if(theActionsDataFormatInput.length > 1) {
						saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonCheckDataFormatInput, layoutNameDataFormatInput,testsuite);
					}
					
					//check select box
					var selectvalues = checkSelectBox(layoutFileName, theControl);
					var tmpSelectBox = selectvalues.slect1;
					var controlNameSl = selectvalues.slect2;
				    if(tmpSelectBox.length > 0) {
				        selectCount += 1;
				        for(var t = 0; t < tmpSelectBox.length; t++) {
				            theActionsSelectBox.push(tmpSelectBox[t]);
				        }
				    }
				    
				    var layoutNameSelectbox = 'check selectbox ' + controlNameSl;
					let jsonCheckSelectBox = jsonGenerateTestcase(glProjectId, testsuiteName,
							layoutNameSelectbox, layoutNameSelectbox, ds,theActionsSelectBox);
					if(theActionsSelectBox.length > 1) {
						saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonCheckSelectBox, layoutNameSelectbox,testsuite);
					}
				}
				
				// write testcase required
				var layoutNameRequired = 'check required';
				let jsonCheckRequired = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameRequired, layoutNameRequired, ds,theActionsRequired);
				if(theActionsRequired.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonCheckRequired, layoutNameRequired,testsuite);
				}
				
				//write testcase initvalue
				var layoutNameInitValue = 'check initValue';
				let jsonCheckInitValue = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameInitValue, layoutNameInitValue, ds,theActionsInitValue);
				if(theActionsInitValue.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonCheckInitValue, layoutNameInitValue,testsuite);
				}
				
				//write testcase ReadOnly
				var layoutNameReadOnly = 'check readOnly';
				let jsonCheckReadOnly = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameReadOnly, layoutNameReadOnly, ds,theActionsReadOnly);
				if(theActionsReadOnly.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonCheckReadOnly, layoutNameReadOnly,testsuite);
				}
				
				//write testcase Disable 
				var layoutNameDisable = 'check disable';
				let jsonCheckDisable = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameDisable, layoutNameDisable, ds,theActionsDisable);
				if(theActionsDisable.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonCheckDisable, layoutNameDisable,testsuite);
				}
				
				//write testcase TextValue
				var layoutNameTextValue = 'check textValue';
				let jsoncheckTextValue = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameTextValue, layoutNameTextValue, ds,theActionsTextValue);
				if(theActionsTextValue.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckTextValue, layoutNameTextValue,testsuite);
				}
				
				//write testcase Href
				var layoutNameHref = 'check href';
				let jsoncheckHref = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameHref, layoutNameHref, ds,theActionsHref);
				if(theActionsHref.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckHref, layoutNameHref,testsuite);
				}
				
				//write testcase MinLength
				var layoutNameMinLength = 'check minLength';
				let jsoncheckMinLength = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameMinLength, layoutNameMinLength, ds,theActionsMinLength);
				if(theActionsMinLength.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckMinLength, layoutNameMinLength,testsuite);
				}
				
				//write testcase MaxLength
				var layoutNameMaxLength = 'check maxLength';
				let jsoncheckMaxLength = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameMaxLength, layoutNameMaxLength, ds,theActionsMaxLength);
				if(theActionsMaxLength.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckMaxLength, layoutNameMaxLength,testsuite);
				}
				
				//write testcase background color
				var layoutNameBackgroundColor = 'check backgroundColor';
				let jsoncheckBackgroundColor = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameBackgroundColor, layoutNameBackgroundColor, ds,theActionsBackgroundColor);
				if(theActionsBackgroundColor.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckBackgroundColor, layoutNameBackgroundColor,testsuite);
				}
				
				//write testcase font color
				var layoutNameFontColor = 'check fontColor';
				let jsoncheckFontColor = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameFontColor, layoutNameFontColor, ds,theActionsFontColor);
				if(theActionsFontColor.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckFontColor, layoutNameFontColor,testsuite);
				}
				
				//write testcase text align
				var layoutNameTextAlign = 'check textAlign';
				let jsoncheckTextAlign = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameTextAlign, layoutNameTextAlign, ds,theActionsTextAlign);
				if(theActionsTextAlign.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckTextAlign, layoutNameTextAlign,testsuite);
				}
				
				//write testcase checkbox
				var layoutNameCheckbox = 'checkbox';
				let jsoncheckbox = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameCheckbox, layoutNameCheckbox, ds,theActionsCheckbox);
				if(theActionsCheckbox.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsoncheckbox, layoutNameCheckbox,testsuite);
				}
				
				//write testcase radio
				var layoutNameRadio = 'check radio';
				let jsonradio = jsonGenerateTestcase(glProjectId, testsuiteName,
						layoutNameRadio, layoutNameRadio, ds,theActionsRadio);
				if(theActionsRadio.length > 1) {
					saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, jsonradio, layoutNameRadio,testsuite);
				}
				
//				$("#modalGenerateTestcase .loading-important").fadeOut();
				$("#modalGenerateTestcase").modal("hide");
				$("#btnGenerate").html("<i class='ti-check'></i>"+ getLang(currentLocale,"Generate"));
				$("#btnGenerate").prop("disabled", false);
				$("#generateTC_name").val("");
				$("#urlTest").val("");
			}
			
	
			function saveFileTestcase(jscontext, projectId, testsuiteName, layoutFileName, json, layoutName, testsuite){
				
				            var listTestscript = JSON.parse(json)["Testscripts"];
				            var listTestcaseProcedure = "";
				            var listExpectedOutput = "";
				            for(var p = 0; p < listTestscript.length ; p++){
				            	var obj = listTestscript[p];
				                listTestcaseProcedure = generateTestcaseProcedure(obj, p, listTestcaseProcedure);
				                listExpectedOutput = generateExpectedOutpute(obj, p, listExpectedOutput);
				            }
				            var jsonText = JSON.parse(json);
				            jsonText["TestcaseProcedure"] = listTestcaseProcedure;
				            jsonText["ExpectedOutput"] = listExpectedOutput;
				            testsuite = jsonText["Testsuite"];
				            
				            var projectPro = {
			    					"projectId": projectId,
			    					"testsuite": testsuiteName,
			    					"testcase": ""
			    			}
			    			var automationTestcase = {
			    					"projectProgressIdentity": projectPro,
			    				    "description": jsonText["Description"],
			    				    "expectedOutput": jsonText["ExpectedOutput"],
			    				    "testcaseProcedure": jsonText["TestcaseProcedure"],
			    				    "updateDate": new Date().toISOString(),
			    				    "testscript": JSON.stringify(jsonText["Testscripts"]),
			    				    "createDate": new Date().toISOString(),
			    				    "updateUser": projectId,
			    				    "assignTo": "",
			    				    "preCondition": "",
			    				    "status": "add",
			    				    "type": "automation",
			    				    "statusIssue": "New",
								    "planStartDate": "",
								    "planEndDate": "",
								    "estimatedTime": "",
								    "actualTime": "",
								    "category": "",
								    "orderId": 0,
								    "milestone": $("#milestoneGen").val(),
			    			}
				            
				            $.ajax({
				                url : jscontext + "/project/" + projectId + "/"+ testsuiteName + "/saveTestCase",
				                type : "POST",
				                data : JSON.stringify(automationTestcase),
				                contentType : "application/json",
				                async : false,
				                success : function(result) {
//				                	reloadListTestcase();
				                    $("#modalGenerateTestcase").modal("hide");
				                    $("#bodyTestcaseRecord").empty();
				                    document.getElementById("generate_Testcase").reset();
				                    
				                },
				        		error : function(xhr,e) {
				        			console.log("ERROR: ", e);
				        			var statusCode = xhr.status;
				        			if (statusCode == 403) {
				        				window.location= jscontext + "/login";
				        			}
				        		},
				        		done : function(e) {
				        			console.log("DONE");
				        		}
				            });
				            swal({
				                title : getLang(currentLocale, "TestCase has been updated!!!"),
				                type : "success",
				                timer : 1000,
				                showConfirmButton : false
				            });
//				            drawTestsuiteReport();
//				        },
//						error : function(xhr,e) {
//							console.log("ERROR: ", e);
//							var statusCode = xhr.status;
//							if (statusCode == 403) {
//								window.location= jscontext + "/login";
//							}
//						},
//						done : function(e) {
//							console.log("DONE");
//						}
//
//				});
			}

			
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
		
	});
	
	function reloadListTestcase(){
		$.ajax({
			url : jscontext +"/project/" + projectId+"/"+ glTestsuiteId +"/getAllTestcases",
			type : "get",
			data : "",
			async: false,
			contentType : "application/json", 
			success : function(result) {
				
				if (!(result.errormessage) ) {
					listTestcase = result.data;
				} else {
				}
			},
			error : function(request, e) {
				console.log("ERROR: ", e);
				
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
						title: getLang(currentLocale,"Could not load get All Testcase"),
				        text: getLang(currentLocale,"You can reload the page again!"),
				        type: "warning", 
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}

	// func check URL
	function checkUrlTest(layoutFileName) {
		var act;
		var actions = [];
		if (urlTest != null || urlTest != undefined) {
			act = {
				ClassName : "AC_Open",
				Language : {
					en : {
						StepName : "Navigate open",
						ActionName : "open"
					}
				},
				Params : [ {
					Name : "Url",
					Value : urlTest
				} ],
			}
			actions.push(act);
		}
		return act;
	}

	// func checkInitValue
	function checkInitValue(layoutFileName, layout) {
		var listInitValue = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listInitValue.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let act1;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "initvalue"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check init value of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						},{
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listInitValue.push(act);
				}
			}
		}
		return listInitValue;
	}

	// action checkControlID
	function checkControlID(layoutFileName, control) {
		theAttribute = control["Attributes"];
		var act;
		for (var j = 0; j < theAttribute.length; j++) {
			if (theAttribute[j].Name == "id"
					&& theAttribute[j].Value != undefined
					&& theAttribute[j].Value != "") {
				act = {
					ClassName : "AC_CheckControlAttribute",
					Language : {
						en : {
							StepName : "Check id of" + " [" + control.Name
									+ "]",
							ActionName : "check control attribute"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : control.Name
					}, {
						Name : "Index",
						Value : "0"
					},{
						Name : "Attribute",
						Value : theAttribute[j].Name
					}, {
						Name : "Value",
						Value : theAttribute[j].Value
					} ]
				}
			}
		}
		return act;
	}

	// action checkControlName
	function checkControlName(layoutFileName, control) {
		var act;
		theAttribute = control["Attributes"];
		for (var j = 0; j < theAttribute.length; j++) {
			if (theAttribute[j].Name == "name"
					&& theAttribute[j].Value != undefined
					&& theAttribute[j].Value != "") {
				act = {
					ClassName : "AC_CheckControlAttribute",
					Language : {
						en : {
							StepName : "Check name of" + " [" + control.Name
									+ "]",
							ActionName : "check control attribute"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : control.Name
					}, {
						Name : "Index",
						Value : "0"
					},{
						Name : "Attribute",
						Value : theAttribute[j].Name
					}, {
						Name : "Value",
						Value : theAttribute[j].Value
					} ],
				}
			}
		}
		return act;
	}

	// action checkControlName
	function checkControlClass(layoutFileName, control) {
		var act;
		theAttribute = control["Attributes"];
		for (var j = 0; j < theAttribute.length; j++) {
			if (theAttribute[j].Name == "class"
					&& theAttribute[j].Value != undefined
					&& theAttribute[j].Value != "") {
				act = {
					ClassName : "AC_CheckControlAttribute",
					Language : {
						en : {
							StepName : "Check class of" + " [" + control.Name
									+ "]",
							ActionName : "check control attribute"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : control.Name
					}, {
						Name : "Index",
						Value : "0"
					},{
						Name : "Attribute",
						Value : theAttribute[j].Name
					}, {
						Name : "Value",
						Value : theAttribute[j].Value
					} ],
				}

			}
		}
		return act;
	}

	function actionClickButton(layoutFileName) {
		var act;
		for (var i = 0; i < layout.Controls.length; i++) {
			for (var j = 0; j < layout.Controls[i]["Attributes"].length; j++) {
				act = {
					ClassName : "AC_Click",
					Language : {
						en : {
							StepName : "click button",
							ActionName : "click"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : layout.Controls[i].Name
					},{
						Name : "Index",
						Value : "0"
					}],
				}
			}
		}
		return act;
	}

	function actionClickSubmit(layoutFileName) {
		var act;
		for (var i = 0; i < layout.Controls.length; i++) {
			for (var j = 0; j < layout.Controls[i]["Attributes"].length; j++) {
				act = {
					ClassName : "AC_Click",
					Language : {
						en : {
							StepName : "click submit",
							ActionName : "click"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : layout.Controls[i].Name
					},{
						Name : "Index",
						Value : "0"
					} ],
				}
			}
		}
		return act;
	}

	// func checkDisable
	function checkDisable(layoutFileName, layout) {
		var listDisable = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listDisable.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "disabled"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value == "true") {

					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check disabled of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						},{
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listDisable.push(act);
				}
			}
		}
		return listDisable;
	}

	// function checkHref
	function checkHref(layoutFileName, layout) {
		var listHref = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listHref.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "href"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check href of" + " [" + control.Name
										+ "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						},{
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listHref.push(act);
				}
			}
		}
		return listHref;
	}

	// function check backgroundColor
	function checkBackgroundColor(layoutFileName, layout) {
		var listBackgroundColor = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listBackgroundColor.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 1; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "background-color"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check background-color of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listBackgroundColor.push(act);
				}
			}
		}
		return listBackgroundColor;
	}

	// func CheckMinlength
	function checkMinlength(layoutFileName, layout) {
		var listMinlength = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listMinlength.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			var actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "minlength"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check text value of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listMinlength.push(act);
				}
			}
		}
		return listMinlength;
	}

	// function check font color
	function checkFontColor(layoutFileName, layout) {
		var listFontColor = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listFontColor.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 1; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "color"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check color of" + " [" + control.Name
										+ "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listFontColor.push(act);
				}
			}
		}
		return listFontColor;
	}

	// function check text align
	function checkTextAlign(layoutFileName, layout) {
		var listTextAlign = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listTextAlign.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 1; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "text-align"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check text align of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listTextAlign.push(act);
				}
			}
		}
		return listTextAlign;
	}
	// func generate input check dataformat
	function checkDataFormatInput(layoutFileName, control) {
		let act;
		let actions = [];
		let theAttribute = control["Attributes"];
		let strType = theAttribute.find(el => el.Name === "type");
	    let type = strType["Value"];
	    let strDataFormat = theAttribute.find(el => el.Name === "dataformat");
	    let dataformat = strDataFormat["Value"];
	    let strErrorAttributeName = theAttribute.find(el => el.Name === "errorAttributeName");
	    let errorAttributeName = strErrorAttributeName["Value"];
	    let strErrorAttributeValue = theAttribute.find(el => el.Name === "errorAttributeValue");
	    let errorAttributeValue = strErrorAttributeValue["Value"];
	    if(type == "text" && dataformat != "" && errorAttributeName != "" && errorAttributeValue != "") {
	        //number #,###,###.###
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$random(10)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            },{
					Name : "Index",
					Value : "0"
				}],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);
	        
	        //hiragana
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$hiragana(10)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				} ],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // katakana
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$katakana(10)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				}],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date yyyy/mm/dd
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$date(yyyy/mm/dd HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				} ],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date yyyy/dd/mm
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$date(yyyy/dd/mm HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				} ],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date dd/mm/yyyy
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$date(dd/mm/yyyy HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				}],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date mm/dd/yyyy
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$date(mm/dd/yyyy HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				}],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date yyyy/mmm/dd
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$date(yyyy/mmm/dd HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            },
	            {
					Name : "Index",
					Value : "0"
				}],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date yyyy/dd/mmm
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				},{
	                Name : "Value",
	                Value : "$date(yyyy/dd/mmm HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				} ],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date dd/mmm/yyyy
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				}, {
	                Name : "Value",
	                Value : "$date(dd/mmm/yyyy HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				} ],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);

	        // date mmm/dd/yyyy
	        act = {
	            ClassName : "AC_Enter",
	            Language : {
	                en : {
	                    StepName : "Enter" + " [" + control.Name + "]",
	                    ActionName : "enter"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : control.Name
	            }, {
					Name : "Index",
					Value : "0"
				},{
	                Name : "Value",
	                Value : "$date(mmm/dd/yyyy HH:MM:SS)"
	            } ],
	        }
	        actions.push(act);
	        act = {
	            ClassName : "AC_Click",
	            Language : {
	                en : {
	                    StepName : "Click" + " [" + typeControlClick + "]",
	                    ActionName : "click"
	                }
	            },
	            Params : [ {
	                Name : "Screen",
	                Value : layoutFileName
	            }, {
	                Name : "Item",
	                Value : typeControlClick
	            }, {
					Name : "Index",
					Value : "0"
				}],
	        }
	        actions.push(act);
	        act = {
	            ClassName: "AC_CheckControlAttribute",
	            Language: {
	                en: {
	                    StepName: "Check control attribute of" + " [" + control.Name + "]",
	                    ActionName: "check control attribute"
	                }
	            },
	            Params: [
	                { 
	                    Name: "Screen", 
	                    Value: layoutFileName
	                },
	                { 
	                    Name: "Item", 
	                    Value: control.Name
	                },{
						Name : "Index",
						Value : "0"
					},
	                { 
	                    Name: "Attribute", 
	                    Value: errorAttributeName
	                },
	                { 
	                    Name: "Value", 
	                    Value: errorAttributeValue
	                }
	            ],
	        }
	        actions.push(act);
	    }
		return {
			first: actions,
	        second: control.Name,
		};
	}

	// func selectbox
	function checkSelectBox(layoutFileName, control) {
		let act;
		let actions = [];
		let theAttribute = control["Attributes"];
		for (let j = 0; j < theAttribute.length; j++) {
			if (theAttribute[j].Name == "type"
					&& theAttribute[j].Value == "select") {
				act = {
					ClassName : "AC_GetControlValue",
					Language : {
						en : {
							StepName : "get control value",
							ActionName : "get control value"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : control.Name
					}, {
						Name : "Index",
						Value : "0"
					},{
						Name : "Value",
						Value : "#selectValue"
					} ]
				}
				actions.push(act);
				act = {
					ClassName : "AC_Loop",
					Language : {
						en : {
							StepName : "loop",
							ActionName : "loop"
						}
					},
					Params : [ {
						Name : "Times",
						Value : "len(#selectValue)"
					}, {
						Name : "Variable",
						Value : "#item"
					} ]
				}
				actions.push(act);
				act = {
					ClassName : "AC_Select",
					Language : {
						en : {
							StepName : "select value" + " [" + control.Name
									+ "]",
							ActionName : "select"
						}
					},
					Params : [ {
						Name : "Screen",
						Value : layoutFileName
					}, {
						Name : "Item",
						Value : control.Name
					}, {
						Name : "Index",
						Value : "0"
					},{
						Name : "Value",
						Value : "#item[1]"
					} ]
				}
				actions.push(act);
				act = {
					ClassName : "AC_EndLoop",
					Language : {
						en : {
							StepName : "end loop",
							ActionName : "end loop"
						}
					},
					Params : []
				}
				actions.push(act);
			}
		}
		return {
			slect1: actions,
			slect2: control.Name,
		};
	}

	// function checkbox
	function checkbox(layoutFileName, layout) {
		var listcheckbox = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listcheckbox.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			let theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "type"
						&& theAttribute[j].Value == "checkbox") {
					act = {
						ClassName : "AC_CheckControlValue",
						Language : {
							en : {
								StepName : `check checkbox ${control.Name}`,
								ActionName : "check control value"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Operator",
							Value : "equal"
						}, {
							Name : "Expected",
							Value : "true"
						} ]
					}
					listcheckbox.push(act);
					act = {
						ClassName : "AC_CheckControlValue",
						Language : {
							en : {
								StepName : `check checkbox ${control.Name}`,
								ActionName : "check control value"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						},{
							Name : "Operator",
							Value : "equal"
						}, {
							Name : "Expected",
							Value : "false"
						} ]
					}
					listcheckbox.push(act);
				}
			}
		}
		
		return listcheckbox;
	}

	// func radio
	function checkRadio(layoutFileName, layout) {
		var listRadio = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listRadio.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			let theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "type"
						&& theAttribute[j].Value == "radio") {
					act = {
						ClassName : "AC_CheckControlValue",
						Language : {
							en : {
								StepName : `check radio ${control.Name}`,
								ActionName : "check control value"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Operator",
							Value : "equal"
						}, {
							Name : "Expected",
							Value : "true"
						} ]
					}
					listRadio.push(act);
					act = {
						ClassName : "AC_CheckControlValue",
						Language : {
							en : {
								StepName : `check radio ${control.Name}`,
								ActionName : "check control value"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Operator",
							Value : "equal"
						}, {
							Name : "Expected",
							Value : "false"
						} ]
					}
					listRadio.push(act);
				}
			}
		}
		return listRadio;
	}

	// func check Maxlength
	function checkMaxlength(layoutFileName, layout) {
		var listMaxlength = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listMaxlength.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			var actions = [];
			theAttribute = control["Attributes"];
			for (let j = 1; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "maxlength"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check text value of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listMaxlength.push(act);
				}
			}
		}
		return listMaxlength;
	}

	// func checkDisplayValue
	function checkTextValue(layoutFileName, layout) {
		var listTextValue = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listTextValue.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "textContent"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value != "") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check text value of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listTextValue.push(act);
				}
			}
		}
		
		return listTextValue;
	}

	// func checkReadOnly
	function checkReadOnly(layoutFileName, layout) {
		var listReadOnly = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listReadOnly.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "readonly"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value == "true") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check readonly of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listReadOnly.push(act);
				}
			}
		}
		
		return listReadOnly;
	}

	// func check required
	function checkRequired(layoutFileName, layout) {
		var listRequired = [];
		tmpUrlTest = checkUrlTest(layoutFileName);
		listRequired.push(tmpUrlTest);
		for (var i = 0; i < layout.Controls.length; i++) {
			var control = layout.Controls[i];
			let act;
			let actions = [];
			theAttribute = control["Attributes"];
			for (let j = 0; j < theAttribute.length; j++) {
				if (theAttribute[j].Name == "required"
						&& theAttribute[j].Value != undefined
						&& theAttribute[j].Value == "true") {
					act = {
						ClassName : "AC_CheckControlAttribute",
						Language : {
							en : {
								StepName : "Check required of" + " ["
										+ control.Name + "]",
								ActionName : "check control attribute"
							}
						},
						Params : [ {
							Name : "Screen",
							Value : layoutFileName
						}, {
							Name : "Item",
							Value : control.Name
						}, {
							Name : "Index",
							Value : "0"
						}, {
							Name : "Attribute",
							Value : theAttribute[j].Name
						}, {
							Name : "Value",
							Value : theAttribute[j].Value
						} ],
					}
					listRequired.push(act);
				}
			}
		}
		return listRequired;
	}

}

function generateTestcaseProcedure(testscript, count, listTestcaseProcedure){
	count += 1;
	var lsActions = listActions;
	var params = testscript["Params"];
	var currentAction = getActionByClassName( testscript["ClassName"]);
	var descriptstr= currentAction.data.description;
    for(var i = 0; i < params.length; i++) {
	  	descriptstr= descriptstr.split("{"+i+"}").join(params[i].Value);
	}
    descriptstr1 = count + ". " + descriptstr;		
    if (currentAction.data.type == "in") {
    	if (listTestcaseProcedure == ""){
    		listTestcaseProcedure = descriptstr1;
    	} else {
    		listTestcaseProcedure = listTestcaseProcedure + "<br>" + descriptstr1;
    	}
    	
    } 
    return listTestcaseProcedure;
}

function generateExpectedOutpute(testscript, count, listExpectedOutput){
	count += 1;
	var lsActions = listActions;
	var params = testscript["Params"];
	var currentAction = getActionByClassName( testscript["ClassName"]);
	var descriptstr= currentAction.data.description;
    for(var i = 0; i < params.length; i++) {
	  	descriptstr= descriptstr.split("{"+i+"}").join(params[i].Value);
	}
    descriptstr1 = count + ". " + descriptstr;		
    if (currentAction.data.type == "out") {
    	if(listExpectedOutput == "") {
    		listExpectedOutput = descriptstr1;
    	} else {
    		listExpectedOutput = listExpectedOutput + "<br>" + descriptstr1;
    	}
    }
    
    return listExpectedOutput;
}

function getActionByClassName(className){
	for (var i =0 ; i < listActions.length; i++){
		if (listActions[i].data.test_class == className){
			return listActions[i];
		}
	}
	return null;
}
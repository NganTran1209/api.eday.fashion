
var glTestdataList = null;
var glTestdataListNotJson = null;
var glTestCaseList= null;
var glTestResulList= null;
var glLayoutList=null;
var tmpCaseNums = null;
var listTestcases;
var tableItemList=["controlName","type","id","name", "class","xpath","href","textContent","initvalue","dataformat","readonly","disabled","maxlength","minlength","required","background-color","color","text-align", "errorAttributeName", "errorAttributeValue"];
var conditionList=["id","name", "class","href","textContent","xpath"];
jQuery(document).ready(function() {
	validateAddLayout();
	validateAddLayoutDetail();
});

function getDetailTestcase(testsuiteName, itemNameTestcase){
	$("#loadDetailTestCase").show();
//	var itemNameTestcase = $(isThis).attr("data-name");
//	var testsuiteName = $(isThis).attr("testsuite-name");
	// enable CTRL + A in input field
	$('#sortable').keydown(function(e){
	   if (e.keyCode == 65 && e.ctrlKey) {
	     e.target.select()
	   }
	});
	$( "#sortable" ).sortable({
        connectWith: ".sortOk"
    }).disableSelection();

      
	$("#tableListTestcase").hide();
	$("#detailsTestCase").slideToggle();
	
	$("#titleTestcase").text(itemNameTestcase);
	
	getDetailTestcasebyAPI(itemNameTestcase, testsuiteName);
	
	loadLayouts(testsuiteName);
	
	loadTestdatas(testsuiteName);
	
	loadTestcases(testsuiteName);
	
//	loadResults();
	
}

// get test suite and update to global variable testsuite, JsonTest
function getDetailTestcasebyAPI(itemNameTestcase, testsuiteName){

	$.ajax({
	url : jscontext + "/project/" +projectId +"/" + testsuiteName + "/getTestcase/" + itemNameTestcase,
	type : "GET",
	data : "",
	async: false,
	contentType : "application/json",
	success : function(result) {
		$("#loadDetailTestCase").fadeOut("slow");
		if (!(result.errormessage) ) {
			testcaseRecord = result.data;
				if(testcaseRecord.projectProgressIdentity["testcase"] == itemNameTestcase){
					
					if(testcaseRecord["assignTo"] != ""){
						$("#filterAssignTos").val(testcaseRecord["assignTo"]);
					} else {
						$("#filterAssignTos").val(lsAssignTo[0].username);
					}
					
					if(testcaseRecord["statusIssue"] != ""){
						$("#statusIssueDetail").val(testcaseRecord["statusIssue"]);
					} 
					
					if(testcaseRecord["category"] != ""){
						$("#categoryIssueDetail").val(testcaseRecord["category"]);
					}
					
					$("#planStarDa").val((testcaseRecord["planStartDate"] ? new Date(testcaseRecord["planStartDate"]).toISOString().split("T")[0]: ""));
					$("#planEndDa").val((testcaseRecord["planEndDate"] ? new Date(testcaseRecord["planEndDate"]).toISOString().split("T")[0]: ""));
					$("#estimatedTimeDetail").val(testcaseRecord["estimatedTime"]);
					$("#actualTimeDetail").val(testcaseRecord["actualTime"]);
					$("#testcaseDescription").val(testcaseRecord["description"]);
					$("#milestoneTCDetail option[value='" + testcaseRecord["milestone"] + "']").attr('selected','selected');
					drawTestCaseDetail(testcaseRecord);
					$(function () {
						$('[data-toggle="tooltip"]').tooltip()
					});
					
					return;
				}
		}
		
  },
  error : function(request,error)
  {
	  $("#loadDetailTestCase").fadeOut("slow");
	  console.log(error);
	  var statusCode = request.status;
		if (statusCode == 403) {
			window.location= jscontext + "/login";
		}
      
  }
});
	
}

// Draw a testcase
function drawTestCaseDetail(theTestCase){
	if (theTestCase["testscript"] != "" && theTestCase["testscript"] != null) {
		lsTestscript = JSON.parse(theTestCase["testscript"]);
			if ($("#sortable").length == 0) {
			$("#sortable").empty();
		}
//		$("#sortable").empty();
		for (var i=0; i < lsTestscript.length ; i++){
			$("#sortable").append(createStep(lsTestscript[i],i));
		}
		applySelect2All();
		onChangeInputStep();
	}
}
function onChangeInputStep(){
	var inputStep = $("#sortable li").find("input");
	inputStep.on('change',function(){
		if($(this).hasClass("select2-hidden-accessible") == false){
			existEdit = true;
		}
	});
}
// Create step UI for the scriptStep 
// if scriptStep is null, create new step
function createStep(scriptStep,stepIndex){
	var stepName ="";
	var className ="";
	// check not new step
	if (scriptStep != null && scriptStep != undefined){
		stepName= scriptStep["Language"]["en"]["StepName"];
		className=scriptStep["ClassName"];
	}
	var template = 
		'<li class="testscript formStyle">'+
		'<div class="d-flex styleForm">'+
		'        <div class="helpButton" id="stepHelpButton" onclick="helpStep(this);" data-toggle="tooltip" title="'+getLang(currentLocale,'Help')+'">'+
		'            <i class="ti-help" id="step"></i></div>'+
		'   	 <div class="row step"> '+
		'           <div class="bottest ClassName col-md-2 col-sm-12 col-xs-12 itemContents">'+
		'               <label>'+getLang(currentLocale,'Action')+':</label>'+
		'               <div><input class="bottest ClassName form-control" name="ClassName"  value = "'+ className + '" />'+
		'               </div>' +
		'		    </div>';
	// check not new step
		var theAction =getActionByClassName(className);
		template += drawActionParams(theAction,scriptStep,stepIndex);
		template +=

		'        <div class="actionTestcase">'+
		'            <div class="addStepButton bg-success" id="stepAddButton" onclick="appendStep(this)" data-toggle="tooltip" title="'+getLang(currentLocale,'Add New Step')+'">'+
		'                <i class="icon-plus"></i>'+
		'            </div>'+
		'            <div id="stepDragButton" class="dragStepButton bg-warning" data-toggle="tooltip" title="'+getLang(currentLocale,'Move Step')+'">'+
		'                <i class="ti-move" id="step"></i>'+
		'            </div>'+
//		'            <div id="stepDragButton" class="editStepButton bg-info" data-toggle="tooltip" title="Edit Step">'+
//		'                <i class="icon-note" id="step"></i>'+
//		'            </div>'+
		'            <div class="copyStepButton" id="stepCopyButton" onclick="copyStep(this)" data-toggle="tooltip" title="'+getLang(currentLocale,'Copy Step')+'">'+
		'                <i class="ti-layers"></i>'+
		'            </div>'+
		'            <div class="deleteButton" id="stepDeleteButton" onclick="deleteStep(this)" data-toggle="tooltip" title="'+getLang(currentLocale,'Delete Step')+'">'+
		'                <i class="ti-trash" id="step"></i>'+
		'            </div>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'</div>'+
		'<div class="helpIfo">'+
		'</div>'+
		'</li>'	;
		

	return template;
	
}

function helpStep(theElement){
	let helpClass = $(theElement).parents("li.testscript").find("input.bottest.ClassName").val();
	let helpValue= getHelpValue(helpClass);
	$(theElement).parents(".styleForm").next().toggle();
	$(theElement).parents(".styleForm").next().html(helpValue);
}

function getHelpValue(className){
	var htmlHelp='';
	if(className != "" && actionHelp[className] != ""){
		var help = actionHelp[className]["document"];
		if(typeof(help)=="object"){
			help=Object.entries(help);
			for(var i = 0 ; i < help.length ; i++ ){
				if(currentLocale == "en"){
					var content = help[i][1].split('. ').join('. <br/>');
				} else{
					var content = help[i][1].split('。　').join('。 <br/>');
				}
//				var contentNew = content.substring(0,content.length - 6); 
				htmlHelp  +='<div class="d-flex ml-5 documentHelp">'+help[i][0]+' :  <p class="contentHelp"> '+content+'</p></div>';
			}
		} else{
			htmlHelp = '<div><p> '+help+'</p></div>';;
		}
		return '<div class=""><div class=""><p>'+getLang(currentLocale,"Description:")+'</p>'+htmlHelp+'</div></div><div class="pictureHelp d-flex"><p class="mr-3">'+getLang(currentLocale,"Example")+':</p> <img class="pictureEx" src="'+context+actionHelp[className]["picture"]+'"></div>';	
	} else{
		if(currentLocale == "en"){
			return '<div class="d-flex">'+getLang(currentLocale,"Description")+':<p class=" ml-5">Choose action</p></div><div class="pictureHelp">'+getLang(currentLocale,"Example")+':  <img src="'+context+'/assets/img/AC_example.PNG"></di>';
		} else{
			return '<div class="d-flex">'+getLang(currentLocale,"Description")+':<p class=" ml-5">動作選択</p></div><div class="pictureHelp">'+getLang(currentLocale,"Example")+':  <img src="'+context+'/assets/img/AC_example_ja.PNG"></di>';
		}
	} 
	
}


function drawActionParams(theAction, scriptStep,stepIndex){
	var template ="";
	if (theAction == null){
		return "";
	}
	if (theAction.data.test_class === "AC_API"){
		var value = scriptStep?JSON.stringify(scriptStep["Params"],undefined, 4):"";
		value = '{ "Params":' + value +'}';
		var html =
			'        <div class="bottest action_param_div col-md-7 col-sm-12 col-xs-12 itemContents"  >'+
			'            <label>API Params</label>'+
			'            <div><textarea id="api_'+ new Date().getTime() + "_"+stepIndex +'" name="api_params" readonly class="bottest action_param api" style="width:100%;height:150px" size="3">'+ value +'</textarea></div>'+
			'        </div>' +
			'        <div class="bottest action_param_div col-md-1 col-sm-12 col-xs-12 itemContents"  >'+
			'            <label></label>'+
			'            <div><button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm bottest action_param api_edit" ><i class="icon-note mr-1"></i>'+getLang(currentLocale,'Edit')+'</button></div>'+
			'        </div>' 
			;
		return html;
		
	}

	for (var i =0; i < theAction["params"].length ; i++){
		var value = getScriptStepParamValue(theAction["params"][i]["name"],scriptStep);
		var element ="";
		if (("string" == theAction["params"][i]["param_type"]) ||
			("filepath" == theAction["params"][i]["param_type"])){
			element = '<input class="bottest action_param '+ theAction["params"][i]["name"] +' form-control" paramtype="'+theAction["params"][i]["param_type"] +'" stepIndex="'+ i+'"  name="' + theAction["params"][i]["name"]+'"  value ="'+ value +'" />';
		} else if("number" == theAction["params"][i]["param_type"]){
			if(value == undefined || value == "") {
				value = 0;
			}
			element = '<input class="bottest action_param '+ theAction["params"][i]["name"] +' form-control" paramtype="'+theAction["params"][i]["param_type"] +'" stepIndex="'+ i+'"  name="' + theAction["params"][i]["name"]+'"  value ="'+ value +'" />';
		}else {
			element = '<select class="bottest action_param '+ theAction["params"][i]["name"] +' form-control" paramtype="'+theAction["params"][i]["param_type"] +'" stepIndex="'+ i+'"  name="' + theAction["params"][i]["name"]+'" >' ;
			if (value != ""){
				element += '<option value="'+ value +'">'+value+'</option>';
			}  
			element += 	'</select>';
		}
		var addnew ="";
		if ("layout"== theAction["params"][i]["param_type"]){
			addnew = '<button type="button" onclick="addNewLayoutAtTestCase()"  style="border:none; float:right;margin-right:2px"><i class="fa fa-plus"></i></button>'
		}else if("control"== theAction["params"][i]["param_type"]){
			addnew = '<button type="button" onclick="addNewControlAtTestCase(this)"  style="border:none; float:right;margin-right:2px"><i class="fa fa-plus"></i></button>'
		}
		
		template +=
			'        <div class="bottest action_param_div col-md-2 col-sm-12 col-xs-12 itemContents"  >'+
			'            <label>'+ theAction["params"][i]["name"] +'</label>'+ addnew +
			'            <div>'+ element +'</div>'+
			'        </div>';
	
	}
//	template +=
//		'<p  class="infHelp" hidden >'+theAction["data"]["test_class"]+'</p>';
	return template;
	
}


//apply select 2 for all script
function applySelect2All(){
	var listLiItem = $('#sortable')[0].children;
	$.each(listLiItem, function (index, element){
		applySelect2Actions(element);
		applySelect2ListType(element);
		applyOnFocus4API(element);
	})

}

//apply select 2 for all script
function applySelect2AllLayout(){
	var listLiItem = $('#sortable')[0].children;
	$.each(listLiItem, function (index, element){
		applySelect2Screen(element);
		applySelect2Controls(element)
	})

}

//apply select 2 for all script
function applySelect2AllTestData(){
	var listLiItem = $('#sortable')[0].children;
	$.each(listLiItem, function (index, element){
		applySelect2Testdata(element);
		applySelect2Casenum(element);
		applyOnFocus4API(element);
	})

}

//apply select 2 for all script
function applySelect2AllTestCase(){
	var listLiItem = $('#sortable')[0].children;
	$.each(listLiItem, function (index, element){
		applySelect2Testcases(element);
	})

}

//apply select 2 for one row
function applySelect2Element(element){
		applySelect2Actions(element);
		applySelect2Screen(element);
		applySelect2Controls(element);
		applySelect2Testcases(element);
		applySelect2Testdata(element);
		applySelect2Casenum(element);
		applySelect2ListType(element);	
		applyOnFocus4API(element);
}

//apply a action in a row
function applySelect2Actions(element){
	var actions = listActions.map(function(obj){
		return {"id": obj.data.test_class, "text":obj.data.name}
	});

	$(element).find("input.bottest.ClassName").each(function(index, obj){
		  var action2 = $(this);
		  var current_value=action2.attr('current_value');
		  action2.select2({
			  width: '100%',
			  data:actions,
			  dropdownParent:$("#sortable"),
			  cushion: 50 
		  });
	});
	
	
	$(element).find("input.bottest.ClassName").off('change').change( function(e) {
		e.preventDefault();
    	var action = e.target;
    	$(action).parents("li.testscript").find("div.bottest.action_param_div").remove();
    	$(action).parents("li.testscript").find(".error.text-center").remove();
    	var className = $(action).val();
    	let helpValue = getHelpValue(className);
    	$(action).parents(".styleForm").next().html(helpValue);
    	theAction = getActionByClassName(className);
        	if (theAction !=null){
            	var html = drawActionParams(theAction,null,"");
            	$(html).insertAfter( $(action).parents("div.bottest.ClassName")) ;
            	var stepName = $(action).parents("li.testscript").find("input[name='StepName']").val();
//            	if (stepName.trim() ==""){
//            		$(action).parents("li.testscript").find("input[name='StepName']").val(theAction["data"]["description"]);
//            	}
        	}
        	if (className =="AC_API"){
        		var  apiTextarea = $(action).parents("li.testscript").find("textarea.bottest.action_param.api");
            	var value = apiTextarea.val().trim();
            	$("#modalActionAPI").attr("fromtextAreaId", apiTextarea.attr("id"));
        		bindingDataToAPIModal(null);
        	}
        	if(className == "AC_While") {
        		 var theTestcase = {
        			    "testscript": '[{"Language": {"en": {"ActionName": "end while","StepName": "end while"}},"ClassName": "AC_EndWhile","Params": []}]'
        			}
        		drawTestCaseDetail(theTestcase);
        	}
        	if(className == "AC_Loop"){
        		var theTestcase = {
        			    "testscript": '[{"Language": {"en": {"ActionName": "end loop","StepName": "end loop"}},"ClassName": "AC_EndLoop","Params": []}]'
        			}
        		drawTestCaseDetail(theTestcase);
        	}
        	if(className == "AC_If"){
        		var theTestcase = {
        			    "testscript": '[{"Language": {"en": {"ActionName": "end if","StepName": "end if"}},"ClassName": "AC_EndIf","Params": []}]'
        			}
        		drawTestCaseDetail(theTestcase);
        	}
        	applySelect2Element(element); 
    })
}

// apply select2 for screen in a row
function applySelect2Screen(element){
//	while (!glLayoutList){
//		sleep(200);
//	}
	$(element).find("select.bottest.action_param[paramType=layout]").each(function(index, obj){
		 var layouts  = glLayoutList.map(function (obj){
				return {"id": obj.layoutName, "text":obj.layoutName}
			});
			layouts.push({"id": "BOT_LAYOUT_DYNAMIC", "text": "[Current Screen]"});
			
		  var layoutSelect = $(this);
		  layoutSelect.select2({
			  width: '100%',
			  data:layouts,
			  dropdownParent:$("#sortable")
		  });
		  onChangeSelect2(layoutSelect);
		});
	
	$(element).find("select.bottest.action_param[paramType=layout]").change( function(e) {
    	var action = e.target;
    	$(element).find("select.bottest.action_param[paramType=control]").empty();
    	applySelect2Controls(element);
    })
}

// apply for all control in a row
function applySelect2Controls(element){
	// control of screen
	$(element).find("select.bottest.action_param[paramType=control]").each(function(index, obj){
		  var controlSelect = $(this);
		  var layoutName=$(element).find("select.bottest.action_param[paramType=layout]").val();
		  var theControls=[];
		  for(var j=0; j < glLayoutList.length; j++){
			  if(glLayoutList[j]["layoutName"] == layoutName){
				  theControls = (JSON.parse(glLayoutList[j].layoutContent)).Controls;
				  j=glLayoutList.length;
			  }
		  }
//		  if (layoutName == "BOT_LAYOUT_DYNAMIC"){
//			  theControls.push({"Name":"id" });
//			  theControls.push({"Name":"class"});
//			  theControls.push({"Name":"name"});
//			  theControls.push({"Name":"css"});
//		  }
		  controlSelect.select2({
			  width: '100%',
			  tags:(layoutName == "BOT_LAYOUT_DYNAMIC"),
			  data:theControls.map(function(obj){
				  return {"id": obj.Name, "text": obj.Name}
			  }),
			  dropdownParent:$("#sortable")
		  });
		  onChangeSelect2(controlSelect);

		});
}

//apply for all control in a row
function applySelect2Casenum(element){
	// control of screen
	$(element).find("select.bottest.action_param[paramType=casenum]").each(function(index, obj){
		  var controlSelect = $(this);
		  var dataname=$(element).find("select.bottest.action_param[paramType=testdata]").val();
		  loadCaseNum(dataname);
		  controlSelect.select2({
			  width: '100%',
			  data: tmpCaseNums,
			  dropdownParent:$("#sortable")
		  });
		  onChangeSelect2(controlSelect);

		});
}


function applySelect2Testcases(element){
//	while (!glTestCaseList){
//		sleep(200);
//	}
	$(element).find("select.bottest.action_param[paramType=testcase]").each(function(index, obj){
		  var theSelect = $(this);
		  theSelect.select2({
			  width: '100%',
			  data:loadTestcases(),
			  dropdownParent:$("#sortable")
		  });
		  onChangeSelect2(theSelect);
		});
}

function applySelect2Testdata(element){
//	while (!glTestdataList){
//		sleep(200);
//	}
	$(element).find("select.bottest.action_param[paramType=testdata]").each(function(index, obj){
		  var theSelect = $(this);
		  theSelect.select2({
			  width: '100%',
			  data:loadTestdatas().first,
			  dropdownParent:$("#sortable")
		  });
		  onChangeSelect2(theSelect);
		});
	
	$(element).find("select.bottest.action_param[paramType=testdatas]").each(function(index, obj){
		  var theSelect = $(this);
		  theSelect.select2({
			  width: '100%',
			  data:loadTestdatas().second,
			  dropdownParent:$("#sortable")
		  });
		  onChangeSelect2(theSelect);
		});
}

//apply select2 for screen in a row
function applyOnFocus4API(element){

	$(element).find("button.bottest.action_param.api_edit").click( function(e) {
		var  apiTextarea = $(this).parents("li.testscript").find("textarea.bottest.action_param.api");
    	var value = apiTextarea.val().trim();
    	$("#modalActionAPI").attr("fromtextAreaId", apiTextarea.attr("id"));
    	bindingDataToAPIModal(isJson(value));
    })
}

function isJson(str) {
	var value =null;
    try {
    	value = JSON.parse(str);
    } catch (e) {
        //return null;
    	//do nothing
    }
    return value;
}

function loadTestdatas(testsuiteName){
	if (!glTestdataList && !glTestdataListNotJson){
		$.ajax({
			url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/getTestData",
			type : "GET",
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					if (result.data.length ){
						glTestdataList = result.data.map(function(obj){
							if (obj.type == "layout"){
								var dataname = obj.fileName.replace(".json","");
								return {"id": dataname, "text":dataname}
							}
						});
						glTestdataList = glTestdataList.filter(function(x) {
							   return x !== undefined;
						});
						glTestdataListNotJson = result.data.map(function(obj){
							if (obj.type == "file"){
								var dataname = obj.fileName.split(".")[0];
								return {"id": dataname, "text":dataname}
							}
						})
						glTestdataListNotJson = glTestdataListNotJson.filter(function(x) {
							   return x !== undefined;
						});
					}
	 			}
				if (!glTestdataList && !glTestdataListNotJson) {
	 				glTestdataList = [];
	 				glTestdataListNotJson = [];
	 			}
				applySelect2AllTestData();
	      },
	      error : function(request,error)
	      {
	    	  glTestdataList = [];
	    	  glTestdataListNotJson = [];
	    	  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
						title: getLang(currentLocale,"Can not get list testdata!!!"),
				        text: "Please sync project!!!",
				        type: "warning", 
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
	    	 
	      }
	    });
		
	}
	return {
		first: glTestdataList,
        second: glTestdataListNotJson,
	};
	
}

function inputTestcaseToSelect(){
	listTestcases = glTestCaseList.map(function(obj){
		if (obj.projectProgressIdentity.testcase != testcaseRecord.projectProgressIdentity.testcase && obj.type == 'automation') {
			return {"id": obj.projectProgressIdentity.testcase, "text":obj.projectProgressIdentity.testcase}
		}
//		if (obj.isLayoutData){
//			return {"id": obj.projectProgressIdentity.testcase, "text":obj.projectProgressIdentity.testcase}
//		}
	});	
	return listTestcases;
}

function loadTestcases(testsuiteName){
	if (!glTestCaseList){
		
		$.ajax({
			url : jscontext+"/project/"+glProjectId+"/"+testsuiteName+"/getTestcasesInDB",
			type : "GET",
			contentType : "application/json",
			async: false,
			success : function(result) {
				if (!(result.errormessage) ) {
					if (result.data.length){
						listTestcases = result.data;
						glTestCaseList = result.data.map(function(obj){
							if (obj.projectProgressIdentity.testcase != testcaseRecord.projectProgressIdentity.testcase && obj.type == 'automation') {
								return {"id": obj.projectProgressIdentity.testcase, "text":obj.projectProgressIdentity.testcase}
							}
//							if (obj.isLayoutData){
//								return {"id": obj.projectProgressIdentity.testcase, "text":obj.projectProgressIdentity.testcase}
//							}
						});						
					}
				}
				
				if (!glTestCaseList){
					glTestCaseList =[];
				}
				applySelect2AllTestCase();
	      },
	      error : function(request,error)
	      {
	    	  glTestCaseList = [];
	    	  
	    	  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: "Can not get list testcase!!!",
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
	      }
	    });
		
	}
	return glTestCaseList;
	
}

function loadLayouts(testsuiteName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/getLayouts",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				glLayoutList = result.data;
 			}
			if (!glLayoutList){
				glLayoutList =[];
			}
			applySelect2AllLayout();
      },
      error : function(request,error)
      {
    	  glLayoutList =[];
    	  
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not get list layout!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
      }
    });
	
	return glLayoutList;
	
}

function loadResults(){
	if (!glTestResulList){
//		$.ajax({
//			url : jscontext+"/project/"+glProjectId+"/"+glTestsuiteId+"/getLayouts",
//			type : "GET",
//			contentType : "application/json",
//			async: false,
//			success : function(result) {
//				if (!(result.errormessage) ) {
//					glTestResulList = JSON.parse(result.data);
//	 			}
//	      },
//	      error : function(request,error)
//	      {
//	          alert("Can not get list layout");
//	      }
//	    });
		
	}
	return glTestResulList;
	
}

function loadCaseNum(testdataname){
	$.ajax( {
		url : jscontext+"/project/"+glProjectId+"/"+glTestsuiteId+"/getTestDataDetail/" + testdataname,
		type : "GET",
		contentType : "application/json",
		async:false,
		success: function (result) {
			if (result.data != "") {
				tmpCaseNums = JSON.parse(result.data).listCases.map(function(obj){
		        	if (obj.description != ""){
		        		return {"id": obj.casenum, "text":obj.casenum + "_" + obj.description}
		        	}else {
		        		return {"id": obj.casenum, "text":obj.casenum }
		        	}
		        	
		        })
			}
	        
	    },
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	  })
}


function applySelect2ListType(element){

	$(element).find("select.bottest.action_param").each(function(index, obj){
		  var theSelect = $(this);
		  var data =[];
		  var theParamType = theSelect.attr("paramtype");
		  if ((theParamType != null) && (theParamType != undefined) && theParamType.startsWith("list=")){
			  data = theParamType.replace("list=","").split(",");
			  theSelect.select2({
				  width: '100%',
				  data:data.map(function(obj){return {id:obj, text:obj}}),
				  dropdownParent:$("#sortable")
			  }); 
			  onChangeSelect2(theSelect);
		  }
		  var theParamTypeName = theSelect.attr("name");
		  if (theParamTypeName == "Variable Type"){
			  theSelect.on('change', function(){
				  let itemType = theSelect.val();
				  if (itemType=="Set Password"){
					  $(element).find("input.bottest.action_param.Value").attr('type',"password");  
				  }else {
					  $(element).find("input.bottest.action_param.Value").attr('type',"text");
				  }
				  
			  } );
		  }
		});
}


function getActionByClassName(className){
	for (var i =0 ; i < listActions.length; i++){
		if (listActions[i].data.test_class == className){
			return listActions[i];
		}
	}
	return null;
}


// get paramtype  from action base on and param name from testscript
// use for draw step 
function getActionParamType(className, paramName){
	var theAction = getActionByClassName(className)
	if (theAction != null){
		for (var j =0; j < theAction.params.length; j ++){
			if (theAction.params[j].name == paramName){
				return theAction.params[j].param_type;
			}
		}
	}
	
	return "";
	
}
// Get value from a step of script by paramName
function getScriptStepParamValue(paramName, scriptStep){
	if ((scriptStep == null)|| (scriptStep == undefined)){
		return "";
	}
	if ((paramName == null)|| (paramName == undefined)){
		return "";
	}
	for (var j =0; j < scriptStep["Params"].length; j ++){
		if (scriptStep["Params"][j]["Name"] == paramName){
			return scriptStep["Params"][j]["Value"];
		}
	}
	
	return "";
	
}

function onChangeSelect2(elemntSelect2){
	elemntSelect2.on('select2:select', function (e) {
		  existEdit = true;
		}); 
}
// append last element
$("#addStep").click(function(){
	var element = createStep(null,"");
	$("#sortable").append(element);
	applySelect2Element($("li.testscript").last());
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
})

// create new step  and append end of theElement be clicked
function appendStep(theElement){
	var newStep = createStep(null,"");
	$(newStep).insertAfter( $(theElement).parents("li.testscript")) ;
	applySelect2Element($(theElement).parents("li.testscript").next());
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
}

// copy step form theElement be clicked
function copyStep(theElement){
	var newStep = $(theElement).parent().parent().parent().parent().clone();
	newStep.find("select.bottest").removeClass("select2-hidden-accessible").removeAttr("data-select2-id");
	newStep.find(".select2 ").remove();
	newStep.find(".error.text-center").remove();
	$(newStep).insertAfter( $(theElement).parents("li.testscript")) ;
	applySelect2Element($(theElement).parents("li.testscript").next());
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
	
}

function deleteStep(theElement){
	$(theElement).tooltip("hide");
	$(theElement).parents("li.testscript").remove();
}


function generateStepName(testscript){
	var lsActions = listActions;
	var params = testscript["Params"];
	var currentAction = getActionByClassName( testscript["ClassName"]);
	if(currentAction != null){
		var descriptstr= currentAction.data.description;
	    for(var i = 0; i < params.length; i++) {
		  	descriptstr= descriptstr.split("{"+i+"}").join(params[i].Value);
		}
	    testscript["Language"]["en"]["StepName"] = descriptstr;
	    testscript["Language"]["en"]["ActionName"] = currentAction.data.name;
	}
	

}

function generateTestcaseProcedure(testscript, count, listTestcaseProcedure){
	count += 1;
	var lsActions = listActions;
	var params = testscript["Params"];
	var currentAction = getActionByClassName( testscript["ClassName"]);
	if(currentAction != null){
		var descriptstr= currentAction.data.description;
	    for(var i = 0; i < params.length; i++) {
		  	descriptstr= descriptstr.split("{"+i+"}").join(params[i].Value);
		}
	    descriptstr1 = count + ". " + descriptstr;		
	    if (currentAction.data.type == "in") {
	    	if (listTestcaseProcedure == ""){
	    		listTestcaseProcedure = descriptstr1;
	    	} else {
	    		listTestcaseProcedure = listTestcaseProcedure + "<br>" + descriptstr1;
	    	}
	    	
	    } 
	}
	
    return listTestcaseProcedure;
}

function generateExpectedOutpute(testscript, count, listExpectedOutput){
	count += 1;
	var lsActions = listActions;
	var params = testscript["Params"];
	var currentAction = getActionByClassName( testscript["ClassName"]);
	if(currentAction != null){
		var descriptstr= currentAction.data.description;
	    for(var i = 0; i < params.length; i++) {
		  	descriptstr= descriptstr.split("{"+i+"}").join(params[i].Value);
		}
	    descriptstr1 = count + ". " + descriptstr;		
	    if (currentAction.data.type == "out") {
	    	if(listExpectedOutput == "") {
	    		listExpectedOutput = descriptstr1;
	    	} else {
	    		listExpectedOutput = listExpectedOutput + "<br>" + descriptstr1;
	    	}
	    }
	}
	
    
    return listExpectedOutput;
}

function saveStepAuto(){
	saveStep(testsuiteName);
}

//update detail testcase
function saveStep(testsuiteName){
	$("#saveStep").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveStep").prop("disabled", true);
//  $('#error')[0].innerHTML = "";      
  var lsTestAction = [];
  var listLiItem = $('#sortable')[0].children;
  var i;
  var formValid = true;
  lsSaveTestscripts = []
  isIfAction = 0;
  isLoopAction = 0;
  var listTestcaseProcedure = "";
  var listExpectedOutput = "";
  $('#sortable').find("div.error").remove();
  for(i = 0; i < listLiItem.length; i++) {
    var params =$(listLiItem[i]).find(".bottest.action_param").serializeArray().map(function(obj){
    	return {"Name":obj.name, "Value":obj.value}
    });
    
   // var description, actionName = generateStepName(listLiItem[i], params);
    var obj = {
    	"ClassName": $(listLiItem[i]).find("input.bottest.ClassName").val(),
    	"Language":{
    		"en": {
    	          "StepName": $(listLiItem[i]).find("input[name='StepName']").val(),
    	          "ActionName":  ""
    	        }
    	},
    	"Params": params
    }
    //AC_API
    if (obj["ClassName"] == "AC_API"){
		var  apiTextarea = $(listLiItem[i]).find("textarea.bottest.action_param.api");
		obj["Params"]  = isJson( apiTextarea.val().trim())["Params"];
    }

    if (obj["ClassName"] == "AC_If"){
      isIfAction += 1;
    }else if(obj["ClassName"] == "AC_EndIf"){
      isIfAction += -1;
    }else if (obj["ClassName"] == "AC_Loop"){
      isLoopAction += 1;
    }else if (obj["ClassName"] == "AC_EndLoop"){
      isLoopAction += -1;
    }; 
    
    for (var j=0; j < params.length; j++){
    	if (params[j]["Value"] > "" ){
    	
    	}else {
    		if(params[j]["Name"] == "From parent"){
    			
    		} else {
    			$(listLiItem[i]).append ('<div class="error text-center" style="color: red;">The['+ params[j]["Name"] +']'+getLang(currentLocale,'be required')+'</div>')
        		formValid = false;
        		$("#saveStep").html("<i class='ti-check'></i>"+getLang(currentLocale,'Save'));
    			$("#saveStep").prop("disabled", false);
    		}
    		
    	}
    }
    
//    if( obj["Language"]["en"]["StepName"] == ""){
//      $(listLiItem[i]).append ('<div class="error">Step name can\'t be blank !!!</div>')
//      formValid = false;
//      
//    }
    if (obj["ClassName"] == ""){
      $(listLiItem[i]).append ('<div class="error text-center" style="color: red;">'+getLang(currentLocale,'Missing Action')+'</div>')
      formValid = false;    
      $("#saveStep").html("<i class='ti-check'></i> "+getLang(currentLocale,"Save"));
		$("#saveStep").prop("disabled", false);
    }
    generateStepName(obj);
    listTestcaseProcedure = generateTestcaseProcedure(obj, i, listTestcaseProcedure);
    listExpectedOutput = generateExpectedOutpute(obj, i, listExpectedOutput);
    $(listLiItem[i]).find("input[name='StepName']").val(obj["Language"]["en"]["StepName"]);
    lsSaveTestscripts.push(obj);
 
  }
  
//validate action if and action loop
  if(isIfAction > 0){
	  $('#sortable').append('<div class="error text-center" style="color: red;">'+getLang(currentLocale,'Missing [end if] action !!!')+'</div>');
      formValid = false;
      $("#saveStep").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
		$("#saveStep").prop("disabled", false);
  }
  if(isIfAction < 0){
	  $('#sortable').append('<div class="error text-center" style="color: red;">'+getLang(currentLocale,'Missing [if] action !!!')+'</div>');
      formValid = false;
      $("#saveStep").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
		$("#saveStep").prop("disabled", false);
  }
  if (isLoopAction > 0){
	  $('#sortable').append('<div class="error text-center" style="color: red;">'+getLang(currentLocale,'Missing [end loop] action !!!')+'</div>');
      formValid = false;
      $("#saveStep").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
		$("#saveStep").prop("disabled", false);
  }
  if (isLoopAction < 0){
	  $('#sortable').append('<div class="error text-center" style="color: red;">'+getLang(currentLocale,'Missing [loop] action !!!')+'</div>');
      formValid = false;
      $("#saveStep").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
		$("#saveStep").prop("disabled", false);
  }


  // execute code after validate  
  if(!formValid){
	  swal({
	        title: getLang(currentLocale,'There are some error!'),
	        type: "warning",
	        timer: 1000,
	        showConfirmButton: false
		});
	  return;
  }
//    var $this = $(this);
//    $(isThis).button('Saving');
	var testcase_id = $('#titleTestcase').text();
	var projectPro = {
			"projectId": projectId,
			"testsuite": testsuiteName,
			"testcase": testcase_id
	}
	var automationTestcase = {
			"projectProgressIdentity": projectPro,
		    "description": $('#testcaseDescription').val(),
		    "expectedOutput": listExpectedOutput,
		    "testcaseProcedure": listTestcaseProcedure,
		    "updateDate": new Date().toISOString(),
		    "testscript": JSON.stringify(lsSaveTestscripts),
		    "createDate": new Date().toISOString(),
		    "updateUser": projectId,
		    "assignTo": $("#filterAssignTos").val(),
		    "preCondition": "",
		    "status": "add",
		    "type": "automation",
		    "statusIssue": $("#statusIssueDetail option:selected").text(),
		    "planStartDate": $("#planStarDa").val(),
		    "planEndDate": $("#planEndDa").val(),
		    "estimatedTime": $("#estimatedTimeDetail").val(),
		    "actualTime": $("#actualTimeDetail").val(),
		    "category": $("#categoryIssueDetail option:selected").text(),
		    "milestone": $("#milestoneTCDetail option:selected").val()
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/updateTestCase",
		type : "POST",
		data : JSON.stringify(automationTestcase),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
//        	$this.button('Save');
			 swal({
			        title: getLang(currentLocale,'TestCase has been updated!!!'),
			        type: "success",
			        timer: 1000,
			        showConfirmButton: false
				});
			}
//			getAllTestcasesByStatus();
			$("#saveStep").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
			$("#saveStep").prop("disabled", false);
      },
      error : function(request,error)
      {
    	  console.log(error);
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
      }
    });
  
}   

function addNewLayoutAtTestCase(e){
	$("#modalAddNewLayout").modal("show");

}

//$('#modalAddNewLayout').on('hidden.bs.modal', function () {
//	  //glLayoutList = null;
//		var listLiItem = $('#sortable')[0].children;
//		$.each(listLiItem, function (index, element){
//			loadLayouts(testsuiteName);
//			applySelect2Screen(element);
//		})
//	  
//})

function addNewControlAtTestCase(element){
	$("#modalDetailLayout").modal("show");
	document.getElementById("bodyLayoutDetails").reset();
	$("#btnNewControlToLayout").removeClass("hidden");
	var layoutName= $(element).parents("li.testscript ").find("select.bottest.action_param[paramType=layout]").val();
	$("#modalDetailLayout").attr("layoutName", layoutName);

}

$('#modalDetailLayout').on('hidden.bs.modal', function () {
	  //glLayoutList = null;

	  
})

var validateAddLayoutDetail = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var formDetail = $('#bodyLayoutDetails');
    var error2 = $('.alert-danger', formDetail);
    var success2 = $('.alert-success', formDetail);
    var layouts = glLayoutList;
    
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    
    formDetail.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        
        rules: {
        	controlName: {
                minlength: 1,
                maxlength: 50,
                required: true,
                regex:"^[a-zA-Z0-9.\-_]*$"
            },
            type: {
                required: true,
            },
            errorAttributeName :{
            	
            }
        },
        
        messages: {
        	controlName:{
        		maxlength: getLang(currentLocale,"Please enter at less 50 characters!"),
        		required:getLang(currentLocale,"Cannot be blank!"),
        		regex: getLang(currentLocale,"Cannot special characters!"),
        	},
        	type: getLang(currentLocale,"Cannot be blank!")
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            $("#hiddenDataName").attr("data-name",$("#valControlName").val());
            var hiddenDataName = $("#hiddenDataName");
            var itemNameLayout= $('#titleLayout').text();
        	var layouts = glLayoutList;
	    	for(var i = 0; i < layouts.length; i++){
				if(layouts[i]["Name"] == itemNameLayout){
					var currentLayout = layouts[i];
					var controls = currentLayout.Controls;
					for(var j = 0; j < controls.length; j++){
						var AttributesArr = controls[j]['Attributes'];
						for(var m = 0; m < AttributesArr.length; m ++){
							var controlName = $("#valControlName").val();
							if(AttributesArr[m]['Name'] == "controlName" && AttributesArr[m]['Value'] == controlName){
									var $alertas = $('#bodyLayoutDetails');
									error2.show();
									success2.hide();
									$alertas.validate().resetForm();
									$alertas.find('.form-group').removeClass('has-success');
							        $alertas.find('.controllNameVal').addClass('has-error');
							        $alertas.find('.controlNameIcon').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Control Name is exist. Please enter another name!!!"));
							        return false;
							}
						}
					}
				}
	    	}
	    	
    	    var $alertas = $('#bodyLayoutDetails');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
        
    });
}

//func reset Form of modal_add_new_Layout
function resetFormDeitalLayout() {
	$('#modalDetailLayout').on('hidden.bs.modal', function() {
		document.getElementById("bodyLayoutDetails").reset();
	    var $alertas = $('#bodyLayoutDetails');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

//Save the controll from Modal to current layout
function saveNewControlToLayout(){
  if ($('#bodyLayoutDetails').valid() !=true){
		return;
  }
  $("#btnNewControlToLayout").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
  $("#btnNewControlToLayout").prop("disabled", true);
  var newControl = createControlFromModal();
  var layoutName = $("#modalDetailLayout").attr("layoutName");
  addControlToLayout(newControl,layoutName);
  
}

function createControlFromModal(){
	var attributes = [];
	var conditions =[];
	for (var item = 1; item < tableItemList.length ; item++){
		if ($("#bodyLayoutDetails [name="+ tableItemList[item] + "]").attr("type") == "checkbox"){
			attributes.push ({"Name": tableItemList[item], "Value": $("#bodyLayoutDetails [name="+ tableItemList[item] + "]").prop("checked").toString()});
		}else{
			attributes.push ({"Name": tableItemList[item], "Value": $("#bodyLayoutDetails [name="+ tableItemList[item] + "]").val()});
		}
	}
	
	for (var j =0; j < attributes.length; j++){
		if ((conditionList.indexOf( attributes[j]["Name"])>=0) && (attributes[j]["Value"] >"" )){
			conditions.push(attributes[j]);
			//console.log(attributes[j]);
		}
	}
	var controlName = $("#bodyLayoutDetails [name="+ tableItemList[0] + "]").val();
	
	var newControl = {
			"Name": controlName,
    	    "Condition": conditions,
    	    "Attributes": attributes	
		}
	
	
	return newControl;

}

function addControlToLayout(newControl, layoutname){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/getLayoutByName/" +layoutname ,
		type : "GET",
		contentType : "application/json",
		//async: false,
		success : function(result) {
			if (!(result.errormessage) ) {
				var thelayout = result.data;
				var controls = (JSON.parse(thelayout.layoutContent)).Controls
				var isExist =false;
				for (var j = 0; j < controls.length; j++){
					if (controls[j].Name == newControl.Name){
						controls[j] = newControl;
						j = controls.length;
						isExist = true;
					}
					if(controls[j].Name == ""){
						controls.splice(j,1);
					}
					
				}
				
				// create new controll if not exist
				if (!isExist){
					controls.push(newControl);
				}
				var layoutContent = JSON.parse(thelayout.layoutContent);
				layoutContent.Controls=controls;
				thelayout.layoutContent = JSON.stringify(layoutContent);
				saveTheLayout(thelayout);
				
 			}
      },
      error : function(request,error)
      {
          
          var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: "Can not get list layout!!!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
      }
    });
}

function saveTheLayout(thelayout){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/saveLayout",
		type : "POST",
		data : JSON.stringify(thelayout),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
//				swal({
//			        title: "Update Layout Success!!!",
//			        type: "success",
//			        timer: 500,
//			        showConfirmButton: false
//			    });
				  for(var j=0; j < glLayoutList.length; j++){
					  if(glLayoutList[j]["layoutName"] == thelayout["layoutName"]){
						  glLayoutList[j] = thelayout;
						  j=glLayoutList.length
					  }
				  }
				var listLiItem = $('#sortable')[0].children;
				$.each(listLiItem, function (index, element){
					loadLayouts(testsuiteName);
					applySelect2Controls(element);
				})
				$("#btnNewControlToLayout").html("<i class='ti-check'></i> Create");
				$("#btnNewControlToLayout").prop("disabled", false);
				$('#modalDetailLayout').modal("hide");
				  var $alertas = $('#bodyLayoutDetails');
				  $alertas.find('.form-group').removeClass('has-success');
				  $alertas.find('.fa').removeClass('fa-check');
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});		
}

function sleep (time) {
	  return new Promise((resolve) => setTimeout(resolve, time));
	}

function getListTestsuite(status, callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				
				
				if(status == "true"){
					tsByProjectId = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.testsuite;
						obTs["text"] = values.testsuite;
						tsByProjectId.push(obTs);
					});
					callbackF(tsByProjectId);
				} else {
					callbackF(result.data);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func validate add Layout
var validateAddLayout = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#new_layout');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    $.validator.addMethod("checkExist",function(value, element, regexp) {
    	var $alertas = $('#new_layout');
    	var nameLayout = $("#layout_name").val();
    	var checkBoolean = true;
    	if(JSON.stringify(glLayoutList) != "{}" ){
	    	$.each(glLayoutList, function(ind,vals){
	    		if(vals.layoutName == nameLayout){
	    			checkBoolean = false;
	    		} 
	    	});
    	}else{
    		return checkBoolean;
    	}
    	return checkBoolean;
    });
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	layoutName: {
                minlength: 2,
                maxlength:50, 
                required: {
                	depends:function(){
		            $(this).val($.trim($(this).val()));
		            return true;
                	}
                },
                regex:"^[A-Za-z0-9][A-Za-z0-9]{1,50}$",
                checkExist: true
            },
            layoutDescription: {
                required: true,
                minlength: 5,
            },
           
        },     
        messages: {
        	layoutName:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 2 characters!"),
        		regex: getLang(currentLocale,"Cannot special characters!"),
        		checkExist: getLang(currentLocale,"Layout Name is exist. Please enter another name!!!")
			},
        	layoutDescription:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        	} ,
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
        }
        
    });
}

function addNewlayout(){
	if ($('#new_layout').valid() !=true){
		return;
	}
	$("#saveNewLayout").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveNewLayout").prop("disabled", true);
	var testsuiteName = $("#lsTestsuite option:selected").text();
	var layoutName = $('#layout_name').val();
	var layoutDescription = $('#layout_description').val();
	var attrs=[];
	for (var i = 1; i < tableItemList.length; i ++){
		attrs.push({"Name": tableItemList[i] , "Value":"" })
	}
	var controls =[{
		"Name": "",
	      "Condition": [
	        {
	          "Name": "",
	          "Value": ""
	        }
	      ],
	      "Attributes": attrs
	      
	}];
	var layout = 
	{
		"Name":layoutName,
		"Description":layoutDescription,
		"Controls": controls
	}
	
	var screenDesign = {
			"projectId": projectId,
			"testsuiteName": testsuiteName,
			"layoutName": layoutName,
			"description": layoutDescription,
			"layoutContent": JSON.stringify(layout),
			"createBy": "",
			"createDate": new Date().toISOString()
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/saveLayout",
		type : "POST",
		data : JSON.stringify(screenDesign),
		contentType : "application/json; charset=UTF-8",
		success : function(result) {
			if (!(result.errormessage) ) {
				resetFormLayout();
				$("#modalAddNewLayout").modal("hide");
				$("#bodyLayoutRecord").empty();
				
				$("#saveNewLayout").html("<i class='ti-check'></i> Save");
				$("#saveNewLayout").prop("disabled", false);
				document.getElementById("new_layout").reset();
	    	    var $alertas = $('#new_layout');
	    	    $alertas.validate().resetForm();
	    	    
	    	    
	    	    var listLiItem = $('#sortable')[0].children;
	    		$.each(listLiItem, function (index, element){
	    			loadLayouts(testsuiteName);
	    			applySelect2Screen(element);
	    		})
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func reset Form of modal_add_new_Layout
function resetFormLayout() {
	$('#modalAddNewLayout').on('hidden.bs.modal', function() {
		document.getElementById("new_layout").reset();
	    var $alertas = $('#new_layout');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

function duplicateUser(arr){
	var duplicateIds = [];
	var valueDupl = arr
    .map(e => e['lastname'] + e['firstname'])
    .map((e, i, final) => final.indexOf(e) !== i && i)
    .filter(obj=> arr[obj])
    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
	 $.each(valueDupl, function(index, value){
		 $.each(arr, function(indexArr, valueArr){
			 var name = valueArr["lastname"] + valueArr["firstname"]; 
			 if(name == value){
				 duplicateIds.push(valueArr);
			 }
		 })
	 })
	 
	 return duplicateIds ;
}

function getUserAssignTo(status, callbackF){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getUsers",
		type : "GET",
		contentType : "application/json",
		async: false,
		success : function(result) {
			if (!(result.errormessage) ) {
				if(status == "true"){
					var lsUser = [];
					var duplicateUsers = duplicateUser(result.data);
					$.each(result.data, function(indexs, values){
						if(values.lastname != null && values.firstname != null){
							var obTs = {};
							var count = 0 ;
							obTs["id"] = values.username;
							if(duplicateUsers.length > 0){
								$.each(duplicateUsers, function(index,value){
									if(value.username == values.username){
										obTs["text"] =values.lastname +" "+values.firstname +"("+values.username+")" ;
										count ++;
									}					
								})
							} 
							
							if(count == 0 ){
								obTs["text"] =values.lastname +" "+values.firstname ;
							}
							lsUser.push(obTs);
						}
					});
					callbackF(lsUser);
				} else {
					lsAssignTo = result.data;
					callbackF(lsAssignTo);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

function getAllMilestoneInIssue(callbackF){
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getAllMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				callbackF(result.data);
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: " Can get data!!!",
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}


function infProjectCom(callbackF){
	$.ajax({
		type:"GET",
		url: jscontext +"/project/"+projectId+"/task/getProCo",
		success : function(result) {
			if(!(result.errormessage) ){	
				projectCo = result.data;
				
				callbackF(projectCo);
			} 
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
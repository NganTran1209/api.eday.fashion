//Use for view picture galary 
function viewfulImgLayout(){
	$(".pictures_eyes_indicators").remove();
	$(".picshade").remove();
	$(".pictures_eyes_close").remove();
	$(".pictures_eyes").remove();
	(function($){
	    $.fn.picEyes = function(){
	        var $obj = this;
	        var num,zg = $obj.length - 1;
	        var win_w = $(window).width();
	        var win_h = $(window).height();
	        var eyeHtml = '<div class="picshade"></div>'
	            +'<a class="pictures_eyes_close" href="javascript:;"><i class="fa fa-times" style="margin-top:5px;"></i></a>'
	            +'<div class="pictures_eyes">'
	            +'<div class="pictures_eyes_in">'
	            +'<img src="" />'
	            +'<div class="next"><i class="fa fa-angle-right" style="margin-right: -2px;"></i></div>'
	            +'<div class="prev"><i class="fa fa-angle-left" style="margin-left: -2px;"></i></div>'
	            +'</div>'
	            +'</div>'
	            +'<div class="pictures_eyes_indicators"></div>';
	        $('body').append(eyeHtml);
	        	$obj.click(function() {
//		            $(".picshade").css("height", win_h);
		        	$('.picshade').css('background-color','rgba(0, 0, 0, 0.72)');
		            var n = $(this).find("img").attr('src');
		            $(".pictures_eyes img").attr("src", n);
		            num = $obj.index(this);
		            popwin($('.pictures_eyes'));
		            
		        });
	        $(".pictures_eyes_close,.picshade,.pictures_eyes").click(function() {
	            $(".picshade,.pictures_eyes,.pictures_eyes_close,.pictures_eyes_indicators").fadeOut();
	            $('body').css({'overflow':'auto'});
	        });
	        $('.pictures_eyes img').click(function(e){
	            stopPropagation(e);
	        });
	        $(".next").click(function(e){
	            if(num < zg){
	                num++;
	            }else{
	                num = 0;
	            }
	            var xx = $obj.eq(num).find('img').attr("src");
	            $(".pictures_eyes img").attr("src", xx);
	            stopPropagation(e);
	            popwin($('.pictures_eyes'));
	        });
	        $(".prev").click(function(e){
	            if(num > 0){
	                num--;
	            }else{
	                num = zg;
	            }
	            var xx = $obj.eq(num).find('img').attr("src");
	            $(".pictures_eyes img").attr("src", xx);
	            stopPropagation(e);
	            popwin($('.pictures_eyes'));
	        });
	        function popwin(obj){
	            $('body').css({'overflow':'hidden'});
	            var Pwidth = obj.width();
	            var Pheight = obj.height();
	            obj.show();
	            $('.picshade,.pictures_eyes_close').fadeIn();
	            indicatorsList();
	        }
	        function updatePlace(obj){
	            var Pwidth = obj.width();
	            var Pheight = obj.height();
	            obj.css({left:(win_w - Pwidth)/2,top:(win_h - Pheight)/2});
	        }
	        function indicatorsList(){
	            var indHtml = '';
	            $obj.each(function(){
	                var img = $(this).find('img').attr('src');
	                indHtml +='<a href="javascript:;"><img src="'+img+'"/></a>';
	            });
	            $('.pictures_eyes_indicators').html(indHtml).fadeIn();
	            $('.pictures_eyes_indicators a').eq(num).addClass('current').siblings().removeClass('current');
	            $('.pictures_eyes_indicators a').click(function(){
	                $(this).addClass('current').siblings().removeClass('current');
	                var xx = $(this).find('img').attr("src");
	                $(".pictures_eyes img").attr("src", xx);
	                updatePlace($('.pictures_eyes'));
	            });
	        }
	        function stopPropagation(e) {
	            e = e || window.event;  
	            if(e.stopPropagation) {
	                e.stopPropagation();  
	            } else {  
	                e.cancelBubble = true;
	            }  
	        }
	    }
	})(jQuery);
		$('#imageLayoutDetail li .containers').picEyes();
}

function getOject(id) {

    (function ($) {
        $.fn.picEyes = function () {
            var $obj = this;
            var num, zg = $obj.length - 1;
            var win_w = $(window).width();
            var win_h = $(window).height();

            var eyeHtml = '<div class="picshade_ampm"></div>'
                + '<a class="pictures_eyes_close_result" href="javascript:;"><i class="fa fa-times w-100" style="margin-top:5px;"></i></a>'
                + '<div class="pictures_eyes_result">'
                + '<div class="pictures_eyes_result_in">'
                + '<img src="" />'
                + '<div class="next_result"><i class="fa fa-angle-right" style="margin-right: -2px;"></i></div>'
                + '<div class="prev_result"><i class="fa fa-angle-left" style="margin-left: -2px;"></i></div>'
                + '</div>'
                + '</div>'
                + '<div class="pictures_eyes_indicators_result"></div>';

            $('body').append(eyeHtml);
            $('.picshade_ampm').css('background-color','rgba(0, 0, 0, 0.72)');
            //$(".picshade_ampm").css("height", win_h);
            var n = $(this).find('img').attr('src');
            $(".pictures_eyes_result img").attr("src", n);
            num = $obj.index(this);
            popwin($('.pictures_eyes_result'));
            $('.pictures_eyes_close_result').css('display','flex');
            $(".pictures_eyes_close_result,.picshade_ampm,.pictures_eyes_result").click(function () {
                $(".picshade_ampm,.pictures_eyes_result,.pictures_eyes_close_result,.pictures_eyes_indicators_result").fadeOut().remove();
                $('body').css({ 'overflow': 'auto' });
            });
            $('.pictures_eyes_result img').click(function (e) {
                stopPropagation(e);
            });
            $(".next_result").click(function (e) {
                if (num < zg) {
                    num++;
                } else {
                    num = 0;
                }
                var xx = $obj.eq(num).find('img').attr("src");
                $(".pictures_eyes_result img").attr("src", xx);
                stopPropagation(e);
                popwin($('.pictures_eyes_result'));
            });
            $(".prev_result").click(function (e) {
                if (num > 0) {
                    num--;
                } else {
                    num = zg;
                }
                var xx = $obj.eq(num).find('img').attr("src");
                $(".pictures_eyes_result img").attr("src", xx);
                stopPropagation(e);
                popwin($('.pictures_eyes_result'));
            });
            function popwin(obj) {
                $('body').css({ 'overflow': 'hidden' });
                var Pwidth = obj.width();
                var Pheight = obj.height();
                obj.show();
                $('.picshade_ampm,.pictures_eyes_result_close_result_result').fadeIn();
                indicatorsListResult();
                
            }
            function updatePlace(obj) {
                var Pwidth = obj.width();
                var Pheight = obj.height();
                obj.css({ left: (win_w - Pwidth) / 2, top: (win_h - Pheight) / 2 });
            }
            function indicatorsListResult() {
                var indHtmlResult = '';
                $obj.each(function () {
                    var img = $(this).find('img').attr('src');
                    indHtmlResult += '<a href="javascript:;"><img src="' + img + '"/></a>';
                });
                $('.pictures_eyes_indicators_result').html(indHtmlResult).fadeIn();
                $('.pictures_eyes_indicators_result a').eq(num).addClass('current_result').siblings().removeClass('current_result');
                $('.pictures_eyes_indicators_result a').click(function () {
                    $(this).addClass('current_result').siblings().removeClass('current_result');
                    var xx = $(this).find('img').attr("src");
                    $(".pictures_eyes_result img").attr("src", xx);
                    updatePlace($('.pictures_eyes_result'));
                });
                
            }
            function stopPropagation(e) {
                e = e || window.event;
                if (e.stopPropagation) {
                    e.stopPropagation();
                } else {
                    e.cancelBubble = true;
                }
                
            }
        }
    })(jQuery);
    $('#' + id + ' li').picEyes();
}


$(document).ready(function(){
	
	$(document).on('change', '#fileUploadTestData', function() {
		var files = $('#fileUploadTestData')[0].files;
		var nameFile = $(this).parents('.upload-btn-wrapper').find('.nameFileUpload');
		var html =""
		for (var i =0; i < files.length; i++){
			html +=  '<i class="icon-doc mr-1"></i>'+files[i].name +'<br>';
		}
		nameFile.html(html);
	});
//	loadListScreenLayout();
	
	$('#uploadTestData').on('hidden.bs.modal', function () {
	  $('#descriptionTestData').val('');
	  $('#fileUploadTestData').val('');
	});
	
	$('#modalCopyData').on('hidden.bs.modal', function() {
		$("#messageErrors").text("");
	});
	
	drawParentTaskInTestcase(taskList);

});

function showModalCreateTD(){
	$('#chooseScreenTestData').modal({
		backdrop: 'static'
	});
} 

function showModalUploadTD(){
	$('#uploadTestData').modal({
		backdrop: 'static'
	});
}


//func reset Form test data
function resetFormTestData() {
	$("#messageFileError").text("");
	$(".nameFileUpload").text("");
	$('#uploadTestData').on('hidden.bs.modal', function() {
		document.getElementById("formUploadTestData").reset();
	    var $alertas = $('#formUploadTestData');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

function showFile(){
	$(".nameFileUpload").show();
}

//$("#modalCopyData").on("show.bs.modal", function (){
//	var count = 0;
//	$.each( testsuitels, function(index, valuesItemTestsuite) {
//		if(count == 0){
//			$.ajax({
//				url : jscontext + "/project/" +projectId +"/" + valuesItemTestsuite["testsuite"] + "/getLayouts",
//				type : "GET",
//				data : "",
//				contentType : "application/json",
//				success : function(result) {
//						var testData = JSON.parse(result.data);
//						var htmlLayout = "";
//						if(testData.length == 0){
//							htmlLayout += '<option value="Layout empty!">Layout Empty!</option>'
//						} else {
//							for(var j = 0; j < testData.length; j++) {
//								htmlLayout += '<option value="'+testData[j].Name+'">'+testData[j].Name+'</option>'
//							}
//						}
//						
//						$("#listLayoutss").append(htmlLayout);
//						funcCopyData();
//					},
//					error : function(request,error)
//				    {
//						var statusCode = request.status;
//						if (statusCode == 403) {
//							window.location= jscontext + "/login";
//						}
//				    }
//				});
//		}
//		count += 1;
//	});
//	funcCopyData();
//});
//function funcCopyData() {
//	$("#testsuiteListss").empty();
//	var nameTestData = $("#nameTestData").val().split(".")[0];
//	$("#nameTestDataOld").val(nameTestData);
//
//}



//function downloadData(filename, testsuiteName){
//	$('[role=tooltip]').tooltip('hide');
//	$.ajax({
//		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/downloadTestdata/"+filename,
//		type : "POST",
//		contentType : "application/json",
//		data: filename,
//		success : function(result) {
//			if (!(result.errormessage) ) {
//				var exportedFilenmae = filename;
//			    var blob = new Blob([result.data], { type: 'text/csv;charset=utf-8;' });
//			    if (navigator.msSaveBlob) { // IE 10+
//			        navigator.msSaveBlob(blob, exportedFilenmae);
//			    } else {
//			        var link = document.createElement("a");
//			        if (link.download !== undefined) { // feature detection
//			            // Browsers that support HTML5 download attribute
//			            var url = URL.createObjectURL(blob);
//			            link.setAttribute("href", url);
//			            link.setAttribute("download", exportedFilenmae);
//			            link.style.visibility = 'hidden';
//			            document.body.appendChild(link);
//			            link.click();
//			            document.body.removeChild(link);
//			        }
//			    }
// 			}
//			loadListTestData();
//      },
//      error : function(request,error)
//      {
//    	  var statusCode = request.status;
//			if (statusCode == 403) {
//				window.location= jscontext + "/login";
//			}
//      }
//    });
//	$('[role=tooltip]').tooltip('hide');
//}




//---------------------------------------------Test Data--------------------------------------------------------
//testdata again
function drawListTestData(testData){
	$("#bodyTestDataRecord").empty();
	if(testData.length > 0 ){
		
		$("#numbTestData").html(testData.length);
		$.each( testData, function(index, valuesTestData) { 
			var idDelete = "del_"+valuesTestData["id"]+"_TestData";
			var createDate = valuesTestData["createDate"];
			if(createDate == undefined){
				createDate = "";
			} else {
				createDate = new Date(createDate).toISOString();
				createDate = createDate.split("T")[0]+" "+(createDate.split("T")[1]).split(".")[0];
			}
			
			var htmlRecordTestsuites =	'<tr class="odd pointer tr-bottest">'
										+'<td class="td-bottest"><i class="fa fa-caret-right fa-1x"></i>';
										if(valuesTestData['type'] == "layout"){
											htmlRecordTestsuites += '<a href="'+jscontext+ "/project/"+projectId+"/"+valuesTestData["testsuiteName"]+"/testDataDetail/"+valuesTestData["fileName"].split(".")[0]+'"> '+ valuesTestData["fileName"]+'</a>';
										} else {
											htmlRecordTestsuites += '<a> '+ valuesTestData["fileName"]+'</a>';
										}
										htmlRecordTestsuites += '</td>'
										+'<td class="td-bottest">' + createDate
										+'</td>'
										+'<td class="td-bottest">' + valuesTestData["description"]
										+'</td>'
										+'<td class="td-bottest">'
										+ (valuesTestData['type'] != "file" ? '<i class="icon-check text-success"></i>': '')
										+'</td>'
										+'<td>'
										+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success btn-copy-testSuites" href="'+jscontext+"/project/"+projectId+"/"+valuesTestData["testsuiteName"]+"/downloadTestdata/"+valuesTestData["fileName"]+'" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Download")+'"><i class="icon-cloud-download"></i></a>'
										+ (valuesTestData['type'] == "layout" ? '<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-info btn-copy-testSuites" href="'+ jscontext+ "/project/"+projectId+"/"+valuesTestData["testsuiteName"]+"/testDataDetail/"+valuesTestData["fileName"].split(".")[0] +'" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Edit")+'"><i class="icon-pencil"></i></a>' : "")
										+ (valuesTestData['type'] == "layout" ? '<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites" data-number="'+valuesTestData["id"]+'" data-name="'+valuesTestData["fileName"]+'" testsuite-name="'+valuesTestData["testsuiteName"]+'" onclick="functionCopyTestData(this);" title="'+getLang(currentLocale,"Copy")+'"><i class="icon-docs"></i></a>' : "")
										+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-copy-testSuites" href="#" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Delete")+'" onclick="deleteData(\''+ valuesTestData["fileName"] +'\', \''+ valuesTestData["id"] +'\', \''+ valuesTestData["testsuiteName"] +'\')" id="'+idDelete+'"><i class="fa fa-trash-o"></i></a>'
										+'<input hidden type="text" id="nameTestData" value="'+valuesTestData["fileName"]+'">'
										+'</td>'
										+'<tr>';
			$("#bodyTestDataRecord").append(htmlRecordTestsuites);
		});	 
		$(function () {
			$('[data-toggle="tooltip"]').tooltip();
		});
	}else{
		var htmlRecordTestsuites =	'<tr><td colspan="5" class="text-center text-danger">'+getLang(currentLocale,"The Test Data is empty!!!")+'</td></tr>'
		$("#bodyTestDataRecord").append(htmlRecordTestsuites);
	}
}

function getListTestsuiteData(callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				
				callbackF(result.data);
				
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function loadListScreenLayout(testsuiteName, idSelect, status){
	$(idSelect).select2({
		dropdownParent: "",
		placeholder: getLang(currentLocale,"Select a screen design"),
		width: '100%',
	});
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/getLayouts",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var layouts = result.data;
				drawSelectBox(layouts, idSelect, status);
			}
      },
      error : function(request,error)
      {
          
          var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Can not save!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
      }
	})
}

function drawSelectBox(layouts, idSelect, status){
	
	if(status == true){
		$("#listScreenLayout").empty();
		$("#listLayouts").empty();
	} else {
		$(idSelect).empty();
	}
	$.each( layouts, function(indexRecordLayout, valuesRecordLayout) { 
		var htmlOptionChooseScreen = '<option value="'+valuesRecordLayout.layoutName+'">'+valuesRecordLayout.layoutName+'</option>';
		if(status == true){
			$("#listScreenLayout").append(htmlOptionChooseScreen);
			$("#listLayouts").append(htmlOptionChooseScreen);
		} else {
			$(idSelect).append(htmlOptionChooseScreen);
		}
		
	}); 
}

function saveChooseScreenLayout(){
	var testsuiteSl = $("#listTestsuite").val();
	var layoutSl = $("#listScreenLayout").val();
	window.location.href =  jscontext+"/project/"+projectId+"/"+testsuiteSl+"/testDataDetail/" + layoutSl;
}

//func validation add TestSuite
var validateUploadFieTestData = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation
    var form2 = $('#formUploadTestData');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    
    $.validator.addMethod(
            "typeFile",
            function(value, element, regexp) {
                var re = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + regexp.join('|') + ")$");
                var fileUpload = document.getElementById("fileUploadTestData");
                if (!re.test(fileUpload.value.toLowerCase())) {
                	return false
                }
                return this.optional(element) || re.test(value);
            }
    );
    
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	desFormUploadTestData: {
                minlength: 1,
                required: true,
                
            },
            filesUploadTestData: {
                required: true,
                typeFile:[".csv", ".xlsx", ".xls", ".png", ".PNG", ".jpg"]
            },
        },
        
        messages: {
        	filesUploadTestData:{
        		required:getLang(currentLocale,"Cannot be blank!"),
        		typeFile: getLang(currentLocale,"Incorrect format type!")
        	},
        	desFormUploadTestData: getLang(currentLocale,"Cannot be blank!"),
        	
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            uploadTestDatas();
    	    var $alertas = $('#formUploadTestData');
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
    });
    
    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}

function uploadTestDatas(){
	$("#btnUploadFileTestData").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#btnUploadFileTestData").prop("disabled", true);
	var description = $("#descriptionTestData").val();
	var testsuiteName = $("#listTestsuiteUp").val();
	$.ajax({
		type : "POST",
		url :  jscontext+"/project/"+projectId+"/"+testsuiteName + "/uploadTestData",
		enctype : 'multipart/form-data',
		processData : false,
		contentType : false,
		data : new FormData($("#formUploadTestData")[0]),
		timeout : 600000,
		success : function(result) {
			if (!(result.errormessage) ) {
				if(result.data != null) {
					$("#messageFileError").text(result.data);
					$("#btnUploadFileTestData").prop("disabled", false);
					$("#btnUploadFileTestData").html("<i class='ti-check'></i>"+getLang(currentLocale, "Save"));
				} else {
					$("#uploadTestData").modal('hide');
					$('.nameFileUpload').html("");
					$('#fileUploadTestData').val("");
					
					var testsuiteName = $("#filterTestsuite").val();
					var createDate = $("#filterCreateDate").val();
					var type = $("#filterTypeFile").val();
					var fileName = $("#filterFileName").val();
					drawTestcaseByFilter(testsuiteName, createDate, type, fileName);
					//call for modal action api
//					getDataFromTestData();
					$("#btnUploadFileTestData").html("<i class='ti-check'></i>"+getLang(currentLocale, "Save"));
					$("#btnUploadFileTestData").prop("disabled", false);
				}
			}
		},
		error : function(request, e) {
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		}
	});
	
}

function loadListTestData(){
	$("#loaderTestData").show();
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getTestData",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				drawListTestData(result.data);
				$("#loaderTestData").fadeOut("slow");
				$('[role=tooltip]').tooltip('hide');
 			}
      },
      error : function(request,error)
      {
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Can not get list testdata!!!"),
			        text: "Please sync project!!!",
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}	
      }
    });
	
}

function deleteData(filename, idTestData, testsuiteName){
	$("#del_"+idTestData+"_TestData").html("<i class='fa fa-circle-o-notch fa-spin'></i>");
	$("#del_"+idTestData+"_TestData").prop("disabled", true);
	$('[role=tooltip]').tooltip('hide');
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/deleteTestData/" + filename + "/" + idTestData,
		type : "POST",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var testsuiteName = $("#filterTestsuite").val();
				var createDate = $("#filterCreateDate").val();
				var type = $("#filterTypeFile").val();
				var fileName = $("#filterFileName").val();
				drawTestcaseByFilter(testsuiteName, createDate, type, fileName);
//				drawTestsuiteReport();
				$("#del_"+idTestData+"_TestData").html("<i class='fa fa-trash-o'></i>");
				$("#del_"+idTestData+"_TestData").prop("disabled", false);
 			}
      },
      error : function(request,error)
      {
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
      }
    });
	
}

function functionCopyTestData(isThis){
	$('#modalCopyData').modal({
		backdrop: 'static'
	});
	 var fileName = $(isThis).attr("data-name");
	 var testsuiteName = $(isThis).attr("testsuite-name");
	 var idTestData = $(isThis).attr("data-number");
	 
	 $("#nameTestDataOld").val(fileName.split(".")[0]);
	 $("#testsuiteId").val(testsuiteName);
	 $("#idTestData").val(idTestData);
}


function copyTestData(){
	var testsuiteId = $("#testsuiteId").val();
	var testsuiteChoose = $("#testsuiteListss option:selected").text();
	
	var nameTestDataOld = $("#nameTestDataOld").val();
	var nameTestDataNew = $("#listLayouts option:selected").text();
	
	var idTestData = $("#idTestData").val();
	
	if(testsuiteChoose == testsuiteId && nameTestDataOld == nameTestDataNew) {
		$("#messageErrors").text("Please change name data!");
	} else {
		$("#messageErrors").text("");
		
		$("#saveTestDataCopy").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
		$("#saveTestDataCopy").prop("disabled", true);
		$.ajax({
			url : jscontext+"/project/"+projectId+"/"+testsuiteChoose+"/copyTestData/"+idTestData+"/"+ testsuiteId,
			type : "POST",
			data : "",
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					$("#saveTestDataCopy").html("<i class='ti-check'></i>"+ getLang(currentLocale,"Save"));
					$("#saveTestDataCopy").prop("disabled", false);
					$("#modalCopyData").modal("hide");
					swal({
				        title: getLang(currentLocale,"Copy TestData Success!!!"),
				        type: "success",
				        timer: 1000,
				        showConfirmButton: false
				    });
					var testsuiteName = $("#filterTestsuite").val();
					var createDate = $("#filterCreateDate").val();
					var type = $("#filterTypeFile").val();
					var fileName = $("#filterFileName").val();
					drawTestcaseByFilter(testsuiteName, createDate, type, fileName);
//					drawTestsuiteReport();
				}
			},
			error : function(request, e) {
				console.log("ERROR: ", e);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});	
		
	}
	
}

function drawTestcaseByFilter(testsuiteName, createDate, fileData, fileName){
	testsuiteNameParse = testsuiteName; 
	createDateSS = createDate; 
	typeParse = fileData; 
	fileNameSS = fileName;
	var filterTestData = {
			"testsuiteName": testsuiteName,
			"createDate": createDate,
			"typeData": fileData,
			"fileName": fileName
	}
	$.ajax({
		url : jscontext +"/project/" + projectId +"/filterTestData",
		type : "post",
		data : JSON.stringify(filterTestData),
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var tcFilter = result.data;
				drawListTestData(tcFilter);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function filterTestdata(){
	var testsuiteName = $("#filterTestsuite").val();
	var createDate = $("#filterCreateDate").val();
	var fileData = $("#filterTypeFile").val();
	var fileName = $("#filterFileName").val();
	$("#filTestData").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filTestData").prop("disabled", true);
	
	drawTestcaseByFilter(testsuiteName, createDate, fileData, fileName);
	
	$("#filTestData").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filTestData").prop("disabled", false);
	
	
}

$("#filterTestsuite").on('select2:opening', function(){
	checkSession = false;
	$("#filterTestsuite").one('change', function(){
		var testsuiteName = $("#filterTestsuite").val();
		var createDate = $("#filterCreateDate").val();
		var fileData = $("#filterTypeFile").val();
		var fileName = $("#filterFileName").val();
		
		if(checkSession != true){
			drawTestcaseByFilter(testsuiteName, createDate, fileData, fileName);
		}
		
	});
});

$("#filterCreateDate").on('change', function(){
	checkSession = false;
	var testsuiteName = $("#filterTestsuite").val();
	var createDate = $("#filterCreateDate").val();
	var fileData = $("#filterTypeFile").val();
	var fileName = $("#filterFileName").val();
	
	if(checkSession != true){
		drawTestcaseByFilter(testsuiteName, createDate, fileData, fileName);
	}
	
});

$("#filterTypeFile").on('select2:opening', function(){
	checkSession = false;
	$("#filterTypeFile").one('change', function(){
		var testsuiteName = $("#filterTestsuite").val();
		var createDate = $("#filterCreateDate").val();
		var fileData = $("#filterTypeFile").val();
		var fileName = $("#filterFileName").val();
		if(checkSession != true){
			drawTestcaseByFilter(testsuiteName, createDate, fileData, fileName);
		}
		
	});
});

$("#filterFileName").on('change',function(){
	checkSession = false;
	var testsuiteName = $("#filterTestsuite").val();
	var createDate = $("#filterCreateDate").val();
	var fileData = $("#filterTypeFile").val();
	var fileName = $("#filterFileName").val();
	
	if(checkSession != true){
		drawTestcaseByFilter(testsuiteName, createDate, fileData, fileName);
	}
});

function backTestData(){
	$("#newTask").hide();
	$("#drawTestData").show();
}
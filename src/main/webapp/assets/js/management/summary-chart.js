//$("#calSchedulerTabs").on('click', function () {
//	creatingProgress();
//	creatingProgress();
//	testProgress();
//});
$(document).ready(function() {
//	$("body").removeClass("sidemenu-closed");
	graphicIssue();
	creatingProgress();
	testProgress();
	drawParentTaskInTestcase(taskList);
}); 

function randomColor()
{
     color='rgb('+Math.round(Math.random()*255)+','+Math.round(Math.random()*255)+','+Math.round(Math.random()*255)+')';

     return color;
}

function graphicIssue(){
	var listIssue = "";
	try{
		listIssue = JSON.parse(listIssueGraphics);
		listStatus = JSON.parse(listStatus);
	}catch(e){
		
	}
	if(listIssue != ""){
		pieByIssue(listIssue, listStatus);
	}
	
}

function creatingProgress(){
	// get total testcase
	var ls = JSON.parse(listGraphics);
//	var listManualAutomation = JSON.parse(listManAut);
	if(ls.length <= 0){
		$("#creatingProgressCircleEmpty").text(getLang(currentLocale,"The project is empty!!!"));
		return;
	}
	var htmlAppend = '<div class="chart-timeline col-md-9">'
     	+'<div id="creatingtProgress" class="chart-window"></div>'
     +'</div>'
     +'<div class="col-md-3 element-chart chart-legend-middle ml-auto my-auto">'
     	 +'<div class="box-chart my-auto mx-auto">'
			+'<canvas id="creatingProgressMilestoneCircle"  width="300"></canvas>'
		 +'</div>'
	+'</div>';
$("#progressCreateAdd").html(htmlAppend);
	var listTestsuite={};
	// get list testsuite
	for(var i=0;i<ls.length;i++){
		listTestsuite[ls[i].projectSummaryIdentity.testsuite]=0;
	}
	
	// group in date
	var listJson = [];
	var objDateMin = ls[0].projectSummaryIdentity.summary_date;
	var date = new Date(objDateMin);
	var dateCreateMin = date.getFullYear() + "-" + (date.getMonth() + 1) + '-'+ date.getDate();
	//var objObject = {...listTestsuite};
	var objObject = {listTestsuite};
	    objObject["period"] =  dateCreateMin;
	    // objObject.push(...listTestsuite);
	    listJson.push( objObject );
	for(var i=0;i< ls.length;i++){
	  var date = new Date(ls[i].projectSummaryIdentity.summary_date);
	  var dateCreateMin = date.getFullYear() + "-" + (date.getMonth() + 1) + '-'+ date.getDate();

	  if(dateCreateMin == objObject["period"]){
			objObject[""+ls[i].projectSummaryIdentity.testsuite+""] = ls[i].total;
	  } else {
		  
		  //var newObject = {...objObject};
		  var newObject = {objObject};
		  objObject = newObject;
		  objObject["period"] = dateCreateMin;
		  objObject[""+ls[i].projectSummaryIdentity.testsuite+""] = ls[i].total;
		  listJson.push( objObject );


	  }

	}
		
		// random color
		var colors = [];
		var keys = Object.keys(listTestsuite);
		for(var k = 0; k < keys.length;k++){
			var color = randomColor();
			colors.push(color);
		}
// creatingProgressCircle(colors, keys,listJson);
//		creatingMilestoneCircle(listManualAutomation, projectId);
		$("#creatingtProgress").empty();
		Morris.Area({
	        element: 'creatingtProgress',
	        data: listJson,
			xkey: 'period',
			ykeys: keys,
			xLabels: 'day',
			labels: keys,
			lineColors: colors,
			parseTime: false,
			xLabelMargin: 10,
// xLabelAngle: 1,
			gridTextSize:10,
			behaveLikeLine: false,
			resize: true,
	        redraw: true,
	        smooth: false,
	        hideHover: false,
	        lineWidth: 0,
	        pointSize: 0,
	        option:{
	        	 responsive: true
	        }
	    });
		
// setFontSiveLabelChart();
}

function setFontSiveLabelChart(){
	for(var i = 0 ; i < $(".chart-window svg text").length ;i++){
		var xLabelChart =  $(".chart-window svg text")[i] ;
		$(xLabelChart).css("font-size", "14px");
	}
}

function testProgress(){
	// get testcase pass fail
	var ls = JSON.parse(listGraphics);
	
	if(ls.length > 0){
		var htmlAppend = '<div class="chart-timeline col-md-9">'
			+'<div id="testProgress" class="chart-window"></div>'
  			+'</div>'
  			+'<div class=" col-md-3 element-chart chart-legend-middle ml-auto  my-auto">'
     		+'<div class="box-chart my-auto mx-auto">'
				 +'<canvas id="testProgressCircle" width="300"></canvas>'
			 	+'</div>'
			+'</div>';
  		$("#progressTestAdd").html(htmlAppend);
  		
  		var listTestsuite={};
  		for(var i=0;i<ls.length;i++){
  				listTestsuite["Pass"]=0;
  		listTestsuite["Fail"]=0;
  		listTestsuite["Degrade"]=0;
  		listTestsuite["NA"]=0;
  			}
  			
  			// group in date
  			var listJson = [];
  			var objDateMin = ls[0].projectSummaryIdentity.summary_date;
  			var date = new Date(objDateMin);
  			var dateCreateMin = date.getFullYear() + "-" + (date.getMonth() + 1) + '-'+ date.getDate();
  			//var objObject = {...listTestsuite};
  			var objObject = {listTestsuite};
  			    objObject["period"] =  dateCreateMin;
  			    // objObject.push(...listTestsuite);
  			    listJson.push( objObject );
  			for(var i=0;i< ls.length;i++){
  			  var date = new Date(ls[i].projectSummaryIdentity.summary_date);
  			  var dateCreateMin = date.getFullYear() + "-" + (date.getMonth() + 1) + '-'+ date.getDate();

  			  if(dateCreateMin == objObject["period"]){
  					objObject["Pass"] += ls[i].pass;
  		            objObject["Fail"] += ls[i].fail;
  					objObject["Degrade"] += ls[i].degrade;
  					objObject["NA"] += ls[i].notRun;
  			  } else {
  				  
  				  //var newObject = {...objObject};
  				  var newObject = {objObject};
  				  objObject = newObject;
  				  objObject["period"] = dateCreateMin;
  				  objObject["Pass"] = ls[i].pass;
  		          objObject["Fail"] = ls[i].fail;
  			      objObject["Degrade"] = ls[i].degrade;
  				  objObject["NA"] = ls[i].notRun;
  				  listJson.push( objObject );


  			  }

  			}
		
		testProgressCircle(listJson);
		
		$("#testProgress").empty();
		Morris.Area({
	        element: 'testProgress',
	        data: listJson,
			xkey: 'period',
			ykeys: ['Pass','Fail', 'Degrade', 'NA'],
			labels: ['Pass','Fail', 'Degrade', 'NA'],
			lineColors: ["#1dcb8b","#CB371D", "#AB47BC","#f5dc01"],
			resize: true,
	        redraw: true,
	        parseTime: false,
			gridTextSize: 10,
			behaveLikeLine: false,
	        smooth: false,
	        hideHover: false,
	        lineWidth: 0,
	        pointSize: 0,
	        option:{
	        	 responsive: true
	        }
	    });
		
// setFontSiveLabelChart();
	} else {
		$("#testProgressCircleEmpty").text(getLang(currentLocale,"The project is empty!!!"));
	}
}

function creatingProgressCircle(colors, listTestsuites, listJson){
	var totalTestsuite = 0;
	for(var i = 0; i < listTestsuites.length; i++){
		totalTestsuite += listJson[listJson.length-1][""+listTestsuites[i]+""];
	}
	var listPercent = [];
	for(var k = 0; k < listTestsuites.length; k++){
		var percent = listJson[listJson.length-1][""+listTestsuites[k]+""];
		listPercent.push(percent);
	}
	
	 var configCreatProCircle = {
      type: 'pie',
	    data: {
	        datasets: [{
	        	data:listPercent,
	        	backgroundColor:colors,
	            label: 'Creating Progress'
	        }],
	        labels: listTestsuites
	    },
	    options: {
	    	elements: {
    		    arc: {
    		      borderWidth: 0, // <-- Set this to derired value
    		    }
    		},
			responsive: true,
			scaleBeginAtZero: false,
			legend: {
	            display: false,
	            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
			},
			events: true,
			animation: {
				duration: 500,
				easing: "easeOutQuart",
				onComplete: function () {
				  var ctx = this.chart.ctx;
				  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
				  ctx.textAlign = 'center';
				  ctx.textBaseline = 'center';
				
				  this.data.datasets.forEach(function (dataset) {
				
					for (var i = 0; i < dataset.data.length; i++) {
					  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
					      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
					      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
					      start_angle = model.startAngle,
					      end_angle = model.endAngle,
					      mid_angle = start_angle + (end_angle - start_angle)/2;
					
					  var x = mid_radius * Math.cos(mid_angle);
					  var y = mid_radius * Math.sin(mid_angle);
					
					  ctx.fillStyle = '#fff';
					  if (i == 3){ // Darker text color for lighter background
					    ctx.fillStyle = '#fff';
					  }
					  ctx.font = "14px sans-serif";
					  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
					  if(dataset.data[i] > 0 ){
					  	  ctx.fillText(dataset.data[i], model.x + x, model.y + y);
						  // Display percent in another line, line break
							// doesn't work for fillText
						  ctx.fillText(percent, model.x + x + 3, model.y + y + 20);
					  }
					}
				  });               
				}
			},
			tooltips: {
				enabled: true,
		        callbacks: {
		            label: function(tooltipItem, data) {
		                return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
		                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
		                });
		            }
		        }
		    }
		}
	};

  var creatingProgressCircle = document.getElementById("creatingProgressCircle").getContext("2d");
  var chartCreatingProgressCircle = new Chart(creatingProgressCircle, configCreatProCircle);
  $("#legendCreateProgress").html(chartCreatingProgressCircle.generateLegend());
  
}

function pieByIssue(listIssue, listStatus){
	var listIs = [];
	var listColor = [];
    for(var k = 0; k < listIssue.length; k++){
    	var checkAdd = false;
        var listSt = [];
        var itemSt = {};
        var vals = listIssue[k];
        itemSt[vals.status] = vals.count;
        listSt.push(itemSt);
        var objIs = {};

        for(var i = 0; i < listIs.length; i++){
    
            if(vals.tracker in listIs[i]){
                checkAdd = true;
                var itemSt = {};
                itemSt[vals.status] = vals.count;
                var listItem = listIs[i][vals.tracker];
                listItem.push(itemSt);
            } 
        }
		if(checkAdd == false){
		    objIs[vals.tracker]= listSt;
		        listIs.push(objIs);
		}

    }
    
    var itemNameLs = [];
	for(var l = 0; l < listStatus.length; l++){
		itemNameLs.push(listStatus[l].itemName);
		listColor.push(listStatus[l].colorCode);
	}
	var checkLegend = true;
	
	for(var j = 0; j < listIs.length; j++){
		var trackerName = Object.keys(listIs[j])[0];
		var listStOb = Object.values(listIs[j]);
		var listDataSt = [];
		var listLabelSt = [];
		var labelLs = [];
		var dataLs = [];
		var total = 0;
		for(var i = 0; i < listStOb[0].length; i++){
			var statusName = Object.keys(listStOb[0][i])[0];
			var statusValue = Object.values(listStOb[0][i])[0];
			listDataSt.push(statusValue);
			listLabelSt.push(statusName);
			total = total +statusValue;
		}
		var reducer = (accumulator, currentValue) => accumulator + currentValue;
		
		if(listDataSt.length < listStatus.length){
			for(var f = 0; f < itemNameLs.length; f++){
				if(listLabelSt.includes(itemNameLs[f]) == true){
					var indexData = listLabelSt.indexOf(itemNameLs[f]);
					dataLs.push(listDataSt[indexData]);
				} else {
					dataLs.push(0);
				}
				labelLs.push(itemNameLs[f]);
			}
			
		}
		
		if(listDataSt.reduce(reducer) != 0){
			var configIssueCircle = {
		            type: 'pie',
		    	    data: {
		    	        datasets: [{
		    	        	data: dataLs,
		    	        	backgroundColor: listColor,
		    	            label: 'Issue'
		    	        }],
		    	        labels: labelLs
		    	    },
		    		options: {
		    			elements: {
		        		    arc: {
		        		      borderWidth: 0, // <-- Set this to derired value
		        		    }
		        		},
		    			responsive: true,
		    			scaleBeginAtZero: true,
		    			legend: {
		    	            display: false,
		    	            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		    			},
		    			events: true,
		    			animation: {
		    				duration: 500,
		    				easing: "easeOutQuart",
		    				onComplete: function () {
		    				  var ctx = this.chart.ctx;
		    				  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		    				  ctx.textAlign = 'center';
		    				  ctx.textBaseline = 'center';
		    				
		    				  this.data.datasets.forEach(function (dataset) {
		    				
		    					for (var i = 0; i < dataset.data.length; i++) {
		    					  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		    					      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
		    					      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
		    					      start_angle = model.startAngle,
		    					      end_angle = model.endAngle,
		    					      mid_angle = start_angle + (end_angle - start_angle)/2;
		    					
		    					  var x = mid_radius * Math.cos(mid_angle);
		    					  var y = mid_radius * Math.sin(mid_angle);
		    					
		    					  ctx.fillStyle = '#fff';
		    					  if (i == 3){ // Darker text color for lighter background
		    					    ctx.fillStyle = '#fff';
		    					  }
		    					  ctx.font = "14px sans-serif";
		    					  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
		    					  if(dataset.data[i] > 0 ){
		    						  ctx.fillText(dataset.data[i], model.x + x, model.y + y -25);
		    						  // Display percent in another line, line break
		    							// doesn't work for fillText
		    						  ctx.fillText(percent, model.x + x + 3, model.y + y - 5);
		    					  }
		    					}
		    				  });               
		    				},
		    			},
		    		}
		    	};
		} else {
			var dataChange = listDataSt;
			dataChange[0] = 1;
			var listCo = [];
			for(var k = 0 ; k < listLabelSt.length; k++){
				listCo.push("#9E9E9E");
			}
			var configIssueCircle = {
					type: 'pie',
					data: {
			    	  datasets: [{
			    		  data: dataChange,
			    	        backgroundColor: listCo,
			    	        label: 'Issue'
			    	    }],
			    	    labels: listLabelSt
					},
					options: {
						elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
					    tooltips: {enabled: false},
					    hover: {mode: null},
					    legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  				},
					  }
			    }
		}
		
		var htmlAppend = '<div class=" col-md-2 element-chart my-auto">'
	 		+'<div class="box-chart my-auto">'
				 +'<canvas id="testIssueCircle_'+j+'" width="300"></canvas>'
				 +'<h4 class="text-center"><a href="#" onclick="redirectFilterIssue(\''+projectId+'\', \''+trackerName+'\')">'+trackerName+'('+total+')'+'</a></h4>'
			 	+'</div>'
				+'<div class="legend" id="legendIssue_'+j+'"></div>'
			+'</div>';
			$("#progressIssueAdd").append(htmlAppend);
	    
		var testIssueCircle = document.getElementById("testIssueCircle_"+j).getContext("2d");
	    window.myPie = new Chart(testIssueCircle, configIssueCircle);
//	    $("#legendIssue_"+j).html(window.myPie.generateLegend());
		if(listDataSt.reduce(reducer) != 0){
			if(checkLegend == true){
				$("#legendIssue").html(window.myPie.generateLegend());
				checkLegend = false;
			}
			
		}
	}
	
}

function redirectFilterIssue(projectId, trackerName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterIssue/"+trackerName,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/issue";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}


function testProgressCircle(listTestProgress){
	var pass = listTestProgress[listTestProgress.length-1]["Pass"];
	var fail = listTestProgress[listTestProgress.length-1]["Fail"];
	var NA = listTestProgress[listTestProgress.length-1]["NA"];
	var degrade = listTestProgress[listTestProgress.length-1]["Degrade"];
	 var configTestPrCircle = {
        type: 'pie',
	    data: {
	        datasets: [{
	        	data:[pass,fail,degrade,NA],
	        	backgroundColor:["#1dcb8b","#CB371D","#AB47BC","#f5dc01"],
	            label: 'Test Progress'
	        }],
	        labels: [
	        	 getLang(currentLocale,"Pass"),
	        	 getLang(currentLocale,"Fail"),
	        	 getLang(currentLocale,"Degrade"),
	        	 getLang(currentLocale, "NA"),
	        ]
	    },
		options: {
			elements: {
    		    arc: {
    		      borderWidth: 0, // <-- Set this to derired value
    		    }
    		},
			responsive: true,
			scaleBeginAtZero: true,
			legend: {
	            display: false,
	            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
			},
			events: true,
			animation: {
				duration: 500,
				easing: "easeOutQuart",
				onComplete: function () {
				  var ctx = this.chart.ctx;
				  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
				  ctx.textAlign = 'center';
				  ctx.textBaseline = 'center';
				
				  this.data.datasets.forEach(function (dataset) {
				
					for (var i = 0; i < dataset.data.length; i++) {
					  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
					      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
					      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
					      start_angle = model.startAngle,
					      end_angle = model.endAngle,
					      mid_angle = start_angle + (end_angle - start_angle)/2;
					
					  var x = mid_radius * Math.cos(mid_angle);
					  var y = mid_radius * Math.sin(mid_angle);
					
					  ctx.fillStyle = '#fff';
					  if (i == 3){ // Darker text color for lighter background
					    ctx.fillStyle = '#fff';
					  }
					  ctx.font = "14px sans-serif";
					  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
					  if(dataset.data[i] > 0 ){
						  ctx.fillText(dataset.data[i], model.x + x, model.y + y -25);
						  // Display percent in another line, line break
							// doesn't work for fillText
						  ctx.fillText(percent, model.x + x + 3, model.y + y - 5);
					  }
					}
				  });               
				},
			},
		}
	};

    var testProgressCircle = document.getElementById("testProgressCircle").getContext("2d");
    window.myPie = new Chart(testProgressCircle, configTestPrCircle);
    $("#legendTestProgress").html(window.myPie.generateLegend());
}


function creatingMilestoneCircle(listManualAutomation, projectId){
	var dataChart=[];
	$.each(listManualAutomation, function(index,vals){
		if(vals["ProjectId"] == projectId){
			var automation = 0;
			var manual = 0;
			if(vals["manual"] != undefined){
				manual = vals["manual"];
			}
			
			if(vals["automation"] != undefined){
				automation = vals["automation"];
			}
			
			dataChart.push(automation);
			dataChart.push(manual);
		}
	})
	 var configTestPrCircle = {
        type: 'pie',
	    data: {
	    	datasets: [{
		        data:dataChart,
		        backgroundColor:["#4CAF50","#FFC107"],
		        label: getLang(currentLocale,'Testcase Project')
		    }],
		    labels: [
		    	getLang(currentLocale,"Automation Testcase"),
		    	getLang(currentLocale,"Manual Testcase"),
		    ]
	    },
		options: {
			elements: {
    		    arc: {
    		      borderWidth: 0, // <-- Set this to derired value
    		    }
    		},
			responsive: true,
			scaleBeginAtZero: true,
			legend: {
	            display: false,
	            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
			},
			events: true,
			animation: {
				duration: 500,
				easing: "easeOutQuart",
				onComplete: function () {
				  var ctx = this.chart.ctx;
				  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
				  ctx.textAlign = 'center';
				  ctx.textBaseline = 'center';
				
				  this.data.datasets.forEach(function (dataset) {
				
					for (var i = 0; i < dataset.data.length; i++) {
					  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
					      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
					      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
					      start_angle = model.startAngle,
					      end_angle = model.endAngle,
					      mid_angle = start_angle + (end_angle - start_angle)/2;
					
					  var x = mid_radius * Math.cos(mid_angle);
					  var y = mid_radius * Math.sin(mid_angle);
					
					  ctx.fillStyle = '#fff';
					  if (i == 3){ // Darker text color for lighter background
					    ctx.fillStyle = '#fff';
					  }
					  ctx.font = "14px sans-serif";
					  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
					  if(dataset.data[i] > 0 ){
						  ctx.fillText(dataset.data[i], model.x + x, model.y + y -25);
						  // Display percent in another line, line break
							// doesn't work for fillText
						  ctx.fillText(percent, model.x + x + 3, model.y + y - 5);
					  }
					}
				  });               
				},
			},
		}
	};

    var testProgressCircle = document.getElementById("creatingProgressMilestoneCircle").getContext("2d");
    window.myPie = new Chart(testProgressCircle, configTestPrCircle);
    $("#legendCreateProgress").html(window.myPie.generateLegend());
}

jQuery(".category.postform").click(function(){
	jQuery(".category.postform").removeClass('multiple_select_active');
	jQuery(this).addClass('multiple_select_active');
	jQuery(this).mousedown(function(e) {
		if (e.target.tagName == "OPTION") 
		{
		return; //don't close dropdown if i select option
		}
		jQuery(this).toggleClass('multiple_select_active'); //close dropdown if click inside <select> box
	});
	jQuery(this).on('blur', function(e) {
		jQuery(this).removeClass('multiple_select_active'); //close dropdown if click outside <select>
	});
		
	jQuery(this).mousedown(function(e) { //no ctrl to select multiple
		e.preventDefault(); 
		jQuery(this).prop('selected',jQuery(this).prop('selected') ? false : true); //set selected options on click
		jQuery(this).parent().change(); //trigger change event
	});

		
	jQuery(this).on('change', function() {
		var selected = $(".category.postform").val().toString(); //here I get all options and convert to string
		var document_style = document.documentElement.style;
		if(selected !== "")
		document_style.setProperty('--text', "'Selected: "+selected+"'");
		else
		document_style.setProperty('--text', "'Select values'");
	});
});

function backSummary(){
	$("#newTask").hide();
	$("#drawSummary").show();
}
var roleUser = '';
var UserEdit = '';
$( document ).ready(function() {
	getRoleUserProject();
	getInfoInvited();
	getUserProjectByProjectId();
	drawParentTaskInTestcase(taskList);
});

function getProject(){
	$(".itemStatusProject").attr("hidden",false);
	$("#modalAddNewProject .modal-title").text(getLang(currentLocale,"Edit Project"));
	$('#modalAddNewProject').modal({
		backdrop: 'static'
	});
	var infoProjectName =  document.getElementById("infoProjectName").innerHTML;
	var infoProjectType =  document.getElementById("infoProjectType").innerHTML;
	var infoDesciptionType =  document.getElementById("infoDesciptionType").innerHTML;
	var infoReportTime =  document.getElementById("infoReportTime").innerHTML;
	var infoReportTimeZone =  document.getElementById("infoReportTimeZone").innerHTML;
	var infoProjectId =  document.getElementById("infoProjectId").innerHTML;
	var infoStatusType = document.getElementById("infoStatusType").innerHTML;
	$("#nameProject").val(infoProjectName);
	$("#typeProject").val(infoProjectType);
	$("#descriptionProject").val(infoDesciptionType);
	$("#planStDa").val(detailProject.planStartDate ? moment(detailProject.planStartDate).format("MM/DD/YYYY"):"");
	$("#planEnDa").val(detailProject.planEndDate ? moment(detailProject.planEndDate).format("MM/DD/YYYY"):"");
	$("#actualStDa").val(detailProject.actualStartDate ? moment(detailProject.actualStartDate).format("MM/DD/YYYY"):"");
	$("#actualEnDa").val(detailProject.actualEndDate ? moment(detailProject.actualEndDate).format("MM/DD/YYYY"):"");
	$("#standardSize").val(detailProject.standardSize ? detailProject.standardSize:"");
	$("#standardUnit").val(detailProject.standardUnit ? detailProject.standardUnit:"");
	$("#estimatedProductivity").val(detailProject.estimatedProductivity ? detailProject.estimatedProductivity:"");
	$("#estimatedPrice").val(detailProject.estimatedPrice ? detailProject.estimatedPrice:"");
	$("#estimatedCost").val(detailProject.estimatedCost ? detailProject.estimatedCost:"");
	$("#estimatedDuration").val(detailProject.estimatedDuration ? detailProject.estimatedDuration:"");
	$("#actualQuantity").val(detailProject.actualQuantity ? detailProject.actualQuantity:"");
	$("#actualProductivity").val(detailProject.actualProductivity ? detailProject.actualProductivity:"");
	$("#agricultureSector").val(detailProject.agricultureSector ? detailProject.agricultureSector:"");
	$("#standardGAP").val(detailProject.standardGAP ? detailProject.standardGAP:"");
	$("#actualPrice").val(detailProject.actualPrice ? detailProject.actualPrice:"");
	$("#productionUnit").val(detailProject.productionUnit ? detailProject.productionUnit:"");
	$("#productType").val(detailProject.productType? detailProject.productType:"");
	$("#productCode").val(detailProject.productCode? detailProject.productCode:"");
	$("#productName").val(detailProject.productName? detailProject.productName:"");
	if(detailProject.image){
		listImgEditProject = JSON.parse(detailProject.image);
		drawListFileProject(JSON.parse(detailProject.image));
	}
	if(infoReportTime.length > 0) {
		$("#reportTime").val(infoReportTime.substring(0,5));
	} else {
		$("#reportTime").val(infoReportTime);
	}
	$('#statusProject option[value="'+infoStatusType+'"]').prop('selected', true);
	$("#reportTimeZone").val(infoReportTimeZone);
	$("#idProject").val(infoProjectId);
	
	// check if controlName exist this is valid
	if (infoProjectId != null){
		$("#nameProject").prop("readonly", true);
	}
}

// add new mail to in invited member
var countInputMail;
function addNewMailTo(){
	var countInputMail = $(".emailTo").length; 
	var numbAuto = countInputMail+=1;
	var itemAddNewEmail = 	'<div class="item form-group add-mail" id="mail'+numbAuto+'"><div class="input-icon right"><i class="fa check-true"></i>'
					    	+'<input class="form-control emailTo" placeholder="'+getLang(currentLocale,'Please enter')+'" required="required" type="email" name="emailTo"> <button class="close close-mail" onclick="removeEmail(\''+numbAuto+'\')"><span aria-hidden="true">&times;</span></button>'
					    	+'</div></div>';
	$("#apend_email").append(itemAddNewEmail);
}

function removeEmail(value){
	$("#mail"+value).remove();
}
//func validation add new project
var validateInviteMember= function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#invitedMemberModal');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }
    );
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	emailTo: {
        		email: true,
                required: true,
                maxlength: 50,
                minlength: 5,
                regex:/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            },
	    	roleProject: {
	    		required: true,
	    	}
        },
        
        messages: {
        	roleProject:{
        		required: getLang(currentLocale,"Cannot be blank!")
        		},
        	emailTo: {
        		regex: getLang(currentLocale,"Please enter the correct Email format !"),
        		email: getLang(currentLocale,"Please enter the correct Email format !"),
        		minlength: getLang(currentLocale,"Please enter at least 5 characters!"),
        		maxlength: getLang(currentLocale,"Please enter at less 50 characters!")
        	}
          },
          
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },
       
        submitHandler: function (form) {
            success2.show();
            error2.hide();
            sendMail();
            form2.validate().resetForm();
            form2.find('.form-group').removeClass('has-success');
            form2.find('.fa').removeClass('fa-check');
        }
    });

    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}

//func reset Form of modal add new mailto
function resetFormProject() {
	$('#modalInviteMember').on('hidden.bs.modal', function() {
		document.getElementById("invitedMemberModal").reset();
	    var $alertas = $('#invitedMemberModal');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	    $(".item.form-group.add-mail").remove();
	});
}

// send email to invite member
function sendMail(){
	if ($('#invitedMemberModal').valid() != true){
		return;
	}
	$("#buttonInvited").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#buttonInvited").prop("disabled", true);
	var infoProjectName =  document.getElementById("infoProjectName").innerHTML;
	var valueEmailName="";
	var itemEmailName = $("#invitedMemberModal input[name = emailTo]").serializeArray();
	var messageTo = $("#messageTo").val();
	var role = $("#roleProject").val();
	var sendEmail = {
			"toEmail":"",
			"title":getLang(currentLocale,"You have been invited to join Bottest.io on ")+infoProjectName+getLang(currentLocale," Project"),
			"content":messageTo,
			"projectRole":role,
			"projectId":$('#hiddenProjectId').val(),
			"@type" : "SendMailDTO"
	}
	for(var i = 0; i < itemEmailName.length; i++){
		if(i == itemEmailName.length-1 ){
			valueEmailName = valueEmailName + itemEmailName[i].value;
		}else if(itemEmailName[i+1].value != "") {
			valueEmailName = valueEmailName + itemEmailName[i].value + ",";
		}else{
			valueEmailName = valueEmailName + itemEmailName[i].value;
		}
	}
	sendEmail.toEmail = valueEmailName;
	$.ajax({
		url : jscontext+"/project/"+projectId+"/sendMail",
		type : "POST",
		data : JSON.stringify(sendEmail),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				if(result.data != ""){
					swal({
				        title: result.data,
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
				} else{
					swal({
				        title: "Save Success!!!",
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
					
					var html = "";
					for(var i = 0 ; i < itemEmailName.length ; i++){
						if($('#user-add-invited #member p[name="'+itemEmailName[i]["value"]+'"]').length == 0){
							html +='<p name="'+itemEmailName[i]["value"]+'">'+ itemEmailName[i]["value"]+'( '+getLang(currentLocale,"Not Joined")+' )</p>';
						}	
					}
					$("#user-add-invited #member").append(html);
				}
				$("#modalInviteMember").modal("hide"); 
				$("#emailTo").val("");
				$("#messageTo").val("");
				$("#buttonInvited").html("<i class='icon-user-follow'></i>"+getLang(currentLocale,"Invited"));
				$("#buttonInvited").prop("disabled", false);
				resetFormProject();
			} 
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function showModalInvitedMember(){
	$('#modalInviteMember').modal({
		backdrop: 'static'
	});
}

function getInfoInvited(){
	$("#user-add-invited").html("<div class='regencyMember'>"+getLang(currentLocale,"User Is Being Invited")
			+"</div><div id='member' class='font-italic text-success'></div>");
	$.ajax({
		url : jscontext+"/project/"+ projectId + "/getInfoMember",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			var html = "";
			var userIn = result.data;
			$("#member").find('li').remove();
			$("#member").html("");
			if(userIn.length > 0){
				for(var i = 0 ; i < userIn.length ; i++){
					html +='<p name="'+userIn[i]["toEmail"]+'">' + userIn[i]["toEmail"]+'( '+getLang(currentLocale,"Not Joined")+' )</p>';
				}
				$("#member").append(html);
			}
		},
		error : function(request,error)
	      {	
				console.log("Error: "+error);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
	      }
	});
}

function getRoleUserProject(){
	roleUser = "";
	$.ajax({
		url : jscontext +"/project/"+projectId+"/getUserProject",
		type : "GET",
		async: false,
		success : function(result) {
			var data= result.data;
			roleUser = data.projectRole;
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function updateRole(){
	var userProject = {
			"projectRole":$('#roleProjectEdit').val(),
			"id":userEdit,
			}
	$.ajax({
		url : jscontext +"/project/"+projectId+"/editRoleProject",
		type : "POST",
		data : JSON.stringify(userProject),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: "Save Success!!!",
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				getUserProjectByProjectId();
				$("#modalEditMemberSetting").modal("hide");
			} 
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getUserProjectByProjectId(){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/getUserProjectByProjectId",
		type : "GET",
		success : function(result) {
			if (!(result.errormessage) ) {
				getUserProjects(result.data);
			} 
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function duplicateUser(arr){
	var duplicateIds = [];
	var valueDupl = arr
    .map(e => e['lastname'] + e['firstname'])
    .map((e, i, final) => final.indexOf(e) !== i && i)
    .filter(obj=> arr[obj])
    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
	 $.each(valueDupl, function(index, value){
		 $.each(arr, function(indexArr, valueArr){
			 var name = valueArr["lastname"] + valueArr["firstname"]; 
			 if(name == value){
				 duplicateIds.push(valueArr);
			 }
		 })
	 })
	 
	 return duplicateIds ;
}

function getUserProjects(userProjects){
	var duplicateIds = duplicateUser(userProjects);
	$(".role-admin").html("");
	$(".role-manager").html("");
	$(".role-developer").html("");
	$(".role-tester").html("");
	$(".role-customer").html("");
	$.each(userProjects,function(index, value){
		if(value["projectRole"] == 'PROJECT_ADMIN'){
			if(value["lastname"] != null && value["firstname"] != null){
				var count = 0 ;
				if(duplicateIds.length > 0){
					$.each(duplicateIds, function(indexs,values){
						if(values.username == value["username"]){
							$(".role-admin").append('<div class="font-italic color-role" >'+ value["lastname"]+" "+value["firstname"]+" ( "+value["username"]+" ) "+'</div>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$(".role-admin").append('<div class="font-italic color-role">' +value["lastname"]+" "+value["firstname"]+'</div>');
				}
			} else{
				$(".role-admin").append('<div class="font-italic color-role">'+value["username"]+'</div>');
			}
		}
		if(value["projectRole"] == 'Manager'){
			
			if(value["lastname"] != null && value["firstname"] != null){
				var count = 0 ;
				if(duplicateIds.length > 0){
					$.each(duplicateIds, function(indexs,values){
						if(values.username == value["username"]){
							$(".role-manager").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
									+ value["projectRole"]+'\')">'+value["lastname"]+" "+value["firstname"]+" ( "+value["username"]+" ) "+'</div>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$(".role-manager").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
							+ value["projectRole"]+'\')">'+value["lastname"]+" "+value["firstname"]+'</div>');
				}
			} else{
				$(".role-manager").append('<div class="font-italic color-role" id="user'+value["id"]+'"'
						+'onclick="editMember(\''+value["id"]+'\',\''+value["projectRole"]+'\')">'+value["username"]+'</div>');
			}
		}
		
		if(value["projectRole"] == 'Developer'){
			if(value["lastname"] != null && value["firstname"] != null){
				var count = 0 ;
				if(duplicateIds.length > 0){
					$.each(duplicateIds, function(indexs,values){
						if(values.username == value["username"]){
							$(".role-developer").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
									+value["projectRole"]+'\')">'+value["lastname"]+" "+value["firstname"]+" ( "+value["username"]+" ) "+'</div>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$(".role-developer").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''+value["projectRole"]
						+ '\')">'+value["lastname"]+" "+value["firstname"]+'</div>');
				}
			} else{
				$(".role-developer").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
						+ value["projectRole"]+'\')">'+value["username"]+'</div>');
			}
		}
		
		if(value["projectRole"] == 'Tester'){
			if(value["lastname"] != null && value["firstname"] != null){
				var count = 0 ;
				if(duplicateIds.length > 0){
					$.each(duplicateIds, function(indexs,values){
						if(values.username == value["username"]){
							$(".role-tester").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
								+value["projectRole"]+'\')" >'+value["lastname"]+" "+value["firstname"]+" ( "+value["username"]+" ) "+'</div>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$(".role-tester").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
						+value["projectRole"]+'\')">'+value["lastname"]+" "+value["firstname"]+'</div>');
				}
			} else{
				$(".role-tester").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
					+value["projectRole"]+'\')">'+value["username"]+'</div>');
			}
		}
		
		if(value["projectRole"] == 'Customer'){
			if(value["lastname"] != null && value["firstname"] != null){
				var count = 0 ;
				if(duplicateIds.length > 0){
					$.each(duplicateIds, function(indexs,values){
						if(values.username == value["username"]){
							$(".role-customer").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
									+ value["projectRole"]+'\')">'+value["lastname"]+" "+value["firstname"]+" ( "+value["username"]+" ) "+'</div>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$(".role-customer").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
							+value["projectRole"]+'\')" >'+value["lastname"]+" "+value["firstname"]+'</div>');
				}
			} else{
				$(".role-customer").append('<div class="font-italic color-role" id="user'+value["id"]+'" onclick="editMember(\''+value["id"]+'\',\''
						+value["projectRole"]+'\')">'+value["username"]+'</div>');
			}
		}
		if(roleUser == "PROJECT_ADMIN" || roleUser == "Manager"){
			$("#user"+value["id"]).hover(function(e){
				$(this).css("color",e.type === "mouseenter"?"#008a00":"#1dcb8b") ;
			});
		}	
	})
}

function resetFormEditRole(){
	$('#modalEditMemberSetting').on('hidden.bs.modal', function() {
		document.getElementById("EditMemberModal").reset();
	    var $alertas = $('#EditMemberModal');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	    $(".itemStatusProject").attr("hidden",true);
	});
} 

function editMember(id,roleMember){
	userEdit = id ;
	if(roleUser == "PROJECT_ADMIN"){
		$("#modalEditMemberSetting").modal("show");
		$('#roleProjectEdit option[value='+roleMember+']').prop('selected', true);
	}
	if(roleUser == "Manager"){
		if(roleMember == "Developer" || roleMember == "Tester" || roleMember == "Customer"){
			$("#modalEditMemberSetting").modal("show");
			$('#roleProjectEdit option[value='+roleMember+']').prop('selected', true);
		}
	}
}
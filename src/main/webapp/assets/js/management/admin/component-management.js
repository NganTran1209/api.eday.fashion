var arrayComponentList = '';
$( document ).ready(function() {
	getListComponent();
	hiddenModalEditConponent();
});


function callBackJsListUser(){
	$('#listComponentProject').DataTable( {
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]],
        "scrollX": true,
        "columnDefs": [
        	   { orderable: false, targets: -1 }
        	]
} );
}

//func get list component
function getListComponent(){
	//var userComponent = [];
	$.ajax({
		url : jscontext +"/admin/component/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage)) {
				var userComponent =  result.data;
				drawComponentList(userComponent);
				arrayComponentList = userComponent;
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
			
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

// function drawing list component
function drawComponentList(userComponent){
	$("#tableComponentManagement").empty();
	$.each(userComponent, function(indexComponent, valueComponent){		
		var indexPlus = indexComponent + 1;
		var itenTable = '<tr class="odd gradeX">'
						+'<td class="center">'+valueComponent['id']+'</td>'
						+'<td class="center">'+valueComponent['groupCode']+'</td>'
						+'<td class="center">'+valueComponent['itemValue']+'</td>'
						+'<td class="center">'+valueComponent['itemName']+'</td>'
						+'<td class="center">'+valueComponent['defaultC']+'</td>'
						+'<td class="center">'
						+'<a href="javascript:void(0);" class="btn btn-tbl-edit btn-info btn-xs shawdow-0" onclick="getEditComponent(\''+valueComponent['id']+'\')"><i class="fa fa-pencil"></i></a>'
						+'<a class="btn btn-tbl-delete btn-xs btn-default shawdow-0" onclick="callDeleteComponent(\''+valueComponent['id']+'\',\''+valueComponent['itemName']+'\')"><i class="fa fa-trash-o "></i></a>'
						+'</td>'
						+'</tr>';
		$("#tableComponentManagement").append(itenTable);
	});
	callBackJsListUser();
}


function saveComponent(){
	$("#editComponent").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#editComponent").prop("disabled", true);
	var idComponentExist = $("#idComponent").val();
	var groupCodeComponent = $("#editGroupCode").val();
	var itemValueComponent = $("#editItemValue").val();
	var itemNameComponent = $("#editItemName").val();
	var itemComponentUpdate = '';
	var itemCheckDefault = '';
	if( $('#defaultComponent').is(":checked	") ){
		itemCheckDefault = 1;
	} else{
		itemCheckDefault = 0;
	}
	itemComponentUpdate = [{
								"id": idComponentExist,
					            "groupCode": groupCodeComponent,
					            "itemValue": itemValueComponent,
					            "itemName": itemNameComponent,
					            "defaultC": itemCheckDefault
							}];
	if(idComponentExist == '' || idComponentExist == null){
		var textAlert = 'Create';
	}else{
		var textAlert = 'Update';
	}
	$.ajax({
		url : jscontext +"/admin/component/create",
		type : "POST",
		data : JSON.stringify(itemComponentUpdate),
		contentType : "application/json",
		success : function(result) {
			console.log(result);
			if (!(result.errormessage)) {
				var table = $('#listComponentProject').DataTable();
//			    	load list component
		    	 table.destroy();
		    	 getListComponent();
		    	 $("#modalEditComponent").modal("hide");
		    	 $("#editComponent").html("<i class='ti-check'></i> Save");
		    	 $("#editComponent").prop("disabled", false);
		    	 swal({
				        title: ""+textAlert+" Component \n"+itemNameComponent+" Success!!!",
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
			} else {
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
}


// function load info component - param: username
function getEditComponent(itemComponentId){
	$("#itemIdInterfaceLock").show();
	$("#idComponentLock").val(itemComponentId); 
	$("#idComponent").val(itemComponentId); 
	$('#modalEditComponent').find(".modal-title").text("Edit Component");  
	$("#modalEditComponent").modal("show");
	
	$.each(arrayComponentList, function(indexInfoComponent, valueInfoComponent){
		if(valueInfoComponent['id'] == itemComponentId){ 
			$("#editGroupCode option[value='"+valueInfoComponent['groupCode']+"']").prop("selected",true)
			$("#editItemValue").val(valueInfoComponent['itemValue']);
			$("#editItemName").val(valueInfoComponent['itemName']);
			if( valueInfoComponent['defaultC'] == true){
				$("#defaultComponent").prop( "checked", true );
			}
		}
	});
}

// function call and alert delete
function callDeleteComponent(itemComponentId,itemComponentName){
	
	 swal({
	        title: "Are you sure delete component:\n" + itemComponentName +"?",
	        text: "You will not be able to recover this imaginary component!",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        closeOnConfirm: false
	    }, function () {
//	    	function delete record
	    	getDeleteComponent(itemComponentId,itemComponentName);
	    });
}

function getDeleteComponent(itemComponentId,itemComponentName){
	var dataItemComponent = '';
	var dataItemNameComponent = '';
	$.each(arrayComponentList, function(indexInfoComponent, valueInfoComponent){
		if(valueInfoComponent['id'] == itemComponentId || valueInfoComponent['itemName'] == valueInfoComponent){
			dataItemComponent = {
					"id": valueInfoComponent['id'],
		            "groupCode": valueInfoComponent['groupCode'],
		            "itemValue": valueInfoComponent['itemValue'],
		            "itemName": valueInfoComponent['itemName']
				};
			dataItemNameComponent = valueInfoComponent['itemName'];
			
		}
	});
	$.ajax({
		url : jscontext +"/admin/component/delete",
		type : "POST",
		data : JSON.stringify(dataItemComponent),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var table = $('#listComponentProject').DataTable();
//		    	load list component
		    	 table.destroy();
		    	 getListComponent();
			} else {
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			swal({
		        title: "Delete Component \n"+dataItemNameComponent+"\n Success!!!",
		        type: "success",
		        timer: 2000,
		        showConfirmButton: false
		    });
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function hiddenModalEditConponent(){
	$('#modalEditComponent').on('hidden.bs.modal', function () {
		$("#idComponent").val("");
		$("#formEditComponent")[0].reset();
	})
}

function callModalAddComponent(){
	$("#itemIdInterfaceLock").hide();
	$('#modalEditComponent').find(".modal-title").text("Add New Component");
}
var arrayInterfaceList = '';
$( document ).ready(function() {
	getListInterface();
	getListComponent();
	hiddenModalEditInterface();
});


function callBackJsListUser(){
	$('#listInterfaceProject').DataTable( {
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]],
        "scrollX": true,
        "columnDefs": [
        	   { orderable: false, targets: -1 }
        	]
} );
}


//func get list interface
function getListInterface(){
	//var userInterface = [];
	$.ajax({
		url : jscontext +"/admin/interface/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userInterface =  result.data;
				console.log(userInterface);
				drawInterfaceList(userInterface);
				arrayInterfaceList = userInterface;
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


function getListComponent(){
	//var userComponent = [];
	$.ajax({
		url : jscontext +"/admin/component/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userComponent =  result.data;
				loadOsComponentList(userComponent);
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


// function drawing list interface
function drawInterfaceList(userInterface){
	$("#tableInterfaceManagement").empty();
	$.each(userInterface, function(indexInterface, valueInterface){		
		var itenTable = '<tr class="odd gradeX">'
						+'<td class="center">'+valueInterface['id']+'</td>'
						+'<td class="center">'+valueInterface['domain']+'</td>'
						+'<td class="center">'+valueInterface['username']+'</td>'
						+'<td class="center">'+valueInterface['jobName']+'</td>'
						+'<td class="center">'+valueInterface['os']+'</td>'
						+'<td class="center">'+valueInterface['runningOS']+'</td>'
						+'<td class="center">'+valueInterface['browser']+'</td>'
						+'<td class="center">'+valueInterface['scmType']+'</td>'
						+'<td class="center">'+valueInterface['nodeLabel']+'</td>'
						+'<td class="center">'
						+'<a href="javascript:void(0);" class="btn btn-tbl-edit btn-info btn-xs shawdow-0" onclick="getEditInterface(\''+valueInterface['id']+'\')"><i class="fa fa-pencil"></i></a>'
						+'<a class="btn btn-tbl-delete btn-xs btn-default shawdow-0" onclick="calldeletedInterface(\''+valueInterface['id']+'\',\''+valueInterface['jobName']+'\')"><i class="fa fa-trash-o "></i></a>'
						+'</td>'
						+'</tr>';
		$("#tableInterfaceManagement").append(itenTable);
	});
	callBackJsListUser();
}

function loadOsComponentList(userComponent){
	$.each(userComponent, function(indexComponent, valueComponent){	
		if(valueComponent["groupCode"] == "OS"){
			var optionOs = '<option value="'+valueComponent["itemValue"]+'">'+valueComponent["itemName"]+'</option>';
			$("#slOs").append(optionOs);
		}
		if(valueComponent["groupCode"] == "RUNNING_OS"){
			console.log(valueComponent["runningOS"]);
			var optionRunning = '<option value="'+valueComponent["itemValue"]+'">'+valueComponent["itemName"]+'</option>';
			$("#runningOS").append(optionRunning);
		}
		if(valueComponent["groupCode"] == "BROWSER"){
			var optionBrowser = '<option value="'+valueComponent["itemValue"]+'">'+valueComponent["itemName"]+'</option>';
			$("#eidtBrowser").append(optionBrowser);
		}
		if(valueComponent["groupCode"] == "LOCATION"){
			var optionLocation = '<option value="'+valueComponent["itemValue"]+'">'+valueComponent["itemName"]+'</option>';
			$("#editInterfLocation").append(optionLocation);
		}
		
	});
}

function saveInterface(){
	$("#saveInterfaceToDB").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveInterfaceToDB").prop("disabled", true);
	var idInterfaceExist = $("#idInterface").val();
	var itemDomainInterface = $("#editDomian").val();
	var itemUsernameInterface = $("#editInterfUsename").val();
	var itemJobNameInterface = $("#editInterfJobname").val();
	var itemOsInterface = $("#slOs").val();
	var itemRunningOsNameInterface = $("#runningOS").val();
	var itemLocationInterface = $("#editInterfLocation").val();
	var itemBrowserInterface = $("#eidtBrowser").val();
	var itemScmTypeInterface = $("#editScmType").val();
	var itemNodeLabelInterface = $("#editNodeLabel").val();
	var itemInterfaceUpdate = '';
	
	itemInterfaceUpdate = [{
								"id": idInterfaceExist,
					            "domain": itemDomainInterface,
					            "username": itemUsernameInterface,
					            "token": $("#editInterfToken").val(),
					            "jobName": itemJobNameInterface,
					            "os": itemOsInterface,
					            "runningOS": itemRunningOsNameInterface,
					            "browser": itemBrowserInterface,
					            "location": itemLocationInterface,
					            "scmType": itemScmTypeInterface,
					            "nodeLabel": itemNodeLabelInterface
							}];
	if(idInterfaceExist == '' || idInterfaceExist == null){
		var textAlert = 'Create';
	}else{
		var textAlert = 'Update';
	}					
	$.ajax({
		url : jscontext +"/admin/interface/create",
		type : "POST",
		data : JSON.stringify(itemInterfaceUpdate),
		contentType : "application/json",
		success : function(result) {
			console.log(result);
			if (!(result.errormessage) ) {
				var table = $('#listInterfaceProject').DataTable();
//			    	load list component
		    	 table.destroy();
		    	 getListInterface();
		    	 $("#modalEditInterface").modal("hide");
		    	 $("#saveInterfaceToDB").html("<i class='ti-check'></i> Save");
				$("#saveInterfaceToDB").prop("disabled", false);
		    	 swal({
				        title: ""+textAlert+" Component \n"+itemJobNameInterface+" Success!!!",
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
			} else {
				$("#saveInterfaceToDB").html("<i class='ti-check'></i> Save");
				$("#saveInterfaceToDB").prop("disabled", false);
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
}

// function load info interface - param: id
function getEditInterface(itemNameId){
	$("#itemIdInterfaceLock").show();
	$("#idInterface").val(itemNameId);  
	$("#idInterfaceLock").val(itemNameId);
	$('#modalEditInterface').find(".modal-title").text("Edit Interface");  
	$("#modalEditInterface").modal("show");
	$.each(arrayInterfaceList, function(indexInfoInterface, valueInfoInterface){
		if(valueInfoInterface['id'] == itemNameId){
			$("#editDomian").val(valueInfoInterface['domain']);
			$("#editInterfUsename").val(valueInfoInterface['username']);
			$("#editInterfJobname").val(valueInfoInterface['jobName']);
			$("#editInterfLocation option[value='"+valueInfoInterface['location']+"']").prop("selected",true);
			$("#editInterfToken").val(valueInfoInterface['token']);
			$("#slOs option[value='"+valueInfoInterface['os']+"']").prop("selected",true);
			$("#runningOS option[value='"+valueInfoInterface['runningOS']+"']").prop("selected",true);
			$("#eidtBrowser option[value='"+valueInfoInterface['browser']+"']").prop("selected",true);
			$("#editScmType option[value='"+valueInfoInterface['scmType']+"']").prop("selected",true);
		}
	});
}

function calldeletedInterface(itemInterfaceId,itemInterfaceName){
	 swal({
	        title: "Are you sure delete interface:" + itemInterfaceName +"?",
	        text: "You will not be able to recover this imaginary interface!",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        closeOnConfirm: false
	    }, function () {
	//	    	function xoa tai day
    		deleteInterface(itemInterfaceId,itemInterfaceName);
	    });
}

function deleteInterface(itemInterfaceId,itemInterfaceName){
	var dataItemComponent = '';
	$.each(arrayInterfaceList, function(indexInfoInterface, valueInfoInterface){
		if(valueInfoInterface['id'] == itemInterfaceId || valueInfoInterface['itemName'] == itemInterfaceName){

			dataIteminterface = {
					"id": valueInfoInterface['id'],
		            "domain": valueInfoInterface['domain'],
		            "username": valueInfoInterface['username'],
		            "token": valueInfoInterface['token'],
		            "jobName": valueInfoInterface['jobName'],
		            "os": valueInfoInterface['os'],
		            "runningOS": valueInfoInterface['runningOS'],
		            "browser": valueInfoInterface['browser'],
		            "scmType": valueInfoInterface['scmType']
				};
			
		}
	});
	$.ajax({
		url : jscontext +"/admin/interface/delete",
		type : "POST",
		data : JSON.stringify(dataIteminterface),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var table = $('#listInterfaceProject').DataTable();
//		    	load list component
		    	 table.destroy();
		    	 getListInterface();
			} else {
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			swal({
		        title: "Delete interface\n"+ itemInterfaceName +" Success!!!",
		        type: "success",
		        timer: 2000,
		        showConfirmButton: false
		    });
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function hiddenModalEditInterface(){
	$('#modalEditInterface').on('hidden.bs.modal', function () {
		$("#idInterface").val("");
		$("#formEditInterface")[0].reset();
	})
}


function callModalAddInterface(){
	$("#itemIdInterfaceLock").hide();
	$('#modalEditInterface').find(".modal-title").text("Add New Interface");
}
$( document ).ready(function() {
	countUserDashboard();
	countListComponentDashboard();
	countListInterface();
	countProjects();
});

//func count list user
function countUserDashboard(){
	$.ajax({
		url : jscontext +"/admin/user/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userList =  result.data;
				jQuery(function($) {
			        $('#numbUser').countTo({
			            from: 0,
			            to: userList.length,
			            speed: 1000,
			            refreshInterval: 50,
			            onComplete: function(value) {
			                console.debug(this);
			            }
			        });
			    });
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


//func count list component
function countListComponentDashboard(){
	$.ajax({
		url : jscontext +"/admin/component/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userComponent =  result.data;
				jQuery(function($) {
			        $('#numbComponent').countTo({
			            from: 0,
			            to: userComponent.length,
			            speed: 1000,
			            refreshInterval: 50,
			            onComplete: function(value) {
			                console.debug(this);
			            }
			        });
			    });
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func count list interface
function countListInterface(){
	//var userInterface = [];
	$.ajax({
		url : jscontext +"/admin/interface/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userInterface =  result.data;
				console.log(userInterface)
				jQuery(function($) {
			        $('#numbInterface').countTo({
			            from: 0,
			            to: userInterface.length,
			            speed: 1000,
			            refreshInterval: 50,
			            onComplete: function(value) {
			                console.debug(this);
			            }
			        });
			    });
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func count list project
function countProjects(){
	$.ajax({
		url : jscontext +"/project/list",
		type : "get",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var listProject =  result.data;
				jQuery(function($) {
			        $('#numbProject').countTo({
			            from: 0,
			            to: listProject.length,
			            speed: 1000,
			            refreshInterval: 50,
			            onComplete: function(value) {
			                console.debug(this);
			            }
			        });
			    });
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


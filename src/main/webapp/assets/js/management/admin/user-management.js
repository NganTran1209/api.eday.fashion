var countryArray = [];
$( document ).ready(function() {
	getListUser();
	getArrayCountry();
	hiddenModalEditUser();
});

function getArrayCountry(){
	$.each($("#country option"), function(indexInfoUser, valueInfoUser){
	var countryName = valueInfoUser.label;
	var countryValue = valueInfoUser.value;
	var itemCountry = {"Name":countryName,"Value":countryValue};
	countryArray.push(itemCountry);
	});
}

function callBackJsListUser(){
	$('#listUserProject').DataTable( {
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]],
        "scrollX": true,
        "columnDefs": [
        	   { orderable: false, targets: -1 }
        	]
} );
}
var arrayUserList = '';
//func get list user
function getListUser(){
	//var userList = [];
	$.ajax({
		url : jscontext +"/admin/user/getAll",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userList =  result.data;
				drawUseList(userList);
				arrayUserList = userList;
			} else {
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

// function drawing list user
function drawUseList(userList){
	$("#tableUserManagement").empty();
	$.each(userList, function(indexUser, valueUser){
		console.log(valueUser);
		var getRoleUser = valueUser['role'];
		var itemRoleUser = '';
		var itemCountry = '';
		var colorRole='';
		if(getRoleUser == 'ROLE_ADMIN'){
			itemRoleUser = 'Admin';
			colorRole = 'success';
		}else if(getRoleUser == 'ADMIN'){
			itemRoleUser = 'Admin';
			colorRole = 'success';
		}else{
			itemRoleUser = 'User';
			colorRole = 'info';
		}
		var getActiveUser = valueUser['enabled'];
		var itemActiveUser = '';
		colorActive = '';
		if(getActiveUser == true){
			itemActiveUser = 'Yes';
			colorActive = 'success';
		}else{
			itemActiveUser = 'No';
			colorActive = 'danger';
		}
		$.each(countryArray, function(indexCountry, valueCountry){
			if(valueCountry.Value == valueUser['country']){
				console.log(valueCountry.Value);
				itemCountry = valueCountry["Name"]; 
				
			}
		});
		var itenTable = '<tr class="odd gradeX">'
						+'<td class="user-circle-img sorting_1" width="5%"><img src="'+jscontext+'/assets/img/user/user1.jpg" alt=""></td>'
						+'<td class="center" width="20%">'+valueUser['username']+'</td>'
						+'<td class="center" width="10%">'
						+(valueUser['firstname'] != null ? valueUser['firstname'] : "")+'&nbsp;'+(valueUser['lastname'] != null ? valueUser['lastname'] : "")
						+'</td>'
						+'<td class="center" width="10%">'
						+(valueUser['company'] != null ? valueUser['company'] : "")+
						'</td>'  
						+'<td class="center" width="10%">'+
						'<a href="mailto:'+( valueUser['email'] != null ? valueUser['email'] : "")+'">'
						+(valueUser['email'] != null ? (valueUser['email']): "")
						+'</a>'
						+'</td>'
						+'<td class="center" width="10%">'+itemCountry+'</td>'
						+'<td class="center" width="10%">'
						+(valueUser['position'] != null ? valueUser['position'] : "")
						+'</td>'
						+'<td class="center" width="10%">'
						+(valueUser['language'] != null ? valueUser['language'] : "")
						+'</td>'
						+'<td class="center" width="5%"><span class="label label-'+colorRole+' label-history">'+itemRoleUser+'</span></td>'
						+'<td class="center" width="5%"><span class="label label-'+colorActive+' label-history">'+itemActiveUser+'</span></td>'
						+'<td class="center" width="5%">'
						+'<a href="javascript:void(0);" class="btn btn-tbl-edit btn-info btn-xs shawdow-0" onclick="getEditUser(\''+valueUser['username']+'\')"><i class="fa fa-pencil"></i></a>'
						+'<a class="btn btn-tbl-delete btn-xs btn-default shawdow-0" onclick="callDeletedUser(\''+valueUser['username']+'\')"><i class="fa fa-trash-o "></i></a>'
						+'</td>'
						+'</tr>';
		$("#tableUserManagement").append(itenTable);
	});
	callBackJsListUser();
}

// function load info user - param: username
function getEditUser(itemNameUser){
	$("#modalEditUser").modal("show");
	$.each(arrayUserList, function(indexInfoUser, valueInfoUser){
		if(valueInfoUser['username'] == itemNameUser){
			$("#userNameHidden").val(valueInfoUser['username']);
			$("#editUserName").val(valueInfoUser['username']);
			$("#editfirstName").val(valueInfoUser['firstname']);
			$("#editlastName").val(valueInfoUser['lastname']);
			$("#editcompany").val(valueInfoUser['company']);
			$("#editcountry option[value='"+valueInfoUser['country']+"']").prop("selected",true);
			$("#editUserRoles option[value='"+valueInfoUser['role']+"']").prop("selected",true);
		}
	});
}

function saveUser(){
	$("#updateRoleUser").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#updateRoleUser").prop("disabled", true);
	var userName= $("#userNameHidden").val()
	var role = $("#editUserRoles").val(); 
	var enable = $("#editUserEnable").val(); 
	var itemUserUpdate= {
				"username": userName,
				"email": userName,
				"role": role,
				"enable": enable,
				"@type" : "BottestUser"
		};
	
	if(userNameHidden == '' || userNameHidden == null){
		var textAlert = 'Create';
	}else{
		var textAlert = 'Update';
	}					
	$.ajax({
		url : jscontext +"/admin/user/update",
		type : "POST",
		data : JSON.stringify(itemUserUpdate),
		contentType : "application/json",
		async:false,
		success : function(result) {
			console.log(result);
			if (!(result.errormessage) ) {
				var table = $('#listUserProject').DataTable();
//			    	load list user
		    	 table.destroy();
		    	 getListUser();
		    	 $("#modalEditUser").modal("hide");
		    	 $("#updateRoleUser").html("<i class='ti-check'></i> Save");
				 $("#updateRoleUser").prop("disabled", false);
		    	 swal({
				        title: ""+textAlert+" User \n"+userName+" Success!!!",
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
			} else {
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
}


function callDeletedUser(itemNameUser){
	 swal({
	        title: "Are you sure delete user: \n" + itemNameUser +"?",
	        text: "You will not be able to recover this imaginary user!",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        closeOnConfirm: false
	    }, function () {
//	    	function xoa tai day
	    	deleteUser(itemNameUser);
	    	
	    });
}


function deleteUser(itemUserName){
	var itemUserSelected = {
		"username": itemUserName,
	};
	console.log(JSON.stringify(itemUserSelected));
	$.ajax({
		url : jscontext +"/admin/user/delete",
		type : "POST",
		data : JSON.stringify(itemUserSelected),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var table = $('#listUserProject').DataTable();
//		    	load list user
		    	 table.destroy();
		    	 getListUser();
		    	 swal({
				        title: "Delete User \n"+itemUserName+"\n Success!!!",
				        type: "success",
				        timer: 2000,
				        showConfirmButton: false
				    });
			} else {
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			swal({
		        title: "Delete User\n"+ itemUserName +"\n Success!!!",
		        type: "success",
		        timer: 2000,
		        showConfirmButton: false
		    });
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function hiddenModalEditUser(){
	$('#modalEditUser').on('hidden.bs.modal', function () {
		$("#formEditUser")[0].reset();
	})
}
var arraySoftwareList = '';
var requirement;
var releaseNote;
$( document ).ready(function() {
	getAllSoftWare();
	hiddenModalEditSoftware();
	modelEditor();
});

function modelEditor(){
	ClassicEditor
		.create( document.querySelector( '#requirement' ), {
			// toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
		} )
		.then( editor => {
			window.editor = editor;
			requirement = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
	ClassicEditor
	.create( document.querySelector( '#releaseNote' ), {
		// toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
	} )
	.then( editor => {
		window.editor = editor;
		releaseNote = editor;
	} )
	.catch( err => {
		console.error( err.stack );
	} );
}
function saveSoftware(){
	$("#UploadSoftware").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#UploadSoftware").prop("disabled", true);
	var formData = new FormData($("#formEditSoftware")[0]);
	formData.set('requirement',requirement.getData());
	formData.set('releaseNote',releaseNote.getData());
	var idSoftware = $("#idSoftware").val();
	var uploadSoftware;
	if(idSoftware == ""){
		uploadSoftware = "Upload ";
	} else{
		uploadSoftware = "Edit ";
	}
	$.ajax({
		type : "POST",
		url : jscontext +"/admin/software/create",
		enctype : 'multipart/form-data',
		processData : false,
		contentType : false,
		data : formData,
		timeout : 600000,
		success : function(result) {
			if (!(result.errormessage) ) {
				if(result.data == "File was exist!"){
					$("#modalUploadSoftware").modal("hide");
			    	$("#UploadSoftware").html("<i class='ti-check'></i> Save");
					$("#UploadSoftware").prop("disabled", false);
			    	swal({
					        title:"File was exist!",
					        type: "warning",
					        timer: 2000,
					        showConfirmButton: false
					});
				} else{
			    	 $("#modalUploadSoftware").modal("hide");
			    	 $("#UploadSoftware").html("<i class='ti-check'></i> Save");
					 $("#UploadSoftware").prop("disabled", false);
			    	 swal({
					        title: uploadSoftware	+ "Software Success!!!",
					        type: "success",
					        timer: 2000,
					        showConfirmButton: false
					    });
				}
				getAllSoftWare();
			} else {
				$("#UploadSoftware").html("<i class='ti-check'></i> Save");
				$("#UploadSoftware").prop("disabled", false);
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
			
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}	
function drawSoftwareList(softwares){
		$("#tableSoftware").empty();
		$.each(softwares, function(indexSoftware, valueSoftware){	
			var upDate = new Date(valueSoftware["upDate"]).toISOString().slice(0,10);
			var itenTable = '<tr class="odd gradeX">'
							+'<td class="center" >'+valueSoftware['id']+'</td>'
							+'<td class="center" width="10%">'+valueSoftware['type']+'</td>'
							+'<td class="center" width="5%">'+valueSoftware['version']+'</td>'
							+'<td class="center" width="10%">'+valueSoftware['fileName']+'</td>'
							+'<td class="center" width="10%">'+upDate+'</td>'
							+'<td class="center" width="30%">'+valueSoftware['requirement']+'</td>'
							+'<td class="center" width="30%">'+valueSoftware['releaseNote']+'</td>'
							+'<td class="center" width="5%">'
							+'<a class="btn btn-tbl-edit btn-info btn-xs shawdow-0" onclick="editSoftware('+valueSoftware['id']+')"><i class="fa fa-pencil"></i></a>'
							+'<a class="btn btn-tbl-delete btn-xs btn-default shawdow-0" onclick="deleteSoftware('+valueSoftware['id']+')"><i class="fa fa-trash-o "></i></a>'
							+'</td>'
							+'</tr>';
			$("#tableSoftware").append(itenTable);
		});
}
	
function getAllSoftWare(){
	$.ajax({
		url: jscontext+"/admin/software/getAllSoftware",
		type:"GET",
		contentType:"application/Json",
		success:function(result){
			if(!result.errormessage){
				var software = result.data;
				if(software.length == 0){
					var htmlSoftware =	'<tr><td colspan="12" class="text-center text-danger">'+getLang(currentLocale,"No software for download")+'</td></tr>'
					$("#tableSoftware").append(htmlSoftware);
					
				}else{
					arraySoftwareList = software;
					drawSoftwareList(software);
				}
			}
		},
		error:function(xhr,e){
			console.log("ERROR:",e);
			var statusCode = xhr.status;
			if(statusCode == 403){
				windown.localhost = jscontext + "/login";
			}
		},
		done : function(e){
			console.log(e);
		}
	});
}

function deleteSoftware(id){
	var software = '';
	$.each(arraySoftwareList, function (indexSoftware, valueSoftware){
		if(valueSoftware["id"] == id){
			var upDate = new Date(valueSoftware["upDate"]).toISOString().slice(0,10);
			software = {
					"id" :  valueSoftware["id"],
					"type": valueSoftware["type"],
					"version" : valueSoftware["version"]	
				}
		}
	});
	
	$.ajax({
		url: jscontext + "/admin/software/deleteSoftware",
		type: "POST",
		data: JSON.stringify(software),
		contentType:"application/Json",
		success: function(result){
			if(!result.errormessage){
				swal({
			        title: "Delete Software Success!!!",
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				$("#tableSoftware").html("");
				getAllSoftWare();
			}else{
				swal({
			        title: "Error! Please again!",
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function hiddenModalEditSoftware(){
	$('#modalUploadSoftware').on('hidden.bs.modal', function () {
		$("#idSoftware").val("");
		$("#formEditSoftware")[0].reset();	
		requirement.setData('');
		releaseNote.setData('');
		$(".fileUpload div label").hide();
		$("#fileName").show();
	})
}

function editSoftware(id){
	$('#modalUploadSoftware').modal("show");
	$.each(arraySoftwareList, function(indexSoftware, valueSoftware){	
		if(valueSoftware["id"] == id){
			var upDate = new Date(valueSoftware["upDate"]).toISOString().slice(0,10);
			$("#modalUploadSoftware h4").text("Edit software");
			$("#typeSoftware").val(valueSoftware["type"] );
			$("#versionSoftware").val(valueSoftware["version"]);
			$("#upDate").val(upDate);
			requirement.setData(valueSoftware["requirement"]);
			releaseNote.setData(valueSoftware["releaseNote"]);
			$("#idSoftware").val(id);
			$("#fileName").hide();
			$(".fileUpload div").append('<label>'+valueSoftware["fileName"]+'</label>');
		}
	});
}


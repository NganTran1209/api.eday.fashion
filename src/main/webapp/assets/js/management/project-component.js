var arrayProjectComponentList = '';
var listDefinition = [];
$( document ).ready(function() {	
	getTrackers();
});

var validateDefinition = function(idForm, key) {
	  // for more info visit the official plugin documentation: 
	  // http://docs.jquery.com/Plugins/Validation

	  var form2 = $('#definitionNewValue'+idForm);
	  var error2 = $('.alert-danger', form2);
	  var success2 = $('.alert-success', form2);
	  form2.validate({
	      errorElement: 'span', //default input error message container
	      errorClass: 'help-block help-block-error', // default input error message class
	      focusInvalid: true, // do not focus the last invalid input
	      ignore: "",  // validate all fields including form hidden input
	      rules: {
	          itemName: {
	        	  required:{
	            		depends:function(){
	                        $(this).val($.trim($(this).val()));
	                        return true;
	                    }
	                },																																																																																																
	              minlength: 1
	              
	          },
	          
	      },
	      
	      messages: {
				itemName:{
					required:getLang(currentLocale,"Cannot be blank!")
				},
	        },

	      invalidHandler: function (event, validator) { //display error alert on form submit              
	          success2.hide();
	          error2.show();
	      },

	      errorPlacement: function (error, element) { // render error placement for each input type
	          var icon = $(element).parent('.input-icon').children('i');
	          icon.removeClass('fa-check').addClass("fa-warning");  
	          icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
	         
	      },

	      highlight: function (element) { // hightlight error inputs
	          $(element)
	              .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	          tooltipValidation();
	      },
	      success: function (label, element) {
	      	
	          var icon = $(element).parent('.input-icon').children('i');
	          $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	          icon.removeClass("fa-warning").addClass("fa-check");
	          tooltipValidation();
	      },

	      submitHandler: function (form) {
	          success2.show();
	          error2.hide();
	          createTracker(idForm,key);
	  	    var $alertas = $('#definitionNewValue'+idForm);
	  	    $alertas.validate().resetForm();
	  	    $alertas.find('.form-group').removeClass('has-success');
	  	    $alertas.find('.fa').removeClass('fa-check');
	      }
	      
	  });
	  return {
	  //main function to initiate the module
	      init: function () {
	
	          handleValidation1();
	          handleValidation2();
	      }
	  }
}

function updateGroupCode(id,color,name,key) {
	var status = ($('#'+id).is(':checked') ? 1 : 0);
	var tracker =[{
		"id" :id,
		"groupCode": key,
		"itemName" : name,
		"itemValue":name,
		"projectId":projectId,
		"status":status,
		"colorCode":color
	}];
    $.ajax({
		url : jscontext+"/createProjectComponent",
		type : "POST",
		data : JSON.stringify(tracker),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
			} 
		},
		error : function(request,error)
	      {	
				$(".preloader").fadeOut("slow");
	          //alert("Request: Can not save");
	          
	          var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can not save!"),	
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
				}
	      },
		done : function(e) {
			$(".preloader").fadeOut("slow");
			//console.log("DONE");
			 swal({
			        title: getLang(currentLocale,"Can not save!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
    		}		
    	});
    } 
function createTracker(idForm, key){
	var color = "";
	$(".btn-create-proCo").attr("disabled",true);
	if(key == "Status"){
		 color = $(".btn-choose-color").text().trim();
		 
	}
	var tracker =[{
			"id" :"",
			"groupCode": key,
			"itemName" : $("#itemName"+idForm).val(),
			"itemValue":$("#itemName"+idForm).val(),
			"projectId":projectId,
			"status":1,
			"colorCode":color
	}];
	$.ajax({
		url : jscontext+"/createProjectComponent",
		type : "POST",
		data : JSON.stringify(tracker),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: "Save Success!!!",
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				$(".btn-create-proCo").attr("disabled",false);
				getTrackers();
			} 
		},
		error : function(request,error)
	      {	
				$(".preloader").fadeOut("slow");
	          //alert("Request: Can not save");
	          
	          var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can not save!"),	
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
				}
	      },
		done : function(e) {
			$(".preloader").fadeOut("slow");
			//console.log("DONE");
			 swal({
			        title: getLang(currentLocale,"Can not save!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
		}		
	});
}
var listColor = ["#000000","#FFFFFF","#FF0000","#00FF00","#0000FF","#FFFF00","#00FFFF","#FF00FF","#C0C0C0","#808080","#800000","#008000","#800080","#008080","#000080"];
function getTrackers(){
	$("#contentCode").html("");
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getProjectComponent",
		type : "GET",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#list-trackers").html("");
				var projectComponent = result.data;
				var groupCode = {};
				
				$.each(projectComponent,function(index, value){
					let group = value["groupCode"];
					if(!groupCode[group]){
						groupCode[group] = [];
					}
					groupCode[group].push(value);
				});
				
				for(var key in groupCode){
					var idForm = key;
					var header = '<div class="contentGroupCode">';
					const group = groupCode[key];
					header +=	 `<div class="d-flex"><p>`+ key +`</p><button class="btn-success btn-addTracker mr-4 mt-2" id="`+idForm+`" onclick ="showFormNewTracker(this)" ><i class="fa fa-plus"></i></button></div>
								  <div class="box add-tracker position-relative " style="display:none" id="form-`+idForm+`"><div> 
								  <h4>New `+key+`</h4> <button class="close btn-close" onclick="hidenForm('`+idForm+`')"> <span aria-hidden="true">&times;</span></button> </div>
								  <form class="form-horizontal form-label-left form-addNew-common" action="javascript:void(0)" accept-charset="UTF-8" method="post" id="definitionNewValue`+idForm+`">`;
					
					if(key != 'Status'){
						header += ` <div class="row item form-group"> 
										<label  class="control-label col-md-3 col-sm-12 col-xs-3">`+getLang(currentLocale,"Value")+`</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="input-icon right">
												<i class="fa"></i>
												<input id="itemName`+idForm+`" class="form-control itemName" name ="itemName">
											</div>
										</div>
									</div>`;
					} else{
						header += `<div class="row item form-group"> 
										<label  class="control-label col-md-3 col-sm-12 col-xs-3">`+getLang(currentLocale,"Value")+`</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="input-icon right">
												<i class="fa"></i>
												<input id="itemName`+idForm+`" class="form-control itemName" name ="itemName">
											</div>
										</div>
									</div>
									<div class="row item form-group"> 
										<label  class="control-label col-md-3 col-sm-12 col-xs-3">`+getLang(currentLocale,"Color")+`</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="input-icon right ">
												<i class="fa"></i>
												<div class="dropdown">
													<button class="btn dropdown-toggle form-control btn-choose-color" type="button" data-toggle="dropdown" style="background:#FF0000"> #FF0000
													<span class="caret"></span></button>
													<ul class="dropdown-menu content-color">
														<li class="p-1 choose-color" >`
														$.each(listColor,function(index, value){
																header += ' <a style="background:'+value+';width: 32px;height: 32px; display: inline-block" at="'+value+'" id="color'+index+'" onclick="chooseColor(this)"></a>';
															})
					    header += 						`</li>
													</ul>
												</div>
											</div>
										</div>		
									</div>`;					
					}
					
					header += `<div class="row form-group justify-content-center"> 
											<button type="submit"  class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success button-sm btn-create-proCo" onclick="validateDefinition('`+idForm+`','`+key+`');"><i class="ti-check mr-2"></i>`+getLang(currentLocale,"Save")+`</button> 
											<button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm ml-3" onclick="hidenForm('`+idForm+`')"><i class="ti-close">&nbsp;</i>`+getLang(currentLocale,"Close")+`</button> 
										</div>
									</form>
								</div>
								<div class="card-body formCode pt-0 " id="list`+key+`">
								`;
					
					$("#contentCode").append(header);
					$.each(group,function(index, value){
						var listCode =  '<div class="proCo">'
										+	'<span class="floating" >'
										+  		'<input name="'+value["itemName"]+'" type="checkbox" value="'+value["id"]+'" class="mr-2" id="'+value["id"]+'" onclick = "updateGroupCode(\''+value["id"]+'\',\''+value["colorCode"]+'\',\''+value["itemName"]+'\',\''+key+'\')">'
										+ 	'</span><label id="pr-Co-'+value["id"]+'" onclick="updateColor(\''+value["id"]+'\',\''+value["colorCode"]+'\',\''+value["itemName"]+'\',\''+key+'\')">'+value["itemName"]+'</label>'
										+'<div id="update-color'+value["id"]+'"></div>'
					    			+'</div>';
						
						$("#list"+key).append(listCode);
						$("#pr-Co-"+value["id"]).css("color",value["colorCode"]);
						checkedItem(value["status"],value["id"]);
						$("#pr-Co-"+value["id"]).hover(function(e){
							$(this).css("color",e.type === "mouseenter"?"#505050":value["colorCode"]) ;
						});
					});
					$("#contentCode").append("</div></div></div>");
				}
			}
			listDefinition = [];
		    $('#contentTasks :checkbox:checked').each(function(i){
		    	listDefinition.push( $(this).val());
		    });
		},
		error : function(request,error)
	      {	
			$(".preloader").fadeOut("slow");
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
	      }
	});
}

function chooseColor(value){
	$(".btn-choose-color").text("");
	$(".btn-choose-color").css("background",$("#"+value.id).attr('at'));
	$(".btn-choose-color").text($("#"+value.id).attr('at'));
}

function showFormNewTracker(key) {
	$("#form-"+$(key).attr('id')).css("display","block");
}

function hidenForm(value){
	 $("#form-"+value).css("display","none");
	 $(".btn-choose-color").css("background","#e74c3c");
	 $(".itemName").val("");
}

function checkedItem(status, itemValue){
	if(status == 1){
		$("#"+ itemValue).prop('checked',true);
	}
}

function updateColor(id,color,name,key){
	if(key=="Status"){
		$("#listStatus .proCo").css("display","block");
		$("#update-color"+id).css("display","block");
		$("#update-color"+id).html("");
		var html = '';
		html +=`<div class="box add-tracker position-relative">
					<div> 
						<h4>Update Status</h4> <button class="close btn-close" onclick="hidenFormColor('`+id+`')"><span aria-hidden="true">×</span></button>
					</div>
					<form>
						<div class="row item form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-3">Color</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<button class="btn dropdown-toggle form-control" id="btn-color-status`+id+`" type="button" data-toggle="dropdown" style="background:`+color+`"> `+ color + 
									`<span class="caret"></span></button>
									<ul class="dropdown-menu content-color">
										<li class="p-1 choose-color" >`;
										$.each(listColor,function(index, value){
											html +=` <a style="background:`+value+`;width: 32px;height: 32px; display: inline-block" at="`+value+`" id="color-status`+index+`" onclick='chooseColorStatus("`+id+`","`+index+`")'></a>`;
											})
								html+=	`</li>
									</ul>
							</div>
						</div>
						<div class="row form-group justify-content-center"> 
							<button type="button"  class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success button-sm" onclick='updateColorS("`+id+`","`+key+`","`+name+`");'><i class="ti-check mr-2"></i>`+getLang(currentLocale,"Save")+`</button> 
							<button  type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default button-sm ml-3" onclick="hidenFormColor('`+id+`')"><i class="ti-close">&nbsp;</i>`+getLang(currentLocale,"Close")+`</button> 
						</div>
					</form>
				</div>`;
		$("#update-color"+id).append(html);
	} 
}

function hidenFormColor(id){
	 $("#update-color"+id).css("display","none");
	 $("#listStatus .proCo").css("display","inline-block");
}

function chooseColorStatus(id, index){
	$("#btn-color-status"+id).text("");
	$("#btn-color-status"+id).css("background",$("#color-status"+index).attr('at'));
	$("#btn-color-status"+id).text($("#color-status"+index).attr('at'));
}

function updateColorS(id,key,name){
	var statusC =[{
		"id" :id,
		"groupCode": key,
		"itemName" : name,
		"itemValue":name,
		"projectId":projectId,
		"status":($('#'+id).is(':checked') ? 1 : 0),
		"colorCode":$("#btn-color-status"+id).text(),
	}];
	$.ajax({
		url : jscontext+"/createProjectComponent",
		type : "POST",
		data : JSON.stringify(statusC),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: "Save Success!!!",
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
			} 
			getTrackers();
		},
		error : function(request,error)
	      {	
				$(".preloader").fadeOut("slow");
	          //alert("Request: Can not save");
	          
	          var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can not save!"),	
				        type: "warning",
				        timer: 3000,
				        showConfirmButton: false
				    });
				}
	      },
		done : function(e) {
			$(".preloader").fadeOut("slow");
			//console.log("DONE");
			 swal({
			        title: getLang(currentLocale,"Can not save!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
		}		
	});
}

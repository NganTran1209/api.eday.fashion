var stompClient = null;
//var testsuitels1 ;
var listGraphic;
var lsProject;
$( document ).ready(function() {
	connect();
	getProjects("false", function(project){
	});
	checkBreadcrumb();
	loadRunning(); 
	setOptionSelectUserEdit();
	validateEditProfileMember();
	$("#modalEditMember").on('show.bs.modal', function () {
		$("#loadDataProfile").show();
		$("#modalEditMember .form-group").removeClass("has-success").removeClass("has-error");
		$("#modalEditMember .form-group .input-icon i").removeClass('fa-warning').removeClass('fa-check').removeAttr("data-original-title");
		$("#informationUser").slideDown();
		$("#infoPasswordUser").slideUp();
		
		$("#loadDataProfile").css("z-index","10056");
		loaddataProfile();
//		$("#formeditMember .row.item.form-group .form-control").prop("disabled",true);
	});
	getUser();
});

function loadGraphic(){
	$.ajax({
		url : jscontext +"/project/graphicTestcaseAutAndMan",
		type : "GET",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				listGraphic = JSON.parse(result.data);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
}

function connect() {
    var socket = new SockJS(jscontext +'/wsrunning');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        sendTest();
        stompClient.subscribe( '/user/bot_notify/job', function (jobs) {
        	changeRunningJob(jobs);
        	loadRunning();
        	finishRunning(jobs);
        });
    });
}

function sendTest() {
    stompClient.send("/bot/listen", {}, "Hi server listen");
}

function checkBreadcrumb(){
	if(glProjectId == "" || glProjectId == undefined || glProjectId == null){
		$(".nav.page-title-breadcrumb li.breadcrumb-child").addClass("hidden");
		$("#reloadProject").hide();
	}else{
		$("#reloadProject").show();
		if(glProjectId == "" || glProjectId == undefined || glProjectId == null){
			$(".nav.page-title-breadcrumb li.breadcrumb-child.projectNameItem").removeClass("hidden").show();
			var html = '<li class="breadcrumb-child projectNameItem" style="display:block">' 
						+'<div class="nav-item px-3">'
						+getLang(currentLocale,'Overview') 
						+'</div>'
						+'</li>';
			$(".nav.page-title-breadcrumb li.breadcrumb-child.projectNameItem .back-history").attr("href",jscontext+ "/project/");
			$(".nav.page-title-breadcrumb li.breadcrumb-child:nth-child(3)").after(html).show();  
			
		}else{
			$(".nav.page-title-breadcrumb li.breadcrumb-child").removeClass("hidden").show(); 
		}
	}
}

function loadRunning(){
	$.ajax({
		url : jscontext +"/project/getExecutingJob",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				drawRunningPart(result.data);
				if($(".loading-important").length > 0){
				    $('.loading-important').fadeOut();
				}
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
}
function getResult(projectId,testsuites,id){
	window.location.href = jscontext + "/project/"+projectId+"/"+"runnings?jobScheduleId=" + id;
	//location.reload();
}

function drawRunningPart(executingJobs){
	var listItemRunning ="" 
	for(var i =0 ; i < executingJobs.length; i++){
		listItemRunning += '<li><a href="#" class="py-2" onclick="getResult(\''+ executingJobs[i]["projectId"] +'\', \''+ executingJobs[i]["testsuite"] +'\', \''+ executingJobs[i]["id"] +'\');"><i class="fa fa-caret-right my-auto"></i>'+getLang(currentLocale,"User")+':['
						+ executingJobs[i]["username"] 
						+ '],'+getLang(currentLocale,"Project")+' : '+ executingJobs[i]["projectName"] +', 	'
						+ getLang(currentLocale,"Testsuite")+': ' + executingJobs[i]["testsuite"]
						+ '<br>'+ executingJobs[i]["runningName"] +
						' </a></li>';
	}
	var runningLabel= executingJobs.length;
	if (runningLabel == 0){
		runningLabel ="";
		$("#header_inbox_bar_running").hide();
	}else {
		$("#header_inbox_bar_running").show();
	}

	$("#runningLabel1").text(runningLabel);
	$("#runningLabel2").text(runningLabel);
	
	if($("#runningBox ul.dropdown-menu-list").length > 0){
		$("#runningBox ul.dropdown-menu-list")[0].innerHTML = listItemRunning;
	}
	$(".runningMess")[0].innerHTML = listItemRunning;
}

function changeRunningJob(job){
	if (typeof htmlDrawScheduler === "function") { 
		//var theJob = $("#listRunningJobSchedule").find('div[job-scheduler-id = "'+job.id +'"]');
		var  jobHTML = htmlDrawScheduler(JSON.parse(job["body"]),"#listRunningJobSchedule");
		var idJob = JSON.parse(job.body).id;
		var theJob = $("#listRunningJobSchedule").find('div[job-scheduler-id = "'+ idJob +'"]');
		if(theJob.length > 0){
			$(theJob[0]).html($(jobHTML).html());
		} else {
			$("#listRunningJobSchedule").prepend(jobHTML);
		}
	}
}

function finishRunning(jobs){
	var jsonMessage = JSON.parse(jobs.body);
	var statusRunning = jsonMessage.status;

	if(statusRunning !== undefined){
		if(statusRunning == 'SUCCESS' || statusRunning == 'FAILURE'){
			 $('#reRun_'+jsonMessage["id"]+'_Sche').removeAttr('hidden');
			 $('#deTail_'+jsonMessage["id"]+'_Sche').removeAttr('hidden');
			var countNotification = $("#numberCountNotifi").text();
			var countNotifiPlus = countNotification =+ 1 ;
			$("#numberCountNotifi").html(countNotifiPlus);
			var alertBubbles = $("#dropdown-alert-running");
			if(statusRunning == 'SUCCESS'){
				alertBubbles.addClass("bg-success");
			}else if(statusRunning == 'FAILURE' || statusRunning == 'ABORTED'){
				alertBubbles.addClass("bg-warning");
			}
			var itemMessageRunning = '<a href="'+jscontext+"/project/"+projectId+"/"+jsonMessage.testsuite+"/result/"+jsonMessage.runningName+'">'
									+ '<div class="w-100 d-flex">- '+getLang(currentLocale,"User")+':['
									+ jsonMessage. username
									+ ']</div>'
									+ '<div class="w-100 d-flex">- '+getLang(currentLocale,"Project")+':'+ jsonMessage.projectId+'</div>'
									+ '<div class="w-100 d-flex">- '+getLang(currentLocale,"Testsuite")+':' +jsonMessage.testsuite+'</div>'
									+ '<div class="w-100 d-flex">- '+getLang(currentLocale,"Running Name")+':'+ jsonMessage.runningName+'</div>'
									+ '<div class="w-100 d-flex">- '+getLang(currentLocale,"Status")+':<span class="font-weight-bold">'+statusRunning+'</span></div>'
									+ '</a>';
			$("#messageNotifications").html(itemMessageRunning);
			
			alertBubbles.addClass("show"); 
			$(window).click(function() {
				$("#dropdown-alert-running").removeClass("show");
			});
			
			$("#header_inbox_bar_running a").click(function() {
				$("#dropdown-alert-running").removeClass("show");
			});

			$('#dropdown-alert-running').click(function(event){
				$("#dropdown-alert-running").addClass("show");
			    event.stopPropagation();
			});
			
			$("#header_notification_bar>a").click(function(){
				$("#dropdown-alert-running").removeClass("show");
			});
		}
	}
	
}

function collsapseCardBody(Isthis){
	var idParentColspase = $(Isthis).parents().eq(2).attr('id');
	$('#'+idParentColspase+ " .card-body").toggleClass("collapse-height"); 
	$('#'+idParentColspase+ " .card-body").slideToggle("fast") ;
}

function getProjects(status, callback){
	$.ajax({
		url : jscontext +"/project/list",
		type : "get",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var projectJson = result.data;
				var projects = JSON.parse(projectJson);
				lsProject = projects;
				if(status == "true"){
					countProject(projects);
					drawProject(projects);
					drawProjectSlidebar(projects);
				} else {
					drawProjectSlidebar(projects);
				}
				callback(projects);
				//getProjectTestSuitesGl();
				Layout.init();
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
function actionLeftSlide(Isthis){
           var that = $(Isthis).closest('.nav-item').find('.nav-toggle-custome');
           var parent =that.parent().parent();
           var the = that;
           var menu = parent.parent();
           var sub = that.parent().next();
           var slideOffeset = -200;
           var autoScroll = menu.data("auto-scroll");
           $('.arrow', the).toggleClass("open");
           the.parents().eq(1).toggleClass("open");
           sub.slideToggle(function() {
           	if ($('body').hasClass('sidemenu-container-fixed')) {
					menu.slimScroll({
						'scrollTo': (the.position()).top
					});
				} else {
					App.scrollTo(the, slideOffeset);
				}
           });

  }

function countProject(projects){
	var totalProject = 0;
	for(var i = 0; i < projects.length; i++){
		totalProject +=1;
		/*if(projects[i].projects !=null && projects[i].projects.deletedFlag == 0 ){}*/
	}
	if($(".countProject").length > 0 ){
		$(".countProject").text("("+totalProject+" "+getLang(currentLocale,"Project")+")"); 
	}
	
}

function drawProjectSlidebar(projects){
	if($(".sidemenu-container .sidemenu.sidemenuListProject").length > 0){
	$(".sidemenu-container .sidemenu.sidemenuListProject").html("");
	for (var i=0;i< projects.length ; i++)	{
		 var projectId=projects[i].id;
		    var description =projects[i].description;
		    var projectsName=projects[i].name;
		    var color=""; 
		    
			var items ='<li class="nav-item '+(projectId == glProjectId ? 'active-page':'')+'" ' 
				+ (color != null ? 'style=border-color:'+color+';'+(projectId == glProjectId ? 'background:'+color+'':'')+'':'')+'>'
				+ '<span class="nav-link" style="'+(projectId == glProjectId ? 'background:'+color+'':'')+'">'
				+ '<i class="icon-folder-alt ml-0 d-flex"></i>'
				+ '<a class="title" href="'+jscontext +'/project/'+projectId+'/overview">'+projectsName+'</a>'
				+ '<span class="nav-link nav-toggle-custome position-relative float-right" onclick="">'
				+ '<span class="arrow d-block" style="display:none" id="arrow_'+projectsName+'"></span>'
				+ '</span>'
				items	+= '</span>'
				+ '<ul class="sub-menu p-0" id="id_'+projectsName+'"></ul>'
				+ '</li>';
				$(".sidemenu-container .sidemenu.sidemenuListProject").append(items);
//				loadItemTestsuitesChild(projects[i],projectsName,projectId);
			}
	}
	
}

// func draw project
function drawProject(projects){
	$("ul.projects.root.list-unstyled").empty();
	for(var i = 0 ; i < projects.length; i++)	
	{
		var items = '';
		var dataChart = [];	
	    var projectId = projects[i].id;
	    var description =projects[i].description;
	    var projectsName = projects[i].name;
	    var countDegrade = 0;
	    	
	   
		
		items +='<li class="root">'
		+ '<div class="root">'
			+ '<a class="project parent my-project" href="'+jscontext +'/project/'+	projectId+'/overview">'
				+ '<div class="box-card-project">'
					+ '<div class="name-project ">'+projectsName+'</div>'
						+ '<small class="project parent my-project text-muted m-0 font-italic">'+description+'</small>'		
					+ '</div>'
					+' <div class="row" >'
					+'<div class="col-md-12" id="frameIssue'+projectId+'" style="display: none">'
						+'<div class="card card-box card-issue">'
						+'<div class="card-head card-head-icon">'
		             		+'<div class="col-md-2">'
		             			+'<h4 >Issue Status</h4>'
		             		+'</div>'
						+'<div class="col-md-10 legend headerLe w-100 text-right mr-3 legend-issue" id="legendIssue'+projectId+'"></div>'
						
						+'</div>'
						+'<div class="card-body no-padding height-9 body-issue">'
		             		+'<div class="text-danger" style="text-align: center;" id="issueCircleEmpty"></div>'
		             		+'<div id="progressIssueOverView'+projectId+'" class="row"> </div>'
		             	+'</div>'
		             	+'</div>'
		             +'</div>'
                +'</div>'
//					+ '<div class="row row-testing" >'
//						+'<div class="col-md-12">'
//						+'<div class="card-head card-head-icon ">'
//							+'<h4 class="col-md-4">Testing Status</h4>'
//						+'</div>'
//						+'</div>'
//						+ '<div class="element-list-number-total ">'
//					 	    + '<p class="item-count-project mb-2" >-   '+getLang(currentLocale, "Total testsuites")+' : '+testsuite+'</p>'
//					 	    + '<p class="item-count-project mb-2" >-   '+getLang(currentLocale, "Total testcases")+' : '+totalTestcase+'</p>'
//				 	    + '</div>'
//						+ '<div class="element-chart chart-legend-middle font-chart">'
//							+ '<div class="box-chart font-middle" id="creating'+projectId+'"></div>'
//							+ '<div class="legend " id="elementChart'+projectId+'">'
//							
//						 	+ '</div>' 
//						+'</div>' 
//						+ '<div class="element-chart chart-legend-middle font-chart">'
//							+ '<div class="box-chart font-middle" id="chartManualAuto'+projectId+'"></div>'
//							+ '<div class="legend " id="elementManualAuto'+projectId+'"></div>' 
//						+'</div>' 
//					+ '</div>'
//					
//					
//			    + '</div>'  
			+ '</a>'
			+ '<div class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-project my-auto mr-0 ml-2 float-right"'
			+ 'onclick="cofirmDeleteProject(\''+projectsName+'\',\''+projectId +'\');"><i class="fa fa-trash-o"></i></div>'
			
		+ '</div>'
		+ '</li>';
		
		
//		dataChart.push(testcasePass, testcasetFail, countDegrade,testcaseNotTest);
		
		$("ul.projects.root.list-unstyled").append(items);
//		getListStatusById(projectId, function(listStatus){
//			drawChartIssue(projectId, listStatus);
//			
//		});
//		drawChartProjectList(dataChart,projectId);
//		drawChartManualAuto(projectId);
		
		
		
	}
	$(".preloader").fadeOut("slow");
	$(".preloader").css("z-index"," 1040");
}

function drawChartProjectList(dataChart,id){
	var chartData = {
	    CC: [{
	      code: id,
	      datasets: [{
		        data:dataChart,
		        backgroundColor:["#4CAF50","#CB371D","#AB47BC","#FFC107"],
		        label: 'Testcase Project'
		    }],
		    labels: [
		    	getLang(currentLocale,"Testcase Passed"),
		    	getLang(currentLocale,"Testcase Fail"),
		    	getLang(currentLocale,"Testcase Degrade"),
		    	getLang(currentLocale,"Test Not Tested")
  	    	    ]
	    }]
	  };
	chartData.CC.forEach(function(data, index){
	    var canvas = document.createElement('canvas');
	    canvas.setAttribute("width", "150");
	    canvas.setAttribute("height", "150");
	    canvas.setAttribute("id", 'chart' + data.code);
        chartId = 'chart' + data.code;
	    var idCanvasChart = document.getElementById("creating"+id);
	    if($(idCanvasChart).length > 0){
	    	idCanvasChart.appendChild(canvas);
		    var context = document.getElementById(chartId).getContext('2d');
		   // var drawChartProjectList = document.getElementById(chartId).getContext("2d");
			  
		    if(dataChart.toString() == "0,0,0,0"){
		    	var configCreatProCircle = {
					type: 'pie',
					data: data,
					data: {
			    	  datasets: [{
			    	        data:[1],
			    	        backgroundColor:["#9E9E9E"],
			    	        label: 'Testcase Project'
			    	    }],
			    	    labels: [
			    	    	getLang(currentLocale,"Testcase"),
			    	    	getLang(currentLocale,"Testsuite"),
			    	    ]
					},
					options: {
						elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
					    tooltips: {enabled: false},
					    hover: {mode: null},
					    legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  				},
					  }
			    }
		    }else{
		    	var configCreatProCircle = {
		  	      type: 'pie',
		  	      data: data,
		  	      data: {
		  	    	    datasets: [{
		  	    	        data:dataChart,
		  	    	        backgroundColor:["#4CAF50","#CB371D","#AB47BC","#FFC107"],
		  	    	        label: 'Testcase Project'
		  	    	    }],
		  	    	    labels: [
		  	    	    	getLang(currentLocale,"Testcase Passed"),
		  	    	    	getLang(currentLocale,"Testcase Fail"),
		  	    	    	getLang(currentLocale,"Testcase Degrade"),
		  	    	    	getLang(currentLocale,"Test Not Tested")
		  	    	    ]
		    	
		  	    	},
		  	    	 options: {
		  	    		elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
		  				responsive: true,
		  				scaleBeginAtZero: false,
		  				legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  		             
		  				},
		  				events: true,
		  				animation: {
		  					duration: 500,
		  					easing: "easeOutQuart",
		  					onComplete: function () {
		  					  var ctx = this.chart.ctx;
		  					  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		  					  ctx.textAlign = 'center';
		  					  ctx.textBaseline = 'bottom';
		  					
		  					  this.data.datasets.forEach(function (dataset) {
		  					
		  						for (var i = 0; i < dataset.data.length; i++) {
		  						  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		  						      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
		  						      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
		  						      start_angle = model.startAngle,
		  						      end_angle = model.endAngle,
		  						      mid_angle = start_angle + (end_angle - start_angle)/2;
		  						
		  						  var x = mid_radius * Math.cos(mid_angle);
		  						  var y = mid_radius * Math.sin(mid_angle);
		  						
		  						  ctx.fillStyle = '#fff';
		  						  if (i == 3){ // Darker text color for lighter background
		  						    ctx.fillStyle = '#fff';
		  						  }
		  						  ctx.font = "11px sans-serif";
		  						  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
		  						  if(dataset.data[i] > 0 ){
			  					  	   ctx.fillText(dataset.data[i], model.x + x, model.y + y);
			  						  // Display percent in another line, line break doesn't work for fillText
			  						  ctx.fillText(percent, model.x + x, model.y + y + 15);
		  						  }
		  						}
		  					  });               
		  					}
		  				},
		  				tooltips: {
		  					enabled: true,
		  			        callbacks: {
		  			            label: function(tooltipItem, data) {
		  			                return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
		  			                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
		  			                });
		  			            }
		  			        }
		  			    }
		  	    	}
		  	    }
		    }
		    var chartCreatingProgressCircle = new Chart(context, configCreatProCircle);
			$("#elementChart"+id).append(chartCreatingProgressCircle.generateLegend());
	    }
	});
	
}


function drawChartManualAuto(id){
	var dataChart=[];
	$.each(listGraphic, function(index,vals){
		if(vals["ProjectId"] == id){
			var automation = 0;
			var manual = 0;
			if(vals["manual"] != undefined){
				manual = vals["manual"];
			}
			
			if(vals["automation"] != undefined){
				automation = vals["automation"];
			}
			
			dataChart.push(automation);
			dataChart.push(manual);
		}
	})
	var chartData = {
	    CC: [{
	      code: id,
	      datasets: [{
		        data:dataChart,
		        backgroundColor:["red","darkseagreen"],
		        label: 'Testcase Project'
		    }],
		    labels: [
		    	getLang(currentLocale, "Automation Testcase"),
		    	getLang(currentLocale,"Manual Testcase"),
		    ]
	    }]
	  };
	chartData.CC.forEach(function(data, index){
	    var canvas = document.createElement('canvas');
	    canvas.setAttribute("width", "150");
	    canvas.setAttribute("height", "150");
	    canvas.setAttribute("id", 'chartMA' + data.code);
        chartId = 'chartMA' + data.code;
	    var idCanvasChart = document.getElementById("chartManualAuto"+id);
	    if($(idCanvasChart).length > 0){
	    	idCanvasChart.appendChild(canvas);
		    var context = document.getElementById(chartId).getContext('2d');
		   // var drawChartProjectList = document.getElementById(chartId).getContext("2d");
			  
		    if(dataChart.toString() == "0,0"){
		    	var configCreatProCircle = {
					type: 'pie',
					data: data,
					data: {
			    	  datasets: [{
			    		  	data:[1],
			    	        backgroundColor:["#9E9E9E"],
			    	        label: 'Testcase Project'
			    	    }],
			    	    labels: [
			    	        "N/A",
			    	    ]
					},
					options: {
						elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
					    tooltips: {enabled: false},
					    hover: {mode: null},
					    legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  				},
					  }
			    }
		    }else{
		    	var configCreatProCircle = {
		  	      type: 'pie',
		  	      data: data,
		  	      data: {
		  	    	    datasets: [{
		  	    	    	data:dataChart,
		  	    	        backgroundColor:["#4CAF50","#FFC107"],
		  	    	        label: 'Testcase Project'
		  	    	    }],
		  	    	    labels: [
		  	    	    	getLang(currentLocale,"Automation Testcase"),
		  	    	    	getLang(currentLocale,"Manual Testcase"),
		  	    	    ]
		    	
		  	    	},
		  	    	 options: {
		  	    		elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
		  				responsive: true,
		  				scaleBeginAtZero: false,
		  				legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  		             
		  				},
		  				events: true,
		  				animation: {
		  					duration: 500,
		  					easing: "easeOutQuart",
		  					onComplete: function () {
		  					  var ctx = this.chart.ctx;
		  					  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		  					  ctx.textAlign = 'center';
		  					  ctx.textBaseline = 'bottom';
		  					
		  					  this.data.datasets.forEach(function (dataset) {
		  					
		  						for (var i = 0; i < dataset.data.length; i++) {
		  						  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		  						      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
		  						      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
		  						      start_angle = model.startAngle,
		  						      end_angle = model.endAngle,
		  						      mid_angle = start_angle + (end_angle - start_angle)/2;
		  						
		  						  var x = mid_radius * Math.cos(mid_angle);
		  						  var y = mid_radius * Math.sin(mid_angle);
		  						
		  						  ctx.fillStyle = '#fff';
		  						  if (i == 3){ // Darker text color for lighter background
		  						    ctx.fillStyle = '#fff';
		  						  }
		  						  ctx.font = "11px sans-serif";
		  						  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
		  						if(dataset.data[i] > 0 ){
		  					  	   ctx.fillText(dataset.data[i], model.x + x, model.y + y);
		  						  // Display percent in another line, line break doesn't work for fillText
		  						  ctx.fillText(percent, model.x + x, model.y + y + 15);
		  						}
		  						}
		  					  });               
		  					}
		  				},
		  				tooltips: {
		  					enabled: true,
		  			        callbacks: {
		  			            label: function(tooltipItem, data) {
		  			                return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
		  			                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
		  			                });
		  			            }
		  			        }
		  			    }
		  	    	}
		  	    }
		    }
		    var chartCreatingProgressCircle = new Chart(context, configCreatProCircle);
			$("#elementManualAuto"+id).append(chartCreatingProgressCircle.generateLegend());
	    }
	});
	
}

function getListStatusById(projectId, callback){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/getListStatus",
		type : "GET",
		data : "",
		async: false,
		contentType : "application/json",
		
		success : function(result) {
			if (!(result.errormessage) ) {
				var listStatusId = result.data;
				callback(listStatusId);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function drawChartIssue(projectId, listStatus){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/listIssueAdd",
		type : "GET",
		data : "",
		async: false,
		contentType : "application/json",
		
		success : function(result) {
			if (!(result.errormessage) ) {
				listIssue = JSON.parse(result.data);
				pieByIssueOverView(listIssue, projectId, listStatus);
				if(listIssue.length>0){
					document.getElementById("frameIssue"+projectId).style.display = "block";
				}
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
	
}

function loadItemTestsuitesChild(testsuitelist,idItem, projectId){
	for(var key in testsuitelist){
		if (key == "ProjectPTS"){
			$("#id_"+idItem).closest(".nav-item ").find(".nav-toggle-custome").hide();
		}else{
			$("#id_"+idItem).closest(".nav-item ").find(".nav-toggle-custome").show();
		}
	 }
   var htmlItemTestsuites = "";
   //$("#arrow_"+idItem).show();  
   $("#id_"+idItem).empty();
   for(var key in testsuitelist){
	   if(key != "ProjectPTS"){
		   htmlItemTestsuites += '<li class="nav-item">'
				+ '<a href="'+jscontext +'/project/'+projectId+"/"+key+'" class="nav-link ">'
				+ '<span class="title">'+key+'</span>'
				+ '</a>'
				+ '</li>';
	   }
		
   }
   $("#id_"+idItem).append(htmlItemTestsuites);	
}


//Sync project with scm server
function syncProject(e){
	$(".preloader-main").fadeIn("slow");
	$.ajax({
		url : jscontext + "/project/"+ projectId+"/syncProject",
		type : "POST",
		data : "",
		contentType : "application/json",
		success : function(result) {
			$(".preloader-main").fadeOut("slow");
			if (!(result.errormessage) ) {
				var bodyListTestsuiteElm = $("#bodyTestsuiteRecord");
				if(bodyListTestsuiteElm.length > 0){
					loadListRs(result.data);
				}
				
			}else {
				swal({
				       title: "",
				       text: getLang(currentLocale,"Sync project fail:") + result.errormessage,
				       type: "warning",
				       timer: 2000,
			        	showConfirmButton: false
				   });
//				window.location= jscontext + "/login";
			}
      },
      error : function(request,error)
      {
    	  $(".preloader-main").fadeOut("slow");
    	  
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({ 
				       title: "",
				       text: "Sync project fail:" + error,
				       type: "warning",
				       timer: 2000,
			        	showConfirmButton: false
				   });
			}
      }
	
	});
}

function setOptionSelectUserEdit(){
	var valOptionCountry = $("#countryProfile").attr("data-select");
	$("#countryProfile option[value='"+valOptionCountry+"']").prop("selected",true);
	var valbusinessRolesProfile = $("#businessRolesProfile").attr("data-select");
	$("#businessRolesProfile option[value='"+valbusinessRolesProfile+"']").prop("selected",true);
}


var validateEditProfileMember= function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#formeditMember');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	firstNameProfile: {
        		required: {
        			depends:function(){
	    	            $(this).val($.trim($(this).val()));
	    	            return true;
        			}
        		},
                maxlength: 50
            },
            lastNameProfile: {
            	required: {
        			depends:function(){
	    	            $(this).val($.trim($(this).val()));
	    	            return true;
        			}
        		},
                maxlength: 50
            },
            companyProfile: {
                required: true,
                maxlength: 200
            },
            countryProfile: {
                required: true
            },
            businessRoles: {
                required: true
            }
        },
        messages: {
        	firstNameProfile: {
                required: getLang(currentLocale,"Cannot be blank!"),
                maxlength: getLang(currentLocale,"Please enter at less 50 characters!"),
            },
            lastNameProfile: {
                required: getLang(currentLocale,"Cannot be blank!"),
                maxlength: getLang(currentLocale,"Please enter at less 50 characters!"),
            },
            companyProfile: {
                required: getLang(currentLocale,"Cannot be blank!"),
                maxlength: getLang(currentLocale,"Please enter at less 200 characters!"),
            }
        },
        
//        messages: {
//        	projectId: "Project Id cannot be blank!!!",
//          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            form2.validate().resetForm();
            form2.find('.form-group').removeClass('has-success');
            form2.find('.fa').removeClass('fa-check');
//            editProfile();
        }
    });
    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}

function loaddataProfile(){
	var url = "";
    var role = authoRole.replace("[","").replace("]","");
    if (role == "ROLE_ADMIN") {
    	url = jscontext +"/admin/user/getAll";
    	
    } else {
    	url = jscontext +"/user/user/getAll";
    }
	var userNameProfile = $("#userNameProfile").val();
	$.ajax({
		url : url,
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var userListArray =  result.data;
				$.each(userListArray, function(indexUser, valueUser){
					if(valueUser['username'] == userNameProfile){
						$("#firstNameProfile").val(valueUser['firstname']);
						$("#lastNameProfile").val(valueUser['lastname']);
						$("#companyProfile").val(valueUser['company']);
						$("#countryProfile").val(valueUser['country']);
						$("#businessRolesProfile").val(valueUser['position']);
						$("#EditlanguageSetingUser").val(valueUser['language']);
					}
				});
				
			} else {
			}
			$("#loadDataProfile").fadeOut("slow");
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			 var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function editProfile(){
	if ($('#formeditMember').valid() != true){
		return;
	}
	$("#saveEditProfile").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveEditProfile").prop("disabled", true);
	var username = $("#userNameProfile").val();
	var firstname = $("#firstNameProfile").val();
	var lastname = $("#lastNameProfile").val();
	var company = $("#companyProfile").val();
	var country = $("#countryProfile").val();
	var businessRole = $("#businessRolesProfile").val();
	var language = $("#EditlanguageSetingUser").val();
	
	var theUser = {
			"username":username, 
			"firstname":firstname, 
			"lastname":lastname, 
			"country":country, 
			"company":company, 
			"position":businessRole,
			"language":language
	}
	
	$.ajax({
		url : jscontext + "/editProfile",
		type : "POST",
		data : JSON.stringify(theUser),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalEditMember").modal("hide");
				$("#saveEditProfile").html("<i class='ti-check'></i> Save");
				$("#saveEditProfile").prop("disabled", false);
				if($("#tableUserManagement").length > 0){
					var table = $("#listUserProject").DataTable();
					table.destroy();
					getListUser();
				}
				swal({
			        title: getLang(currentLocale,"Your profile is has been updated!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				location.reload();
			}
      },
      error : function(request,error)
      {
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
      }
	});
}


function actionChangPassword(){
	$("#editProfile").attr("hidden", false);
	$("#changePasss").attr("hidden", true);
	$("#informationUser").slideUp();
	$("#infoPasswordUser").slideDown();
}

function actionEditProfile(){
	$("#editProfile").attr("hidden", true);
	$("#changePasss").attr("hidden", false);
	$("#informationUser").slideDown();
	$("#infoPasswordUser").slideUp();
}

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}
var validateChangePass = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#changPassUser');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    $.validator.addMethod("checkPassConfirm",function(value, element, regexp) {
    	//func check match Pass is exist
        var password = $("#newPassword").val();
        var passconfirmation = $("#confirmNewPass").val();
		if(passconfirmation != password){
			var $alertas = $('#changPassUser');
			error2.show();
			success2.hide();
			$alertas.validate().resetForm();
			$alertas.find('.form-group.bad.passConfirm').removeClass('has-success');
	        $alertas.find('.passConfirm').addClass('has-error');
	        $alertas.find('.fa.confirmPass').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"Your password and confirmation password do not match!!!"));
	        return false;
		} else {
			return true;
		}
    });
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	oldPassword: {
                required: true,
            },
            newPassword: {
                required: true,
                regex:"^(?=.*[A-Z])(?=.*[0-9])^([a-zA-Z0-9!@#$%&*]{6,15})$",
                checkPassConfirm: true,
            },
            confirmNewPass:{
            	required: true,
            	regex:"^(?=.*[A-Z])(?=.*[0-9])^([a-zA-Z0-9!@#$%&*]{6,15})$",
            	checkPassConfirm: true,
            },
            
        },
        
        messages: {
        	oldPassword:{
        		required:getLang(currentLocale,"Cannot be blank!"),
			},
			newPassword:{
				required:getLang(currentLocale,"Cannot be blank!"),
				regex: getLang(currentLocale,"Password must contain at least six characters and most fifteen characters, at least one number and uppercase letters"),
				checkPassConfirm: getLang(currentLocale,"Your password and confirmation password do not match!!!"),
			},
			confirmNewPass:{
				required:getLang(currentLocale,"Cannot be blank!"),
				regex: getLang(currentLocale,"Password must contain at least six characters and most fifteen characters, at least one number and uppercase letters"),
				checkPassConfirm: getLang(currentLocale,"Your password and confirmation password do not match!!!"),
			},
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
        },

        errorPlacement: function (error, element) { // render error placement for each input type
        	var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
			
            
			// func sign up
			changePasswordUser();
    	    var $alertas = $('#formeditMember');
    	    $alertas.validate().resetForm();
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
    	    
        }
        
    });
    return {
    //main function to initiate the module
        init: function () {
            handleValidation1();
            handleValidation2();
        }
    }
}

//func reset Form of modal changePassword
function resetFormChangePass() {
	$('#modalEditMember').on('hidden.bs.modal', function() {
		$("#editProfile").attr("hidden", true);
		$("#changePasss").attr("hidden", false);
		document.getElementById("changPassUser").reset();
	    var $alertas = $('#changPassUser');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

function changePasswordUser(){
	if ($('#changPassUser').valid() != true){
		return;
	}
	$('#changPassUser').find('.passOld').removeClass('has-success');
	$('.passOld').find('i').hide();
	$("#saveChangePassword").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveChangePassword").prop("disabled", true);
	var username = $("#userNameProfile").val();
	var olPasswordUser = $("#oldPassword").val();
	var newPasswordUser = $("#newPassword").val();
	var confirmPasswordUser = $("#confirmNewPass").val();
	var jsonChangePassword = {
			"username":username, 
			"oldPassword":olPasswordUser, 
			"password":newPasswordUser, 
			"confirmPass":confirmPasswordUser
	}
	var url = "";
	var role = authoRole.replace("[","").replace("]","");
    if (role == "ROLE_ADMIN") {
    	url = jscontext + "/admin/user/editPassword";
    } else {
    	url = jscontext + "/user/user/editPassword";
    }

	$.ajax({
		url : url,
		type : "POST",
		data : JSON.stringify(jsonChangePassword),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$('#changPassUser').find('.passOld').addClass('has-success');
			 	$('.passOld').find('i').show();
				$("#modalEditMember").modal("hide");
				$("#saveChangePassword").html("<i class='ti-check'></i> Save");
				$("#saveChangePassword").prop("disabled", false);
				swal({
			        title: getLang(currentLocale,"Successful password update!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				window.location= jscontext + "/login";
			} else{
				$("#saveChangePassword").html("<i class='ti-check'></i> Save");
				$("#saveChangePassword").prop("disabled", false);
				$('.passOld').find('i').show();
				var $alertas = $('#changPassUser');
				$alertas.validate().resetForm();
				$alertas.find('.form-group.bad.passOld').removeClass('has-success');
		        $alertas.find('.passOld').addClass('has-error');
		        $alertas.find('.fa.oldPass').addClass('fa-warning').removeClass('fa-check').attr("data-original-title",getLang(currentLocale,"PassWord Old not exist"));
			}
      },
      error : function(request,error)
      {
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
      }
	});
}

function deleteProject(projectId){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/deleteProject",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			getProjects("true", function(project){
			});
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function cofirmDeleteProject(projectName, projectId){	
	swal({
		title: getLang(currentLocale,"Are you sure delete project")+" " + projectName +"?",
        text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
        type: "warning", 
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
        cancelButtonText:getLang(currentLocale,"Cancel"),
        closeOnConfirm: true
    },function(){
    	$(".preloader-main").fadeIn("slow");
    	deleteProject(projectId);
	});
}

var role = '';
function getUser(){
	var nameuser = username;
	$.ajax({
		url : jscontext +"/project/"+"getUser",
		type : "POST",
		data: nameuser,
		contentType : "application/json",
		success : function(result) {
			var data= result.data;
			if(data.lastname != null && data.firstname != null){
				$("#nameUser").text(data.lastname+" "+ data.firstname);
				$("#creatorName").text(data.lastname+" "+ data.firstname);
				$(".username-header").text(data.lastname + " " + data.firstname );
			}
			role = data.role;
			$("#role").text(data.role);
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
function goBack() {
	$(window).bind('hashchange', function() {
		var hashTag = window.location.hash; 
		if(hashTag){
			$('a[href="'+hashTag+'"]').click();
		} else{
			window.location.reload();
		}
	});
	window.history.back();
}

function pieByIssueOverView(listIssue, id, listStatus){
	var listIs = [];
	var listColor = [];
    for(var k = 0; k < listIssue.length; k++){
    	var checkAdd = false;
        var listSt = [];
        var itemSt = {};
        var vals = listIssue[k];
        itemSt[vals.status] = vals.count;
        listSt.push(itemSt);
        var objIs = {};

        for(var i = 0; i < listIs.length; i++){
    
            if(vals.tracker in listIs[i]){
                checkAdd = true;
                var itemSt = {};
                itemSt[vals.status] = vals.count;
                var listItem = listIs[i][vals.tracker];
                listItem.push(itemSt);
            } 
        }
		if(checkAdd == false){
		    objIs[vals.tracker]= listSt;
		        listIs.push(objIs);
		}

    }
    
    var itemNameLs = [];
	for(var l = 0; l < listStatus.length; l++){
		itemNameLs.push(listStatus[l].itemName);
		listColor.push(listStatus[l].colorCode);
	}
	var checkLegend = true;
	
	for(var j = 0; j < listIs.length; j++){
		var trackerName = Object.keys(listIs[j])[0];
		var listStOb = Object.values(listIs[j]);
		var listDataSt = [];
		var listLabelSt = [];
		var labelLs = [];
		var dataLs = [];
		var total = 0;
		for(var i = 0; i < listStOb[0].length; i++){
			var statusName = Object.keys(listStOb[0][i])[0];
			var statusValue = Object.values(listStOb[0][i])[0];
			listDataSt.push(statusValue);
			listLabelSt.push(statusName);
			total = total +statusValue;
		}
		var reducer = (accumulator, currentValue) => accumulator + currentValue;
		
		if(listDataSt.length < listStatus.length){
			for(var f = 0; f < itemNameLs.length; f++){
				if(listLabelSt.includes(itemNameLs[f]) == true){
					var indexData = listLabelSt.indexOf(itemNameLs[f]);
					dataLs.push(listDataSt[indexData]);
				} else {
					dataLs.push(0);
				}
				labelLs.push(itemNameLs[f]);
			}
			
		}
		
		if(listDataSt.reduce(reducer) != 0){
			var configIssueCircle = {
		            type: 'pie',
		    	    data: {
		    	        datasets: [{
		    	        	data: dataLs,
		    	        	backgroundColor: listColor,
		    	            label: 'Issue'
		    	        }],
		    	        labels: labelLs
		    	    },
		    		options: {
		    			elements: {
		        		    arc: {
		        		      borderWidth: 0, // <-- Set this to derired value
		        		    }
		        		},
		    			responsive: true,
		    			scaleBeginAtZero: true,
		    			legend: {
		    	            display: false,
		    	            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		    			},
		    			events: true,
		    			animation: {
		    				duration: 500,
		    				easing: "easeOutQuart",
		    				onComplete: function () {
		    				  var ctx = this.chart.ctx;
		    				  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		    				  ctx.textAlign = 'center';
		    				  ctx.textBaseline = 'center';
		    				
		    				  this.data.datasets.forEach(function (dataset) {
		    				
		    					for (var i = 0; i < dataset.data.length; i++) {
		    					  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		    					      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
		    					      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
		    					      start_angle = model.startAngle,
		    					      end_angle = model.endAngle,
		    					      mid_angle = start_angle + (end_angle - start_angle)/2;
		    					
		    					  var x = mid_radius * Math.cos(mid_angle);
		    					  var y = mid_radius * Math.sin(mid_angle);
		    					
		    					  ctx.fillStyle = '#fff';
		    					  if (i == 3){ // Darker text color for lighter background
		    					    ctx.fillStyle = '#fff';
		    					  }
		    					  ctx.font = "14px sans-serif";
		    					  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
		    					  if(dataset.data[i] > 0 ){
		    						  ctx.fillText(dataset.data[i], model.x + x, model.y + y -25);
		    						  // Display percent in another line, line break
		    							// doesn't work for fillText
		    						  ctx.fillText(percent, model.x + x + 3, model.y + y - 5);
		    					  }
		    					}
		    				  });               
		    				},
		    			},
		    		}
		    	};
		} else {
			var dataChange = listDataSt;
			dataChange[0] = 1;
			var listCo = [];
			for(var k = 0 ; k < listLabelSt.length; k++){
				listCo.push("#9E9E9E");
			}
			var configIssueCircle = {
					type: 'pie',
					data: {
			    	  datasets: [{
			    		  data: dataChange,
			    	        backgroundColor: listCo,
			    	        label: 'Issue'
			    	    }],
			    	    labels: listLabelSt
					},
					options: {
						elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
					    tooltips: {enabled: false},
					    hover: {mode: null},
					    legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  				},
					  }
			    }
		}
		
		var htmlAppend = '<div class=" col-md-2 element-chart my-auto" style="padding: 10px; padding-bottom:0px">'
	 		+'<div class="box-chart my-auto">'
				 +'<canvas id="testIssueCircleList_'+j+id+'" width="200" class="canvasMile" style="margin-left: 10px"></canvas>'
				 +'<h5 class="text-center text-list" ><a href="#" onclick="redirectFilterIssueL(\''+id+'\', \''+trackerName+'\')">'+trackerName+'('+total+')'+'</a></h5>'
			 	+'</div>'
			+'</div>';
			$("#progressIssueOverView"+id).append(htmlAppend);
			
		if(document.getElementById("testIssueCircleList_"+j+id) !=null){
			var testIssueCircleList = document.getElementById("testIssueCircleList_"+j+id).getContext("2d");
			window.myPie = new Chart(testIssueCircleList, configIssueCircle);
//			    $("#legendIssue_"+j).html(window.myPie.generateLegend());
		    if(listDataSt.reduce(reducer) != 0){
				if(checkLegend == true){
					$("#legendIssue"+id).html(window.myPie.generateLegend());
					checkLegend = false;
				}
				
			}
		}else{
		}
	
		
	}
}
function redirectFilterIssueL(projectId, trackerName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterIssue/"+trackerName,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/issue";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}

function addIssueAllScreen(){
	setEditTable();
	$("#planToolMaterial").empty();
	var id = $(".changeAddIssue").attr("id");
	var backButton = "";
	var saveButton = "";
	if(id == "tableListTestcase"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListTestcase();">'
			+'<i class="icon-arrow-left"></i> Back'
			+'</button>';
		
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'testcase\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';	
	} else if(id == "tableListManualTestcase"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListTestcase(this);">'
					+'<i class="icon-arrow-left"></i> Back'
					+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'testcaseManual\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "screenTask"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListIssue(\'true\')">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'issue\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "drawSummary"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backSummary()">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'summary\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "box-schedule-init"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backMilestone()">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'milestone\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "drawTestsuites"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backTestsuites()">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'testsuites\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "drawScreenDesign"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backScreenDesign()">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'screenDesign\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "drawTestData"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backTestData()">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'testdata\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "drawSchedule"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backSchedule(\'true\')">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'schedule\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "runningBox"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backSchedule(\'false\')">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'running\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "drawSetting"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backSetting(\'false\')">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'setting\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "boxTestDataDetail"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backDetailTestData()">'
			+'<i class="icon-arrow-left mr-2"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="submit"  onclick ="newTask(\'detailData\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	} else if(id == "dtTask"){
		backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backDetailIssue()">'
			+'<i class="icon-arrow-left"></i> Back'
			+'</button>';
		saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'issue\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	}
	
	if(id != ""){
		$("#title-new").text(getLang(currentLocale,'New Issue'));
		$("#idTask").text("");
		listImages = [];
		$(".newTask").trigger("reset");
		$(".listFiles").empty();
		descriptionTask.data.set("");
		$("#noteEdit").attr("hidden", true);
		
		$("#errorSubject").text("")
		$("#errorPlanDate").text("");
		$("#errorAcutalDate").text("");
		$("#errorMilestone").text("");
	}
	$("#"+id).hide();
	$("#backIssueButton").html(backButton);
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	
	loadDataInPage();
	getUserProject(function(userProjectList){
		$("#assigneeTask").empty();
		$("#watchers").empty();
		$("#assigneeTask").append('<option value = ""></option>');
		for(var i =0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null &&  userProjectList[i]["firstname"] != null){
				var count = 0 ;
				if(duplicateUsers.length > 0){
					$.each(duplicateUsers, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</li>');
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</option>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+'</li>');
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+'</option>');
				}
			}
		}
		$('#assigneeTask option[value=""]').prop("selected",true);
	});
	$(".choose-watcher").empty();
	$(".tag-watchers").click(function(){
		$("#watchers li input").prop("checked", false) ;
		$('.choose-watcher :checkbox:checked').each(function(i){
	    	$("#watchers li[name='"+$(this).val()+"'] input").prop("checked", true);
		});
		$(".show-watchers").on('hide.bs.dropdown', function () {
			$(".choose-watcher").empty();
			$('.show-watchers :checkbox:checked').each(function(i){
				var nameWatcher = $(this).val();
				$.each(userProjectList,function(index, value){
					if(value["username"] == nameWatcher){
						var count = 0 ;
						if(duplicateUsers.length > 0){
							$.each(duplicateUsers, function(indexDup,valueDup){
								if(valueDup.username == nameWatcher){
									$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " "+ value.firstname +"("+ nameWatcher+")"+'</p>');
									count ++;
								}					
							})
						} 
						
						if(count == 0 ){
							$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " " + value.firstname+'</p>');
						}
					}
				})
		    	$("#watcher-"+i).prop( "checked", true );
			});
		});	
	});
}

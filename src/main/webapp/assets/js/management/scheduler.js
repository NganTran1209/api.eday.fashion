var listScheduler = "";
var testsuitels = "";
var listMileStone ;
$(document).ready(function(){
	

	$("#addTimeScheduler").click(function(){
		var countCornTab = $("#drawCronTab").find(".cronTab").length;
		var numbItem = countCornTab+1;
		$("#drawCronTab").append('<div id="corntab'+numbItem+'" class="cronTab mb-3"></div>');
		$('#corntab'+numbItem).cron({
	        initial: "1 * * * *",
	        effectOpts: {
	            openEffect: "fade",
	            openSpeed: "slow",
	            onChange: function() {
	                $('#corntab'+numbItem).text($(this).cron("value"));
	            }
	        },
	        
	    });
	});
	drawParentTaskInTestcase(taskList);
});


function selectAllTestcase() {
	$("#runAsSchedulerAll").click(function(){
		if( $(this).prop("checked") == true ){
			$("#lsTestcasesScheduler input[type='checkbox']").prop("checked",true);
		}else{
			$("#lsTestcasesScheduler input[type='checkbox']").prop("checked",false);
		}
	});
}


function getListTestsuite(status, callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				
				
				if(status == "true"){
					tsByProjectId = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.testsuite;
						obTs["text"] = values.testsuite;
						tsByProjectId.push(obTs);
					});
					callbackF(tsByProjectId);
				} else {
					callbackF(result.data);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


//function getListTestsuites() {
//	$.ajax({
//		url : jscontext +"/project/" + projectId +"/getTestSuites",
//		type : "get",
//		data : "",
//		contentType : "application/json", 
//		success : function(result) {
//			
//			if (!(result.errormessage) ) {
//				testsuitels = result.data;
//				
//			} else {
//			}
//		},
//		error : function(request, e) {
//			console.log("ERROR: ", e);
//			var statusCode = request.status;
//			if (statusCode == 403) {
//				window.location= jscontext + "/login";
//			}
//		},
//		done : function(e) {
//			console.log("DONE");
//		}
//	});
//}

function getScheduler(){
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getScheduleJob",
		type : "GET",
		data : "",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				listScheduler = result.data;
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getRunningJobSchedules(){
	if(glTestsuiteId == '' || glTestsuiteId == undefined || glTestsuiteId == null){
		$.ajax({
			url : jscontext+"/project/"+projectId+"/getRunningJob",
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					var listRunning = result.data;
					if(listRunning.length > 0){
						drawListScheduler(listRunning, "#listRunningJobSchedule"); 
					}else{
						$("#alert-empty-result").html(getLang(currentLocale,"The Result is empty!!!"));
					}
					
					$(".progress-loadMainContent").fadeOut("slow");
				} else {
				}
			},
			error : function(request, e) {
				console.log("ERROR: ", e);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
	return false;
}
var jsonDataScheduler;
function drawListScheduler(listScheduler, idName){
	$("#alert-empty-result").empty();
	$(idName).empty();
	$(".progress-loadMainContent").show();
	if(listScheduler.length > 0){
		$.each( listScheduler, function(index, valuesitemScheduler) {
				var itemScheduler = htmlDrawScheduler(valuesitemScheduler, idName);
				$(idName).append(itemScheduler);
				if(valuesitemScheduler.status == 'SUCCESS' || valuesitemScheduler.status == 'FAILURE'){
					 $('#reRun_'+valuesitemScheduler.id+'_Sche').removeAttr('hidden');
					 $('#deTail_'+valuesitemScheduler.id+'_Sche').removeAttr('hidden');
				}
		});
	} else {
		if(idName == "#listRunningJobSchedule"){
			$("#alert-empty-result").html(getLang(currentLocale,"The Result is empty!!!"));
		} else {
			$("#alert-empty-schedule").html(getLang(currentLocale,"The Schedule is empty!!!"));
		}
		
	}
	$(".progress-loadMainContent").fadeOut("slow");
	
}

function htmlDrawScheduler(valuesitemScheduler, idName){
		var timeRunning = new Date(valuesitemScheduler["startDate"]);
		
		var year = timeRunning.getFullYear();
		var month = ("0"+(timeRunning.getMonth()+1)).slice(-2);
		var day = ("0" + timeRunning.getDate()).slice(-2);
		var time = ("0" + timeRunning.getHours()).slice(-2);
		var minute = ("0" + timeRunning.getMinutes()).slice(-2);
		var seconds = ("0" + timeRunning.getSeconds()).slice(-2);
		var displayDate = year+"-" + month+ "-" + day + ' ' + time +":" + minute+":" + seconds;
		
		if(valuesitemScheduler["status"] == "NOT YET"){
			status ='label-info';
		}else if(valuesitemScheduler["status"] == "ABORT"){
			status ='label-danger';
		}else if(valuesitemScheduler["status"] == "FAILED"){
			status ='label-danger';
		}else{
			status ='label-success';
		}
		var resultName = "Result" + "_" + valuesitemScheduler["username"].replace("@",".") + "_"+ valuesitemScheduler["runningDate"] ;
		var totalTscPass = "" ;
		var totalTscFail = "" ;
		var resultLastRun ="";
		var itemScheduler = '<div class="col-md-12 mb-2 d-block box-item-scheduler" job-scheduler-id = "'+valuesitemScheduler.id +'">'
								
									+'<div class="row border py-3">'
										+'<div class="col-md-3 pr-2">'
											+'<div class="box-itemSchedule">'
												+'<div class="page-title d-flex w-100 m-0 mb-1" style="font-size:18px">'+getLang(currentLocale,"Project")+'</div> '
												+'<div class="w-100 itemSchedule">'
													+'<span class="">- ' +getLang(currentLocale,"Execute Name")+' : </span> '
													+'<span class="">'+valuesitemScheduler["runningName"] +'</span>'
												+'</div>'
												+'<div class="w-100 itemSchedule">'
													+'<span class="">- ' +getLang(currentLocale,"Project Id")+' :</span>'
													+'<span class="">'+ valuesitemScheduler["projectId"] +'</span>'
												+'</div>'
												+'<div class="w-100 itemSchedule">'
													+'<span class="">- '+getLang(currentLocale,"Testsuite Name")+':</span>'
													+'<span class="font-weight-bold">'+ valuesitemScheduler["testsuite"] +'</span> '
												+'</div>'
												+'<div class="w-100 itemSchedule">'
													+'<span class="scheduleSpec">- '+getLang(currentLocale,"Schedule Spec")+':</span> '
													+'<span class="font-weight-bold valueSpec">'+valuesitemScheduler["scheduleSpec"] +'</span>'
												+'</div>'
												+'<div class="w-100 itemSchedule">'
													+'<div class="descRunResult d-block mb-2" style="line-height: 18px;"> - '+getLang(currentLocale,"Run Description")+' : </div><textarea class="form-control" rows="5" readonly value="'+ valuesitemScheduler["runDescription"] +'">'+ valuesitemScheduler["runDescription"] +'</textarea>'
												+'</div>'
											+'</div>'
										+'</div>'
										+'<div class="col-md-3 px-2">'
											+'<div class="box-itemSchedule">'
												+'<div class="page-title d-flex w-100 m-0 mb-1" style="font-size:18px">'+getLang(currentLocale,"Run infor")+'</div> '
												+'<div class="list-colum-Jsdata">'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Create User")+':</span> '
														+'<span class="">'+valuesitemScheduler["username"] +'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- OS :</span> '
														+'<span class="">'+valuesitemScheduler["runningOS"] +'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Browser")+':</span> '
														+'<span class="">'+valuesitemScheduler["browser"] +'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Run Location") +':</span> '
														+'<span class="">'+valuesitemScheduler["runningLocation"] +'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Screen Resolution")+ ':</span> '
														+'<span class="">'+valuesitemScheduler["screenRes"] +'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Timeout(ms)")+ ':</span> '
														+'<span class="">'+valuesitemScheduler["timeout"] +'</span>'
													+'</div>' 
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Record Video")+ ':</span> '
														+'<span class="">'+(valuesitemScheduler["recordVideo"] == 'true' ? "Yes": "No")+'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Mobile Mode") + ':</span> '
														+'<span class="">'+(valuesitemScheduler["mobileMode"] == 'true' ? "Yes": "No") +'</span>'
													+'</div>'
													+'<div class="w-100 itemSchedule">'
														+'<span class="">- '+getLang(currentLocale,"Device Mobile")+ ':</span> '
														+'<span class="">'+valuesitemScheduler["deviceName"] +'</span>'
													+'</div>'
													
												+'</div>'
											+'</div>'
										+'</div>'
										+'<div class="col-md-4 px-2">'
											+'<div class="page-title d-flex w-100 m-0 mb-1" style="font-size:18px">'+getLang(currentLocale,"Testcases")+'</div> '
											+'<div class="w-100 itemSchedule-testcase">'
												+'<div class="list-item-testcase-run ml-1" id="idListTestcase'+valuesitemScheduler.id+'">' ;
												if(valuesitemScheduler["type"] == "RUNNING" && !((valuesitemScheduler["status"] == "RUNNING" || valuesitemScheduler["status"] == "PREPARING" || valuesitemScheduler["status"] == "WAITING"))){
													// load from db to get listtasecase and status and then draw it;
													loadItemTestcase(valuesitemScheduler["testsuite"], resultName, valuesitemScheduler["lsTestcases"],valuesitemScheduler.id, idName);		
												}else if (valuesitemScheduler["type"] == "RUNNING" && ((valuesitemScheduler["status"] == "RUNNING" || valuesitemScheduler["status"] == "PREPARING" || valuesitemScheduler["status"] == "WAITING"))) {
													// draw list testcase
													itemScheduler += getListTestcaseOfJob(valuesitemScheduler["lsTestcases"]);

													var iconRunningStt = "<i class='fa-spin fa fa-spinner mr-2'></i>";
													resultLastRun = '<span class="label label-success  animated-custom bounce-always d-inline-block label-history"><i class="fa-spin fa fa-spinner mr-2"></i>'+ valuesitemScheduler["status"] +'</span>';
												}else {
													// draw list testcase
													itemScheduler += getListTestcaseOfJob(valuesitemScheduler["lsTestcases"]);
												}
												
							itemScheduler +=	'</div>'
											+'</div>'
										+'</div>'
										+'<div class="col-md-2 px-2">'
											+'<div class="box-itemSchedule">'
												+'<div class="page-title d-flex w-100 m-0 mb-1" style="font-size:18px">'+(valuesitemScheduler["type"] != "RUNNING"? getLang(currentLocale,"Run History"): getLang(currentLocale,"Run Enviroment"))+'</div>'
												+'<div class="w-auto itemSchedule itemRunHistory mb-2">';
													if(timeRunning){
													itemScheduler = itemScheduler +'<div class="main-schedule"> '
														+'<span class="'+(valuesitemScheduler["type"] != "RUNNING"? "label "+status+" label-history d-block": "font-weight-bold")+' "><i class="icon-calendar mr-2"></i>'+ displayDate +'</span>'
													+'</div>';
													}
						itemScheduler +='</div>';
							if(valuesitemScheduler["type"] == "RUNNING"){
								itemScheduler +='<div class="w-100 text-success">'
													+'<span>'+getLang(currentLocale,"Total testcases passed")+':</span>'
													+'<span id="totalListTestcase'+valuesitemScheduler.id+'Pass" class="ml-2"></span>'
												+'</div>'
												+'<div class="w-100 text-danger">'
													+'<span  class="mr-1">'+getLang(currentLocale,"Total testcases failed")+':</span>'
													+'<span id="totalListTestcase'+valuesitemScheduler.id+'Fail" class="ml-2"></span>'
												+'</div>'
							    	            +'<div class="w-100 mt-2" id="statusLastRunTsc'+valuesitemScheduler.id+'">'+resultLastRun +'</div>'
							    	            +'<div class="w-100 text-danger">'
								    	            +'<span  class="mr-1">'+getLang(currentLocale,"Exception Message")+':</span>'
								    	            +'<span id="totalListTestcase'+valuesitemScheduler.id+'ExceptionMessage" class="ml-2">'+ (valuesitemScheduler["exceptionMessage"] == null?"":valuesitemScheduler["exceptionMessage"]) +'</span>'
							    	            +'</div>'
							}
							var linkRunning = jscontext+'/project/'+projectId +'/'+valuesitemScheduler["testsuite"]+'/result/'+resultName;
							itemScheduler +='</div>'
										+'</div>'
										+'<div class="col-md-12 mt-3 text-right">'
										    +(valuesitemScheduler["type"] != "RUNNING" ? 
										    		'<button onclick="editScheduler(\''+ valuesitemScheduler["id"] +'\')" type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm"><i class="ti-pencil-alt mr-2"></i> '+getLang(currentLocale, "Edit")+'</button>'
										    		+'<button data-name="'+valuesitemScheduler["id"]+'" onclick="showConfirmMessagesSchedule(this)" id="delete_'+valuesitemScheduler["id"]+'" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default my-auto button-sm ml-2"><i class="ti-trash mr-2"></i> '+getLang(currentLocale, "Delete")+'</button>' 
										    		: '<button id="reRun_'+valuesitemScheduler.id+'_Sche" type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm mr-1" onclick="reRunSchedule(\''+ valuesitemScheduler["testsuite"] +'\', \''+ valuesitemScheduler.id+'\')" hidden><i class="icon-reload mr-2"></i> '+getLang(currentLocale, "Rerun")+'</button>'
									    			+ '<a href="#" onclick="redirectResultSchedule(\''+ jscontext +'\', \''+ glProjectId+'\', \''+ valuesitemScheduler["testsuite"] +'\', \''+ resultName +'\')" id="deTail_'+valuesitemScheduler.id+'_Sche" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm mr-1" hidden><i class="ti-receipt mr-2"></i> '+getLang(currentLocale, "Detail")+'</a>'
										    		+ '<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkRunning(\''+linkRunning+'\')"><li class="fa fa-link"></li></button>'
										    )
										+'</div>'
									+'</div>'
							+'</div>';
		
		
		return itemScheduler;
		
}

function copyLinkRunning(link){
	$('#modalCopyLink').modal({
		backdrop: 'static'
	});
	$("#titleFormCopy").text(getLang(currentLocale,'Copy link of the result'));
	
	$("#copyItem").val(window.location.origin + link);
	
}

function copyLink(){
	var copyText = document.getElementById("copyItem");
	copyText.select();
	copyText.setSelectionRange(0, 99999);
	document.execCommand("copy");
	$("#modalCopyLink").modal('hide');
}

function resetFormCopy(){
	$('#modalCopyLink').on('hidden.bs.modal', function() {
		document.getElementById("copyLink").reset();
	    var $alertas = $('#copyLink');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

function syncProject(e){
	$(".preloader-main").fadeIn("slow");
	$.ajax({
		url : jscontext + "/project/"+ projectId+"/syncProject",
		type : "POST",
		data : "",
		contentType : "application/json",
		success : function(result) {
			$(".preloader-main").fadeOut("slow");
			if (!(result.errormessage) ) {
				var bodyListTestsuiteElm = $("#bodyTestsuiteRecord");
				if(bodyListTestsuiteElm.length > 0){
					loadListRs(result.data);
				}
				
			}else {
				swal({
				       title: "",
				       text: getLang(currentLocale,"Sync project fail:") + result.errormessage,
				       type: "warning",
				       timer: 2000,
			        	showConfirmButton: false
				   });
//				window.location= jscontext + "/login";
			}
      },
      error : function(request,error)
      {
    	  $(".preloader-main").fadeOut("slow");
    	  
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({ 
				       title: "",
				       text: "Sync project fail:" + error,
				       type: "warning",
				       timer: 2000,
			        	showConfirmButton: false
				   });
			}
      }
	
	});
}

function redirectResultSchedule(jscontext, projectId, testsuiteName, resultName) {
	syncProject();
	var url = jscontext+'/project/'+projectId +'/'+testsuiteName+'/result/'+resultName;
	window.open(url);
}
//reRun
function reRunSchedule(testsuiteName, idJob){
	$("#reRun_"+idJob+"_Sche").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#reRun_"+idJob+"_Sche").prop("disabled", true);

	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/reRunExecuteTest/" + idJob,
		type : "POST",
		contentType : "application/json", 
		success : function(result) {
			if (!(result.errormessage) ) {
//				$("#calRunningTabs").trigger("click");
				$("#reRun_"+idJob+"_Sche").html("<i class='ti-check'></i> "+getLang(currentLocale,"RERUN"));
				$("#reRun_"+idJob+"_Sche").prop("disabled", false);
				//loadRunning();
			}
		  },
		  error : function(request,error)
		  {
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
		  }
	
	})
}

function formatDateTimeRs(theDateTime){
	var dateUtc = new Date(theDateTime);
	return dateUtc.toLocaleString(); 
} 

function loadItemTestcase(testsuiteName,resultName,listTestcaseScheduler ,jobId,idRootElement){
	var testcaseNamesScheduler ="";
		$.ajax({
			url : jscontext+"/project/"+projectId+"/"+testsuiteName+ "/getResultInfoByExecuteJobId"+"/"+jobId,
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					listTestcaseSchedulerJS = result.data;
					console.log(listTestcaseSchedulerJS);
					var countTestCaseFail = 0;
					var countTestCasePass = 0;
					for(var i =0; i < listTestcaseSchedulerJS.length; i++){
						var sttResult = listTestcaseSchedulerJS[i].testCaseResult;
						if(sttResult == "NOT YET"){
							var statusTestcase ="label-info";
						}else if(sttResult == "ABORT"){
							var statusTestcase ="label-danger";
						}else if(sttResult == "FAILED"){
							var statusTestcase ="label-danger";
							countTestCaseFail = countTestCaseFail + 1;
						}else if (sttResult == "PASS"){
							var statusTestcase ="label-success";
							countTestCasePass = countTestCasePass + 1;

						}else if (sttResult == "DEGRADE"){
							var statusTestcase ="label-degrade";
							
						}
						testcaseNamesScheduler+= "<div class='item-box-testcase'><span class='item-testcase-run'>"+listTestcaseSchedulerJS[i]["testCaseName"] +"</span>"
												+"<span class='label "+ statusTestcase +" label-history'>"+ sttResult +"</span></div>"
					}
					$(idRootElement +" #totalListTestcase"+jobId+"Fail").html(countTestCaseFail);
					$(idRootElement +" #totalListTestcase"+jobId+"Pass").html(countTestCasePass);
					$(idRootElement +" #idListTestcase"+jobId).html(testcaseNamesScheduler);
					if(listTestcaseSchedulerJS.length > 0 ){
						var sttResultLastRun = listTestcaseSchedulerJS[0]["testResult"];
						if(sttResultLastRun == "FAILED"){
							var statusTestcaseLastRun ="label-danger";
							var iconRunningStt = "";
						}else if(sttResultLastRun == "PASS"){
							var statusTestcaseLastRun ="label-success";
							var iconRunningStt = "";
						}else {
							var statusTestcaseLastRun ="label-danger";
							var iconRunningStt = "";							
						}
						var resultLastRun = '<span class="label '+statusTestcaseLastRun+' label-history d-inline-block">'+ iconRunningStt + sttResultLastRun +'</span>';
						$(idRootElement +" #statusLastRunTsc"+jobId).html(resultLastRun);
					}
				} else {
				}
			},
			error : function(request,e) {
				console.log("ERROR: ", e);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});
		return false;
}


function getListTestcaseOfJob(listTestcaseScheduler ){
	var lsTestcase = listTestcaseScheduler.split(";");
	var testcaseNamesScheduler = "";
	for(var i =0; i < lsTestcase.length; i++){
		testcaseNamesScheduler += "<div class='item-box-testcase'><span class='item-testcase-run'>"+lsTestcase[i] +"</span></div>";
	}
	return testcaseNamesScheduler;
}
$("#slOsScheduler").on('change', function(){
	var osSchedule = $("#slOsScheduler").val();
	$("#slbrowserScheduler").empty();
	if(osSchedule == "MacOS"){
		
		var html = '<option value="chrome">Google Chrome</option>'
		+'<option value="safari">Safari</option>';
	} else {
		var html = '<option value="ie">Internet Explorer 11</option>'
			+'<option value="edge">Microsoft Edge</option>'
			+'<option value="firefox">Mozilla Firefox</option>'
			+'<option value="chrome">Google Chrome</option>'
			+'<option value="safari">Safari</option>';
		
	}
	$("#slbrowserScheduler").append(html);
});

$("#slOs").on('change', function(){
	var osSchedule = $("#slOs").val();
	$("#slbrowser").empty();
	if(osSchedule == "MacOS"){
		
		var html = '<option value="chrome">Google Chrome</option>'
		+'<option value="safari">Safari</option>';
	} else {
		var html = '<option value="ie">Internet Explorer 11</option>'
			+'<option value="edge">Microsoft Edge</option>'
			+'<option value="firefox">Mozilla Firefox</option>'
			+'<option value="chrome">Google Chrome</option>'
			+'<option value="safari">Safari</option>';
		
	}
	$("#slbrowser").append(html);
});

function editScheduler(id){
	getAllMilestoneSchedule("false", function(listMileStone){
    	$("#executeNameScheduler").empty();
		$.each(listMileStone, function(index, values){
			var dateStart = new Date(values["startDate"]);
			var dateEnd = new Date(values["endDate"]);
			var dateStartStr = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate()+" "+dateStart.getHours()+":"+dateStart.getMinutes()+":"+dateStart.getSeconds();
			var dateEndStr = dateEnd.getFullYear() + "-" + (dateEnd.getMonth() + 1) + '-'+ dateEnd.getDate()+" "+dateEnd.getHours()+":"+dateEnd.getMinutes()+":"+dateEnd.getSeconds();
			var selected ="";
			var dateNow = new Date().getTime();
			if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){ 
				selected = "selected";
				$("#runDescriptionScheduler").val(values["description"]);
			}
			var html = '<option value="'+values.projectMilestoneIdentity.mileStoneName+'" '+ selected +' >'+values.projectMilestoneIdentity.mileStoneName+"("+dateStartStr+","+dateEndStr+")"+'</option>';
			$("#executeNameScheduler").append(html);
		});
    });	
	$("#modalRunScheduler .modal-title").text(getLang(currentLocale,"Edit Schedule"));
	$("#addTimeScheduler").hide();
	var schedulerTime ="";
	$.each( listScheduler, function(index, valuesitemSchedulerEdit) {
		if(valuesitemSchedulerEdit["type"] == "SCHEDULE"){
			var idJobScheduler = valuesitemSchedulerEdit["id"];
			if(idJobScheduler == id){
				schedulerTime = valuesitemSchedulerEdit["scheduleSpec"];
				$("#idJobScheduler").val(idJobScheduler);
				$("#jobNameScheduler").val(valuesitemSchedulerEdit["jobName"]);
				$("#statusScheduler").val(valuesitemSchedulerEdit["status"]);
				$("#queueIdScheduler").val(valuesitemSchedulerEdit["queueId"]);
				$("#buildNumberScheduler").val(valuesitemSchedulerEdit["buildNumber"]);
				$("#jsonDataScheduler").val(valuesitemSchedulerEdit["jsonData"]);
				$("#createDateScheduler").val(valuesitemSchedulerEdit["createdDate"]);
				$("#updateDateScheduler").val(valuesitemSchedulerEdit["updatedDate"]);
				$("#startDateScheduler").val(valuesitemSchedulerEdit["startDate"]);
				  
				$("#executeNameScheduler").val(valuesitemSchedulerEdit["executeName"]);
				
				$("#slOsScheduler option").prop("selected",false);
				$("#slOsScheduler option[value="+valuesitemSchedulerEdit["runningOS"]+"]").prop("selected",true);
				
				$("#slbrowserScheduler option").prop("selected",false);
				$("#slbrowserScheduler option[value="+valuesitemSchedulerEdit["browser"]+"]").prop("selected",true);
				
				$("#sladdressScheduler option").prop("selected",false); 
				
				for(var i = 0; i <= $("#sladdressScheduler option").length; i++){
					var nameOptionLocation = $("#sladdressScheduler option:nth-child("+i+")").text();
					if(nameOptionLocation == valuesitemSchedulerEdit["runningLocation"]){
						$("#sladdressScheduler option:nth-child("+i+")").prop("selected",true);
					}
				}
				$("#slscreenScheduler option").prop("selected",false);
				for(var j = 0 ; j < $("#slscreenScheduler option").length; j++){
					var nameOptionLocation = $("#slscreenScheduler option")[j].value;
					if(nameOptionLocation == valuesitemSchedulerEdit["screenRes"]){
						//$("#sladdressScheduler option:nth-child("+j+")").prop("selected",true);
						$("#slscreenScheduler option")[j].selected = true;
					}
					
				}
				
				$("#txtTimeoutScheduler").val(valuesitemSchedulerEdit["timeout"]);
				
				if(valuesitemSchedulerEdit["mobileMode"] == 'false'){
					$("#mobileModeScheduler").prop("checked",false);
				}else{
					$("#mobileModeScheduler").prop("checked",true);
				}
				
				if(valuesitemSchedulerEdit["recordVideo"] == 'false'){
					$("#videoModeScheduler").prop("checked",false);
				}else{
					$("#videoModeScheduler").prop("checked",true);
				}
				
				$("#lsTestcasesScheduler").empty(htmlTestcase);

				$("#devicesSelectionScheduler option").prop("selected",false);
				for(var h = 0 ; h <= $("#devicesSelectionScheduler option").length; h++){
					var nameOptionDevices = $("#devicesSelectionScheduler option:nth-child("+h+")").text();
					if(nameOptionDevices == valuesitemSchedulerEdit["deviceName"]){
						$("#devicesSelectionScheduler option:nth-child("+h+")").prop("selected",true);
					}
					
				}
				$("#testsuiteNameInModalScheduler").html(valuesitemSchedulerEdit["testsuite"]);
				
				$("#runDescriptionScheduler").val(valuesitemSchedulerEdit["runDescription"]);
				var lsTestcases = valuesitemSchedulerEdit["lsTestcases"].split(";");
				for(var k = 0 ; k < lsTestcases.length; k++){
					console.log(k);
					var htmlTestcase = '<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0">'
											+'<input type="checkbox" name="runTestcaseScheduler" id="runAsTestcaseScheduler'+k+'"value="'+lsTestcases[k]+'" checked="checked">'
											+'<label for="runAsTestcaseScheduler'+k+'">'+lsTestcases[k]+'</label>'
										+'</div>';
					$("#lsTestcasesScheduler").append(htmlTestcase);
				}
				
				
				$("#runAsScheduler").prop("checked",true);
				$("#selectTimeRun").show();
				
				$("#example1b-val").val(valuesitemSchedulerEdit["scheduleSpec"]);
				
				if(glTestsuiteId == '' || glTestsuiteId == undefined || glTestsuiteId == null){
//					loadItemTestsuiteScheduler();
					
					$("#testsuiteRunScheduler").val(valuesitemSchedulerEdit["testsuite"]).trigger('change.select2');
					$('#testsuiteRunScheduler').on('change', function() {
						generationTestcaseSelect();
					});
					$('#testsuiteNameInModalScheduler').hide();
					$('#testsuiteRunScheduler').show();
				}else{
					loadItemTestcaseScheduler();
					$("#testsuiteNameInModalScheduler").html(glTestsuiteId);
					$('#testsuiteNameInModalScheduler').show();
					$('#testsuiteRunScheduler').hide();
				}
				selectAllTestcase();
			}
		}
	});
	
	$('.cronTab').empty();
	$('#corntab1').cron({
        initial: schedulerTime,
        effectOpts: {
            openEffect: "fade",
            openSpeed: "slow"
        },
        onChange: function() {
            var valueScheduler = $('#example1b-val').text($(this).cron("value"));
            console.log(valueScheduler.text());
        },
    });
	$("#btnRunScheduler").html('<i class="ti-check mr-1"></i>'+getLang(currentLocale,"Save"));
	$("#modalRunScheduler").modal("show");
}


function hideModalRunScheduler(){
	$('#modalRunScheduler').on('hidden.bs.modal', function () {
		$("#selectTimeRun").hide();
		$("#lsTestcasesScheduler").empty();
		$("#runTestCasesScheduler")[0].reset();
	})
}

function getAllMilestoneSchedule(status, callbackF){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getAllMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				if(status == "true"){
					var lsMilestone = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.projectMilestoneIdentity.mileStoneName
						obTs["text"] = values.projectMilestoneIdentity.mileStoneName
						lsMilestone.push(obTs);
					});
					callbackF(lsMilestone);
				} else {
					listMileStone = result.data;
					callbackF(listMileStone);
				}
				
			}
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can get data!!!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}

$("#executeNameScheduler").on('change', function(){
	var mileStoneName = $("#executeNameScheduler").val();
	$.each(listMileStone, function(index, values){
		if(mileStoneName == index){
			$("#runDescriptionScheduler").val(values["description"]);
		}
	});
});

function creatScheduler(){
	if(Object.entries(testsuitels).length > 0) {
		$("#addTimeScheduler").show();
		$("#modalRunScheduler .modal-title").text(getLang(currentLocale,"Create Run Schedule"));
		$('.cronTab').empty();
		$('#corntab1').cron({
	        initial: "42 3 * * 5",
	        effectOpts: {
	            openEffect: "fade",
	            openSpeed: "slow"
	        },
	        onChange: function() {
	            $('#example1b-val').text($(this).cron("value"));
	        },
	    });
		
		    getAllMilestoneSchedule("false", function(listMileStone){
		    	$("#executeNameScheduler").empty();
				$.each(listMileStone, function(index, values){
					var dateStart = new Date(values["startDate"]);
					var dateEnd = new Date(values["endDate"]);
					var dateStartStr = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate()+" "+dateStart.getHours()+":"+dateStart.getMinutes()+":"+dateStart.getSeconds();
					var dateEndStr = dateEnd.getFullYear() + "-" + (dateEnd.getMonth() + 1) + '-'+ dateEnd.getDate()+" "+dateEnd.getHours()+":"+dateEnd.getMinutes()+":"+dateEnd.getSeconds();
					var selected ="";
					var dateNow = new Date().getTime();
					if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){ 
						selected = "selected";
						$("#runDescriptionScheduler").val(values["description"]);
					}
					var html = '<option value="'+values.projectMilestoneIdentity.mileStoneName+'" '+ selected +' >'+values.projectMilestoneIdentity.mileStoneName+"("+dateStartStr+","+dateEndStr+")"+'</option>';
					$("#executeNameScheduler").append(html);
				});
		    });	
			if(glTestsuiteId == '' || glTestsuiteId == undefined || glTestsuiteId == null){
//				loadItemTestsuiteScheduler();
				generationTestcaseSelect();
				$('#testsuiteRunScheduler').on('change', function() {
					generationTestcaseSelect();
				});
				$('#testsuiteNameInModalScheduler').hide();
				$('#testsuiteRunScheduler').show();
			}else{
				loadItemTestcaseScheduler();
				$("#testsuiteNameInModalScheduler").html(glTestsuiteId);
				$('#testsuiteNameInModalScheduler').show();
				$('#testsuiteRunScheduler').hide();
			}
			
			$("#btnRunScheduler").html('<i class="ti-check mr-1"></i>'+getLang(currentLocale,"Save"));
			$("#modalRunScheduler").modal("show");
	} else {
		swal({
	        title: "Testsuite is empty!!!",
	        type: "warning",
	        timer: 2000,
	        showConfirmButton: false
	    });
	}
}

$("#modalRunScheduler").on('shown.bs.modal', function () {
	
})


function setDefaultMileStones(listMileStones){
	$.each(listMileStones, function(index, values){
		var dateNow = new Date().getTime();
		if(dateNow >= values["startDate"] && dateNow <= values["endDate"]){
			$("#executeNameScheduler option").prop("selected",false);
			$("#executeNameScheduler option[value="+values.projectMilestoneIdentity["mileStoneName"]+"]").prop("selected",true);
			$("#runDescriptionScheduler").val(values["description"]);
			return;
		}
	});
}

function saveSheduler(){
	var executeName = $("#runTestCasesScheduler [name='executeName']").val();
	if(executeName != ""){
		$("#btnRunScheduler").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
		$("#saveNewLayout").prop("disabled", true);
		var idJobScheduler = $("#idJobScheduler").val();
		var testsuiteName = $("#select2-testsuiteRunScheduler-container").attr("title");
		if(testsuiteName == undefined) {
			testsuiteName = $("#testsuiteNameInModalScheduler").text();
		} else {
			testsuiteName = testsuiteName
		}
		var executingJob = {
				"id": idJobScheduler,
				"type": "SCHEDULE",
				"username": username,
				"projectId": projectId,
				"testsuite": testsuiteName,
				"queueId": $("#queueIdScheduler").val(),
				"buildNumber": $("#buildNumberScheduler").val(),
				"runningName": $("#runTestCasesScheduler [name='executeName']").val(),
				"jsonData": "",
				"lsTestcases": $("#lsTestcasesScheduler  [name='runTestcaseScheduler']").serializeArray().map(function(obj){return obj.value;}).join(";") ,
				"runningOS": $("#runTestCasesScheduler [name='runningOS']").val(),
				"timeout": $("#runTestCasesScheduler [name='timeout']").val(),
				"runningLocation": $("#runTestCasesScheduler [name='runningLocation']").val(),
				"screenRes": $("#runTestCasesScheduler [name='screenRes']").val(),
				"mobileMode": $("#runTestCasesScheduler [name='mobileMode']").prop("checked").toString(),
				"mobileRes": $("#runTestCasesScheduler [name='deviceName']").val(),
				"deviceName": $("#runTestCasesScheduler [name='deviceName']")[0].selectedOptions[0].text,
				"browser": $("#runTestCasesScheduler [name='browser']").val(),
				"recordVideo": $("#runTestCasesScheduler [name='isRecordVideo']").prop("checked").toString(),
				"scheduleSpec": $("#example1b-val").text(),
				"runDescription": $("#runTestCasesScheduler [name='runDescription']").val(),
				"executeName": executeName,
				"projectName":$("#projectIdInModalScheduler").text(),
		}
		if(idJobScheduler == "") {
			var getTestsuiteID = $("#select2-testsuiteRunScheduler-container").attr("title");
			$.ajax({
				url : jscontext+"/project/"+glProjectId+"/"+getTestsuiteID+"/createScheduleJob",
				type : "POST",
				data : JSON.stringify(executingJob), 
				contentType : "application/json",
				success : function(result) {
					if (result.errormessage == null || result.errormessage == "") {
						$("#modalRunScheduler").modal("hide");
						swal({
					        title: getLang(currentLocale,"Create Schedule Success!!!"),
					        type: "success",
					        timer: 1000,
					        showConfirmButton: false
					    });
						getScheduler();
						var testsuiteName = $("#filterTestsuite").val();
						drawScheduleByFilter(testsuiteName);
						
						$("#alert-empty-schedule").empty();
						$("#btnRunScheduler").html("<i class='ti-check'></i>"+ getLang(currentLocale,"Save"));
						$("#btnRunScheduler").prop("disabled", false);
					}
				  },
				  error : function(request, status, error)
				  {
					  
					  var statusCode = request.status;
						if (statusCode == 403) {
							window.location= jscontext + "/login";
						} else {
							swal({
						        title: getLang(currentLocale,"Can not save!!!"),
						        type: "warning",
						        timer: 2000,
						        showConfirmButton: false
						    });
						}
				  }
			
			});
		} else {
			executingJob["jobName"] = $("#jobNameScheduler").val();
			var getTestsuiteIDs = $("#testsuiteNameInModalScheduler").text();
			$.ajax({
				url : jscontext+"/project/"+glProjectId+"/"+getTestsuiteIDs+"/editScheduleJob",
				type : "POST",
				data : JSON.stringify(executingJob), 
				contentType : "application/json",
				success : function(result) {
					if (result.errormessage == null || result.errormessage == "") {
						$("#modalRunScheduler").modal("hide");
						swal({
					        title: getLang(currentLocale,"Edit Schedule Success!!!"),
					        type: "success",
					        timer: 1000,
					        showConfirmButton: false
					    });
						getScheduler();
						var testsuiteName = $("#filterTestsuite").val();
						drawScheduleByFilter(testsuiteName);
						$("#btnRunScheduler").html("<i class='ti-check'></i>"+ getLang(currentLocale,"Save"));
						$("#btnRunScheduler").prop("disabled", false);
					}
				  },
				  error : function(request, status, error)
				  {
					 
					  var statusCode = request.status;
						if (statusCode == 403) {
							window.location= jscontext + "/login";
						} else {
							 swal({
							        title: getLang(currentLocale,"Can not save!!!"),
							        type: "warning",
							        timer: 2000,
							        showConfirmButton: false
							    });
						}
				  }
			
			});
		}
	} else {
		$("#messageError").val("ExecuteName is empty!");
	}
	
}


function loadItemTestsuiteScheduler(){
	$("#testsuiteRunScheduler").empty();
	$.each( testsuitels, function(index, valuesItemTestsuite) { 
		var optionSelectTestsuite = '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
		$("#testsuiteRunScheduler").append(optionSelectTestsuite);
		$("#testsuiteRunScheduler").select2({
			dropdownParent: $("#modalRunScheduler"),
			placeholder: getLang(currentLocale,"Select an option"),
			allowClear: true,
			width: '100%',
		});
	});
}

function generationTestcaseSelect(){
	var nameTestcaseSc = $("#testsuiteRunScheduler").val();
	if(nameTestcaseSc == null){
		nameTestcaseSc = $("#testsuiteRunScheduler").text();
	}
	$.ajax({
	url : jscontext + "/project/" +glProjectId +"/" + nameTestcaseSc + "/getTestcases",
	type : "GET",
	data : "",
	contentType : "application/json",
	success : function(result) {
		if (!(result.errormessage) ) {
			var JsonTestcase =result.data;
			$("#lsTestcasesScheduler").empty(); 
			$.each( JsonTestcase, function(index, valuesItemTestcase) {
				var htmlTestcase = '<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0">'
					+'<input type="checkbox" name="runTestcaseScheduler" id="runAsTestcaseScheduler'+index+'"value="'+valuesItemTestcase.testcase+'" checked="checked">'
					+'<label for="runAsTestcaseScheduler'+index+'">'+valuesItemTestcase.testcase+'</label>' 
				+'</div>';
				$("#lsTestcasesScheduler").append(htmlTestcase);
			});
			selectAllTestcase();
		}
	},
	error : function(request, e) {
		console.log("ERROR: ", e);
		var statusCode = request.status;
		if (statusCode == 403) {
			window.location= jscontext + "/login";
		}
	},
	done : function(e) {
		console.log("DONE");
	}
	});
}


function loadItemTestcaseScheduler(){
	$("#lsTestcasesScheduler").empty();
	$.each(listTestcase, function(index, valuesItemTestcase){
		if(valuesItemTestcase.status == "add"){
			var htmlTestcase = '<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0">'
				+'<input type="checkbox" name="runTestcaseScheduler" id="runAsTestcaseScheduler'+index+'" value="'+valuesItemTestcase.projectProgressIdentity.testcase+'" checked="checked">'
				+'<label for="runAsTestcaseScheduler'+index+'">'+valuesItemTestcase.projectProgressIdentity.testcase+'</label>'
			+'</div>';
			$("#lsTestcasesScheduler").append(htmlTestcase);
			selectAllTestcase();
		}
		
	});
}

function deleteScheduler(id){
	$(".progress-loadMainContent").show();
	var topBoxListScheduler = $("#listScheduler").height() / 2 - 200;
	$(window).scrollTop(topBoxListScheduler);
	var offsetItemDeleted = $("#delete_"+id).closest(".box-item-scheduler").offset().top -100;
	console.log(offsetItemDeleted);
	
	var itemJod = "";
	var executingJob = {};
	$.each( listScheduler, function(index, valuesitemSchedulerDelete) {
		if(valuesitemSchedulerDelete["type"] == "SCHEDULE"){
			var idJobScheduler = valuesitemSchedulerDelete["id"];
			if(idJobScheduler == id){
				var itemJod =  valuesitemSchedulerDelete;
				executingJob = {
						"id": idJobScheduler,
						"type": itemJod["type"],
						"username": itemJod["username"],
						"projectId": itemJod["projectId"],
						"testsuite": itemJod["testsuite"],
						"status": itemJod["status"] ,
						"jobName": itemJod["jobName"],
						"queueId": itemJod["queueId"],
						"buildNumber": itemJod["buildNumber"],
						"runningName": itemJod["runningName"],
						"jsonData": "",
						"lsTestcases": itemJod["lsTestcases"].toString(),
						"runningOS": itemJod["runningOS"],
						"timeout": itemJod["timeout"],
						"runningLocation": itemJod["runningLocation"],
						"screenRes": itemJod["screenRes"],
						"mobileMode": itemJod["mobileMode"],
						"mobileRes": itemJod["mobileRes"],
						"deviceName": itemJod["deviceName"],
						"browser": itemJod["browser"],
						"recordVideo": itemJod["recordVideo"],
						"scheduleSpec": itemJod["scheduleSpec"]
				}
			}
		}
		
	});
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/deleteScheduleJob",
		type : "POST",
		data : JSON.stringify(executingJob),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title:  getLang(currentLocale,"Delete Schedule Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
				getScheduler(); 
				var testsuiteName = $("#filterTestsuite").val();
				drawScheduleByFilter(testsuiteName);
				
				$(window).scrollTop(offsetItemDeleted);
				$(".progress-loadMainContent").fadeOut("slow");
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
			        title: getLang(currentLocale,"Delete Scheduler Error!!!"),
			        type: "warning",
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function showConfirmMessagesSchedule(isThis) {
	 var item = $(isThis).attr("data-name");
	
   swal({
       title:  getLang(currentLocale,"Are you sure delete Schedule?"),
       text:  getLang(currentLocale,"You will not be able to recover this imaginary file!"),
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
       cancelButtonText: getLang(currentLocale,"Cancel"),
       closeOnConfirm: false
   }, function () {
	    deleteScheduler(item);
		$("#listScheduler").empty();
	   
   	
   });
}

function drawScheduleByFilter(testsuiteName){
	$(".progress-loadMainContent").show();
	testsuiteNameParse = testsuiteName;
	var filterSchedule = {
			"testsuiteName": testsuiteName,
			"type": "SCHEDULE"
	}
	$.ajax({
		url : jscontext +"/project/" + projectId +"/filterSchedule",
		type : "post",
		data : JSON.stringify(filterSchedule),
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var tcFilter = result.data;
				drawListScheduler(tcFilter, "#listScheduler");	
				$(".progress-loadMainContent").fadeOut("slow");
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function drawRunningByFilter(testsuiteName, status, executeBy, milestone){
	testsuiteParse = testsuiteName;
	statusSession = status;
	executeByParse = executeBy;
	milestoneParse = milestone;
	var filterRunning = {
			"testsuiteName": testsuiteName,
			"statusTestcase": status,
			"executeBy": executeBy,
			"milestone": milestone,
			"type": "RUNNING"
	}
	$(".progress-loadMainContent").show();
	$.ajax({
		url : jscontext +"/project/" + projectId +"/filterRunning",
		type : "post",
		data : JSON.stringify(filterRunning),
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var tcFilter = result.data;
				drawListScheduler(tcFilter, "#listRunningJobSchedule");	
			} else {
			}
			$(".progress-loadMainContent").fadeOut("slow");
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getExecuteBy(callbackF){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getExecuteBy",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var listExecuteBy = result.data.map(function(obj){
					return {"id": obj, "text":obj}
				});		
			} else {
			}
			callbackF(listExecuteBy);
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function filterSchedule(){
	var testsuiteName = $("#filterTestsuite").val();
	
	$("#filSchedule").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filSchedule").prop("disabled", true);
	
	drawScheduleByFilter(testsuiteName);
	
	$("#filSchedule").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filSchedule").prop("disabled", false);
}

function filterRunning(){
	var testsuiteName = $("#filterTestsuiteRunning").val();
	var status = $("#filterStatus").val();
	var executeBy = $("#filterExecuteBy").val();
	var milestone = $("#filterMileStone").val();
	
	
	$("#filRunning").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filRunning").prop("disabled", true);
	
	drawRunningByFilter(testsuiteName, status, executeBy, milestone);
	
	$("#filRunning").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filRunning").prop("disabled", false);
}

$("#filterTestsuite").on('select2:opening', function(){
	checkSession = false;
	$("#filterTestsuite").one('change', function(){
		var testsuiteName = $("#filterTestsuite").val();
		
		if(checkSession != true){
			drawScheduleByFilter(testsuiteName);
		}
		
	});
});

$("#filterTestsuiteRunning").on('select2:opening', function(){
	checkSession = false;
	$("#filterTestsuiteRunning").one('change', function(){
		checkFilterRunning(checkSession);
		
	});
});

$("#filterStatus").on('select2:opening', function(){
	checkSession = false;
	$("#filterStatus").one('change', function(){
		checkFilterRunning(checkSession);
		
	});
});

$("#filterExecuteBy").on('select2:opening', function(){
	checkSession = false;
	$("#filterExecuteBy").one('change', function(){
		checkFilterRunning(checkSession);
		
	});
});

$("#filterMileStone").on('select2:opening', function(){
	checkSession = false;
	$("#filterMileStone").one('change', function(){
		checkFilterRunning(checkSession);
		
	});
});

function checkFilterRunning(checkSession){
	var testsuiteName = $("#filterTestsuiteRunning").val();
	var status = $("#filterStatus").val();
	var executeBy = $("#filterExecuteBy").val();
	var milestone = $("#filterMileStone").val();
	if(checkSession != true){
		drawRunningByFilter(testsuiteName, status, executeBy, milestone);
	}
}

function backSchedule(status){
	if(status == "true"){
		$("#newTask").hide();
		$("#drawSchedule").show();
	} else {
		$("#newTask").hide();
		$("#runningBox").show();
	}
	
}
//Load List Layout
var existEdit = false;
$( document ).ready(function() {
	drawParentTaskInTestcase(taskList);
});
function getValueControl(currentLayout){
//	var select = document.getElementById("typeSelect");
//	select.options.length = 0;
	var htmlOptions="";
	for(var i=0; i< currentLayout.Controls.length; i++){
		for(var j=0; j < currentLayout.Controls[i].Attributes.length; j++){
			var vb = currentLayout.Controls[i].Attributes[j];
			if(vb.Name == "type" && (vb.Value == "submit" || vb.Value == "button" || vb.Value == "a")){
				htmlOptions += ' <option value="'+ currentLayout.Controls[i].Name +'">'+currentLayout.Controls[i].Name+'</option>';
			}
		}
	}
	$(".typeSelect").append(htmlOptions);
}


function funcCopyLayout(isThis) {
	
	$('#modalCopyLayout').modal({
		backdrop: 'static'
	});
	var item = $(isThis).attr("data-name");
	var testsuiteName = $(isThis).attr("testsuite-name");
	$("#testsuiteId").val(testsuiteName);
	$("#layout_new_copy").val(item);
	$("#layoutNameOld").text(item);
	$("#messageErrorss").hide();
	$("#layoutId").val($(isThis).attr("data-id"));
	$("#testsuiteLists").empty();
	getListTestsuite("false", function(testsuitels){
		
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteLists").append(htmlOptions);
		$('#testsuiteMan option[value="'+testsuiteName+'"]').prop("selected",true);
	});
}
function funExportCSV(){
	$(".btnExportCSV").click(function(){
		var itemNameLayout= $(this).attr("data-name");
	});
	modalhideclearAttr();
}

function copyLayout(){
	var layoutNameOld = $("#layoutNameOld").text();
	var layoutNew = $("#layout_new_copy").val();
	var testsuiteChoose = $("#testsuiteLists option:selected").text();
	var testsuiteId = $("#testsuiteId").val();
	var layoutId = $("#layoutId").val();
	
	if(testsuiteChoose == testsuiteId && layoutNameOld == layoutNew) {
		$("#messageErrorss").show();
		$("#messageErrorss").text(getLang(currentLocale,"Please change name layout!"));
	} else if(layoutNew==""){
		$("#messageErrorss").show();
		$("#messageErrorss").text(getLang(currentLocale,"Cannot be blank!"));
	} else  {
		$("#copyLayoutScreen").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
		$("#copyLayoutScreen").prop("disabled", true);
		
		var dataLayout = "";
		$.each(layoutls, function(ind, vals){
			if(vals.id == layoutId){
				dataLayout = vals;
			}
		});
		var screenDesign = {
				"projectId": projectId,
				"testsuiteName": testsuiteChoose,
				"layoutName": layoutNew,
				"description": dataLayout.description,
				"layoutContent": dataLayout.layoutContent,
				"createBy": "",
				"createDate": new Date().toISOString()
		}
		
		$.ajax({
			url : jscontext+"/project/"+projectId+"/"+testsuiteChoose+"/saveCopyLayout/"+testsuiteId,
			type : "POST",
			data : JSON.stringify(screenDesign),
			contentType : "application/json",
			success : function(result) {
				if (!(result.errormessage) ) {
					$("#modalCopyLayout").modal("hide");
					swal({
				        title: getLang(currentLocale,"Copy Screen Design Success!!!"),
				        type: "success",
				        timer: 1000,
				        showConfirmButton: false
				    });
					var testsuiteName = $("#filterTestsuite").val();
					var createBy = $("#filterCreateBy").val();
					var createDate = $("#filterCreateDate").val();
					var fileName = $("#filterFileName").val();
					drawLayoutByFilter(testsuiteName, createBy, createDate, fileName);
					
					$("#copyLayoutScreen").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
					$("#copyLayoutScreen").prop("disabled", false);
				}
			},
			error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});	

		
	}
	
}

//layout db
function functionAddNewLayout(){
	$('#modalAddNewLayout').modal({
		backdrop: 'static'
	});
}

function getListTestsuite(status, callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				
				
				if(status == "true"){
					tsByProjectId = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.testsuite;
						obTs["text"] = values.testsuite;
						tsByProjectId.push(obTs);
					});
					callbackF(tsByProjectId);
				} else {
					callbackF(result.data);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getAllLayouts(){
	
	
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getLayouts",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				loadListLayout(result.data);
				$(".progress-loadMainContent").fadeOut("slow");
			} 
			$(".preloader").fadeOut("slow");
		},
		error : function(xhr,e) {
			console.log("ERROR: ", e);
			
			var statusCode = xhr.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load layouts"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function loadListLayout(layoutls){
	if(!$("#bodyLayoutRecord").empty()){
		$("#loadingScreenDesign").show();
	}else{	}
	if(layoutls.length == 0){
		var htmlRecordTestsuites =	'<div class="w-100 text-center text-danger">'+getLang(currentLocale,"The Layout is empty!!!")+'</div>'
		$("#bodyLayoutRecord").html(htmlRecordTestsuites);
	}else{
		var layoutRecord = layoutls;
		$.each( layoutRecord, function(index, values) {
			var htmlRecordLayout =  '<div class="col-md-4 itemLayout"><div class="lib-panel"><div class="row-panel box-shadow">'
									+'<div class="col-md-5 p-0" href="javascript:void(0);" testsuite-name="'+values["testsuiteName"]+'" data-name="'+values["layoutName"]+'" data-numb="'+values["id"]+'" onclick="generateDetailLayout(this,\''+ values["layoutName"] +'\')"> '
									+'<img class="img-responsive w-100" id="imageLayout" src="'+jscontext+"/project/"+projectId+"/"+values["testsuiteName"]+'/'+values["layoutName"]+ '/getOneImage">'
									+'</div>'
									+'<div class="col-md-7 p-0">'
									+'<div class="lib-row lib-header">'
									+'<a href="javascript:void(0);" data-name="'+values["layoutName"]+'" testsuite-name="'+values["testsuiteName"]+'" data-numb="'+values["id"]+'"onclick="generateDetailLayout(this,\''+ values["layoutName"] +'\')">'+values["layoutName"]+'</a><div class="lib-header-seperator"></div>'
									+'</div>'
									+'<div class="lib-row lib-desc">'
									+'<p>'+values["description"]+'</p>'
									+'<div class="w-100 btn-action-gr form-group">'
									+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success btnGenerateItem" testsuite-name="'+values["testsuiteName"]+'" data-name="'+values["layoutName"]+'" data-number="'+index+'" type="button" onclick="showModalGenerateUTC();" href="javascript:void(0);">'+getLang(currentLocale,"Generate UTCs")+'</button>'
									+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-warning btnExportCSV" type="button" testsuite-name="'+values["testsuiteName"]+'" data-name="'+values["layoutName"]+'" data-number="'+index+'" onclick="exportCSVFile(this);">'+getLang(currentLocale,"Export Screen Item")+'</button>'
									+'<a href="'+jscontext+'/project/'+projectId+'/'+values["testsuiteName"]+'/testDataDetail/'+values["layoutName"]+'"><button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-info" type="button" data-name="'+values["layoutName"]+'" onclick="">'+getLang(currentLocale,"Create Test Data")+'</button></a>'
									+'<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" testsuite-name="'+values["testsuiteName"]+'" data-name="'+values["layoutName"]+'" data-id="'+values.id+'" data-numb="'+index+'" type="button" href="javascript:void(0); " onclick="funcCopyLayout(this)">'+getLang(currentLocale,"Copy")+'</button>'
									+'</div>'
									+'<div class="box-action-btn">'
									+'<a class="mdl-button mdl-js-button mdl-js-ripple-effect btn-info btn-detailLayout" href="javascript:void(0);" testsuite-name="'+values["testsuiteName"]+'" data-name="'+values["layoutName"]+'" data-numb="'+values["id"]+'" onclick="generateDetailLayout(this,\''+ values["layoutName"] +'\')"><i class="ti-info-alt mr-2"></i>   '+getLang(currentLocale,"Detail")+'</a>'
									+'<a class="mdl-button mdl-js-button mdl-js-ripple-effect btn-default btn-deleteLayout" href="javascript:void(0);" testsuite-name="'+values["testsuiteName"]+'" data-id="'+values.id+'" data-name="'+values["layoutName"]+'" onclick="showConfirmMessageDeleteLayout(this)"><i class="fa fa-trash-o mr-2"></i>  '+getLang(currentLocale,"Delete")+'</a>'
									+'</div>'
									+'</div>'
									+'</div>'
									+'</div></div></div>';
			$("#bodyLayoutRecord").append(htmlRecordLayout);
		});
		
		funcGenerateTC();
	}
	
}

function showModalGenerateUTC(){
	$('#modalGenerateTestcase').modal({
		backdrop: 'static'
	});
}

//func get itemNameLayout
function funcGenerateTC(){
	$(".btnGenerateItem").click(function(){
		var indexItem = $(this).attr("data-number");
		var nameLayout = $(this).attr("data-name");
		var testsuiteName = $(this).attr("testsuite-name");
		$.each(layoutls, function (ind, vals) {
			if (ind == indexItem) {
				var currentLayout = JSON.parse(vals.layoutContent);
				var select = document.getElementById("typeSelect");
				select.options.length = 0;
				var htmlOptions="";
				for(var i=0; i< currentLayout.Controls.length; i++){
					for(var j=0; j < currentLayout.Controls[i].Attributes.length; j++){
						var vb = currentLayout.Controls[i].Attributes[j];
						if(vb.Name == "type" && (vb.Value == "submit" || vb.Value == "button" || vb.Value == "a")){
							htmlOptions += ' <option value="'+ currentLayout.Controls[i].Name +'">'+currentLayout.Controls[i].Name+'</option>';
						}
					}
				}
				$(".typeSelect").append(htmlOptions);
				$('#modalGenerateTestcase').on('shown.bs.modal', function () {
					  $('#btnGenerate').attr("data-name", nameLayout);
					  $('#btnGenerate').attr("testsuite-name", testsuiteName);
					})
			}
		} );
		
		getAllMilestoneInLayout(function(milestones){
			$("#milestoneGen").empty();
			var htmlOptions = "";
			var number = 0;
			$.each(milestones,function(index, value){
				var dateStart = new Date(value["startDate"]);
				var dateEnd = new Date(value["endDate"]);
				
				var dateNow = new Date().getTime();
				if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){
					if(number == 0){
						htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" selected>'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
						number ++;
					} else{
						htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
					}
				} else{
					htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
				}
			})
			$("#milestoneGen").append(htmlOptions);
		})
	});
	modalhideclearAttr();
}

function getCreateBy(callbackF){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getCreateByLayout",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var listCreateBy = result.data.map(function(obj){
					return {"id": obj, "text":obj}
				});		
			} else {
			}
			callbackF(listCreateBy);
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function drawLayoutByFilter(testsuiteName, createBySl, createDate, fileName){
	testsuiteNameParse = testsuiteName; 
	createByParse = createBySl;
	createDateSS = createDate;
	fileNameSS = fileName;
	var filterScreenDesign = {
			"testsuiteName": testsuiteName,
			"createBy": createBySl,
			"createDate": createDate,
			"fileName": fileName
	}
	$.ajax({
		url : jscontext +"/project/" + projectId +"/filterScreenDesign",
		type : "post",
		data : JSON.stringify(filterScreenDesign),
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				var tcFilter = result.data;
				loadListLayout(tcFilter);
			} 
			$(".preloader").fadeOut("slow");
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function filterScreenDesign(){
	var testsuiteName = $("#filterTestsuite").val();
	var createBySl = $("#filterCreateBy").val();
	var createDate = $("#filterCreateDate").val();
	var fileName = $("#filterFileName").val();
	
	$("#filterScrDesign").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filterScrDesign").prop("disabled", true);
	
	drawLayoutByFilter(testsuiteName, createBySl, createDate, fileName);
	
	$("#filterScrDesign").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filterScrDesign").prop("disabled", false);
}

$("#filterTestsuite").on('select2:opening', function(){
	checkSession = false;
	$("#filterTestsuite").one('change', function(){
		var testsuiteName = $("#filterTestsuite").val();
		var createBySl = $("#filterCreateBy").val();
		var createDate = $("#filterCreateDate").val();
		var fileName = $("#filterFileName").val();
		if(checkSession != true){
			drawLayoutByFilter(testsuiteName, createBySl, createDate, fileName);
		}
		
	});
});

$("#filterCreateBy").on('select2:opening',function(){
	checkSession = false;
	$("#filterCreateBy").one('change', function(){
		var testsuiteName = $("#filterTestsuite").val();
		var createBySl = $("#filterCreateBy").val();
		var createDate = $("#filterCreateDate").val();
		var fileName = $("#filterFileName").val();
		if(checkSession != true){
			drawLayoutByFilter(testsuiteName, createBySl, createDate, fileName);
		}
	});
});

$("#filterCreateDate").on('change', function(){
	$("#filterCreateDate").attr("disabled",false);
	checkSession = false;
	var testsuiteName = $("#filterTestsuite").val();
	var createBySl = $("#filterCreateBy").val();
	var createDate = $("#filterCreateDate").val();
	var fileName = $("#filterFileName").val();
	if(checkSession != true){
		drawLayoutByFilter(testsuiteName, createBySl, createDate, fileName);
	}
});

$("#filterFileName").on('change', function(){
	checkSession = false;
	var testsuiteName = $("#filterTestsuite").val();
	var createBySl = $("#filterCreateBy").val();
	var createDate = $("#filterCreateDate").val();
	var fileName = $("#filterFileName").val();
	if(checkSession != true){
		drawLayoutByFilter(testsuiteName, createBySl, createDate, fileName);
	}
});

function backScreenDesign(){
	$("#newTask").hide();
	$("#drawScreenDesign").show();
}
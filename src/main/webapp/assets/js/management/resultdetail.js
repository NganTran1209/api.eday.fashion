$(document).on("click", ".close" , function() {
	$('.modal-Img').hide();
});
function countNumberInfo(resultDetail){
	var totalTestcaseRun = resultDetail["Testcases"].length;
	$("#testcaseRun").html(totalTestcaseRun);
	
		var testcasePass = 0;
	$.each(resultDetail.Testcases, function(index, value){
		if(value.Result == "PASS"){
			testcasePass += 1;
		}
	});
	$("#testcasePass").html(testcasePass);
	$("#testcaseFail").html(totalTestcaseRun-testcasePass);
}

function drawResultDetail(resultDetail){
	infoBoxResult(resultDetail);
	generateResultDetail(resultDetail);
	
}

function infoBoxResult(values){
		console.log(values)
		var recordTestResult = values["TestResult"];
			
			if(values["TestResult"]["Result"] == "NOT YET"){
				status ='<span class="label label-info label-history">'+values["TestResult"]["Result"]+'</span>';
			}else if(values["TestResult"]["Result"] == "PASS"){
				status ='<span class="label label-success label-history">'+values["TestResult"]["Result"]+'</span>';
			} else if(values["TestResult"]["Result"] == "FAILED"){
				status ='<span class="label label-danger label-history">'+values["TestResult"]["Result"]+'</span>';
			}
			
			var videoRsLastRun = values["TestResult"]["Video"];
			var logfileName = window.location.href + "/" + values["ResultName"].replace("Result","Log") + ".txt";
			
		
			$("#box-testsuite").empty();
			$("#box-mode").empty();
			$("#box-environment").empty();
//			creat html tesuite
			var htmlRecordBoxResufts = 	' <div class="widget_summary"><h4>- '+ getLang(currentLocale,'Testsuite Name')+' : '+values['Testsuite']+'</h4></div>'
										+' <div class="widget_summary"><h4>- '+ getLang(currentLocale,'Run User')+' : '+values['RunningUser']+'</h4></div>'
										+' <div class="widget_summary form-group"><h4>- '+ getLang(currentLocale,'Result Status')+' : '+status+'</h4></div>'
										+' <div class="widget_summary">'
			if(videoRsLastRun != ""){
				htmlRecordBoxResufts = htmlRecordBoxResufts	+'<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2 call-modal-video">'
															+'<i class="fa fa-file-video-o icon-result" aria-hidden="true"></i> '+getLang(currentLocale,'Video')
															+'</a>';
				$("#player").attr("src",window.location.href + "/" + values['TestResult']['Video']);
			}					
			htmlRecordBoxResufts = htmlRecordBoxResufts +'<a href="'+ logfileName+'" class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success mr-2">'
								   +'<i class="fa fa-file-text-o icon-result" aria-hidden="true"></i> '+getLang(currentLocale,'File Log')
								   +'</a>';
			htmlRecordBoxResufts = htmlRecordBoxResufts +'</div>';
			$("#box-testsuite").append(htmlRecordBoxResufts);
			
//			creat html Mode
			var browserName = values['Testcases'][0]['TestScripts'][0]["Params"][0]["Value"];
			var htmlModeResuft = 	'<div class="widget_summary"><h4>- '+ getLang(currentLocale,'Mode')+' : '+values["Mode"]["Name"]+'</h4></div>'
									+' <div class="widget_summary"><h4>- '+ getLang(currentLocale,'Mobile Mode Type')+' : '+values['Mode']['Type']+'</h4></div>'
									+' <div class="widget_summary"><h4>- '+ getLang(currentLocale,'Browser')+' : '
									if(browserName !== null || browserName !== undefined || browserName !== ''){
										if(browserName == "chrome"){
											htmlModeResuft = htmlModeResuft +'Google Chrome';
										}else if(browserName == "ie"){
											htmlModeResuft = htmlModeResuft +'Internet Explorer 11';
										}else if(browserName == "edge"){
											htmlModeResuft = htmlModeResuft +'Microsoft Edge';
										}else if(browserName == "firefox"){
											htmlModeResuft = htmlModeResuft +'Mozilla Firefox';
										}
									}
			htmlModeResuft = htmlModeResuft +'</h4></div>'
											+'<div class="widget_summary"><h4>- '+ getLang(currentLocale,'Run Time')+ ' : '+formatDateTimeRs(values["ExecuteDate"])+'</h4></div>';
			$("#box-mode").append(htmlModeResuft);
			
//			creat html rnvironment
			var htmlBoxEnvironment = 	'<div class="widget_summary"><h4>- OS : '+values['Environment']['OS']+'</h4></div>'
										+'<div class="widget_summary"><h4>- '+ getLang(currentLocale,'OS Version')+' : '+values['Environment']['Name']+'</h4></div>'
										+'<div class="widget_summary"><h4>- '+ getLang(currentLocale,'Resolution')+' : '+values['Environment']['Resolution']+'</h4></div>'
										+'<div class="widget_summary"><h4>- '+ getLang(currentLocale,'Location ')+' : '+values['Environment']['Location']+'</h4></div>'
										+'<div class="widget_summary"><h4>- '+ getLang(currentLocale,'Time out')+' : '+values['Environment']['TimeOut']+'</h4></div>'
			$("#box-environment").append(htmlBoxEnvironment);
}


//Generate item details
function generateResultDetail(values){
			var recordTestcase = values["Testcases"];
			//console.log(recordTestcase);
			$.each(recordTestcase, function(indexTescRs, valuesTescRs) {
				var html = 	'<div id="home_'+indexTescRs+'" class="tab-pane">'
				+'<div class="x_content">'
				+'<ul id="listTsc'+indexTescRs+'" class="list-unstyled ul-list-steps">'
				if(testCaseName != ""){
					if(testCaseName == valuesTescRs["Name"]){
						html += detailMoreResult(indexTescRs, valuesTescRs, html);
					}
				} else {
					html += detailMoreResult(indexTescRs, valuesTescRs, html);
				}
								
						 
				html = html +'</ul>'
		 				+'</div>'
		 				+'</div>';
				
		 		$("#listTsc"+indexTescRs).append(html);
		 		$(".TabsContent-results").append(html);
			});
			var formImg = '<div id="modalImgResult" class="modal-Img">'
			 	+'<span class="close" id="close">&times;</span>'
			 	+'<img class="modal-content-img" id="pictureResultTest">'	
			 +'</div>';
			$(".TabsContent-results").append(formImg);
	callBackJs();
}

function checkDisplayStatusScript(resultScript, html){
	if (resultScript['ResultCode'] == "1" && resultScript['ResultCode'] !== ''){
    	html = html+'<span class="label label-success label-result label-result-success scr-recod">'+getLang(currentLocale,'PASSED')+'</span>';
	} else if(resultScript['ResultCode'] == "3" && resultScript['ResultCode'] !== ''){
		html = html	+'<span class="label label-danger label-result form-group scr-recod">'+getLang(currentLocale,'Exception')+'</span>';
		
	} else if(resultScript['ResultCode'] == "2" && resultScript['ResultCode'] !== ''){
		html = html	+'<span class="label label-danger label-result form-group scr-recod">'+getLang(currentLocale,'FAILED')+'</span>';
		
	} else{
		html = html	+'<span class="label label-danger label-result form-group scr-recod">'+getLang(currentLocale,'Exception')+'</span>';
		
	}
	return html;
}

function checkActionDisplayData(tabIndex, valuesTestscriptlsRs, html, index){
	var resultExpected = valuesTestscriptlsRs["Result"]["ResultObj"];
	var withoutLastChunk = "";
	if(resultExpected == undefined){
		var htmlListImg = "";
	}else if(resultExpected.includes('testData')){
		nameFileImage = resultExpected.split("/")[1];
		var winHref = window.location.href;
		var img = "";
		for(var i = 0; i < 2; i++){
		    withoutLastChunk = winHref.slice(0, winHref.lastIndexOf("/"));
		    winHref = withoutLastChunk;
		}
		if(nameFileImage == "{}"){
			var htmlListImg = '<div class="p-0 component-file-upload-result " id="component-file-upload-result">'
			     +'<img src="'+jscontext+"/project/"+projectId+"/"+testsuiteId+'/'+"layoutBTPTS"+ '/getOneImage" alt=""class="img-thumbnail border-0 path-img-result" id="img_' +tabIndex + '_' + index+'">'
		      +'</div>';
		} else {
			var htmlListImg = '<div class="p-0 component-file-upload-result " id="component-file-upload-result">'
			     +'<img src="'+withoutLastChunk+"/" + "TestDatas" + "/" + nameFileImage+'" alt=""class="img-thumbnail border-0 path-img-result" id="img_' +tabIndex + '_' + index+'">'
		      +'</div>';
		}
		
	} else if(resultExpected.includes('testResult')){
		var nameFileImage = resultExpected.split("/")[1];
		if(nameFileImage == "{}"){
			var htmlListImg = '<div class="p-0 component-file-upload-result ">'
				 +'<img src="'+jscontext+"/project/"+projectId+"/"+testsuiteId+'/'+"layoutBTPTS"+ '/getOneImage" alt=""class="img-thumbnail border-0 path-img-result" id="img_'+tabIndex + '_' + index+'">'
		      +'</div>';
		} else {
			var htmlListImg = '<div class="p-0 component-file-upload-result ">'
				 +'<img src="'+window.location.href + "/" + nameFileImage+'" alt=""class="img-thumbnail border-0 path-img-result" id="img_'+tabIndex + '_' + index+'">'
		      +'</div>';
		}
		
	} else {
		if(resultExpected == "{}"){
			var htmlListImg = '<div class="p-0 component-file-upload-result ">'
				 +'<img src="'+jscontext+"/project/"+projectId+"/"+testsuiteId+'/'+"layoutBTPTS"+ '/getOneImage" alt=""class="img-thumbnail border-0 path-img-result" id="img_'+tabIndex + '_'+ index+'" >'
			  +'</div>';
		} else {
			var htmlListImg = '<div class="p-0 component-file-upload-result ">'
				 +'<img src="'+window.location.href + "/" + resultExpected+'" alt=""class="img-thumbnail border-0 path-img-result" id="img_'+tabIndex + '_'+ index+'" >'
			  +'</div>';
		}
			
	}
	

	
	if(valuesTestscriptlsRs['ClassName'] == 'AC_CompareImage' || valuesTestscriptlsRs['ClassName'] == 'AC_TakeScreenshot' || valuesTestscriptlsRs['ClassName'] == 'AC_TakeFullScreen' || valuesTestscriptlsRs['ClassName'] == 'AC_TakeScreenshotWithResult' || valuesTestscriptlsRs['ClassName'] == 'AC_CaptureControl'){
		html += htmlListImg ;

	} else if(valuesTestscriptlsRs['ClassName'] == 'AC_CheckControlValue' || valuesTestscriptlsRs['ClassName'] == 'AC_CheckControlAttribute'){
	    html += htmlListImg ;
		
	} else if(valuesTestscriptlsRs['ClassName'] == 'AC_WriteToCSV' ){
		if(resultExpected.includes('testData')){
			
			html = html +'<a class="label label-info scr-recod label-result" target="_blank" href="' +withoutLastChunk+"/" + "TestDatas" + "/" + nameFileImage+ '" >'+nameFileImage+'</a>';
			
		} else if(resultExpected.includes('testResult')){
			html = html +'<a class="label label-info scr-recod label-result" target="_blank" href="' +window.location.href + "/" + nameFileImage+ '" >'+nameFileImage+'</a>';
		} else {
			html = html +'<a class="label label-info scr-recod label-result" target="_blank" href="' +window.location.href + "/" + resultExpected+ '" >'+nameFileImage+'</a>';
		}
	
	}
	return html;
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function detailMoreResult(indexTescRs, valuesTescRs, html) {
	var testscriptlsRs = valuesTescRs["TestScripts"];
	var tabsTestcaseRs = '<li><a data-toggle="tab" href="#home_'+indexTescRs+'">Testcases: '+valuesTescRs['Name']+'</a></li>';
	$(".navTabs-results").append(tabsTestcaseRs);
	
	$.each(testscriptlsRs, function(indexTestscriptlsRs, valuesTestscriptlsRs) {
		var index = indexTestscriptlsRs + 1;
		
		html += '<li class="formStyleRs">'
			+'<div class="row step form-group">'
				+'<div class="col-md-2 col-contents">'
					+'<label class="font-weight-bold">'+index+'. '+getLang(currentLocale,'Step name')+':</label>'
					+'<div>'+valuesTestscriptlsRs['Language']['en']['StepName']+'</div>'
				+'</div>'
				+'<div class="col-md-4 col-contents">'
					+'<div class="list-colum-Jsdata">'
						+'<div class="w-100 itemSchedule"><label class="font-weight-bold">'+getLang(currentLocale,'Action')+':</label>'
							+'<label>&nbsp '+valuesTestscriptlsRs['Language']['en']['ActionName']+'</label>'
						+'</div>'
						$.each( valuesTestscriptlsRs['Params'], function(indexDetailPr, valuesTestscriptlsRsPr) {
							html += '<div class="w-100 itemSchedule"><label class="font-weight-bold">'+valuesTestscriptlsRsPr['Name']+':</label>'
								+'<label>&nbsp '+JSON.stringify(valuesTestscriptlsRsPr['Value'])+'</label>'
							+'</div>';
						});
						
					html +='</div>'
				+'</div>'
				+'<div class="col-md-2  ml-auto">'
					+'<div class="page-title d-flex w-100 m-0 mb-1" style="font-size:18px"></div> '
					+'<div class="list-colum-Jsdata">';
							html = checkDisplayStatusScript(valuesTestscriptlsRs['Result'], html);
						html += '<div class="w-100 itemSchedule"><label class="font-weight-bold">Message: </label>'
							if (valuesTestscriptlsRs["Result"]["ResultMsg"] != ""){
								html += '<div class="scroll-content-manualTestCase" >'
									+'<textarea class="font-italic form-control" readonly="readonly" rows="8" style="font-style: italic; color: red;background-color: transparent;border: 0;resize: none;">'+valuesTestscriptlsRs["Result"]["ResultMsg"]+'</textarea>'
								+'</div>'
							}
							
						html += '</div>'
						
						html += '<div class="w-100 itemSchedule"><label class="font-weight-bold">Actual: </label>'
							if(valuesTestscriptlsRs["Result"]["ResultRecorded"] != ""){
								var conVert = valuesTestscriptlsRs["Result"]["ResultRecorded"];
								if(IsJsonString(valuesTestscriptlsRs["Result"]["ResultRecorded"])){
									conVert = JSON.stringify(JSON.parse(valuesTestscriptlsRs["Result"]["ResultRecorded"]), undefined, 2);
								}
								html += '<div class="scroll-content-manualTestCase" >'
									+'<textarea class="font-italic form-control" readonly="readonly" rows="8" style="font-style: italic; color: #1b6fb9f7;background-color: transparent;border: 0;resize: none;">'+conVert+'</textarea>'
								+'</div>'
							}
						html += '</div>'
							
						html += '<div class="w-100 itemSchedule"><label class="font-weight-bold">Expected: </label>'
							if(valuesTestscriptlsRs["Result"]["ResultExpected"] != ""){
								
								var conVert = valuesTestscriptlsRs["Result"]["ResultExpected"];
								if(IsJsonString(valuesTestscriptlsRs["Result"]["ResultExpected"])){
									conVert = JSON.stringify(JSON.parse(valuesTestscriptlsRs["Result"]["ResultExpected"]), undefined, 2);
								}
								html += '<div class="scroll-content-manualTestCase" >'
									+'<textarea class="font-italic form-control" readonly="readonly" rows="8" style="font-style: italic; color: #1b6fb9f7;background-color: transparent;border: 0;resize: none;">'+conVert+'</textarea>'
								+'</div>'
							}
						html += '</div>'
					+'</div>'
				+'</div>'
				+'<div class="col-md-4 col-contents">'
					html = checkActionDisplayData(indexTescRs,valuesTestscriptlsRs, html, index);
				html += '</div>'
			+'</div>'
		+'</li>';
				
		$(document).on('click', '#' + 'img_'+indexTescRs+'_'+index, function(){
			$("#pictureResultTest").attr("src",this.src);
			$(".modal-Img").show();
			 document.body.scrollTop = 0;
			 document.documentElement.scrollTop = 0;
		});
	});
	
	return html;
}

function callBackJs(){
    $(".video").css({
        "width": $("#player").css("width"),
        "height": $("#player").css("height")
    });

    $(".call-modal-video").click(function () {
        $(".video-wrapper").fadeIn('fast', function () {
            $(".video").fadeIn();
            $(".video").center();
        });
    });

    $(".video-wrapper").click(function (e) {
        if ($(e.target).is(".video-wrapper")) {
            $(".video").fadeOut(function () {
                $(".video-wrapper").fadeOut(function () {
                    $(".video, .video-wrapper").css({ 'display': 'none' });
                    var src = $("#player").attr("src");
                    $("#player").attr("src", "");
                    $("#player").attr("src", src);
                });
            });
        }
    });

    $(document).keyup(function (e) {
        var isShown = $(".video-wrapper").css("display");

        if (isShown !== "none" && e.which == 27) {
            $(".video-wrapper").click();
        }

    });

    $(".navTabs-results li:first-child a").addClass("active show");
    $(".TabsContent-results .tab-pane:first-child").addClass("active show");
    var hidWidth;
    var scrollBarWidths = 40;

    var widthOfList = function(){
	    var itemsWidth = 0;
	    $('.list li').each(function(){
	        var itemWidth = $(this).outerWidth();
	        itemsWidth+=itemWidth;
	    });
	    return itemsWidth;
    };
    
    var pageWidth = function () {
    	var liElementWidth = $('.list li')[0].getBoundingClientRect().width;
    	var sumLi = 0;
    	while ($('.wrapper').outerWidth() - sumLi > liElementWidth) {
    		sumLi += liElementWidth;
    	}
    	return sumLi;
    };

//    var widthOfHidden = function(){
//    	return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
//    };

    var getLeftPosi = function(){
    	return $('.list').position().left;
    };

    var reAdjust = function(){
	    if (($('.wrapper').outerWidth()) < widthOfList()) {
	        $('.scroller-right').show();
	    }
	    else {
	        $('.scroller-right').hide();
	    }
	    
	    if (getLeftPosi()<0) {
	        $('.scroller-left').show();
	    }
	    else {
	        $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
	        $('.scroller-left').hide();
	    }
    }

    reAdjust();

    $(window).on('resize',function(e){  
        reAdjust();
    });
//    $('.scroller-right').click(function(event) {
//    	var checkMax = getLeftPosi() + widthOfList();
//    	if (checkMax < $('.wrapper').outerWidth()*2) {
//    		$('.scroller-right').fadeOut('slow');
//    	}
//    	$('.scroller-left').fadeIn('slow');
//
//	    $('.list').animate({left:"-="+pageWidth()+"px"},'slow',function(){
//	    });
//	    
//    });
    $('.scroller-right').click(function(event) {
		$(".scroller-right").prop("disabled",true);
		$(".scroller-left").prop("disabled",false);
    	var checkMax = getLeftPosi() + widthOfList();
    	if (checkMax < $('.wrapper').outerWidth()) {
    		$('.scroller-right').fadeOut();
    	}else{
			$('.list').animate({left:"-="+pageWidth()+"px"},'complete',function(){
				$(".scroller-right").prop("disabled",false);
			});
		}
    	$('.scroller-left').fadeIn();
    });

//    $('.scroller-left').click(function(event) {
//
//    	if (getLeftPosi() >= -pageWidth()) {
//    		$('.scroller-left').fadeOut('slow');
//    		
//    	} 
//    	$('.scroller-right').fadeIn('slow');
//
//    	$('.list').animate({left:"+="+pageWidth()+"px"},'slow',function(){
//    		
//    	});
//    	
//    });
    $('.scroller-left').click(function(event) {
		$(".scroller-left").prop("disabled",true);
		$(".scroller-right").prop("disabled",false);
    	if (getLeftPosi() >= -pageWidth()) {
    		$('.scroller-left').fadeOut();
    	}else{
			$('.list').animate({left:"+="+pageWidth()+"px"},'complete',function(){
				$(".scroller-left").prop("disabled",false);
			});
		}
    	$('.scroller-right').fadeIn();
    });
   
}


//format string to datetime in list resuft
function formatDateTimeRs(theDateTime){
	var strDateTimeRs = theDateTime.replace("_","");
	

	var year = strDateTimeRs.substring(0, 4);
	var month = strDateTimeRs.substring(4, 6);
	var day = strDateTimeRs.substring(6, 8);
	var time = strDateTimeRs.substring(8, 10);
	var minute = strDateTimeRs.substring(10, 12);
	var seconds = strDateTimeRs.substring(12, 14);
	
	var displayDate = year + '-' + month  + '-' + day + 'T' + time + ':' + minute + ':' + seconds ;
	var executeDate = new Date(Date.UTC(year, month -1, day, time, minute, seconds));
	
	return executeDate.toLocaleString(); 
} 

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}
 

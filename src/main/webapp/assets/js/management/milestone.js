$(document).ready(function(){
	var formMode = 'add';
	hidenModalMilestone();
	$("#calProjectMilestone").click(function(){
		getDateTimePicker();
	});
	
	$("#milestonePredifinition").select2({
		width:"100%",
		allowClear: true,
		minimumResultsForSearch: Infinity,
		placeholder: getLang(currentLocale,"Select a state"),
		dropdownParent: $('#modalAddTestcaseToStone')
	});
	
	$("#filterTestsuite").select2({
		placeholder: getLang(currentLocale,"Select Testsuite"),
		width:"100%",
		allowClear: true,
		dropdownParent: $('#modalAddTestcaseToStone')
	});
	
	$("#filterStatus").select2({
		placeholder: getLang(currentLocale,"Select status"),
		width:"100%",
		minimumResultsForSearch: Infinity,
		allowClear: true,
		dropdownParent: $('#modalAddTestcaseToStone')
	});
	
	validateAddMilestone();
	drawParentTaskInTestcase(taskList);
});

var listMilestone;
var dataDetails;
var projectId;
function getAllMilestone(){
	$("#loadlistMilestone").show();
	$("#listMilestone").empty();
//	getMileStones
	$.ajax({
		url : jscontext+"/project/"+glProjectId+"/getMileStones",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null || result.errormessage == "") {
				listMileStone = result.data;
				drawProjectMilestone(listMileStone, projectId);
			}
			$("#loadlistMilestone").hide();
		  },
		  error : function(request, status, error)
		  {
			  
			  var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					swal({
				        title: getLang(currentLocale,"Can get data!!!"),
				        type: "warning",
				        timer: 2000,
				        showConfirmButton: false
				    });
				}
		  }
	
	});
}

function drawProjectMilestone(listMileStone, projectId){
	$("#listMilestone").empty();
	for(var i in listMileStone){
		var dateStart = new Date(listMileStone[i].startDate);
		var startDate = dateStart.getFullYear() + "/" + (dateStart.getMonth() + 1) + '/'+ dateStart.getDate();
		
		var dateEnd = new Date(listMileStone[i].endDate);
		var endDate = dateEnd.getFullYear() + "/" + (dateEnd.getMonth() + 1) + '/'+ dateEnd.getDate();
		
		var html =	'<div class="col-md-12 px-1 item-milestone">'
			+ '<div class="card card-box rounded-0 border-0 mb-3">'
				+'<div class="card-head card-head-icon border-0 border-secondary bg-success text-light">'
					+'<header class="py-2 d-inline-block font-weight-normal w-100">'	
						+ '<i class="icon-info"></i>' + i + '<span class="text-light font-italic title-sub-datetime ml-1">'+' (' + startDate + " - " + endDate + ')' + '</span>'
					+ '</header>'
					+'<div class="tools d-flex ml-auto">'
						+'<a class="icon-note btn-color mr-3 btn-edit-project text-light"'
							+'href="javascript:;" onclick="getEditMileStone(\''+i+'\');" data-toggle="tooltip" data-placement="top" title='+getLang(currentLocale,"Edit Milestone")+'></a> '
						+'<a class="ti-trash btn-color btn-edit-project text-light" href="javascript:;" onclick="showConfirmMessagesMileStone(\''+i+'\');"'
							+'data-toggle="tooltip" data-placement="top" title='+getLang(currentLocale,"Delete MileStone")+'></a> '
					+'</div>'
				+'</div>'
				+'<div class="card-body collapse-height border border-top-0">'
					+'<div class="w-100">'
			           	+'<div class="w-100 mb-3">'
			           		+'<div class="infoProjectLabel">'
			           			+'<label>'+getLang(currentLocale,'Description')+'</label>'
			           			+'<textarea class="text-muted font-italic form-control" readonly="readonly" rows="3">'+listMileStone[i].description+'</textarea>'
			           		+'</div>'
		           		+'</div>'
		           	+'</div>'
		           	+'<div class="row" style="margin-left:0px">'
			           	+ '<div class="element-chart chart-legend-middle justify-content-start font-chart col-md-9">'
							+ '<div class="box-chart font-middle p-0" id="chartMilestone'+i+'"></div>'
							+ '<div class="legend changeLangue" id="elementMilestone'+i+'"></div>' 
						+'</div>' 
						+ '<div class="col-md-3">'
							+'<a class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success my-auto button-sm"'
								+'href="javascript:;" onclick="detailTestcaseMilesStone(\''+i+'\');"><i class="ti-info-alt mr-2"></i>'+getLang(currentLocale,"Detail Testcase Milestone")+'</a> '
						+'</div>' 
		           	+'</div>'
		           	+'<div class="row" style="padding-top:25px">'
			           	+'<div class="col-md-12" id="frameIssueMilestone'+i+'" style="display: none">'
							+'<div class="card card-box" >'
								+'<div class="card-head card-head-icon">'
				             		+'<div class="col-md-3">'
				             			+'<h4>Issue Status</h4>'
				             		+'</div>'
				             		+ '<div class="col-md-9 legend headerLe w-100 text-right mr-3" id="legendIssue'+i+'" style="padding-right:0px"></div>'
								
								+'</div>'
								+'<div class="card-body no-padding height-9">'
				             		+'<div class="text-danger" style="text-align: center;" id="issueCircleEmpty"></div>'
				             		+'<div id="progressIssueMilestone'+i+'" class="row"> </div>'
				             	+'</div>'
			             	+'</div>'
			             +'</div>'
						
		           	+'</div>'
	           +'</div> '
			+ '</div>'
		+'</div>';
		$("#listMilestone").append(html);
		var testcasePass = 0;
		var testcaseFail = 0;
		var testcaseDegrade = 0;
		var testcaseNotRun = 0;
		if(listMileStone[i].testcaseName != "NOT-RUN"){
			testcasePass = listMileStone[i].testcasePass;
			testcaseFail = listMileStone[i].testcaseFail;
			testcaseDegrade = listMileStone[i].testcaseDegrade;
			testcaseNotRun = listMileStone[i].totalTestcase - (testcasePass + testcaseFail + testcaseDegrade);
		} else {
			testcaseNotRun = listMileStone[i].totalTestcase;
		}
		
		var dataChart = [testcasePass,testcaseFail,testcaseDegrade,testcaseNotRun];
		drawChartMilestonetList(dataChart,i);
		getListStatusByIdMilestone(projectId, function(listStatus){
			drawIssueMilestone(projectId, i, listStatus);
		});
		
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	}
}
function getListStatusByIdMilestone(projectId, callback){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/getListStatus",
		type : "GET",
		data : "",
		async: false,
		contentType : "application/json",
		
		success : function(result) {
			if (!(result.errormessage) ) {
				var listStatusId = result.data;
				callback(listStatusId);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}
/*function drawChartIssueInMilestone(projectId){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/listIssueAdd",
		type : "GET",
		data : "",
		async: false,
		contentType : "application/json",
		
		success : function(result) {
			if (!(result.errormessage) ) {
				listIssue = JSON.parse(result.data);
				pieByIssueMilestone(listIssue);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
	
}
*/
function getDataDetailMilestone(projectId, milestoneName){
	$("#listBodyDetailTestcaseMilestone").empty();
	
	$.ajax({
		url : jscontext +"/project/" + projectId +"/"+ milestoneName +"/getDetailMileStone",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				 dataDetails = result.data;
				
				var outputArray = []; 
		        var count = 0; 
		        var start = false; 
		        for (j = 0; j < dataDetails.length; j++) { 
		            for (k = 0; k < outputArray.length; k++) { 
		                if ( dataDetails[j].testsuite == outputArray[k] ) { 
		                    start = true; 
		                } 
		            } 
		            count++; 
		            if (count == 1 && start == false) { 
		                outputArray.push(dataDetails[j].testsuite); 
		            } 
		            start = false; 
		            count = 0; 
		        }
		        
		        
		        $("#filterTestsuite").empty();
		        $("#filterTestsuite").append('<option></option>')
		        $.each(outputArray, function (ind,value){
		        	var html = '<option value="'+value+'">'+value+'</option>';
		        	$("#filterTestsuite").append(html);
		        });
		        $.each(dataDetails, function(index,vals){
		        	getAllDetail(index,vals);
		    	});
		        
				
				$("#loadTableDetailMilestone").fadeOut();
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getAllDetail(index,vals){
	var checked = "";
	var classColor = "checkbox-green";
	var classText = "";
	$.each(dataTables, function(inds, item){
		if(vals["testsuite"] == item.projectMilestoneTestcaseIdentity["testsuite"] && vals["testcase"] == item.projectMilestoneTestcaseIdentity["testcase"]){
			checked = "checked";
			classColor = "checkbox-green";
			classText = "";
		}
	});
	if(vals["status"] != "NOT-RUN"){
		checked = "checked";
		classColor = "checkbox-red";
		classText = "text-danger";
		$.each(dataTables, function(inds, item){
			if(vals["testsuite"] == item.projectMilestoneTestcaseIdentity["testsuite"] && vals["testcase"] == item.projectMilestoneTestcaseIdentity["testcase"]){
				checked = "checked";
				classColor = "checkbox-green";
				classText = "";
				return;
			}
		}); 
	}
	var html = '<tr class="w-100 d-flex '+classText+'">'
           			+'<td width="5%">'
           			   	+'<div class="checkbox '+classColor+' form-check form-check-inline checkbox-fix-top p-0 m-0">'
							+'<input type="checkbox" name="mobileMode" id="checked_'+index+'" '+checked+'>'
							+'<label for="checked_'+index+'" class="mt-0"></label>'
						+'</div>'
           			+'</td>'
           			+'<td width="25%">'+vals["testsuite"]+'</td>'
           			+'<td width="25%">'+vals["testcase"]+'</td>'
           			+'<td width="25%">'+(vals["description"] != null ? vals["description"] : "-")+'</td>'
           			+'<td width="20%">';
						if(vals["status"] == "PASS"){
							html += '<label class="label label-success label-history mb-1 d-inline-block">'+vals["status"]+'</label>';
						} else if(vals["status"] == "FAILED"){
							html += '<label class="label label-danger label-history mb-1 d-inline-block">'+vals["status"]+'</label>';
						} else if(vals["status"] == "DEGRADE"){
							html += '<label class="label label-degrade label-history mb-1 d-inline-block">'+vals["status"]+'</label>';
						} else {
							html += '<label class="label label-info label-history mb-1 d-inline-block">'+vals["status"]+'</label>';
						}
						if(vals["execute_date"] == null) {
							html += '<div>-</div>';
						} else {
							var dateStart = new Date(vals["execute_date"]);
							dateStart = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate()+" "+dateStart.getHours()+":"+dateStart.getMinutes()+":"+dateStart.getSeconds(); 
							html += '<div>'+dateStart+'</div>'
						}
           				
           			+'</td>'
           		+'</tr>';
	$("#listBodyDetailTestcaseMilestone").append(html);
	
}
var dataTables;
function detailTestcaseMilesStone(milestone){
	$("#loadTableDetailMilestone").show();
	getDataMilestoneTestcase(projectId, milestone,function(){
		getDataDetailMilestone(projectId, milestone);
	});
	$('#modalAddTestcaseToStone').modal({
		backdrop: 'static'
	});
	$("#listBodyDetailTestcaseMilestone").empty();
	$("#milestoneNameText").val(milestone);
	
	$("#filterTestsuite").on('change', function(){
		var testsuiteChoose = $("#filterTestsuite").val();
		var testStatus = $("#filterStatus").val();
		var predifinition = $("#milestonePredifinition").val();
		$("#listBodyDetailTestcaseMilestone").empty();
		if(testsuiteChoose != "" && testStatus != "" && predifinition == ""){
			$.each(dataDetails, function(index,vals){
				if(vals["testsuite"] == testsuiteChoose && vals["status"] == testStatus){
					getAllDetail(index,vals);
				}
			});
		} else if(testsuiteChoose != "" && testStatus == "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose){
						getAllDetail(index,vals);
					}
		    	});
			}
		} else if (testsuiteChoose != "" && testStatus != "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];

			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;

			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose && vals["status"] == testStatus){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose && vals["status"] == testStatus){
						getAllDetail(index,vals);
					}
		    	});
			}
			
		} else if (testsuiteChoose != ""){
			var count = 0;
			$.each(dataDetails, function(index,vals){
				if(vals["testsuite"] == testsuiteChoose){
					count += 1;
					getAllDetail(index,vals);
				}
			});
			if (count == 0) {
				var html = '<p style="color:red; text-align: center;">'+getLang(currentLocale,"Not data!!!")+'</p>';
				$("#listBodyDetailTestcaseMilestone").append(html);
			}
			
		} else {
			$.each(dataDetails, function(index,vals){
	        	getAllDetail(index,vals);
	    	});
		}
		
	});
	
	$("#filterStatus").on('change', function(){
		var statusChoose = $("#filterStatus").val();
		var testsuiteChoose = $("#filterTestsuite").val();
		var predifinition = $("#milestonePredifinition").val();
		$("#listBodyDetailTestcaseMilestone").empty();
		if(statusChoose != "" && testsuiteChoose != "" && predifinition == ""){
			$.each(dataDetails, function(index,vals){
				if(vals["status"] == statusChoose && vals["testsuite"] == testsuiteChoose){
					getAllDetail(index,vals);
				}
			});
		} else if(statusChoose != "" && testsuiteChoose == "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}
		    	});
			}
		} else if (testsuiteChoose != "" && statusChoose != "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose && vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose && vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}
		    	});
			}
			
		} else if(statusChoose != ""){
			var count = 0;
			$.each(dataDetails, function(index,vals){
				if(vals["status"] == statusChoose){
					count += 1;
					getAllDetail(index,vals);
				}
			});
			if (count == 0) {
				var html = '<p style="color:red; text-align: center;">'+getLang(currentLocale,"Not data!!!")+'</p>';
				$("#listBodyDetailTestcaseMilestone").append(html);
			}
		} else {
			$.each(dataDetails, function(index,vals){
	        	getAllDetail(index,vals);
	    	});
		}
	});
	
	$("#milestonePredifinition").on('change', function(){
		var predifinition = $("#milestonePredifinition").val();
		var statusChoose = $("#filterStatus").val();
		var testsuiteChoose = $("#filterTestsuite").val();
		$("#listBodyDetailTestcaseMilestone").empty();
		if(statusChoose != "" && testsuiteChoose == "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}
		    	});
			}
		} else if(testsuiteChoose != "" && statusChoose == "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose){
						getAllDetail(index,vals);
					}
		    	});
			}
		} else if (testsuiteChoose != "" && statusChoose != "" && predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				$.each(listTestData, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose && vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}

		    	});
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					if(vals["testsuite"] == testsuiteChoose && vals["status"] == statusChoose){
						getAllDetail(index,vals);
					}
		    	});
			}
			
		} else if(predifinition != ""){
			var listTestData = [];
			var listTestUnchecked = [];
			
			var values = checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails);
			listTestData = values.first;
			listTestUnchecked = values.second;
			
			if(predifinition == "checked"){
				var count = 0;
				$.each(listTestData, function(index,vals){
					count += 1;
					getAllDetail(index,vals);

		    	});
				if (count == 0) {
					var html = '<p style="color:red; text-align: center;">'+getLang(currentLocale,"Not data!!!")+'</p>';
					$("#listBodyDetailTestcaseMilestone").append(html);
				}
			} else if(predifinition == "unchecked"){
				$.each(listTestUnchecked, function(index,vals){
					count += 1;
					getAllDetail(index,vals);
		    	});
				if (count == 0) {
					var html = '<p style="color:red; text-align: center;">'+getLang(currentLocale,"Not data!!!")+'</p>';
					$("#listBodyDetailTestcaseMilestone").append(html);
				}
			}
			
		} else {
			$.each(dataDetails, function(index,vals){
	        	getAllDetail(index,vals);
	    	});
		}
	});
}

function comparer(otherArray){
  return function(current){
    return otherArray.filter(function(other){
      return other.testsuite == current.testsuite && other.testcase == current.testcase
    }).length == 0;
  }
}

function checkUncheckPredifinition(listTestData, listTestUnchecked, dataDetails){
	$.each(dataDetails, function(index,vals){
		$.each(dataTables, function(ind,value){
			if(vals["testsuite"] == value.projectMilestoneTestcaseIdentity["testsuite"] && vals["testcase"] == value.projectMilestoneTestcaseIdentity["testcase"]){
				listTestData.push(vals);
	        }
		});
	    if(vals["status"] != "NOT-RUN"){
			var d = 0;
	        $.each(listTestData, function(i, item){
	            if(vals["testsuite"] == item["testsuite"] && vals["testcase"] == item["testcase"]){
					d += 1;
	                return;
	            } 
	        });
	        if(d == 0){
	            listTestData.push(vals);
	        }
				
	    }
	});
	
	var onlyInA = dataDetails.filter(comparer(listTestData));
	var onlyInB = listTestData.filter(comparer(dataDetails));

	listTestUnchecked = onlyInA.concat(onlyInB);
	return {
		first: listTestData,
        second: listTestUnchecked,
	};
}

function drawChartMilestonetList(dataChart,id){
	var chartData = {
	    CC: [{
	      code: id,
	      datasets: [{
		        data:dataChart,
		        backgroundColor:["#1dcb8b","#CB371D","#AB47BC","#f5dc01"],
		        label: 'Testcase Project'
		    }],
		    labels: [
		    	getLang(currentLocale,"Testcase Passed"),
		    	getLang(currentLocale,"Testcase Fail"),
		    	getLang(currentLocale,"Testcase Degrade"),
		    	getLang(currentLocale,"Test Not Tested")
  	    	    ]
	    }]
	  };
	chartData.CC.forEach(function(data, index){
	    var canvas = document.createElement('canvas');
	    canvas.setAttribute("width", "150");
	    canvas.setAttribute("height", "150");
	    canvas.setAttribute("id", 'chart' + data.code);
        chartId = 'chart' + data.code;
	    var idCanvasChart = document.getElementById("chartMilestone"+id);
	    if($(idCanvasChart).length > 0){
	    	idCanvasChart.appendChild(canvas);
		    var context = document.getElementById(chartId).getContext('2d');
		   // var drawChartProjectList = document.getElementById(chartId).getContext("2d");
			  
		    if(dataChart.toString() == "0,0,0,0"){
		    	var configCreatProCircle = {
					type: 'pie',
					data: data,
					data: {
			    	  datasets: [{
			    	        data:[1],
			    	        backgroundColor:["#9E9E9E"],
			    	        label: 'Testcase Project'
			    	    }],
			    	    labels: [
			    	        "Testcase",
			    	    ]
					},
					options: {
						elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
					    tooltips: {enabled: false},
					    hover: {mode: null},
					    legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li ><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  				},
					  }
			    }
		    }else{
		    	var configCreatProCircle = {
		  	      type: 'pie',
		  	      data: data,
		  	      data: {
		  	    	    datasets: [{
		  	    	        data:dataChart,
		  	    	        backgroundColor:["#1dcb8b","#CB371D","#AB47BC","#f5dc01"],
		  	    	        label: 'Testcase Project'
		  	    	    }],
		  	    	    labels: [
		  	    	    	getLang(currentLocale,"Testcase Passed"),
		  			    	getLang(currentLocale,"Testcase Fail"),
		  			    	getLang(currentLocale,"Testcase Degrade"),
		  			    	getLang(currentLocale,"Test Not Tested")
		  	    	    ]
		    	
		  	    	},
		  	    	 options: {
		  	    		elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
		  				responsive: true,
		  				scaleBeginAtZero: false,
		  				legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li ><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  		             
		  				},
		  				events: true,
		  				animation: {
		  					duration: 500,
		  					easing: "easeOutQuart",	
		  					onComplete: function () {
		  					  var ctx = this.chart.ctx;
		  					  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		  					  ctx.textAlign = 'center';
		  					  ctx.textBaseline = 'bottom';
		  					
		  					  this.data.datasets.forEach(function (dataset) {
		  						
		  						for (var i = 0; i < dataset.data.length; i++) {
		  							
			  						  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
				  						      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
				  						      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
				  						      start_angle = model.startAngle,
				  						      end_angle = model.endAngle,
				  						      mid_angle = start_angle + (end_angle - start_angle)/2;
				  						  var x = mid_radius * Math.cos(mid_angle);
				  						  var y = mid_radius * Math.sin(mid_angle);
				  						  ctx.fillStyle = '#fff';
				  						  if (i == 3){ // Darker text color for lighter background
				  						    ctx.fillStyle = '#fff';
				  						    
				  						  }
				  						 
				  						  ctx.font = "11px sans-serif";
				  						  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
				  						if(dataset.data[i] > 0 ){
				  					  	   ctx.fillText(dataset.data[i], model.x + x, model.y + y);
				  						  // Display percent in another line, line break doesn't work for fillText
				  						  ctx.fillText(percent, model.x + x, model.y + y + 15);
				  						}
		  						}
		  					  });               
		  					}
		  				},
		  				tooltips: {
		  					enabled: true,
		  			        callbacks: {
		  			            label: function(tooltipItem, data) {
		  			                return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
		  			                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
		  			                });
		  			            }
		  			        }
		  			    }
		  	    	}
		  	    }
		    }
		    var chartCreatingProgressCircle = new Chart(context, configCreatProCircle);
			$("#elementMilestone"+id).append(chartCreatingProgressCircle.generateLegend());
	    }
	});
	
}



function getEditMileStone(milestone){
	$(".status-show").attr("hidden",false);
	$("#modalAddMilestone .modal-title").text(getLang(currentLocale,"Edit Milestone"));
	$("#milestoneName").val(milestone);
	formMode = 'edit';
	$("#milestoneName").prop("readonly", true);
	$("#descriptionMile").val(listMileStone[milestone].description);
	var dateStart = new Date(listMileStone[milestone].startDate);
	var startDate = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate();
	var dateEnd = new Date(listMileStone[milestone].endDate);
	var endDate = dateEnd.getFullYear() + "-" + (dateEnd.getMonth() + 1) + '-'+ dateEnd.getDate();
	$('#statusMile option[value="'+listMileStone[milestone].statusMilestone+'"]').prop('selected', true);
	$("#createDate").val(startDate);
	$("#endDate").val(endDate);

	$('#createDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false
	});
	
	$('#endDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false
	});
	
	$("#editmile").val(milestone);
	$('#modalAddMilestone').modal({
		backdrop: 'static'
	});
}

function deleteMileStone(itemMilestone){
	var dataStr = {
			"projectId": projectId,
			"mileStoneName": itemMilestone
	}
	var mileStoneStr = {
			"projectMilestoneIdentity": dataStr
	}
	$.ajax({
		url : jscontext+"/project/"+projectId+"/deleteMilestone",
		type : "POST",
		data: JSON.stringify(mileStoneStr),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				getAllMilestone();
				swal({
			        title: getLang(currentLocale,"Delete Milestone Success!!!"),
			        type: "success",
			        timer: 2000,
			        showConfirmButton: false
			    });
			} else {
			}
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function collsapseCardBody(Isthis){
	var idParentColspase = $(Isthis).parents().eq(2).attr('id');
	$('#'+idParentColspase+ " .card-body").toggleClass("collapse-height"); 
	$('#'+idParentColspase+ " .card-body").slideToggle("fast");
}

function getDateTimePicker(){
	var dateNow = new Date();
	var date = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + '-'+ dateNow.getDate();
	$("#createDate").val(date);
	$("#endDate").val($("#createDate").val());

	$('#createDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false
	});
	$('#endDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : false,
		time: false
	});
}

function hidenModalMilestone(){
	$('#modalAddMilestone').on('hidden.bs.modal', function () {
		$("#milestoneName").val("");
		$("#descriptionMile").val("");
		getDateTimePicker();
		$("#milestoneName").prop("readonly", false);
		$("#modalAddMilestone").find(".item.form-group.bad").removeClass("has-error");
		$("#modalAddMilestone").find(".item.form-group.bad").removeClass("has-success");
		
		$("#modalAddMilestone .input-icon.right").find("i").attr("class","fa milestoneNameValid");
		$("#modalAddMilestone .input-icon.right").find("i").attr("data-original-title","");
	})
}

function creatMileStone(){
	$("#modalAddMilestone .modal-title").text(getLang(currentLocale,"Create Milestone"));
	getDateTimePicker();
	formMode = 'add';
	$('#modalAddMilestone').modal({
		backdrop: 'static'
	});
	
}

function sendMileStone(){
	if ($('#createprojectMilestone').valid() !=true){
		return;
	}
	$("#saveMilestone").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveMilestone").prop("disabled", true);
	var checkMile = $("#editmile").val();
	var milestoneIdentity = {
			"projectId": projectId,
			"mileStoneName": $("#milestoneName").val()
	}
	var milestoneProject = {
			"projectMilestoneIdentity": milestoneIdentity,
			"description": $("#descriptionMile").val(),
			"startDate": new Date($("#createDate").val()+" 12:00:00").toISOString(),
			"endDate": new Date($("#endDate").val()+" 12:00:00").toISOString(), 
			"status": "add"
	}
	if(checkMile == ""){
		milestoneProject["statusMilestone"] = "Open";
		$.ajax({
			url : jscontext+"/project/"+glProjectId+"/createProjectMilestone",
			type : "POST",
			data : JSON.stringify(milestoneProject), 
			contentType : "application/json",
			success : function(result) {
				if (result.errormessage == null || result.errormessage == "") {
					$("#modalAddMilestone").modal("hide");
					swal({
				        title: getLang(currentLocale,"Create MileStone Success!!!"),
				        type: "success",
				        timer: 1000,
				        showConfirmButton: false
				    });
					//draw milestone
					getAllMilestone();
					$("#saveMilestone").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
					$("#saveMilestone").prop("disabled", false);
				}
			  },
			  error : function(request, status, error)
			  {
				  
				  var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					} else {
						swal({
					        title:getLang(currentLocale,"Can not save!!!"),
					        type: "warning",
					        timer: 2000,
					        showConfirmButton: false
					    });
					}
			  }
		
		});
	} else {
		milestoneProject["statusMilestone"] = $("#statusMile").val();
		$.ajax({
			url : jscontext+"/project/"+glProjectId+"/editProjectMilestone",
			type : "POST",
			data : JSON.stringify(milestoneProject), 
			contentType : "application/json",
			success : function(result) {
				$("#createprojectMilestone").validate({
					  onfocusout: false
					});
				if (result.errormessage == null || result.errormessage == "") {
					$("#modalAddMilestone").modal("hide");
					swal({
				        title: getLang(currentLocale,"Edit Milestone Success!!!"),
				        type: "success",
				        timer: 1000,
				        showConfirmButton: false
				    });
					//draw milestone
					getAllMilestone();
					$("#saveMilestone").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
					$("#saveMilestone").prop("disabled", false);
					$("#milestoneName").prop("readonly", false);
					$(".status-show").attr("hidden",true);
					resetFormTestCase();
					
				}
			  },
			  error : function(request, status, error)
			  {
				  
				  var statusCode = request.status;
					if (statusCode == 403) {
						window.location= jscontext + "/login";
					} else {
						swal({
					        title: getLang(currentLocale,"Can not save!!!"),
					        type: "warning",
					        timer: 2000,
					        showConfirmButton: false
					    });
					}
			  }
		
		});
	}
	
}

function showConfirmMessagesMileStone(itemMilestone) {
	//Thoa
	  swal({
	      title: getLang(currentLocale,"Are you sure delete milestone: ") + itemMilestone +"?",
	      text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
	      cancelButtonText: getLang(currentLocale,"Cancel"),
	      closeOnConfirm: false
	  }, function () {
		  deleteMileStone(itemMilestone);
	  });
	}

function resetFormTestCase(){
	$('#modalAddTestcaseToStone').on('hidden.bs.modal', function () {
		$("#filterStatus").val("");
		$("#filterTestsuite").val("");
		$("#milestonePredifinition").val("");
		$("#select2-milestonePredifinition-container").empty();
		$("#select2-milestonePredifinition-container").append('<span class="select2-selection__placeholder">'+getLang(currentLocale,"Select a state")+'</span>');
		$("#select2-filterStatus-container").empty();
		$("#select2-filterStatus-container").append('<span class="select2-selection__placeholder">'+getLang(currentLocale,"Select status")+'</span>');
	})
}

function saveMilestoneTestcaseToTable(){
	var milestoneName = $("#milestoneNameText").val();
	var lsDataTable = [];
	var rows =$("#listBodyDetailTestcaseMilestone");
	var trs = rows.find('tr');
	for(var i = 0; i < trs.length;i++){
		var tds = $(trs[i]).find('td');
		if($(tds[0]).find('input').prop('checked') == true){
			var projectMsTcInd = {
					"projectId": projectId,
					"milestoneName": milestoneName,
					"testsuite": $(tds[1]).text(),
					"testcase": $(tds[2]).text(),
					"status": $(tds[4]).find('label').text()
			}
			var projectMsTc = {
					"projectMilestoneTestcaseIdentity": projectMsTcInd,
					"updateDate": new Date().toISOString(),
					"description": $(tds[3]).text()
			}
			lsDataTable.push(projectMsTc);
	    }
	}
	$("#saveMilestoneTcToTable").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#saveMilestoneTcToTable").prop("disabled", true);
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+milestoneName+"/saveTestcaseToMilestone",
		type : "POST",
		data: JSON.stringify(lsDataTable),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
			        title: getLang(currentLocale,"Save Success!!!"),
			        type: "success",
			        timer: 1000,
			        showConfirmButton: false
			    });
				$("#filterStatus").val("");
				$("#filterTestsuite").val("");
				$("#milestonePredifinition").val("");
				$("#select2-milestonePredifinition-container").empty();
				$("#select2-milestonePredifinition-container").append('<span class="select2-selection__placeholder">'+getLang(currentLocale,"Select a state")+'</span>');
				$("#select2-filterStatus-container").empty();
				$("#select2-filterStatus-container").append('<span class="select2-selection__placeholder">'+getLang(currentLocale,"Select status")+'</span>');
				
				$("#saveMilestoneTcToTable").html("<i class='ti-check'></i>"+getLang(currentLocale," Update To Milestone"));
				$("#saveMilestoneTcToTable").prop("disabled", false);
				$("#modalAddTestcaseToStone").modal("hide");
			} else {
			}
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getDataMilestoneTestcase(projectId, milestoneName, callbackF){
	$.ajax({
		url : jscontext+"/project/"+projectId +"/" + milestoneName+"/getDataMsTc",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				dataTables = result.data;
			callbackF();
			} else {
			}
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func validate form api
var validateAddMilestone = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#createprojectMilestone');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    
    $.validator.addMethod("checkExist",function(value, element, regexp) {
    	if (formMode == 'edit') {
    		return true;
    	}
    	var $alertas = $('#createprojectMilestone');
    	var newNameMileStone = $("#milestoneName").val();
    	if(JSON.stringify(listMileStone) != "{}" ){
	    	for(var i in listMileStone){
	        	if(newNameMileStone == i){
			        return false;
	        	}else{
	        		return true;
	        	}
	        }
    	}else{
    		return true;
    	}
    });
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
        	milestoneName: {
        		required:  {
        	        depends:function(){
        	            $(this).val($.trim($(this).val()));
        	            return true;
        	        }
        	    },
        		checkExist:true,
        		regex: /^[a-zA-Z0-9\\_\\-]*$/,
        		maxlength: 50,
            }
        },     
        messages: {
        	milestoneName:{
        		required: getLang(currentLocale,"Cannot be blank!"),
        		checkExist: getLang(currentLocale,"MileStone Name is exist. Please enter another name!!!"),
        		regex: getLang(currentLocale,"Cannot special characters!"),
        		maxlength:getLang(currentLocale,"Please enter at less 50 characters!"),
			},
        },
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            //tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
	    	 success2.show();
	         error2.hide();
			var $alertas = $('#createprojectMilestone');
		    var newNameMileStone = $("#milestoneName").val();
		    $alertas.find('.form-group').removeClass('has-success');
		    $alertas.find('.fa').removeClass('fa-check');
		    return true;
        }
        
    });
}

function resetFormMilestone() {
	$('#modalAddMilestone').on('hidden.bs.modal', function() {
		document.getElementById("createprojectMilestone").reset();
	    var $alertas = $('#createprojectMilestone');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
		$(".status-show").attr("hidden",true);
		$("#editmile").val("");
	});
}


function drawIssueMilestone(projectId,nameMilestone,listStatus){
//	getMileStones
	$.ajax({
		url : jscontext +"/project/"+projectId+"/"+nameMilestone+ "/listIssueAdd",
		type : "GET",
		data : "",
		async: false,
		contentType : "application/json",
		
		success : function(result) {
			if (!(result.errormessage) ) {
				listIssue = JSON.parse(result.data);
				pieByIssueMilestone(listIssue, nameMilestone,listStatus, projectId);
				if(listIssue.length>0){
					document.getElementById("frameIssueMilestone"+nameMilestone).style.display = "block";
				}
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function pieByIssueMilestone(listIssue, nameMilestone, listStatus,projectId){
	var listIs = [];
	var listColor = [];
    for(var k = 0; k < listIssue.length; k++){
    	var checkAdd = false;
        var listSt = [];
        var itemSt = {};
        var vals = listIssue[k];
        itemSt[vals.status] = vals.count;
        listSt.push(itemSt);
        var objIs = {};

        for(var i = 0; i < listIs.length; i++){
    
            if(vals.tracker in listIs[i]){
                checkAdd = true;
                var itemSt = {};
                itemSt[vals.status] = vals.count;
                var listItem = listIs[i][vals.tracker];
                listItem.push(itemSt);
            } 
        }
		if(checkAdd == false){
		    objIs[vals.tracker]= listSt;
		        listIs.push(objIs);
		}

    }
    
    var itemNameLs = [];
	for(var l = 0; l < listStatus.length; l++){
		itemNameLs.push(listStatus[l].itemName);
		listColor.push(listStatus[l].colorCode);
	}
	var checkLegend = true;
	
	for(var j = 0; j < listIs.length; j++){
		var trackerName = Object.keys(listIs[j])[0];
		var listStOb = Object.values(listIs[j]);
		var listDataSt = [];
		var listLabelSt = [];
		var labelLs = [];
		var dataLs = [];
		var total = 0;
		for(var i = 0; i < listStOb[0].length; i++){
			var statusName = Object.keys(listStOb[0][i])[0];
			var statusValue = Object.values(listStOb[0][i])[0];
			listDataSt.push(statusValue);
			listLabelSt.push(statusName);
			total = total +statusValue;
		}
		var reducer = (accumulator, currentValue) => accumulator + currentValue;
		
		if(listDataSt.length < listStatus.length){
			for(var f = 0; f < itemNameLs.length; f++){
				if(listLabelSt.includes(itemNameLs[f]) == true){
					var indexData = listLabelSt.indexOf(itemNameLs[f]);
					dataLs.push(listDataSt[indexData]);
				} else {
					dataLs.push(0);
				}
				labelLs.push(itemNameLs[f]);
			}
			
		}
		
		if(listDataSt.reduce(reducer) != 0){
			var configIssueCircle = {
		            type: 'pie',
		    	    data: {
		    	        datasets: [{
		    	        	data: dataLs,
		    	        	backgroundColor: listColor,
		    	            label: 'Issue'
		    	        }],
		    	        labels: labelLs
		    	    },
		    		options: {
		    			elements: {
		        		    arc: {
		        		      borderWidth: 0, // <-- Set this to derired value
		        		    }
		        		},
		    			responsive: true,
		    			scaleBeginAtZero: true,
		    			legend: {
		    	            display: false,
		    	            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		    			},
		    			events: true,
		    			animation: {
		    				duration: 500,
		    				easing: "easeOutQuart",
		    				onComplete: function () {
		    				  var ctx = this.chart.ctx;
		    				  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		    				  ctx.textAlign = 'center';
		    				  ctx.textBaseline = 'center';
		    				
		    				  this.data.datasets.forEach(function (dataset) {
		    				
		    					for (var i = 0; i < dataset.data.length; i++) {
		    					  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
		    					      total = dataset._meta[Object.keys(dataset._meta)[0]].total,
		    					      mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
		    					      start_angle = model.startAngle,
		    					      end_angle = model.endAngle,
		    					      mid_angle = start_angle + (end_angle - start_angle)/2;
		    					
		    					  var x = mid_radius * Math.cos(mid_angle);
		    					  var y = mid_radius * Math.sin(mid_angle);
		    					
		    					  ctx.fillStyle = '#fff';
		    					  if (i == 3){ // Darker text color for lighter background
		    					    ctx.fillStyle = '#fff';
		    					  }
		    					  ctx.font = "14px sans-serif";
		    					  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
		    					  if(dataset.data[i] > 0 ){
		    						  ctx.fillText(dataset.data[i], model.x + x, model.y + y -25);
		    						  // Display percent in another line, line break
		    							// doesn't work for fillText
		    						  ctx.fillText(percent, model.x + x + 3, model.y + y - 5);
		    					  }
		    					}
		    				  });               
		    				},
		    			},
		    		}
		    	};
		} else {
			var dataChange = listDataSt;
			dataChange[0] = 1;
			var listCo = [];
			for(var k = 0 ; k < listLabelSt.length; k++){
				listCo.push("#9E9E9E");
			}
			var configIssueCircle = {
					type: 'pie',
					data: {
			    	  datasets: [{
			    		  data: dataChange,
			    	        backgroundColor: listCo,
			    	        label: 'Issue'
			    	    }],
			    	    labels: listLabelSt
					},
					options: {
						elements: {
		  	    		    arc: {
		  	    		      borderWidth: 0, // <-- Set this to derired value
		  	    		    }
		  	    		},
					    tooltips: {enabled: false},
					    hover: {mode: null},
					    legend: {
		  		            display: false,
		  		            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		  				},
					  }
			    }
		}
		
		var htmlAppend = '<div class=" col-md-2 element-chart my-auto">'
	 		+'<div class="box-chart my-auto">'
				 +'<canvas id="testIssueCircleMilestone_'+j+nameMilestone+'" width="100"  class="canvas-milestone"></canvas>'
				 +'<h5 class="text-center text-track" ><a href="#" onclick="redirectFilterIssueMilestone(\''+projectId+'\', \''+nameMilestone+'\', \''+trackerName+'\')">'+trackerName+'('+total+')'+'</a></h5>'
			 	+'</div>'
			+'</div>';
			$("#progressIssueMilestone"+nameMilestone).append(htmlAppend);
			if(document.getElementById("testIssueCircleMilestone_"+j+nameMilestone) != null){
		    	var testIssueCircle = document.getElementById("testIssueCircleMilestone_"+j+nameMilestone).getContext("2d");
			    window.myPie = new Chart(testIssueCircle, configIssueCircle);
//			    $("#legendIssue_"+j).html(window.myPie.generateLegend());
			    
			    if(listDataSt.reduce(reducer) != 0){
					if(checkLegend == true){
						$("#legendIssue"+nameMilestone).html(window.myPie.generateLegend());
						checkLegend = false;
					}
					
				}
		}else{
		}
		
	}
	
}
function redirectFilterIssueMilestone(projectId, nameMilestone, trackerName){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/redirectFilterIssue/"+trackerName+"/"+nameMilestone,
		type : "POST",
		data : "",
		async: false,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ){
				window.location.href= jscontext+"/project/"+projectId+"/issue";
			}
		},error : function(xhr,e) {
				console.log("ERROR: ", e);
				var statusCode = xhr.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				} else {
					
				}
			},
			done : function(e) {
				console.log("DONE");
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		});
}


function backMilestone(){
	$("#newTask").hide();
	$("#box-schedule-init").show();
}


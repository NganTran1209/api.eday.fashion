$(document).ready(function(){
	callbackJsTestData();
	if(testData != null) {
		editCase(convertedObject.Controls, testData);
	} else {
		if(convertedObject != ""){
			creatCase(convertedObject.Controls);
		}
	}
	drawParentTaskInTestcase(taskList);
});
function callbackJsTestData(){
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
	$(function () {
		$('[data-toggle="popover"]').popover()
	});
	$( ".contentItemLayout" ).sortable({
        connectWith: ".itemLayoutCase"
    }).disableSelection(); 
}
var numberCaseName = "";

function editCase(listDatas, dataSaved){
	for(var k = 0; k < dataSaved.listCases.length; k++) {
		var casenumber = dataSaved.listCases[k].casenum;
		var description = dataSaved.listCases[k].description;
		
		  var htmlCase = "";					
		  for(var m = 0; m < dataSaved.listCases[k].listControl.length; m++){
			  var controlType = getControlType(listDatas, dataSaved.listCases[k].listControl[m].Name);
			  
			  var d = new Date();
			  var n = d.getTime();
			  var inputItem = "";
			  if( controlType == "text"  || controlType == "select" ){
				  inputItem = ' <input class="form-control itemEnter valueControl" name="" placeholder="'+getLang(currentLocale,"Please enter")+'" type="text" value="'+ dataSaved.listCases[k].listControl[m].Value +'">' ;
			  } else if(controlType == "checkbox"){
				  inputItem = '<div class="checkbox checkbox-green form-check form-check-inline w-100">'
							  +' 	<input type="'+controlType+'" '+(dataSaved.listCases[k].listControl[m].Value == "true" ? "checked" : "")+' name="" id="checkox'+k+'_'+n+'" required="required" class="flat valueControl">'
							  +' 	<label for="checkox'+k+'_'+n+'" class="w-100 d-block"></label>'
							  +'</div>';
				  
			  }else if(controlType == "radio"){
				  inputItem = '<div class="radio radio-green form-check form-check-inline w-100">'
					  +' 	<input type="'+controlType+'" '+(dataSaved.listCases[k].listControl[m].Value == "true" ? "checked" : "")+' name="radio'+k+'_'+n+'" id="radio'+k+'_'+n+'" required="required" class="flat valueControl">'
					  +' 	<label for="radio'+k+'_'+n+'" class="w-100 d-block"></label>'
					  +'</div>';
			  } else if(controlType != "submit" && controlType != "button"){
				  inputItem = '<textarea class="form-control valueControl" name="" rows="2">'+ dataSaved.listCases[k].listControl[m].Value +'</textarea>';
			  }
			  htmlCase += ' <div class="row mb-1 itemLayoutCase">'
					+' <label class="col-md-3 controlName">'+dataSaved.listCases[k].listControl[m].Name+'</label>'
					+' <div class="col-md-9 d-flex">'
						+  inputItem
//						+' <div class="helpItemCase" data-toggle="modal" data-target="#helpEnterInput"><i class="ti-help-alt"></i></div>'
						+' <div class="moveItemCase"><i class="ti-move"></i></div>'
						+' <div class="deleteItemCase" onclick="deleteItem(event)"><i class="ti-trash"></i></div>'
					+' </div>'
				+' </div> ';
		  }
		  var htmlCase1 = 
		'<li class="formStyle mt-1" id="itemCase_'+casenumber+'">'
			+' <div class="col-md-12 col-center-block listCase">'
			+' <div class="row"> '
				+' <div class="col-md-3"> '
					+' <div class="w-100 form-group">'
						+' <div class="page-title d-flex w-100 mt-0 mb-2" style="font-size:18px">'
							+' <i class="fa fa-circle my-auto mr-2"></i>'
							+' <span class="caseName">'+getLang(currentLocale,"Case Number")+': #'+casenumber+'</span> '
						+' </div>'
						+'<textarea class="form-control descCase" name="caseName'+casenumber+'" placeholder="'+getLang(currentLocale,"Description")+'" rows="3">'+description+'</textarea>'
					+' </div>'
					+' <div class="w-100">'
						+' <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites" '
						+' href="javascript:void(0);"  onclick="copyItemId(itemCase_'+casenumber+')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Copy")+'" data-original-title="Copy">'
							+' <i class="icon-docs"></i>'
						+' </a>'
						+' <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites" '
						+' href="javascript:void(0);" onclick="deleteItemId(itemCase_'+casenumber+')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Delete")+'" data-original-title="Delete">'
							+' <i class="fa fa-trash-o"></i>'
						+' </a>'
					+' </div>'
				+' </div>'
				+' <div class="col-md-9 contentItemLayout">'
				+  htmlCase 
				+' </div>'
			+' </div>'
			+' </div>'
		+' </li>';
	    $(".listCaseAddNew").append(htmlCase1);
	}
} 


function getControlType(controls, controlName){
	 for(var i = 0; i < controls.length; i++) {
		  if (controls[i].Name == controlName){
			  for(var j = 0; j < controls[i].Attributes.length; j++) {
				  if(controls[i].Attributes[j].Name == "type"){
					  return controls[i].Attributes[j].Value;
				  }
			  }
		  }
	 }
}

function creatCase(listDatas){
	var listCase = $(".listCaseAddNew")[0];
	try{
		var countLi = $(listCase).find("li")[$(".listCaseAddNew li").length -1].id.split("_")[1];
	} catch(error){
		var countLi = $(".listCaseAddNew li").length;
	}

	numberCaseName = parseInt(countLi) + 1;
	console.log(numberCaseName);
	
  var htmlCase = "";					
  for(var i = 0; i < listDatas.length; i++) {
	  var inputItem = "";
	  for(var j = 0; j < listDatas[i].Attributes.length; j++) {
		  if(listDatas[i].Attributes[j].Name == "type") {
			  var d = new Date();
			  var n = d.getTime();
			  if(listDatas[i].Attributes[j].Value == "text" || listDatas[i].Attributes[j].Value == "select") {
				  inputItem = ' <input class="form-control itemEnter valueControl" name="" placeholder="'+getLang(currentLocale,"Please enter")+'" type="text" value="">';
			  } else if(listDatas[i].Attributes[j].Value == "checkbox") {
				  inputItem = ' <input class="flat valueControl" name="" type="'+listDatas[i].Attributes[j].Value+'">';
				  inputItem = '<div class="checkbox checkbox-green form-check form-check-inline w-100 ">'
							  +' 	<input type="'+listDatas[i].Attributes[j].Value+'" name="" id="checkox'+i+'_'+n+'" required="required" class="flat valueControl">'
							  +' 	<label for="checkox'+i+'_'+n+'" class="w-100 d-block"></label>'
							  +'</div>';
			  } else if(listDatas[i].Attributes[j].Value == "radio"){
				  inputItem = '<div class="radio radio-green form-check form-check-inline w-100 ">'
							  +' 	<input type="'+listDatas[i].Attributes[j].Value+'" name="" id="radio'+i+'_'+n+'" required="required" class="flat valueControl">'
							  +' 	<label for="radio'+i+'_'+n+'" class="w-100 d-block"></label>'
							  +'</div>';
			  }else if(listDatas[i].Attributes[j].Value != "submit" && listDatas[i].Attributes[j].Value != "button") {
				  inputItem = '<textarea class="form-control valueControl" name="" rows="2"></textarea>';
			  }
		  }
	  } 
	  if(inputItem != ""){
		  htmlCase += ' <div class="row mb-1 itemLayoutCase">'
				+' <label class="col-md-3 controlName">'+listDatas[i].Name+'</label>'
				+' <div class="col-md-9 d-flex">'
					+'<div class="w-100 itemEnter">'
					+ inputItem 
					+'</div>'
//					+' <div class="helpItemCase" data-toggle="modal" data-target="#helpEnterInput"><i class="ti-help-alt"></i></div>'
					+' <div class="moveItemCase"><i class="ti-move"></i></div>'
					+' <div class="deleteItemCase" onclick="deleteItem(event)"><i class="ti-trash"></i></div>'
				+' </div>'
			+' </div> ';
	  }
	  
  }
  
  var htmlCase1 = 	'<li class="formStyle mt-1" id="itemCase_'+numberCaseName+'">'
	+' <div class="col-md-12 col-center-block listCase">'
		+' <div class="row"> '
			+' <div class="col-md-3"> '
				+' <div class="w-100 form-group">'
					+' <div class="page-title d-flex w-100 mt-0 mb-2" style="font-size:18px">'
						+' <i class="fa fa-circle my-auto mr-2"></i>'
						+' <span class="caseName">'+getLang(currentLocale,"Case Number")+': #'+numberCaseName+'</span> '
					+' </div>'
					+'<textarea class="form-control descCase" name="caseName'+numberCaseName+'" placeholder="'+getLang(currentLocale,"Description")+'" rows="3"></textarea>'
				+' </div>'
				+' <div class="w-100">'
					+' <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites" '
					+' href="javascript:void(0);"  onclick="copyItemId(itemCase_'+numberCaseName+')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Copy")+'" data-original-title="Copy">'
						+' <i class="icon-docs"></i>'
					+' </a>'
					+' <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites" '
					+' href="javascript:void(0);" onclick="deleteItemId(itemCase_'+numberCaseName+')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Delete")+'" data-original-title="Delete">'
						+' <i class="fa fa-trash-o"></i>'
					+' </a>'
				+' </div>'
			+' </div>'
			+' <div class="col-md-9 contentItemLayout">'
			+ htmlCase
			+' </div>'
			+' </div>'
			+' </div>'
		+' </li>';
 					
	$(".listCaseAddNew").append(htmlCase1);
	callbackJsTestData();
}

function saveCase() {
	$("#btnSaveCaseNum").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#btnSaveCaseNum").prop("disabled", true);
	var layoutName = $("#loadNameLayout").text();
	var jsonDataText = {
			"layoutName": convertedObject.Name,
			"description": convertedObject.Description,
			"listCases": []
	}
	var listCase = $(".listCaseAddNew")[0];
	for(var i = 0; i < $(listCase).find("li").length; i++){
		var caseItem = {
				"casenum": "",
				"description": "",
				"listControl": []
		}
		
		var itemList = $(listCase).find(".listCase")[i];
		var description = $(listCase).find(".descCase")[i].value;
		var casenum = $(listCase).find("li")[i].id.split("_")[1];
		for(var j = 0; j < $(itemList).find(".controlName").length; j++){
			var nameControl = $(itemList).find(".controlName")[j].innerText;
			var valueControl = "";
			if($(itemList).find(".valueControl")[j].type == "checkbox" || $(itemList).find(".valueControl")[j].type == "radio"){
				valueControl = $(itemList).find(".valueControl")[j].checked.toString();
			} else {
				valueControl = $(itemList).find(".valueControl")[j].value;
			} 
			var item = {"Name" : nameControl, "Value": valueControl};
			caseItem.listControl.push(item);
		}
		caseItem.casenum = casenum;
		caseItem.description = description;
		jsonDataText.listCases.push(caseItem);
	}
	var idT = "";
	if(idTestData != null){
		idT = idTestData;
	}
	var uploadTestData = {
			"id": idT,
			"fileName": convertedObject.Name+".json",
			"description": convertedObject.Description,
			"projectId": projectId,
			"testsuiteName": testsuiteName,
			"type": "layout",
			"testdataContent": JSON.stringify(jsonDataText)
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/saveTestData/" + layoutName,
		type : "POST",
		data : JSON.stringify(uploadTestData),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#btnSaveCaseNum").html("<i class='ti-check'></i> "+getLang(currentLocale,"Save"));
				$("#btnSaveCaseNum").prop("disabled", false);
//        	$this.button('Save');
			 swal({
			        title: getLang(currentLocale,"TestData has been save!!!"),
			        type: "success",
			        timer: 1000,
			        showConfirmButton: false
				});
			 window.location.href =  jscontext+"/project/"+projectId+"/testData";
			}
      },
      error : function(request,error)
      {
    	  console.log(error);
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
          
      }
    });
}
function deleteItemId(id){
	$(id).fadeToggle(function () {
		$('[data-toggle="tooltip"]').tooltip('hide');
		$(id).remove();
	});
	
}
function deleteItem(event){
	$(event.target).closest(".itemLayoutCase").remove();
}
function copyItemId(id) {
	var htmlContentCase = $("#"+ $(id).attr("id") + "").find(".contentItemLayout").html();
	var description = $(id).find(".descCase")[0].value;
	var listCase = $(".listCaseAddNew")[0];
	var countLi = $(listCase).find("li")[$(".listCaseAddNew li").length -1].id.split("_")[1];
	var number = parseInt(countLi) + 1;
	var htmlContentItemLayout = '<li class="formStyle mt-1" id="itemCase_'+number+'">'
							+' <div class="col-md-12 col-center-block listCase">'
							+' <div class="row"> '
								+' <div class="col-md-3"> '
									+' <div class="w-100 form-group">'
										+' <div class="page-title d-flex w-100 mt-0 mb-2" style="font-size:18px">'
											+' <i class="fa fa-circle my-auto mr-2"></i>'
											+' <span class="caseName">'+getLang(currentLocale,"Case Number")+': #'+number+'</span> '
										+' </div>'
										+'<textarea class="form-control descCase" name="caseName'+number+'" placeholder="'+getLang(currentLocale,"Description")+'" rows="3">'+description+'</textarea>'
									+' </div>'
									+' <div class="w-100">'
										+' <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites" '
										+' href="javascript:void(0);"  onclick="copyItemId(itemCase_'+number+')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Copy")+'" data-original-title="Copy">'
											+' <i class="icon-docs"></i>'
										+' </a>'
										+' <a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites" '
										+' href="javascript:void(0);" onclick="deleteItemId(itemCase_'+number+')" data-toggle="tooltip" data-placement="top" title="'+getLang(currentLocale,"Delete")+'" data-original-title="Delete">'
											+' <i class="fa fa-trash-o"></i>'
										+' </a>'
									+' </div>'
								+' </div>'
								+' <div class="col-md-9 contentItemLayout">';
	
	var html3 = '<div>'
			+' </div>'
			+' </div>'
			+' </li>';
	
	$(".listCaseAddNew").append(htmlContentItemLayout+htmlContentCase+html3);
}

function backDetailTestData(){
	$("#newTask").hide();
	$("#boxTestDataDetail").show();
}
// ===================Load list item==============================
// Load List Testcase
var testsuitels;
//var listExecuteBy = null;
//var tsByProjectId = null;
var testcasels = "";
var testResultData = "";
var duplicateUserList = '';
$(document).ready(function(){
	$('#testcase_name').on('blur keyup', function(){
        $(this).removeAttr('title');
	});
	getListBottestUser();
	drawBugInTestcase(autoTcLs);
	drawParentTaskInTestcase(tasks);
	
});

$("#bodyTestcaseRecord").sortable({
	start: function(event, ui) {
   	 	ui.item.startPos = ui.item.index();
	},
	update: function(event, ui) {
		var indexOld = ui.item.startPos + 1;
        console.log("Old: "+ indexOld);
		var indexNew = ui.item.index() + 1;
        console.log("New: " +indexNew);
        var productOrder = $(this).sortable('toArray');
        var lsTestcaseS = [];
        $.each(productOrder, function(ind, val){
        	var projectPro = {
        			"projectId": projectId,
        			"testsuite": "",
        			"testcase": val
        	}
        	var automationTestcase = {
        			"projectProgressIdentity": projectPro,
        		    "type": "automation",
        		    "orderId": ind+1
        	}
        	lsTestcaseS.push(automationTestcase);
        });
        
        $.ajax({
    		url : jscontext +"/project/" + projectId +"/orderIdTestcase",
    		type : "POST",
    		data : JSON.stringify(lsTestcaseS),
    		contentType : "application/json", 
    		success : function(result) {
    			
    			if (!(result.errormessage) ) {
    				getAllTestcaseAutomationByProject();
    				
    				var testsuiteName = $("#filterTestsuite").val();
    				var executeBySl = $("#filterExecuteBy").val();
    				var statusTestcase = $("#filterStatusTestcase").val();
    				var fromDate = $("#filterFromDate").val();
    				var toDate = $("#filterToDate").val();
    				var filterAssignTo = $("#filterAssignTo").val();
    				var description = $("#filterDescription").val();
    				var statusIssue = $("#filterTestcaseStatus").val();
    				var categoryIssue = $("#filterTestcaseCategory").val();
    				var milestoneTestcase = $("#filterTestcaseMilestone").val();
    				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue, milestoneTestcase);
    			} else {
    			}
    		},
    		error : function(request, e) {
    			console.log("ERROR: ", e);
    			
    			var statusCode = request.status;
    			if (statusCode == 403) {
    				window.location= jscontext + "/login";
    			} else {
    				swal({
    					title: getLang(currentLocale,"Could not load get All Testcase"),
    			        text: getLang(currentLocale,"You can reload the page again!"),
    			        type: "warning", 
    			        timer: 2000,
    			        showConfirmButton: false
    			    });
    			}
    		},
    		done : function(e) {
    			console.log("DONE");
    		}
    	});
        
//        var itItem = productOrder[indexNew-1];
//        var prevItem = (productOrder[indexNew-2] ? productOrder[indexNew-2]:"");
//        var nextItem = (productOrder[indexNew] ? productOrder[indexNew]: "");
        
//        var indexPrevOld = (listObTc[prevItem] ? listObTc[prevItem] : "");
//        var indexNextOld = (listObTc[nextItem] ? listObTc[nextItem] : "");
//        var indexItOld = (listObTc[itItem] ? listObTc[itItem] : "");
        
//        if(indexItOld < indexPrevOld || indexItOld < indexNextOld){
//        	console.log("di chuyen xuong");
//        	moveDownTestcase(itItem, indexPrevOld);                                                                                                         
//        } else {
//        	console.log("di chuyen len");
//        	moveUpTestcase(itItem, indexNextOld);
//        }
    }
});

$("#bodyTestcaseRecord").disableSelection();


//function drawBugInTestcase(){
//	var lsBug = [];
//	$.each(autoTcLs, function(ind, vals){
//		if(vals.testCaseResult == "FAILED"){
//			var itemIssue = {
//					value: getLang(currentLocale,"Result Id")+": "+vals.id + " - ",
//				    label: vals.id,
//				    desc: vals.testcase,
//				}
//				lsBug.push(itemIssue);
//		}
//		
//	});
//	autocompleteBugId(lsBug);
//}
//
//function drawParentTaskInTestcase(){
//	var lsIssue = [];
//	$.each(tasks, function(ind, vals){
//		var itemIssue = {
//			value: vals.tracker +" # " + vals.id + ": ",
//		    label: vals.id,
//		    desc: vals.subject,
//		}
//		lsIssue.push(itemIssue);
//	});
//	
//	autocompleteParentTask(lsIssue);
//}

function onChangeSelect2(elemntSelect2){
	elemntSelect2.on('select2:select', function (e) {
		  existEdit = true;
		}); 
}


function getAllTestcasesByStatus(){
	$.ajax({
		url : jscontext +"/project/" + projectId+"/"+ glTestsuiteId +"/getAllTestcases",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				listTestcase = result.data;
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get All Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function syncProject(e){
	$(".preloader-main").fadeIn("slow");
	$.ajax({
		url : jscontext + "/project/"+ projectId+"/syncProject",
		type : "POST",
		data : "",
		contentType : "application/json",
		success : function(result) {
			$(".preloader-main").fadeOut("slow");
			if (!(result.errormessage) ) {
				var bodyListTestsuiteElm = $("#bodyTestsuiteRecord");
				if(bodyListTestsuiteElm.length > 0){
					loadListRs(result.data);
				}
				
			}else {
				swal({
				       title: "",
				       text: getLang(currentLocale,"Sync project fail:") + result.errormessage,
				       type: "warning",
				       timer: 2000,
			        	showConfirmButton: false
				   });
//				window.location= jscontext + "/login";
			}
      },
      error : function(request,error)
      {
    	  $(".preloader-main").fadeOut("slow");
    	  
    	  var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({ 
				       title: "",
				       text: "Sync project fail:" + error,
				       type: "warning",
				       timer: 2000,
			        	showConfirmButton: false
				   });
			}
      }
	
	});
}

function redirectResult(jscontext, projectId, testsuiteName, testcaseName, resultName) {
	syncProject();
	var url = jscontext+'/project/'+projectId+'/'+testsuiteName+"/"+testcaseName+'/result/'+resultName;
	window.open(url);
}

$("#exportTestcaseToExcel").click(function(e){
	$("#exportTestcaseToExcel").html("<i class='fa fa-circle-o-notch fa-spin'></i>"+ getLang(currentLocale,"Please waiting!"));
	$("#exportTestcaseToExcel").prop("disabled", true);
	
	var testsuiteName = $("#filterTestsuite").val();
	var executeBySl = $("#filterExecuteBy").val();
	var statusTestcase = $("#filterStatusTestcase").val();
	var fromDate = $("#filterFromDate").val();
	var toDate = $("#filterToDate").val();
	var filterAssignTo = $("#filterAssignTo").val();
	var description = $("#filterDescription").val();
	
	var filterTestcase = {
			"testsuiteName": testsuiteName,
			"executeBy": executeBySl,
			"statusTestcase": statusTestcase,
			"fromDate": fromDate,
			"toDate": toDate,
			"assignTo": filterAssignTo,
			"type": "automation",
			"fileName": description
	}
	
	$.ajax({
		url : jscontext +"/project/" + projectId+"/exportTestcases",
		type : "POST",
		data : JSON.stringify(filterTestcase),
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				window.location.href = jscontext +"/project/" + projectId+"/"+ glTestsuiteId +"/downloadTestcaseExport";
				$("#exportTestcaseToExcel").html('<i class="ti-export text-white"></i> Export');
				$("#exportTestcaseToExcel").prop("disabled", false);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get All Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
});
//func reset Form of modal_add_new_Testcase
function resetFormTestCase() {
	$('#modalAddNewTestcase').on('hidden.bs.modal', function() {
		document.getElementById("new_testcase").reset();
	    var $alertas = $('#new_testcase');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	    $("#orderIdAuto").val("");
	});
}

$("#modalAddNewTestcase").on('show.bs.modal', function(){

});

function tooltipValidation(){
	$('.input-icon.right i').hover(function(){
		$('.tooltip').css("z-index","11111");
	});
	
}

function funcCopyTestcase(isThis) {
	$('#modalCopyTestcases').modal({
		backdrop: 'static'
	});
	$("#copyTestcaseId").show();
	$("#copyTestcaseId").prop("disabled",false);
	$("#copyTestcaseIdManual").hide();
	$("#testsuiteList").empty();
	var item = $(isThis).attr("data-name");
	var testsuitename = $(isThis).attr("testsuite-name");
	$("#testcaseNameOld").text(item);
	$("#testsuiteId").text(testsuitename);
	getListTestsuite("false", function(testsuitels){
		
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteList").append(htmlOptions);
		
	});
	$.each(testsuitels, function(index, value){
		if (testsuitename == value["testsuite"]) {
			$("#testsuiteList option[value='"+value["testsuite"]+"']").prop("selected",true);
		}
	});
	
};

function getListTestsuite(status, callbackF) {
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getTestSuites",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				
				
				if(status == "true"){
					tsByProjectId = [];
					$.each(result.data, function(index, values){
						var obTs = {};
						obTs["id"] = values.testsuite;
						obTs["text"] = values.testsuite;
						tsByProjectId.push(obTs);
					});
					callbackF(tsByProjectId);
				} else {
					callbackF(result.data);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}


$('#modalCopyTestcases').on('hidden.bs.modal', function () {
	$("#messageError").text("");
	})

function getAllTestcaseAutomationByProject(){
	$("#loadListTestCase").fadeIn("slow");
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getAllTestcaseByProject",
		type : "get",
		data : "",
		contentType : "application/json", 
		success : function(result) {
			
			if (!(result.errormessage) ) {
				autoTcLs = result.data;

			} else {
			}
			$("#loadListTestCase").fadeOut("slow");
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get All Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function drawListTestcaseByProjectId(index, values){
	var splitStr = values.testcase.split("_");
	recordNumber = splitStr[splitStr.length-1];
	var linkTestcaseDetail = jscontext + "/project/"+projectId+"/"+values.testsuite+"/testcases/"+values.testcase;
	var linkLastRun = jscontext+'/project/'+projectId+'/'+values.testsuite+"/"+values.testCaseName+'/result/'+values.resultName;
	var htmlRecordTestsuites =	'<tr id="'+values.testcase+'">'
		+'<td data-name="'+values.testcase+'" order-id="'+values.orderId+'" testsuite-name="'+values.testsuite+'"  style="line-height: 20px; cursor:move;" width="3%" class="text-center">'
		+'<div class="mb-2">'+recordNumber+'</div>'
		+'<div class="actionListTable mr-1">'
		+'<button class="btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success" id="btnAdd_'+index+'"  data-id="'+index+'" data-name="'+values.testcase+'" testsuite-name="'+values.testsuite+'" onclick="showModalAddNewTestcaseByOrderAuto(\''+values.orderId+'\');">'
		+'<i class="ti-plus text-white"></i>'
		+'</button>'
		+'<button class="btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-success" id="btnPlay_'+index+'"  data-id="'+index+'" data-name="'+values.testcase+'" testsuite-name="'+values.testsuite+'" onclick="getANameTestCase(this);" data-toggle="modal" data-target="#modalRun">'
		+'<i class="fa fa-play"></i>'
		+'</button>'
		+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites" href="javascript:void(0);" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" onclick="funcCopyTestcase(this)">'
		+'<i class="icon-docs"></i>'
		+'</a>'
		+'<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-default btn-delete-testSuites" href="javascript:void(0);" testsuite-name="'+values.testsuite+'" data-name="'+values.testcase+'" onclick="showConfirmMessages(this)">'
		+'<i class="fa fa-trash-o"></i>'
		+'</a>'
		+'<a class="btnPlay mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab" style="background: #0288D1; cursor:move;" id="btnAdd_'+index+'"  data-id="'+index+'" data-name="'+values.testcase+'" testsuite-name="'+values.testsuite+'" onclick="">'
		+'<i class="ti-move text-white"></i>'
		+'</a>'
		+'</div>'
		+'</td>'
		+'<td data-name="'+values.testcase+'" testsuite-name="'+values.testsuite+'"  style="line-height: 20px;cursor:move;" width="14%">'
		+'<div class="w-100 d-flex justify-content-between">'
		+'<a href="javascript:void(0);" class="itemNameTestcase mr-3" onclick="window.location.href='+"'"+jscontext + "/project/"+projectId+"/"+values.testsuite+"/testcases/"+values.testcase+"'"+'">'+values.testcase+'</a>'
		+'<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkAutomation(\''+linkTestcaseDetail+'\', \''+"true"+'\')"><i class="fa fa-link"></i></button>'
		+'</div>'
		+(values.description != null ? '<div class="pl-2 mb-3 font-bold">'+(values.description)+'</div>': "");
		if((values.assignTo != null && values.assignTo != "") || (values.statusIssue != null && values.statusIssue != "")){
			htmlRecordTestsuites += '<div class="form-group" style="border: 1px solid #1dcb8b;padding: 5px 10px;"><div style="word-break: break-all;">'
				if(values.assignTo){
					var count = 0 ;
					if(duplicateUserList.length > 0){
						$.each(duplicateUserList, function(indexDup,valueDup){
							if(valueDup.username == values.assignTo){
								htmlRecordTestsuites+= (values.lastname != null &&  values.firstname != null ? values.lastname +" "+values.firstname +"("+ values.assignTo +")" : values.assignTo ) ;	
								count ++;
							}					
						})
					} 
					
					if(count == 0 ){
						htmlRecordTestsuites+= (values.lastname != null &&  values.firstname != null ? values.lastname +" "+values.firstname  : values.assignTo ) ;	
					}
				}
			htmlRecordTestsuites +='</div><div>'+(values.statusIssue ? "- " + values.statusIssue : "")+'</div>'
			+'</div>';
		}
		
		var runningUser = '';
		$.each(listUsers,function(index, value){
			if(value["username"].replace("@",".") == values.runningUser){
				var count = 0 ;
				if(duplicateUserList.length > 0){
					$.each(duplicateUserList, function(indexDup,valueDup){
						if(valueDup.username == value["username"]){
							runningUser = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] +"(" +value["username"]+")" : ""); 
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					runningUser = (value["lastname"] != null && value["firstname"] != null ? value["lastname"] + " " + value["firstname"] :""); 
				}
				
			}
		})
		
htmlRecordTestsuites +='</td>'
		+'<td data-name="'+values.testcase+'" style="cursor:move;" testsuite-name="'+values.testsuite+'" onclick="window.location.href='+"'"+jscontext + "/project/"+projectId+"/"+values.testsuite+"/testcases/"+values.testcase+"'"+'"  width="30%">'+ (values.testcaseProcedure != null ? values.testcaseProcedure: "")+'</td>'
		+'<td data-name="'+values.testcase+'" style="cursor:move;" testsuite-name="'+values.testsuite+'" onclick="window.location.href='+"'"+jscontext + "/project/"+projectId+"/"+values.testsuite+"/testcases/"+values.testcase+"'"+'"  width="30%">'+(values.expectedOutputTc != null ? values.expectedOutputTc: "")+'</td>'
		+'<td data-name="'+values.testcase+'" style="cursor:move;" testsuite-name="'+values.testsuite+'"  width="18%">';
		if (values.testCaseName != null) {
			if (values.testCaseResult == 'PASS') {
				htmlRecordTestsuites += '<a class="label label-success 0 label-history mr-2" href="#" onclick="redirectResult(\''+ jscontext +'\', \''+ projectId+'\', \''+ values.testsuite +'\', \''+ values.testCaseName +'\', \''+ values.resultName +'\')">'+values.testCaseResult+'</a>';
			} else if (values.testCaseResult == 'FAILED' || values.testCaseResult == 'ABORT') {
				htmlRecordTestsuites += '<a class="label label-danger label-history mr-2" href="#" onclick="redirectResult(\''+ jscontext +'\', \''+ projectId+'\', \''+ values.testsuite +'\', \''+ values.testCaseName +'\', \''+ values.resultName +'\')">'+values.testCaseResult+'</a>';
				
			} else if (values.testCaseResult == 'DEGRADE') {
				htmlRecordTestsuites += '<a class="label label-degrade label-history mr-2" href="#" onclick="redirectResult(\''+ jscontext +'\', \''+ projectId+'\', \''+ values.testsuite +'\', \''+ values.testCaseName +'\', \''+ values.resultName +'\')">'+values.testCaseResult+'</a>';
				
			}
		}
		
		if (values.testCaseResult != null){
			htmlRecordTestsuites += '<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab btn-warning btn-copy-testSuites m-0 mr-2 item-tooltip" onclick="copyLinkAutomation(\''+linkLastRun+'\', \''+"false"+'\')"><i class="fa fa-link"></i></button>'
		}
		if (values.testCaseResult != null) {
			if (values.testCaseResult == 'FAILED' || values.testCaseResult == 'DEGRADE') {
				htmlRecordTestsuites += '<div class="w-100 mt-2">- <strong>'+getLang(currentLocale,"Result Id")+': </strong> '+values.id+'</div>';
			}
		}
		
htmlRecordTestsuites	+= '<div class="w-100" style="white-space: nowrap;">- '+(values.testCaseName != null ? new Date(values.executeDate).toISOString().substr(0,new Date(values.executeDate).toISOString().indexOf('.')).replace("T"," "): "")+'</div>'
		+'<div class="w-100" style="word-break: break-all;">- '+ (values.testCaseName != null ? runningUser: "") +'</div>'
		+'<div id="issue_'+values.id+'" class="d-flex">';
		if(values.testCaseResult == 'FAILED' || values.testCaseResult == 'ABORT' || values.testCaseResult == 'DEGRADE'){
			getStatusIssue(projectId, values.id, function(taskData){
				if(taskData == null || taskData == undefined ){
					$("#issue_"+values.id+"").html('<button class="btn btn-success label-history mt-2" onclick="fucntionAddNewIssue(\''+ projectId+'\', \''+ values.id + '\');"><i class="ti-plus text-white"></i>'+getLang(currentLocale,"Log Bug")+'</button>');
					
				} else {
					if(taskData.assignee != ""){
						$.each(listUsers,function(index, value){
							if(value["username"] == taskData.assignee){
								var count = 0 ;
								if(duplicateUserList.length > 0){
									$.each(duplicateUserList, function(indexDup,valueDup){
										if(valueDup.username == value["username"]){
											$("#issue_"+values.id+"").html('<label class="mr-2">- <strong>Issue: </strong> </label><div><a href="'+jscontext + "/project/"+projectId+"/issue/"+taskData.id+'"> '+taskData.status+'</a><div>'+value["lastname"] + " " + value["firstname"]+'('+value["username"]+')'+'</div></div>');
											count ++;
										}					
									})
								} 
								
								if(count == 0 ){
									$("#issue_"+values.id+"").html('<label class="mr-2">- <strong>Issue: </strong> </label><div><a href="'+jscontext + "/project/"+projectId+"/issue/"+taskData.id+'"> '+taskData.status+'</a><div>'+value["lastname"] + " " + value["firstname"]+'</div></div>');
								}
								
							}
						});
					} else{
						$("#issue_"+values.id+"").html('<label class="mr-2">- <strong>Issue: </strong> </label><div><a href="'+jscontext + "/project/"+projectId+"/issue/"+taskData.id+'"> '+taskData.status+'</a></div>');
					}
				}
			});
			
		}
htmlRecordTestsuites+='</div>'
		+'</td>'
//		+'<td  width="5%">'
//		
//		+'</td>'
		+'</tr>';
	if(values.statusTc == "add"){
		$("#bodyTestcaseRecord").append(htmlRecordTestsuites);
	}
	
}

var listUsers = '';
function getListBottestUser(){
	$.ajax({
		url : jscontext +"/project/" + projectId +"/getUsers",
		type : "get",
		data : "",
		contentType : "application/json", 
		async : false,
		success : function(result) {
			if (!(result.errormessage) ) {
				listUsers = result.data;
				duplicateUserList = duplicateUser(listUsers);
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function copyLinkAutomation(link, status){
	$('#modalCopyLink').modal({
		backdrop: 'static'
	});
	if(status != "true"){
		$("#titleFormCopy").text(getLang(currentLocale,'Copy link result manual testcase'));
	} else {
		$("#titleFormCopy").text(getLang(currentLocale,'Copy link detail automation testcase'));
	}
	
	$("#copyItem").val(window.location.origin + link);
	
}

function copyLink(){
	var copyText = document.getElementById("copyItem");
	copyText.select();
	copyText.setSelectionRange(0, 99999);
	document.execCommand("copy");
	$("#modalCopyLink").modal('hide');
}

function resetFormCopy(){
	$('#modalCopyLink').on('hidden.bs.modal', function() {
		document.getElementById("copyLink").reset();
	    var $alertas = $('#copyLink');
	    $alertas.validate().resetForm();
	    $alertas.find('.form-group').removeClass('has-error').removeClass('has-success');
	    $alertas.find('.fa').removeClass('fa-warning').removeClass('fa-check');
	});
}

function funcAddNewTestcase(){
	
	$('#modalAddNewTestcase').modal({
		backdrop: 'static'
	});
	$('[data-toggle="tooltip"]').tooltip();
	$("#lsTestsuite").empty();
	getListTestsuite("false", function(testsuitels){
			
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#lsTestsuite").append(htmlOptions);
		
	});
	
	infProjectCom(function(projectCo){
		$("#statustaskTc").empty();
		$("#categorytaskTc").empty();
		var html = "";
		var htmlCategory = "";
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Status"){
				html += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			} else if(projectCo[i]["groupCode"] == "Category"){
				htmlCategory += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			}
		}
		$("#statustaskTc").append(html);
		$("#categorytaskTc").append(htmlCategory);
	});
	
	$("#assignToTc").empty();
	getUserAssignTo("false", function(lsAssign){
		var htmlOptions = "";
		$.each( lsAssign, function(index, valuesItemTestsuite) { 
			if(valuesItemTestsuite["lastname"] != null && valuesItemTestsuite["firstname"] != null){
				var count = 0 ;
				if(duplicateUserList.length > 0){
					$.each(duplicateUserList, function(indexDup,valueDup){
						if(valueDup.username == valuesItemTestsuite["username"]){
							htmlOptions += '<option value="'+valuesItemTestsuite["username"]+'">'+valuesItemTestsuite["lastname"]+" "+valuesItemTestsuite["firstname"]+"(" + valuesItemTestsuite["username"] + ")"+'</option>';
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					htmlOptions += '<option value="'+valuesItemTestsuite["username"]+'">'+valuesItemTestsuite["lastname"]+" "+valuesItemTestsuite["firstname"] +'</option>';
				}
			}
		});
		$("#assignToTc").append(htmlOptions);
	});
	
	getAllMilestoneInIssue(function(milestones){
		$("#milestoneTC").empty();
		var htmlOptions = "";
		var number = 0;
		$.each(milestones,function(index, value){
			var dateStart = new Date(value["startDate"]);
			var dateEnd = new Date(value["endDate"]);
			var dateStartStr = dateStart.getFullYear() + "-" + (dateStart.getMonth() + 1) + '-'+ dateStart.getDate()+" "+dateStart.getHours()+":"+dateStart.getMinutes()+":"+dateStart.getSeconds();
			var dateEndStr = dateEnd.getFullYear() + "-" + (dateEnd.getMonth() + 1) + '-'+ dateEnd.getDate()+" "+dateEnd.getHours()+":"+dateEnd.getMinutes()+":"+dateEnd.getSeconds();
			var dateNow = new Date().getTime();
			if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){
				if(number == 0){
					htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" selected>'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
					number ++;
				} else{
					htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
				}
			} else{
				htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
			}
		})
		$("#milestoneTC").append(htmlOptions);
	})
}

function getExecuteBy(callbackF){
		$.ajax({
			url : jscontext +"/project/" + projectId +"/getExecuteBy",
			type : "get",
			data : "",
			contentType : "application/json", 
			success : function(result) {
				
				if (!(result.errormessage) ) {
					var listExecuteBy = result.data.map(function(obj){
						return {"id": obj, "text":obj}
					});		
				} else {
				}
				callbackF(listExecuteBy);
				
			},
			error : function(request, e) {
				console.log("ERROR: ", e);
				var statusCode = request.status;
				if (statusCode == 403) {
					window.location= jscontext + "/login";
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});
}

function drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue, milestoneTestcase){
	
	if(statusTestcase == "NOT-RUN"){
		$("#filterExecuteBy").attr("disabled", true);
		$("#filterFromDate").attr("disabled",true);
		$("#filterToDate").attr("disabled",true);

	} else {
		$("#filterExecuteBy").attr("disabled", false);
		$("#filterFromDate").attr("disabled",false);
		$("#filterToDate").attr("disabled",false);

	}

	testsuiteNameParse = testsuiteName; 
	executeBySlParse = executeBySl; 
	statusTestcaseSS = statusTestcase; 
	fromDateSS = fromDate;
	toDateSS = toDate;
	assignToParse = filterAssignTo; 
	descriptionSS = description; 
	statusIssueParse = statusIssue;
	categoryIssueParse = categoryIssue;
	milestoneParse = milestoneTestcase;
	var filterTestcase = {
			"testsuiteName": testsuiteName,
			"executeBy": executeBySl,
			"statusTestcase": statusTestcase,
			"fromDate": fromDate,
			"toDate": toDate,
			"assignTo": filterAssignTo,
			"type": "automation",
			"fileName": description,
			"statusIssue": statusIssue,
			"categoryIssue": categoryIssue,
			"milestone": milestoneTestcase,
	}
	$("#bodyTestcaseRecord").empty();
	$("#loadListTestCase").fadeIn("slow");
	$.ajax({
		url : jscontext +"/project/" + projectId +"/filterAutoTc",
		type : "post",
		data : JSON.stringify(filterTestcase),
		contentType : "application/json", 
		success : function(result) {
			$("#bodyTestcaseRecord").empty();
			if (!(result.errormessage) && (result.data != null)) {
				var tcFilter = result.data;
				
				if(tcFilter.length == 0){
					$("#totalTcAuto").text(0);
					var htmlRecordTestsuites =	'<tr><td colspan="6" class="text-center text-danger">'+getLang(currentLocale,"The testcase automation is empty!!!")+'</td></tr>'
					$("#bodyTestcaseRecord").html(htmlRecordTestsuites);
					
				}else{
					$("#totalTcAuto").text(tcFilter.length);
					$.each(tcFilter, function(index,values){
//						listObTc[values.testcase] = values.orderId;
						drawListTestcaseByProjectId(index, values);
					});
				}
				
				$("#loadListTestCase").fadeOut("slow");
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load get Testcase"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//func validation add TestCase
var validateAddTestCase = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#new_testcase');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }
    );
    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        rules: {
            testcaseDescription: {
                minlength: 5,
            },
        },
         
        messages: {
        	testcaseDescription: getLang(currentLocale,"Please enter at least 5 characters!")
          },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'}); 
           
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            tooltipValidation();
        },
        success: function (label, element) {
        	
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
            tooltipValidation();
        },

        submitHandler: function (form) {
            success2.show();
            error2.hide();
            
            addNewTestCase();
            document.getElementById("new_testcase").reset();
    	    var $alertas = $('#new_testcase');
    	    $alertas.validate().resetForm();
    	    $alertas.find('.form-group').removeClass('has-success');
    	    $alertas.find('.fa').removeClass('fa-check');
        }
    });
    return {
    //main function to initiate the module
        init: function () {

            handleValidation1();
            handleValidation2();
        }
    }
}

//func add Test Case
function addNewTestCase(){
	$("#saveTestCase").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#saveTestCase").prop("disabled", true);
	
	var testsuiteName = $("#lsTestsuite option:selected").text();
	var variableProjectId = $("#variableProject").val();
	var assignTo = $("#assignToTc option:selected").val();
	var ds = new Date().toISOString();
	var testcaseName = $('#testcase_name').val();
	if(testcaseName == undefined){
		testcaseName = "";
	}
	
	var orderIdAuto = ($("#orderIdAuto").val() ? parseInt($("#orderIdAuto").val())+1 : 0);
	var testcaseDescription = $('#testcase_description').val();
	var Testscripts =[];
	var projectPro = {
			"projectId": projectId,
			"testsuite": testsuiteName,
			"testcase": testcaseName
	}
	var automationTestcase = {
			"projectProgressIdentity": projectPro,
		    "description": testcaseDescription,
		    "expectedOutput": "",
		    "testcaseProcedure": "",
		    "updateDate": ds,
		    "testscript": "",
		    "createDate": ds,
		    "updateUser": projectId,
		    "assignTo": assignTo,
		    "preCondition": "",
		    "status": "add",
		    "type": "automation",
		    "statusIssue": $("#statustaskTc option:selected").text(),
		    "planStartDate": $("#planStDa").val(),
		    "planEndDate": $("#planEnDa").val(),
		    "estimatedTime": $("#estimatedTc").val(),
		    "actualTime": $("#actualTimeTc").val(),
		    "category": $("#categorytaskTc option:selected").text(),
		    "orderId": orderIdAuto,
		    "milestone": $("#milestoneTC option:selected").val(),
	}
	
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/saveTestCase",
		type : "POST",
		data : JSON.stringify(automationTestcase),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				swal({
				    type: 'success',
				    title: result.data + " "+getLang(currentLocale,"testcase was created"),
				    timer: 2000,
			        showConfirmButton: false
				});
				$("#modalAddNewTestcase").modal("hide");
				$("#bodyTestcaseRecord").empty();
				getAllTestcaseAutomationByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcase").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue,milestoneTestcase );
//				drawTestsuiteReport();
//				getAllTestcasesByStatus();
				$("#saveTestCase").html("<i class='ti-check'></i>"+getLang(currentLocale,"Save"));
				$("#saveTestCase").prop("disabled", false);
				$("#orderIdAuto").val("");
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

function showConfirmMessages(isThis) {
	 var testcaseName = $(isThis).attr("data-name");
	 var testsuiteName = $(isThis).attr("testsuite-name");
   swal({
       title: getLang(currentLocale,"Are you sure delete TestCase: ") + testcaseName +"?",
       text: getLang(currentLocale,"You will not be able to recover this imaginary file!"),
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: getLang(currentLocale,"Yes, delete it!"),
       closeOnConfirm: false
   }, function () {
   	deleteTestCase(testcaseName, testsuiteName);
   	swal({
	        title: getLang(currentLocale,"Delete TestCase Success!!!"),
	        type: "success",
	        timer: 2000,
	        showConfirmButton: false
	    });
   });
}

//delete testcase
function deleteTestCase(testcaseName, testsuiteName){
	var projectId = $("#projectId").val();
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteName+"/deleteTestCase",
		type : "POST",
		data : testcaseName,
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#bodyTestcaseRecord").empty();
				getAllTestcaseAutomationByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcase").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue,categoryIssue,milestoneTestcase);
				
//				drawTestsuiteReport();
			} else {
			}
			
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

//coppy Testcase
function copyTestCases(){
	$("#copyTestcaseId").html("<i class='fa fa-circle-o-notch fa-spin'></i> "+getLang(currentLocale,"Please waiting!"));
	$("#copyTestcaseId").prop("disabled", true);

	var testcaseNameOld = $("#testcaseNameOld").text();
	
	var testsuiteChoose = $("#testsuiteList option:selected").text();

	var testCase = "";
	$.each(autoTcLs, function(index, value){
		if(value.testcase == testcaseNameOld) {
			testCase =  Object.assign({}, value);
			return false;
		}
	});
	
	var projectPro = {
			"projectId": projectId,
			"testsuite": testsuiteChoose,
			"testcase": ""
	}
	var testscript = "";
	if(testCase.testscript != ""){
		
		testscript = testCase.testscript;
	}
	var automationTestcase = {
			"projectProgressIdentity": projectPro,
		    "description": testCase.description,
		    "expectedOutput": testCase.expectedOutput,
		    "testcaseProcedure": testCase.testcaseProcedure,
		    "updateDate": new Date().toISOString(),
		    "testscript": testscript,
		    "createDate": new Date().toISOString(),
		    "updateUser": projectId,
		    "assignTo": testCase.assignTo,
		    "preCondition": "",
		    "status": "add",
		    "statusIssue": "New",
		    "type": "automation",
		    "orderId": 0,
		    "planStartDate": (testCase.planStartDate ? new Date(testCase.planStartDate).toISOString(): ""),
		    "planEndDate": (testCase.planEndDate ? new Date(testCase.planEndDate).toISOString() : ""),
		    "estimatedTime": (testCase.estimatedTime ? testCase.estimatedTime: ""),
		    "actualTime": (testCase.actualTime ? testCase.actualTime: ""),
		    "category": (testCase.category ? testCase.category: ""),
		    "milestone": (testCase.milestone ? testCase.milestone: "")
	}
	
    
	$.ajax({
		url : jscontext+"/project/"+projectId+"/"+testsuiteChoose+"/saveTestCase",
		type : "POST",
		data : JSON.stringify(automationTestcase),
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				$("#modalCopyTestcases").modal("hide");
				$("#bodyTestcaseRecord").empty();
				
				getAllTestcaseAutomationByProject();
				
				var testsuiteName = $("#filterTestsuite").val();
				var executeBySl = $("#filterExecuteBy").val();
				var statusTestcase = $("#filterStatusTestcase").val();
				var fromDate = $("#filterFromDate").val();
				var toDate = $("#filterToDate").val();
				var filterAssignTo = $("#filterAssignTo").val();
				var description = $("#filterDescription").val();
				var statusIssue = $("#filterTestcaseStatus").val();
				var categoryIssue = $("#filterTestcaseCategory").val();
				var milestoneTestcase = $("#filterTestcaseMilestone").val();
				drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue, milestoneTestcase);
//				drawTestsuiteReport();
//				getAllTestcasesByStatus();
				$("#copyTestcaseId").html("<i class='ti-check'></i>"+ getLang(currentLocale,"Save"));
				$("#copyTestcaseId").prop("disabled", false);
				
			} else {
			}
			swal({
			    type: 'success',
			    title: result.data + " testcase was created",
			    timer: 2000,
		        showConfirmButton: false
			});
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			}
		},
		done : function(e) {
			console.log("DONE");
		}
	});

	
	
}

var lsAssign;
//get user assign to
function getUserAssignTo(status, callbackF){
	$.ajax({
		url : jscontext+"/project/"+projectId+"/getUsers",
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				if(status == "true"){
					var lsUser = [];
					duplicateUserList = duplicateUser(result.data);
					$.each(result.data, function(index, values){
						if(values.lastname != null && values.firstname != null ){
							var obTs = {};
							obTs["id"] = values.username;
							var count = 0 ;
							if(duplicateUserList.length > 0){
								$.each(duplicateUserList, function(indexDup,valueDup){
									if(valueDup.username == values.username){
										obTs["text"] = values.lastname  + " "+ values.firstname + "(" + values.username + ")";
										count ++;
									}					
								})
							} 
							
							if(count == 0 ){
								obTs["text"] = values.lastname  + " "+ values.firstname;
							}
							
							lsUser.push(obTs);
						}
					});
					callbackF(lsUser);
				} else {
					callbackF(result.data);
				}
				
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

function duplicateUser(arr){
	var duplicateIds = [];
	var valueDupl = arr
    .map(e => e['lastname'] + e['firstname'])
    .map((e, i, final) => final.indexOf(e) !== i && i)
    .filter(obj => arr[obj])
    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
	 $.each(valueDupl, function(index, value){
		 $.each(arr, function(indexArr, valueArr){
			 var name = valueArr["lastname"] + valueArr["firstname"]; 
			 if(name == value){
				 duplicateIds.push(valueArr);
			 }
		 })
	 })
	 
	 return duplicateIds ;
}

var scrollTop = "";

function fucntionAddNewIssue(projectId, idLastResult){
	scrollTop = $("html,body").scrollTop();
	$("#tableListTestcase").hide();
	var backButton = '<button class="btnBackDetail mdl-button mdl-js-button mdl-js-ripple-effect btn-default" onclick="backListTestcase();">'
			+'<i class="icon-arrow-left"></i> Back'
			+'</button>';
	$("#backIssueButton").html(backButton);
	var saveButton = '<button class="mdl-button mdl-js-button mdl-js-ripple-effect btn-success" type="button"  onclick ="newTask(\'testcase\',\'true\')" id="create-task"><i class="ti-check mr-2"></i>Save</button>';
	$("#saveIssueButton").html(saveButton);
	$('#newTask').removeAttr('hidden');
	$("#newTask").show();
	
	$("#idTask").text("");
	listImages = [];
	$(".newTask").trigger("reset");
	$(".listFiles").empty();
	descriptionTask.data.set("");
	$("#noteEdit").attr("hidden", true);
	
	$("#errorSubject").text("")
	$("#errorPlanDate").text("");
	$("#errorAcutalDate").text("");
	$("#errorMilestone").text("");
	
	infProjectCom(function(projectCo){
		$("#trackerTask").empty();
		$("#statusTask").empty();
		$("#priorityTask").empty();
		$("#categoryTask").empty();
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Tracker"){
				$("#trackerTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>');
			} else if(projectCo[i]["groupCode"] == "Status"){
				$("#statusTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Priority"){
				$("#priorityTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			} else if(projectCo[i]["groupCode"] == "Category"){
				$("#categoryTask").append('<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>')
			}
		}
		$("#priorityTask option[value='Normal']").prop("selected",true);
		if($("#trackerTask").val() == "Bug"){
			$("#bugId").attr("disabled", false);
		}
	});
	$("#bugId").val(idLastResult);
	getUserProject(function(userProjectList){
		$("#assigneeTask").empty();
		$("#watchers").empty();
		$("#assigneeTask").append('<option value = ""></option>');
		for(var i =0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null  && userProjectList[i]["firstname"] != null){
				var count = 0 ;
				if(duplicateUserList.length > 0){
					$.each(duplicateUserList, function(indexDup,valueDup){
						if(valueDup.username == userProjectList[i]["username"]){
							$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</li>');
							$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+"("+userProjectList[i]["username"]+")"+'</option>');
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					$("#watchers").append('<li name="'+userProjectList[i]["username"]+'" ><input class="mx-2 my-2 ckb-watcher"  type="checkbox" value="'+userProjectList[i]["username"]+'" id="'+userProjectList[i]["id"]+'">'+userProjectList[i]["lastname"]+" "+ userProjectList[i]["firstname"]+'</li>');
					$("#assigneeTask").append('<option value = "'+ userProjectList[i]["username"] +'">'+ userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"]+'</option>');
				}
			}	
		}
		$('#assigneeTask option[value=""]').prop("selected",true);
	});
	
	getListTestsuite("false", function(testsuitels){
		$("#testsuiteIssue").empty();
		var htmlOptions = "";
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			
		});
		$("#testsuiteIssue").append(htmlOptions);
		
	});
	
	getAllMilestoneInIssue(function(milestoneLs){
		listMilestone = milestoneLs;
		$("#milestoneIssue").empty();
		var htmlOptions = "";
		var number = 0;
		$.each( milestoneLs, function(index, valuesItemTestsuite) { 
			var dateStart = new Date(valuesItemTestsuite["startDate"]);
			var dateEnd = new Date(valuesItemTestsuite["endDate"]);

			var dateNow = new Date().getTime();
			if(dateNow >= dateStart.getTime() && dateNow <= dateEnd.getTime()){ 
				number += 1;
				if(number == 1){
					htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'" selected>'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
					
					$("#planEndDateTask").val(converDate(valuesItemTestsuite["endDate"]));
					
				} else {
					htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
				}
				
			} else {
				htmlOptions += '<option value="'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'">'+valuesItemTestsuite.projectMilestoneIdentity["mileStoneName"]+'</option>';	
			}
		});
		$("#milestoneIssue").append(htmlOptions);
		
	});
	
	$(".choose-watcher").empty();
	$(".tag-watchers").click(function(){
		$("#watchers li input").prop("checked", false) ;
		$('.choose-watcher :checkbox:checked').each(function(i){
	    	$("#watchers li[name='"+$(this).val()+"'] input").prop("checked", true);
		});
		$(".show-watchers").on('hide.bs.dropdown', function () {
			$(".choose-watcher").empty();
			$('.show-watchers :checkbox:checked').each(function(i){
				var nameWatcher = $(this).val();
				$.each(listUsers,function(index, value){
					if(value["username"] == nameWatcher){
						var count = 0 ;
						if(duplicateUserList.length > 0){
							$.each(duplicateUserList, function(indexDup,valueDup){
								if(valueDup.username == nameWatcher){
									$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " "+ value.firstname +"("+ nameWatcher+")"+'</p>');
									count ++;
								}					
							})
						} 
						
						if(count == 0 ){
							$(".choose-watcher").append('<p class="m-0"><input class="mx-2 my-2 watcher-ck" type="checkbox" value="'+nameWatcher+'" id="watcher-'+i+'">'+value.lastname + " " + value.firstname+'</p>');
						}
					}
				})
		    	$("#watcher-"+i).prop( "checked", true );
			});
		});	
	});
	
	var testcaseLs = "";
	$.each(autoTcLs, function(ind, vals){
		if(vals.id == idLastResult){
			testcaseLs = vals;
		}
	});
	var descHtml = getValueDescriptionIssue(testcaseLs).first;
	var subjectHtml = getValueDescriptionIssue(testcaseLs).second;
	
	descriptionTask.data.set(descHtml);
	$("#subjectTask").val(subjectHtml);
}

function getValueDescriptionIssue(testcaseLs){
	
	var descriptionHtml = '- <strong>Milestone</strong>: '+ (testcaseLs.milestone ? testcaseLs.milestone : "") +'<br>'
	+ '- <strong>Os</strong>: ' + (testcaseLs.os ? testcaseLs.os :"") + '<br>'
	+ '- <strong>Browser</strong>: ' + (testcaseLs.browser ? testcaseLs.browser : "") + '<br>'
	+ '- <strong>Screen Resolution</strong>: ' + (testcaseLs.screenResolution ? testcaseLs.screenResolution : "")+ '<br>'
	+ '- <strong>Location</strong>: ' + (testcaseLs.location ? testcaseLs.location : "") + '<br>'
	+ '- <strong>Mobile Mode</strong>: ' + (testcaseLs.mobileMode ? testcaseLs.mobileMode: "") + '<br>'
	+ '- <strong>Device Name</strong>: ' + (testcaseLs.deviceName ? testcaseLs.deviceName : "") + '<br>'
	+ '- <strong>Run Status</strong>: ' + (testcaseLs.testCaseResult ? testcaseLs.testCaseResult: "") + '<br>'
	+ '- <strong>Run User</strong>: ' + (testcaseLs.runningUser ? testcaseLs.runningUser : "") + '<br>'
	+ '- <strong>Run Time</strong>: ' + (testcaseLs.executeDate ? testcaseLs.executeDate : "") + '<br>'
	+ '- <strong>Run Description</strong>: ' + (testcaseLs.runningDescription ? testcaseLs.runningDescription : "") + '<br>'
	+ '- <strong>Pre Condition</strong>: ' + (testcaseLs.preCondition ? testcaseLs.preCondition : "") +'<br>'
	+ '- <strong>Testcase Procedure</strong>: ' + (testcaseLs.testcaseProcedure ? testcaseLs.testcaseProcedure : "") +'<br>'
	+ '- <strong>Expected Output</strong>: ' + (testcaseLs.expectedOutput ? testcaseLs.expectedOutput : "")+ '<br>' 
	+ '- <strong>Actual Output</strong>: ' + (testcaseLs.actualOutput ? testcaseLs.actualOutput : "");

	var subjectHtml = testcaseLs.testcase + " ("+ testcaseLs.description+"): " + testcaseLs.testCaseResult;
	
	return {
		first: descriptionHtml,
        second: subjectHtml,
	};
}

function backListTestcase(){
		$(".newTask").trigger("reset");
		descriptionTask.data.set("");
		$("#newTask").hide();
		$("#tableListTestcase").show();
		$("html,body").scrollTop(scrollTop);
		scrollTop = "";
}

function getStatusIssue(projectId, bugId, callbackF){
	$.ajax({
		url : jscontext +"/project/"+projectId+"/tasks/getTaskByBugId/" + bugId,
		type : "GET",
		contentType : "application/json",
		success : function(result) {
			if (!(result.errormessage) ) {
				var task = result.data;
				callbackF(task);
			} else {
			}
		},
		error : function(request, e) {
			console.log("ERROR: ", e);
			
			var statusCode = request.status;
			if (statusCode == 403) {
				window.location= jscontext + "/login";
			} else {
				swal({
					title: getLang(currentLocale,"Could not load data"),
			        text: getLang(currentLocale,"You can reload the page again!"),
			        type: "warning", 
			        timer: 2000,
			        showConfirmButton: false
			    });
			}
		},
		done : function(e) {
			console.log("DONE");
			swal({
				title: getLang(currentLocale,"Could not load data"),
		        text: getLang(currentLocale,"You can reload the page again!"),
		        type: "warning", 
		        timer: 2000,
		        showConfirmButton: false
		    });
		}
	});
}

function filterAutoTestcase(){
	var testsuiteName = $("#filterTestsuite").val();
	var executeBySl = $("#filterExecuteBy").val();
	var statusTestcase = $("#filterStatusTestcase").val();
	var fromDate = $("#filterFromDate").val();
	var toDate = $("#filterToDate").val();
	var filterAssignTo = $("#filterAssignTo").val();
	var description = $("#filterDescription").val();
	var statusIssue = $("#filterTestcaseStatus").val();
	var categoryIssue = $("#filterTestcaseCategory").val();
	var milestoneTestcase = $("#filterTestcaseMilestone").val();
	$("#filterTcAuto").html("<i class='fa fa-circle-o-notch fa-spin'></i> " +getLang(currentLocale,"Please waiting!"));
	$("#filterTcAuto").prop("disabled", true);
	
	drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description, statusIssue, categoryIssue, milestoneTestcase);
	
	$("#filterTcAuto").html("<i class='ti-search text-white'></i> "+getLang(currentLocale,"Search"));
	$("#filterTcAuto").prop("disabled", false);
}

function showModalAddNewTestcaseByOrderAuto(orderId){
	funcAddNewTestcase();
	$("#orderIdAuto").val(orderId);
}

$("#filterTestsuite").on('select2:opening', function(){
	checkSession = false;
	$("#filterTestsuite").one('change', function(){
		checkFilterManual(checkSession);
		
	});
});

$("#filterStatusTestcase").on('select2:opening', function(){
	checkSession = false;
	$("#filterStatusTestcase").one('change', function(){
		$("#filterExecuteBy").attr("disabled", false);
		checkFilterManual(checkSession);
	});
});

$("#filterExecuteBy").on('select2:opening',function(){
	checkSession = false;
	$("#filterExecuteBy").one('change', function(){
		checkFilterManual(checkSession);
	});
});

$("#filterAssignTo").on('select2:opening',function(){
	checkSession = false;
	$("#filterAssignTo").one('change', function(){
		checkFilterManual(checkSession);
	});
});

$("#filterFromDate").on('change', function(){
	$("#filterFromDate").attr("disabled",false);
	checkSession = false;
	checkFilterManual(checkSession);
});

$("#filterToDate").on('change', function(){
	$("#filterToDate").attr("disabled",false);
	checkSession = false;
	checkFilterManual(checkSession);
});

$("#filterDescription").on('change', function(){
	checkSession = false;
	checkFilterManual(checkSession);
});

function checkFilterManual(checkSession){
	var testsuiteName = $("#filterTestsuite").val();
	var executeBySl = $("#filterExecuteBy").val();
	var statusTestcase = $("#filterStatusTestcase").val();
	var fromDate = $("#filterFromDate").val();
	var toDate = $("#filterToDate").val();
	var filterAssignTo = $("#filterAssignTo").val();
	var description = $("#filterDescription").val();
	if(checkSession != true){
		drawTestcaseByFilter(testsuiteName, executeBySl, statusTestcase, fromDate, toDate, filterAssignTo, description);
	}
}

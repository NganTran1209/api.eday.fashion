package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the user_project database table.
 * 
 */
@Entity
@Table(name="user_project")
@NamedQuery(name="UserProject.findAll", query="SELECT u FROM UserProject u")
public class UserProject implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final String PROJECT_OWNER = "PROJECT_OWNER";
	public static final String PROJECT_MEMBER = "PROJECT_MEMBER";
	public static final String PROJECT_ADMIN = "PROJECT_ADMIN";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long projectId;
	
	private String projectName;

	private String projectRole;

	private String username;

	public UserProject() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProjectId() {
		return this.projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectRole() {
		return this.projectRole;
	}

	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@Override
	public String toString() {
		return "UserProject [id=" + id + ", projectId=" + projectId + ", projectName=" + projectName + ", projectRole="
				+ projectRole + ", username=" + username + "]";
	}
	
	

}
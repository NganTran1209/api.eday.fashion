package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the project database table.
 * 
 */
@Entity
@NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p")
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;
	
	@Column(columnDefinition="TEXT")
	private String description;

	/**
	 * Public Project, Private Project
	 */
	private String type;

	private String scmType;

	private String scmUrl;

	private String scmUsername;

	private String scmPassword;

	private String parentProject;

	private String createdBy;

	private String reportTime;

	private String status;

	/**
	 * neu private project thi refer process la public process ma tu do copy
	 */
	private String referProcess;
	
	@Column(columnDefinition="TEXT")
	private String image;
	
	@Column(columnDefinition="TEXT")
	private String video;

	/**
	 * quy mo toi thieu, quy mo tieu chuan tu do tinh ra du toan
	 */
	private double standardSize;

	/**
	 * don vi: m2, con
	 */
	private String standardUnit;

	/**
	 * uoc luong nang suat
	 */
	private double estimatedProductivity;
	/**
	 * gia ban du tinh
	 */
	private double estimatedPrice;

	/**
	 * tong du toan
	 */
	private double estimatedCost;
	
	/**
	 * von hien co
	 */
	private double investAmmount;

	/**
	 * khoang thoi gian san xuat
	 */
	private int estimatedDuration;

	private Date planStartDate;

	private Date planEndDate;
	
	/**
	 * qui mo san xuat
	 */
	private double actualSize;

	private Date actualStartDate;

	private Date actualEndDate;

	private double actualPrice;

	/**
	 * nang suat thuc te
	 */
	private double actualProductivity;

	/**
	 * san luong thuc te
	 */
	private double actualQuantity;

	/**
	 * trong trot, chan nuoi
	 */
	private String productType;
	
	/**
	 * ma san pham cua quy trinh
	 */
	private String productCode;
	
	private String productName;
	
	/**
	 * ma gap tuong ung voi quy trinh
	 */
	private String standardGAP;
	
	private String reportTimeZone;

	@Column(name = "create_date")
	private Date createDate;

	private int deletedFlag;
	
	private String color;
	
	private String address;
	
	private String location;
	
	private String productionUnit;
	
	private boolean requires;

	public String getProductionUnit() {
		return productionUnit;
	}

	public void setProductionUnit(String productionUnit) {
		this.productionUnit = productionUnit;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStandardGAP() {
		return standardGAP;
	}

	public void setStandardGAP(String standardGAP) {
		this.standardGAP = standardGAP;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReportTime() {
		return reportTime;
	}

	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Project() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScmType() {
		return scmType;
	}

	public void setScmType(String scmType) {
		this.scmType = scmType;
	}

	public String getScmUrl() {
		return scmUrl;
	}

	public void setScmUrl(String scmUrl) {
		this.scmUrl = scmUrl;
	}

	public String getScmUsername() {
		return scmUsername;
	}

	public void setScmUsername(String scmUsername) {
		this.scmUsername = scmUsername;
	}

	public String getScmPassword() {
		return scmPassword;
	}

	public void setScmPassword(String scmPassword) {
		this.scmPassword = scmPassword;
	}

	public String getParentProject() {
		return parentProject;
	}

	public void setParentProject(String parentProject) {
		this.parentProject = parentProject;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getReportTimeZone() {
		return reportTimeZone;
	}

	public void setReportTimeZone(String reportTimeZone) {
		this.reportTimeZone = reportTimeZone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public Date getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public String getReferProcess() {
		return referProcess;
	}

	public void setReferProcess(String referProcess) {
		this.referProcess = referProcess;
	}

	public String getStandardUnit() {
		return standardUnit;
	}

	public void setStandardUnit(String standardUnit) {
		this.standardUnit = standardUnit;
	}

	public double getEstimatedProductivity() {
		return estimatedProductivity;
	}

	public void setEstimatedProductivity(double estimatedProductivity) {
		this.estimatedProductivity = estimatedProductivity;
	}

	public double getEstimatedPrice() {
		return estimatedPrice;
	}

	public void setEstimatedPrice(double estimatedPrice) {
		this.estimatedPrice = estimatedPrice;
	}

	public double getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(double estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public int getEstimatedDuration() {
		return estimatedDuration;
	}

	public void setEstimatedDuration(int estimatedDuration) {
		this.estimatedDuration = estimatedDuration;
	}

	public double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public double getActualProductivity() {
		return actualProductivity;
	}

	public void setActualProductivity(double actualProductivity) {
		this.actualProductivity = actualProductivity;
	}

	public double getActualQuantity() {
		return actualQuantity;
	}

	public void setActualQuantity(double actualQuantity) {
		this.actualQuantity = actualQuantity;
	}

	public double getStandardSize() {
		return standardSize;
	}

	public void setStandardSize(double standardSize) {
		this.standardSize = standardSize;
	}

	public double getInvestAmmount() {
		return investAmmount;
	}

	public void setInvestAmmount(double investAmmount) {
		this.investAmmount = investAmmount;
	}

	public double getActualSize() {
		return actualSize;
	}

	public void setActualSize(double actualSize) {
		this.actualSize = actualSize;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public boolean isRequire() {
		return requires;
	}

	public void setRequire(boolean requires) {
		this.requires = requires;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
}
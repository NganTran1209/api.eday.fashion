package io.bottest.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "bill")
public class Bill {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String code;
	
	private int userBuy;
	
	private int userSell;
	
	private int productId;
	
	private Long amount;
	
	@Column(name = "create_day")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date createDay;
	
	private String addrBuy;
	
	private String addrShipTo;
	
	private String type_payment;
	
	private String status;
	
	private int couponId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getUserBuy() {
		return userBuy;
	}

	public void setUserBuy(int userBuy) {
		this.userBuy = userBuy;
	}

	public int getUserSell() {
		return userSell;
	}

	public void setUserSell(int userSell) {
		this.userSell = userSell;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Date getCreateDay() {
		return createDay;
	}

	public void setCreateDay(Date createDay) {
		this.createDay = createDay;
	}

	public String getAddrBuy() {
		return addrBuy;
	}

	public void setAddrBuy(String addrBuy) {
		this.addrBuy = addrBuy;
	}

	public String getAddrShipTo() {
		return addrShipTo;
	}

	public void setAddrShipTo(String addrShipTo) {
		this.addrShipTo = addrShipTo;
	}

	public String getType_payment() {
		return type_payment;
	}

	public void setType_payment(String type_payment) {
		this.type_payment = type_payment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCouponId() {
		return couponId;
	}

	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	
	
}

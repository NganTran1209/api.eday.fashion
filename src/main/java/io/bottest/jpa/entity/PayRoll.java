package io.bottest.jpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * bang luong
 */
@Entity
@Table(name = "payroll")
public class PayRoll {
	
	@EmbeddedId
	private PayRollIdentity payRollIdentity;
	
	
	/**
	 * Luong theo hop dong
	 */
	@Column(name = "contract_salary")
	private Long contractSalary;
	
	/**
	 * so gio lam viec trong ngay
	 */
	@Column(name = "hour_of_day")
	private String hourOfDay;
	
	/**
	 * luong toi cuoi thang
	 */
	@Column(name = "salary_to_month")
	private	Long salaryToMonth;
	
	/**
	 * ngay nghi cong nhan
	 */
	@Column(name = "day_off",length = 500)
	private String dayOff;
	
	/**
	 * ngay lam viec cong nhan
	 */
	@Column(name = "day_work",length = 500)
	private String dayWork;
	
	/**
	 * tinh trang luong cua cong nhan
	 */
	@Column(name = "status")
	private boolean status;
	
	/**
	 * luong thuong them
	 */
	@Column(name = "bonus")
	private Long bonus;
	
	/**
	 * tong luong
	 */
	@Column(name = "total_salary")
	private Long totalSalary;
	
	/**
	 * ghi chu them
	 */
	@Column(name = "comment",length = 500)
	private String comment;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "workerId", referencedColumnName = "id",insertable=false, updatable=false, nullable = true)
	private Worker worker;
	
	public PayRollIdentity getPayRollIdentity() {
		return payRollIdentity;
	}

	public void setPayRollIdentity(PayRollIdentity payRollIdentity) {
		this.payRollIdentity = payRollIdentity;
	}

	public String getHourOfDay() {
		return hourOfDay;
	}

	public void setHourOfDay(String hourOfDay) {
		this.hourOfDay = hourOfDay;
	}

	public Long getSalaryToMonth() {
		return salaryToMonth;
	}

	public void setSalaryToMonth(Long salaryToMonth) {
		this.salaryToMonth = salaryToMonth;
	}

	public String getDayOff() {
		return dayOff;
	}

	public void setDayOff(String dayOff) {
		this.dayOff = dayOff;
	}

	public String getDayWork() {
		return dayWork;
	}

	public void setDayWork(String dayWork) {
		this.dayWork = dayWork;
	}

	public Long getContractSalary() {
		return contractSalary;
	}

	public void setContractSalary(Long contractSalary) {
		this.contractSalary = contractSalary;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Long getBonus() {
		return bonus;
	}

	public void setBonus(Long bonus) {
		this.bonus = bonus;
	}

	public Long getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(Long totalSalary) {
		this.totalSalary = totalSalary;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Worker getWorker() {
		return worker;
	}

	public void setWorker(Worker worker) {
		this.worker = worker;
	}
	
	
}

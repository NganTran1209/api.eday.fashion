package io.bottest.jpa.entity;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name= "project_milestone")
public class ProjectMilestone {
	
	@EmbeddedId
	private ProjectMileStoneIdentity projectMilestoneIdentity;
	
	private String description;
	
	private Date startDate;
	
	private Date endDate;
	
	private Date updateDate;
	
	private String status;
	private String statusMilestone;

	public ProjectMilestone() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public ProjectMileStoneIdentity getProjectMilestoneIdentity() {
		return projectMilestoneIdentity;
	}

	public void setProjectMilestoneIdentity(ProjectMileStoneIdentity projectMilestoneIdentity) {
		this.projectMilestoneIdentity = projectMilestoneIdentity;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatusMilestone() {
		return statusMilestone;
	}


	public void setStatusMilestone(String statusMilestone) {
		this.statusMilestone = statusMilestone;
	}
	
}

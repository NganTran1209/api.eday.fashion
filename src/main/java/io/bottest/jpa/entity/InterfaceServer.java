package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="InterfaceServer.findAll", query="SELECT i FROM InterfaceServer i")
public class InterfaceServer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6065465250331847743L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	private String domain;
	
	
	private String username;
	
	
	private String token;
	
	
	private String jobName;
	

	private String os;
	
	private String runningOS;
	
	private String browser;
	
	private String location;
	
	private String scmType;
	
	private String nodeLabel;
	
	private boolean isLocal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}


	public String getRunningOS() {
		return runningOS;
	}

	public void setRunningOS(String runningOS) {
		this.runningOS = runningOS;
	}

	@Override
	public String toString() {
		return "InterfaceServer [id=" + id + ", domain=" + domain + ", username=" + username + ", token=" + token
				+ ", jobName=" + jobName + ", os=" + os + ", runningOS=" + runningOS + ", browser=" + browser + ",location=" + location + "]";
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getScmType() {
		return scmType;
	}

	public void setScmType(String scmType) {
		this.scmType = scmType;
	}

	public String getNodeLabel() {
		return nodeLabel;
	}

	public void setNodeLabel(String nodeLabel) {
		this.nodeLabel = nodeLabel;
	}

	public boolean isLocal() {
		return isLocal;
	}

	public void setLocal(boolean isLocal) {
		this.isLocal = isLocal;
	}
	
	


	
	
	
}

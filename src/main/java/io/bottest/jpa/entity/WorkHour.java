package io.bottest.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "work_hour")
public class WorkHour {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int userId;
	
	private int month;
	
	private int year;
	
	@Column(name = "date_leave_from")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date dateLeaveFrom;
	
	@Column(name = "date_leave_to")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date dateLeaveTo;
	
	private int typeLeave;
	
	private int work_time;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getDateLeaveFrom() {
		return dateLeaveFrom;
	}

	public void setDateLeaveFrom(Date dateLeaveFrom) {
		this.dateLeaveFrom = dateLeaveFrom;
	}

	public Date getDateLeaveTo() {
		return dateLeaveTo;
	}

	public void setDateLeaveTo(Date dateLeaveTo) {
		this.dateLeaveTo = dateLeaveTo;
	}

	public int getTypeLeave() {
		return typeLeave;
	}

	public void setTypeLeave(int typeLeave) {
		this.typeLeave = typeLeave;
	}

	public int getWork_time() {
		return work_time;
	}

	public void setWork_time(int work_time) {
		this.work_time = work_time;
	}
	
	
	
}

package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="ProjectComponent.findAll", query="SELECT pc FROM ProjectComponent pc")
public class ProjectComponent implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String groupCode;
	
	private String itemName;
	
	private String itemValue;
	
	private Long projectId;
	
	private int status;
	
	private String colorCode;
	
	
	
	public ProjectComponent() {
		super();
	}

	public ProjectComponent(String groupCode, String itemName, String itemValue, Long projectId, int status,
			String colorCode) {
		super();
		this.groupCode = groupCode;
		this.itemName = itemName;
		this.itemValue = itemValue;
		this.projectId = projectId;
		this.status = status;
		this.colorCode = colorCode;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getGroupCode() {
		return groupCode;
	}
	
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getItemValue() {
		return itemValue;
	}
	
	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}
	
	public Long getProjectId() {
		return projectId;
	}
	
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	
}

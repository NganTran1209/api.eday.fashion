package io.bottest.jpa.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name= "project_summary")
public class ProjectSummary {
	@EmbeddedId
	private ProjectSummaryIdentity projectSummaryIdentity;
	
	private int pass;
	
	private int fail;
	
	private int degrade;
	
	private int notRun;
	
	private int total;
	
	private int layout;
	
	private int testdata;
	
	private int result;
	
	private int automation;
	
	private int manual;
	
	private int schedule;
	
	private String description;

	public ProjectSummary() {
		super();
	}

	public ProjectSummaryIdentity getProjectSummaryIdentity() {
		return projectSummaryIdentity;
	}

	public void setProjectSummaryIdentity(ProjectSummaryIdentity projectSummaryIdentity) {
		this.projectSummaryIdentity = projectSummaryIdentity;
	}

	public int getPass() {
		return pass;
	}

	public void setPass(int pass) {
		this.pass = pass;
	}

	public int getFail() {
		return fail;
	}

	public void setFail(int fail) {
		this.fail = fail;
	}

	public int getDegrade() {
		return degrade;
	}

	public void setDegrade(int degrade) {
		this.degrade = degrade;
	}

	public int getNotRun() {
		return notRun;
	}

	public void setNotRun(int notRun) {
		this.notRun = notRun;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	public int getTestdata() {
		return testdata;
	}

	public void setTestdata(int testdata) {
		this.testdata = testdata;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getAutomation() {
		return automation;
	}

	public void setAutomation(int automation) {
		this.automation = automation;
	}

	public int getManual() {
		return manual;
	}

	public void setManual(int manual) {
		this.manual = manual;
	}

	public int getSchedule() {
		return schedule;
	}

	public void setSchedule(int schedule) {
		this.schedule = schedule;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	};
	
}

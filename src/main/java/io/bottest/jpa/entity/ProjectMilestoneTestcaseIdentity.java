package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.Null;

@Embeddable
public class ProjectMilestoneTestcaseIdentity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Null
	private Long projectId;
	
	@Null
	private String milestoneName;
	
	@Null
	private String testsuite;
	
	@Null
	private String testcase;
	
	@Null
	private String status;

	public ProjectMilestoneTestcaseIdentity() {
		super();
	}
	
	public Long getProjectId() {
		return projectId;
	}


	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}


	public String getMilestoneName() {
		return milestoneName;
	}

	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

	public String getTestcase() {
		return testcase;
	}

	public void setTestcase(String testcase) {
		this.testcase = testcase;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}

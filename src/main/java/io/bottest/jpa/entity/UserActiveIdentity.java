package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Null;

public class UserActiveIdentity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Null
	private String username;
	
	@Null
	@Temporal(TemporalType.DATE)
	private Date activeDate;

	public UserActiveIdentity() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

}

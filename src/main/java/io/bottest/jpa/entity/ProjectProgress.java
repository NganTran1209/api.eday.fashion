package io.bottest.jpa.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name= "project_progress")
public class ProjectProgress {
	
	@EmbeddedId
	private ProjectProgressIdentity projectProgressIdentity;
	
	private String description;
	
	@Column(columnDefinition="TEXT")
	private String testscript;
	
	private String status;
	
	private Date createDate;
	
	private Date updateDate;
	
	private String updateUser;
	
	private String assignTo;
	
	@Column(columnDefinition="TEXT")
	private String testcaseProcedure;
	
	@Lob
	private String preCondition;
	
	@Column(columnDefinition="TEXT")
	private String expectedOutput;
	
	private String type;
	
	private String statusIssue;
	
	private Date planStartDate;
	
	private Date planEndDate;
	
	private String estimatedTime;
	
	private String actualTime;
	
	private String category;
	
	private String milestone;
	
	@Column(nullable = true)
	private Integer orderId;
	
	@Lob
	private String file;
	
	@Transient
	@Column(columnDefinition="TEXT")
	private List<ResultInfo> results;

	public ProjectProgress() {
		super();
	}


	public ProjectProgressIdentity getProjectProgressIdentity() {
		return projectProgressIdentity;
	}

	public void setProjectProgressIdentity(ProjectProgressIdentity projectProgressIdentity) {
		this.projectProgressIdentity = projectProgressIdentity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTestscript() {
		return testscript;
	}

	public void setTestscript(String testscript) {
		this.testscript = testscript;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getTestcaseProcedure() {
		return testcaseProcedure;
	}

	public void setTestcaseProcedure(String testcaseProcedure) {
		this.testcaseProcedure = testcaseProcedure;
	}

	public String getPreCondition() {
		return preCondition;
	}

	public void setPreCondition(String preCondition) {
		this.preCondition = preCondition;
	}

	public String getExpectedOutput() {
		return expectedOutput;
	}

	public void setExpectedOutput(String expectedOutput) {
		this.expectedOutput = expectedOutput;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ResultInfo> getResults() {
		return results;
	}

	public void setResults(List<ResultInfo> results) {
		this.results = results;
	}

	public String getStatusIssue() {
		return statusIssue;
	}

	public void setStatusIssue(String statusIssue) {
		this.statusIssue = statusIssue;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public String getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}


	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

}

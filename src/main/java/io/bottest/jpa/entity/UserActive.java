package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_active")
@NamedQuery(name = "UserActive.findAll", query = "SELECT usa FROM UserActive usa")
public class UserActive implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private UserActiveIdentity userActiveIdentity;
	
	@Temporal(TemporalType.TIME)
	private Date activeTime;
	
	public UserActive() {
		super();
	}

	public UserActiveIdentity getUserActiveIdentity() {
		return userActiveIdentity;
	}



	public void setUserActiveIdentity(UserActiveIdentity userActiveIdentity) {
		this.userActiveIdentity = userActiveIdentity;
	}



	public Date getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(Date activeTime) {
		this.activeTime = activeTime;
	}
	
	
}

package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the project database table.
 * 
 */
@Entity
@NamedQuery(name="ExecutingJob.findAll", query="SELECT e FROM ExecutingJob e")
public class ExecutingJob implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String type;
	
	private String username;
	
	private String projectId;

	private String testsuite;
	
	private Date startDate;
	
	private String status;

	private String jobName;
	
	private String nodeLabel;
	
	
	@Column( columnDefinition="integer not null default -1")
	private int queueId;
	
	@Column( columnDefinition="integer not null default -1")
	private int buildNumber;
	
	private String runningName;
	
	@Column(columnDefinition="TEXT")
	private String resultJson;
	
	@Column(columnDefinition="TEXT")
	private String exceptionMessage;
	
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	private Date createdDate;
	
	private Date updatedDate;
	
	@Column(length=5000)
	private String lsTestcases;
	
	private String runningOS;
	
	private String timeout;
	
	private String runningLocation;
	
	private String screenRes;
	
	private String mobileMode;
	
	private String mobileRes;
	
	private String deviceName;
	
	private String browser;
	
	private String recordVideo;
	
	private String scheduleSpec;
	
	private String runDescription;
	
	private String newStatus;
	
	private String executeName;
	
	private String runningDate;
	
	private String projectName;
	
	private String runOnLocal;
	
			
	public ExecutingJob() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}


	public String getRunningName() {
		return runningName;
	}

	public void setRunningName(String runningName) {
		this.runningName = runningName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(int buildNumber) {
		this.buildNumber = buildNumber;
	}


	public int getQueueId() {
		return queueId;
	}

	public String getLsTestcases() {
		return lsTestcases;
	}

	public void setLsTestcases(String lsTestcases) {
		this.lsTestcases = lsTestcases;
	}

	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	public String getRunningOS() {
		return runningOS;
	}

	public void setRunningOS(String runningOS) {
		this.runningOS = runningOS;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public String getRunningLocation() {
		return runningLocation;
	}

	public void setRunningLocation(String runningLocation) {
		this.runningLocation = runningLocation;
	}

	public String getScreenRes() {
		return screenRes;
	}

	public void setScreenRes(String screenRes) {
		this.screenRes = screenRes;
	}

	public String isMobileMode() {
		return mobileMode;
	}

	public void setMobileMode(String mobileMode) {
		this.mobileMode = mobileMode;
	}

	public String getMobileRes() {
		return mobileRes;
	}

	public void setMobileRes(String mobileRes) {
		this.mobileRes = mobileRes;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getRecordVideo() {
		return recordVideo;
	}

	public void setRecordVideo(String recordVideo) {
		this.recordVideo = recordVideo;
	}

	public String getExecuteName() {
		return executeName;
	}

	public void setExecuteName(String executeName) {
		this.executeName = executeName;
	}

	public String getMobileMode() {
		return mobileMode;
	}

	public String getScheduleSpec() {
		return scheduleSpec;
	}

	public void setScheduleSpec(String scheduleSpec) {
		this.scheduleSpec = scheduleSpec;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ExecutingJob [id=" + id + ", type=" + type + ", username=" + username + ", projectId=" + projectId+", projectName="+projectName
				+ ", testsuite=" + testsuite + ", startDate=" + startDate + ", status=" + status + ", jobName="
				+ jobName + ", queueId=" + queueId + ", buildNumber=" + buildNumber + ", runningName=" + runningName
				+ ", createdDate=" + createdDate + ", updatedDate=" + updatedDate
				+ ", lsTestcases=" + lsTestcases + ", runningOS=" + runningOS + ", timeout=" + timeout
				+ ", runningLocation=" + runningLocation + ", screenRes=" + screenRes + ", mobileMode=" + mobileMode
				+ ", mobileRes=" + mobileRes + ", deviceName=" + deviceName + ", browser=" + browser + ", recordVideo="
				+ recordVideo + ", scheduleSpec=" + scheduleSpec + "]";
	}

	public String getRunDescription() {
		return runDescription;
	}

	public void setRunDescription(String runDescription) {
		this.runDescription = runDescription;
	}

	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public String getRunningDate() {
		return runningDate;
	}

	public void setRunningDate(String runningDate) {
		this.runningDate = runningDate;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getNodeLabel() {
		return nodeLabel;
	}

	public void setNodeLabel(String nodeLabel) {
		this.nodeLabel = nodeLabel;
	}

	public String getRunOnLocal() {
		return runOnLocal;
	}

	public void setRunOnLocal(String runOnLocal) {
		this.runOnLocal = runOnLocal;
	}

	public String getResultJson() {
		return resultJson;
	}

	public void setResultJson(String resultJson) {
		this.resultJson = resultJson;
	}
	
}
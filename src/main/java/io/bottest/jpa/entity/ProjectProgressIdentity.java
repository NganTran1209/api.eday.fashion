package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.Null;
@Embeddable
public class ProjectProgressIdentity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Null
	private Long projectId;
	
	@Null
	private String testsuite;
	
	@Null
	private String testcase;

	public ProjectProgressIdentity() {
		super();
	}

	public ProjectProgressIdentity(Long projectId, String testsuite, String testcase) {
		super();
		this.projectId = projectId;
		this.testsuite = testsuite;
		this.testcase = testcase;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

	public String getTestcase() {
		return testcase;
	}

	public void setTestcase(String testcase) {
		this.testcase = testcase;
	}
	
}

package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.validation.constraints.Null;

@Embeddable
public class ProjectSummaryIdentity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Null
	private Date summary_date;

	@Null
	private String projectId;

	@Null
	private String testsuite;

	public ProjectSummaryIdentity() {
		super();
	}

	public ProjectSummaryIdentity(@Null Date summary_date, @Null String projectId, @Null String testsuite) {
		super();
		this.summary_date = summary_date;
		this.projectId = projectId;
		this.testsuite = testsuite;
	}

	public Date getDate() {
		return summary_date;
	}

	public void setDate(Date summary_date) {
		this.summary_date = summary_date;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

}

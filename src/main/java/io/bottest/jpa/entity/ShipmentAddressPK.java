package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ShipmentAddressPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="user_id")
	private String userId;
	
	@Column(name="seq")
	private int seq;
	
	public ShipmentAddressPK() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ShipmentAddressPK)) {
			return false;
		}
		ShipmentAddressPK castOther = (ShipmentAddressPK)other;
		return 
			this.userId.equals(castOther.userId)
			&& this.seq == castOther.seq;
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.seq;
		
		return hash;
	}
}

package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "active_otp")
@NamedQuery(name = "ActiveOTP.findAll", query = "SELECT a FROM ActiveOTP a")
public class ActiveOTP implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "active_otp_id")
	private Long activeOtpId;
	
	@Column(name = "active_email")
	private String activeEmail;
	
	@Column(name = "active_otp_code")
	private String activeOtpCode;
	
	@Column(name = "is_active")
	private int isActive;
	
	@Column(name = "begin_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date beginTime;
	
	@Column(name = "end_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endTime;

	public Long getActiveOtpId() {
		return activeOtpId;
	}

	public void setActiveOtpId(Long activeOtpId) {
		this.activeOtpId = activeOtpId;
	}

	public String getActiveEmail() {
		return activeEmail;
	}

	public void setActiveEmail(String activeEmail) {
		this.activeEmail = activeEmail;
	}

	public String getActiveOtpCode() {
		return activeOtpCode;
	}

	public void setActiveOtpCode(String activeOtpCode) {
		this.activeOtpCode = activeOtpCode;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}

package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="ResultInfo.findAll", query="SELECT r FROM ResultInfo r")
public class ResultInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3326842588889486948L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String resultName;
	
	private String testSuite;
	
	private String testResult;
	
	private String runningUser;
	
	private Date executeDate;
	
	private String testCaseName;
	
	private String testCaseResult;
	
	private int projectId;
	
	private String milestone;
	
	private String os;
	
	private String browser;
	
	private String screenResolution;
	
	private String location;
	
	private String mobileMode;
	
	private String deviceName;
	
	@Lob
	private String runningDescription;
	
	@Column(columnDefinition="TEXT")
	private String expectedOutput;
	
	@Column(columnDefinition="TEXT")
	private String actualOutput;
	
	private String type;
	
	@Column(columnDefinition="TEXT")
	private String files;
	
	private String status;
	
	public ResultInfo(int projectId,String testSuite,String testCaseNameLong, Date executeDate, Long id,String testCaseResult) {
	
		this.id = id;
		this.testSuite = testSuite;
		this.executeDate = executeDate;
		this.testCaseResult = testCaseResult;
		this.projectId = projectId;
	}

	public ResultInfo() {
		
	}

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	public String getTestSuite() {
		return testSuite;
	}

	public void setTestSuite(String testSuite) {
		this.testSuite = testSuite;
	}

	public String getTestResult() {
		return testResult;
	}

	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}

	public String getRunningUser() {
		return runningUser;
	}

	public void setRunningUser(String runningUser) {
		this.runningUser = runningUser;
	}

	public Date getExecuteDate() {
		return executeDate;
	}

	public void setExecuteDate(Date executeDate) {
		this.executeDate = executeDate;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getTestCaseResult() {
		return testCaseResult;
	}

	public void setTestCaseResult(String testCaseResult) {
		this.testCaseResult = testCaseResult;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getScreenResolution() {
		return screenResolution;
	}

	public void setScreenResolution(String screenResolution) {
		this.screenResolution = screenResolution;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMobileMode() {
		return mobileMode;
	}

	public void setMobileMode(String mobileMode) {
		this.mobileMode = mobileMode;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getRunningDescription() {
		return runningDescription;
	}

	public void setRunningDescription(String runningDescription) {
		this.runningDescription = runningDescription;
	}

	public String getExpectedOutput() {
		return expectedOutput;
	}

	public void setExpectedOutput(String expectedOutput) {
		this.expectedOutput = expectedOutput;
	}

	public String getActualOutput() {
		return actualOutput;
	}

	public void setActualOutput(String actualOutput) {
		this.actualOutput = actualOutput;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}

package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="Component.findAll", query="SELECT c FROM Component c")
public class Component implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3326842588889486948L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String groupCode;
	
	private String itemValue;
	
	private String itemName;
	private boolean defaultC;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getItemValue() {
		return itemValue;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public boolean isDefaultC() {
		return defaultC;
	}

	public void setDefaultC(boolean defaultC) {
		this.defaultC = defaultC;
	}
}

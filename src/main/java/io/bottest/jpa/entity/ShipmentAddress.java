package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "shipment_address")
@NamedQuery(name="ShipmentAddress.findAll", query="SELECT a FROM ShipmentAddress a")
public class ShipmentAddress implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ShipmentAddressPK id;
	
	@Column(name="name_receive")
	private String nameReceive;
	
	@Column(name="address_receive")
	private String addressReceive;
	
	@Column(name="city")
	private String city;
	
	@Column(name="districts")
	private String districts;
	
	@Column(name="streets")
	private String streets;
	
	@Column(name="wards")
	private String wards;
	
	@Column(name="mail")
	private String mail;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="address_type")
	private String addressType; //nha rieng, cty
	
	@Column(name="is_default")
	private int isDefault;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;
	
	public ShipmentAddress() {
		super();
	}

	public ShipmentAddressPK getId() {
		return id;
	}

	public void setId(ShipmentAddressPK id) {
		this.id = id;
	}

	public String getNameReceive() {
		return nameReceive;
	}

	public void setNameReceive(String nameReceive) {
		this.nameReceive = nameReceive;
	}

	public String getAddressReceive() {
		return addressReceive;
	}

	public void setAddressReceive(String addressReceive) {
		this.addressReceive = addressReceive;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistricts() {
		return districts;
	}

	public void setDistricts(String districts) {
		this.districts = districts;
	}

	public String getStreets() {
		return streets;
	}

	public void setStreets(String streets) {
		this.streets = streets;
	}

	public String getWards() {
		return wards;
	}

	public void setWards(String wards) {
		this.wards = wards;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public int getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}

package io.bottest.jpa.entity;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name= "project_milestone_testcase")
public class ProjectMilestoneTestcase {
	
	@EmbeddedId
	private ProjectMilestoneTestcaseIdentity projectMilestoneTestcaseIdentity;
	
	private Date updateDate;
	
	private String description;

	public ProjectMilestoneTestcase() {
		super();
	}

	public ProjectMilestoneTestcaseIdentity getProjectMilestoneTestcaseIdentity() {
		return projectMilestoneTestcaseIdentity;
	}

	public void setProjectMilestoneTestcaseIdentity(ProjectMilestoneTestcaseIdentity projectMilestoneTestcaseIdentity) {
		this.projectMilestoneTestcaseIdentity = projectMilestoneTestcaseIdentity;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}

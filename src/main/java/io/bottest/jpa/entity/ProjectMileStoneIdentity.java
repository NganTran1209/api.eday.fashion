package io.bottest.jpa.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.Null;

@Embeddable
public class ProjectMileStoneIdentity implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Null
	private Long projectId;
	
	@Null
	private String mileStoneName;

	public ProjectMileStoneIdentity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ProjectMileStoneIdentity(@Null Long projectId, @Null String mileStoneName) {
		super();
		this.projectId = projectId;
		this.mileStoneName = mileStoneName;
	}


	public Long getProjectId() {
		return projectId;
	}


	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}


	public String getMileStoneName() {
		return mileStoneName;
	}


	public void setMileStoneName(String mileStoneName) {
		this.mileStoneName = mileStoneName;
	}

}

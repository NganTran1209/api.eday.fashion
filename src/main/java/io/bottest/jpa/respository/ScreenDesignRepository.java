package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.ScreenDesign;

@Repository
public interface ScreenDesignRepository extends JpaRepository<ScreenDesign, Long>, ScreenDesignCustom {
	
	List<ScreenDesign> findScreenDesignByProjectId(Long projectId);
	
	@Query(value = "SELECT * FROM screen_design where project_id=?1 "
			+ "and testsuite_name=?2 and layout_name=?3", nativeQuery = true)
	ScreenDesign findLayoutByName(long projectId, String testsuiteId, String fileNameLayout);
	
	@Query(value = "SELECT DISTINCT create_by FROM screen_design where project_id=? group by create_by ", nativeQuery = true)
	List<String> getNameCreateByLayout(Long projectId);

	List<ScreenDesign> findScreenDesignByProjectIdAndTestsuiteName(long projectId, String testsuiteId);
	
	@Query(value = "SELECT count(*) FROM screen_design where project_id=?1 and testsuite_name=?2", nativeQuery = true)
	int countScreenDesgin(Long projectId, String testsuiteName);
}

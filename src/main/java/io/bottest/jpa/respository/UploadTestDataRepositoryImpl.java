package io.bottest.jpa.respository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import io.bottest.dto.FilterConditionData;
import io.bottest.jpa.entity.UploadTestData;

@Component
public class UploadTestDataRepositoryImpl implements UploadTestDataCustom{
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<UploadTestData> filterUploadTestData(Long projectId, FilterConditionData filter) {
		String condition = "";
		List<String> testsuiteName = filter.getTestsuiteName();
		String createDate = filter.getCreateDate();
		List<String> type = filter.getTypeData();
		String fileName = filter.getFileName();
		
		if(testsuiteName != null) {
			if(testsuiteName.size() > 0) {
				  condition += " and ( testsuite_name='"+testsuiteName.get(0)+"'";
				  for(int i = 1; i < testsuiteName.size(); i++) {
					  condition += " or testsuite_name='"+testsuiteName.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(type != null) {
			if(type.size() > 0) {
				  condition += " and ( type='"+type.get(0)+"'";
				  for(int i = 1; i < type.size(); i++) {
					  condition += " or type='"+type.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(!createDate.isEmpty() && createDate != null) {
			condition += " and create_date like '"+createDate+"%'";
		}
		
		if(!fileName.isEmpty() && fileName != null) {
			condition += " and file_name like '%"+fileName+"%'";
		}
		
		String query = "SELECT * FROM upload_testdata where project_id="+projectId+" "
				+ condition;
		List<UploadTestData> results = entityManager.createNativeQuery(query, UploadTestData.class).getResultList();
		return results;
	}
	
}

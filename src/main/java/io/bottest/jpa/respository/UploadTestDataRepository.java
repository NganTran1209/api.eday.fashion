package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.UploadTestData;

@Repository
public interface UploadTestDataRepository extends JpaRepository<UploadTestData, Long>, UploadTestDataCustom{
	
	@Query(value = "SELECT * FROM upload_testdata where file_name=?1 and project_id=?2 and testsuite_name=?3", nativeQuery = true)
	UploadTestData findByFileName(String fileName, Long projectId, String testsuiteName);
	
	List<UploadTestData> findUploadTestDataByProjectId(Long projectId);

	List<UploadTestData> findUploadTestDataByProjectIdAndTestsuiteName(Long projectId, String testsuiteId);
	
	@Query(value = "SELECT count(*) FROM upload_testdata where project_id=?1 and testsuite_name=?2", nativeQuery = true)
	int countTestData(long projectId, String testsuiteId);
	
	@Query(value = "SELECT * FROM upload_testdata where project_id=?1 and testsuite_name=?2 and file_name=?3", nativeQuery = true)
	UploadTestData getUploadTestData(String projectId, String testsuiteId, String layoutName);
}

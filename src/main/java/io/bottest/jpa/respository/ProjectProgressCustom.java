package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import io.bottest.dto.FilterConditionData;

public interface ProjectProgressCustom {
	List<Map<String, Object>> filterTestcaseAutomation( Long projectId, FilterConditionData filter);

}

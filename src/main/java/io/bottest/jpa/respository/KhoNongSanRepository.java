package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import io.bottest.dto.SellManagerDTO;
import io.bottest.jpa.entity.Work;

@Repository
@Transactional
public interface KhoNongSanRepository extends JpaRepository<Work, Long>{

	@Query(value = "SELECT pr.PRODUCT_ID, pr.PRODUCT_CODE, pr.PRODUCT_NAME, pr.CREATED_DATE, pr.EXPIRE_DATE, pr.QUANTITY_INCLUDED, pr.AMOUNT_UOM_TYPE_ID, pr.BRAND_NAME, pr.GUARANTEE, pr.IS_ACTIVE, pr.INVENTORY_ITEM_TYPE_ID, br.USER_AUTH "+
		    " FROM fibo_cart.product as pr left join fibo_cart.product_facility as prFa on pr.PRODUCT_ID = prFa.PRODUCT_ID " + 
			"	left join fibo_cart.branch_facility as brFa on brFa.FACILITY_ID = prFa.FACILITY_ID " + 
			"    left join fibo_cart.brand_member as brMe on brMe.BRANCH_ID = brFa.BRANCH_ID " + 
			"    left join fibo_cart.brand as br on br.BRAND_ID = brMe.BRAND_ID " + 
			"    where br.USER_AUTH = ? order by pr.PRODUCT_CODE asc", nativeQuery = true)
	List<Map<String, Object>> getListProduct(String user);

	@Query(value="SELECT * FROM fibo_cart.order_interface_item WHERE PRODUCT_ID=?", nativeQuery = true)
	List<Map<String, Object>> getListSell(long productId);

	@Query(value="SELECT pr.product_id, pr.product_name,pr.product_code, pr.is_active, pr.created_by_user_login, pr.brand_name, pr.username, pr.price_detail_text, prW.created_date, prW.total_quantity, prW.inventory, prW.create_by, prW.date_finish, prW.updated_date "+
					"FROM farmate.product pr left join farmate.product_warehouse prW on pr.product_code = prW.product_code where username=? and prW.store_type=?", nativeQuery = true)
	List<Map<String, Object>> getListProductKhoNs(String userName, int i);

	@Query(value="SELECT CASE WHEN UNIT_PRICE IS NULL THEN 0 ELSE UNIT_PRICE  END as UNIT_PRICE  FROM fibo_cart.order_interface_item where PRODUCT_CODE=? order by UNIT_PRICE desc limit 1  ", nativeQuery = true)
	float getPrice(long product_code);
	
}

package io.bottest.jpa.respository;

import java.util.List;

import io.bottest.dto.FilterConditionData;
import io.bottest.jpa.entity.ExecutingJob;

public interface ExecutingJobCustom {
	List<ExecutingJob> filterExecutingJob(Long projectId, FilterConditionData filter);
}

package io.bottest.jpa.respository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import io.bottest.dto.FilterTaskDTO;

@Component
public class TaskRepositoryImpl implements TaskRepositoryCustom {
	
	  @PersistenceContext
	   private EntityManager entityManager;
	  
	  @Override
	  public List<Map<String, Object>> findTask(FilterTaskDTO task , Long projectId){
		  
		  String condition = "";
		  String conditionTestcase = "";
		  List<String> trackerF = task.getTracker();
		  List<String> statusF = task.getStatus();
		  List<String> priorityF = task.getPriority();
		  List<String> categoryF = task.getCategory();
		  List<String> assigneeF = task.getAssignee();
		  String planStartDateF = task.getPlanStartDate();
		  String planEndDateF = task.getPlanEndDate();
		  List<String> testsuiteF = task.getTestsuite();
		  List<String> milestoneF = task.getMilestone();
		  String description = task.getDescription();
		  
		  boolean checkTc = false;
		  if(trackerF.size() == 0) {
			  checkTc = true;
		  }
		  
		  String checkOpenStatus = "";
		  
		  if(statusF != null) {
			  if(statusF.size() > 0) {
				  checkTc = true;
				  if(statusF.indexOf("Open") != -1) {
					  checkOpenStatus = "where (k.status != 'Reject' and k.status != 'Closed')";
				  } else {
					  condition += " and ( t.status='"+statusF.get(0)+"'";
					  conditionTestcase += " and ( p.status_issue='"+statusF.get(0)+"'";
					  for(int i = 1; i < statusF.size(); i++) {
						  condition += " or t.status='"+statusF.get(i)+"'";
						  conditionTestcase += " or p.status_issue='"+statusF.get(i)+"'";
					  }
					  condition += ")";
					  conditionTestcase += ")";
				  }
				  
			  }
		  }
		  
		  
		  if(assigneeF != null) {
			  if(assigneeF.size() > 0) {
				  checkTc = true;
				  condition += " and ( t.assignee='"+assigneeF.get(0)+"'";
				  conditionTestcase += " and ( p.assign_to='"+assigneeF.get(0)+"'";
				  for(int i = 1; i < assigneeF.size(); i++) {
					  condition += " or t.assignee='"+assigneeF.get(i)+"'";
					  conditionTestcase += " or p.assign_to='"+assigneeF.get(i)+"'";
					  
				  }
				  condition += ")";
				  conditionTestcase += ")";
			  }
		  }
		  
		  if(planStartDateF != null) {
			  if(!planStartDateF.isEmpty()) {
				  checkTc = true;
				  condition +=  " and t.plan_start_date like '"+planStartDateF+"%'";
				  conditionTestcase +=  " and p.plan_start_date like '"+planStartDateF+"%'";
			  }
		  }
		  
		  if(planEndDateF != null) {
			  if(!planEndDateF.isEmpty()) {
				  checkTc = true;
				  condition +=  " and t.plan_end_date like '"+planEndDateF+"%'";
				  conditionTestcase +=  " and p.plan_end_date like '"+planEndDateF+"%'";
			  }
		  }
		  
		  if(testsuiteF != null) {
			  if(testsuiteF.size() > 0) {
				  checkTc = true;
				  condition += " and ( t.testsuite='"+testsuiteF.get(0)+"'";
				  conditionTestcase += " and ( p.testsuite='"+testsuiteF.get(0)+"'";
				  for(int i = 1; i < testsuiteF.size(); i++) {
					  condition += " or t.testsuite='"+testsuiteF.get(i)+"'";
					  conditionTestcase += " or p.testsuite='"+testsuiteF.get(i)+"'";
					  
				  }
				  condition += ")";
				  conditionTestcase += ")";
			  }
		  }
		  
		  if(description != null) {
			  if(!description.isEmpty()) {
				  checkTc = true;
				  if(checkOpenStatus != "") {
					  checkOpenStatus += " and (k.subject like '%"+description+"%' or k.description like '%"+description+"%') ";
				  } else {
					  checkOpenStatus += " where (k.subject like '%"+description+"%' or k.description like '%"+description+"%') ";
				  }
				  
			  }
		  }
		  
		  if(trackerF.size() > 0) {
			  if(trackerF.contains("Testcase")) {
				  checkTc = true;
			  } else {
				  checkTc = false;
			  }
			  condition += " and ( t.tracker='"+trackerF.get(0)+"'";
			  for(int i = 1; i < trackerF.size(); i++) {
				  condition += " or t.tracker='"+trackerF.get(i)+"'";
			  }
			  condition += ")";
			  
		  }
		  
		  if(milestoneF != null) {
			  if(milestoneF.size() > 0) {
				  checkTc = false;
				  condition += " and ( t.milestone='"+milestoneF.get(0)+"'";
				  for(int i = 1; i < milestoneF.size(); i++) {
					  condition += " or t.milestone='"+milestoneF.get(i)+"'";
					  
				  }
				  condition += ")";
			  }
		  }
		  

		  if(categoryF != null) {
			  if(categoryF.size() > 0) {
				  checkTc = false;
				  condition += " and ( t.category='"+categoryF.get(0)+"'";
				  for(int i = 1; i < categoryF.size(); i++) {
					  condition += " or t.category='"+categoryF.get(i)+"'";
					  
				  }
				  condition += ")";
				
			  }
		  }
		  

		  if(priorityF != null) {
			  if(priorityF.size() > 0) {
				  checkTc = false;
				  condition += " and ( t.priority='"+priorityF.get(0)+"'";
				  for(int i = 1; i < priorityF.size(); i++) {
					  condition += " or t.priority='"+priorityF.get(i)+"'";
					  
				  }
				  condition += ")";
				
			  }
		  }
		  
		  String query1 = "select k.id, k.actual_end_date, k.actual_start_date, k.assignee, k.description, " 
		  		      +"k.estimated_time,k.plan_end_date, k.plan_start_date, k.priority, k.project_id, " 
		  		      +"k.status, k.subject, k.tracker,k.done,k.testsuite, k.actual_time, k.bug_id, " 
		  		      +"k.category, k.create_by, k.update_date,k.create_date, k.file_name, k.parent_task, k.milestone, u.firstname, u.lastname "
		  		      + "from (select t.id, t.actual_end_date, t.actual_start_date, t.assignee, t.description, t.estimated_time,t.plan_end_date, t.plan_start_date, "
					  +"t.priority, t.project_id, t.status, t.subject, t.tracker,t.done,t.testsuite, t.actual_time"
					  + ", t.bug_id, t.category, t.create_by, t.update_date,t.create_date, t.file_name, t.parent_task, t.milestone "
					  +"from task t where t.project_id="+projectId+" " + condition;
		  String query2 =" ) ";
		  if(checkTc) {
			  query2 = " union all "
					  +"SELECT SUBSTRING_INDEX(p.testcase,'_',-1)*1, p.update_date, p.create_date, "
					  +"p.assign_to, p.description, p.estimated_time, p.plan_end_date, p.plan_start_date, "
					  +"p.type, p.project_id, p.status_issue, p.testcase, 'Testcase', null, p.testsuite, p.actual_time,"
					  + "null,null,null,null,null,null,null,null "
					  +"FROM project_progress p where p.project_id="+projectId+" "+conditionTestcase+" and p.status='add' ) ";
		  }
		  String query3 = " as k left join bottest_user u on k.assignee = u.username "+checkOpenStatus+" order by k.id desc";
		  String query = query1 + query2 + query3;
		  List<Object> results = entityManager.createNativeQuery(query).getResultList();
		  List<Map<String, Object>> listOb = new ArrayList<Map<String,Object>>();
		  for(int i = 0; i < results.size(); i++) {
				Object[] result = (Object[]) results.get(i);
				
				Map<String, Object> ob = new HashMap<String, Object>();
				listOb = setDataToObject(ob, result, listOb);
				
			}
		  return listOb;
	  }
	  
	  public List<Map<String, Object>> setDataToObject(Map<String, Object> ob, Object[] result, List<Map<String, Object>> listOb) {
			
			ob.put("id", result[0]);
			ob.put("actualEndDate", result[1]);
			ob.put("actualStartDate", result[2]);
			ob.put("assignee", result[3]);
			ob.put("description", result[4]);
			ob.put("estimatedTime", result[5]);
			ob.put("planEndDate", result[6]);
			ob.put("planStartDate", result[7]);
			ob.put("priority", result[8]);
			ob.put("projectId", result[9]);
			ob.put("status",result[10] );
			ob.put("subject", result[11]);
			ob.put("tracker", result[12]);
			ob.put("done", result[13]);
			ob.put("testsuite", result[14]);
			ob.put("actualTime", result[15]);
			ob.put("bugId", result[16]);
			ob.put("category", result[17]);
			ob.put("createBy", result[18]);
			ob.put("updateDate", result[19]);
			ob.put("createDate", result[20]);
			ob.put("fileName", result[21]);
			ob.put("parentTask",result[22]);
			ob.put("milestone", result[23]);
			ob.put("firstname", result[24]);
			ob.put("lastname", result[25]);
			listOb.add(ob);
			
			return listOb;
		}
}

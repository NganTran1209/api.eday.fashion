package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;
import io.bottest.dto.FilterSupplieDTO;

public interface SupplieCustom {

	List<Map<String, Object>> filterVatTu(FilterSupplieDTO filterVatTu, String userName);

}

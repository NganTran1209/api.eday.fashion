package io.bottest.jpa.respository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import io.bottest.dto.FilterConditionData;

@Component
public class ProjectProgressRepositoryImpl implements ProjectProgressCustom {
	
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public List<Map<String, Object>> filterTestcaseAutomation(Long projectId, FilterConditionData filter) {
		String condition = "";
		boolean checkStatus = false;
		
		List<String> executeBy = filter.getExecuteBy();
		List<String> testsuiteName = filter.getTestsuiteName();
		String fromDate = filter.getFromDate();
		String toDate = filter.getToDate();
		String statusTc = filter.getStatusTestcase();
		List<String> assignTo = filter.getAssignTo();
		String description = filter.getFileName();
		List<String> statusIssue = filter.getStatusIssue();
		String planStartDate = filter.getPlanStartDate();
		String planEndDate = filter.getPlanEndDate();
		List<String> categoryIssue = filter.getCategoryIssue();
		List<String> milestoneTc = filter.getMilestone();
		List<String> newExecuteBy = executeBy;
		String newFromDate = fromDate;
		String newToDate = toDate;
		
		if(statusTc != null) {
			if(!statusTc.isEmpty()) {
				if(statusTc.equalsIgnoreCase("NOT-RUN")) {
					newExecuteBy = new ArrayList<String>();
					newFromDate = "";
					newToDate = "";
					checkStatus = true;
				} else {
					condition += " and r.test_case_result='"+statusTc+"'";
				}
				
			}
		}
		
		if(newFromDate != null && newToDate != null) {
			if(!newFromDate.isEmpty() && !newToDate.isEmpty() ) {
				condition += " and r.execute_date  BETWEEN CAST('"+newFromDate+"' AS DATE) AND CAST('"+newToDate+"' AS DATE)";
			} else if(!newFromDate.isEmpty() && newFromDate != null) {
				condition += " and r.execute_date like '"+newFromDate+"%'";
			} else if(!newToDate.isEmpty() && newToDate != null) {
				condition += " and r.execute_date like '"+newToDate+"%'";
			}
		}
		
		if(testsuiteName != null) {
			if(testsuiteName.size() > 0) {
				condition += " and ( p.testsuite='"+testsuiteName.get(0)+"'";
				for(int i = 1; i < testsuiteName.size(); i++) {
					  condition += " or p.testsuite='"+testsuiteName.get(i)+"'";
					  
				  }
				condition += ")";
			}
			
		}
		
		if(milestoneTc != null) {
			if(milestoneTc.size() > 0) {
				condition += " and ( p.milestone='"+milestoneTc.get(0)+"'";
				for(int i = 1; i < milestoneTc.size(); i++) {
					  condition += " or p.milestone='"+milestoneTc.get(i)+"'";
					  
				  }
				condition += ")";
			}
			
		}
		
		if(newExecuteBy != null) {
			if(newExecuteBy.size() > 0) {
				  condition += " and ( r.running_user='"+newExecuteBy.get(0)+"'";
				  for(int i = 1; i < newExecuteBy.size(); i++) {
					  condition += " or r.running_user='"+newExecuteBy.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(assignTo != null) {
			if(assignTo.size() > 0) {
				  condition += " and ( p.assign_to='"+assignTo.get(0)+"'";
				  for(int i = 1; i < assignTo.size(); i++) {
					  condition += " or p.assign_to='"+assignTo.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(statusIssue != null) {
			if(statusIssue.size() > 0) {
				  condition += " and ( p.status_issue='"+statusIssue.get(0)+"'";
				  for(int i = 1; i < statusIssue.size(); i++) {
					  condition += " or p.status_issue='"+statusIssue.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(categoryIssue != null) {
			if(categoryIssue.size() > 0) {
				  condition += " and ( p.category='"+categoryIssue.get(0)+"'";
				  for(int i = 1; i < categoryIssue.size(); i++) {
					  condition += " or p.category='"+categoryIssue.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(planStartDate != null) {
			if(!planStartDate.isEmpty()) {
				condition += " and p.plan_start_date like '"+planStartDate+"%'";
			}
		}
		
		if(planEndDate != null) {
			if(!planEndDate.isEmpty()) {
				condition += " and p.plan_end_date like '"+planEndDate+"%'";
			}
		}
		
		if(description != null) {
			if(!description.isEmpty()) {
				condition += " and (p.testcase like '%"+description+"%' or p.description like '%"+description+"%')";
			}
		}
//		if(condition == "") {
//			return null;
//		}
		
		if(filter.getType() != null) {
			condition += "and p.type='"+filter.getType()+"'";
		}
		
		String query ="SELECT p.project_id as project_idTc, p.testcase,p.testsuite,p.assign_to,p.create_date,"
			+ "p.description,p.status as statusTc,p.testscript,p.update_date,p.update_user,"
			+ "p.expected_output as expected_outputTc,p.pre_condition,p.testcase_procedure , p.status_issue, p.plan_start_date,"
			+ "p.plan_end_date, p.estimated_time, p.actual_time,"
			+ " r.id, r.execute_date,"
			+ " r.project_id, r.result_name, r.running_user, r.test_case_name, r.test_case_result, r.test_result,"
			+ " r.test_suite, r.actual_output,r.browser, r.device_name, r.expected_output, r.location, r.milestone,"
			+ " r.mobile_mode, r.os, r.running_description, r.screen_resolution, r.type, r.files, r.status , p.type as typeTc, p.category,p.order_id,u.firstname, u.lastname  "
			+ "FROM project_progress as p left join bottest_user as u on p.assign_to = u.email left join  ( select * from result_info "
			+ "where id in (select max(id) from result_info as r1 group by"
			+ " r1.project_id, r1.test_suite, r1.test_case_name)) as r on p.project_id=r.project_id "
			+ "and p.testsuite=r.test_suite and p.testcase=r.test_case_name where p.project_id="+projectId+" "
			+ " and p.status='add' "
			+ condition
			+ " order by order_id";
		
		List<Object> results = entityManager.createNativeQuery(query).getResultList();
		List<Map<String, Object>> listOb = new ArrayList<Map<String,Object>>();
		for(int i = 0; i < results.size(); i++) {
			Object[] result = (Object[]) results.get(i);
			
			Map<String, Object> ob = new HashMap<String, Object>();
			if(checkStatus) {
				if(result[23] == null) {
					listOb = setDataToObject(ob, result, listOb);
				}
			} else {
				listOb = setDataToObject(ob, result, listOb);
			}
			
		}
		return listOb;
		
	}
	
	public List<Map<String, Object>> setDataToObject(Map<String, Object> ob, Object[] result, List<Map<String, Object>> listOb) {
		
		ob.put("projectIdTc", result[0]);
		ob.put("testcase", result[1]);
		ob.put("testsuite", result[2]);
		ob.put("assignTo", result[3]);
		ob.put("createDate", result[4]);
		ob.put("description", result[5]);
		ob.put("statusTc", result[6]);
		ob.put("testscript", result[7]);
		ob.put("updateDate", result[8]);
		ob.put("updateUser", result[9]);
		ob.put("expectedOutputTc",result[10] );
		ob.put("preCondition", result[11]);
		ob.put("testcaseProcedure", result[12]);
		ob.put("statusIssue", result[13]);
		ob.put("planStartDate", result[14]);
		ob.put("planEndDate", result[15]);
		ob.put("estimatedTime", result[16]);
		ob.put("actualTime", result[17]);
		ob.put("id", result[18]);
		ob.put("executeDate", result[19]);
		ob.put("projectId", result[20]);
		ob.put("resultName", result[21]);
		ob.put("runningUser",result[22]);
		ob.put("testCaseName", result[23]);
		ob.put("testCaseResult",result[24] );
		ob.put("testResult", result[25]);
		ob.put("testSuite", result[26]);
		ob.put("actualOutput",result[27] );
		ob.put("browser", result[28]);
		ob.put("deviceName", result[29]);
		ob.put("expectedOutput", result[30]);
		ob.put("location", result[31]);
		ob.put("milestone", result[32]);
		ob.put("mobileMode", result[33]);
		ob.put("os",  result[34]);
		ob.put("runningDescription",result[35]);
		ob.put("screenResolution", result[36]);
		ob.put("type", result[37]);
		ob.put("files", result[38]);
		ob.put("status", result[39]);
		ob.put("typeTc", result[40]);
		ob.put("category", result[41]);
		ob.put("orderId", result[42]);
		ob.put("firstname", result[43]);
		ob.put("lastname", result[44]);
		listOb.add(ob);
		
		return listOb;
	}
	

}

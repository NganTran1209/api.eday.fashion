package io.bottest.jpa.respository;

import java.math.BigInteger;
import java.util.Date;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import io.bottest.jpa.entity.Project;

@Repository
@Transactional
public interface OrderRepository extends JpaRepository<Project, Long>{

	@Modifying
	@Query(value="INSERT INTO fibo_cart.order_interface_header (BRANCH_ID,BRANCH_NAME,BRAND_ID,ORDER_TYPE_ID,ORDER_DATE,STATUS_ID,CREATED_BY) values (?,?,?,'SALE_PRODUCT',?,'SALE_WAITING', ?)", nativeQuery = true)
	void createOrderInterfaceHeader(String branch_id, String branch_name, BigInteger brandId, Date date, String username);

	@Modifying
	@Query(value="INSERT INTO fibo_cart.order_interface_item (ORDER_ID,ORDER_ITEM_TYPE_ID,PRODUCT_CODE,CATALOG_NAME,SELECTED_AMOUNT,UNIT_PRICE, CREATED_STAMP,ORDER_ITEM_SEQ_ID) values (?,'PRODUCT_SALE_ITEM',?,?,?,?,?,1)", nativeQuery = true)
	void createOrderInterfaceItem(String orderId, String productCode, String name, Float amount,Float price, Date date);

	@Query(value="SELECT * FROM fibo_cart.order_interface_item where ORDER_ID=?", nativeQuery = true)
	Map<String, Object> findProductSell(String orderId);

	@Query(value="SELECT * FROM fibo_cart.order_interface_header where BRANCH_ID=? and BRANCH_NAME=? and BRAND_ID=? and ORDER_TYPE_ID='SALE_PRODUCT' and STATUS_ID='SALE_WAITING' "
			+ "and CREATED_BY=? order by ORDER_ID desc limit 1", nativeQuery = true)
	Map<String, Object> findOrderInterfaceHeader(String branch_id, String branch_name, BigInteger brandId, String username);

	@Query(value="SELECT * FROM fibo_cart.order_interface_item where ORDER_ID=? and ORDER_ITEM_TYPE_ID='PRODUCT_SALE_ITEM' and PRODUCT_CODE=? and CATALOG_NAME=? and SELECTED_AMOUNT=? "
			+ "and UNIT_PRICE=?  order by ORDER_ID desc limit 1", nativeQuery = true)
	Map<String, Object> findOrderInterfaceItem(String orderId, String productCode, String name, Float amount,Float price);

	@Query(value="SELECT total_quantity FROM farmate.product_warehouse where product_code=?", nativeQuery = true)
	Float findTotalQuantity(String productCode);

}

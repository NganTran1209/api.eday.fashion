package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Work;

@Repository
@Transactional
public interface WorkRepository extends JpaRepository<Work, Long>{
	
	@Query(value = "SELECT * FROM work WHERE worker_id = :worker_id AND status = :status", nativeQuery = true)
	List<Work> findAllWorkByWorkerIdAndStatus(@Param("worker_id") Long workerId, @Param("status") String status);
	
}

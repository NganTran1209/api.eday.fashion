package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Material;

@Repository
@Transactional
public interface MaterialRepository extends JpaRepository<Material, Long>{

	@Query(value = "SELECT m.id, m.image, m.title, m.supplier, m.price, m.amount, c.name, m.input_date "
			+ "FROM material m "
			+ "INNER JOIN category c "
			+ "ON m.category_id = c.id "
			+ "WHERE m.user_id = ? ", nativeQuery = true)
	public List<Map<String, Object>> findAllMaterialById(Long userId);
}

package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.Component;

@Repository
public interface ComponentRespository extends JpaRepository<Component, Integer> {

	List<Component> findByGroupCode(String groupCode);
	
}

package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Post;

@Transactional
@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

	@Query(value = "SELECT * FROM post p WHERE p.user_id = :userId", nativeQuery = true)
	public List<Post> findAllPostOfUserByUserId(Long userId);
}

package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.Software;

@Repository
public interface SoftwareRepository extends JpaRepository<Software, Long> {
	@Query(value = "SELECT * FROM software where file_name=?1", nativeQuery = true)
	Software findByFileName(String fileName);
	
	@Query(value = "SELECT TB1.* FROM software TB1,(SELECT type, max(version) AS version FROM software"
			+ " GROUP BY type) TB2 WHERE TB1.type = TB2.type AND TB1.version = TB2.version", nativeQuery = true)
	List<Software> getVersionNew();
	
	
	@Query(value = "SELECT * FROM software order by id desc", nativeQuery = true)
	List<Software> getAll();
}
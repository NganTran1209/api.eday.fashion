package io.bottest.jpa.respository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.jpa.entity.Product;

@Component
public class ProductRepositoryImpl implements ProductRepositoryCustom{

	@PersistenceContext
	   private EntityManager entityManager;
	@Override
	public List<Map<String, Object>> filterProduct(FilterProductDTO filterProduct) {
		String condition = "";
		  String name = filterProduct.getName();
		  int catId = filterProduct.getCatId();
		  List<String> size = filterProduct.getSize();
		 
		  boolean checkTc = false;
		 
		  if(name != null) {
			  if(!name.isEmpty()) {
				  checkTc = true;
				  condition +=  "where p.name like '%"+name+"%'";
			  }
		  }
		  
		  if(catId > 0) {
			  checkTc = true;
			  condition +=  " and p.cat_id = "+catId+"";
		  }
		  
		  if(size != null) {
			  if(size.size() > 0) {
				  checkTc = true;
				  condition += " and ( p.size='"+size.get(0)+"'";
				  for(int i = 1; i < size.size(); i++) {
					  condition += " or p.size='"+size.get(i)+"'";
				  }
				  condition += ")";
			  }
		  }
		  
		  
		  String query = "SELECT p.id, p.name, p.amount, p.image, p.media, p.price_buy, p.price_sell, p.size, c.name as catName  "
				  			+"FROM product as p left join category as c  on p.cat_id = c.id ";
		  if(checkTc) {
			   query += condition;
		  }
		List<Object> results = entityManager.createNativeQuery(query).getResultList();
		  List<Map<String, Object>> listOb = new ArrayList<Map<String,Object>>();
		  for(int i = 0; i < results.size(); i++) {
				Object[] result = (Object[]) results.get(i);
				
				Map<String, Object> ob = new HashMap<String, Object>();
				listOb = setDataToObject(ob, result, listOb);
				
			}
		  return listOb;
	}
	private List<Map<String, Object>> setDataToObject(Map<String, Object> ob, Object[] result,List<Map<String, Object>> listOb) {
		ob.put("id", result[0]);
		ob.put("name", result[1]);
		ob.put("amount", result[2]);
		ob.put("image", result[3]);
		ob.put("media", result[4]);
		ob.put("price_buy", result[5]);
		ob.put("price_sell", result[6]);
		ob.put("size", result[7]);
		ob.put("catName", result[8]);
		listOb.add(ob);
		
		return listOb;
	}

	
}

package io.bottest.jpa.respository;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.Task;

@Repository
public interface TaskRepository  extends JpaRepository<Task,Long>, TaskRepositoryCustom{
	
	@Query(value = "SELECT * FROM task where file_name=?1", nativeQuery = true)
	Task findByFileName(String fileName);
	
	@Query(value = "SELECT t.actual_end_date as actualEndDate , t.actual_start_date as actualStartDate, t.actual_time as actualTime, t.assignee, t.bug_id as bugId, "
					+ " t.category, t.create_by as createBy, t.create_date as createDate , t.description, t.done, t.estimated_time as estimatedTime, t.file_name as fileName, "
					+ " t.id, t.milestone, t.notes, t.parent_task as parentTask, t.plan_end_date as planEndDate , t.plan_start_date as planStartDate, t.priority, t.project_id as projectId, " 
					+ "t.status, t.subject, t.testsuite, t.tracker, t.update_date as updateDate FROM task t "
					+ " where project_id=?1 order by t.id desc  ", nativeQuery = true)
	List<Map<String, Object>> findByProjectId(Long projectId);
	
	@Query(value = "SELECT *  FROM task ORDER BY create_date DESC LIMIT 1;", nativeQuery = true)
	Long findTaskNewId(Long projectId);
	
	List<Task> findAllByProjectId(Long projectId);
	
	List<Task> findTaskByProjectIdAndParentTask(Long projectId, Long parentTask);
	
	List<Task> findTaskByProjectIdAndId(Long projectId, Long id);
	
	@Query(value = "SELECT * FROM task where project_id=?1 and assignee=?2 order by id desc  ", nativeQuery = true)
	List<Task> findTaskByProjectId(Long projectId, String assignee);
	
	@Query(value = "SELECT * FROM task where project_id=?1 and id > ?2 and assignee=?3 order by id asc limit 1", nativeQuery = true)
	Task previousIssue(Long projectId, Long id, String assignee);
	
	@Query(value = "SELECT * FROM task where project_id=?1 and id < ?2 and assignee=?3 order by id desc limit 1", nativeQuery = true)
	Task nextIssue(Long projectId, Long id, String assignee);
	
	Task findTaskByProjectIdAndBugId(Long projectId, Long bugId);
	
	@Query(value = "select tt.tracker, tt.status, tt.count,p.color_code as colorCode from(SELECT t.tracker, t.status, count(t.status) as count " + 
			" FROM task t  " + 
			" where t.project_id =?1" + 
			" group by t.tracker, t.status order by t.tracker, t.status) as tt " + 
			" join project_component p " + 
			" on p.item_name = tt.status where p.project_id=?1 and p.group_code='Status' order by tt.tracker asc ", nativeQuery = true)
	List<Map<String, Object>> graphicDataIssue(Long projectId);
	
	@Query(value = "SELECT * FROM task where bug_id=?1 order by id desc  ", nativeQuery = true)
	Task findByBugId(Long id);
	
	@Query(value = "SELECT t.actual_end_date as actualEndDate , t.actual_start_date as actualStartDate, t.actual_time as actualTime, t.assignee, t.bug_id as bugId, "
			+ " t.category, t.create_by as createBy, t.create_date as createDate , t.description, t.done, t.estimated_time as estimatedTime, t.file_name as fileName, "
			+ " t.id, t.milestone, t.notes, t.parent_task as parentTask, t.plan_end_date as planEndDate , t.plan_start_date as planStartDate, t.priority, t.project_id as projectId, "
			+ "t.status, t.subject, t.testsuite, t.tracker, t.update_date as updateDate, b.firstname, b.lastname, t.plan_tool_material as planToolMaterial FROM task t left join "
			+ " bottest_user b on t.assignee = b.username where t.id = ?1 ", nativeQuery = true)
	Map<String , Object> findIssueById(Long id);
	
	@Query(value = "SELECT tracker,status, count(status) as count, ?4 as colorCode FROM task where project_id=?1 and tracker=?2 and status=?3 and milestone=?5", nativeQuery = true)
	Map<String, Object> graphicDataIssueMilestone(long projectId, String itemName, String itemName2, String colorCode,
			String nameMilestone);

	@Query(value = "select tt.tracker, tt.status, tt.count,p.color_code as colorCode from(SELECT t.tracker, t.status, count(t.status) as count " + 
			" FROM task t  " + 
			" where t.project_id =?1 and t.milestone=?2" + 
			" group by t.tracker, t.status order by t.tracker, t.status) as tt " + 
			" join project_component p " + 
			" on p.item_name = tt.status where p.project_id=?1 and p.group_code='Status' order by tt.tracker asc ", nativeQuery = true)
	List<Map<String, Object>> graphicDataIssueMilestone(long projectId, String nameMilestone);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE task SET parent_task = ?3 WHERE project_id=?1 and parent_task=?2 ", nativeQuery=true)
	void updateTaskPrivate(Long projectId, Long parentId, Long parentNew);
}
package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.UserProject;

@Repository
public interface UserProjectRepository extends JpaRepository<UserProject, Long> {

	List<UserProject> findByUsername(String username);

	List<UserProject> findByProjectIdAndUsername(Long id, String username);
	
	List<UserProject> findByProjectIdOrderByProjectRole(Long projectId);
		
	@Query(value="SELECT username FROM user_project where project_id= ?1 and project_role = 'PROJECT_ADMIN' or project_role = 'Manager' ", nativeQuery = true)
	List<String> findByProjectIdAndByProjectRole(Long projectId);
	
	@Query(value="SELECT usPro.project_name as projectName, usPro.id, usPro.project_role as projectRole,  "
			+ "usPro.username, boUs.firstname, boUs.lastname FROM user_project  usPro left join bottest_user boUs "
			+ "on usPro.username = boUs.username where usPro.project_id =?1 order by project_role", nativeQuery = true)
	List<Map<String, Object>> getUserProjectByProjectId (Long projectId);
	
	void deleteByUsername(String username);
	
	// Get project by flag
    @Query(value = "SELECT user_project.username "
    		+ "FROM project INNER JOIN user_project "
    		+ "ON project.id = user_project.project_id "
    		+ "WHERE project.deleted_flag='0' AND user_project.project_id=?", nativeQuery = true)
	public List<Map<String, Object>> getGroupUserByProjectId(Integer project_id);
    
    UserProject findUserProjectByProjectIdAndProjectNameAndUsername(Long id, String projectName, String username);
    
    UserProject findUserProjectByProjectIdAndUsername(Long id, String username);
}

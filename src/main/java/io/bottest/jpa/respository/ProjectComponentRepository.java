package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import io.bottest.jpa.entity.ProjectComponent;

public interface ProjectComponentRepository extends JpaRepository<ProjectComponent, Long> {
	@Query(value= "SELECT * FROM  project_component where project_id =?1 and status = 1 and item_name != 'Testcase'", nativeQuery = true)
	List<ProjectComponent> getProjectComponentByProjectIdStatus(Long projectId);
	
	@Query(value= "SELECT * FROM  project_component where project_id =?1", nativeQuery = true)
	List<ProjectComponent> getProjectComponentByProjectId(Long projectId);
	
	ProjectComponent findByProjectIdAndItemValue(String projectId, String trackerValue);
	
	@Query(value = "SELECT * FROM project_component where project_id=? and group_code='Tracker' and status=1", nativeQuery = true)
	List<ProjectComponent> getAllTracker(Long projectId);
	
	@Query(value = "SELECT * FROM project_component where project_id=? and group_code='Status' and status=1", nativeQuery = true)
	List<ProjectComponent> getAllStatus(Long projectId);
}

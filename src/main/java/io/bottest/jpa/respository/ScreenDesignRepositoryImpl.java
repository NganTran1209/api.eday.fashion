package io.bottest.jpa.respository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import io.bottest.dto.FilterConditionData;
import io.bottest.jpa.entity.ScreenDesign;

@Component
public class ScreenDesignRepositoryImpl implements ScreenDesignCustom {
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<ScreenDesign> filterScreenDesign(Long projectId, FilterConditionData filter){
		String condition = "";
		List<String> testsuiteName = filter.getTestsuiteName();
		List<String> createBy = filter.getCreateBy();
		String createDate = filter.getCreateDate();
		String fileName =  filter.getFileName();
		
		if(testsuiteName != null) {
			if(testsuiteName.size() > 0) {
				  condition += " and ( testsuite_name='"+testsuiteName.get(0)+"'";
				  for(int i = 1; i < testsuiteName.size(); i++) {
					  condition += " or testsuite_name='"+testsuiteName.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(createBy != null) {
			if(createBy.size() > 0) {
				  condition += " and ( create_by='"+createBy.get(0)+"'";
				  for(int i = 1; i < createBy.size(); i++) {
					  condition += " or create_by='"+createBy.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(!createDate.isEmpty() && createDate != null) {
			condition += " and create_date like '"+createDate+"%'";
		}
		
		if(fileName != null) {
			if(!fileName.isEmpty()) {
				condition += " and layout_name like '%"+fileName+"%'";
			}
		}
		String query = "SELECT * FROM screen_design where project_id="+projectId+" "
				+ condition;
		List<ScreenDesign> results = entityManager.createNativeQuery(query, ScreenDesign.class).getResultList();
		
		return results;
	}
}

package io.bottest.jpa.respository;

 
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.ResultInfo;

@Repository
public interface ResultInfoRepository extends JpaRepository<ResultInfo, Long>{
	
	@Query(value = "SELECT * FROM result_info where project_id=?1 and test_suite=?2 and test_case_name=?3 order by id desc limit 1",nativeQuery = true)
	ResultInfo findByProjectIdAndTestSuiteAndTestCaseName(int projectId, String testSuite, String testcaseName);
	
	@Query(value = "select count(DISTINCT test_case_name) FROM result_info where project_id=? and test_suite=?",nativeQuery = true)
	int findTestcaseTested(int projectId, String testsuite);
	
	@Query(value = "select count(DISTINCT test_case_name) FROM result_info where project_id=? ",nativeQuery = true)
	int getTestcaseTested(int projectId);
	
	@Query(value = "SELECT count(id) FROM result_info where project_id=?1 and test_suite=?2 and id in (select max(id) FROM result_info where project_id=?1 and test_suite=?2 group by test_case_name ) and test_case_result='PASS'",nativeQuery = true)
	int findTestcasePassed(int projectId, String testsuite);

	@Query(value = "SELECT count(DISTINCT result_name) FROM result_info where project_id=?1 and test_suite=?2 and type='automation'", nativeQuery = true)
	int countResults(int projectId, String testsuite);
	
	@Query(value = "SELECT execute_date FROM result_info where project_id=? group by execute_date order by execute_date", nativeQuery = true)
	List<Date> findAllDate(int projectId);
	
	@Query(value = "select pr.project_id , pr.testsuite, IFNULL(rs.test_case_result, 'NOT-RUN' ) as status, count(1) as numcases from project_progress as pr  " + 
			"left join " + 
			" ( " + 
			"	select  project_id, test_suite, test_case_name, execute_date , id ,test_case_result " + 
			"		from  result_info " + 
			"		where id in (select max(rss.id) from result_info  rss where  rss.execute_date <=?2 " + 
			"        group by rss.project_id, rss.test_suite, rss.test_case_name) and status='add' " + 
			"	) as rs " + 
			"    on rs.project_id = pr.project_id and pr.testsuite = rs.test_suite and pr.testcase = rs.test_case_name " + 
			"    where create_date <=?2 and pr.project_id=?1 and pr.status='add'" + 
			"    group by  pr.project_id , pr.testsuite, rs.test_case_result order by pr.project_id , pr.testsuite, status ", nativeQuery = true)
	List<Map<String,Object>> findTotalGraphic(int projectId, Date executeDate);

	List<ResultInfo> findByProjectIdAndTestSuiteAndResultName(int projectNumber, String testsuiteId, String resultname);
	
	List<ResultInfo> findByProjectIdAndTestSuite(int projectId, String testsuiteId);
	
	@Query(value = "SELECT * FROM result_info where project_id=?1 and test_suite=?2 and test_case_name=?3", nativeQuery = true)
	List<ResultInfo> getAllResultInProjectIdAndTestSuiteAndTestcaseName(int projectId, String testsuiteId, String testcaseName);
	
	@Query(value = "SELECT * FROM result_info where id in (select max(id) from result_info where project_id=?1 and test_suite=?2 and test_case_name=?3)", nativeQuery = true)
	ResultInfo findByProjectIdAndTestsuiteAndTestcaseName(int projectId, String testsuite, String testcaseName);
	
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO  result_info(result_name, test_suite, test_result, running_user, execute_date, test_case_name, "
			+ " test_case_result, project_id, type, running_description, status,browser, device_name,"
			+ "expected_output, location, milestone, mobile_mode, os, screen_resolution) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?);", nativeQuery = true)
	void insertResultInfo(String resultName, String testSuite, String testResult, String runningUser, 
			Date executeDate, String testCaseName, String testCaseResult, int projectId, String automation, String runningDescription, String status,
			String browser, String deviceName, String expectedOutput, String location, String milestone, String mobileMode, String os, String screenResolution);
	
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO  result_info(result_name, test_suite, test_result, running_user, execute_date, "
			+ " project_id, type, milestone, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?);", nativeQuery = true)
	void insertResultInfo2(String resultName, String testSuite, String testResult, String runningUser, 
			Date executeDate, int projectId, String automation, String runningName, String status);
	
	@Query(value = "SELECT DISTINCT running_user FROM result_info where project_id=?1 "  +
			" and id in (select max(id) FROM result_info where project_id=?1 group by test_case_name ) " + 
			"and status='add' and type='automation'", nativeQuery = true)
		List<String> getNameExecuteBy(int projectId);
	
	@Query(value = "SELECT DISTINCT running_user FROM result_info where project_id=?1 "  +
			" and id in (select max(id) FROM result_info where project_id=?1 group by test_case_name ) " + 
			"and status='add' and type='manual'", nativeQuery = true)
		List<String> getNameExecuteByManual(int projectId);
	
	ResultInfo findResultInfoById(Long id);
	
	@Query(value = "SELECT * FROM result_info where project_id=?1 and test_suite=?2 and test_case_name=?3 and id < ?4 order by id desc limit 1", nativeQuery = true)
	ResultInfo previousManualResultInfo(Long projectId, String testsuite, String testcaseName, Long id);
	
	@Query(value = "SELECT * FROM result_info where project_id=?1 and test_suite=?2 and test_case_name=?3 and id > ?4 order by id asc limit 1", nativeQuery = true)
	ResultInfo nextManualResultInfo(Long projectId, String testsuite, String testcaseName, Long id);
	@Query(value = "SELECT * FROM result_info where project_id=?1 and test_suite=?2 and test_case_name=?3 order by id desc limit 1 offset 1", nativeQuery = true)
	ResultInfo findResultInfoByProjectIdAndTest(String projectId, String testsuiteId, String testCaseName);
}

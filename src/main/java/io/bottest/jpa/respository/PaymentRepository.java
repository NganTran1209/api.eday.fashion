package io.bottest.jpa.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Payment;

@Repository
@Transactional
public interface PaymentRepository extends JpaRepository<Payment, Long>{

	@Query(value= "SELECT * FROM  payment where order_id =?", nativeQuery = true)
	Payment getPaymentByOrderId(Long id);

}

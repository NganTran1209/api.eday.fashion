package io.bottest.jpa.respository;

import java.util.List;

import io.bottest.dto.FilterConditionData;
import io.bottest.jpa.entity.UploadTestData;

public interface UploadTestDataCustom {
	List<UploadTestData> filterUploadTestData(Long projectId, FilterConditionData filter);
}

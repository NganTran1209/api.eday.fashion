package io.bottest.jpa.respository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, String>{

	User findByUsernameIgnoreCase(String username);
	
	@Query(value= "SELECT * FROM  user where email =?1 AND enable=1", nativeQuery = true)
	User getUserByUsername(String loginUserName);

	@Query(value= "SELECT * FROM  user ", nativeQuery = true)
	List<User> findByAllUsers();

	@Query(value= "SELECT * FROM  user where username =?1", nativeQuery = true)
	User getBottestUserByUsername(String username);

	@Query(value= "SELECT * FROM  user where email =?1", nativeQuery = true)
	User getUserByEmail(String email);
	
	@Query(value= "SELECT * FROM  user where id =?1", nativeQuery = true)
	User findById(Long id);
	
	@Query(value= "SELECT * FROM  user where (role='ROLE_STAFF' or role='ROLE_MANAGER') and enable=1", nativeQuery = true)
	List<User> getListStaff();

	@Query(value= "SELECT * FROM  user where role='ROLE_CUSTOMER'  and enable=1", nativeQuery = true)
	User getListCustomer();
	
	@Query(value= "SELECT * FROM  user where username =?1 and role=?2 and enable=1", nativeQuery = true)
	User getUserAuthen(String username, Object object);

	@Query(value= "SELECT * FROM  user where username =?1 and password=?2 and enable=1", nativeQuery = true)
	User findByUserNameAndPassword(String username, String password);

	@Query(value= "SELECT * FROM  user where role='ROLE_CUSTOMER' and enable=1", nativeQuery = true)
	List<User> findAllCustomer();

	@Query(value= "SELECT * FROM  user where role='ROLE_CUSTOMER'  and enable=1 and id =?1", nativeQuery = true)
	User getUser(Long userId);

}

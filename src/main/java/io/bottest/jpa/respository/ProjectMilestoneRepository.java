package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.ProjectMileStoneIdentity;
import io.bottest.jpa.entity.ProjectMilestone;

@Repository
public interface ProjectMilestoneRepository extends JpaRepository<ProjectMilestone, ProjectMileStoneIdentity>{
	
	@Query(value = "select * from project_milestone where project_id=?1 and mile_stone_name=?2", nativeQuery = true)
	ProjectMilestone findByProjectMileStoneIdentity_ProjectIdAndProjectMileStoneIdentity_MileStoneName(Long projectId, String milestoneName);
	
	@Query(value = "select p.mile_stone_name, p.project_id, p.description, IFNULL(rs.test_case_name, 'NOT-RUN') as testcaseName, IFNULL(rs.test_case_result, 'NOT-RUN' ) as status, count(1) as numberText from project_milestone p left join ( " + 
			"select milestone,project_id, test_case_name, test_case_result from result_info where id in (select max(id)FROM result_info where project_id=?1 group by test_case_name, project_id, milestone) " + 
			") as rs on p.project_id = rs.project_id and p.mile_stone_name = rs.milestone " + 
			"where p.project_id=?1 and p.status='add' group by p.mile_stone_name, rs.test_case_result order by p.mile_stone_name, p.end_date", nativeQuery = true)
	List<Map<String, Object>> findTotalGraphicForMistone(Long projectId);
	
	@Query(value = "select * from project_milestone where project_id=?1 and status='add'", nativeQuery = true)
	List<ProjectMilestone> getAllMilestone(Long projectId);
}

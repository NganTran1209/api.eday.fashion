package io.bottest.jpa.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.UserActive;

@Repository
public interface UserActiveRepository extends JpaRepository<UserActive, String>{

}
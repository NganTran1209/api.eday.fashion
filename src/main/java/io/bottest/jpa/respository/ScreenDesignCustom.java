package io.bottest.jpa.respository;

import java.util.List;

import io.bottest.dto.FilterConditionData;
import io.bottest.jpa.entity.ScreenDesign;

public interface ScreenDesignCustom {
	List<ScreenDesign> filterScreenDesign(Long projectId, FilterConditionData filter);
}

package io.bottest.jpa.respository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.InterfaceServer;

@Repository
public interface InterfaceServerRepository extends JpaRepository<InterfaceServer, String> {

	@Query(value="SELECT DISTINCT ins.* "
			+ "FROM interface_server ins  "
			+ "WHERE (ins.job_name, ins.browser, ins.os, ins.node_label) not in (SELECT DISTINCT exc.job_name, exc.browser, exc.runningos, exc.node_label FROM executing_job exc WHERE exc.status in ('RUNNING', 'WAITING'))"
			+ " AND ins.os=? AND ins.browser=? AND ins.scm_type=?", 
			nativeQuery = true)
	List<InterfaceServer> findByOsAndBrowserFree(String runningOS, String browser, String scm_type);

	@Query(value="SELECT ins.* "
			+ "FROM interface_server ins INNER JOIN  "
			+ " (SELECT job_name, runningos, browser, node_label, count(1) as count FROM executing_job WHERE status in (\"WAITING\", \"RUNNING\")  GROUP BY job_name, runningos, browser, node_label "
			+ " ORDER BY count) AS excj ON ins.job_name = excj.job_name AND ins.browser = excj.browser AND ins.os = excj.runningos AND ins.node_label = excj.node_label"
			+ " WHERE ins.os=? AND ins.browser=? AND ins.scm_type=?", 
			nativeQuery = true)
	List<InterfaceServer> findByOsAndBrowserRunning(String runningOS, String browser, String scm_type);

	List<InterfaceServer> findByBrowser(String browser);
	
	@Query(value="SELECT * FROM interface_server WHERE os=? AND browser = ? AND scm_type=? AND node_label=?", nativeQuery=true)
	InterfaceServer findByOsAndBrowserAndScmTypeAndNodeLabel(String runningOS, String browser, String scmType, String nodeLabel);

	@Query(value="INSERT INTO interface_server(domain, job_name, os, runningos, token, username, location, scm_type, node_label) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)", nativeQuery=true)
	void insertInterface(String domain, String job_name, String os, String runningos, String token, String username,
			String location, String scm_type, String nodeLabel);

	@Query(value="SELECT * FROM interface_server WHERE os=? AND node_label=?", nativeQuery=true)
	List<InterfaceServer> checkInterfaceOsLabel(String nodeLabel, String string);

	@Query(value="SELECT * FROM interface_server WHERE os=? AND scm_type=? AND node_label=?", nativeQuery=true)
	InterfaceServer findByOsAndScmTypeAndNodeLabel(String os, String projectType, String username);

	@Modifying
	@Transactional
	@Query(value="INSERT INTO `interface_server`\n" + 
			"(`domain`, `job_name`, `os`, `runningos`, `token`, `username`, `location`, `scm_type`, `node_label`, `isLocal`)\n" + 
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", nativeQuery=true)
	void createInterfaceServer(String domain, String jobName, String os, String runningOs, String token,
			String username, String location, String scm_type, String node_label, boolean isLocal);

	@Query(value="SELECT * FROM interface_server WHERE os=? AND scm_type=? AND  isLocal=? AND node_label LIKE ?%", nativeQuery=true)
	List<InterfaceServer> findByNodeLabelAndScmAndWindow(String runningOS, String scmType, boolean isLocal,
			String nodeLabel);

}

package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
	
	// Get project by flag
    @Query(value = "SELECT * "
    		+ "FROM project "
    		+ "WHERE deleted_flag='0' ", nativeQuery = true)
	List<Project> getAllUserProjectByFlag();
    
    @Query(value = "SELECT * "
    		+ "FROM project "
    		+ "WHERE deleted_flag='0' and created_by=?1", nativeQuery = true)
	List<Project> findProjectByDeletedFlagAndCreatedBy(String username);
    
    @Query(value = "SELECT * FROM project where id=?", nativeQuery = true)
    Project getProjectNew(Long id);
    
    @Query(value = "SELECT * "
    		+ "FROM project "
    		+ "WHERE deleted_flag='0' and type='Public Project' ", nativeQuery = true)
	List<Project> getAllUserProjectByFlagAndByType();
	
	@Query(value = "SELECT orh.ORDER_ID , orh.CREATED_BY, orh.ORDER_DATE, orh.STATUS_ID, orh.TEMP_PRICE, orh.FINISH_PTICE,"
			+ " ori.QUANTITY, p.FIRST_NAME, p.LAST_NAME FROM fibo_cart.order_interface_header orh join fibo_cart.order_interface_item ori on "
			+ "orh.ORDER_ID = ori.ORDER_ID join fibo_cart.party p on p.PARTY_ID = orh.CREATED_BY  where orh.BRAND_ID in"
			+ "(select BRAND_ID from  fibo_cart.brand b where  b.USER_AUTH = ?1) and ori.PRODUCT_ID = ?2 ", nativeQuery = true)
	List<Map<String,Object>> orderList(String user, String productId);
}

package io.bottest.jpa.respository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.ActiveOTP;

@Repository
@Transactional
public interface ActiveOPTRepository extends JpaRepository<ActiveOTP, Long>{

	List<ActiveOTP> findAllByActiveEmailAndBeginTimeLessThanEqualAndEndTimeGreaterThanEqualOrderByEndTimeDesc(
			String email, Date timeNow, Date currentTime);
	
}

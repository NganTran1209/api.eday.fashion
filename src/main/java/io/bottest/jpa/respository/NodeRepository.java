package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.NodeInfo;

@Repository
public interface NodeRepository extends JpaRepository<NodeInfo, Long> {
	
    
    @Query(value = "SELECT * FROM node_info WHERE username=? AND os=? AND mac=?", nativeQuery = true)
    NodeInfo getNodeInfo(String username, String os, String mac);

    @Modifying
	@Transactional
    @Query(value = "INSERT INTO node_info(username, secret_key, computer_name, os, mac) VALUES(?, ?, ?, ?, ?)", nativeQuery = true)
	int insertNodeinfo(String username, String secretKey, String machineName, String os, String mac);

    @Query(value = "SELECT * FROM node_info WHERE username=? AND os LIKE CONCAT(?,'%') AND is_alive = ?", nativeQuery = true)
	List<NodeInfo> getNodeInfoList(String nodeLabel, String runningOS, boolean status);
	
    
    @Modifying
	@Transactional
    @Query(value = "INSERT INTO node_info(username, session_id, is_alive, os, request_date) VALUES(?, ?, ?, ?, ?)", nativeQuery = true)
	int insertNewNodeinfo(String username, String session_id, boolean is_alive, String os, String request_date);
    
    @Modifying
	@Transactional
    @Query(value = "UPDATE node_info SET is_alive=false WHERE session_id=?", nativeQuery = true)
	void disconnectBySessionId(String sessionId);

    @Modifying
	@Transactional
    @Query(value = "UPDATE node_info SET is_alive=false WHERE username=? AND request_date<>? AND is_alive=true", nativeQuery = true)
	void updateNodeDisconnectWithUser(String name, String dateTime);

    @Modifying
	@Transactional
    @Query(value = "UPDATE node_info SET is_alive=true, request_date=?2 WHERE session_id=?1", nativeQuery = true)
	void updateNodeConnected(String sessionId, String requestDate);
}

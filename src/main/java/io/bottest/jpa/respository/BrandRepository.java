package io.bottest.jpa.respository;

import java.util.Date;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import io.bottest.jpa.entity.Work;

@Repository
@Transactional
public interface BrandRepository extends JpaRepository<Work, Long>{
	
	@Query(value="SELECT * FROM fibo_cart.brand where BRAND_NAME=?", nativeQuery = true)
	Map<String, Object> findByBrandName(String username);
	
	@Modifying
	@Query(value="INSERT INTO fibo_cart.brand (BRAND_NAME,TITLE,SUBTITLE,ADDRESS,PHONE_NUMBER,USER_AUTH,PASS_AUTH,CREATED_DATE,TOKEN) values (?,?,?,?,?,?,?,?,?)", nativeQuery = true)
	void insertBrand(String username, String string, String string2,String address,String phone, String string3,String password,  Date date, String token);

	@Query(value="SELECT * FROM fibo_cart.branch where BRANCH_NAME=?", nativeQuery = true)
	Map<String, Object> findByBrachName(String name);

	@Modifying
	@Query(value="INSERT INTO fibo_cart.brand_member (BRAND_ID,BRANCH_ID,CREATED_BY,CREATED_DATE) values (?,?,?,?)", nativeQuery = true)
	void createBrandMember(String brand_id, String branch_id, String name, Date date);

	@Modifying
	@Query(value="INSERT INTO fibo_cart.branch (BRANCH_ID,BRANCH_NAME,ADDRESS,PHONE_NUMBER,MAIL,CREATED_DATE,IS_DELETE) values (?,?,?,?,?,?,false)", nativeQuery = true)
	void insertBranch(String brandId, String name, String address, String phone, String username,Date date);

	@Modifying
	@Query(value="INSERT INTO fibo_cart.branch_facility (BRANCH_ID,FACILITY_NAME,CREATED_DATE,UPDATED_DATE) values (?,?,?,?)", nativeQuery = true)
	void createBranchFacility(String string, String string2, Date date, Date date2);

}

package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import io.bottest.dto.FilterTaskDTO;

public interface TaskRepositoryCustom {
	List<Map<String, Object>> findTask(FilterTaskDTO task , Long projectId);
}

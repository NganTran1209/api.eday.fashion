package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.ShipmentAddress;
import io.bottest.jpa.entity.ShipmentAddressPK;

@Repository
@Transactional 
public interface ShipmentAddressRepository extends JpaRepository<ShipmentAddress, ShipmentAddressPK>{

	List<ShipmentAddress> findAllById_UserId(String userId);

	@Query(value = "SELECT sa.seq FROM shipment_address sa WHERE sa.user_id = :userId ORDER BY sa.seq DESC LIMIT 1", nativeQuery = true)
	Integer findFirstByUserIdOrderBySeqDesc(String userId);

	@Modifying
	@Query(value = "UPDATE shipment_address sa SET sa.is_default = 0 WHERE sa.user_id = :userId", nativeQuery = true)
	void updatedUndefautAllRecordByUserId(String userId);

	ShipmentAddress findFirstById_UserIdOrderById_SeqDesc(String userId);
}
package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import io.bottest.jpa.entity.CostRevenueOther;

@Repository
public interface CostRevenueOtherRepository extends JpaRepository<CostRevenueOther, Long>   {
	@Query(value = "SELECT * FROM cost_revenue_other where owner = ?1 and status = ?2 order by id desc",nativeQuery = true)
	List<CostRevenueOther> getCostRevenueOtherByUserAndStatus (String username, String status);
}

package io.bottest.jpa.respository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import io.bottest.dto.FilterConditionData;
import io.bottest.jpa.entity.ExecutingJob;

@Component
public class ExecutingJobRepositoryImpl implements ExecutingJobCustom{
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public List<ExecutingJob> filterExecutingJob(Long projectId, FilterConditionData filter) {
		String condition = "";
		List<String> testsuiteName = filter.getTestsuiteName();
		List<String> executeBy = filter.getExecuteBy();
		String status = filter.getStatusTestcase();
		String type = filter.getType();
		List<String> miletone = filter.getMilestone();
		
		if(testsuiteName != null) {
			if(testsuiteName.size() > 0) {
				  condition += " and ( e.testsuite='"+testsuiteName.get(0)+"'";
				  for(int i = 1; i < testsuiteName.size(); i++) {
					  condition += " or e.testsuite='"+testsuiteName.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		if(executeBy != null) {
			if(executeBy.size() > 0) {
				  condition += " and ( r.running_user='"+executeBy.get(0).replace("@", ".")+"'";
				  for(int i = 1; i < executeBy.size(); i++) {
					  condition += " or r.running_user='"+executeBy.get(i).replace("@", ".")+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		
		if(status != null) {
			if(!status.isEmpty()){
				condition += " and r.test_result='"+status+"'";
			}
			
		}
		
		if(miletone != null) {
			if(miletone.size() > 0) {
				  condition += " and ( e.running_name='"+miletone.get(0)+"'";
				  for(int i = 1; i < miletone.size(); i++) {
					  condition += " or e.running_name='"+miletone.get(i)+"'";
					  
				  }
				  condition += ")";
				
			}
		}
		
		String query = "SELECT e.* FROM executing_job e left join (select * from result_info " + 
				"where id in (select max(id) from result_info as r1 group by " + 
				"r1.result_name)) as  r on concat('Result_', replace(e.username, '@','.'),'_',e.running_date) = r.result_name " + 
				"where e.project_id="+projectId+"  and e.type='"+type+"' "
				+ condition;
		List<ExecutingJob> results = entityManager.createNativeQuery(query, ExecutingJob.class).getResultList();
		return results;
	}

}

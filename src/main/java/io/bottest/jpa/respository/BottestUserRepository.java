package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.BottestUser;

@Repository
@Transactional
public interface BottestUserRepository extends JpaRepository<BottestUser, String> {
	BottestUser findByUsernameIgnoreCase(String username);
	
	@Query(value = "SELECT * FROM bottest_user where enable=1", nativeQuery = true)
	List<BottestUser> findByAllUsers();
	
	@Query(value= "SELECT * FROM  bottest_user where username =?1", nativeQuery = true)
	BottestUser getBottestUserByUsername(String username);
		
	@Modifying
	@Transactional
	@Query(value= "UPDATE bottest_user SET status=? where username =?", nativeQuery = true)
	int updateStatus(boolean status, String username);

	@Query(value= "SELECT status FROM bottest_user where username =?", nativeQuery = true)
	boolean getStatusByUsername(String username);
	
	@Query(value = "SELECT * FROM bottest_user b WHERE b.username = ? AND b.enable = true", nativeQuery = true)
	BottestUser findByUserName(String username);

}
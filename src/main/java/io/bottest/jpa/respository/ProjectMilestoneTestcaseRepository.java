package io.bottest.jpa.respository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.ProjectMilestoneTestcase;
import io.bottest.jpa.entity.ProjectMilestoneTestcaseIdentity;

@Repository
public interface ProjectMilestoneTestcaseRepository extends JpaRepository<ProjectMilestoneTestcase, ProjectMilestoneTestcaseIdentity>{

	List<ProjectMilestoneTestcase> findByProjectMilestoneTestcaseIdentity_ProjectIdAndProjectMilestoneTestcaseIdentity_MilestoneName(Long projectId, String milestoneName);
	
	@Modifying
	@Transactional
	@Query(value = "delete FROM project_milestone_testcase where milestone_name=?2 and project_id=?1", nativeQuery = true)
	int deleteDataFromTable(Long projectId, String milestoneName);
}

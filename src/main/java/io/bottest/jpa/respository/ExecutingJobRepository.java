package io.bottest.jpa.respository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.ExecutingJob;

@Repository
public interface ExecutingJobRepository extends JpaRepository<ExecutingJob, Long>, ExecutingJobCustom {

	List<ExecutingJob> findByStatusAndProjectIdIn(String running, Set<String> projectNames);

	List<ExecutingJob> findByStatusIn(Collection<String> checkStatus);

	List<ExecutingJob> findByStatusInAndProjectIdIn(List<String> listRunning, Set<String> projectNames);
	
	ExecutingJob findByStatusInAndProjectIdIn(List<String> listRunning, String projectId);

	List<ExecutingJob> findByProjectIdOrderByIdDesc(String projectId);
	
	List<ExecutingJob> findByProjectIdAndTestsuiteOrderByIdDesc(String projectId, String testsuite);

	List<ExecutingJob>  findByTypeAndProjectIdAndTestsuiteOrderByIdDesc(String jobSchedule, String projectId, String testsuiteId);

	List<ExecutingJob> findByTypeAndProjectIdOrderByIdDesc(String jobSchedule, String projectId);
	
	@Query(value = "SELECT count(id) FROM executing_job where project_id=?1 and testsuite=?2 and type='SCHEDULE'", nativeQuery = true)
	int countSchedules(int projectId, String testsuite);
	
	@Query(value = "SELECT * FROM executing_job where status !=new_status or status='RUNNING'", nativeQuery = true)	
	List<ExecutingJob> findAllStatusChanged();

	@Query(value = "SELECT result_json FROM eday.executing_job WHERE project_id=? AND testsuite=? AND running_date=?", nativeQuery = true)	
	String findResultByProjectIdAndTestsuiteAndRunningDate(String projectId, String testsuilt, String runningDate);
	
	@Query(value = "SELECT * FROM executing_job where project_id=?1 and testsuite=?2 and type='SCHEDULE'", nativeQuery = true)
	List<ExecutingJob> findExecutingJobByProjectIdAndTestsuite(String projectId, String testsuiteName);
	
}

package io.bottest.jpa.respository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.PayRoll;
import io.bottest.jpa.entity.PayRollIdentity;

@Transactional
@Repository
public interface PayRollRepository extends JpaRepository<PayRoll, PayRollIdentity>{
		
	@Query(value = "SELECT * FROM payroll p WHERE p.worker_id = :worker_id ORDER BY month DESC LIMIT 3", nativeQuery = true)
	List<PayRoll> findAllPayRollByWorkerId(@Param("worker_id") Long workerId);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE payroll SET salary_to_month = ? WHERE month = ? AND worker_id = ?", nativeQuery = true)
	void updateSalaryToMonthByMonthAndWorkerId(Long salaryToMonth, Date month, Long workerId );
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE payroll SET hour_of_day = ?, unit_salary = ?, contract_salary = ? WHERE month = ? AND worker_id = ?", nativeQuery = true)
	void updatePayRollBasedOnMonthAndWorkerId(String hourOfDay, String unitSalary, Long contractSalary, String month, Long workerId);
	
	@Query(value = "SELECT * FROM payroll p WHERE p.month = :month AND p.worker_id = :workerId", nativeQuery = true)
	PayRoll findPayRollWorkerByMonthAndWorkerId(@Param("month") String month, @Param("workerId") Long workerId);

	@Transactional
	@Modifying
	@Query(value = "UPDATE payroll SET status = ? WHERE month = ? AND worker_id = ?", nativeQuery = true)
	void updateStatusPayRollOfWorker(boolean status, String month, Long workerId);

	@Transactional
	@Modifying
	@Query(value = "UPDATE payroll SET contract_salary = ?, total_salary = ? WHERE month = ? AND worker_id = ?", nativeQuery = true)
	void updatePayRollBasedOnMonthAndWorkerId(Long unit_salary, Long contract_salary, String monthToPayRoll, Long id);

	@Query(value = "SELECT * FROM payroll p WHERE p.month = ? ORDER BY month ", nativeQuery = true)
	List<PayRoll> findByMonth(String month);

	@Query(value = "SELECT * FROM payroll p ORDER BY month DESC", nativeQuery = true)
	List<PayRoll> findAllSortByMonth();

	@Query(value = "SELECT * FROM payroll p WHERE p.month = :month AND p.worker_id = :workerId AND status = false", nativeQuery = true)
	PayRoll findPayRollWorkerUnpaidByMonthAndWorkerId(String month, Long workerId);
	
}

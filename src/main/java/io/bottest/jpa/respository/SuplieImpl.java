package io.bottest.jpa.respository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;
import io.bottest.dto.FilterSupplieDTO;

@Component
public class SuplieImpl implements SupplieCustom{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Map<String, Object>> filterVatTu(FilterSupplieDTO filterVatTu, String userName) {
		String condition = "";
		List<String> type = filterVatTu.getType();
		String fromDate = filterVatTu.getFromDate();
		String toDate = filterVatTu.getToDate();
		if(type != null) {
			  if(type.size() > 0) {
				  condition += " and ( pr.product_type_id like '"+type.get(0)+"'";
				  for(int i = 1; i < type.size(); i++) {
					  condition += " or pr.product_type_id like '"+type.get(i)+"'";
				  }
				  condition += ")";
			  }
		  }
		if(fromDate != null) {
			  if(!fromDate.isEmpty()) {
				  condition +=  " and prW.created_date >='"+fromDate+"'";
			  }
		  }
		if(toDate != null) {
			  if(!toDate.isEmpty()) {
				  condition +=  " and prW.created_date <='"+toDate+"'";
			  }
		  }
		 String query = "SELECT pr.product_id, pr.product_name,pr.product_code, pr.is_active, pr.created_by_user_login, pr.brand_name, pr.username, pr.price_detail_text,pr.product_type_id  , prW.created_date, prW.total_quantity, prW.inventory, prW.create_by, prW.date_finish, prW.updated_date " + 
		 				"FROM farmate.product pr left join farmate.product_warehouse prW on pr.product_code = prW.product_code where username = '"+userName+"' " + condition;
		 List<Object> results = entityManager.createNativeQuery(query).getResultList();
		  List<Map<String, Object>> listOb = new ArrayList<Map<String,Object>>();
		  for(int i = 0; i < results.size(); i++) {
				Object[] result = (Object[]) results.get(i);
				Map<String, Object> ob = new HashMap<String, Object>();
				listOb = setDataToObject(ob, result, listOb);
			}
		  return listOb;
	}

	private List<Map<String, Object>> setDataToObject(Map<String, Object> ob, Object[] result,List<Map<String, Object>> listOb) {
		ob.put("product_id", result[0]);
		ob.put("product_code", result[1]);
		ob.put("is_active", result[2]);
		ob.put("created_by_user_login", result[3]);
		ob.put("brand_name", result[4]);
		ob.put("created_by_user_login", result[5]);
		ob.put("username", result[6]);
		ob.put("price_detail_text", result[7]);
		ob.put("product_type_id", result[8]);
		ob.put("created_date", result[9]);
		ob.put("total_quantity", result[10]);
		ob.put("inventory", result[11]);
		ob.put("create_by", result[12]);
		ob.put("date_finish", result[13]);
		ob.put("updated_date", result[14]);
		listOb.add(ob);
		return listOb;
	}
	
}

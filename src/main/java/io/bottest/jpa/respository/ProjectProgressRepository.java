package io.bottest.jpa.respository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.ProjectProgress;
import io.bottest.jpa.entity.ProjectProgressIdentity;
@Repository
public interface ProjectProgressRepository extends JpaRepository<ProjectProgress, ProjectProgressIdentity>,ProjectProgressCustom {
	@Query(value = "SELECT count(DISTINCT testsuite)FROM project_progress Where project_id=? and project_progress.status='add' ",nativeQuery = true)
		int counttestsuite(int projectId);
	@Query(value = "SELECT count(project_id)FROM project_progress Where project_id=? and project_progress.status='add' ",nativeQuery = true)
	  	int counttestcase(Long projectId);
	@Query(value = "SELECT create_date from project_progress where project_id=? and project_progress.status='add' group by create_date order by create_date",nativeQuery = true)
	List<Date> findAllByCreateDate(Long projectId);
	
	@Query(value= "select p.project_id, p.testsuite, count(p.testsuite) as numOfCases, p.create_date as createDate from project_progress p where p.project_id=? group by p.project_id, p.testsuite,  p.create_date order by p.project_id, p.create_date, p.testsuite", nativeQuery = true)
	List<Map<String, Object>> findAllTestsuite(Long projectId);

	List<ProjectProgress> findByProjectProgressIdentity_ProjectIdAndProjectProgressIdentity_Testsuite(Long projectId,
			String testsuiteId);
	
	@Query(value = "SELECT * FROM project_progress where project_id=?1 and testsuite=?2 and type='automation'", nativeQuery = true)
	List<ProjectProgress> findByProjectProgressStatusAutomation(Long projectId,String testsuiteId);
	
	@Query(value = "SELECT p.project_id as projectIdTc, p.testcase,p.testsuite,p.assign_to as assignTo,p.create_date as createDate,p.description,p.status as statusTc,p.testscript,p.update_date as updateDate,p.update_user as updateUser,p.expected_output as expectedOutputTc,p.pre_condition as preCondition,p.testcase_procedure as testcaseProcedure"
			+ ", p.status_issue as statusIssue, p.plan_start_date as planStartDate, p.plan_end_date as planEndDate, p.estimated_time as estimatedTime, p.actual_time as actualTime, p.type as typeTc, p.category,p.order_id, "
			+"r.id, r.execute_date as execute_date, r.project_id as projectId, r.result_name as resultName, r.running_user as runningUser, r.test_case_name as testCaseName,"
			+ "r.test_case_result as testCaseResult, r.test_result as testResult, r.test_suite as testSuite, r.actual_output as actualOutput, r.browser, r.device_name as deviceName,"
			+ "r.expected_output as expectedOutput, r.location, r.milestone, r.mobile_mode as mobileMode, r.os, r.running_description as runningDescription, "
			+ "r.screen_resolution as screenResolution, r.type, r.files, r.status "
			+ "FROM project_progress as p "
			+ "left join "
			+ "("
				+ "select * from result_info where id in "
				+ "("
					+ "select max(id) from result_info as r1 group by r1.project_id, r1.test_suite, r1.test_case_name)"
				+ ") "
			+ "as r on p.project_id=r.project_id and p.testsuite=r.test_suite and p.testcase=r.test_case_name "
			+ "where p.project_id=? and p.type='automation' and p.status='add' "
			+ "order by order_id", nativeQuery = true)
	List<Map<String, Object>> findAllProjectProgressByProjectId(Long projectId);
	
	@Query(value = "SELECT p.project_id as projectIdTc, p.milestone as milestoneTc, p.testcase,p.testsuite,p.assign_to as assignTo,p.create_date as createDate,p.description,p.status as statusTc,p.testscript,p.update_date as updateDate,p.update_user as updateUser,p.expected_output as expectedOutputTc,p.pre_condition as preCondition,p.testcase_procedure as testcaseProcedure"
			+ ", p.status_issue as statusIssue, p.plan_start_date as planStartDate, p.plan_end_date as planEndDate, p.estimated_time as estimatedTime, p.actual_time as actualTime,p.type as typeTc, p.category,p.order_id, "
			+"r.id, r.execute_date as execute_date, r.project_id as projectId, r.result_name as resultName, r.running_user as runningUser, r.test_case_name as testCaseName,"
			+ "r.test_case_result as testCaseResult, r.test_result as testResult, r.test_suite as testSuite, r.actual_output as actualOutput, r.browser, r.device_name as deviceName,"
			+ "r.expected_output as expectedOutput, r.location, r.milestone, r.mobile_mode as mobileMode, r.os, r.running_description as runningDescription, "
			+ "r.screen_resolution as screenResolution, r.type, r.files, r.status "
			+ "FROM project_progress as p "
			+ "left join "
			+ "("
				+ "select * from result_info where id in "
				+ "("
					+ "select max(id) from result_info as r1 group by r1.project_id, r1.test_suite, r1.test_case_name)"
				+ ") "
			+ "as r on p.project_id=r.project_id and p.testsuite=r.test_suite and p.testcase=r.test_case_name "
			+ "where p.project_id=? and p.type='manual' and p.status='add' "
			+ "order by order_id", nativeQuery = true)
	List<Map<String, Object>> findAllPPManualByProjectId(Long projectId);
	
	@Query(value = "SELECT p.project_id as projectIdTc, p.testcase,p.testsuite,p.assign_to as assignTo,p.create_date as createDate,p.description,p.status as statusTc,p.testscript,p.update_date as updateDate,p.update_user as updateUser,p.expected_output as expectedOutputTc,p.pre_condition as preCondition,p.testcase_procedure as testcaseProcedure"
			+ ", p.status_issue as statusIssue, p.plan_start_date as planStartDate, p.plan_end_date as planEndDate, p.estimated_time as estimatedTime, p.actual_time as actualTime,p.type as typeTc, p.category, "
			+"r.id, r.execute_date as execute_date, r.project_id as projectId, r.result_name as resultName, r.running_user as runningUser, r.test_case_name as testCaseName,"
			+ "r.test_case_result as testCaseResult, r.test_result as testResult, r.test_suite as testSuite, r.actual_output as actualOutput, r.browser, r.device_name as deviceName,"
			+ "r.expected_output as expectedOutput, r.location, r.milestone, r.mobile_mode as mobileMode, r.os, r.running_description as runningDescription, "
			+ "r.screen_resolution as screenResolution, r.type, r.files, r.status "
			+ "FROM project_progress as p "
			+ "left join "
			+ "("
				+ "select * from result_info where id in "
				+ "("
					+ "select max(id) from result_info as r1 group by r1.project_id, r1.test_suite, r1.test_case_name)"
				+ ") "
			+ "as r on p.project_id=r.project_id and p.testsuite=r.test_suite and p.testcase=r.test_case_name "
			+ "where p.project_id=?  and p.status='add' and r.test_case_result='FAILED' "
			+ "order by (SUBSTRING_INDEX(testcase,'_',-1)*1) desc", nativeQuery = true)
	List<Map<String, Object>> getForBugId(Long projectId);
	
	@Query(value = "SELECT * FROM project_progress where project_id=?1 and testsuite=?2 and type='manual'", nativeQuery = true)
	List<ProjectProgress> findByProjectProgressStatusManual(Long projectId,String testsuiteId);
	
	@Query(value="select * from project_progress where project_id=?1 and testsuite=?2 and testcase=?3", nativeQuery = true)
	ProjectProgress findByProjectIdAndTestsuiteAndTestcase(Long projectId, String testsuite, String testcase);
	
	@Query(value = "SELECT * FROM project_progress where project_id=?1 and testsuite=?2 and type='manual' and status='add' order by (SUBSTRING_INDEX(testcase,'_',-1)*1) desc ", nativeQuery = true)
	List<ProjectProgress> findAllTestcaseManual(Long projectId, String testsuite);
	
	ProjectProgress findByProjectProgressIdentity_ProjectIdAndProjectProgressIdentity_TestsuiteAndProjectProgressIdentity_Testcase(Long projectId, String testsuiteId, String testcase);
	
	@Query(value = "SELECT count(project_id) FROM project_progress where project_id=?1 and testsuite=?2 and type=?3 and status='add'", nativeQuery = true)
	int countTcManual(Long projectId, String testsuite, String type);
	
	@Query(value = "SELECT type, count(testcase) as numberText FROM project_progress Where project_id=? and project_progress.status='add' group by type", nativeQuery = true)
	List<Map<String, Object>> findGraphicTestcaseAutAndMan(Long projectId);
	
	@Query(value = "select pr.testsuite, pr.testcase, pr.description , IFNULL(rs.test_case_result, 'NOT-RUN' ) as status,  rs.execute_date	from project_progress as pr  " + 
			"			left join " + 
			"			( " + 
			"				select  project_id, test_suite, test_case_name, execute_date , id ,test_case_result " + 
			"					from  result_info " + 
			"					where id in (select max(rss.id) from result_info  rss where rss.project_id=?1 and rss.milestone=?2 " + 
			"			        group by rss.test_case_name) " + 
			"				) as rs " + 
			"			    on rs.project_id = pr.project_id and pr.testsuite = rs.test_suite and pr.testcase = rs.test_case_name  " + 
			"			    where pr.project_id=?1 and pr.status='add' " + 
			"			    group by  pr.project_id , pr.testsuite, pr.testcase, rs.test_case_result order by pr.project_id , pr.testsuite, status", nativeQuery = true)
	List<Map<String, Object>> getAllMilestoneGraphic(Long projectId, String miletoneName);
	
	@Query(value = "SELECT * FROM project_progress where project_id=?1 and testsuite=?2 and type='automation' and status='add' order by (SUBSTRING_INDEX(testcase,'_',-1)*1) desc  ", nativeQuery = true)
	List<ProjectProgress> findAllProjectProgressAutomation(Long projectId,String testsuiteId);
	
	@Query(value = "SELECT p.project_id as project_idTc, p.testcase,p.testsuite,p.assign_to,p.create_date,p.description,p.status as statusTc,p.testscript,p.update_date,p.update_user,p.expected_output as expected_outputTc,p.pre_condition,p.testcase_procedure, r.*   FROM project_progress as p left join (select * from result_info where id in (select max(id) from result_info as r1 group by r1.project_id, r1.test_suite, r1.test_case_name)) as r on p.project_id=r.project_id and p.testsuite=r.test_suite and p.testcase=r.test_case_name where p.project_id=?1 and p.type='automation' and p.status='add' and p.testsuite=?2 order by (SUBSTRING_INDEX(testcase,'_',-1)*1) desc", nativeQuery = true)
	List<Map<String, Object>> findAllProjectProgressAutomations(Long projectId,String testsuiteId);
	
	@Query(value="select * from project_progress where project_id=?1 and testsuite=?2 and testcase=?3", nativeQuery = true)
	ProjectProgress getProjectProgressById(Long projectId, String testsuite, String testcase);
	
	@Query(value="select * from project_progress where project_id=?1 and testcase=?2", nativeQuery = true)
	ProjectProgress findProjectProgressByProjectIdAndTestcase(Long projectId, String testcase);
	
	@Query(value = "SELECT IFNULL(max(order_id),0) FROM project_progress where project_id=?1 and status='add' and type=?2", nativeQuery = true)
	int getMaxOrderId(Long projectId, String type);
	
	@Query(value = "SELECT p.testcase, p.expected_output as expectedOutputTc, p.testcase_procedure as testcaseProcedure, r.* "
			+"FROM project_progress as p "
			+"left join "
			+"("
			+"	select * from result_info where id in "
			+"	("
			+"		select max(id) from result_info as r1 group by r1.project_id, r1.test_suite, r1.test_case_name)"
			+"	) "
			+"as r on p.project_id=r.project_id and p.testsuite=r.test_suite and p.testcase=r.test_case_name "
			+"where p.project_id=?1 and p.status='add' and p.testsuite=?2 group by p.type, p.testcase "
			+"order by order_id", nativeQuery = true)
	List<Map<String, Object>> getDataExportExcel(Long projectId, String testsuite);
	
	@Query(value = "SELECT 'Testcase' as tracker,status_issue as status, count(status) as count, ?3 as colorCode FROM project_progress where project_id=?1 and status_issue=?2 and status='add'", nativeQuery = true)
	Map<String, Object> graphicTestcaseStatus(Long projectId, String status, String colorCode);
	
	@Query(value = "SELECT 'Testcase' as tracker,status_issue as status, count(status) as count, ?3 as colorCode FROM project_progress where project_id=?1 and status_issue=?2 and status='add' and milestone=?4", nativeQuery = true)
	Map<String, Object> graphicTestcaseStatusMilestone(long projectId, String itemName, String colorCode, String nameMilestone);
}

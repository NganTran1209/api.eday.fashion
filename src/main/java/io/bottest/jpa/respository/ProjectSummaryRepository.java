package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.ProjectSummary;
import io.bottest.jpa.entity.ProjectSummaryIdentity;

@Repository
public interface ProjectSummaryRepository extends JpaRepository<ProjectSummary, ProjectSummaryIdentity>{
	
	@Query(value = "SELECT * FROM project_summary where project_id=?1 and testsuite=?2 order by summary_date desc limit 1", nativeQuery = true)
	ProjectSummary findByDateAndProjectIdAndTestsuite(String projectId, String testsuite);
	
	List<ProjectSummary> findByProjectSummaryIdentity_ProjectId(String projectId);
	
	@Query(value = "SELECT * FROM project_summary where project_id=? and summary_date=curdate()", nativeQuery = true)
	List<ProjectSummary> findProjectSummayByProjectIdAndCurDate(String projectId);
	
	@Query(value = "SELECT testsuite FROM project_summary where project_id=? group by testsuite", nativeQuery = true)
	List<String> getAllTestsuiteInProject(Long projectId);
}

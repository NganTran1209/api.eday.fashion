package io.bottest.jpa.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

	@Query(value= "SELECT * FROM  category where id =?1", nativeQuery = true)
	Category findByCatId(Long catId);

}

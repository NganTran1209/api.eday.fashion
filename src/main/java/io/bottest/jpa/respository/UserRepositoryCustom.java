package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import io.bottest.dto.FilterUserDTO;

public interface UserRepositoryCustom {

	List<Map<String, Object>> filterStaff(FilterUserDTO filterStaff, int i);

}

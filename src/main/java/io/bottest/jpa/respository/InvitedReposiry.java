package io.bottest.jpa.respository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.jpa.entity.InvitedToProject;


@Repository
public interface InvitedReposiry extends JpaRepository<InvitedToProject, Long> {
	
	InvitedToProject findByToEmailAndProjectId(String toEmail, String projectId);
	
	@Query(value = "SELECT * FROM invited_to_project WHERE to_email = ?1 order by status asc ", nativeQuery = true)
	List<InvitedToProject> findByToUserName(String username);
	
	@Query(value = "SELECT * FROM invited_to_project WHERE id = ?1  ", nativeQuery = true)
	InvitedToProject findByIdInvited(String id);
	
	@Query(value = "SELECT * FROM invited_to_project WHERE project_id = ?1 and status = false ", nativeQuery = true)
	List<InvitedToProject> findByProjectIdAndStatus(String projectId);
	
	InvitedToProject findByProjectIdAndToEmail(String projectId, String toEmail);
	
}
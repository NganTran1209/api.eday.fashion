package io.bottest.utils;

public interface FarmateConstants {
	final public static String STATUS_DOING = "doing";
	final public static String STATUS_DONE = "done";
	final public static String PROJECT_TYPE_PUBLIC = "Public Project";
	final public static String PROJECT_TYPE_PRIVATE = "Private Project";
}

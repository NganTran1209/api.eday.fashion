package io.bottest.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.PayRollDTO;
import io.bottest.dto.UserDTO;
import io.bottest.dto.WorkerDTO;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.PayRoll;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.entity.Worker;
import io.bottest.jpa.respository.PayRollRepository;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.jpa.respository.WorkerRepository;
import io.bottest.service.PayRollService;
import io.bottest.service.WorkerService;
import io.bottest.utils.DateUtil;
import io.bottest.utils.FarmateConstants;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RestController
@RequestMapping("/payroll")
public class PayRollController {

	private final Logger LOGGER = LogManager.getLogger(PayRollController.class);
	
	@Autowired
	PayRollRepository payRollRepo;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	WorkerRepository workerRepo;
	
	@Autowired
	PayRollService payRollService;
	
	@Autowired
	WorkerService workerService;
	
	@RequestMapping(value = { "/getAll" }, method = RequestMethod.GET)
	public APIResult getAllUser(@AuthenticationPrincipal UserDetails userDetails) {

		LOGGER.info("Start GET /payroll/getAll");

		APIResult result = new APIResult();
		if(userDetails != null ) {
			User auth = userRepository.findByUserNameAndPassword(userDetails.getUsername(),userDetails.getPassword() );
			if(auth.getRole().equals("ROLE_ADMIN")) {
				List<PayRoll> listPayRoll = payRollRepo.findAllSortByMonth();
				result.setData(listPayRoll);
			}else {
				result.setErrormessage("User don't permission!!");
			}
		}else {
	    	result.setErrormessage("403 Authencation!!");
	    }

		LOGGER.info("End GET /payroll/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/createPayRollMonth" }, method = RequestMethod.POST)
	public APIResult createPayRollMonth(@AuthenticationPrincipal UserDetails userDetails) {

		LOGGER.info("Start GET /payroll/createPayRollMonth");

		APIResult result = new APIResult();
		if(userDetails != null ) {
			User auth = userRepository.findByUserNameAndPassword(userDetails.getUsername(),userDetails.getPassword() );
			if(auth.getRole().equals("ROLE_ADMIN")) {
				String month = this.getMonthToPayRoll();
				List<Worker> listWorkerEntities = workerRepo.findAll();
				for(Worker worker : listWorkerEntities) {
					PayRoll payRoll = payRollRepo.findPayRollWorkerByMonthAndWorkerId(month, worker.getId());
					if(payRoll == null) {
						payRollService.createNewPayMonth(month, worker);
					}
				}
				List<PayRoll> listPayRoll = payRollRepo.findAll();
				result.setData(listPayRoll);
			}else {
				result.setErrormessage("User don't permission!!");
			}
		}else {
	    	result.setErrormessage("403 Authencation!!");
	    }

		LOGGER.info("End GET /payroll/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/getListByMonth" }, method = RequestMethod.GET)
	public APIResult getListPayRollByMonth(@AuthenticationPrincipal UserDetails userDetails,@RequestBody PayRollDTO payRollDTO) {

		LOGGER.info("Start GET /payroll/getListByMonth");

		APIResult result = new APIResult();
		if(userDetails != null ) {
			User auth = userRepository.findByUserNameAndPassword(userDetails.getUsername(),userDetails.getPassword() );
			if(auth.getRole().equals("ROLE_ADMIN")) {
				String month = payRollDTO.getMonth();
				List<PayRoll> listPayRoll = payRollRepo.findByMonth(month);
				String filePath = payRollService.createFileExcelPayRoll(listPayRoll,month );
				result.setData(filePath);
			}else {
				result.setErrormessage("User don't permission!!");
			}
		}else {
	    	result.setErrormessage("403 Authencation!!");
	    }

		LOGGER.info("End GET /payroll/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/downloadPayRollExport" }, method = RequestMethod.POST)
	public ResponseEntity<InputStreamResource> downloadPayRollExport(@AuthenticationPrincipal UserDetails userDetails,Principal principal,@RequestBody PayRollDTO payRollDTO,HttpServletRequest request) {
		String month = payRollDTO.getMonth();
		final String LOG_STRING = "GET /payroll/" + month  +"/downloadPayRollExport";
		LOGGER.info("Start " + LOG_STRING);
        HttpHeaders responseHeader=null;
        InputStreamResource inputStreamResource=null;
		try {

			responseHeader = new HttpHeaders();
			File files = ResourceUtils.getFile(this.getListPayRollByMonth(userDetails, payRollDTO).getData().toString());
		    byte[] data = FileUtils.readFileToByteArray(files);
		    responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		      // Thiết lập thông tin trả về
	        responseHeader.set("Content-disposition", "attachment; filename=" + files.getName());
	        responseHeader.setContentLength(data.length);
	        InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
	        inputStreamResource = new InputStreamResource(inputStream);
	       
		} catch (FileNotFoundException e) {
			LOGGER.error(LOG_STRING + " ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}catch (IOException e) {
			LOGGER.error(LOG_STRING + " ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));

		}
        
		LOGGER.info("End " + LOG_STRING);
		return new ResponseEntity<InputStreamResource>(inputStreamResource, responseHeader, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/{workerId}/pay", method = RequestMethod.POST)
	public APIResult updateStatusPayRollOfWorker(@AuthenticationPrincipal UserDetails userDetails,
			@RequestBody PayRollDTO payRollDTO, @PathVariable("workerId") Long workerId) {
		final String LOG_STRING = "POST /payroll/" + workerId + "/pay";
		LOGGER.info("START" + LOG_STRING);
		LOGGER.info(LOG_STRING + "PARAMS payRollDTO:" + payRollDTO);
		LOGGER.info(LOG_STRING + "PARAMS workerId:" + workerId);

		APIResult result = new APIResult();
		try {
			
			if (payRollDTO == null || workerId == null) {
				result.setErrormessage("Input request null");
				return result;
			}
			WorkerDTO workerDTO = workerService.findWorkerById(Long.valueOf(workerId));
			if (workerDTO == null) {
				result.setErrormessage("Infor worker doesn't exist !");
				return result;
			}
			workerService.updateStatusPayRollOfWorker(workerId, payRollDTO);
			PayRoll payRoll = payRollRepo.findPayRollWorkerByMonthAndWorkerId(payRollDTO.getMonth(), workerId);
			result.setData(payRoll);
		} catch (Exception ex) {
			ex.printStackTrace();
			result.setErrormessage("EROR");
		}

		LOGGER.info("END" + LOG_STRING);
		return result;
	}

	@RequestMapping(value = "/{workerId}/updateBonus", method = RequestMethod.POST)
	public APIResult updateDayOffOrDayWorkOfWorker(@AuthenticationPrincipal UserDetails userDetails,
			@RequestBody PayRollDTO payRollDTO, @PathVariable("workerId") Long workerId) {
		final String LOG_STRING = "POST /api/v1/worker/" + workerId + "/dayOffOrdayWork/update";
		LOGGER.info("START " + LOG_STRING);
		LOGGER.info(LOG_STRING + "PARAMS userDetails:" + userDetails);
		LOGGER.info(LOG_STRING + "PARAMS payRollDTO:" + payRollDTO);
		LOGGER.info(LOG_STRING + "PARAMS workerId:" + workerId);

		APIResult result = new APIResult();
		try {
			
			WorkerDTO workerDTO = workerService.findWorkerById(Long.valueOf(workerId));
			if (workerDTO == null) {
				result.setErrormessage("Infor worker doesn't exist !");
				return result;
			}
			PayRoll payRoll = payRollRepo.findPayRollWorkerUnpaidByMonthAndWorkerId(payRollDTO.getMonth(), workerId);
			if(payRoll == null) {
				result.setErrormessage("Infor payRoll doesn't exist or payroll paid !");
				return result;
			}else {
				payRoll.setBonus(payRollDTO.getBonus());
				payRoll.setComment(payRollDTO.getComment());
				Worker worker = workerRepo.getWorkerById(workerId);
				Long totalSalary = workerService.setSalaryWorker(payRoll, worker);
				payRoll.setTotalSalary(totalSalary);
				payRollRepo.save(payRoll);
				result.setData(payRoll);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	public String getMonthToPayRoll() {
		LOGGER.info("Begin Service WorkerService Function getFirstDayOfMonthToPayRoll");
		
        Date date = new Date();
        try {
            String month = DateUtil.convertDateToFormaterString(date);
            if(!StringUtils.isEmpty(month)) {
            	return month;
            }
        } catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
        LOGGER.info("End Service WorkerService Function getFirstDayOfMonthToPayRoll");
        return "";
	}
	
}

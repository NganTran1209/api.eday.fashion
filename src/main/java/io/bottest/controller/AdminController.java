package io.bottest.controller;

import java.lang.reflect.Type;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.CatDTO;
import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.FilterUserDTO;
import io.bottest.dto.OrderDTO;
import io.bottest.dto.PayRollDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.dto.StaffDTO;
import io.bottest.dto.TestsuiteTestingProgress;
import io.bottest.dto.UpdateUserDTO;
import io.bottest.dto.UserDTO;
import io.bottest.dto.WorkerDTO;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.Category;
import io.bottest.jpa.entity.Component;
import io.bottest.jpa.entity.InterfaceServer;
import io.bottest.jpa.entity.Order;
import io.bottest.jpa.entity.PayRoll;
import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.ProductWareHouse;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.entity.ProjectComponent;
import io.bottest.jpa.entity.Software;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.entity.Work;
import io.bottest.jpa.entity.Worker;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.CategoryRepository;
import io.bottest.jpa.respository.OrderPoductRepository;
import io.bottest.jpa.respository.PayRollRepository;
import io.bottest.jpa.respository.PaymentRepository;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProductWareHouseResponsitory;
import io.bottest.jpa.respository.SoftwareRepository;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.jpa.respository.UserRepositoryCustom;
import io.bottest.jpa.respository.WorkerRepository;
import io.bottest.service.CategoryServise;
import io.bottest.service.ComponentService;
import io.bottest.service.FileService;
import io.bottest.service.InterfaceServerService;
import io.bottest.service.PayRollService;
import io.bottest.service.ProductService;
import io.bottest.service.ProductWareHouseService;
import io.bottest.service.ProjectComponentServise;
import io.bottest.service.ProjectService;
import io.bottest.service.SoftwareService;
import io.bottest.service.WorkerService;
import io.bottest.utils.DateUtil;
import io.bottest.utils.FarmateConstants;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RestController
@RequestMapping("/admin")
public class AdminController { 

	private static final String LOG_STRING = null;

	private final Logger LOGGER = LogManager.getLogger(AdminController.class);

	@Autowired
	BottestUserRepository userRepo;

	@Autowired
	ComponentService conponentService;

	@Autowired
	InterfaceServerService interfaceSevice;

	@Autowired
	BottestUserRepository bottestUserRepository;

	@Autowired
	ComponentService componentService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	SoftwareService softwareService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SoftwareRepository softwareRepo;
	
	@Autowired
	ProjectComponentServise projectComponentService;
	
	@Autowired
	CategoryServise categoryService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	CategoryRepository catRepo;
	
	@Autowired
	OrderPoductRepository orderRepo;
	
	@Autowired
	WorkerRepository workerRepo;
	
	@Autowired
	FileService fileService;
	
	@Autowired
	private WorkerService workerService;
	
	@Autowired
	private PayRollRepository payRollRepository;
	
	@Autowired
	private ProductWareHouseService productWareService;
	
	@Autowired
	ProductWareHouseResponsitory productWareRepo;
	
	@Autowired
	PaymentRepository paymentRepo;
	
	@Autowired
	UserRepositoryCustom userCustome;
	
	@Autowired
	PayRollService payRollService;
	/**
	 * @return
	 */
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
	public ModelAndView getManagement() {

		LOGGER.info("Start GET /admin ");

		ModelAndView mav = new ModelAndView("homeAdmin");

		LOGGER.info("End GET /admin");
		return mav;
	}

	/**
	 * @param users
	 * @return
	 */
	@RequestMapping(value = { "/user/create" }, method = RequestMethod.POST)
	public APIResult createUser(@RequestBody Map<String, BottestUser> users) {

		final String LOG_STRING = "POST /admin/user/create";
		LOGGER.info("Start " + LOG_STRING);
		LOGGER.info(LOG_STRING + " PARAMS users:" + new Gson().toJson(users, new TypeToken<Map<String, BottestUser>>() {
		}.getType()));
		APIResult result = new APIResult();
		BottestUser user = users.get("user");

		PasswordEncoder encoder = new BCryptPasswordEncoder();
		if (user != null) {
			user.setPassword(encoder.encode(user.getPassword()));
		}
		userRepo.save(user);
		user.setPassword("");

		result.setData(user);

		LOGGER.info("End POST /admin/user/create");
		return result;

	}


	//API EDAY
	/**
	 * @return
	 */
	@RequestMapping(value = { "/user/getAll" }, method = RequestMethod.GET)
	public APIResult getAllUser() {

		LOGGER.info("Start GET /admin/user/getAll");

		APIResult result = new APIResult();
		List<User> users = userRepository.findAll();
		result.setData(users);

		LOGGER.info("GET /admin/user/getAll RESULTS List users:"
				+ new Gson().toJson(users, new TypeToken<List<BottestUser>>() {
				}.getType()));

		LOGGER.info("End GET /admin/user/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/user/getAllCustomer" }, method = RequestMethod.GET)
	public APIResult getAllCustomer(@AuthenticationPrincipal UserDetails userDetails) {

		LOGGER.info("Start GET /admin/user/getAll");

		APIResult result = new APIResult();
		if(userDetails != null ) {
			User auth = userRepository.findByUserNameAndPassword(userDetails.getUsername(),userDetails.getPassword() );
			if(auth.getRole().equals("ROLE_ADMIN")) {
				List<User> users = userRepository.findAllCustomer();
				result.setData(users);
		
				LOGGER.info("GET /admin/user/getAll RESULTS List users:"
						+ new Gson().toJson(users, new TypeToken<List<BottestUser>>() {
						}.getType()));
			}else {
				result.setErrormessage("User don't permission!!");
			}
			
		}else {
	    	result.setErrormessage("403 Authencation!!");
	    }
		LOGGER.info("End GET /admin/user/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/user/update" }, method = RequestMethod.POST)
	public APIResult updateRoleUser(@RequestBody UpdateUserDTO user) {

		LOGGER.info("Start POST /admin/user/update");

		APIResult result = new APIResult();
		User theUser = userRepository.findById(user.getId());
		theUser.setRole(user.getRole());
		userRepository.save(theUser);
		Worker worker = new Worker();
		Worker check = workerRepo.findByUserId(theUser.getId());
		if(check== null) {
			LOGGER.info("End GET -------" + theUser.getRole());
			if(theUser.getRole().equals("ROLE_STAFF") || theUser.getRole().equals("ROLE_MANAGER")) {
				worker.setUserId(theUser.getId());
				worker.setCreateDay(new Date());
				workerRepo.save(worker);
				workerRepo.save(worker);
				worker = workerRepo.findByUserId(theUser.getId());
//				workerService.savePayRollWorker(worker);
				String month = this.getMonthToPayRoll();
				payRollService.createNewPayMonth(month, worker);
				
			}
		}
		
		result.setData(theUser);

		LOGGER.info("End POST /admin/user/update");
		return result;

	}

	
	@RequestMapping(value = { "/user/delete" }, method = RequestMethod.POST)
	public APIResult deleteUser(@RequestBody UpdateUserDTO user) {

		LOGGER.info("Start GET /admin/user/delete");

		APIResult result = new APIResult();
		User theUser = userRepository.findById(user.getId());
		theUser.setEnable(false);
		userRepository.save(theUser);
		Worker worker = workerRepo.findByUserId(user.getId());
		if(worker != null ) {
			List<PayRoll> listPayroll = payRollRepository.findAllPayRollByWorkerId(worker.getId());
			payRollRepository.deleteAll(listPayroll);
//			workerRepo.deleteById(worker.getId());
		}
		result.setData("");

		LOGGER.info("End GET /admin/user/delete");
		return result;

	}
	
	@RequestMapping(value = { "/detailUser/{userId}" }, method = RequestMethod.GET)
	public APIResult detailUser(@PathVariable("userId") Long userId ) {

		final String LOG_STRING = "GET /admin/detailUser";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		User theUser = userRepository.getUser(userId);
		result.setData(theUser);
		LOGGER.info("End GET /admin/detailUser");
		return result;

	}
	/**
	 * @param components
	 * @return
	 */
	@RequestMapping(value = { "/component/create" }, method = RequestMethod.POST)
	public APIResult createComponent(@RequestBody List<Component> components) {

		LOGGER.info("Start POST /admin/component/create");

		LOGGER.info("POST /admin/component/create PARAMS components:"
				+ new Gson().toJson(components, new TypeToken<List<Component>>() {
				}.getType()));
		APIResult result = new APIResult();
		for( int i = 0 ; i < components.size() ; i++ ) {
			if( components.get(i).isDefaultC()) {
				List<Component> componentList = componentService.findByGroupCode(components.get(i).getGroupCode());
				for (int j = 0; j < componentList.size(); j++) {
					Component component = componentList.get(j);
					component.setDefaultC(false);
					componentService.Update(component);
				}
			}
		}
		conponentService.save(components);
		result.setData(components);
		LOGGER.info("End POST /admin/component/create");
		return result;

	}

	/**
	 * @param components
	 * @return
	 */
	@RequestMapping(value = { "/component/delete" }, method = RequestMethod.POST)
	public APIResult createComponent(@RequestBody Component component) {

		LOGGER.info("Start POST /admin/component/delete");

		LOGGER.info("POST /admin/component/delete PARAMS component:"
				+ new Gson().toJson(component, new TypeToken<Component>() {
				}.getType()));
		APIResult result = new APIResult();
		conponentService.delete(component);
		result.setData(component);

		LOGGER.info("End POST /admin/component/delete");
		return result;

	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/component/getAll" }, method = RequestMethod.GET)
	public APIResult getALlComponent() {

		LOGGER.info("Start GET /admin/component/getAll");

		APIResult result = new APIResult();
		List<Component> components = conponentService.getAll();
		result.setData(components);

		LOGGER.info("GET /admin/component/getAll RESULTS components:"
				+ new Gson().toJson(components, new TypeToken<List<Component>>() {
				}.getType()));

		LOGGER.info("End GET /admin/component/getAll");
		return result;

	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/component/deleteAll" }, method = RequestMethod.POST)
	public APIResult deleteAllComponent() {

		LOGGER.info("Start POST /admin/component/deleteAll");

		APIResult result = new APIResult();
		List<Component> components = conponentService.deleteAll();
		result.setData(components);

		LOGGER.info("POST /admin/component/deleteAll RESULTS components:"
				+ new Gson().toJson(components, new TypeToken<List<Component>>() {
				}.getType()));

		LOGGER.info("End POST /admin/component/deleteAll");

		return result;

	}

	/**
	 * @param interfaces
	 * @return
	 */
	@RequestMapping(value = { "/interface/delete" }, method = RequestMethod.POST)
	public APIResult createInterface(@RequestBody InterfaceServer interfaces) {

		LOGGER.info("Start POST /admin/interface/delete");

		LOGGER.info("POST /admin/interface/delete PARAMS interfaces:"
				+ new Gson().toJson(interfaces, new TypeToken<InterfaceServer>() {
				}.getType()));

		APIResult result = new APIResult();
		interfaceSevice.delete(interfaces);
		result.setData(interfaces);

		LOGGER.info("End POST /admin/interface/delete");

		return result;

	}

	/**
	 * @param interfaces
	 * @return
	 */
	@RequestMapping(value = { "/interface/create" }, method = RequestMethod.POST)
	public APIResult createInterface(@RequestBody List<InterfaceServer> interfaces) {

		LOGGER.info("Start POST /admin/interface/create");

		LOGGER.info("POST /admin/interface/create PARAMS interfaces:"
				+ new Gson().toJson(interfaces, new TypeToken<List<InterfaceServer>>() {
				}.getType()));

		for (InterfaceServer item : interfaces) {
			InterfaceServer checkInterface = interfaceSevice.findByOsAndBrowserAndScmTypeAndNodeLabel(item.getOs(),
					item.getBrowser(), item.getScmType(), item.getNodeLabel());
			if (checkInterface != null) {
				APIResult result = new APIResult();
				result.setData(interfaces);
				result.setErrormessage("OS, Browser, Scm Type, Nodel Label already exist !!");
				LOGGER.info("End POST /admin/interface/create");
				return result;
			}
		}

		APIResult result = new APIResult();

		interfaceSevice.save(interfaces);
		result.setData(interfaces);

		LOGGER.info("End POST /admin/interface/create");
		return result;

	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/interface/getAll" }, method = RequestMethod.GET)
	public APIResult getALlInterface() {

		LOGGER.info("Start GET /admin/interface/getAll");

		APIResult result = new APIResult();
		List<InterfaceServer> interfaces = interfaceSevice.getAll();
		result.setData(interfaces);

		LOGGER.info("POST /admin/interface/getAll RESULTS List InterfaceServer:"
				+ new Gson().toJson(interfaces, new TypeToken<List<InterfaceServer>>() {
				}.getType()));

		LOGGER.info("End GET /admin/interface/getAll");
		return result;

	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/interface/deleteAll" }, method = RequestMethod.POST)
	public APIResult deleteAllInterface() {

		LOGGER.info("Start POST /admin/interface/deleteAll");

		APIResult result = new APIResult();
		List<InterfaceServer> interfaces = interfaceSevice.deleteAll();
		result.setData(interfaces);

		LOGGER.info("POST /admin/interface/deleteAll RESULTS interfaces:"
				+ new Gson().toJson(interfaces, new TypeToken<List<InterfaceServer>>() {
				}.getType()));

		LOGGER.info("End POST /admin/interface/deleteAll");
		return result;

	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/user" }, method = RequestMethod.GET)
	public ModelAndView getListUserProject() {

		LOGGER.info("Start GET /admin/user");

		ModelAndView mav = new ModelAndView("userManagement");

		LOGGER.info("End GET /admin/user");
		return mav;
	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/component" }, method = RequestMethod.GET)
	public ModelAndView getListComponentProject() {

		LOGGER.info("Start GET /admin/component");

		ModelAndView mav = new ModelAndView("componentManagement");

		LOGGER.info("End GET /admin/component");
		return mav;
	}

	@RequestMapping(value = { "/interface" }, method = RequestMethod.GET)
	public ModelAndView getListInterfacetProject() {

		LOGGER.info("Start GET /admin/interface");

		ModelAndView mav = new ModelAndView("interfaceManagement");

		LOGGER.info("End GET /admin/interface");
		return mav;
	}

	@RequestMapping(value = { "/software" }, method = RequestMethod.GET)
	public ModelAndView getListSoftware() {

		LOGGER.info("Start GET /admin/software");

		ModelAndView mav = new ModelAndView("softwareManagement");

		LOGGER.info("End GET /admin/software");
		return mav;
	}

	@ModelAttribute
	public void getRunModal(Model model, @AuthenticationPrincipal UserDetails userDetails) {

		LOGGER.info("Start FUNCTION getRunModal");

		LOGGER.info("POST FUNCTION getRunModal PARAMS Model:" + new Gson().toJson(model, new TypeToken<Model>() {
		}.getType()));

		LOGGER.info("POST FUNCTION getRunModal PARAMS UserDetails:"
				+ new Gson().toJson(userDetails, new TypeToken<UserDetails>() {
				}.getType()));

		List<Component> lsOs = componentService.findByGroupCode(ComponentService.OS_GROUP);
		List<Component> lsLocation = componentService.findByGroupCode(ComponentService.LOCATION_GROUP);
		List<Component> lsResolution = componentService.findByGroupCode(ComponentService.RESOLUTION_GROUP);
		List<Component> lsBrowser = componentService.findByGroupCode(ComponentService.BROWSER_GROUP);
		List<Component> lsDevice = componentService.findByGroupCode(ComponentService.DEVICE_GROUP);

		LOGGER.info(
				"POST FUNCTION getRunModal RESULTS lsOs:" + new Gson().toJson(lsOs, new TypeToken<List<Component>>() {
				}.getType()));
		LOGGER.info("POST FUNCTION getRunModal RESULTS lsLocation:"
				+ new Gson().toJson(lsLocation, new TypeToken<List<Component>>() {
				}.getType()));
		LOGGER.info("POST FUNCTION getRunModal RESULTS lsResolution:"
				+ new Gson().toJson(lsResolution, new TypeToken<List<Component>>() {
				}.getType()));
		LOGGER.info("POST FUNCTION getRunModal RESULTS lsBrowser:"
				+ new Gson().toJson(lsBrowser, new TypeToken<List<Component>>() {
				}.getType()));
		LOGGER.info("POST FUNCTION getRunModal RESULTS lsDevice:"
				+ new Gson().toJson(lsDevice, new TypeToken<List<Component>>() {
				}.getType()));

		model.addAttribute("lsOs", lsOs);
		model.addAttribute("lsLocation", lsLocation);
		model.addAttribute("lsResolution", lsResolution);
		model.addAttribute("lsBrowser", lsBrowser);
		model.addAttribute("lsDevice", lsDevice);
		model.addAttribute("userDetails", userDetails);

		LOGGER.info("End FUNCTION getRunModal");

	}

	@ModelAttribute
	public void getTypeByComponent(Model model, @AuthenticationPrincipal UserDetails userDetails) {

		LOGGER.info("Start FUNCTION getTypeByComponent");

		LOGGER.info("POST FUNCTION getTypeByComponent PARAMS Model:" + new Gson().toJson(model, new TypeToken<Model>() {
		}.getType()));

		LOGGER.info("POST FUNCTION getTypeByComponent PARAMS UserDetails:"
				+ new Gson().toJson(userDetails, new TypeToken<UserDetails>() {
				}.getType()));

		List<Component> lsBottestTool = componentService.findByGroupCode(ComponentService.BOTTESTTOOL_GROUP);

		LOGGER.info("POST FUNCTION getRunModal RESULTS lsBottestTool:"
				+ new Gson().toJson(lsBottestTool, new TypeToken<List<Component>>() {
				}.getType()));

		model.addAttribute("lsBottestTool", lsBottestTool);

		LOGGER.info("End FUNCTION getTypeByComponent");

	}

	/**
	 * @param softwares
	 * @return
	 */
	@RequestMapping(value = { "/software/create" }, method = RequestMethod.POST)
	public APIResult createSoftware(@RequestParam("fileName") List<MultipartFile> fileName,
			@RequestParam("typeSoftware") String typeSoftware,
			@RequestParam("versionSoftwares") String versionSoftwares, @RequestParam("upDate") String upDate,
			@RequestParam("requirement") String requirement, @RequestParam("releaseNote") String releaseNote,
			@RequestParam("idSoftware") Long id) {
			System.out.println("idcheck"+ id);
			LOGGER.info("Start POST /admin/software/create");
			APIResult result = new APIResult();
			try {
				Date upDateTime = new SimpleDateFormat("yyyy-MM-dd").parse(upDate);
				Date date = new Date(upDateTime.getTime() + 86400000);
				if(id == null) {
					String datas = null;
					for (MultipartFile file : fileName) {
						if (file != null && file.getSize() > 0) {
							if (file != null && file.getSize() > 0) {
								String filename = file.getOriginalFilename();
								if (filename.length() > 5) {
									filename = filename.substring(filename.length() - 5);
								} else if (filename.length() > 4) {
									filename = filename.substring(filename.length() - 4);
								}
								datas = softwareService.uploadSoftware(file, typeSoftware, versionSoftwares, date,
									    requirement, releaseNote);	
							}
						}
					}
					result.setData(datas);
					LOGGER.info(LOG_STRING + " RESULTS datas:" + datas);
				} else{
					softwareService.editSoftware(typeSoftware, versionSoftwares, date, requirement, releaseNote, id);
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				result.setErrormessage(e.getMessage());
				LOGGER.error(LOG_STRING + " ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
			return result;
	}

	/**
	 * @param softwares
	 * @return
	 */
	@RequestMapping(value = { "/software/getAllSoftware" }, method = RequestMethod.GET)
	public APIResult getAllSoftware() {

		LOGGER.info("Start POST /admin/software/getAllSoftware");
		List<Software> softwares = softwareService.getAll();
		APIResult result = new APIResult();
		result.setData(softwares);
		LOGGER.info("POST /admin/software/getAllSoftware PARAMS softwares:"
				+ new Gson().toJson(softwares, new TypeToken<List<Software>>() {
				}.getType()));
		LOGGER.info("End POST /admin/software/getAllSoftware");
		return result;
	}

	/**
	 * @param softwares
	 * @return
	 */
	@RequestMapping(value = { "/software/deleteSoftware" }, method = RequestMethod.POST)
	public APIResult deleteSoftware(@RequestBody Software software) {
		LOGGER.info("Start POST /admin/software/deleteSoftware");
		APIResult result = new APIResult();
		softwareService.deleteSoftware(software.getId(), software.getType(),software.getVersion());
		result.setData(software);
//			LOGGER.info("POST /admin/software/deleteSoftware PARAMS software:" + new Gson().toJson(software, new TypeToken<List<Software>>() {}.getType()));
		LOGGER.info("End POST /admin/software/deleteSoftware");
		return result;
	}
	
	@RequestMapping(value = { "/projectComponent/getAll" }, method = RequestMethod.GET)
	public APIResult getAllProjectComponent() {
	
		LOGGER.info("Start GET /admin/projectComponent/getAll");
	
		APIResult result = new APIResult();
		List<ProjectComponent> components = projectComponentService.findAll();
		result.setData(components);
	
		LOGGER.info("GET /admin/projectComponent/getAll RESULTS components:"
				+ new Gson().toJson(components, new TypeToken<List<Component>>() {
				}.getType()));
	
		LOGGER.info("End GET /admin/projectComponent/getAll");
		return result;
	
	}
	
	@RequestMapping(value = { "/getListProject" }, method = RequestMethod.GET)
	public APIResult getListProject() {
		APIResult result = new APIResult();
		List<Project> listProject = projectService.getAllProjectActive();
		result.setData(listProject);
		return result;
	
	}
	
	/**
	 * @param user
	 * @return
	 */
	@RequestMapping(value = { "/user/editPassword" }, method = RequestMethod.POST)
	public APIResult editPassword(@RequestBody UpdateUserDTO user) {
		
		LOGGER.info("Start POST /editPassword ");
		LOGGER.info("POST /editPassword PARAMS user:" + new Gson().toJson(user, new TypeToken<UpdateUserDTO>() {}.getType()));
		APIResult result = new APIResult();
		BottestUser theUser = userRepo.findByUsernameIgnoreCase(user.getUsername());
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		if (encoder.matches(user.getOldPassword(), theUser.getPassword())) {
			theUser.setPassword(passwordEncoder.encode(user.getPassword()));
			theUser.setConfirmPass(passwordEncoder.encode(user.getConfirmPass()));
			userRepo.save(theUser);
			result.setData(theUser);
		} else{
			result.setErrormessage("Incorrect password");
		}
		
		LOGGER.info("End POST /editPassword ");
		return result;
	}
	
	
	//API EDAY PRODUCT
	/**
	 * @param product
	 * @return
	 */
	@RequestMapping(value = { "/product/create" }, method = RequestMethod.POST)
	public APIResult createProduct(@RequestBody Product product, @AuthenticationPrincipal UserDetails userDetails) {

		final String LOG_STRING = "POST /admin/product/create";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		
		product.setAmount_sell((long) 0);
		Product productNew = productRepo.save(product);
		if(productNew != null) {
			productWareService.addProductToWareHouse(productNew, userDetails);
			result.setData(productNew);
		}

		LOGGER.info("End POST /admin/product/create");
		return result;

	}
	
	@RequestMapping(value = "/uploadImgProduct", method = RequestMethod.POST)
	public APIResult uploadImgProduct(@RequestParam("file") MultipartFile file) {
		
		final String LOG_STRING = "POST /uploadImgProduct";
		LOGGER.info("Start " + LOG_STRING);
		APIResult result = new APIResult();

		List<JSONObject> listJson = new ArrayList<JSONObject>();
		
		int count = 0;
			count += 1;
			if (file != null && file.getSize() > 0) {
					if (file != null && file.getSize() > 0) {
						String filename = file.getOriginalFilename();
						
						String fileNameWithoutExtension = filename.replaceFirst("[.][^.]+$", "");
						fileNameWithoutExtension = fileNameWithoutExtension.replaceAll("[^a-zA-Z0-9 ]", "");
						
						String contentType = FilenameUtils.getExtension(filename);
						
						String idName;
							
						JSONObject json = new JSONObject();
						try {
							Date date = new Date();  
			                DateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");  
			                String strDate = dateFormat.format(date);  
			                
							idName = filename+"_" + strDate;
							fileService.uploadImgProduct(file,idName, contentType);
							json.put("Id", idName);
							json.put("Name", filename);
							json.put("Type", file.getContentType());
							json.put("File", idName+"."+contentType);
							listJson.add(json);
						} catch (JSONException e) {
							LOGGER.error(LOG_STRING + " ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
						}
						
					}
			}
		
		result.setData(listJson.toString());

		LOGGER.info(LOG_STRING + " RESULTS result:" + new Gson().toJson(result.getData(), new TypeToken<Object >() {}.getType()));
		LOGGER.info("End " + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = { "/product/update" }, method = RequestMethod.POST)
	public APIResult updateProduct(@RequestBody ProductDTO product, HttpServletRequest request) {
		
		LOGGER.info("Start POST /cat/update ");
		APIResult result = new APIResult();
		Product theProduct = productRepo.findByProductId(product.getId());
		theProduct.setName(product.getName());
		theProduct.setAmount(product.getAmount());
		theProduct.setCatId(product.getCatId());
		theProduct.setInputDay(product.getInputDay());
		theProduct.setPrice_buy(product.getPrice_buy());
		theProduct.setPrice_sell(product.getPrice_sell());
		theProduct.setSize(product.getSize());
		theProduct.setImage(product.getImage());
		Product productUpdate = productRepo.save(theProduct);
		
		ProductWareHouse productWareHouse = productWareRepo.findByProductId(productUpdate.getId());
		productWareHouse.setAmount(productUpdate.getAmount());
		productWareHouse.setTotalMoneyBuy(productUpdate.getAmount() * productUpdate.getPrice_buy());
		productWareRepo.save(productWareHouse);
		
		result.setData(theProduct);
		
		LOGGER.info("End POST /product/update ");
		return result;
	}
	@RequestMapping(value = { "/{productId}/deleted" }, method = RequestMethod.DELETE)
	public APIResult deleteProduct(Principal principal,@PathVariable(name = "productId") String productId) {
		
		final String LOG_STRING = "POST /admin/"+ productId + "/deleted";
		LOGGER.info("Start " + LOG_STRING);
		APIResult result = new APIResult();
		if(productId != null && Long.valueOf(productId) > 0) {
			Product product = productRepo.findByProductId(Long.valueOf(productId));
			if(product != null) {
				List<Order> listOrder = orderRepo.findByProductId(Long.valueOf(productId));
				if(listOrder != null) {
					for (Order order : listOrder) {
						if(order.getState() != 2) {
							orderRepo.delete(order);
						}
					}
				}
				ProductWareHouse productWareHouse  = productWareRepo.findByProductId(Long.valueOf(productId));
				if(productWareHouse != null) {
					productWareRepo.delete(productWareHouse);
				}
				productRepo.delete(product);
			}else {
				result.setErrormessage("Product don't exist!!");
			}
			
		}else {
			result.setErrormessage("Invalid productId!!");
		}
		
		
		LOGGER.info("End " + LOG_STRING);
		return result;

	}
	//API EDAY CATEGORY
		/**
		 * @param product
		 * @return
		 */
		@RequestMapping(value = { "/cat/create" }, method = RequestMethod.POST)
		public APIResult createCat(@RequestBody Category cat) {

			final String LOG_STRING = "POST /admin/cat/create";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			
			catRepo.save(cat);
			
			result.setData(cat);

			LOGGER.info("End POST /admin/cat/create");
			return result;

		}
		@RequestMapping(value = { "/cat/list" }, method = RequestMethod.GET)
		public APIResult listCat(@AuthenticationPrincipal UserDetails userDetails) {

			final String LOG_STRING = "GET /admin/cat/list";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			List<Category> cats = categoryService.findAll();
			result.setData(cats);

			LOGGER.info("GET /admin/cat/list");
			return result;

		}
		@RequestMapping(value = { "/cat/{catId}/detail" }, method = RequestMethod.GET)
		public APIResult detailCategory(@PathVariable("catId") Long catId, HttpServletRequest request) {
			
			LOGGER.info("Start POST /cat/{catIdId}/detail ");
			APIResult result = new APIResult();
			Category theCat = catRepo.findByCatId(catId);
			
			result.setData(theCat);
			
			LOGGER.info("End POST /cat/{catIdId}/detail ");
			return result;
		}
		
		@RequestMapping(value = { "/cat/update" }, method = RequestMethod.POST)
		public APIResult updateCategory(@RequestBody CatDTO cat, HttpServletRequest request) {
			
			LOGGER.info("Start POST /cat/update ");
			APIResult result = new APIResult();
			Category theCat = catRepo.findByCatId((long) cat.getId());
			theCat.setName(cat.getName());
			catRepo.save(theCat);
			result.setData(theCat);
			
			LOGGER.info("End POST /cat/{catIdId}/detail ");
			return result;
		}
		
		@RequestMapping(value = { "/cat/{catId}/deleted" }, method = RequestMethod.DELETE)
		public APIResult deleteCat(Principal principal,@PathVariable(name = "catId") String catId) {
			
			final String LOG_STRING = "POST /admin/"+ catId + "/deleted";
			LOGGER.info("Start " + LOG_STRING);
			APIResult result = new APIResult();
			if(catId!= null && Integer.parseInt(catId) > 0) {
				
				
					Category theCat = catRepo.findByCatId(Long.valueOf(catId));
					if(theCat != null) {
						List<Product> listProduct = productRepo.getListProductByCatId(Long.valueOf(catId));
						if(listProduct.size() == 0) {
							catRepo.delete(theCat);
							List<Category> cats = categoryService.findAll();
							result.setData(cats);
						}else {
							result.setErrormessage("Catalog contains products . Cannot perform delete !!");
						}
					}else {
						result.setErrormessage("Don't find category!!");
					}
					
				
				
			}else {
				result.setErrormessage("Please check catId!!");
			}
			
			
			LOGGER.info("End " + LOG_STRING);
			return result;

		}
		
		@RequestMapping(value = { "/order/getAll" }, method = RequestMethod.GET)
		public APIResult listOrder( ) {

			final String LOG_STRING = "POST /order/getAll";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			List<Order> orders = orderRepo.findAll();
			List<OrderDTO> listOrderDTO = new ArrayList<OrderDTO>();
			if(orders != null) {
				for (Order order : orders) {
					OrderDTO orderDTO = new OrderDTO();
					orderDTO.setId(order.getId());
					User user = userRepository.findById(order.getUser_id());
					orderDTO.setUser(user);
					Product product = productRepo.findByProductId(order.getProId());
					orderDTO.setProduct(product);
					Payment payment = paymentRepo.getPaymentByOrderId(order.getId());
					orderDTO.setPayment(payment);
					orderDTO.setAmount(order.getAmount());
					orderDTO.setPrice(order.getPrice());
					if(order.getState() == 0) {
						orderDTO.setStatus("Add cart");
					}else {
						if(order.getState() == 1) {
							orderDTO.setStatus("Done payment");
						}else {
							if(order.getState() == 2) {
								orderDTO.setStatus("Done buy");
							}
						}
					}
					listOrderDTO.add(orderDTO);
				}
				result.setData(listOrderDTO);
			}else {
				result.setData(orders);
			}
			LOGGER.info("End POST /order/getAll");
			return result;

		}
		
		@RequestMapping(value = { "/order/{orderId}/delete" }, method = RequestMethod.DELETE)
		public APIResult deleteOrder(@PathVariable("orderId") int orderId ) {

			final String LOG_STRING = "POST /admin/order/" + orderId + "/delete";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			if(orderId > 0) {
				Order order = orderRepo.findByOrderId(orderId);
				if(order != null ) {
					if(order.getState() == 2) {
						orderRepo.deleteById((long) orderId);
						result.setData(order);
					}else {
						result.setErrormessage("The application isn't completed. You can't delete it");
					}
					
				}else {
					result.setData(order);
					result.setErrormessage("No orders exist!");
				}
			}else {
				result.setErrormessage("Order id must be greater than 0 !");
			}
			LOGGER.info("End GET" + LOG_STRING );
			return result;

		}
		//api staff
		
		
		@RequestMapping(value = { "/detailStaff/{userId}" }, method = RequestMethod.GET)
		public APIResult detailStaff(@PathVariable("userId") Long userId ) {

			final String LOG_STRING = "GET /admin/listStaff";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			Worker staff = workerRepo.getStaff(userId);
			result.setData(staff);
			LOGGER.info("End GET /admin/listStaff");
			return result;

		}
		
		@RequestMapping(value = { "/staff/update" }, method = RequestMethod.POST)
		public APIResult updateStaff(@RequestBody StaffDTO staffDTO ) {

			final String LOG_STRING = "GET /admin/staff/update";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			Worker staff = workerRepo.getStaffById(staffDTO.getUserId());
			staff.setBranch_card(staffDTO.getBranch_card());
			staff.setCard_number(staffDTO.getCard_number());
			staff.setExperience(staffDTO.getExperience());
			staff.setUnit_salary(staffDTO.getUnit_salary());
			workerRepo.save(staff);
			payRollRepository.updatePayRollBasedOnMonthAndWorkerId(staffDTO.getUnit_salary(), staffDTO.getUnit_salary(), this.getMonthToPayRoll(), staff.getId());
			result.setData(staff);
			LOGGER.info("End GET /admin/staff/update");
			return result;

		}
		
		@RequestMapping(value = "/{workerId}/dayOffOrdayWork/update", method = RequestMethod.POST)
		public APIResult updateDayOffOrDayWorkOfWorker(@AuthenticationPrincipal UserDetails userDetails,
				@RequestBody PayRollDTO payRollDTO, @PathVariable("workerId") String workerId) {
			final String LOG_STRING = "POST /api/v1/worker/" + workerId + "/dayOffOrdayWork/update";
			LOGGER.info("START " + LOG_STRING);
			LOGGER.info(LOG_STRING + "PARAMS userDetails:" + userDetails);
			LOGGER.info(LOG_STRING + "PARAMS payRollDTO:" + payRollDTO);
			LOGGER.info(LOG_STRING + "PARAMS workerId:" + workerId);

			APIResult result = new APIResult();
			try {
				
				WorkerDTO workerDTO = workerService.findWorkerById(Long.valueOf(workerId));
				if (workerDTO == null) {
					result.setErrormessage("Infor worker doesn't exist !");
					return result;
				}
				workerService.updateDayOffOrDayWorkWorker(workerId, payRollDTO);
				PayRoll payRoll = payRollRepository.findPayRollWorkerUnpaidByMonthAndWorkerId(payRollDTO.getMonth(), Long.valueOf(workerId));
				result.setData(payRoll);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			LOGGER.info("END" + LOG_STRING);
			return result;
		}
		
		@RequestMapping(value = "/callPayroll", method = RequestMethod.POST)
		public APIResult callPayroll(@AuthenticationPrincipal UserDetails userDetails) {
			final String LOG_STRING = "POST /callPayroll" ;
			LOGGER.info("START " + LOG_STRING);
			APIResult result = new APIResult();
			List<Worker> listWorkerEntities = workerRepo.findAll();
			for(Worker worker : listWorkerEntities) {
				String month = this.getMonthToPayRoll();
				PayRoll payRoll = payRollRepository.findPayRollWorkerByMonthAndWorkerId(month, worker.getId());
				if(payRoll != null) {
					Long totalSalary = workerService.setSalaryWorker(payRoll, worker);
					payRoll.setTotalSalary(totalSalary);
					payRollRepository.save(payRoll);
					
				}
			}
			List<PayRoll> listPayRoll = payRollRepository.findAll();
			result.setData(listPayRoll);
			LOGGER.info("END" + LOG_STRING);
			return result;
		}
		
		@RequestMapping(value = "/callDelete", method = RequestMethod.POST)
		public APIResult callDelete(@AuthenticationPrincipal UserDetails userDetails) {
			final String LOG_STRING = "POST /callPayroll" ;
			LOGGER.info("START " + LOG_STRING);
			APIResult result = new APIResult();
			List<PayRoll> listPayRoll = payRollRepository.findAll();
			payRollRepository.deleteAll();
			List<Worker> listWorkerEntities = workerRepo.findAll();
			workerRepo.deleteAll();
			
			result.setData(listPayRoll);
			LOGGER.info("END" + LOG_STRING);
			return result;
		}
		
		public String getMonthToPayRoll() {
			LOGGER.info("Begin Service WorkerService Function getFirstDayOfMonthToPayRoll");
			
	        Date date = new Date();
	        try {
	            String month = DateUtil.convertDateToFormaterString(date);
	            if(!StringUtils.isEmpty(month)) {
	            	return month;
	            }
	        } catch (Exception ex) {
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
	        LOGGER.info("End Service WorkerService Function getFirstDayOfMonthToPayRoll");
	        return "";
		}
		
		@RequestMapping(value = { "/staff/filter" }, method = RequestMethod.POST)
		public APIResult filterStaff(Principal principal, @RequestBody FilterUserDTO filterStaff,HttpSession session,
				@AuthenticationPrincipal UserDetails userDetails) {

			APIResult result = new APIResult();
			Gson gson = new Gson();
			Type listType = new TypeToken<FilterUserDTO>() {
			}.getType();
			String filStr = gson.toJson(filterStaff, listType);
			session.setAttribute("filterStaff", filStr);
			List<Map<String, Object>> staffList = userCustome.filterStaff(filterStaff, 1);
			if(staffList != null && staffList.size() > 0) {
				result.setData(staffList);
			}else {
				result.setData("No data available");
			}
			
			return result;
		}
		
		@RequestMapping(value = { "/user/filter" }, method = RequestMethod.POST)
		public APIResult filterCustomer(Principal principal, @RequestBody FilterUserDTO filterCustomer,HttpSession session,
				@AuthenticationPrincipal UserDetails userDetails) {

			APIResult result = new APIResult();
			Gson gson = new Gson();
			Type listType = new TypeToken<FilterUserDTO>() {
			}.getType();
			String filStr = gson.toJson(filterCustomer, listType);
			session.setAttribute("filterStaff", filStr);
			List<Map<String, Object>> staffList = userCustome.filterStaff(filterCustomer, 2);
			if(staffList != null && staffList.size() > 0) {
				result.setData(staffList);
			}else {
				result.setData("No data available");
			}
			
			return result;
		}
}

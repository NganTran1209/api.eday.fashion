package io.bottest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.bottest.dto.APIResult;
import io.bottest.dto.FilterQuyTrinhDTO;
import io.bottest.dto.QuyTrinhDTO;
import io.bottest.service.QuyTrinhService;
@Controller
@RestController
@RequestMapping("/api/v1/quytrinh")
public class FarmatePublicProcess {
	private final Logger LOGGER = LogManager.getLogger(FarmatePublicProcess.class);
	@Autowired
	QuyTrinhService quyTrinhService;
	
	@RequestMapping(value = {"/"}, method = RequestMethod.POST)
	public APIResult findPublicProject(@RequestBody FilterQuyTrinhDTO filterQuyTrinh) {
		APIResult result = new APIResult();
		List<QuyTrinhDTO> quyTrinhList = quyTrinhService.getAllPublicProject();
		result.setData(quyTrinhList);
		
		return result;
	}
		
}
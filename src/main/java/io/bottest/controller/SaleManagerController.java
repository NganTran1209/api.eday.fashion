package io.bottest.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.bottest.controller.FarmatePublicProcess;
import io.bottest.dto.APIResult;
import io.bottest.dto.BrandDTO;
import io.bottest.dto.SellManagerDTO;
import io.bottest.service.OrderService;
import io.bottest.service.ProductService;

@Controller
@RestController
@RequestMapping("/api/v1/sale")
public class SaleManagerController {
	
	private final Logger LOGGER = LogManager.getLogger(FarmatePublicProcess.class);
	
	@Autowired
	ProductService productServive;
	
	@Autowired
	OrderService orderService;
	
	//dang ban nong san
	@RequestMapping(value= {"/{productCode}"}, method= RequestMethod.POST)
	public APIResult saleProduct(@AuthenticationPrincipal UserDetails userDetails, @RequestBody SellManagerDTO sellDTO, @PathVariable(name = "productCode") String productCode) throws Exception {
		APIResult result = new APIResult();
		BrandDTO brand = productServive.findByBrandName(userDetails);
		SellManagerDTO sell = new SellManagerDTO();
		if(sellDTO != null) {
			sell = orderService.sellProduct(sellDTO, brand, productCode, userDetails);
		}
		result.setData(sell);
		return result;
	}

}

package io.bottest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.lang.reflect.Type;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.bottest.dto.APIResult;
import io.bottest.dto.FilterSupplieDTO;
import io.bottest.dto.KhoNongSanDTO;
import io.bottest.dto.MaterialDTO;
import io.bottest.dto.PostDTO;
import io.bottest.dto.RequestOTP;
import io.bottest.dto.ShipmentAddressDTO;
import io.bottest.dto.SignupDTO;
import io.bottest.dto.UpdateUserDTO;
import io.bottest.dto.UserDTO;
import io.bottest.jpa.entity.ActiveOTP;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.CostRevenueOther;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.service.ActiveOPTService;
import io.bottest.service.CostRevenueOtherService;
import io.bottest.service.KhoNongSanService;
import io.bottest.service.MaterialService;
import io.bottest.service.OrderService;
import io.bottest.service.PostService;
//import io.bottest.service.SendMailService;
import io.bottest.service.SoftwareService;
import io.bottest.service.UserService;

@Controller
@RestController
@RequestMapping("/userAu")
public class UserController {

	private final Logger LOGGER = LogManager.getLogger(UserController.class);

	@Autowired
	BottestUserRepository bottestUserRepository;

	@Autowired
	SoftwareService softwareService;

	@Autowired
	BottestUserRepository userRepo;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MaterialService materialService;

	@Autowired
	private PostService postService;

	@Autowired
	KhoNongSanService khoService;

	@Autowired
	private UserService userService;

//	@Autowired
//	private SendMailService sendMailService;

	@Autowired
	CostRevenueOtherService costRevenueOtherService;

	@Autowired
	OrderService orderService;

	@Autowired
	private ActiveOPTService activeOPTService;

	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = { "/editProfile" }, method = RequestMethod.POST)
	public APIResult editProfile(@RequestBody UpdateUserDTO user, @AuthenticationPrincipal UserDetails userDetails, HttpServletRequest request) {
		
		LOGGER.info("Start POST /editProfile ");
		LOGGER.info("POST /editProfile PARAMS user:" + new Gson().toJson(user, new TypeToken<UpdateUserDTO>() {}.getType()));
		LOGGER.info("POST /editProfile PARAMS userDetails:" + new Gson().toJson(userDetails, new TypeToken<UserDetails>() {}.getType()));
		APIResult result = new APIResult();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		if(userDetails != null) {
			User theUser = userRepository.findByUsernameIgnoreCase(userDetails.getUsername());
			theUser.setAddr(user.getAddr());
			theUser.setBirthDay(user.getBirthDay());
			theUser.setFirst_name(user.getFirstname());
			theUser.setLast_name(user.getLastname());
			theUser.setPhone(user.getPhone());
			theUser.setSex(user.isSex());
			theUser.setUsername(user.getUsername());
			userRepository.save(theUser);
			UserDTO userReturn = new UserDTO();
			userReturn.setId(theUser.getId());
			userReturn.setAddr(theUser.getAddr());
			if(theUser.getBirthDay().toString() != null) {
				userReturn.setBirthDay(df.format(theUser.getBirthDay()));
			}
			userReturn.setEmail(theUser.getEmail());
			userReturn.setFirst_name(theUser.getFirst_name());
			userReturn.setLast_name(theUser.getLast_name());
			userReturn.setPassword(theUser.getPassword());
			userReturn.setPhone(theUser.getPhone());
			userReturn.setRole(theUser.getRole());
			userReturn.setSex(theUser.isSex());
			userReturn.setUsername(theUser.getUsername());
			result.setData(userReturn);
		}else {
			result.setErrormessage("403 Authencation!!");
		}

		
		LOGGER.info("End POST /editProfile ");
		return result;
	}
	
	
	

}
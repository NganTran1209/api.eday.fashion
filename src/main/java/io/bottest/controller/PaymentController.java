package io.bottest.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.bottest.dto.APIResult;
import io.bottest.jpa.entity.Order;
import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.OrderPoductRepository;
import io.bottest.jpa.respository.OrderRepository;
import io.bottest.jpa.respository.PaymentRepository;
import io.bottest.jpa.respository.UserRepository;

@Controller
@RestController
@RequestMapping("/payment")
public class PaymentController {
	private static final String LOG_STRING = null;

	private final Logger LOGGER = LogManager.getLogger(PaymentController.class);
	
	@Autowired
	PaymentRepository paymentRepo;
	
	@Autowired
	OrderPoductRepository orderRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@RequestMapping(value = { "/add" }, method = RequestMethod.POST)
	public APIResult addPayment(@RequestBody Payment payment, @AuthenticationPrincipal UserDetails userDetails ) {

		final String LOG_STRING = "POST /payment/add";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		if(userDetails != null) {
			if(payment.getOrderId() > 0) {
				User owner = userRepo.findByUserNameAndPassword(userDetails.getUsername(), userDetails.getPassword());
				payment.setUser_id(owner.getId());
				Order order = orderRepo.findByOrderId(payment.getOrderId());
				if(order != null && order.getState() == 0) {
					payment.setAmount(order.getPrice());
					paymentRepo.save(payment);
					order.setState(1);
					orderRepo.save(order);
					result.setData(payment);
				}else {
					result.setErrormessage("Please check order or status payment of order");
				}
				
			}else {
				result.setErrormessage("Invalid orderId ");
			}
		}else {
			result.setErrormessage("You need login !");
		}	

		LOGGER.info("End POST /payment/add");
		return result;

	}
}

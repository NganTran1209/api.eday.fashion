package io.bottest.controller;

import static org.springframework.http.ResponseEntity.ok;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import io.bottest.configure.security.jwt.JwtTokenProvider;
import io.bottest.dto.APIResult;
import io.bottest.dto.RequestRestAPI;
import io.bottest.dto.ResponseRestAPI;
import io.bottest.service.CustomUserDetailsService;
import io.bottest.service.FileService;
import io.bottest.utils.AWSV4Auth;
import io.bottest.utils.HttpDeleteWithEntity;
import io.bottest.utils.HttpGetWithEntity;

@RestController
@RequestMapping(value="/rest")
public class RestAPIController {

	private final Logger LOGGER = LogManager.getLogger(RestAPIController.class);
	
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    CustomUserDetailsService users;
    
    @Value("${resource.path}")
	private String resourcePath;
    
    /**
     * rest API like postman
     * @param data
     * @return
     */
    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity restAPI(@RequestBody RequestRestAPI data) {
    	
    	// Get data from UI
    	Map<String, Object> params = (Map<String, Object>) data.getData();
    	List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("Params");
    	String method = (String) list.get(0).get("Value");
    	String url = (String) list.get(1).get("Value");
    	List<Map<String, Object>> queryParams = (List<Map<String, Object>>) list.get(2).get("Value");
    	Map<String, Object> authorization = (Map<String, Object>) list.get(3).get("Value");
    	List<Map<String, String>> headers =   (List<Map<String, String>>) list.get(4).get("Value");
    	Map<String, Object> body = (Map<String, Object>) list.get(5).get("Value");
    	
    	if (url.isEmpty()) {
    		return errorMessage("Please check your URI");
    	}
    	if (method == null) {
    		return errorMessage("Please check your method");
    	}
    	
    	// Add query param into url
    	String urlWithParams = urlWithParams(url, queryParams);
    	
    	// Setting body
    	HttpEntity httpEntity = null;
    	
    	// Setting method, url and header
    	HttpEntityEnclosingRequestBase request = null;
    	if ("get".equalsIgnoreCase(method)) {
    		try {
				request = getRest(urlWithParams, headers);
			} catch (URISyntaxException e) {
				LOGGER.error("End POST /rest ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		        return errorMessage("URI have something wrong !!");
			}
    	} else if ("post".equalsIgnoreCase(method)) {
    		request = postRest(urlWithParams, headers);
    	} else if ("put".equalsIgnoreCase(method)) {
    		request = putRest(urlWithParams, headers);
    	}  else if ("delete".equalsIgnoreCase(method)) {
    		try {
				request = deleteRest(urlWithParams, headers);
			} catch (URISyntaxException e) {
				LOGGER.error("End POST /rest ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		        return errorMessage("URI have something wrong !!");
			}
    	}
    	
    	// Setting body
    	if ("raw".equalsIgnoreCase(body.get("Type").toString())) {
	  	    if (body.get("Value") != null) {
	  	    	ObjectMapper objectMapper = new ObjectMapper();
	  	    	String json = null;
				try {
					json = objectMapper.writeValueAsString(body.get("Value"));
				} catch (JsonProcessingException e) {
					LOGGER.error("End POST /rest ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
				}
	  	    	httpEntity = jsonRest(json);
	  	    	request.addHeader("Accept", "application/json");
	  	    	request.addHeader("Content-type", "application/json");
	  	    }
    	} else if ("form-data".equalsIgnoreCase(body.get("Type").toString())) {
	  	    if (body.get("Value") != null) {
	  	    	httpEntity = formDataRest((Map<String, String>) body.get("Value"), data.getPath());
	  	    }
    	}
    	
    	// Setting Authorization
    	if ("none".equalsIgnoreCase(authorization.get("Type").toString())) {
    		
    	} else if ("bearerToken".equalsIgnoreCase(authorization.get("Type").toString())) {
    		Map<String, String> bearerTokenMap = (Map<String, String>) authorization.get("Value");
    		request.addHeader("Authorization", "Bearer " + bearerTokenMap.get("token"));
    	} else if ("basicAuth".equalsIgnoreCase(authorization.get("Type").toString())) {
    		Map<String, String> basicAuthMap = (Map<String, String>) authorization.get("Value");
    		
    		String username = basicAuthMap.get("username");
    		String password = basicAuthMap.get("password");
    		String bsicStr = username + ":" + password;
    		request.addHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString(bsicStr.getBytes()));
    	} else if ("awsSignature".equalsIgnoreCase(authorization.get("Type").toString())) {
    		Map<String, String> awsSignature = (Map<String, String>) authorization.get("Value");
    		try {
				String accessKey = String.valueOf(awsSignature.get("AccessKey"));
				String secretKey = String.valueOf(awsSignature.get("SecretKey"));
				String AWSRegion = String.valueOf(awsSignature.get("AWSRegion"));
				String serviceName = String.valueOf(awsSignature.get("ServiceName"));
				String sessionToken = String.valueOf(awsSignature.get("SessionToken"));
				
				String region = AWSRegion.isEmpty()?"us-east-1":AWSRegion;
				String service = serviceName.isEmpty()?"execute-api":serviceName;
				
				String[] hostAws = urlWithParams.split("/");

		        /**
		         * Add host without http or https protocol.
		         * You can also add other parameters based on your amazon service requirement.
		         */
		        TreeMap<String, String> awsHeaders = new TreeMap<String, String>();
		        awsHeaders.put("host", hostAws[2]);
		        String canonicalURI = "";
		        for (int i = 3 ; i <= hostAws.length - 1; i++) {
		        	canonicalURI += "/" + hostAws[i];
		        }
		        
		        AWSV4Auth aWSV4Auth = new AWSV4Auth.Builder(accessKey, secretKey)
		                                           .regionName(region)
		                                           .serviceName(service) // es - elastic search. use your service name
		                                           .httpMethodName(method.toUpperCase()) //GET, PUT, POST, DELETE, etc...
		                                           .canonicalURI(canonicalURI) //end point
		                                           .queryParametes(null) //query parameters if any
		                                           .awsHeaders(awsHeaders) //aws header parameters
		                                           .payload(null) // payload if any
		                                           .debug() // turn on the debug mode
		                                           .build();

		        
		        Map<String, String> header = aWSV4Auth.getHeaders();
		        for (Map.Entry<String, String> entrySet : header.entrySet()) {
		            String key = entrySet.getKey();
		            String value = entrySet.getValue();
		            request.addHeader(key, value);
		        }
		        request.addHeader("X-Amz-Content-Sha256","e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
			} catch (Exception e) {
				LOGGER.error("End POST /rest ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
    	} else if ("apiKey".equalsIgnoreCase(authorization.get("Type").toString())) {
    		Map<String, String> apiKeyMap = (Map<String, String>) authorization.get("Value");
    		if ("header".equalsIgnoreCase(apiKeyMap.get("addTo"))) {
    			request.addHeader(apiKeyMap.get("key"), apiKeyMap.get("value"));
    		} else if ("queryParams".equalsIgnoreCase(apiKeyMap.get("addTo"))) {
    			StringBuilder apiKeyString = new StringBuilder(urlWithParams);
    			boolean isExistParam = apiKeyString.indexOf("?") == -1 ? false: true;
      		    if (isExistParam) {
      		    	apiKeyString.append("&" + apiKeyMap.get("key") + "=" + apiKeyMap.get("value"));
                } else {
                	apiKeyString.append("?" + apiKeyMap.get("key") + "=" + apiKeyMap.get("value"));
                }
      		    try {
					request.setURI(new URI(apiKeyString.toString()));
				} catch (URISyntaxException e) {
					return errorMessage("URI wrong !!");
				}
    		}
    	}
    	
    	request.setEntity(httpEntity);

    	ResponseRestAPI reponseAPI = new ResponseRestAPI();
    	try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(request)) {

    		// API response
    		if (response.getEntity() != null) {
    			reponseAPI.setResponseBody(EntityUtils.toString(response.getEntity(), "utf-8"));
    		} else {
    			reponseAPI.setResponseBody("");
    		}
    		
    		JsonObject jsontemp = new JsonObject();
    		
    		for (Header header : response.getAllHeaders()) {
    			jsontemp.addProperty(header.getName(), header.getValue().toString());
    		}
    		reponseAPI.setResponseHeaders(jsontemp.toString());
    		reponseAPI.setStatusCode(String.valueOf(response.getStatusLine().getStatusCode()));
    	
        } catch (ParseException e) {
        	LOGGER.error("End POST /rest ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		} catch (IOException e) {
			LOGGER.error("End POST /rest ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}

    	
    	APIResult result = new APIResult();
    	result.setData(reponseAPI);
        return ok(result);
    }
    
    /**
     * Error message when occurs exception
     * @param string
     * @return
     */
	private ResponseEntity errorMessage(String string) {
		ResponseRestAPI reponseAPI = new ResponseRestAPI();
		reponseAPI.setResponseBody(string);
		reponseAPI.setStatusCode("500");
		APIResult result = new APIResult();
		result.setData(reponseAPI);
	    return ok(result);
	 }

	/**
	 * Add query param into url
	 * @param url
	 * @param params
	 * @return
	 */
	private String urlWithParams(String url, List<Map<String, Object>> params) {
		StringBuilder urlWithParams = new StringBuilder(url);
		 
		for (Map<String, Object> param: params) {
			if (!(param == null) && !param.isEmpty()) {
				boolean isExistParam = (urlWithParams.indexOf("?") == -1) ? false: true;
	  		    if (isExistParam) {
	  			    urlWithParams.append("&" + param.get("Key") + "=" + param.get("Value"));
	            } else {
	        	    urlWithParams.append("?" + param.get("Key") + "=" + param.get("Value"));
	            }
			}
		}
		
		return urlWithParams.toString();
	}
    
    /**
     * GET METHOD
     * @param url
     * @param hearder
     * @return
     * @throws URISyntaxException 
     */
    private HttpEntityEnclosingRequestBase getRest(String url, List<Map<String, String>> headers) throws URISyntaxException {
    	
    	HttpGetWithEntity request = new HttpGetWithEntity();
		URI uri = new URI(url);
		request.setURI(uri);
	
		for (Map<String, String> header : headers) {
	        request.setHeader(header.get("Key").toString(), header.get("Value").toString().toString());
		}
    	
    	return request;
    }
    
    /**
     * POST METHOD
     * @param url
     * @param hearder
     * @return
     */
    private HttpEntityEnclosingRequestBase postRest(String url, List<Map<String, String>> headers) {
    	
    	HttpPost request = new HttpPost(url);
    	
    	for (Map<String, String> header : headers) {
	        request.setHeader(header.get("Key").toString(), header.get("Value").toString());
		}
    	
    	return request;
    }
    
    /**
     * PUT METHOD
     * @param url
     * @param hearder
     * @return
     */
    private HttpEntityEnclosingRequestBase putRest(String url, List<Map<String, String>> headers) {
    	HttpPut request = new HttpPut(url);
    	
    	for (Map<String, String> header : headers) {
	        request.setHeader(header.get("Key").toString(), header.get("Value").toString());
		}
    	
    	return request;
    }
    
    /**
     * DELETE METHOD
     * @param url
     * @param hearder
     * @return
     */
    private HttpEntityEnclosingRequestBase deleteRest(String url, List<Map<String, String>> headers) throws URISyntaxException {
    	HttpDeleteWithEntity request = new HttpDeleteWithEntity();

		request.setURI(new URI(url));
	
		for (Map<String, String> header : headers) {
	        request.setHeader(header.get("Key").toString(), header.get("Value").toString());
		}
    	
    	return request;
    }
    
    /**
     * FORM DATA BODY
     * @param url
     * @param hearder
     * @return
     */
    private HttpEntity formDataRest(Map<String, String> map, String pathDirectories) {
    	MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    	// add request parameters or form parameters
    	for (Entry me : map.entrySet()) {
        	if ("text".equalsIgnoreCase(map.get("Type"))) {
        		builder.addTextBody(map.get("Key"), map.get("Value"), ContentType.TEXT_PLAIN);
        	} else if ("file".equalsIgnoreCase(map.get("Type"))) {
        		Path path = Paths.get(this.resourcePath, pathDirectories.replace("@", "."), FileService.TESTDATAS_DIR_NAME, map.get("Value"));
        		File f = path.toFile();
        	    builder.addBinaryBody(map.get("Key"), f, ContentType.DEFAULT_BINARY, f.getName());
        	}
        }
        HttpEntity multipart = builder.build();
        
        return multipart;
        
    }
    
    /**
     * JSON BODY
     * @param body
     * @return
     */
    private StringEntity jsonRest(String body) {
    	StringEntity stringbody = null;
		stringbody = new StringEntity(body, "UTF-8");
    	
    	return stringbody;
    }
    
}



package io.bottest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.bottest.dto.APIResult;
import io.bottest.dto.PayRollDTO;
import io.bottest.dto.WorkerDTO;
import io.bottest.service.WorkerService;

@Controller
@RestController
@RequestMapping("/api/v1/worker")
public class WorkerController {

private final Logger LOGGER = LogManager.getLogger(FarmatePublicProcess.class);
	
	@Autowired
	private WorkerService workerService;
	
	@RequestMapping(value = "/listWorker/{ownerId}", method = RequestMethod.GET)
	public APIResult getWorker(@PathVariable("ownerId") Long ownerId) {
		final String LOG_STRING = "GET /api/v1/worker/listWorker "+ ownerId;
		LOGGER.info("START" + LOG_STRING);
		
		APIResult result = new APIResult();
		List<WorkerDTO> list = workerService.getAllWorkerById(ownerId);
		
		if(list.isEmpty() || list == null) {
			String message = "There are no worker on the list ! ";
			result.setErrormessage(message);
		}
		LOGGER.info(LOG_STRING + "RESULTS listWorker RESULTS listWorker" + list);
		result.setData(list);
		
		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public APIResult createWorker(@RequestBody WorkerDTO workerDTO) {
		final String LOG_STRING = "POST /api/v1/worker/create";
		LOGGER.info("START" + LOG_STRING);
		LOGGER.info("POST" + LOG_STRING + " PARAM workerDTO: " +workerDTO);

		APIResult result = new APIResult(); 	
		if(workerDTO != null) {
			workerService.saveWorker(workerDTO);
		}
		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public APIResult updateWorker(@RequestBody WorkerDTO workerDTO) {
		final String LOG_STRING = "POST /api/v1/worker/update";
		LOGGER.info("START" + LOG_STRING);
		LOGGER.info("POST" + LOG_STRING + " PARAM workerDTO: " +workerDTO);

		APIResult result = new APIResult();
		workerService.updateWorker(workerDTO);

		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = "/{workerId}/dayOffOrdayWork/update", method = RequestMethod.POST)
	public APIResult updateDayOffOrDayWorkOfWorker(@RequestBody PayRollDTO payRollDTO, 
			@PathVariable("workerId") String workerId) {
		final String LOG_STRING = "POST /api/v1/worker/" + workerId + "/dayOffOrdayWork/update" ;
		LOGGER.info("START " + LOG_STRING);
		LOGGER.info(LOG_STRING + "PARAMS payRollDTO:" + payRollDTO);
		
		APIResult result = new APIResult();
		if(workerId != null && payRollDTO != null) {
			workerService.updateDayOffOrDayWorkWorker(workerId,payRollDTO);
		}
		
		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = "/{workerId}/pay", method = RequestMethod.POST)
	public APIResult updateStatusPayRollOfWorker(@RequestBody PayRollDTO payRollDTO,
			@PathVariable("workerId") Long workerId) {
		final String LOG_STRING = "POST /api/v1/worker/" +workerId + "/pay";
		LOGGER.info("START" + LOG_STRING);
		LOGGER.info(LOG_STRING + "PARAMS payRollDTO:" + payRollDTO);
		
		APIResult result = new APIResult();
		if(workerId != null && payRollDTO != null) {
			workerService.updateStatusPayRollOfWorker(workerId, payRollDTO);
		}

		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
}

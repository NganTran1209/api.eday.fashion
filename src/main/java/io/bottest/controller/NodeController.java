package io.bottest.controller;

import static org.springframework.http.ResponseEntity.ok;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.io.Files;
import com.google.gson.Gson;

import io.bottest.configure.security.jwt.JwtTokenProvider;
import io.bottest.dto.APIResult;
import io.bottest.jpa.entity.ExecutingJob;
import io.bottest.jpa.entity.InterfaceServer;
import io.bottest.jpa.entity.NodeInfo;
import io.bottest.jpa.entity.ResultInfo;
import io.bottest.service.FileService;
import io.bottest.service.InterfaceServerService;
import io.bottest.service.JenkinService;
import io.bottest.service.NodeService;

@RestController
@RequestMapping(value = "/node")
public class NodeController { 

	private final Logger LOGGER = LogManager.getLogger(NodeController.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Autowired
	NodeService nodeServices;

	@Value("${resource.path}")
	private String resourcePath;

	//@Value("${jenkins.username}")
	private String jenkinsUser;

	//@Value("${jenkins.password}")
	private String jenkinsPass;

	//@Value("${jenkins.url}")
	private String jenkinsUrl;

	//@Value("${jenkins.token}")
	private String jenkinsToken;

	//@Value("${spring.project.type}")
	private String scmType;

	@Autowired
	NodeService nodeService;

	@Autowired
	InterfaceServerService interfaceServerService;

	@Autowired
	JenkinService jenkinService;
	
	@Autowired
	FileService fileService;

	/**
	 * rest API like postman
	 * 
	 * @param data
	 * @return
	 */
   @RequestMapping(value="/createAndGet", method=RequestMethod.GET)
    public ResponseEntity createAndGetNodeInfo(@AuthenticationPrincipal UserDetails userDetails, @RequestParam String os, @RequestParam String mac) {
    	
    	final String LOG_STRING = "GET /node/createAndGet";
		LOGGER.info("Start " + LOG_STRING);
    	
    	String[] username = userDetails.getUsername().split("@");
    	NodeInfo nodeInfo = nodeServices.getNodeinfo(username[0], os, mac);
    	
    	boolean isAdmin = !userDetails.getAuthorities().toString().contains("ADMIN");
    	
    	APIResult result = null;
    	System.out.println(username[0]);
    	if (nodeInfo == null) {
    		nodeServices.createNodeInJenkins(username[0], jenkinsUser, jenkinsPass, mac);
    		nodeServices.saveNodeInfo(username[0], jenkinsUser, jenkinsPass, os, mac);
    		nodeInfo = nodeServices.getNodeinfo(username[0], os, mac);
    	} 
    	else {
    		if (!nodeInfo.getOs().equals(os) || !nodeInfo.getMac().equals(mac)) {
    			nodeServices.saveNodeInfo(username[0], jenkinsUser, jenkinsPass, os, mac);
        		nodeInfo = nodeServices.getNodeinfo(username[0], os, mac);
    		}
    	}
    	
		String runningOS = nodeInfo.getOs().startsWith("Win")?"WindowOS":"MacOS";
		InterfaceServer infServer = interfaceServerService.findByOsAndScmTypeAndNodeLabel(nodeInfo.getOs(), scmType, nodeInfo.getUsername() +"_"+mac);
		if (infServer == null) {
			jenkinService.createUserJob(nodeInfo.getUsername()+ "_" + mac + "_" + nodeInfo.getOs() + "_" + scmType + "_Bottest_v2", nodeInfo.getUsername()+"_"+mac);
			interfaceServerService.createInterfaceServer(
					jenkinsUrl, 
					nodeInfo.getUsername()+ "_" + mac + "_" + nodeInfo.getOs() + "_" + scmType + "_Bottest_v2", 
					nodeInfo.getOs(), 
					runningOS,
					jenkinsToken,
					jenkinsUser,
					"Japan, Tokyo",
					scmType,
					nodeInfo.getUsername()+ "_" + mac,
					isAdmin
					);
		}
			
			
    	result = new APIResult();
    	result.setData(nodeInfo);
    	
    	LOGGER.info("End " + LOG_STRING);
        return ok(result);
    }
    
    @RequestMapping(value="/install", method=RequestMethod.GET)
    public ResponseEntity install(@AuthenticationPrincipal UserDetails userDetails,  @RequestParam String mac) {
    	
    	final String LOG_STRING = "GET /node/install";
		LOGGER.info("Start " + LOG_STRING);
    	
    	String installUrl = jenkinsUrl + "/job/Bottest_setup_Node_Win_Pipeline/buildWithParameters";
    	HttpPost request = new HttpPost(installUrl);
		String bsicStr = jenkinsUser + ":" + jenkinsPass;
		request.addHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString(bsicStr.getBytes()));
		request.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair("nodename", userDetails.getUsername().split("@")[0] +"_"+ mac));

		try {
			request.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
		String status = null;
    	try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(request)) {
    		status = String.valueOf(response.getStatusLine().getStatusCode());
    	} catch (ClientProtocolException e) {
    		LOGGER.error("End /node/install install ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		} catch (IOException e) {
			LOGGER.error("End /node/install install ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
    	APIResult result = new APIResult();
        
		LOGGER.info("END " + LOG_STRING);
		
    	if ("201".equalsIgnoreCase(status)) {
    		result.setData("201");
            return ok(result);
    	}
    	
    	result.setData("400");
        return ok(result);
    	
    }

	@RequestMapping(value = "/statusChange", method = RequestMethod.POST)
	public void statusChange(@RequestBody ExecutingJob job) throws Exception {
		final String LOG_STRING = "GET /node/statusChange";
		LOGGER.info("Start " + LOG_STRING);
		nodeService.updateStatusChange(job);
		LOGGER.info("END " + LOG_STRING);
	}
	
	@RequestMapping(value = "/resultRunOnLocal", method = RequestMethod.POST)
	public void resultRunOnLocal(@RequestBody String results, Principal principal) throws Exception {
		Gson gson = new Gson();
		final String LOG_STRING = "GET /node/resultRunOnLocal";
		LOGGER.info("Start " + LOG_STRING);
		Map<String, String> mapResults = gson.fromJson(results, Map.class);
		ResultInfo[] list = gson.fromJson(mapResults.get("resultList"), ResultInfo[].class);
		
		ExecutingJob job = gson.fromJson(mapResults.get("job"), ExecutingJob.class);
		
		nodeService.insertResultinfo(Arrays.asList(list));
		nodeService.updateStatusChange(job);
		fileService.syncProject(principal.getName(), String.valueOf(list[0].getProjectId()));
		LOGGER.info("END " + LOG_STRING);
	}
	
	@MessageMapping("/changeStatus")
	public void changeStatus(@Payload Object payload, Principal principal) throws Exception {
		final String LOG_STRING = "GET /node/changeStatus";
		LOGGER.info("Start " + LOG_STRING);
		nodeService.changeStatus(payload, principal.getName());
		LOGGER.info("END " + LOG_STRING);
	}
	
	@RequestMapping(value = "/disconnect", method = RequestMethod.POST)
	public void disconnectBySessionId(@RequestParam String sessionId) throws Exception {
		final String LOG_STRING = "GET /node/disconnect";
		LOGGER.info("Start " + LOG_STRING);
		nodeService.disconnectBySessionId(sessionId);
		LOGGER.info("END " + LOG_STRING);
	}
	
	@RequestMapping(value = "/updateNode", method = RequestMethod.POST)
	public void updateDate(@RequestParam String sessionId, @RequestParam String requestDate) throws Exception {
		final String LOG_STRING = "GET /node/updateNode";
		LOGGER.info("Start " + LOG_STRING);
		nodeService.updateNodeConnected(sessionId, requestDate);
		LOGGER.info("END " + LOG_STRING);
	}
	
	@RequestMapping(value = "/checkOnLocal", method = RequestMethod.GET)
	public ResponseEntity checkOnLocal(@RequestParam String os, Principal principal) throws Exception {
		final String LOG_STRING = "GET /node/checkOnLocal";
		LOGGER.info("Start " + LOG_STRING);
		nodeService.checkOnLocal(principal.getName());
		boolean isOnline = nodeService.getNodeinfoList(principal.getName(), os, true).size() > 0?true:false;
		LOGGER.info("END " + LOG_STRING);
		return ok(isOnline);
		
	}
	
	@RequestMapping(value = "/resultExceptionRunOnLocal", method = RequestMethod.POST)
	public void resultExceptionRunOnLocal(@RequestParam String id, @RequestBody String exception, Principal principal) throws Exception {
//		Gson gson = new Gson();
//		ResultInfo[] list = gson.fromJson(results, ResultInfo[].class);
//		nodeService.insertResultinfo(Arrays.asList(list));
//		fileService.syncProject(principal.getName(), String.valueOf(list[0].getProjectId()));
		
	}
	
	@RequestMapping(value = "/generate", method = RequestMethod.POST)
	public ResponseEntity generateFile(Principal principal) throws Exception {
		final String LOG_STRING = "GET /node/generate";
		LOGGER.info("Start " + LOG_STRING);
		ClassLoader classLoader = getClass().getClassLoader();
		File GenerateExecuteFile = new File(classLoader.getResource("BotTest_GenerateExecuteFile.py").toURI());
		String StringGenerateExecute = Files.asCharSource(GenerateExecuteFile, StandardCharsets.UTF_8).read();
		LOGGER.info("END " + LOG_STRING);
		return ok(StringGenerateExecute);
	}
	
	@RequestMapping(value = "/fast", method = RequestMethod.POST)
	public ResponseEntity fastFile(Principal principal) throws Exception {
		final String LOG_STRING = "GET /node/fast";
		LOGGER.info("Start " + LOG_STRING);
		ClassLoader classLoader = getClass().getClassLoader();
		File GenerateExecuteFile = new File(classLoader.getResource("CombineFile.py").toURI());
		String StringGenerateExecute = Files.asCharSource(GenerateExecuteFile, StandardCharsets.UTF_8).read();
		LOGGER.info("END " + LOG_STRING);
		return ok(StringGenerateExecute);
	}
	
	@RequestMapping(value = "/fastMac", method = RequestMethod.POST)
	public ResponseEntity fastFileMac(Principal principal) throws Exception {
		final String LOG_STRING = "GET /node/fastMac";
		LOGGER.info("Start " + LOG_STRING);
		ClassLoader classLoader = getClass().getClassLoader();
		File GenerateExecuteFile = new File(classLoader.getResource("CombineFileMac.py").toURI());
		String StringGenerateExecute = Files.asCharSource(GenerateExecuteFile, StandardCharsets.UTF_8).read();
		LOGGER.info("END " + LOG_STRING);
		return ok(StringGenerateExecute);
	}
}

package io.bottest.controller;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.ChangePassDTO;
import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.FilterSupplieDTO;
import io.bottest.dto.KhoNongSanDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.dto.SignupDTO;
import io.bottest.dto.UpdateUserDTO;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.Category;
import io.bottest.jpa.entity.InvitedToProject;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.entity.ProjectComponent;
import io.bottest.jpa.entity.Software;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.entity.UserProject;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.InvitedReposiry;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProjectRepository;
import io.bottest.jpa.respository.SoftwareRepository;
import io.bottest.jpa.respository.UserProjectRepository;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.service.CategoryServise;
import io.bottest.service.FileService;
import io.bottest.service.ProductService;
import io.bottest.service.ProjectComponentServise;
import io.bottest.service.ProjectService;
//import io.bottest.service.SendMailService;
import io.bottest.service.SoftwareService;
import io.bottest.service.UserService;
import io.bottest.utils.FunctionUtils;
import io.bottest.utils.TokenUtil;

@CrossOrigin
@Controller
@RestController
@RequestMapping("/")
public class HomeController {
 
	private final Logger LOGGER = LogManager.getLogger(HomeController.class);
	
	public static final String STATUS_REJECT = "REJECT";
	public static final String STATUS_ACCEPT = "ACCEPT";
	public static final String STATUS_WATING = "WAITING";

	@Value("${security.jwt.token.secret-key:Bottest@Paracel}")
	private String secretKey = "Bottest@Paracel";
	
	//@Value("${spring.project.username}")
	private String projectUserName;
	
	//@Value("${spring.project.password}")
	private String projectPassword;
	
	//@Value("${spring.project.url}")
	private String projectUrl;
	
	//@Value("${spring.project.type}")
	private String projectType;
	
	//@Value("${sample.path}")
	private String samplePath;
	
//	@Value("${resource.path}")
//	private String resourcePath;
	
	@Value("${bottest.url}")
	private String bottestUrl;
	

	@Autowired
	BottestUserRepository userRepo;

	@Autowired
	private UserService userService;

	@Autowired
	ProjectService projectService;

//	@Autowired
//	SendMailService sendMailService;

	@Autowired
	private UserProjectRepository userProjectRepo;
	
	@Autowired 
	private InvitedReposiry invitedRepo;
	
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	FileService fileService;
	
	@Autowired
	SoftwareRepository softwareRepo;
	
	@Autowired
	SoftwareService softwareService;
	
	@Autowired
	ProjectComponentServise projectComponentServise;
	
	//EDAY
	@Autowired
	UserRepository userRepos;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ServletContext context;
	
	@Autowired
	CategoryServise categoryService;
	
	@Autowired
	ProductRepository productRepo;
	
	/**
	 * @return
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView getProject() {
		LOGGER.info("Start GET / ");
		
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET / ");
		return mav;
	}
	@RequestMapping(value = { "/en" }, method = RequestMethod.GET)
	public ModelAndView homeEn() {
		LOGGER.info("Start GET /en ");
		
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET /en ");
		return mav;
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login() {

		LOGGER.info("Start GET /login ");
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET /login ");
		return mav;
	}

	/**
	 * @param token
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/signup" }, method = RequestMethod.GET)
	public ModelAndView singup() {
		
		LOGGER.info("Start GET /signup ");
		
		SignupDTO dto = new SignupDTO();
		ModelAndView mav = new ModelAndView("signup");
		mav.addObject("dto", dto);
		
		LOGGER.info("End GET /signup ");
		return mav;
	}

	

	@MessageMapping("/listen")
	//@SendToUser("/bot_notify/job")
	public APIResult listen2(@RequestBody String message, Principal principal) throws Exception {
//		System.out.println("/listen:Client message:" + message);
		// Thread.sleep(1000); // simulated delay
		LOGGER.info("Start Message /listen ");
		
		LOGGER.info("GET /auth/me PARAMS message:" + message);
		APIResult result = new APIResult();
		result.setData("");
		
		LOGGER.info("End Message /listen ");
		return result;
	}

	/**
	 * @param token
	 * @return
	 */
	@RequestMapping(value = { "/activeAccount" }, method = RequestMethod.GET)
	public ModelAndView activeAccount(@RequestParam(name = "token", required = true) String token) {
		
		LOGGER.info("Start GET /activeAccount");
		
		LOGGER.info("GET /activeAccount PARAMS TOKEN:" + token);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("signup");
		if (StringUtils.isNotEmpty(token)) {
			try {
				String strToken = TokenUtil.decrypt(token, secretKey);
				String[] params = strToken.split("&");
				for (String param : params) {
					String[] nameValue = param.split("=");

					if (nameValue.length > 1) {
						// Token checking if the new day is over will not work
						if (param.contains("dateCreate")) {
							String pattern = "dd/MM/yyyy HH:mm:ss";
							Date dateNow = new Date();
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
							String datestr = nameValue[1];
							Date date = simpleDateFormat.parse(datestr);
							if (dateNow.compareTo(date) < 0) {
								mav.setViewName("home");
							}
						}
						if (param.contains("email")) {
							BottestUser user = userRepo.findByUsernameIgnoreCase(nameValue[1]);
							//boolean enableValue = user.isEnabled();
							//boolean checkSave = false;
//							if(enableValue) {
//								checkSave = true;
//							}
							user.setEnable(true);
							
							LOGGER.info("GET /activeAccount RESULTS BottestUser:" + new Gson().toJson(user, new TypeToken<BottestUser>() {}.getType()) );
							
							userRepo.save(user);
							
							//create project mau mac dinh khi signup account 
//							if(!checkSave) {
//								
//								Project project = new Project();
//								project.setName("SampleProject");
//								project.setReportTime("23:00");
//								project.setReportTimeZone("(GMT+07:00) Bangkok, Hanoi, Jakarta");
//								project.setScmPassword(projectPassword);
//								project.setScmUsername(projectUserName);
//								project.setScmUrl(projectUrl);
//								project.setScmType(projectType);
//								project.setDeletedFlag(0);
//								project.setType("Web Application");
//								project.setDescription("sample project");
//								Random random = new Random();
//						        String color = String.format("#%06x", random.nextInt(256*256*256));
//								project.setColor(color);
//								
//								projectService.saveSampleProject(project, nameValue[1]);
//								
//								fileService.readFileSqlDataSample(project.getId().toString());
//								
//								Path dir = Paths.get(this.resourcePath, nameValue[1].replace("@", "."), project.getId().toString());
//								fileService.copyProjectSample(samplePath, dir.toString(), nameValue[1], project.getId().toString());
//								
//							}
							
							mav.setViewName("login");
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				LOGGER.error("GET /activeAccount ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
		}
		
		LOGGER.info("END GET /activeAccount");
		
		return mav;
	}

	/**
	 * @param token
	 * @return
	 */
	@RequestMapping(value = { "/invitedProject" }, method = RequestMethod.GET)
	public ModelAndView invitedProject(@RequestParam(name = "token", required = true) String token) {
		
		LOGGER.info("Start GET /invitedProject");
		
		LOGGER.info("GET /invitedProject PARAMS TOKEN:" + token);
		ModelAndView mav = new ModelAndView();
		SignupDTO dto = new SignupDTO();
		String viewName = "signup";
		if (StringUtils.isNotEmpty(token)) {
			String strToken;
			try {
				strToken = TokenUtil.decrypt(token, secretKey);
				String[] params = strToken.split("&");
				UserProject userProject = new UserProject();
				Project project = null;
				int valid = 0;
				for (String param : params) {
					String[] nameValue = param.split("=");

					if (nameValue.length > 1) {
						if (param.contains("toEmail")) {
							dto.setEmail(nameValue[1]);
							valid++;
						}
						if (param.contains("projectRole")) {
							userProject.setProjectRole(nameValue[1]);
							valid++;
						}
						if (param.contains("projectId")) {
							project = projectService.getProjectById(Long.valueOf(nameValue[1]));
							valid++;
						}
					}
				}
				
				LOGGER.info("GET /invitedProject RESULTS UserProject:" + new Gson().toJson(userProject, new TypeToken<UserProject>() {}.getType()));
				LOGGER.info("GET /invitedProject RESULTS Project:" +  new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
				
				if (valid == 3) {
					boolean checkActive = userService.checkEmailExist(dto.getEmail());
					
					LOGGER.info("GET /invitedProject RESULTS EmailExist:" +  checkActive);
					
					if (checkActive) {
						viewName = "login";
					}
				}
			} catch (Exception e) {
				LOGGER.error("GET /invitedProject ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
			
			
		}

		mav.addObject("dto", dto);
		mav.setViewName(viewName);
		
		LOGGER.info("End GET /invitedProject");
		return mav;
	}
	
	/**
	 * @param token
	 * @return
	 */
	@RequestMapping(value = { "/joinProject" }, method = RequestMethod.GET)
	public ModelAndView joinProject(@RequestParam(name = "token", required = true) String token) {
		
		LOGGER.info("Start GET /joinProject");
		
		LOGGER.info("GET /joinProject PARAMS TOKEN:" + token);
		
		SignupDTO dto = new SignupDTO();
		if (StringUtils.isNotEmpty(token)) {
			if(setUserToProjectFromToken(token, dto)) {
			}
			
		}
		ModelAndView model = new ModelAndView();
		
		LOGGER.info("End GET /joinProject");
		return model;
	}

	private boolean setUserToProjectFromToken(String token, SignupDTO dto) {
		
		LOGGER.info("Start FUNCTION setUserToProjectFromToken");
		
		LOGGER.info("FUNCTION setUserToProjectFromToken PARAMS SignupDTO: " + new Gson().toJson(dto, new TypeToken<SignupDTO>() {}.getType()));

		try {
			String strToken = TokenUtil.decrypt(token, secretKey);
			String[] params = strToken.split("&");
			UserProject userProject = new UserProject();
			Project project = null;
			int valid = 0;
			for (String param : params) {
//				System.out.println(param);
				String[] nameValue = param.split("=");

				if (nameValue.length > 1) {
					if (param.contains("toEmail")) {
						dto.setEmail(nameValue[1]);
						valid++;
					}
					if (param.contains("projectRole")) {
						userProject.setProjectRole(nameValue[1]);
						valid++;
					}
					if (param.contains("projectId")) {
						project = projectService.getProjectById(Long.valueOf(nameValue[1]));
						valid++;
					}
				}
			}
			if (valid == 3) {
				boolean checkActive = userService.checkEmailExist(dto.getEmail());
				if (checkActive) {
					BottestUser user = userRepo.findByUsernameIgnoreCase(dto.getEmail());
					user.setEnable(true);
					userRepo.save(user);
					if (project != null) {
						userProject.setUsername(dto.getEmail());
						userProject.setProjectName(project.getName());
						userProject.setProjectId(project.getId());
						List<UserProject> checkUsername = userProjectRepo.findByProjectIdAndUsername(project.getId(), userProject.getUsername());
						if (checkUsername.isEmpty()) {
							InvitedToProject inviteProject = invitedRepo.findByProjectIdAndToEmail(project.getId().toString(), dto.getEmail());
							inviteProject.setStatus(1);
							invitedRepo.save(inviteProject);
							userProjectRepo.save(userProject);
							fileService.syncProject( dto.getEmail(),project);
						} else {
							LOGGER.info("End FUNCTION setUserToProjectFromToken");
							return true;
						}

					}
					LOGGER.info("End FUNCTION setUserToProjectFromToken");
					return true;
				} else {
					LOGGER.info("End FUNCTION setUserToProjectFromToken");
					return false;
				}
			}
		} catch (Exception e) {
			
			LOGGER.error("FUNCTION setUserToProjectFromToken ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		LOGGER.info("End FUNCTION setUserToProjectFromToken");
		return false;
	}

	@RequestMapping(value = { "/forgotPass" }, method = RequestMethod.GET)
	public ModelAndView forgotPass(@RequestParam(name = "forgotinfo", required = true) String forgotinfo) {

		LOGGER.info("Start GET /forgotPass ");
		
		LOGGER.info("GET /forgotPass PARAMS forgotinfo:" + forgotinfo);
		ModelAndView mav = new ModelAndView("forgotPass");
		try {
			String strToken = TokenUtil.decrypt(forgotinfo, secretKey);
			String[] params = strToken.split("&");
			for (String param : params) {
				String[] nameValue = param.split("=");
				if (nameValue.length > 1) {
					// Token checking if the new day is over will not work
					if (param.contains("dateCreate")) {
						String pattern = "dd/MM/yyyy HH:mm:ss";
						Date dateNow = new Date();
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
						String datestr = nameValue[1];
						Date date = simpleDateFormat.parse(datestr);
						if (dateNow.compareTo(date) < 0) {
							mav.setViewName("home");
						}
					}
				}

			}
		} catch (Exception e) {
			LOGGER.info("GET /forgotPass ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		ChangePassDTO dto = new ChangePassDTO();
		dto.setToken(forgotinfo);
		mav.addObject("changePassDTO", dto);
		
		LOGGER.info("End GET /forgotPass ");
		
		return mav;
	}

	@RequestMapping(value = { "/forgotPass" }, method = RequestMethod.POST)
	public APIResult forgotPass(@RequestBody ChangePassDTO changePassDTO) {
		
		LOGGER.info("Start POST /forgotPass ");
		
		LOGGER.info("POST /forgotPass PARAMS changePassDTO: " + new Gson().toJson(changePassDTO, new TypeToken<ChangePassDTO>() {}.getType()));
		APIResult result = new APIResult();
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		try {
			String strToken = TokenUtil.decrypt(changePassDTO.getToken(), secretKey);
			String[] params = strToken.split("&");
			for (String param : params) {
				String[] nameValue = param.split("=");
				if (nameValue.length > 1) {
					if (param.contains("email")) {
						BottestUser bottestUser = userRepo.findByUsernameIgnoreCase(nameValue[1]);
						boolean checkActive = userService.checkEmailExist(nameValue[1]);
						if (checkActive) {
							bottestUser.setPassword(encoder.encode(changePassDTO.getNewPassword()));
							bottestUser.setConfirmPass(encoder.encode(changePassDTO.getNewConfirmPass()));
							bottestUser.setEnable(true);
							userRepo.save(bottestUser);
						}
					}
				}

			}
			result.setData(changePassDTO);
		} catch (Exception e) {
			LOGGER.info("POST /forgotPass ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}

		LOGGER.info("End POST /forgotPass ");
		return result;
	}

	@RequestMapping(value = { "/sendMailForgotPass" }, method = RequestMethod.POST)
	public APIResult sendMailForgotPass(@RequestBody BottestUser bottestUser) {
		
		LOGGER.info("Start POST /sendMailForgotPass ");
		
		LOGGER.info("POST /sendMailForgotPass PARAMS bottestUser:" + new Gson().toJson(bottestUser, new TypeToken<BottestUser>() {}.getType()));
		APIResult result = new APIResult();
		userService.mailForgotPass(bottestUser);

		LOGGER.info("End POST /sendMailForgotPass ");
		return result;
	}

	/**
	 * @param projectId
	 * @param testsuiteId
	 * @param layouts
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping(value = { "/user/create" }, method = RequestMethod.POST)
	public APIResult createUser(@RequestBody Map<String, BottestUser> users) {
		
		LOGGER.info("Start POST /user/create ");
		
		LOGGER.info("POST /user/create PARAMS users:" + new Gson().toJson(users, new TypeToken<Map<String, BottestUser>>() {}.getType()));
		APIResult result = new APIResult();
		BottestUser user = users.get("user");
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		if (user != null) {
			user.setPassword(encoder.encode(user.getPassword()));
		}
		userRepo.save(user);
		user.setPassword("");
		result.setData(user);
		
		LOGGER.info("End POST /user/create ");
		return result;

	}

	@RequestMapping(value = { "/downloadSoftware" }, method = RequestMethod.GET)
	public ModelAndView downloadSoftwareVersion(@AuthenticationPrincipal UserDetails userDetails) {
		ModelAndView mav = new ModelAndView("downloadSoft");
		List<Software> softwares = softwareRepo.getVersionNew();
		mav.addObject("softwareVersionNews", softwares);
		mav.addObject("userDetails", userDetails);
		return mav;
	}

	
	//EDAY
	@RequestMapping(value = { "/editProfile" }, method = RequestMethod.POST)
	public APIResult editProfile(@RequestBody UpdateUserDTO user, @AuthenticationPrincipal UserDetails userDetails, HttpServletRequest request) {
		
		LOGGER.info("Start POST /editProfile ");
		LOGGER.info("POST /editProfile PARAMS user:" + new Gson().toJson(user, new TypeToken<UpdateUserDTO>() {}.getType()));
		LOGGER.info("POST /editProfile PARAMS userDetails:" + new Gson().toJson(userDetails, new TypeToken<UserDetails>() {}.getType()));
		APIResult result = new APIResult();
		User theUser = userRepos.findByUsernameIgnoreCase(user.getUsername());
		theUser.setAddr(user.getAddr());
		theUser.setBirthDay(user.getBirthDay());
		theUser.setPhone(theUser.getPhone());
		theUser.setSex(user.isSex());
		userRepos.save(theUser);
		result.setData(theUser);
		
		LOGGER.info("End POST /editProfile ");
		return result;
	}
	
	/**
	 * @param fileName
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = {"/VersionNewS"}, method = RequestMethod.GET)
	public ResponseEntity<Resource> getVersionExtentionNewS(HttpServletRequest request, @RequestParam("filename") String fileName) throws IOException {
		// Load file as Resource
		Resource resource = softwareService.loadSoftwareFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			// logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	
	}
	
	/**
	 * @param 
	 * @param
	 * @return
	 */
	@RequestMapping(value = { "/createProjectComponent" }, method = RequestMethod.POST)
	public APIResult createTracker(@RequestBody List<ProjectComponent>  projectComponent) {
		APIResult result = new APIResult();
		for (ProjectComponent proComponent: projectComponent) {
			projectComponentServise.saveProjectComponent(proComponent);
		}
		return result;

	}
	
	
	//API EDAY
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public APIResult signupSubmit(@RequestBody SignupDTO dto) {
		final String LOG_STRING = "POST /api/v1/user/signup";
		LOGGER.info("START" + LOG_STRING);
		LOGGER.info(LOG_STRING + " PARAM dto: " + dto);

		APIResult result = new APIResult();
		if (dto != null) {
			if (dto.getEmail() != null) {
				boolean checkUser = userService.checkEmailExist(String.valueOf(dto.getEmail()));
				if (checkUser == false) {
					result.setErrormessage("Email is exist! Please enter the new username");
					LOGGER.info("POST /signup MESSAGE: Username is exist! Please enter the new username");
				} else {
					User user = new User();
					PasswordEncoder encoder = new BCryptPasswordEncoder();
					user.setEmail(dto.getEmail());
					user.setUsername(dto.getUsername());
					user.setPassword(encoder.encode(dto.getPassword()));
					user.setRole("ROLE_CUSTOMER");
					user.setEnable(true);
					//user.setStartDay(Date );
					userRepos.save(user);
					result.setData(user);
				}
			}
		}

		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = { "/product/list" }, method = RequestMethod.GET)
	public APIResult listProduct() {
		LOGGER.info("GET /product/list");
		APIResult result = new APIResult();
		List<Product> listProduct = productService.getAllProduct();
		result.setData(listProduct);
		return result;
	}
	
	@RequestMapping(value = { "/getImgproduct/{fileName:.+}" }, method = RequestMethod.GET)
	public ResponseEntity<Resource> getImgproduct(@PathVariable String fileName,HttpServletRequest request) throws MalformedURLException {
		
		final String LOG_STRING = "POST /getImgproduct/" + fileName;
		LOGGER.info("Start " + LOG_STRING);
		// Load file as Resource
		APIResult result = new APIResult();
		Resource resource = fileService.loadImg(fileName);
		if (resource == null) {
			resource = new UrlResource(context.getResource("/assets/img/noImage.png"));
			
		}
		String nameAb = "";
		String fileNameWithoutExtension = fileName.replaceFirst("[.][^.]+$", "");
		
		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			LOGGER.error(LOG_STRING + " ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));

		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		LOGGER.info("End " + LOG_STRING);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileNameWithoutExtension + "\"")
				.body(resource);
	}
	
	@RequestMapping(value = { "/product/{productId}/detail" }, method = RequestMethod.GET)
	public APIResult detailProduct(@PathVariable(name = "productId") String productId) {
		APIResult result = new APIResult();
		Product product = productService.getProductById(Long.valueOf(productId));
		result.setData(product);
		return result;
	}
	
	
	@RequestMapping(value = { "/product/filter" }, method = RequestMethod.POST)
	public APIResult filterProduct(Principal principal, @RequestBody FilterProductDTO filterProduct,HttpSession session) {

		APIResult result = new APIResult();
		Gson gson = new Gson();
		Type listType = new TypeToken<FilterProductDTO>() {
		}.getType();
		String filStr = gson.toJson(filterProduct, listType);
		session.setAttribute("filterProduct", filStr);
		List<Map<String, Object>> productList = productService.filterProduct(filterProduct);
		if(productList != null && productList.size() > 0) {
			result.setData(productList);
		}else {
			result.setData("No data available");
		}
		
		return result;
	}
	
	@RequestMapping(value = { "/listStaff" }, method = RequestMethod.GET)
	public APIResult listStaff( ) {

		final String LOG_STRING = "GET /listStaff";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		List<User> listStaff = userRepos.getListStaff();
		result.setData(listStaff);

		LOGGER.info("End GET /listStaff");
		return result;

	}
	
	@RequestMapping(value = { "/cat/list" }, method = RequestMethod.GET)
	public APIResult listCat(@AuthenticationPrincipal UserDetails userDetails) {

		final String LOG_STRING = "GET /admin/cat/list";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		List<Category> cats = categoryService.findAll();
		result.setData(cats);

		LOGGER.info("GET /admin/cat/list");
		return result;

	}
	
	@RequestMapping(value = { "/productFilterCat" }, method = RequestMethod.GET)
	public APIResult productFilterCat(@RequestBody String catId) {

		final String LOG_STRING = "GET /productFilterCat";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		Long id = Long.valueOf(catId);
		if(id == null || id <= 0 ) {
			result.setErrormessage("Check category again!!");
		}else {
			List<Product> listProduct = productRepo.getListProductByCatId(id);
		}
		List<Category> cats = categoryService.findAll();
		result.setData(cats);

		LOGGER.info("GET /productFilterCat");
		return result;

	}
	
}

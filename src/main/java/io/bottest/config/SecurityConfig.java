package io.bottest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import io.bottest.configure.security.jwt.JwtConfigurer;
import io.bottest.service.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomUserDetailsService users;
    
    @Autowired
    JwtConfigurer jwtConfigurer;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
    	AuthenticationManager am = super.authenticationManagerBean();
    	
        return am;
    }
    
    
    @Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http
            .httpBasic().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
            .and()
                .authorizeRequests()
                .antMatchers("/","/en","/jp","/auth/signin","/auth/me","/user/create","/signup","/login","/forgotPass","/sendMailForgotPass","/activeAccount","/invitedProject","/editProfile","/editPassword","/testDataDetail", "/joinProject", "/rest", "/node/**", "/listStaff").permitAll()
                .antMatchers("/resources/**", "/assets/**","/order/**","/payment/**", "/getImgproduct/**", "/productFilterCat", "/cat/**").permitAll()
                .antMatchers("/admin/**", "/payroll/**",  "/warehouse/**").access("@customUserDetailsService.checkAuthen(authentication)")
                .antMatchers("/userAu/**").authenticated()
                .antMatchers("/product/**").permitAll()
                .antMatchers("/project/graphicTestcaseAutAndMan").authenticated()
                .antMatchers("/project/createUserProject").authenticated()
                .antMatchers("/project/deleteInvited").authenticated()
                .antMatchers("/project/infInvited").authenticated()
                .antMatchers("/project/create").authenticated()
                .antMatchers("/project/getExecutingJob").authenticated()
                .antMatchers("/project/getUser").authenticated()
                .antMatchers("/project/uploadFile").authenticated()
                .antMatchers("/project/deleteFileStore").authenticated()
                .antMatchers("/project/copyProject").authenticated()
                .antMatchers("/project/{projectId}/**").access("@customUserDetailsService.checkProjectPermission(authentication,#projectId)")
                .anyRequest().permitAll()
            .and()
            .csrf().disable()
            .apply(jwtConfigurer)
            .and()
            .exceptionHandling().accessDeniedPage("/error/403");
        
        //@formatter:on
    }


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		super.configure(auth);
	}


}


package io.bottest.dto;

public class PostDTO {

	private String id;
	private String dayPost;
	private String title;
	private String image;
	private String like;
	private String comment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDayPost() {
		return dayPost;
	}
	public void setDayPost(String dayPost) {
		String arrayDayPost[] = dayPost.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayDayPost.length; i++) {
			sb.append(arrayDayPost[i]);
		}
		this.dayPost = sb.toString();
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLike() {
		return like;
	}
	public void setLike(String like) {
		this.like = like;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}

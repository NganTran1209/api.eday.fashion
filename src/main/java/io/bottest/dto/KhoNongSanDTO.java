package io.bottest.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class KhoNongSanDTO implements Serializable {
	
	private static final long serialVersionUID = 8351790473947314905L;
	private String id;
	private String productName;
	private String startDay;
	private String endDate;
	private String quantity;
	private String estimatedOutput;
	private String inventory;
	private String madeIn;
	private String invest;
	private boolean finish;
	private List<SellManagerDTO> sellManager;
	private List<OrderDTO> orderedList;
	private OutStockDTO outStock;
	private String updatedDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getStartDay() {
		return startDay;
	}
	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getEstimatedOutput() {
		return estimatedOutput;
	}
	public void setEstimatedOutput(String estimatedOutput) {
		this.estimatedOutput = estimatedOutput;
	}
	public String getInventory() {
		return inventory;
	}
	public void setInventory(String inventory) {
		this.inventory = inventory;
	}
	public String getMadeIn() {
		return madeIn;
	}
	public void setMadeIn(String madeIn) {
		this.madeIn = madeIn;
	}
	public String getInvest() {
		return invest;
	}
	public void setInvest(String invest) {
		this.invest = invest;
	}
	public boolean isFinish() {
		return finish;
	}
	public void setFinish(boolean finish) {
		this.finish = finish;
	}
	public List<SellManagerDTO> getSellManager() {
		return sellManager;
	}
	public void setSellManager(List<SellManagerDTO> sellManager) {
		this.sellManager = sellManager;
	}
	public List<OrderDTO> getOrderedList() {
		return orderedList;
	}
	public void setOrderedList(List<OrderDTO> orderedList) {
		this.orderedList = orderedList;
	}
	public OutStockDTO getOutStock() {
		return outStock;
	}
	public void setOutStock(OutStockDTO outStock) {
		this.outStock = outStock;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "KhoNongSanDTO [id=" + id + ", productName=" + productName + ", startDay=" + startDay + ", endDate="
				+ endDate + ", quantity=" + quantity + ", estimatedOutput=" + estimatedOutput + ", inventory="
				+ inventory + ", madeIn=" + madeIn + ", invest=" + invest + ", finish=" + finish + ", sellManager="
				+ sellManager + ", orderedList=" + orderedList + ", outStock=" + outStock + ", updatedDate="
				+ updatedDate + "]";
	}
	
}

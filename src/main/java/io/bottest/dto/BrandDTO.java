package io.bottest.dto;

import java.math.BigInteger;

public class BrandDTO {
	
	private BigInteger brandId;

	private String brandName;

	private String title;

	private String subTitle;

	private Double brandPoints;

	private String brandType;

	private String address;

	private String city;

	private String districts;

	private String streets;

	private String wards;

	private String mail;

	private String phoneNumber;

	private String imageUrl;

	private BigInteger uploadId;

	private int isFavorite;

	private int isActive = 1;

	private String token;

	private String createdDate;

	private String updateDate;

	private String domain;

	private String tokenBrand;

	public BigInteger getBrandId() {
		return brandId;
	}

	public void setBrandId(BigInteger brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Double getBrandPoints() {
		return brandPoints;
	}

	public void setBrandPoints(Double brandPoints) {
		this.brandPoints = brandPoints;
	}

	public String getBrandType() {
		return brandType;
	}

	public void setBrandType(String brandType) {
		this.brandType = brandType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistricts() {
		return districts;
	}

	public void setDistricts(String districts) {
		this.districts = districts;
	}

	public String getStreets() {
		return streets;
	}

	public void setStreets(String streets) {
		this.streets = streets;
	}

	public String getWards() {
		return wards;
	}

	public void setWards(String wards) {
		this.wards = wards;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public BigInteger getUploadId() {
		return uploadId;
	}

	public void setUploadId(BigInteger uploadId) {
		this.uploadId = uploadId;
	}

	public int getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(int isFavorite) {
		this.isFavorite = isFavorite;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getTokenBrand() {
		return tokenBrand;
	}

	public void setTokenBrand(String tokenBrand) {
		this.tokenBrand = tokenBrand;
	}

	@Override
	public String toString() {
		return "BrandDTO [brandId=" + brandId + ", brandName=" + brandName + ", title=" + title + ", subTitle="
				+ subTitle + ", brandPoints=" + brandPoints + ", brandType=" + brandType + ", address=" + address
				+ ", city=" + city + ", districts=" + districts + ", streets=" + streets + ", wards=" + wards
				+ ", mail=" + mail + ", phoneNumber=" + phoneNumber + ", imageUrl=" + imageUrl + ", uploadId="
				+ uploadId + ", isFavorite=" + isFavorite + ", isActive=" + isActive + ", token=" + token
				+ ", createdDate=" + createdDate + ", updateDate=" + updateDate + ", domain=" + domain + ", tokenBrand="
				+ tokenBrand + "]";
	}

}

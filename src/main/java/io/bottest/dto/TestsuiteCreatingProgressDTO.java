package io.bottest.dto;

import java.io.Serializable;
import java.util.Date;

public class TestsuiteCreatingProgressDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1169501143422424264L;
	
	private Long projectId;
	
	private String testsuite;
	
	private Date createDate;
	
	private int numOfCases;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getNumOfCases() {
		return numOfCases;
	}

	public void setNumOfCases(int numOfCases) {
		this.numOfCases = numOfCases;
	}
	
	
	

}

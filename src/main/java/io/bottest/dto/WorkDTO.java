package io.bottest.dto;

public class WorkDTO {

	private String id;
	private String title;
	private String dateStart;
	private String dateEnd;
	private String idProject;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDateStart() {
		return dateStart;
	}
	public void setDateStart(String dateStart) {
		String arrayDateStart[] = dateStart.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayDateStart.length; i++) {
			sb.append(arrayDateStart[i]);
		}
		this.dateStart = sb.toString();
	}
	public String getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(String dateEnd) {
		String arrayDateEnd[] = dateEnd.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < arrayDateEnd.length; i++) {
			sb.append(arrayDateEnd[i]);
		}
		this.dateEnd = sb.toString();
	}
	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}
	
}

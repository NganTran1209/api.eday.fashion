package io.bottest.dto;

import java.io.Serializable;
import java.util.Date;

public class TestsuiteTestingProgress implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8351790473947314905L;

	private String projectId;
	
	private String testsuite;
	
	private Date executeDate;

	private int totalTestcase;
	
	private int testcasePass;
	
	private int testcaseFail;
	
	private int notRun;
	
	private String projectName;
	
	private String description;
	
	private String color;
	
	private String descriptionTestsuite;
	
	private int countDegrade;
	
	public int getTotalTestcase() {
		return totalTestcase;
	}

	public void setTotalTestcase(int totalTestcase) {
		this.totalTestcase = totalTestcase;
	}

	public int getTestcasePass() {
		return testcasePass;
	}

	public void setTestcasePass(int testcasePass) {
		this.testcasePass = testcasePass;
	}

	public int getTestcaseFail() {
		return testcaseFail;
	}

	public void setTestcaseFail(int testcaseFail) {
		this.testcaseFail = testcaseFail;
	}

	public int getNotRun() {
		return notRun;
	}

	public void setNotRun(int notRun) {
		this.notRun = notRun;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

	public Date getExecuteDate() {
		return executeDate;
	}

	public void setExecuteDate(Date executeDate) {
		this.executeDate = executeDate;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescriptionTestsuite() {
		return descriptionTestsuite;
	}

	public void setDescriptionTestsuite(String descriptionTestsuite) {
		this.descriptionTestsuite = descriptionTestsuite;
	}

	public int getCountDegrade() {
		return countDegrade;
	}

	public void setCountDegrade(int countDegrade) {
		this.countDegrade = countDegrade;
	}
	
}

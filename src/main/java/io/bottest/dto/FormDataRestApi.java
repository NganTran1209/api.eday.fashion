package io.bottest.dto;

public class FormDataRestApi {
	private String key;
	private String value;
	private String type;
	private String pathDirectory;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getPathDirectory() {
		return pathDirectory;
	}
	public void setPathDirectory(String pathDirectory) {
		this.pathDirectory = pathDirectory;
	}
	
}

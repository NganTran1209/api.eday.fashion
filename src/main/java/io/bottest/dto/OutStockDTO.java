package io.bottest.dto;

public class OutStockDTO {

	private Float amount;
	private Float price;
	private Float toMoney;
	
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Float getToMoney() {
		return toMoney;
	}
	public void setToMoney(Float toMoney) {
		this.toMoney = toMoney;
	}
	@Override
	public String toString() {
		return "OutStockDTO [amount=" + amount + ", price=" + price + ", toMoney=" + toMoney + "]";
	}
	
}

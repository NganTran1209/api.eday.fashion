package io.bottest.dto;

import java.util.Date;


public class ProjectProgressResultInfoDTO {
	private Long project_idTc;
	
	private String testcase;
	
	private String testsuite;
	
	private String assign_to;
	
	private Date create_date;
	
	private String description;
	
	private String statusTc;
	
	private String testscript;
	
	private Date update_date;
	
	private String update_user;
	
	private String expected_outputTc;
	
	private String pre_condition;
	
	private String testcase_procedure;
	
	private Long id;
	
	private Date execute_date;
	
	private Long project_id;
	
	private String result_name;
	
	private String running_user;
	
	private String test_case_name;
	
	private String test_case_result;
	
	private String test_result;
	
	private String test_suite;
	
	private String actual_output;
	
	private String browser;
	
	private String device_name;
	
	private String expected_output;
	
	private String location;
	
	private String milestone;
	
	private String mobile_mode;
	
	private String os;
	
	private String running_description;
	
	private String screen_resolution;
	
	private String type;
	
	private String files;
	
	private String status;

	public Long getProject_idTc() {
		return project_idTc;
	}

	public void setProject_idTc(Long project_idTc) {
		this.project_idTc = project_idTc;
	}

	public String getTestcase() {
		return testcase;
	}

	public void setTestcase(String testcase) {
		this.testcase = testcase;
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}

	public String getAssign_to() {
		return assign_to;
	}

	public void setAssign_to(String assign_to) {
		this.assign_to = assign_to;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatusTc() {
		return statusTc;
	}

	public void setStatusTc(String statusTc) {
		this.statusTc = statusTc;
	}

	public String getTestscript() {
		return testscript;
	}

	public void setTestscript(String testscript) {
		this.testscript = testscript;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getUpdate_user() {
		return update_user;
	}

	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}

	public String getExpected_outputTc() {
		return expected_outputTc;
	}

	public void setExpected_outputTc(String expected_outputTc) {
		this.expected_outputTc = expected_outputTc;
	}

	public String getPre_condition() {
		return pre_condition;
	}

	public void setPre_condition(String pre_condition) {
		this.pre_condition = pre_condition;
	}

	public String getTestcase_procedure() {
		return testcase_procedure;
	}

	public void setTestcase_procedure(String testcase_procedure) {
		this.testcase_procedure = testcase_procedure;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getExecute_date() {
		return execute_date;
	}

	public void setExecute_date(Date execute_date) {
		this.execute_date = execute_date;
	}

	public Long getProject_id() {
		return project_id;
	}

	public void setProject_id(Long project_id) {
		this.project_id = project_id;
	}

	public String getResult_name() {
		return result_name;
	}

	public void setResult_name(String result_name) {
		this.result_name = result_name;
	}

	public String getRunning_user() {
		return running_user;
	}

	public void setRunning_user(String running_user) {
		this.running_user = running_user;
	}

	public String getTest_case_name() {
		return test_case_name;
	}

	public void setTest_case_name(String test_case_name) {
		this.test_case_name = test_case_name;
	}

	public String getTest_case_result() {
		return test_case_result;
	}

	public void setTest_case_result(String test_case_result) {
		this.test_case_result = test_case_result;
	}

	public String getTest_result() {
		return test_result;
	}

	public void setTest_result(String test_result) {
		this.test_result = test_result;
	}

	public String getTest_suite() {
		return test_suite;
	}

	public void setTest_suite(String test_suite) {
		this.test_suite = test_suite;
	}

	public String getActual_output() {
		return actual_output;
	}

	public void setActual_output(String actual_output) {
		this.actual_output = actual_output;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getDevice_name() {
		return device_name;
	}

	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}

	public String getExpected_output() {
		return expected_output;
	}

	public void setExpected_output(String expected_output) {
		this.expected_output = expected_output;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

	public String getMobile_model() {
		return mobile_mode;
	}

	public void setMobile_model(String mobile_mode) {
		this.mobile_mode = mobile_mode;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getRunning_description() {
		return running_description;
	}

	public void setRunning_description(String running_description) {
		this.running_description = running_description;
	}

	public String getScreen_resolution() {
		return screen_resolution;
	}

	public void setScreen_resolution(String screen_resolution) {
		this.screen_resolution = screen_resolution;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}

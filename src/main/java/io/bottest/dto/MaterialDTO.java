package io.bottest.dto;

public class MaterialDTO {

	private String id;
	private String category;
	private String image;
	private String title;
	private String supplier;
	private String price;
	private String amount;
	private String inputDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		String arrayInPutDate[] = inputDate.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayInPutDate.length; i++) {
			sb.append(arrayInPutDate[i]);
		}
		this.inputDate = sb.toString();
	}	
}

package io.bottest.dto;

import java.util.List;

import io.bottest.jpa.entity.Project;
import io.bottest.jpa.entity.Task;

public class QuyTrinhDTO {

	private Project project;

	private List<Task> taskList;

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

}

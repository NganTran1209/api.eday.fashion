package io.bottest.dto;

import java.util.List;

public class FilterTaskDTO {
	private List<String> tracker;
	
	private List<String> status;
	
	private List<String> priority;
	
	private List<String> category;
	
	private List<String> assignee;
	
	private String planStartDate;
	
	private String planEndDate;
	
	private List<String> testsuite;
	
	private List<String> milestone;
	
	private String description;

	public List<String> getTracker() {
		return tracker;
	}

	public void setTracker(List<String> tracker) {
		this.tracker = tracker;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public List<String> getPriority() {
		return priority;
	}

	public void setPriority(List<String> priority) {
		this.priority = priority;
	}

	public String getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(String planStartDate) {
		this.planStartDate = planStartDate;
	}

	public String getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}

	public List<String> getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(List<String> testsuite) {
		this.testsuite = testsuite;
	}

	public List<String> getMilestone() {
		return milestone;
	}

	public void setMilestone(List<String> milestone) {
		this.milestone = milestone;
	}

	public List<String> getAssignee() {
		return assignee;
	}

	public void setAssignee(List<String> assignee) {
		this.assignee = assignee;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getCategory() {
		return category;
	}

	public void setCategory(List<String> category) {
		this.category = category;
	}
	
}

package io.bottest.dto;

import java.util.List;

public class FilterSupplieDTO {

	private List<String> type;
	
	private String fromDate;
	
	private String toDate;

	public List<String> getType() {
		return type;
	}

	public void setType(List<String> type) {
		this.type = type;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	@Override
	public String toString() {
		return "FilterSupplieDTO [type=" + type + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
	
}

package io.bottest.dto;

import java.util.List;

public class FilterWareHouseDTO {

	private String name; 
	
	private List<Integer> catId ;
	
	private Long amount;
	
	private Long profit;
	
	private int compareProfit;
	
	private Long stockQuantityFrom;
	
	private Long stockQuantityTo;
	
	private String createdDateFrom;
	
	private String createdDateTo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getCatId() {
		return catId;
	}

	public void setCatId(List<Integer> catId) {
		this.catId = catId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getProfit() {
		return profit;
	}

	public void setProfit(Long profit) {
		this.profit = profit;
	}

	public int getCompareProfit() {
		return compareProfit;
	}

	public void setCompareProfit(int compareProfit) {
		this.compareProfit = compareProfit;
	}

	public Long getStockQuantityFrom() {
		return stockQuantityFrom;
	}

	public void setStockQuantityFrom(Long stockQuantityFrom) {
		this.stockQuantityFrom = stockQuantityFrom;
	}

	public Long getStockQuantityTo() {
		return stockQuantityTo;
	}

	public void setStockQuantityTo(Long stockQuantityTo) {
		this.stockQuantityTo = stockQuantityTo;
	}

	public String getCreatedDateFrom() {
		return createdDateFrom;
	}

	public void setCreatedDateFrom(String createdDateFrom) {
		this.createdDateFrom = createdDateFrom;
	}

	public String getCreatedDateTo() {
		return createdDateTo;
	}

	public void setCreatedDateTo(String createdDateTo) {
		this.createdDateTo = createdDateTo;
	}
	
	
}

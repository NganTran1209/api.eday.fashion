package io.bottest.dto;

public class SellManagerDTO {
	
	private int id;
	private String sellDate;
	private Float amount;
	private Float price;
	private Float toMoney;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSellDate() {
		return sellDate;
	}
	public void setSellDate(String sellDate) {
		this.sellDate = sellDate;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Float getToMoney() {
		return toMoney;
	}
	public void setToMoney(Float toMoney) {
		this.toMoney = toMoney;
	}
	@Override
	public String toString() {
		return "SellManagerDTO [id=" + id + ", sellDate=" + sellDate + ", amount=" + amount + ", price=" + price + ", toMoney=" + toMoney + "]";
	}
	
}

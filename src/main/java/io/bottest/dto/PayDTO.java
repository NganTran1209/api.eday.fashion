package io.bottest.dto;

import io.bottest.jpa.entity.Work;

public class PayDTO {

	private String month;
	private String unitSalary;
	private String hourOfDay;
	private String salaryToMonth;
	private String contractSalary;
	private String[] dayOff;
	private String[] dayWork;
	private String status;
	private String bonus;
	private String totalSalary;
	private String comment;
	private Work work;
	
}

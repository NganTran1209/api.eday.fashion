package io.bottest.dto;

import java.util.List;

public class FilterConditionData {
	private List<String> testsuiteName;
	
	private List<String> executeBy;
	
	private String statusTestcase;
	
	private String fromDate;
	
	private String toDate;
	
	private List<String> assignTo;
	
	private String type;
	
	private List<String> createBy;
	
	private String createDate;
	
	private List<String> typeData;
	
	private String fileName;
	
	private List<String> milestone;
	
	private List<String> statusIssue;
	
	private List<String> categoryIssue;
	
	private String planStartDate;
	
	private String planEndDate;

	public List<String> getTestsuiteName() {
		return testsuiteName;
	}

	public void setTestsuiteName(List<String> testsuiteName) {
		this.testsuiteName = testsuiteName;
	}

	public List<String> getExecuteBy() {
		return executeBy;
	}

	public void setExecuteBy(List<String> executeBy) {
		this.executeBy = executeBy;
	}

	public String getStatusTestcase() {
		return statusTestcase;
	}

	public void setStatusTestcase(String statusTestcase) {
		this.statusTestcase = statusTestcase;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<String> getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(List<String> assignTo) {
		this.assignTo = assignTo;
	}

	public List<String> getCreateBy() {
		return createBy;
	}

	public void setCreateBy(List<String> createBy) {
		this.createBy = createBy;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getMilestone() {
		return milestone;
	}

	public void setMilestone(List<String> milestone) {
		this.milestone = milestone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getTypeData() {
		return typeData;
	}

	public void setTypeData(List<String> typeData) {
		this.typeData = typeData;
	}

	public List<String> getStatusIssue() {
		return statusIssue;
	}

	public void setStatusIssue(List<String> statusIssue) {
		this.statusIssue = statusIssue;
	}

	public String getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(String planStartDate) {
		this.planStartDate = planStartDate;
	}

	public String getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}

	public List<String> getCategoryIssue() {
		return categoryIssue;
	}

	public void setCategoryIssue(List<String> categoryIssue) {
		this.categoryIssue = categoryIssue;
	}
}

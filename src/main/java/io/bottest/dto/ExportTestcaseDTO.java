package io.bottest.dto;

public class ExportTestcaseDTO {
	
	private String testsuite;
	
	private String testcase;
	
	private String testcaseProcedure;
	
	private String expectedOutput;
	
	private String lastRun;

	public ExportTestcaseDTO() {
		super();
	}

	public String getTestsuite() {
		return testsuite;
	}

	public void setTestsuite(String testsuite) {
		this.testsuite = testsuite;
	}
	
	public String getTestcase() {
		return testcase;
	}

	public void setTestcase(String testcase) {
		this.testcase = testcase;
	}

	public String getTestcaseProcedure() {
		return testcaseProcedure;
	}

	public void setTestcaseProcedure(String testcaseProcedure) {
		this.testcaseProcedure = testcaseProcedure;
	}

	public String getExpectedOutput() {
		return expectedOutput;
	}

	public void setExpectedOutput(String expectedOutput) {
		this.expectedOutput = expectedOutput;
	}

	public String getLastRun() {
		return lastRun;
	}

	public void setLastRun(String lastRun) {
		this.lastRun = lastRun;
	}
	
}

package io.bottest.dto;

import java.io.Serializable;

public class ProjectMilestoneDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8351790473947314905L;
	
	private Long projectId;
	
	private String mileStoneName;
	
	private String startDate;
	
	private String endDate;
	
	private String description;
	
	private String testcaseName;
	
	private String status;
	
	private int testcasePass;
	
	private int testcaseFail;
	
	private int testcaseNotRun;
	
	private int testcaseDegrade;
	
	private int totalTestcase;
	private String statusMilestone;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getMileStoneName() {
		return mileStoneName;
	}

	public void setMileStoneName(String mileStoneName) {
		this.mileStoneName = mileStoneName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTestcaseName() {
		return testcaseName;
	}

	public void setTestcaseName(String testcaseName) {
		this.testcaseName = testcaseName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTestcasePass() {
		return testcasePass;
	}

	public void setTestcasePass(int testcasePass) {
		this.testcasePass = testcasePass;
	}

	public int getTestcaseFail() {
		return testcaseFail;
	}

	public void setTestcaseFail(int testcaseFail) {
		this.testcaseFail = testcaseFail;
	}

	public int getTestcaseNotRun() {
		return testcaseNotRun;
	}

	public void setTestcaseNotRun(int testcaseNotRun) {
		this.testcaseNotRun = testcaseNotRun;
	}

	public int getTestcaseDegrade() {
		return testcaseDegrade;
	}

	public void setTestcaseDegrade(int testcaseDegrade) {
		this.testcaseDegrade = testcaseDegrade;
	}

	public int getTotalTestcase() {
		return totalTestcase;
	}

	public void setTotalTestcase(int totalTestcase) {
		this.totalTestcase = totalTestcase;
	}

	public String getStatusMilestone() {
		return statusMilestone;
	}

	public void setStatusMilestone(String statusMilestone) {
		this.statusMilestone = statusMilestone;
	}
	
}

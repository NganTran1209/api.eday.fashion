package io.bottest.dto;

public class ShipmentAddressDTO {

	private String username;
	
	private String seq;
	
	private String nameReceive;
	
	private String addressReceive;
	
	private String city;
	
	private String districts;
	
	private String streets;
	
	private String wards;
	
	private String mail;
	
	private String phoneNumber;
	
	private String addressType; //nha rieng, cty
	
	private String createdDate;
	
	private String updatedDate;
	
	private String isDefault;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getNameReceive() {
		return nameReceive;
	}

	public void setNameReceive(String nameReceive) {
		this.nameReceive = nameReceive;
	}

	public String getAddressReceive() {
		return addressReceive;
	}

	public void setAddressReceive(String addressReceive) {
		this.addressReceive = addressReceive;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistricts() {
		return districts;
	}

	public void setDistricts(String districts) {
		this.districts = districts;
	}

	public String getStreets() {
		return streets;
	}

	public void setStreets(String streets) {
		this.streets = streets;
	}

	public String getWards() {
		return wards;
	}

	public void setWards(String wards) {
		this.wards = wards;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		String arrayCreatedDate[] = createdDate.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayCreatedDate.length; i ++) {
			sb.append(arrayCreatedDate[i]);
		}
		this.createdDate = sb.toString();
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		String arrayUpdatedDate[] = updatedDate.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayUpdatedDate.length; i ++) {
			sb.append(arrayUpdatedDate[i]);
		}
		this.updatedDate = sb.toString();
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	
}

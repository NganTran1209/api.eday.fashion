package io.bottest.dto;

public class CopyLayout {

	private String testsuiteOld;
	
	private String testsuiteNew;
	
	private String layoutNameOld;
	
	private String layoutNameNew;

	public String getTestsuiteOld() {
		return testsuiteOld;
	}

	public void setTestsuiteOld(String testsuiteOld) {
		this.testsuiteOld = testsuiteOld;
	}

	public String getTestsuiteNew() {
		return testsuiteNew;
	}

	public void setTestsuiteNew(String testsuiteNew) {
		this.testsuiteNew = testsuiteNew;
	}

	public String getLayoutNameOld() {
		return layoutNameOld;
	}

	public void setLayoutNameOld(String layoutNameOld) {
		this.layoutNameOld = layoutNameOld;
	}

	public String getLayoutNameNew() {
		return layoutNameNew;
	}

	public void setLayoutNameNew(String layoutNameNew) {
		this.layoutNameNew = layoutNameNew;
	}
	
	
}

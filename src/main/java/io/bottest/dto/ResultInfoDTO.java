package io.bottest.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class ResultInfoDTO {
	private Long id;
	
	private String resultName;
	
	private String testSuite;
	
	private String testResult;
	
	private String manualRunningUser;
	
	private String datetimeLastRun;
	
	private String testCaseName;
	
	private String manualStatus;
	
	private int projectId;
	
	private String mileStoneId;
	
	private String manualOS;
	
	private String manualBrowser;
	
	private String manualResolution;
	
	private String manualLocation;
	
	private String mobileMode;
	
	private String manualDeviceName;
	
	private String manualDescription;
	
	private String manualExpectedOutput;
	
	private String manualActualOutput;
	
	private String type;
	
	private List<MultipartFile> myfile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	public String getTestSuite() {
		return testSuite;
	}

	public void setTestSuite(String testSuite) {
		this.testSuite = testSuite;
	}

	public String getTestResult() {
		return testResult;
	}

	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}

	public String getManualRunningUser() {
		return manualRunningUser;
	}

	public void setManualRunningUser(String manualRunningUser) {
		this.manualRunningUser = manualRunningUser;
	}

	public String getDatetimeLastRun() {
		return datetimeLastRun;
	}

	public void setDatetimeLastRun(String datetimeLastRun) {
		this.datetimeLastRun = datetimeLastRun;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getManualStatus() {
		return manualStatus;
	}

	public void setManualStatus(String manualStatus) {
		this.manualStatus = manualStatus;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getMileStoneId() {
		return mileStoneId;
	}

	public void setMileStoneId(String mileStoneId) {
		this.mileStoneId = mileStoneId;
	}

	public String getManualOS() {
		return manualOS;
	}

	public void setManualOS(String manualOS) {
		this.manualOS = manualOS;
	}

	public String getManualBrowser() {
		return manualBrowser;
	}

	public void setManualBrowser(String manualBrowser) {
		this.manualBrowser = manualBrowser;
	}

	public String getManualResolution() {
		return manualResolution;
	}

	public void setManualResolution(String manualResolution) {
		this.manualResolution = manualResolution;
	}

	public String getManualLocation() {
		return manualLocation;
	}

	public void setManualLocation(String manualLocation) {
		this.manualLocation = manualLocation;
	}

	public String getMobileMode() {
		return mobileMode;
	}

	public void setMobileMode(String mobileMode) {
		this.mobileMode = mobileMode;
	}

	public String getManualDeviceName() {
		return manualDeviceName;
	}

	public void setManualDeviceName(String manualDeviceName) {
		this.manualDeviceName = manualDeviceName;
	}

	public String getManualDescription() {
		return manualDescription;
	}

	public void setManualDescription(String manualDescription) {
		this.manualDescription = manualDescription;
	}

	public String getManualExpectedOutput() {
		return manualExpectedOutput;
	}

	public void setManualExpectedOutput(String manualExpectedOutput) {
		this.manualExpectedOutput = manualExpectedOutput;
	}

	public String getManualActualOutput() {
		return manualActualOutput;
	}

	public void setManualActualOutput(String manualActualOutput) {
		this.manualActualOutput = manualActualOutput;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<MultipartFile> getMyfile() {
		return myfile;
	}

	public void setMyfile(List<MultipartFile> myfile) {
		this.myfile = myfile;
	}

}

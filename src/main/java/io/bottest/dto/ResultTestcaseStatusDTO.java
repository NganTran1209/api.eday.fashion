package io.bottest.dto;

public class ResultTestcaseStatusDTO {
	private String name;
	
	private String result;

	private String statusRun;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getStatusRun() {
		return statusRun;
	}

	public void setStatusRun(String statusRun) {
		this.statusRun = statusRun;
	}
	
}

package io.bottest.dto;

public class TestDataDTO {
	String filename;
	String updateDate;
	boolean isLayoutData;
	String description;
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public boolean isLayoutData() {
		return isLayoutData;
	}
	public void setLayoutData(boolean isLayoutData) {
		this.isLayoutData = isLayoutData;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "TestDataDTO [filename=" + filename + ", updateDate=" + updateDate + ", isLayoutData=" + isLayoutData
				+ description+ "]";
	}
	
	
	
}

package io.bottest.dto;

public class FilterQuyTrinhDTO {

	private double estimatedCost;

	private String agricultureSector;

	private String estimatedDuration;

	private String name;

	private String standardGAP;

	private String description;

	private int fromIndex;

	private int size;

	private String GPSLocation;

	public double getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(double estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getAgricultureSector() {
		return agricultureSector;
	}

	public void setAgricultureSector(String agricultureSector) {
		this.agricultureSector = agricultureSector;
	}

	public String getEstimatedDuration() {
		return estimatedDuration;
	}

	public void setEstimatedDuration(String estimatedDuration) {
		this.estimatedDuration = estimatedDuration;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStandardGAP() {
		return standardGAP;
	}

	public void setStandardGAP(String standardGAP) {
		this.standardGAP = standardGAP;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getFromIndex() {
		return fromIndex;
	}

	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getGPSLocation() {
		return GPSLocation;
	}

	public void setGPSLocation(String gPSLocation) {
		GPSLocation = gPSLocation;
	}

}

package io.bottest.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.jpa.entity.InvitedToProject;
import io.bottest.jpa.entity.Task;
import io.bottest.jpa.entity.UpdateTask;
import io.bottest.jpa.entity.UserProject;
import io.bottest.utils.TokenUtil;

@Service
public class SendMailService {

//	private final Logger LOGGER = LogManager.getLogger(SendMailService.class);
//	
//	@Value("${security.jwt.token.secret-key:Bottest@Paracel}")
//	private String secretKey = "Bottest@Paracel";
//	
//	@Value("${bottest.url}")
//	private String url;
//	
//	@Autowired
//    private JavaMailSender javaMailSender;
//	
//	@Autowired
//	private ProjectService projectService;
//
//	/**
//	 * 
//	 * @param sendMail
//	 * @param host
//	 * @param from
//	 * @param pass
//	 * @param port
//	 * @param domain
//	 */
//	public void sendInviteProjectEmail(InvitedToProject sendMail) {
//		
//		LOGGER.info("Begin Service SendMailService Function sendInviteProjectEmail");
//		LOGGER.info("Service SendMailService Function sendInviteProjectEmail PARAMS sendMail: " + new Gson().toJson(sendMail, new TypeToken<InvitedToProject>() {}.getType()));
//		try {
//			String toEmail = sendMail.getToEmail();
//			String content = sendMail.getContent();
//			String projectRole = sendMail.getProjectRole();
//			String subject = sendMail.getTitle();
//			String projectId = sendMail.getProjectId();
//			String projectName = sendMail.getProjectName();
//			String iviterEmail = sendMail.getInviterEmail();
//
//			String[] to = toEmail.split(",");
//
//			File file = new File(getClass().getClassLoader().getResource("mail.html").getFile());
//			
//			String fileStr = file.toString();
//			String fileDecode = URLDecoder.decode(fileStr, "UTF-8");
//			File fileFinal = new File(fileDecode);
//			
//			BufferedReader in = new BufferedReader(new FileReader(fileFinal));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
//
//			for (int j = 0; j < to.length; j++) { // changed from a while loop
//				
//				UserProject userP = projectService.checkDataInUserProject(Long.parseLong(projectId), projectName, to[j]);
//				if(userP == null) {
//					String contentMail = contentBuilder.toString();
//					String contentEmail = contentMail.replace("paracel", content);
////					SimpleMailMessage message = new SimpleMailMessage();
//					String messageMail = contentEmail;
//					
//					MimeMessage message = javaMailSender.createMimeMessage();
//	                MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");
//	                
//					
//					String pattern = "dd/MM/yyyy HH:mm:ss";
//					Date date = new Date();
//					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//					String dateCreate = simpleDateFormat.format(date);
//					
//					
//					String token = "&projectId=" + projectId + "&toEmail=" + to[j] + "&projectRole=" + projectRole
//							+ "&createDate=" + dateCreate;
//					token = TokenUtil.encrypt(token, secretKey);
//					
//					String contentProject = messageMail.replaceAll("nameproject", projectName);
//					
//					String contentEmailName = contentProject.replaceAll("emailname", iviterEmail);
//					
//					String contentSSS = contentEmailName.replaceAll("http://localhost:8080/bottest/joinProject", this.url + "joinProject?token=" + token);
//					
//					String userNotExist = contentSSS.replaceAll("http://localhost:8080/bottest/signup",
//							this.url + "invitedProject?token=" + token);
//					String urlBottest = userNotExist.replaceAll("http://localhost:8080/bottest", this.url);
//					
//					message.setContent(urlBottest, "text/html");
//	                helper.setTo(to[j]);
//	                helper.setSubject(subject);
//	                javaMailSender.send(message);
//				}
//			}
//			
//		} catch (Exception e) {
//			LOGGER.error("Service SendMailService Function sendInviteProjectEmail ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//		}
//		
//		LOGGER.info("End Service SendMailService Function sendInviteProjectEmail");
//	}
//
//	/**
//	 * @param host
//	 * @param from
//	 * @param pass
//	 * @param port
//	 * @param domain
//	 * @param toEmail
//	 */
//	public void sendActiveEmail(String toEmail) {
//
//		LOGGER.info("Begin Service SendMailService Function sendActiveEmail");
//		LOGGER.info("Service SendMailService Function sendActiveEmail PARAMS toEmail: " +toEmail);
//		try {
//
//			File file = new File(getClass().getClassLoader().getResource("mailActive.html").getFile());
//			String fileStr = file.toString();
//			String fileDecode = URLDecoder.decode(fileStr, "UTF-8");
//			File fileFinal = new File(fileDecode);
//			
//			BufferedReader in = new BufferedReader(new FileReader(fileFinal));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
//
////			SimpleMailMessage message = new SimpleMailMessage();
//			
//			MimeMessage msg = javaMailSender.createMimeMessage();
//
//	        // true = multipart message
//	        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//	        
//			//message.setFrom(new InternetAddress(from));
//	        helper.setTo(toEmail);
//	        helper.setSubject("Complete Registration!");
//			String pattern = "dd/MM/yyyy HH:mm:ss";
//			Date date = new Date();
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//			String dateCreate = simpleDateFormat.format(date);
//			String token = "&email=" + toEmail + "&dateCreate=" + dateCreate;
//			String contentMail = contentBuilder.toString();
//
//			token = TokenUtil.encrypt(token, secretKey);
//			String contentEmail = contentMail.replace("paracel", this.url +
//					"activeAccount?token=" + token);
//			String urlContent = contentEmail.replaceAll("http://localhost:8080/bottest", this.url);
//			helper.setText(urlContent,true);
//			javaMailSender.send(msg);
//			
//
//		} catch (Exception e) {
//			LOGGER.error("Service SendMailService Function sendActiveEmail ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//		}
//		
//		LOGGER.info("End Service SendMailService Function sendActiveEmail");
//	}
//
//	/**
//	 * @param host
//	 * @param from
//	 * @param pass
//	 * @param port
//	 * @param domain
//	 * @param toEmail
//	 */
//	public void sendMailForgotPassword(String toEmail) {
//
//		LOGGER.info("Begin Service SendMailService Function sendMailForgotPassword");
//		LOGGER.info("Service SendMailService Function sendMailForgotPassword PARAMS toEmail: " +toEmail);
//		try {
//
//			File file = new File(getClass().getClassLoader().getResource("forgotPass.html").getFile());
//			
//			String fileStr = file.toString();
//			String fileDecode = URLDecoder.decode(fileStr, "UTF-8");
//			File fileFinal = new File(fileDecode);
//			
//			BufferedReader in = new BufferedReader(new FileReader(fileFinal));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
////			SimpleMailMessage message = new SimpleMailMessage();
//			MimeMessage message = javaMailSender.createMimeMessage();
//			MimeMessageHelper helper = new MimeMessageHelper(message, true);
//			helper.setTo(toEmail);
//			helper.setSubject("Forgot your account!");
//			String pattern = "dd/MM/yyyy HH:mm:ss";
//			Date date = new Date();
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//			String dateCreate = simpleDateFormat.format(date);
//			String token = "&email=" + toEmail + "&dateCreate=" + dateCreate + "&code="
//					+ UUID.randomUUID().toString();
//			String contentMail = contentBuilder.toString();
//			token = TokenUtil.encrypt(token, secretKey);
//			String contentEmail = contentMail.replace("paracel", this.url +
//					"forgotPass?forgotinfo=" + token);
//			String urlContent = contentEmail.replaceAll("http://localhost:8080/bottest", this.url);
//			helper.setText(urlContent, true);
//			javaMailSender.send(message);
//			
//		} catch (Exception e) {
//			LOGGER.error("Service SendMailService Function sendMailForgotPassword ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//		}
//		
//		LOGGER.info("End Service SendMailService Function sendMailForgotPassword");
//	}
//	
//	public void sendMailForgotPasswordAttachOPTCode(String toEmail, String otpCode) {
//
//		LOGGER.info("Begin Service SendMailService Function sendMailForgotPassword");
//		LOGGER.info("Service SendMailService Function sendMailForgotPassword PARAMS toEmail: " +toEmail);
//		try {
//
//			File file = new File(getClass().getClassLoader().getResource("forgotPass.html").getFile());
//			
//			String fileStr = file.toString();
//			String fileDecode = URLDecoder.decode(fileStr, "UTF-8");
//			File fileFinal = new File(fileDecode);
//			
//			BufferedReader in = new BufferedReader(new FileReader(fileFinal));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
////			SimpleMailMessage message = new SimpleMailMessage();
//			MimeMessage message = javaMailSender.createMimeMessage();
//			MimeMessageHelper helper = new MimeMessageHelper(message, true);
//			helper.setTo(toEmail);
//			helper.setSubject("Forgot your account!");
//			String pattern = "dd/MM/yyyy HH:mm:ss";
//			Date date = new Date();
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//			String dateCreate = simpleDateFormat.format(date);
//			String token = "&email=" + toEmail + "&dateCreate=" + dateCreate + "&code="
//					+ UUID.randomUUID().toString();
//			String contentMail = contentBuilder.toString();
//			token = TokenUtil.encrypt(token, secretKey);
//			String contentEmail = contentMail.replace("paracel", this.url +
//					"forgotPass?forgotinfo=" + token);
//			String urlContent = contentEmail.replaceAll("http://localhost:8080/bottest", this.url);
//			helper.setText(urlContent, true);
//			javaMailSender.send(message);
//			
//		} catch (Exception e) {
//			LOGGER.error("Service SendMailService Function sendMailForgotPassword ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//		}
//		
//		LOGGER.info("End Service SendMailService Function sendMailForgotPassword");
//	}
//	
//	public void sendMailIssue(Task task,List<String> userProject,String projectName, UpdateTask content) {
//		try {
//           
//            File file = new File(getClass().getClassLoader().getResource("issue.html").getFile());
//			String fileStr = file.toString();
//			String fileDecode = URLDecoder.decode(fileStr, "UTF-8");
//			File fileFinal = new File(fileDecode);
//			
//			BufferedReader in = new BufferedReader(new FileReader(fileFinal));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
//            for(int i = 0 ; i < userProject.size() ; i++) {	
//    			String contentMail = contentBuilder.toString();
//    			String contentEmail = contentMail.replace("paracel", this.url + "project/" + task.getProjectId().toString() +"/issue/"+task.getId().toString());
//
//    			String messageMail = contentEmail;
//    			
//    			MimeMessage message = javaMailSender.createMimeMessage();
//                MimeMessageHelper helper = new MimeMessageHelper(message, true);          
//    			
//    			String pattern = "dd/MM/yyyy HH:mm:ss";
//    			Date date = new Date();
//    			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//    			String dateCreate = simpleDateFormat.format(date);
//    			
//    			
//    			String token = "&projectId=" + task.getProjectId() + "&toEmail=" + task.getAssignee() + "&createDate=" + dateCreate;
//    			token = TokenUtil.encrypt(token, secretKey);
//    		
//    			String nameProject = messageMail.replace("projectName", projectName);
//    			String inFSubject = nameProject.replaceAll("subject", task.getSubject());
//    			String headerContent = inFSubject.replace("headerContent", "<div style='color:black'><p style='font-size: 21px; margin: 0 0 25px 0;"
//    														+ " text-transform: uppercase;text-align: center; ' class='d-flex'>"
//									    					+ " Project:  <b class='m-0'>" + projectName + "</b></p>"
//									    					+ "<p class='d-flex' style='margin-bottom:10px; font-size: 18px;' > Issue:"
//									    					+ " <b class='m-0'>#" + task.getId() +". " + task.getSubject()+ "</b></p></div>");
//    			String contentInf = " <div style='margin-left:40px; color:black'>";
//    			String createBy = "";
//    			if(content != null) {
//    				String contentTask = content.getContentTask();
//					createBy += "<div style='margin: 0 0 10px 40px;  font-size: 13px;'>Update By " + content.getUpdateBy() + " on " + simpleDateFormat.format(content.getUpdateDate()) + "</div>";
//					String updateBy = headerContent.replaceAll("createBy", createBy);
//					if(!contentTask.equals("{}")) {
//    					int charN = contentTask.lastIndexOf(","); 
//    					String getContent = contentTask.substring(0,charN) + contentTask.substring(charN+1);
//    					try {
//    						JSONObject json = new JSONObject(getContent);
//    						Iterator<String> keys = json.keys();
//    						while(keys.hasNext()) {
//    						    String key = keys.next();
//    						    contentInf +=  "<strong>- " + key + "</strong>" + json.get(key) + "<br>";
//    						}
//    					} catch (JSONException e) {
//    						// TODO Auto-generated catch block
//    						e.printStackTrace();
//    					}
//					}
//					contentInf += "</div>";
//					String contentUpdate = updateBy.replace("contentInf" , contentInf.replace("&nbsp","       "));
//					helper.setText(contentUpdate, true);
//					helper.setTo(userProject.get(i));
//    				
//    			} else {
//    				contentInf += "</div>";
//    				createBy += "<div style='margin-left: 20px;'>Create By " + task.getCreateBy() + " on " + simpleDateFormat.format(task.getCreateDate()) + "</div>";
//    				String updateBy = headerContent.replaceAll("createBy", createBy);
//    				String contentCreate = updateBy.replace("contentInf" , contentInf);
//    				helper.setText(contentCreate, true);
//    				helper.setTo(userProject.get(i));
//    			} 
//    			helper.setSubject(projectName + "   " + task.getSubject());
//                javaMailSender.send(message);
//            }
//            
//		} catch (Exception e) {
//			LOGGER.error("Service SendMailService Function sendMailIssue ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//		}
//	}	
}



package io.bottest.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.dto.FilterSupplieDTO;
import io.bottest.dto.KhoNongSanDTO;
import io.bottest.dto.OrderDTO;
import io.bottest.dto.OutStockDTO;
import io.bottest.dto.SellManagerDTO;
import io.bottest.jpa.respository.KhoNongSanRepository;
import io.bottest.jpa.respository.ProjectRepository;
import io.bottest.jpa.respository.SupplieCustom;
import io.bottest.jpa.respository.WorkRepository;

@Service
@Transactional
public class KhoNongSanService {
	
	private final Logger LOGGER = LogManager.getLogger(ProjectService.class);
	
	@Autowired
	KhoNongSanRepository khoRepo;
	
	@Autowired 
	WorkRepository workRepo;
	
	@Autowired
	SupplieCustom supplieCustom;

	@Autowired
	ProjectRepository projectRepo;
	public List<KhoNongSanDTO> getListKhoNongSan(String user) throws ParseException {
		LOGGER.info("Begin Service KhoNongSanService Function getListKhoNongSan");
		LOGGER.info("Service KhoNongSanService Functon getListKhoNongSan PARAM userId: " + user);
		
		List<KhoNongSanDTO> listKho = new ArrayList<KhoNongSanDTO>();
		List<Map<String, Object>> listProduct = khoRepo.getListProduct(user);
		
		if(listProduct != null) {
			for(int i=0; i<listProduct.size(); i++) {
				KhoNongSanDTO khoDTO = new KhoNongSanDTO();
				String productId =(String) listProduct.get(i).get("PRODUCT_ID");
				khoDTO.setId(productId);
				khoDTO.setProductName((String) listProduct.get(i).get("PRODUCT_NAME"));
				String startDate = (listProduct.get(i).get("CREATED_DATE") != null) ? ((String) listProduct.get(i).get("CREATED_DATE").toString()) : null;
				khoDTO.setStartDay(startDate);
				String endDate = (listProduct.get(i).get("EXPIRE_DATE") != null) ? ((String) listProduct.get(i).get("EXPIRE_DATE").toString()) : null;
				khoDTO.setEndDate(endDate);
				khoDTO.setQuantity((String) listProduct.get(i).get("QUANTITY_INCLUDED"));
				khoDTO.setEstimatedOutput((String) listProduct.get(i).get("AMOUNT_UOM_TYPE_ID"));
				float inventory =(listProduct.get(i).get("QUANTITY_INCLUDED") != null && listProduct.get(i).get("AMOUNT_UOM_TYPE_ID") != null) ? ((float) listProduct.get(i).get("QUANTITY_INCLUDED") - (float) listProduct.get(i).get("AMOUNT_UOM_TYPE_ID")) : 0;
				khoDTO.setInventory(String.valueOf(inventory));
				khoDTO.setMadeIn((String) listProduct.get(i).get("BRAND_NAME"));
				khoDTO.setInvest((String) listProduct.get(i).get("SALE_PRICE"));
				boolean finish = (listProduct.get(i).get("IS_ACTIVE").equals("1")) ? true : false;
				khoDTO.setFinish(finish);
				List<SellManagerDTO> listSell = this.getListSell(productId);
				khoDTO.setSellManager(listSell);
				List<OrderDTO> listOrder = this.getListOrder(user, productId);
				khoDTO.setOrderedList(listOrder);
				listKho.add(khoDTO);
			}
		}
		
		LOGGER.info("Service KhoNongSanService Functon getListKhoNongSan RESULT listKho: " + listKho);
		return listKho;
	}

	private List<OrderDTO> getListOrder(String user, String productId) {
		List<OrderDTO> listOrder = new ArrayList<OrderDTO>();
		List<Map<String, Object>> list= projectRepo.orderList(String.valueOf(user), productId);
		if(list != null) {
			for(int i=0; i<list.size(); i++) {
				OrderDTO orderDTO = new OrderDTO();
				
				listOrder.add(orderDTO);
			}
		}
		return listOrder;
	}

	public List<SellManagerDTO> getListSell(String productId) throws ParseException {
		List<SellManagerDTO> listSell = new ArrayList<SellManagerDTO>();
		List<Map<String, Object>> list= khoRepo.getListSell(Long.parseLong(productId));
		if(list != null) {
			for(int i=0; i<list.size(); i++) {
				SellManagerDTO sellManagerDTO = new SellManagerDTO();
				sellManagerDTO.setId(Integer.parseInt((String) list.get(i).get("ORDER_ID").toString()));
				String sellDate = (list.get(i).get("CREATED_STAMP") != null) ? ((String) list.get(i).get("CREATED_STAMP").toString()) : null;
				sellManagerDTO.setSellDate(sellDate);
				sellManagerDTO.setAmount(Float.parseFloat((String) list.get(i).get("SELECTED_AMOUNT").toString()));
				sellManagerDTO.setPrice(Float.parseFloat((String) list.get(i).get("UNIT_PRICE").toString()));
				Float toMoney =( Float.parseFloat((String) list.get(i).get("SELECTED_AMOUNT").toString()) )* ( Float.parseFloat((String) list.get(i).get("UNIT_PRICE").toString()) );
				sellManagerDTO.setToMoney((float) (Math.round(toMoney*10)/10));
				listSell.add(sellManagerDTO);
			}
		}
		return listSell;
	}

	public List<KhoNongSanDTO> getKhoNS(String userName) throws ParseException {
		LOGGER.info("Begin Service KhoNongSanService Function getKhoNS");
		LOGGER.info("Service KhoNongSanService Functon getKhoNSFinish PARAM userName: " + userName);
		
		List<KhoNongSanDTO> listKhoNS = new ArrayList<KhoNongSanDTO>();
		List<Map<String, Object>> listProduct = khoRepo.getListProductKhoNs(userName, 0);
		
		if(listProduct != null) {
			for(int i=0; i<listProduct.size(); i++) {
				KhoNongSanDTO khoDTO = new KhoNongSanDTO();
				String productId =(String) listProduct.get(i).get("product_id");
				khoDTO.setId(productId);
				khoDTO.setProductName((String) listProduct.get(i).get("product_name"));
				String startDate = (listProduct.get(i).get("created_date") != null) ? ((String) listProduct.get(i).get("created_date").toString()) : null;
				khoDTO.setStartDay(startDate);
				String endDate = (listProduct.get(i).get("date_finish") != null) ? ((String) listProduct.get(i).get("date_finish").toString()) : null;
				khoDTO.setEndDate(endDate);
				khoDTO.setQuantity((String) listProduct.get(i).get("total_quantity").toString());
				khoDTO.setEstimatedOutput((String) listProduct.get(i).get("amount_uom_type_id"));
				BigInteger inventory = (BigInteger) listProduct.get(i).get("inventory");
				khoDTO.setInventory(String.valueOf(inventory));
				khoDTO.setMadeIn((String) listProduct.get(i).get("create_by"));
				boolean finish = (listProduct.get(i).get("is_active").equals("1")) ? true : false;
				khoDTO.setFinish(finish);
				List<SellManagerDTO> listSell = this.getListSell(productId);
				khoDTO.setSellManager(listSell);
				List<OrderDTO> listOrder = this.getListOrder(userName, productId);
				khoDTO.setOrderedList(listOrder);
				//Xuat kho
				OutStockDTO outStock  = new OutStockDTO();
				float amount = ( Float.parseFloat((String) listProduct.get(i).get("total_quantity").toString()) )- ( Float.parseFloat((String) listProduct.get(i).get("inventory").toString()) );
				outStock.setAmount(amount);
				float price = ((String) listProduct.get(i).get("product_code") != null ) ?(khoRepo.getPrice(Long.parseLong((String) listProduct.get(i).get("product_code"))) ): 0;
				outStock.setPrice(price);
				float toMoney = amount * price;
				outStock.setToMoney((float) (Math.round(toMoney*10)/10));
				khoDTO.setOutStock(outStock);
				listKhoNS.add(khoDTO);
			}
		}
		
		LOGGER.info("Service KhoNongSanService Functon getKhoNS RESULT listKhoNS: " + listKhoNS);
		return listKhoNS;
	}

	public List<KhoNongSanDTO> getKhoVatTu(String userName) {
		LOGGER.info("Begin Service KhoNongSanService Function getKhoVatTu");
		LOGGER.info("Service KhoNongSanService Functon getKhoVatTu PARAM userName: " + userName);
		
		List<KhoNongSanDTO> listKhoVatTu = new ArrayList<KhoNongSanDTO>();
		List<Map<String, Object>> listSuplies = khoRepo.getListProductKhoNs(userName, 1);
		
		if(listSuplies != null) {
			for(int i=0; i<listSuplies.size(); i++) {
				KhoNongSanDTO khoDTO = new KhoNongSanDTO();
				String productId =(String) listSuplies.get(i).get("product_id");
				khoDTO.setId(productId);
				khoDTO.setProductName((String) listSuplies.get(i).get("product_name"));
				String startDate = (listSuplies.get(i).get("created_date") != null) ? ((String) listSuplies.get(i).get("created_date").toString()) : null;
				khoDTO.setStartDay(startDate);
				String endDate = (listSuplies.get(i).get("date_finish") != null) ? ((String) listSuplies.get(i).get("date_finish").toString()) : null;
				khoDTO.setEndDate(endDate);
				String updateDate = (listSuplies.get(i).get("updated_date") != null) ? ((String) listSuplies.get(i).get("updated_date").toString()) : null;
				khoDTO.setUpdatedDate(updateDate);
				BigInteger inventory = (BigInteger) listSuplies.get(i).get("inventory");
				khoDTO.setInventory(String.valueOf(inventory));
				listKhoVatTu.add(khoDTO);
			}
		}
		
		LOGGER.info("Service KhoNongSanService Functon getKhoVatTu RESULT listKhoVatTu: " + listKhoVatTu);
		return listKhoVatTu;
	}

	public List<KhoNongSanDTO> filterVatTu(String userName, FilterSupplieDTO filterVatTu) {
		LOGGER.info("Begin Service KhoNongSanService Function getKhoVatTu");
		LOGGER.info("Service KhoNongSanService Functon getKhoVatTu PARAM userName: " + userName);
		
		List<KhoNongSanDTO> listKhoVatTu = new ArrayList<KhoNongSanDTO>();
		List<Map<String, Object>> listSuplies = supplieCustom.filterVatTu(filterVatTu, userName);
		
		if(listSuplies != null) {
			for(int i=0; i<listSuplies.size(); i++) {
				KhoNongSanDTO khoDTO = new KhoNongSanDTO();
				String productId =(String) listSuplies.get(i).get("product_id");
				khoDTO.setId(productId);
				khoDTO.setProductName((String) listSuplies.get(i).get("product_name"));
				String startDate = (listSuplies.get(i).get("created_date") != null) ? ((String) listSuplies.get(i).get("created_date").toString()) : null;
				khoDTO.setStartDay(startDate);
				String endDate = (listSuplies.get(i).get("date_finish") != null) ? ((String) listSuplies.get(i).get("date_finish").toString()) : null;
				khoDTO.setEndDate(endDate);
				String updateDate = (listSuplies.get(i).get("updated_date") != null) ? ((String) listSuplies.get(i).get("updated_date").toString()) : null;
				khoDTO.setUpdatedDate(updateDate);
				BigInteger inventory = (BigInteger) listSuplies.get(i).get("inventory");
				khoDTO.setInventory(String.valueOf(inventory));
				listKhoVatTu.add(khoDTO);
			}
		}
		
		LOGGER.info("Service KhoNongSanService Functon getKhoVatTu RESULT listKhoVatTu: " + listKhoVatTu);
		return listKhoVatTu;
	}
}

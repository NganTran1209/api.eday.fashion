package io.bottest.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.ProjectComponent;
import io.bottest.jpa.respository.ProjectComponentRepository;

@Service
public class ProjectComponentServise {
	
	@Autowired
	private ProjectComponentRepository projectComponentRepo;
	
	public ProjectComponent saveProjectComponent (ProjectComponent proComponent) {
		projectComponentRepo.save(proComponent);
		return proComponent;
	}
	
	public List<ProjectComponent> getProjectComponentByProjectIdStatus(Long projectId){
		return projectComponentRepo.getProjectComponentByProjectIdStatus(projectId);
	}
	
	public List<ProjectComponent> findAll(){
		return projectComponentRepo.findAll();
	}
	
	public List<ProjectComponent> getProjectComponentByProjectId(Long projectId){
		return projectComponentRepo.getProjectComponentByProjectId(projectId);
	}
	
	public void saveProComponent(ProjectComponent projectCompo) {
		projectComponentRepo.save(projectCompo);
	}
	
	public ProjectComponent getProjectComponentById(Long id) {
		return projectComponentRepo.findById(id).get();
	}
}

package io.bottest.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.ExecutingJob;
import io.bottest.jpa.entity.NodeInfo;
import io.bottest.jpa.entity.ResultInfo;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.ExecutingJobRepository;
import io.bottest.jpa.respository.NodeRepository;
import io.bottest.jpa.respository.ResultInfoRepository;
import io.bottest.utils.FunctionUtils;
 
@Service
@Transactional
public class NodeService {

	private final Logger LOGGER = LogManager.getLogger(NodeService.class);
	
	@Autowired
	private NodeRepository nodeRepository;
	
	//@Value("${jenkins.url}")
	private String jenkinsUrl;
	
	//@Value("${spring.project.username}")
//	private String gitUser;
	
	//@Value("${spring.project.password}")
//	private String gitPass;
	
	//@Value("${spring.project.url}")
//	private String gitUrl;
	
	@Autowired
	private JenkinService jenkinService;
	
	@Autowired
	private ExecutingJobRepository exeJobRepo;
	
	@Autowired
    private SimpMessagingTemplate webSocket;
	
	@Autowired
	private ResultInfoRepository resultInfoRepo;
	
	@Autowired 
	BottestUserRepository bottestUserRepository;
	
	/**
	 * @param username
	 * @return
	 * @throws NoSuchElementException
	 */
	public NodeInfo getNodeinfo(String username, String os, String mac) throws NoSuchElementException {
		final String LOG_STRING = "GET getNodeinfo";
		LOGGER.info("Start " + LOG_STRING);
		LOGGER.info("END " + LOG_STRING);
		return nodeRepository.getNodeInfo(username, os, mac);
		
	}

	
	/**
	 * @param username
	 * @return
	 * @throws NoSuchElementException
	 */
	public int insertNodeinfo(String username, String secretKey, String machineName, String os, String mac) {
		final String LOG_STRING = "GET insertNodeinfo";
		LOGGER.info("Start " + LOG_STRING);
		LOGGER.info("END " + LOG_STRING);
		return nodeRepository.insertNodeinfo(username, secretKey, machineName, os, mac);
	}

	public int saveNodeInfo(String username, String jenkinsUser, String jenkinsPass, String os, String mac) {
		final String LOG_STRING = "GET saveNodeInfo";
		LOGGER.info("Start " + LOG_STRING);
		
		String secretKey = getSecretKey(username, jenkinsUser, jenkinsPass, mac);
		int effect = insertNodeinfo(username, secretKey, username, os, mac);
		LOGGER.info("END " + LOG_STRING);
		return effect;
	}
	
	public String getSecretKey(String username, String jenkinsUser, String jenkinsPass, String mac) {
		final String LOG_STRING = "GET getSecretKey";
		LOGGER.info("Start " + LOG_STRING);
		try {
  	      URL url = new URL(jenkinsUrl + "/computer/" + username +"_"+mac); // Jenkins URL localhost:8080, job named 'test'
  	      String authStr = jenkinsUser + ":" + jenkinsPass;
  	      String encoding = Base64.getEncoder().encodeToString(authStr.getBytes("utf-8"));

  	      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
  	      connection.setRequestMethod("GET");
  	      connection.setDoOutput(true);
  	      connection.setRequestProperty("Authorization", "Basic " + encoding);

  	      InputStream content = connection.getInputStream();
  	      BufferedReader in = new BufferedReader(new InputStreamReader(content));
  	      String line;
  	      StringBuilder str = new StringBuilder();
  	      while ((line = in.readLine()) != null) {
  	          int number = line.indexOf("-secret");
  	          if ( number > 0) {
  	        	  str.append(line.substring(number+8, number+72));
  	        	  break;
  	          }
  	      }
  	      

		  if (str.length() == 0) {
		      System.out.println("Secret key do not exist !!");
		      return "";
		  }
			
		  return str.toString();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
		LOGGER.info("END " + LOG_STRING);
		return null;
	}

	public void createNodeInJenkins(String username, String jenkinsUser, String jenkinsPass, String mac) {
		final String LOG_STRING = "GET createNodeInJenkins";
		LOGGER.info("Start " + LOG_STRING);
		try {
	      URL url = new URL(jenkinsUrl + "/scriptText"); // 
	      String authStr = jenkinsUser + ":" + jenkinsPass;
	      System.out.println(authStr);
	      String encoding = Base64.getEncoder().encodeToString(authStr.getBytes("utf-8"));

	      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	      connection.setRequestMethod("POST");
	      connection.setDoOutput(true);
	      connection.setRequestProperty("Authorization", "Basic " + encoding);

          String params = "import hudson.model.Node.Mode \n"
        		  + "import hudson.slaves.* \n"
        		  + "import jenkins.model.Jenkins \n"
        		  + "entry = new EnvironmentVariablesNodeProperty(new EnvironmentVariablesNodeProperty.Entry(\"BOTTEST\", \"C:\\\\PTS\\\\root\")) \n"
        		  + "list = new LinkedList() \n"
        		  + "list.add(entry) \n"
        		  + "DumbSlave dumb = new DumbSlave(\""+ username + "_" + mac + "\", \n"
        		  + "\" Agent\", \n"
        		  + "\"C:\\\\PTS\\\\root\", \n"
        		  + "\"10\", \n"
        		  + "Mode.NORMAL, \n"
        		  + "\"" + username + "_" + mac +"\", \n"
        		  + "new JNLPLauncher(), \n"
        		  + "RetentionStrategy.INSTANCE, list) \n"
        		  + "Jenkins.instance.addNode(dumb) \n";
	      
          System.out.println(params);
	      String urlParams = "script=" + params;
	      byte[] postData = urlParams.getBytes("utf-8");
	      try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
	        wr.write(postData);
	      }

	      InputStream content = connection.getInputStream();
	      BufferedReader in = new BufferedReader(new InputStreamReader(content));
	      String line;
	      while ((line = in.readLine()) != null) {
	        System.out.println(line);
	      }
	      LOGGER.info("END " + LOG_STRING);
	    } catch (Exception e) {
	      e.printStackTrace();
	      LOGGER.info("Error " + LOG_STRING);
	    }
	}

	public List<NodeInfo> getNodeinfoList(String nodeLabel, String runningOS, boolean status) {
		final String LOG_STRING = "GET getNodeinfoList";
		LOGGER.info("Start " + LOG_STRING);
		LOGGER.info("END " + LOG_STRING);
		return nodeRepository.getNodeInfoList(nodeLabel, runningOS.substring(0,2).toLowerCase(), status);
	}
	
//	public void runOnLocal(String projectId, String testsuiteId, Principal principal, ExecutingJob job, String gitUsername, String gitPassword) {}
		public void runOnLocal(String projectId, String testsuiteId, Principal principal, ExecutingJob job) {
		LOGGER.info("START NodeService runOnLocal");
		
	    Date now = new Date();
	    job.setType(jenkinService.RUNNING);
	    job.setStartDate(now);
	    job.setRunningDate(FunctionUtils.convertISO2PythonDate(now));
	    job.setCreatedDate(now);
	    job.setUpdatedDate(now);
	    job.setResultJson("");
	    job.setExceptionMessage("");
	    job.setStatus(jenkinService.WAITING);
	    job.setNewStatus(jenkinService.WAITING);
	    job.setJobName("Run on Local");
	    job.setNodeLabel("RunOnLocal");

	    exeJobRepo.saveAndFlush(job);
	    jenkinService.sendNotification(job);
	    
		if(webSocket == null ) {
			System.out.println("webSocket is null");
		}else {
			Map<String, Object> map = new HashMap<>();
			map.put("projectId", projectId);
			map.put("testsuiteId", testsuiteId);
			map.put("job", job);
//			map.put("gitUser", gitUsername);
//			map.put("gitPass", gitPassword);
//			map.put("gitUrl", gitUrl);
			try {
				Map<String, Object> header = new HashMap<>();
				header.put("ACTION", "TEST");
				webSocket.convertAndSendToUser(principal.getName(), "/bot_notify/runTest", map, header);
			} catch (MessagingException mess) {
				job.setStatus(jenkinService.FAILURE);
			    job.setNewStatus(jenkinService.FAILURE);

			    exeJobRepo.saveAndFlush(job);
			    jenkinService.sendNotification(job);
			}
		}
		
		LOGGER.info("END NodeService runOnLocal");
	}
	
	public void updateStatusChange(ExecutingJob job) {
		final String LOG_STRING = "GET updateStatusChange";
		LOGGER.info("Start " + LOG_STRING);
		
		 exeJobRepo.saveAndFlush(job);
		 jenkinService.sendNotification(job);
		 LOGGER.info("END " + LOG_STRING);
	}
	
	public void insertResultinfo(List<ResultInfo> results) {
		final String LOG_STRING = "GET insertResultinfo";
		LOGGER.info("Start " + LOG_STRING);
		for (ResultInfo rs : results) {
			
			if (rs.getTestCaseResult() != null) {
				String testCaseResult = null;
				
				if ("FAILED".equals(rs.getTestCaseResult())) {
					ResultInfo resultInfo = resultInfoRepo.findByProjectIdAndTestSuiteAndTestCaseName(rs.getProjectId(), rs.getTestSuite(), rs.getTestCaseName());
					if (resultInfo != null) {
						String tscResult =  resultInfo.getTestCaseResult();
						if(tscResult.equals("PASS")) {
							testCaseResult = "DEGRADE";
						} else {
							testCaseResult = rs.getTestCaseResult();
						}
					}
				} else {
					testCaseResult = rs.getTestCaseResult();
				}
				
				resultInfoRepo.insertResultInfo(rs.getResultName(), rs.getTestSuite(), rs.getTestResult(), rs.getRunningUser(), rs.getExecuteDate(),
						rs.getTestCaseName(), testCaseResult, rs.getProjectId(), rs.getType(), rs.getRunningDescription(), rs.getStatus(), rs.getBrowser(),
						rs.getDeviceName(), rs.getExpectedOutput(), rs.getLocation(), rs.getMilestone(), rs.getMobileMode(), rs.getOs(), rs.getScreenResolution());
			} else {
				resultInfoRepo.insertResultInfo2(rs.getResultName(), rs.getTestSuite(), rs.getTestResult(), rs.getRunningUser(), rs.getExecuteDate(),
						 rs.getProjectId(), rs.getType(), rs.getMilestone(), rs.getStatus());
			}
		}
		 LOGGER.info("END " + LOG_STRING);
		
	}


	public void changeStatus(Object payload, String username) {
		
//		Map<String, Object> map = convertToObject(payload);
		final String LOG_STRING = "GET changeStatus";
		LOGGER.info("Start " + LOG_STRING);
		String jsonString = new String((byte[])payload);
		Gson gson = new Gson();
		Map map = gson.fromJson(jsonString, Map.class);
		LocalDateTime localDateTime = LocalDateTime.now();
		String dateTime = localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
		NodeInfo node = new NodeInfo();
		node.setAlive((boolean) map.get("status"));
		node.setSessionId((String) map.get("sessionId"));
		node.setOs((String) map.get("os"));
		node.setUsername(username);
		node.setRequestDate(dateTime);
		nodeRepository.insertNewNodeinfo(node.getUsername(), node.getSessionId(), node.isAlive(), node.getOs(), node.getRequestDate());
		 LOGGER.info("END " + LOG_STRING);
	}


	public void disconnectBySessionId(String sessionId) {
		final String LOG_STRING = "GET disconnectBySessionId";
		LOGGER.info("Start " + LOG_STRING);
		nodeRepository.disconnectBySessionId(sessionId);
		LOGGER.info("END " + LOG_STRING);
	}


	public void checkOnLocal(String name) {
		final String LOG_STRING = "GET checkOnLocal";
		LOGGER.info("Start " + LOG_STRING);
		LocalDateTime localDateTime = LocalDateTime.now();
		String dateTime = localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
		Map<String, String> map = new HashMap<>();
		map.put("datetime", dateTime);
		Map<String, Object> header = new HashMap<>();
		header.put("ACTION", "CHECK");
		webSocket.convertAndSendToUser(name, "/bot_notify/checkNode", map, header);
		try {
		    TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException ie) {
		    Thread.currentThread().interrupt();
		}
		nodeRepository.updateNodeDisconnectWithUser(name, dateTime);
		LOGGER.info("END " + LOG_STRING);
	}


	public void updateNodeConnected(String sessionId, String requestDate) {
		final String LOG_STRING = "GET updateNodeConnected";
		LOGGER.info("Start " + LOG_STRING);
		nodeRepository.updateNodeConnected(sessionId, requestDate);
		LOGGER.info("END " + LOG_STRING);
	}
}

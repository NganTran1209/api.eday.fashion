package io.bottest.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SendMailService2 {

//	@Value("${mail.user}")
//	private String from;
//
//	@Value("${mail.pass}")
//	private String pass;
//
//	@Value("${mail.host}")
//	private String host;
//
//	@Value("${mail.port}")
//	private String port;
//
//	@Value("${mail.domain}")
//	private String domain;
//
//	@Value("${security.jwt.token.secret-key:Bottest@Paracel}")
//	private String secretKey = "Bottest@Paracel";
//	
//	@Value("${bottest.url}")
//	private String url;
//	
//	private Session session;
//
//	/**
//	 * 
//	 * @param sendMail
//	 */
//	public void sendInviteProjectEmail(SendMailDTO sendMail) {
//		String toEmail = sendMail.getTo();
//		String content = sendMail.getContent();
//		String projectRole = sendMail.getProjectRole();
//		String subject = sendMail.getTitle();
//		String projectId = sendMail.getProjectId();
//		try {
//			Transport transport = connectSSLMail();
//			transport.connect(host, from, pass);
//
//			String[] to = toEmail.split(",");
//			InternetAddress[] toAddress = new InternetAddress[to.length];
//
//			// To get the array of addresses
//			for (int i = 0; i < to.length; i++) { // changed from a while loop
//				toAddress[i] = new InternetAddress(to[i]);
//			}
//			File file = new File(getClass().getClassLoader().getResource("mail.html").getFile());
//			BufferedReader in = new BufferedReader(new FileReader(file));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
//
//			String contentMail = contentBuilder.toString();
//			String contentEmail = contentMail.replace("paracel", content);
//
//
//			for (int j = 0; j < toAddress.length; j++) { // changed from a while loop
//				Message message = new MimeMessage(session);
//				message.setFrom(new InternetAddress(from));
//				message.setRecipient(Message.RecipientType.TO, toAddress[j]);
//				message.setSubject(subject);
//				String pattern = "dd/MM/yyyy HH:mm:ss";
//				Date date = new Date();
//				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//				String dateCreate = simpleDateFormat.format(date);
//				String token = "projectId=" + projectId + "&email=" + toAddress[j] + "&projectRole=" + projectRole
//						+ "&dateCreate=" + dateCreate;
//				token = TokenUtil.encrypt(token, secretKey);
//				String userNotExist = contentEmail.replaceAll("http://localhost:8080/bottest/signup",
//						this.url + "/invitedProject?token=" + token);
//				message.setContent(userNotExist, "text/html;charset=utf-8");
//
//				transport.sendMessage(message, message.getAllRecipients());
//			}
//			transport.close();
//		} catch (Exception e) {
//			e.getMessage();
//		}
//	}
//
//	/**
//	 * @param host
//	 * @param from
//	 * @param pass
//	 * @param port
//	 * @param domain
//	 * @param toEmail
//	 */
//	public void sendActiveEmail(String toEmail) {
//
//		try {
//			Transport transport = connectSSLMail();
//			
//			String[] to = { toEmail };
//
//			InternetAddress[] toAddress = new InternetAddress[to.length];
//
//			for (int i = 0; i < to.length; i++) { // changed from a while loop
//				toAddress[i] = new InternetAddress(to[i]);
//			}
//
//			File file = new File(getClass().getClassLoader().getResource("mailActive.html").getFile());
//			BufferedReader in = new BufferedReader(new FileReader(file));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
//
//			transport.connect(host, from, pass);
//
//			Message message = new MimeMessage(session);
//			message.setFrom(new InternetAddress(from));
//			message.setRecipient(Message.RecipientType.TO, toAddress[0]);
//			message.setSubject("Complete Registration!");
//			String pattern = "dd/MM/yyyy HH:mm:ss";
//			Date date = new Date();
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//			String dateCreate = simpleDateFormat.format(date);
//			String token = "&email=" + toAddress[0] + "&dateCreate=" + dateCreate;
//			String contentMail = contentBuilder.toString();
//
//			token = TokenUtil.encrypt(token, secretKey);
//			String contentEmail = contentMail.replace("paracel", this.url +
//					"/activeAccount?token=" + token);
//			message.setContent(contentEmail, "text/html;charset=utf-8");
//			transport.sendMessage(message, message.getAllRecipients());
//			transport.close();
//		} catch (Exception e) {
//			e.getMessage();
//		}
//	}
//
//	/**
//	 * @param host
//	 * @param from
//	 * @param pass
//	 * @param port
//	 * @param domain
//	 * @param toEmail
//	 */
//	public void sendMailForgotPassword(String toEmail) {
//
//		try {
//			String[] to = { toEmail };
//			InternetAddress[] toAddress = new InternetAddress[to.length];
//
//			for (int i = 0; i < to.length; i++) { // changed from a while loop
//				toAddress[i] = new InternetAddress(to[i]);
//			}
//
//			File file = new File(getClass().getClassLoader().getResource("forgotPass.html").getFile());
//			BufferedReader in = new BufferedReader(new FileReader(file));
//			String str;
//			StringBuilder contentBuilder = new StringBuilder();
//			while ((str = in.readLine()) != null) {
//				contentBuilder.append(str);
//			}
//			in.close();
//
//			Transport transport = this.connectSSLMail();
//
//			transport.connect(host, from, pass);
//
//			Message message = new MimeMessage(session);
//			message.setFrom(new InternetAddress(from));
//			message.setRecipient(Message.RecipientType.TO, toAddress[0]);
//			message.setSubject("Forgot your account!");
//			String pattern = "dd/MM/yyyy HH:mm:ss";
//			Date date = new Date();
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//			String dateCreate = simpleDateFormat.format(date);
//			String token = "&email=" + toAddress[0] + "&dateCreate=" + dateCreate + "&code="
//					+ UUID.randomUUID().toString();
//			String contentMail = contentBuilder.toString();
//			token = TokenUtil.encrypt(token, secretKey);
//			System.out.println("Encrypted token:[" + token + "]");
//			String contentEmail = contentMail.replace("paracel", this.url +
//					"/forgotPass?forgotinfo=" + token);
//			message.setContent(contentEmail, "text/html;charset=utf-8");
//			transport.sendMessage(message, message.getAllRecipients());
//			transport.close();
//		} catch (Exception e) {
//			e.getMessage();
//		}
//	}
//	
//	private Transport connectTlsMail() throws NoSuchProviderException {
//		
//		Properties props = System.getProperties();
//		//props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.host", host);
//		props.put("mail.smtp.user", from );
//		props.put("mail.smtp.password", pass);
//		props.put("mail.smtp.port", port);
//		props.put("mail.smtp.domain", domain);
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.starttls.enable", "true"); //TLS
//
//
//		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(from, pass);
//            }
//        });
//		Transport transport = session.getTransport("smtp");
//		return transport;
//		
//	}
//	
//	private Transport connectSSLMail() throws NoSuchProviderException {
//		
//		Properties props = System.getProperties();
//		//props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.host", host);
//		props.put("mail.smtp.user", from );
//		props.put("mail.smtp.password", pass);
//		props.put("mail.smtp.port", port);
//		props.put("mail.smtp.domain", domain);
//		props.put("mail.smtp.auth", "true");
////		props.put("mail.smtp.starttls.enable", "true"); //TLS
//		props.put("mail.smtp.socketFactory.port", "465");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//
//		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(from, pass);
//            }
//        });
//		Transport transport = session.getTransport("smtp");
//		return transport;
//		
//	}

}

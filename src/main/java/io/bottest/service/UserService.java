package io.bottest.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.ShipmentAddressDTO;
import io.bottest.dto.SignupDTO;
import io.bottest.dto.UserDTO;
import io.bottest.jpa.entity.ShipmentAddress;
import io.bottest.jpa.entity.ShipmentAddressPK;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.entity.UserActive;
import io.bottest.jpa.entity.UserActiveIdentity;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.InvitedToProject;
import io.bottest.jpa.entity.UserProject;
import io.bottest.jpa.respository.ShipmentAddressRepository;
import io.bottest.jpa.respository.UserActiveRepository;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.InvitedReposiry;
import io.bottest.jpa.respository.UserProjectRepository;
import io.bottest.jpa.respository.UserRepository;

@Service
@Transactional
public class UserService {

	private final Logger LOGGER = LogManager.getLogger(UserService.class);
	
	@Value("${security.jwt.token.secret-key:Bottest@Paracel}")
	private String secretKey = "Bottest@Paracel";

//	@Autowired
//	SendMailService sendMailService;

	
	@Autowired
	private InvitedReposiry invitedRepository ;
	
	@Autowired
	private UserProjectRepository userProjectRepository ;
	
	@Autowired
	private ShipmentAddressRepository shipmentAddressRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserActiveRepository userActiveRepository;

	/**
	 * @param username
	 * @return
	 * @throws NoSuchElementException
	 */
	public boolean checkEmailExist(String email) throws NoSuchElementException {
		LOGGER.info("Begin Service UserService Function checkEmailExist");
		LOGGER.info("Service UserService Function checkEmailExist PARAMS username: " + email);
		
		LOGGER.info("End Service UserService Function checkEmailExist");
		return (userRepository.getUserByEmail(email) == null) ? true : false;
	}

	/**
	 * @param dto
	 */
	public void saveUserRole(SignupDTO dto) {

		LOGGER.info("Begin Service UserService Function saveUserRole");
		LOGGER.info("Service UserService Function checkEmailExist PARAMS dto: " + new Gson().toJson(dto, new TypeToken<SignupDTO>() {}.getType()));
		User user = new User();

		user.setUsername(dto.getEmail());
		user.setEmail(dto.getEmail());
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(dto.getPassword()));
		
		user.setEnable(false);
		user.setRole("ROLE_USER");
		//user.setCompany(dto.getCompany());
		//user.setCountry(dto.getCountry());
		//user.setFirstname(dto.getFirstname());
		//user.setLastname(dto.getLastname());
		//user.setPosition(dto.getPosition());
		//user.setLanguage(dto.getLanguage());	
		//user.setGitUser(dto.getGitUsername());
		//user.setGitPass(dto.getGitPassword());
		userRepository.save(user);
		
		LOGGER.info("End Service UserService Function saveUserRole");
	}

	public void mailForgotPass(BottestUser bottestUser) {
		LOGGER.info("Begin Service UserService Function mailForgotPass");
		LOGGER.info("Service UserService Function mailForgotPass PARAMS bottestUser: " + new Gson().toJson(bottestUser, new TypeToken<BottestUser>() {}.getType()));
		boolean checkUser = checkEmailExist(bottestUser.getEmail());
//		if (checkUser) {
//			sendMailService.sendMailForgotPassword( bottestUser.getEmail());
//		}
		LOGGER.info("End Service UserService Function mailForgotPass");
	}
	
	public List<User> getAllUsers(){
		LOGGER.info("Begin Service UserService Function getAllUsers");
		
		LOGGER.info("End Service UserService Function getAllUsers");
		return userRepository.findByAllUsers();
	}
	
	public User getBottestUser(String username) {

		return userRepository.getBottestUserByUsername(username);
	}	
	
	public List<InvitedToProject> getInfInvited(String username){
		LOGGER.info("Begin Service UserService Function getInfInvited");
		LOGGER.info("Service UserService Function getInfInvited PARAMS username: " + username);
		
		LOGGER.info("End Service UserService Function getInfInvited");
		return invitedRepository.findByToUserName(username);
	}
	
	public UserProject findById(Long id){
		
		return userProjectRepository.findById(id).get();
	}

	public UserDTO getUserByUserName(String username) { 
		LOGGER.info("Begin Service UserService Function getUserByUserName");
		LOGGER.info("Service UserService Function getUserByUserName PARAM username: " + username);
		 
		
		UserDTO userDTO = new UserDTO();
		
		LOGGER.info("End Service UserService Function getUserByUserName");
		return userDTO;	
	}

	public List<ShipmentAddressDTO> getListAddressByUsername(String userId) {
		LOGGER.info("Begin Service UserService Function getListAddressByUsername");
		LOGGER.info("Service UserService Function getListAddressByUsername PARAM userId: " + userId);
		
		List<ShipmentAddressDTO> result = new ArrayList<ShipmentAddressDTO>();
		if(StringUtils.isEmpty(userId)) {
			return result;
		}
		List<ShipmentAddress> listAddressEntities = shipmentAddressRepository.findAllById_UserId(userId);
		if(listAddressEntities == null || listAddressEntities.size() < 1) {
			return result;
		}
		for(ShipmentAddress address : listAddressEntities) {
			ShipmentAddressDTO addressDTO = new ShipmentAddressDTO();
			addressDTO.setUsername(address.getId().getUserId());
			addressDTO.setSeq(String.valueOf(address.getId().getSeq()));
			addressDTO.setAddressType(address.getAddressType() != null ? address.getAddressType() : "");
			addressDTO.setAddressReceive(address.getAddressReceive() !=  null ? address.getAddressReceive() : "");
			addressDTO.setCity(address.getCity() != null ? address.getCity() : "");
			addressDTO.setDistricts(address.getDistricts() != null ? address.getDistricts() : "");
			addressDTO.setMail(address.getMail() != null ? address.getMail() : "");
			addressDTO.setIsDefault(String.valueOf(address.getIsDefault()));
			addressDTO.setPhoneNumber(address.getPhoneNumber() != null ? address.getPhoneNumber() : "");
			addressDTO.setWards(address.getWards() != null ? address.getWards() : "");
			addressDTO.setNameReceive(address.getNameReceive() != null ? address.getNameReceive() : "");
			addressDTO.setStreets(address.getStreets() != null ? address.getStreets() : "");
			addressDTO.setCreatedDate(address.getCreatedDate() != null ? String.valueOf(address.getCreatedDate()) : "");
			addressDTO.setUpdatedDate(address.getUpdatedDate() != null ? String.valueOf(address.getUpdatedDate()) : "");
			result.add(addressDTO);
		}
		
		LOGGER.info("End Service UserService Function getListAddressByUsername");
		return result;
	}

	public List<ShipmentAddressDTO> updateShipmentAddress(List<ShipmentAddressDTO> addressDTO) {
		LOGGER.info("Begin Service UserService Function updateShipmentAddress");
		LOGGER.info("Service UserService Function updateShipmentAddress PARAM addressDTO: " + addressDTO);
		
		List<ShipmentAddressDTO> result = new ArrayList<ShipmentAddressDTO>();
		for(ShipmentAddressDTO address : addressDTO) {
			if(StringUtils.isEmpty(address.getUsername())) {
				return result;
			}
			
			ShipmentAddress addressEntities = new ShipmentAddress();
			
			ShipmentAddressPK addreessPKEntities = new ShipmentAddressPK();
			addreessPKEntities.setUserId(address.getUsername());
			addreessPKEntities.setSeq(Integer.parseInt(address.getSeq()));
			
			addressEntities.setId(addreessPKEntities);
			addressEntities.setAddressType(address.getAddressType() != null ? address.getAddressType() : "");
			addressEntities.setAddressReceive(address.getAddressReceive() != null ? address.getAddressReceive() : "");
			addressEntities.setCity(address.getCity() != null ? address.getCity() : "");
			addressEntities.setDistricts(address.getDistricts() != null ? address.getDistricts() : "");
			addressEntities.setMail(address.getMail() != null ? address.getMail() : "");
			addressEntities.setIsDefault(address.getIsDefault() != null ? Integer.parseInt(address.getIsDefault()) : 0);
			addressEntities.setPhoneNumber(address.getPhoneNumber() != null ? address.getPhoneNumber() : "");
			addressEntities.setWards(address.getWards() != null ? address.getWards() : "");
			addressEntities.setNameReceive(address.getNameReceive() != null ? address.getNameReceive() : "");
			addressEntities.setStreets(address.getStreets() != null ? address.getStreets() : "");
			addressEntities.setUpdatedDate(new Date());
			addressEntities.setCreatedDate(new Date());
			
			if(addressEntities.getId().getSeq() == 0) {
				//nhan gia tri moi nhat cua record dua theo username
				Integer seq = shipmentAddressRepository.findFirstByUserIdOrderBySeqDesc(addressEntities.getId().getUserId());
				if(seq == null) {
					addressEntities.getId().setSeq(1);
					addressEntities.setIsDefault(1);
				} else {
					addressEntities.getId().setSeq(seq.intValue() + 1);
				}
			}
			if(addressEntities.getIsDefault() == 1) {
				//cap nhat dia chi giao hang thanh 0 (gia tri mac dinh)
				shipmentAddressRepository.updatedUndefautAllRecordByUserId(addressEntities.getId().getUserId());
			}
			addressEntities = shipmentAddressRepository.save(addressEntities);
			address.setIsDefault(String.valueOf(addressEntities.getIsDefault()));
			address.setUsername(addressEntities.getId().getUserId());
			address.setSeq(String.valueOf(addressEntities.getId().getSeq()));

		}
		
		LOGGER.info("End Service UserService Function updateShipmentAddress");
		result = getListAddressByUsername(addressDTO.get(0).getUsername());
		return result;
	}

	public List<ShipmentAddressDTO> deleteShipmentAddress(List<ShipmentAddressDTO> addressDTO) {
		LOGGER.info("Begin Service UserService Function deleteShipmentAddress");
		LOGGER.info("Service UserService Function deleteShipmentAddress PARAM addressDTO: " + addressDTO);
		
		List<ShipmentAddressDTO> result = new ArrayList<ShipmentAddressDTO>();
		for(ShipmentAddressDTO address : addressDTO) {
			
			if(StringUtils.isEmpty(address.getUsername())) {
				return result;
			}
			if(Integer.parseInt(address.getSeq()) == 0) {
				return result;
			}
			
			ShipmentAddressPK addressPKEntities = new ShipmentAddressPK();
			addressPKEntities.setUserId(address.getUsername());
			addressPKEntities.setSeq(Integer.parseInt(address.getSeq()));
			
			ShipmentAddress addressEntities = shipmentAddressRepository.getOne(addressPKEntities);
			if(addressEntities == null) {
				return result;
			}
			shipmentAddressRepository.delete(addressEntities);
			//kiem tra gia tri mac dinh isDefault sau do set default thanh top seq
			if(addressEntities.getIsDefault() == 1) {
				ShipmentAddress topAdd = shipmentAddressRepository.findFirstById_UserIdOrderById_SeqDesc(addressEntities.getId().getUserId());
				if(topAdd != null) {
					topAdd.setIsDefault(1);
					shipmentAddressRepository.save(topAdd);
				}
			}
		}
		
		result = getListAddressByUsername(addressDTO.get(0).getUsername());
		LOGGER.info("End Service UserService Function deleteShipmentAddress");
		return result;
	}

	public UserDTO updateProfile(UserDTO userDTO, String username) {
		LOGGER.info("Begin Service UserService Function updateProfile");
		LOGGER.info("Service UserService Function updateProfile PARAM userDTO: " + userDTO);
		
		UserDTO result = new UserDTO();
		if(StringUtils.isEmpty(username)) {
			return result;
		}
		
		
		
		result = getUserByUserName(username);
		LOGGER.info("End Service UserService Function updateProfile");
		return result;
	}

	public void saveUserActive(String username) {
		UserActiveIdentity userIden = new UserActiveIdentity();
		userIden.setUsername(username);
		userIden.setActiveDate(new Date());
		
		UserActive userActive = new UserActive();
		userActive.setUserActiveIdentity(userIden);
		userActive.setActiveTime(new Date());
		
		userActiveRepository.save(userActive);
		
	}

}


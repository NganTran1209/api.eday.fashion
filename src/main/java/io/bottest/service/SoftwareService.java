package io.bottest.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import io.bottest.jpa.entity.Software;
import io.bottest.jpa.respository.SoftwareRepository;

@Service
public class SoftwareService {
	//@Value("${uploadfileExtention.path}")
	private String uploadfileExtention;

	@Autowired
	private SoftwareRepository softwareRe;

	public String uploadSoftware(MultipartFile file, String typeSoftware, String versionSoftwares, Date upDate,
			String requirement, String releaseNote) {
		String fileName = null;
	  try {
			Path path = Paths.get(this.uploadfileExtention,typeSoftware.replace(" ", "_")+"_"+versionSoftwares+".zip");
			OutputStream out = null;
			Software uploadSoftware = new Software();
			Software ul = softwareRe.findByFileName(file.getOriginalFilename());
			if (ul != null) {
				fileName = "File was exist!";
				return fileName;
			}
			uploadSoftware.setFileName(typeSoftware.replace(" ", "_")+"_"+versionSoftwares+".zip");
			uploadSoftware.setUpDate(upDate);
			uploadSoftware.setReleaseNote(releaseNote);
			uploadSoftware.setRequirement(requirement);
			uploadSoftware.setType(typeSoftware);
			uploadSoftware.setVersion(versionSoftwares);
			softwareRe.save(uploadSoftware);
			try {
				out = new BufferedOutputStream(new FileOutputStream(path.toFile()));
				out.write(file.getBytes());
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {

		}
		return fileName;

	}

	public void editSoftware(String typeSoftware, String versionSoftwares, Date upDate,
			String requirement, String releaseNote, Long id) {
			Software uploadSoftware = softwareRe.findById(id).get();
			uploadSoftware.setUpDate(upDate);
			uploadSoftware.setReleaseNote(releaseNote);
			uploadSoftware.setRequirement(requirement);
			uploadSoftware.setType(typeSoftware);
			uploadSoftware.setVersion(versionSoftwares);
			uploadSoftware.setId(id);
			softwareRe.save(uploadSoftware);
	} 
	
	public List<Software> getAll() {
		return softwareRe.getAll();
	}

	public void deleteSoftware(Long id, String typeSoftware, String versionSoftwares) {
		Path path = Paths.get(this.uploadfileExtention,typeSoftware.replace(" ", "_")+"_"+versionSoftwares+".zip");
		File file = new File(path.toString());
		softwareRe.deleteById(id);
		file.delete();
		
	}
	
	public Resource loadSoftwareFileAsResource( String fileName) {
		try {
			Path path = Paths.get(this.uploadfileExtention,fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				return resource;
			} else {
			}
		} catch (MalformedURLException ex) {
		}
		return null;
	}
	
}
package io.bottest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import io.bottest.jpa.entity.InterfaceServer;
import io.bottest.jpa.respository.InterfaceServerRepository;

@Service
public class InterfaceServerService {
	
	@Autowired
	InterfaceServerRepository interfaceRepo;

	public List<InterfaceServer> getAll() {
		// TODO Auto-generated method stub
		return interfaceRepo.findAll();
	}

	public List<InterfaceServer> deleteAll() {
		// TODO Auto-generated method stub
		List<InterfaceServer> result = interfaceRepo.findAll();
		interfaceRepo.deleteAll();
		return result;
	}

	public void save(List<InterfaceServer> interfaces) {
		for (InterfaceServer item : interfaces) {
			interfaceRepo.save(item);
		}
	}

	public List<InterfaceServer> findByOsAndBrowserFree(String runningOS, String browser, String scmType) {
		return interfaceRepo.findByOsAndBrowserFree(runningOS, browser, scmType);
	}

	public List<InterfaceServer> findByOsAndBrowserRunning(String runningOS, String browser, String scmType) {
		return interfaceRepo.findByOsAndBrowserRunning(runningOS, browser, scmType);
	}

	public List<InterfaceServer> findByBrowser(String browser, String scmType) {
		// TODO Auto-generated method stub
		return interfaceRepo.findByBrowser(browser);
	}

	public void delete(InterfaceServer interfaces) {
		// TODO Auto-generated method stub
		interfaceRepo.delete(interfaces);
	}
	
	public InterfaceServer findByOsAndBrowserAndScmTypeAndNodeLabel(String runningOS, String browser, String scmType, String nodeLabel) {
		return interfaceRepo.findByOsAndBrowserAndScmTypeAndNodeLabel(runningOS, browser, scmType, nodeLabel);
	}

	public InterfaceServer findByOsAndScmTypeAndNodeLabel(String os, String projectType, String username) {
		return interfaceRepo.findByOsAndScmTypeAndNodeLabel(os, projectType, username);
		
	}

	public void createInterfaceServer(String domain, String jobName, String os, String runningOs, String token, 
			String username, String location, String scm_type, String node_label, boolean isLocal) {
		interfaceRepo.createInterfaceServer(domain, jobName, os, runningOs, token, username, location, scm_type, node_label, isLocal);
		
	}

	public List<InterfaceServer> findByNodeLabelAndScmAndWindow(String runningOS, String scmType, boolean isLocal,
			String nodeLabel) {
		return interfaceRepo.findByNodeLabelAndScmAndWindow(runningOS, scmType, isLocal, nodeLabel);
	}

}

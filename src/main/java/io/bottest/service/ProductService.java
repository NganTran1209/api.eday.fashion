package io.bottest.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.configure.security.jwt.JwtTokenProvider;
import io.bottest.dto.BrandDTO;
import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.KhoNongSanDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.BrandRepository;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProductRepositoryCustom;

@Service
@Transactional
public class ProductService {

	@PersistenceContext
	private EntityManager entityManager;
	
	private final Logger LOGGER = LogManager.getLogger(ProductService.class);
	
	@Autowired
	BrandRepository brandRepo;
	
	@Autowired
	BottestUserRepository userRepo;
	
	@Autowired
	 JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	ProductRepositoryCustom proCustom;

	public BrandDTO findByBrandName(UserDetails userDetails) throws ParseException {
		if (StringUtils.isEmpty(userDetails.getUsername())) {
			return null;
		}
		
		BottestUser theUser = userRepo.findByUsernameIgnoreCase(userDetails.getUsername());
		String name = theUser.getLastname().concat(theUser.getFirstname());
		Map<String, Object> brand = brandRepo.findByBrandName(name);
		 String token = jwtTokenProvider.createToken(userDetails.getUsername(), theUser.getRole());
		if (brand.isEmpty()) {
			//tao brand voi brand_nam la name user login
			 brandRepo.insertBrand(name,userDetails.getUsername(), userDetails.getUsername(),theUser.getAddress(),theUser.getPhone(),  name, userDetails.getPassword(), new Date(), token);
			 brand = brandRepo.findByBrandName(name);
			 //tao branch tuong ung cho brand
			brandRepo.insertBranch(brand.get("BRAND_ID").toString() ,name,theUser.getAddress(),theUser.getPhone(),userDetails.getUsername(), new Date());
			Map<String, Object> branch = brandRepo.findByBrachName(name);
			//tao quan he brand-branch
			brandRepo.createBrandMember(brand.get("BRAND_ID").toString(), branch.get("BRANCH_ID").toString(),name,new Date());
			brandRepo.createBranchFacility(branch.get("BRANCH_ID").toString(), branch.get("BRANCH_NAME").toString(),new Date(),new Date());
		}else {}
		BrandDTO brandDTO = convertEntityToBrandDTO(brand);
		return brandDTO;
	}

	private BrandDTO convertEntityToBrandDTO(Map<String, Object> brand) throws ParseException {
		BrandDTO brandDTO = new BrandDTO();
		brandDTO.setBrandId((BigInteger) brand.get("BRAND_ID"));
		brandDTO.setBrandName((String) brand.get("BRAND_NAME"));
		brandDTO.setTitle((String) brand.get("TITLE"));
		brandDTO.setSubTitle((String) brand.get("SUBTITLE"));
		brandDTO.setAddress((String) brand.get("ADDRESS"));
		brandDTO.setCity((String) brand.get("CITY"));
		brandDTO.setDistricts((String) brand.get("DISTRICTS"));
		brandDTO.setWards((String) brand.get("WARDS"));
		brandDTO.setStreets((String) brand.get("STREETS"));
		brandDTO.setDomain((String) brand.get("DOMAIN"));
		brandDTO.setTokenBrand((String) brand.get("TOKEN_BRAND"));
		brandDTO.setToken((String) brand.get("TOKEN"));
		if(StringUtils.isEmpty(brandDTO.getImageUrl())) {
			String imageUrl = brandDTO.getUploadId() != null?new StringBuilder("?uploadId=").
					append(brandDTO.getUploadId()).append("&seq=1").toString():"?uploadId=1&seq=1";
					brandDTO.setImageUrl(imageUrl);
		}else {
			brandDTO.setImageUrl((String) brand.get("LOGO_IMAGE"));
		}
		brandDTO.setUploadId((BigInteger) brand.get("UPLOAD_ID"));
		brandDTO.setCreatedDate((brand.get("CREATED_DATE") != null)? brand.get("CREATED_DATE").toString() : null);
		brandDTO.setUpdateDate((brand.get("UPDATED_DATE") != null)? brand.get("UPDATED_DATE").toString() : null);
		return brandDTO;
	}

	public List<Product> getAllProduct() {
		LOGGER.info("Begin Service ProductService Function getAllProduct");
		List<Product> listProduct = new ArrayList<Product>();
		
		// Call Db get all user project active
		listProduct = productRepo.getAllProduct();
		
		LOGGER.info("Service ProductService Function setFieldInEntity RESULTS listProduct:" + new Gson().toJson(listProduct, new TypeToken<List<Project>>() {}.getType()));
		LOGGER.info("End Service ProductService Function getAllProduct");
		return listProduct;
	}

	public Product getProductById(Long productId) {
		return productRepo.findById(productId).get();
	}

	public List<Map<String, Object>> filterProduct(FilterProductDTO filterProduct) {
		LOGGER.info("Begin Service ProductService Function filterProduct");
		
		return proCustom.filterProduct(filterProduct);
	}
	
}

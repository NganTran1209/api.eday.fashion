package io.bottest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.Component;
import io.bottest.jpa.respository.ComponentRespository;


@Service
public class ComponentService  {
	final public static String RUNNING_OS = "RUNNING_OS";
	final public static String OS_GROUP = "OS";
	final public static String LOCATION_GROUP = "LOCATION";
	final public static String BROWSER_GROUP = "BROWSER";
	final public static String RESOLUTION_GROUP = "RESOLUTION";
	final public static String DEVICE_GROUP = "DEVICE";
	final public static String BOTTESTTOOL_GROUP = "BOTTESTTOOL";
	@Autowired
	ComponentRespository componentRepo;
	
	public List<Component> findByGroupCode(String groupCode) {
		return componentRepo.findByGroupCode(groupCode);
	}

	public void save(List<Component> components) {
		for (Component component : components) {
			componentRepo.save(component);
		}	
	}

	public List<Component> getAll() {
		return componentRepo.findAll();
	}

	public List<Component> deleteAll() {
		List<Component> result =componentRepo.findAll();
		componentRepo.deleteAll();
		return result;
	}

	public void delete(Component component) {
		componentRepo.delete(component);
	}
	
	public void Update(Component component) {
		componentRepo.save(component);
	}
}

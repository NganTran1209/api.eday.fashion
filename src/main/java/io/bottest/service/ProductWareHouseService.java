package io.bottest.service;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.ProductWareHouse;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.ProductWareHouseResponsitory;
import io.bottest.jpa.respository.UserRepository;

@Service
@Transactional
public class ProductWareHouseService {

	private final Logger LOGGER = LogManager.getLogger(ProductWareHouseService.class);

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	ProductWareHouseResponsitory proWareRespo;
	
	public void addProductToWareHouse(Product product, UserDetails userDetails) {
		ProductWareHouse productWareHouse = new ProductWareHouse();
		productWareHouse.setProId(product.getId());
		productWareHouse.setAmount(product.getAmount());
		productWareHouse.setCreated_date(new Date());
		productWareHouse.setTotalMoneyBuy(product.getAmount() * product.getPrice_buy());
		User owner = userRepo.findByUserNameAndPassword(userDetails.getUsername(), userDetails.getPassword());
		productWareHouse.setOwner_id(owner.getId());
		productWareHouse.setSalerableQuantity((long) 0);
		productWareHouse.setStockQuantity(product.getAmount());
		productWareHouse.setTotalMoneySell((long) 0);
		proWareRespo.save(productWareHouse);
	}
}

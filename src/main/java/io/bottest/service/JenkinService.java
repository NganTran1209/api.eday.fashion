package io.bottest.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cdancy.jenkins.rest.JenkinsClient;
import com.cdancy.jenkins.rest.domain.common.IntegerResponse;
import com.cdancy.jenkins.rest.domain.job.BuildInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.ResponseRestAPI;
import io.bottest.jpa.entity.ExecutingJob;
import io.bottest.jpa.entity.InterfaceServer;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.respository.ExecutingJobRepository;
import io.bottest.jpa.respository.ProjectRepository;
import io.bottest.utils.FunctionUtils;
import io.bottest.utils.HttpGetWithEntity;

@Service
public class JenkinService implements Runnable { 
    public static final String SUCCESS = "SUCCESS";
    public static final String UNSTABLE = "UNSTABLE";
    public static final String FAILURE = "FAILURE";
    public static final String NOT_BUILT ="NOT_BUILT";
    public static final String ABORTED = "ABORTED";
    public static final String RUNNING = "RUNNING";
    public static final String WAITING = "WAITING";
    public static final String PREPARING = "PREPARING";
    
    public static final String JOB_SCHEDULE="SCHEDULE";
    public static final String JOB_RUNNING="RUNNING";
    
    private final Logger LOGGER = LogManager.getLogger(JenkinService.class);
	    
	private String[] status = {SUCCESS,UNSTABLE,FAILURE,NOT_BUILT,ABORTED};
    public static final List<String>  LIST_RUNNING = Arrays.asList(WAITING, RUNNING);

    //@Value("${jenkins.username}")
	private String jenkinsUser;
    
    //@Value("${jenkins.password}")
	private String jenkinsPass;
    
    //@Value("${jenkins.token}")
	private String jenkinsToken;
    
    //@Value("${jenkins.url}")
	private String jenkinsUrl;
    
    //@Value("${spring.project.url}")
	private String scmUrl;
    
    //@Value("${spring.project.username}")
	private String scmUsername;
    
   // @Value("${spring.project.password}")
	private String scmPassword;

    //@Value("${jenkin.job.window10:999_BotTest_Win10_v2}")
    private String jenkinTestJob = "999_BotTest_Win10_v2_Java";
	
	@Autowired
	private InterfaceServerService intefaceService;
	
	@Autowired
	private ExecutingJobRepository exeJobRepo; 
	
    @Autowired
    private TaskExecutor taskExecutor;
    
    
    private JenkinsClient client;
    
    @Autowired
    private FileService fileService;
    
    @Autowired
    private NodeService nodeServices;
    
    @Autowired
    private SimpMessagingTemplate webSocket;
    
    @Autowired
    private ProjectRepository projectRepo;
		
//	
//	private InterfaceServer getJenkinJob(ExecuteTestDTO dto) {
//		List<InterfaceServer> servers = intefaceService.findByOsAndBrowser(dto.getRunningOS(),dto.getBrowser());
//		if ((servers != null) &&  (!servers.isEmpty())) {
//			return servers.get(0);
//		}
//		servers = intefaceService.findByOs(dto.getRunningOS());
//		if ((servers != null) &&  (!servers.isEmpty())) {
//			return servers.get(0);
//		}
//		
//		servers = intefaceService.findByBrowser(dto.getBrowser());
//		if ((servers != null) &&  (!servers.isEmpty())) {
//			return servers.get(0);
//		}
//		
//		return null;
//		
//	}
	
	private InterfaceServer getJenkinJobs(ExecutingJob job, String scmType) {
		
		LOGGER.info("Begin Service JenkinService Function getJenkinJobs");
		
		List<InterfaceServer> servers = intefaceService.findByOsAndBrowserFree(job.getRunningOS(),job.getBrowser(), scmType);
		if ((servers != null) &&  (!servers.isEmpty())) {
			
			LOGGER.info("End Service JenkinService Function getJenkinJobs: Finded servers");
			return servers.get(0);
		}
		servers = intefaceService.findByOsAndBrowserRunning(job.getRunningOS(), job.getBrowser(), scmType);
		if ((servers != null) &&  (!servers.isEmpty())) {
			
			LOGGER.info("End Service JenkinService Function getJenkinJobs: Finded servers");
			return servers.get(0);
		}
		
		LOGGER.info("End Service JenkinService Function getJenkinJobs: Server not found");
		return null;
		
	}
	
    private InterfaceServer getJenkinJobs(ExecutingJob job, String scmType, boolean isLocal, String nodeLabel) {
		
		LOGGER.info("Begin Service JenkinService Function getJenkinJobs");
		
		List<InterfaceServer> servers = intefaceService.findByNodeLabelAndScmAndWindow(job.getRunningOS(), scmType, isLocal, nodeLabel);
		if ((servers != null) &&  (!servers.isEmpty())) {
			for (InterfaceServer server : servers) {
				boolean check = checkNodeConnected(server.getNodeLabel());
				LOGGER.info("Begin Service JenkinService Function getJenkinJobs checkServer: " + check);
				LOGGER.info("Begin Service JenkinService Function getJenkinJobs server: " + server);
				if (!check) {
					return server;
				}
			}
			LOGGER.info("End Service JenkinService Function getJenkinJobs: Finded servers");
		}
		
		LOGGER.info("End Service JenkinService Function getJenkinJobs: Server not found");
		return null;
		
	}


	@Transactional
	public APIResult executeJob(ExecutingJob dto, String scmType)  {
		
		LOGGER.info("Begin Service JenkinService Function executeJob");
		
		InterfaceServer  jenkin = this.getJenkinJobs(dto, scmType);
		
		APIResult result = executeJobImplement(dto, jenkin);
		
	    return result;
	}
	
	@Transactional
	public APIResult executeJob(ExecutingJob dto, String scmType, String nodeLabel)  {
		
		LOGGER.info("Begin Service JenkinService Function executeJob");
		
	    InterfaceServer  jenkin = this.getJenkinJobs(dto, scmType, true, nodeLabel);
	    
	    APIResult result = executeJobImplement(dto, jenkin);
	    
	    return result;
	}
	
	public APIResult executeJobImplement(ExecutingJob dto, InterfaceServer jenkin)  {
		
		LOGGER.info("Begin Service JenkinService Function executeJobImplement");
		
		APIResult result = new APIResult();
		
		try {
			  if (jenkin == null) {
				  result.setErrormessage( "Can not found server for OS:[" + dto.getRunningOS() + "], browser:[" + dto.getBrowser() + "]"); 
			  }
			  if (client == null) {
				  client = JenkinsClient.builder()
						  .endPoint(jenkin.getDomain()) // Optional. Defaults to http://127.0.0.1:8080
						  .credentials(jenkin.getUsername() + ":" + jenkin.getToken()) // Optional.
						  .build();
			  }
			  Project project = projectRepo.findById(Long.valueOf(dto.getProjectId())).get();
			  fileService.syncProject( dto.getUsername(), dto.getProjectId());
			  Date now = new Date();
			  dto.setType(JOB_RUNNING);
			  dto.setStartDate(now);
			  dto.setRunningDate(FunctionUtils.convertISO2PythonDate(now));
			  dto.setJobName(jenkin.getJobName());
			  dto.setCreatedDate(now);
			  dto.setUpdatedDate(now);
			  //Gson gson = new Gson();
			  //String jsonData = gson.toJson(dto, ExecuteTestDTO.class);
			  dto.setResultJson("");
			  dto.setStatus(PREPARING);
			  dto.setNewStatus(PREPARING);
			  dto.setNodeLabel(jenkin.getNodeLabel());

			  exeJobRepo.saveAndFlush(dto);
			  sendNotification(dto);
			  
			  String error = runJobImmediately(dto, jenkin, project);
			  result.setErrormessage(error);
			 
			  Map<String,Object> data = new HashMap<String, Object>();
			  result.setData(data);
			  data.put("executingDto", dto);
			  
			  LOGGER.info("Service JenkinService Function executeJobImplement data:"  + new Gson().toJson(result.getData(), new TypeToken<Object >() {}.getType()));

		} catch (Exception e) {
			result.setErrormessage(e.getMessage());
			LOGGER.error("Service JenkinService Function executeJobImplement ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}

		LOGGER.info("End Service JenkinService Function executeJobImplement");
		return result;
	}
	
	@Transactional
	public APIResult createJob(ExecutingJob job)  {
		
		LOGGER.info("Begin Service JenkinService Function createJob");
		
		APIResult result = new APIResult();
		
		try {
			  Project project = projectRepo.findById(Long.valueOf(job.getProjectId())).get();
			  InterfaceServer  jenkin = this.getJenkinJobs(job, project.getScmType());
			  if (jenkin == null) {
				  result.setErrormessage( "Can not found server for OS:[" + job.getRunningOS() + "], browser:[" + job.getBrowser() + "]"); 
			  }
			  Date date = new Date();
	         SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
	         String strDate = formatter.format(date);
			  String jobName = jenkin.getJobName()+ "_" + job.getTestsuite()+"_" + strDate;
			 // if (client == null) {
				  client = JenkinsClient.builder()
						  .endPoint(jenkin.getDomain()) // Optional. Defaults to http://127.0.0.1:8080
						  .credentials(jenkin.getUsername() + ":" + jenkin.getToken()) // Optional.
						  .build();
			  //}
			  fileService.syncProject( job.getUsername(), job.getProjectId());
			  // If Run as Schedule
			  job.setJobName(jobName);
			  job.setStartDate(new Date());
			  job.setCreatedDate(new Date());
			  job.setUpdatedDate(new Date());
			  //Gson gson = new Gson();
			  //ExecuteTestDTO eto = gson.fromJson(job.getJsonData(), ExecuteTestDTO.class);
			  //job.setTestsuite(eto.getTestsuiteName());
			  job.setResultJson("");
			  job.setStatus(PREPARING);
			  job.setNewStatus(PREPARING);
			  String error = createScheduleJob(jenkin, project, job);
			  result.setErrormessage(error);
			 
			 
			  Map<String,Object> data = new HashMap<String, Object>();
			  result.setData(data);
			  data.put("executingDto", job);
			  
			  LOGGER.info("Service JenkinService Function createJob data:"  + new Gson().toJson(result.getData(), new TypeToken<Object >() {}.getType()));

		} catch (Exception e) {
			result.setErrormessage(e.getMessage());
			LOGGER.error("Service JenkinService Function createJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}

		LOGGER.info("End Service JenkinService Function createJob");
		return result;
	}
	
	@Transactional
	public APIResult editJob(ExecutingJob job)  {
		
		LOGGER.info("Begin Service JenkinService Function editJob");
		
		APIResult result = new APIResult();
		
		try {
			Project project = projectRepo.findById(Long.valueOf(job.getProjectId())).get();
			ExecutingJob exJob = exeJobRepo.findById(Long.valueOf(job.getId())).get();
			InterfaceServer  jenkin = this.getJenkinJobs(job, project.getScmType());
			if (jenkin == null) {
				  result.setErrormessage( "Can not found server for OS:[" + job.getRunningOS() + "], browser:[" + job.getBrowser() + "]"); 
			  }
//			if (client == null) {
			 client = JenkinsClient.builder()
						  .endPoint(jenkin.getDomain()) // Optional. Defaults to http://127.0.0.1:8080
						  .credentials(jenkin.getUsername() + ":" + jenkin.getToken()) // Optional.
						  .build();
//			 }
		  client.api().jobsApi().delete(null, exJob.getJobName());
		  
		  fileService.syncProject( job.getUsername(), job.getProjectId());
			  // If Run as Schedule
		  String jobName = job.getJobName();
		  exJob.setJobName(jobName);
		  exJob.setBrowser(job.getBrowser());
		  exJob.setDeviceName(job.getDeviceName());
		  exJob.setResultJson("");
		  exJob.setLsTestcases(job.getLsTestcases());
		  exJob.setMobileMode(job.isMobileMode());
		  exJob.setMobileRes(job.getMobileRes());
		  exJob.setRecordVideo(job.getRecordVideo());
		  exJob.setRunningLocation(job.getRunningLocation());
		  exJob.setRunningName(job.getRunningName());
		  exJob.setRunningOS(job.getRunningOS());
		  exJob.setScheduleSpec(job.getScheduleSpec());
		  exJob.setScreenRes(job.getScreenRes());
		  exJob.setTimeout(job.getTimeout());
		  exJob.setUsername(job.getUsername());
		  exJob.setRunDescription(job.getRunDescription());
		   String error = createScheduleJob(jenkin, project, exJob);
		   result.setErrormessage(error);
			 
			 
//		  Map<String,Object> data = new HashMap<String, Object>();
//		  result.setData(data);
//		  data.put("executingDto", jobNew);

		  LOGGER.info("Service JenkinService Function editJob Error Message:"  + result.getErrormessage());
		   
		} catch (Exception e) {
			result.setErrormessage(e.getMessage());
			LOGGER.error("Service JenkinService Function editJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}

		LOGGER.info("End Service JenkinService Function editJob");
		return result;
	}
	
	private String runJobImmediately(ExecutingJob dto, InterfaceServer jenkin, Project project) throws ParseException {
		
		  LOGGER.info("Begin Service JenkinService Function runJobImmediately");
		
		  Map<String, List<String>> parameter = new HashMap<String,List<String>>();
		  parameter.put("runningDate",Arrays.asList(dto.getRunningDate()));
		  parameter.put("runningUser",Arrays.asList(dto.getUsername().replaceAll("@", ".")));
		  parameter.put("runningOS", Arrays.asList(jenkin.getRunningOS().replaceAll(" ", "~")));
		  parameter.put("OS", Arrays.asList(dto.getRunningOS().replaceAll(" ", "~")));
		  parameter.put("mobileMode", Arrays.asList(String.valueOf(dto.isMobileMode())));
		  if ("true".equalsIgnoreCase(dto.isMobileMode())) {
			  parameter.put("deviceName", Arrays.asList(dto.getDeviceName().replaceAll(" ", "~")));
			  parameter.put("mobileRes", Arrays.asList(dto.getMobileRes().replaceAll(" ", "~")));
		  }else {
			  parameter.put("deviceName", Arrays.asList(" "));
			  parameter.put("mobileRes", Arrays.asList(" "));
		  }

		  parameter.put("runningLocation", Arrays.asList(dto.getRunningLocation().replaceAll(" ", "~")));
		  parameter.put("browser", Arrays.asList(dto.getBrowser().replaceAll(" ", "~")));
		  parameter.put("projectName", Arrays.asList(project.getName()));
		  parameter.put("projectId", Arrays.asList(dto.getProjectId()));
		  parameter.put("testsuiteName", Arrays.asList(dto.getTestsuite().replaceAll(" ", "~")));
		  parameter.put("description", Arrays.asList(dto.getRunningName()));
		  
		  parameter.put("lsTestcases", Arrays.asList( dto.getLsTestcases().replaceAll(" ", "~")));
		  parameter.put("screenRes", Arrays.asList(dto.getScreenRes().replaceAll(" ", "~")));
		  parameter.put("timeout", Arrays.asList(String.valueOf(dto.getTimeout())));
		  parameter.put("isRecordVideo", Arrays.asList(String.valueOf(dto.getRecordVideo())));

		  parameter.put("scmUrl", Arrays.asList(project.getScmUrl()));
		  parameter.put("scmUsername", Arrays.asList(project.getScmUsername()));
		  parameter.put("scmPassword", Arrays.asList(project.getScmPassword()));
		  parameter.put("executingJobId", Arrays.asList(dto.getId().toString()));
		  parameter.put("milestone", Arrays.asList(dto.getRunningName()));
		  
		  IntegerResponse resp = client.api().jobsApi().buildWithParameters(null, jenkin.getJobName(),parameter );
		  if (resp.value() == null || resp.value() < 0 ) {
			  //result.setErrormessage(resp.errors().toString());
			  LOGGER.info("End Service JenkinService Function runJobImmediately ERROR: " + resp.errors().toString());
			  return resp.errors().toString();
		  }
		  dto.setQueueId(resp.value());
		  dto.setStatus(WAITING);
		  dto.setNewStatus(WAITING);
		  exeJobRepo.saveAndFlush(dto);
		  sendNotification(dto);
		  
		  LOGGER.info("End Service JenkinService Function runJobImmediately");
		  return "";
	}

	private String createScheduleJob(InterfaceServer jenkin, Project project, ExecutingJob job ) {
		
		LOGGER.info("Begin Service JenkinService Function createScheduleJob");
		
		String config = client.api().jobsApi().config(null, jenkin.getJobName());
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			StringReader sr = new StringReader(config);
			InputSource is = new InputSource(sr);
			Document document = db.parse(is);

			Element root = document.getDocumentElement();
			
			//Add parameter
			NodeList parameters = document.getElementsByTagName("parameterDefinitions");
			if (parameters != null && parameters.getLength() > 0) {
				Node parameter = parameters.item(0);
				while (parameter.hasChildNodes())
					parameter.removeChild(parameter.getFirstChild());

		    	parameter.appendChild(createDateParamNode(document,"runningDate",FunctionUtils.convertISO2PythonDate(job.getStartDate())));
			    
				parameter.appendChild(createParamNode(document,"runningUser",job.getUsername().replaceAll("@", ".")));
				parameter.appendChild(createParamNode(document,"runningOS", jenkin.getRunningOS().replaceAll(" ", "~")));
				parameter.appendChild(createParamNode(document,"OS", job.getRunningOS().replaceAll(" ", "~")));
				parameter.appendChild(createParamNode(document,"mobileMode", String.valueOf(job.isMobileMode())));		  
				  if (job.isMobileMode() == "true") {
					  parameter.appendChild(createParamNode(document,"mobileRes", job.getMobileRes().replaceAll(" ", "~")));
					  parameter.appendChild(createParamNode(document,"deviceName", job.getDeviceName().replaceAll(" ", "~")));			  
				  }else {
					  parameter.appendChild(createParamNode(document,"deviceName", " "));
					  parameter.appendChild(createParamNode(document,"mobileRes", " "));
				  }

				  parameter.appendChild(createParamNode(document,"runningLocation",job.getRunningLocation().replaceAll(" ", "~")));
				  parameter.appendChild(createParamNode(document,"browser", job.getBrowser().replaceAll(" ", "~")));
				  parameter.appendChild(createParamNode(document,"projectName", project.getName()));
				  parameter.appendChild(createParamNode(document,"projectId", job.getProjectId()));
				  parameter.appendChild(createParamNode(document,"testsuiteName", job.getTestsuite().replaceAll(" ", "~")));
				  parameter.appendChild(createParamNode(document,"description", job.getRunningName()));
				  
				  parameter.appendChild(createParamNode(document,"lsTestcases", job.getLsTestcases()));
				  parameter.appendChild(createParamNode(document,"screenRes", job.getScreenRes().replaceAll(" ", "~")));
				  parameter.appendChild(createParamNode(document,"timeout", String.valueOf(job.getTimeout())));
				  parameter.appendChild(createParamNode(document,"isRecordVideo", String.valueOf(job.getRecordVideo())));

				  parameter.appendChild(createParamNode(document,"scmUrl", project.getScmUrl()));
				  parameter.appendChild(createParamNode(document,"scmUsername", project.getScmUsername()));
				  parameter.appendChild(createParamNode(document,"scmPassword", project.getScmPassword()));
				  parameter.appendChild(createParamNode(document,"executingJobId", "-1"));
				  
//				  String[] jenkinsUrlArray = scmUrl.split("//");
//				  
//				  parameter.appendChild(createParamNode(document,"gitDomain", jenkinsUrlArray[0] + "//"));
//			      parameter.appendChild(createParamNode(document,"gitScmUser", jenkinsUser));
//				  parameter.appendChild(createParamNode(document,"gitScmPass", jenkinsPass));
//				  parameter.appendChild(createParamNode(document,"gitServer",jenkinsUrlArray[1] + "git/bottest/"));
				
			}
			// Add trigger
			NodeList triggers = root.getElementsByTagName("triggers");
			if (triggers != null && triggers.getLength() > 0) {
				Node trigger = triggers.item(0);
				while (trigger.hasChildNodes())
					trigger.removeChild(trigger.getFirstChild());
				
				Node timer = document.createElement("hudson.triggers.TimerTrigger");
				Element spec = document.createElement("spec");
				spec.setTextContent(job.getScheduleSpec());
				timer.appendChild(spec);
				trigger.appendChild(timer);
				
			}
			
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(document), new StreamResult(writer));
			String xmlConfig = writer.getBuffer().toString();
			xmlConfig = xmlConfig.replaceAll("%", "%25");
			//xmlConfig = URLEncoder.encode(xmlConfig, "UTF-8");
			//xmlConfig = URLDecoder.decode( xmlConfig, "UTF-8");
			client.api().jobsApi().create(null, job.getJobName(), xmlConfig);
			
			  exeJobRepo.saveAndFlush(job);
			
		} catch (ParserConfigurationException e) {
			LOGGER.error("Service JenkinService Function createScheduleJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return e.getMessage();
		} catch (SAXException e) {
			LOGGER.error("Service JenkinService Function createScheduleJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return e.getMessage();
		} catch (IOException e) {
			LOGGER.error("Service JenkinService Function createScheduleJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return e.getMessage();
		} catch (TransformerConfigurationException e) {
			LOGGER.error("Service JenkinService Function createScheduleJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return e.getMessage();
		} catch (TransformerException e) {
			LOGGER.error("Service JenkinService Function createScheduleJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return e.getMessage();
		}
		
		LOGGER.info("End Service JenkinService Function createScheduleJob");
		return "";
	}


	private Node createParamNode(Document document, String name, String value) {
		
		LOGGER.info("Begin Service JenkinService Function createParamNode");
		
		Node p = document.createElement("hudson.model.StringParameterDefinition");
		Node c1 = document.createElement("name");
		c1.setTextContent(name);
		Node c2 = document.createElement("description");
		Node c3 = document.createElement("defaultValue");
		c3.setTextContent(value);
		Node c4 = document.createElement("trim");
		c4.setTextContent("false");
		p.appendChild(c1);
		p.appendChild(c2);
		p.appendChild(c3);
		p.appendChild(c4);
		
		LOGGER.info("End Service JenkinService Function createParamNode");
		return p;
	}
	

	private Node createDateParamNode(Document document, String name, String value) {	
		
		LOGGER.info("Begin Service JenkinService Function createDateParamNode");
		
		Element p = document.createElement("me.leejay.jenkins.dateparameter.DateParameterDefinition");
      	p.setAttribute("plugin", "date-parameter@0.0.4");
		Node c1 = document.createElement("name");
		c1.setTextContent(name);
		Node c2 = document.createElement("description");
		Node c3 = document.createElement("stringLocalDateValue");
		Node c4 = document.createElement("stringLocalDate");
		c4.setTextContent("LocalDateTime.now()");
		//c3.setTextContent(value);
		Node c5 = document.createElement("stringDateFormat");
		c5.setTextContent("yyyyMMdd_HHmmss");
		p.appendChild(c1);
		p.appendChild(c2);
		p.appendChild(c3);
		c3.appendChild(c4);
		c3.appendChild(c5);
		
		LOGGER.info("End Service JenkinService Function createDateParamNode");
		return p;
	}


//	private String createSchedule(ExecuteTestDTO dto) {
//		StringBuffer result = new StringBuffer();
//		
//		result.append(FunctionUtils.joinInteger(dto.getMinutes()) + " ");
//		result.append(FunctionUtils.joinInteger(dto.getHours()) + " ");
//		result.append(FunctionUtils.joinInteger(dto.getDayOfWeeks()) + " ");
//		result.append(FunctionUtils.joinInteger(dto.getDayOfMonths()) + " ");
//		result.append(FunctionUtils.joinInteger(dto.getMonths()));
//
//		
//		return result.toString();
//	}


	@EventListener(ApplicationReadyEvent.class)
    public void executeAsynchronously() {
        taskExecutor.execute(this);
    }
	
	protected void sendNotification(ExecutingJob job) {
		Gson gson = new Gson();
//		System.out.println("sendNotification:" + job.toString());

		
		if(webSocket == null ) {
			System.out.println("webSocket is null");
		}else {
			webSocket.convertAndSendToUser(job.getUsername(), "/bot_notify/job", gson.toJson(job));			
		}
	}


	@Override
	public void run() {
		while (true) {
	    	try {
	        	List<ExecutingJob> jobs = exeJobRepo.findAllStatusChanged();
	        	//System.out.println("Running, jobs.isEmpty() :" + jobs.isEmpty());
		    	for (ExecutingJob job : jobs) {
		    		
				    if (SUCCESS.equalsIgnoreCase(job.getNewStatus())) {
				    	fileService.syncProject(job.getUsername() , job.getProjectId());
				    }
				    if (RUNNING.equalsIgnoreCase(job.getNewStatus())) {
				    	BuildInfo buildInfo = client.api().jobsApi().buildInfo(null, job.getJobName(), job.getBuildNumber());
				    	String status = buildInfo.result();
				    	LOGGER.info("statusExecutingJob: " + status);
				    	
				    	if(SUCCESS.equalsIgnoreCase(status)) {
				    		job.setNewStatus(SUCCESS);
				    	} else if((status == null) || (RUNNING.equalsIgnoreCase(status))){
				    		job.setNewStatus(RUNNING);
				    	}else{
				    		job.setNewStatus(FAILURE);
				    	}
				    }
				    job.setStatus(job.getNewStatus());
				    exeJobRepo.saveAndFlush(job);
					sendNotification(job);
				}
				Thread.sleep(3000);
				} catch (  Exception e) {
					// TODO Auto-generated catch block
//					LOGGER.error("Service JenkinService Function createScheduleJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
				}
		}
		
	}
	
	public int deleteScheduleJob(ExecutingJob job, String scmType) {
		
		LOGGER.info("Begin Service JenkinService Function deleteScheduleJob");
		
		//TODO:
		//detel job on database
		exeJobRepo.deleteById(job.getId());
		InterfaceServer  jenkin = this.getJenkinJobs(job, scmType);
		
//		if (client == null) {
		 client = JenkinsClient.builder()
					  .endPoint(jenkin.getDomain()) // Optional. Defaults to http://127.0.0.1:8080
					  .credentials(jenkin.getUsername() + ":" + jenkin.getToken()) // Optional.
					  .build();
//		 }
		client.api().jobsApi().delete(null, job.getJobName());
		
		LOGGER.info("End Service JenkinService Function deleteScheduleJob");
		return 0;
		
	}


	public boolean checkNodeConnected(String computerName) {
		HttpEntityEnclosingRequestBase request = new HttpGetWithEntity();
		try {
			request.setURI(new URI(jenkinsUrl + "/computer/" + computerName + "/api/json"));
    		String bsicStr = jenkinsUser + ":" + jenkinsPass;
    		request.addHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString(bsicStr.getBytes()));
		} catch (URISyntaxException e) {
			LOGGER.error("End JenkinsServices checkNodeConnected ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		ResponseRestAPI reponseAPI = new ResponseRestAPI();
    	try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(request)) {

    		String apiInfo = "";
    		// API response
    		if (response.getEntity() != null) {
    			apiInfo = EntityUtils.toString(response.getEntity(), "utf-8");
    		} 
    		
    		Gson gson = new Gson();
			Map<String, Object> map = gson.fromJson(apiInfo, Map.class);
			boolean offline = (boolean) map.get("offline");
			
			return offline;
    	
    	} catch (IOException e) {
    		LOGGER.error("End JenkinsServices checkNodeConnected ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
    	
    	return false;
		
	}
	
    public void createUserJob(String jobName, String assignLabel) {
		
		LOGGER.info("Begin Service JenkinService Function createUserJob");
		
		if (client == null) {
			  client = JenkinsClient.builder()
					  .endPoint(jenkinsUrl) // Optional. Defaults to http://127.0.0.1:8080
					  .credentials(jenkinsUser + ":" + jenkinsPass) // Optional.
					  .build();
		  }
		
		String config = client.api().jobsApi().config(null, "GIT_04_Bottest_Win10_v2_Java");
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			StringReader sr = new StringReader(config);
			InputSource is = new InputSource(sr);
			Document document = db.parse(is);

			Element root = document.getDocumentElement();
			
			//Add parameter
			NodeList parameters = document.getElementsByTagName("parameterDefinitions");
			if (parameters != null && parameters.getLength() > 0) {
				Node parameter = parameters.item(0);
				while (parameter.hasChildNodes())
					parameter.removeChild(parameter.getFirstChild());

		    	parameter.appendChild(createParamNode(document,"runningDate",FunctionUtils.convertISO2PythonDate(new Date())));
			    
				parameter.appendChild(createParamNode(document,"runningUser", "testing"));
				parameter.appendChild(createParamNode(document,"runningOS","windowOS"));
				parameter.appendChild(createParamNode(document,"OS", "Window~10"));
				parameter.appendChild(createParamNode(document,"mobileMode", "false"));	
				
				parameter.appendChild(createParamNode(document,"mobileRes", "375,812"));
				parameter.appendChild(createParamNode(document,"deviceName", "iPhone~X"));		

				parameter.appendChild(createParamNode(document,"runningLocation","Tokyo,~Japan"));
				parameter.appendChild(createParamNode(document,"browser", " "));
				parameter.appendChild(createParamNode(document,"projectName", " "));
				parameter.appendChild(createParamNode(document,"projectId", " "));
				parameter.appendChild(createParamNode(document,"testsuiteName", " "));
				parameter.appendChild(createParamNode(document,"description", " "));
				  
				parameter.appendChild(createParamNode(document,"lsTestcases", " "));
				parameter.appendChild(createParamNode(document,"screenRes", "1280~x~1024"));
				parameter.appendChild(createParamNode(document,"timeout", "300"));
				parameter.appendChild(createParamNode(document,"isRecordVideo", "false"));

				parameter.appendChild(createParamNode(document,"scmUrl", " "));
				parameter.appendChild(createParamNode(document,"scmUsername", "vuongnt"));
				parameter.appendChild(createParamNode(document,"scmPassword", "pss201806"));
				parameter.appendChild(createParamNode(document,"executingJobId", "-1"));
				
//				String[] scmUrlArray = scmUrl.split("//");
//				  
//			    parameter.appendChild(createParamNode(document,"gitDomain", scmUrlArray[0] + "//"));
//				parameter.appendChild(createParamNode(document,"gitScmUser", scmUsername));
//				parameter.appendChild(createParamNode(document,"gitScmPass", scmPassword));
//				parameter.appendChild(createParamNode(document,"gitServer",scmUrlArray[1] + "git/bottest/"));

				
			}
			
			//Add label
			NodeList label = document.getElementsByTagName("assignedNode");
			label.item(0).setTextContent(assignLabel);
			
			
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			
			transformer.transform(new DOMSource(document), new StreamResult(writer));
			
				
			String xmlConfig = writer.getBuffer().toString();
			xmlConfig = xmlConfig.replaceAll("%", "%25");
			//xmlConfig = URLEncoder.encode(xmlConfig, "UTF-8");
			//xmlConfig = URLDecoder.decode( xmlConfig, "UTF-8");
			client.api().jobsApi().create(null, jobName, xmlConfig);

        } catch (TransformerException e) {
        	LOGGER.error("End JenkinsServices createUserJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		} catch (ParserConfigurationException e) {
			LOGGER.error("End JenkinsServices createUserJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		} catch (SAXException e) {
			LOGGER.error("End JenkinsServices createUserJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		} catch (IOException e) {
			LOGGER.error("End JenkinsServices createUserJob ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service JenkinService Function createUserJob");
	}

}

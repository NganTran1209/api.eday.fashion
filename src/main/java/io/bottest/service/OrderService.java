package io.bottest.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import io.bottest.dto.BrandDTO;
import io.bottest.dto.SellManagerDTO;
import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.BrandRepository;
import io.bottest.jpa.respository.OrderRepository;
import io.bottest.jpa.respository.ProjectRepository;

@Service
public class OrderService {
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	BrandRepository brandRepo;
	
	@Autowired
	BottestUserRepository userRepo;
	
	@Autowired
	OrderRepository orderRepo;
	public List<Map<String,Object>> getListOrder(String user, String productId) {
		List<Map<String,Object>> orderList = projectRepo.orderList(user, productId) ;
		 return orderList;
	}
	//dang ban san pham
	public SellManagerDTO sellProduct(SellManagerDTO sellDTO, BrandDTO brand, String productCode,UserDetails userDetails) throws Exception {
		SellManagerDTO sell = new SellManagerDTO();
		if(productCode != null) {
			BottestUser theUser = userRepo.findByUsernameIgnoreCase(userDetails.getUsername());
			String name = theUser.getLastname().concat(theUser.getFirstname());
			Map<String, Object> branch = brandRepo.findByBrachName(name);
			if(!branch.isEmpty()) {
				//tao order_interface_header cho product
				orderRepo.createOrderInterfaceHeader(branch.get("BRANCH_ID").toString(),branch.get("BRANCH_NAME").toString(),brand.getBrandId(),new Date(), userDetails.getUsername());
				Map<String, Object> orderInterfaceHeader = orderRepo.findOrderInterfaceHeader(branch.get("BRANCH_ID").toString(),branch.get("BRANCH_NAME").toString(),brand.getBrandId(),userDetails.getUsername());
				if(orderInterfaceHeader != null) {
					if(!orderInterfaceHeader.isEmpty()) {
						//tao order_interface_item voi order_id la order_id cua order_interface_header
						orderRepo.createOrderInterfaceItem(orderInterfaceHeader.get("ORDER_ID").toString(),productCode,name,sellDTO.getAmount(),sellDTO.getPrice(), new Date());
						Map<String, Object> orderInterfaceItem = orderRepo.findOrderInterfaceItem(orderInterfaceHeader.get("ORDER_ID").toString(),productCode,name,sellDTO.getAmount(),sellDTO.getPrice());
						if(orderInterfaceItem != null) {
							if(!orderInterfaceItem.isEmpty()) {
								Map<String, Object> productSale  = orderRepo.findProductSell(orderInterfaceItem.get("ORDER_ID").toString());
								if(productSale != null) {
									if(!productSale.isEmpty()) {
										BigDecimal amount= (BigDecimal) productSale.get("SELECTED_AMOUNT");
										Float total_quantity = orderRepo.findTotalQuantity(productCode);
										//so sanh so luong dang ban voi so luong con trong kho
										if(amount.floatValue() <= total_quantity && (amount.floatValue() > 0)) {
											sell.setAmount(amount.floatValue());
											sell.setPrice(((BigDecimal) productSale.get("UNIT_PRICE")).floatValue());
											sell.setSellDate((String) productSale.get("CREATED_STAMP").toString());
											float toMoney = (((BigDecimal) productSale.get("UNIT_PRICE")).floatValue()) *  (((BigDecimal) productSale.get("SELECTED_AMOUNT")).floatValue()) ;
											sell.setToMoney(toMoney);
										}else {
											throw new Exception("More than quantity in stock!");
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return sell;
	}
}

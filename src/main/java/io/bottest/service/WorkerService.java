package io.bottest.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.dto.PayRollDTO;
import io.bottest.dto.WorkDTO;
import io.bottest.dto.WorkerDTO;
import io.bottest.jpa.entity.PayRoll;
import io.bottest.jpa.entity.PayRollIdentity;
import io.bottest.jpa.entity.Work;
import io.bottest.jpa.entity.Worker;
import io.bottest.jpa.respository.PayRollRepository;
import io.bottest.jpa.respository.WorkRepository;
import io.bottest.jpa.respository.WorkerRepository;
import io.bottest.utils.DateUtil;
import io.bottest.utils.FarmateConstants;

@Transactional
@Service
public class WorkerService {
	
	private final Logger LOGGER = LogManager.getLogger(WorkerService.class);
	
	@Autowired
	private WorkerRepository workerRepository;
	
	@Autowired
	private PayRollRepository payRollRepository;
	
	@Autowired
	private WorkRepository workRepository;
	
	public List<WorkerDTO> getAllWorkerById(Long id){
		LOGGER.info("Begin Service WorkerService Function getAllWorker");
		LOGGER.info("Service WorkerService Functon getAllWorker PARAM id: " + id);
		
		List<WorkerDTO> listWorkerDTO = new ArrayList<WorkerDTO>();
		List<Worker> listWorkerEntities = workerRepository.findAllWorkerByOwnerId(id);
		
		for(int i = 0; i < listWorkerEntities.size(); i++) {
			List<PayRollDTO> listPayRollDTO = new ArrayList<PayRollDTO>();
			List<WorkDTO> listWorkDoingDTO = new ArrayList<WorkDTO>();
			List<WorkDTO> listWorkDoneDTO = new ArrayList<WorkDTO>();
			WorkerDTO workerDto = new WorkerDTO();
		}	
			
		return listWorkerDTO;
	}
	
	public String[] convertDateStringToArray(String date) {
		LOGGER.info("Begin Service WorkerService Function convertDateStringToArray");
		LOGGER.info("Service WorkerService Function convertDateStringToArray PARAM date: " + date);
		
		String arrayDate[] = date.split(",");
		List<String> listDate = new ArrayList<String>();
		for(int i = 0; i < arrayDate.length; i ++) {
			StringBuffer sb = new StringBuffer();
			String arrayElementDate[] = arrayDate[i].split("-");
			for(int item = 0; item < arrayElementDate.length; item ++) {
				sb.append(arrayElementDate[item]).toString();
			}
			listDate.add(sb.toString());
		}
		String listDatetoArray[] = new String[listDate.size()];
		listDate.toArray(listDatetoArray);
		
		LOGGER.info("End Service WorkerService Function convertDateStringToArray");
		return listDatetoArray;
	}
	
	public String[] convertScheduleToStandardFormatDate(String[] schedule) {
		LOGGER.info("Begin Service WorkerService Function convertScheduleToStandardFormatDate");
		LOGGER.info("Service WorkerService Function convertScheduleToStandardFormatDate PARAM: " + schedule);
		
		String[] scheduleModified = new String[schedule.length];
		for(int i = 0; i < schedule.length; i ++) {
			switch(schedule[i]) {
			case "th2":
				scheduleModified[i] = "Mon";
				break;
			case "th3":
				scheduleModified[i] = "Tue";
				break;
			case "th4":
				scheduleModified[i] = "Wed";
				break;
			case "th5":
				scheduleModified[i] = "Thu";
				break;
			case "th6":
				scheduleModified[i] = "Fri";
				break;
			case "th7":
				scheduleModified[i] = "Sat";
				break;
			case "c":
				scheduleModified[i] = "Sun";
				break;
			}
		}
		
		LOGGER.info("End Service WorkerService Function convertScheduleToStandardFormatDate");
		return scheduleModified;
	}
	
	public String setSalaryToMonth(Long workerId, Date month, Date dateStart,String[] schedule, String unitSalary, String hourOfDay, String contractSalary, int dayOff, int dayWork) {
		LOGGER.info("Begin Servcice WorkerService Function setSalaryToMonth");
		LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM schedule: " + schedule);
		LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM unitSalary: " + unitSalary);
		LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM hourOfDay: " + hourOfDay);
		LOGGER.info("Service WorkerService Fucntion setSalaryToMonth PARAM contractSalary: " + contractSalary);
		LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM dayOff: " + dayOff);
		LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM dayWork: " + dayWork);
		
		LocalDate localDate = LocalDate.now();
		int dayOfMonth = localDate.getMonth().length(true);
		Calendar calendar = Calendar.getInstance();
		int currentDate = calendar.get(Calendar.DAY_OF_MONTH);
		int currentMonth = calendar.get(Calendar.MONTH) + 1;
		int currentYear = calendar.get(Calendar.YEAR);
		long salaryToMonth = 0;
		
		String convertDateStart = String.valueOf(dateStart);
		String scheduleFormatDate[] = convertScheduleToStandardFormatDate(schedule);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDateFormatter = LocalDate.parse(convertDateStart, formatter);
		int dayStart = localDateFormatter.getDayOfMonth();
		int monthStart = localDateFormatter.getMonthValue();
		int yearStart = localDateFormatter.getYear();
		
		if(yearStart > currentYear) {
			salaryToMonth = 0;
		} else if(yearStart < currentYear) {
			salaryToMonth = setSalaryToCurrentMonthCheckDateStart(scheduleFormatDate,unitSalary,hourOfDay,contractSalary,dayOff,dayWork,dayStart);
		} else if(yearStart == currentYear) {
			if(monthStart > currentMonth) {
				salaryToMonth = 0;
			} else if (monthStart < currentMonth) {
				salaryToMonth = setSalaryToCurrentMonthCheckDateStart(scheduleFormatDate,unitSalary,hourOfDay,contractSalary,dayOff,dayWork,dayStart);
			} else if (monthStart == currentMonth) {
				if (dayStart >  currentDate) {
					salaryToMonth = 0;
				} else if (dayStart <= currentDate) {
					salaryToMonth = setSalaryToCurrentMonthCheckDateStart(scheduleFormatDate,unitSalary,hourOfDay,contractSalary,dayOff,dayWork,dayStart);
				}
			}
		}
		
		if(currentDate == dayOfMonth) {
			payRollRepository.updateSalaryToMonthByMonthAndWorkerId(salaryToMonth, month, workerId);
		}
		
		String convertSalaryToMonth = String.valueOf(salaryToMonth);
		
		LOGGER.info("End Service WorkerService Function setSalaryToMonth");
		return convertSalaryToMonth;
	}
	
	public long setSalaryToCurrentMonthCheckDateStart(String schedule[], String unitSalary, String hourOfDay, String contractSalary, int dayOff, int dayWork, int dayStart) {
		LOGGER.info("Begin Servcice WorkerService Function setSalaryToCurrentMonthCheckDateStart");
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + schedule);
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + unitSalary);
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + hourOfDay);
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + contractSalary);
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + dayOff);
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + dayWork);
		LOGGER.info("Service WorkerService Function setSalaryToCurrentMonthCheckDateStart PARAM: " + dayStart);
		
		Calendar calendar = Calendar.getInstance();
		int currentDate = calendar.get(Calendar.DAY_OF_MONTH);
		int contractSalarys = Integer.parseInt(contractSalary);
		int dayOfMonth;
		int salaryOfDay;
		int countDate = 0;
			
		long salaryToMonth = 0;
		for(int i = dayStart; i <= currentDate; i ++) {
			calendar.set(Calendar.DAY_OF_MONTH, i);
			Date date = calendar.getTime();
			String dateOfWeek = new SimpleDateFormat("EEE").format(date);
			for(int item = 0; item < schedule.length; item ++) {
				if(dateOfWeek.equalsIgnoreCase(schedule[item])) {
					countDate += 1;
					break;
				}
			}
		}		
		switch(unitSalary) {
			case "Tháng":
				/**
				 * mac dinh mot thang la 30 ngay neu tinh luong theo thang
				 */
				dayOfMonth = 30 - ((7-schedule.length) * 4);
				salaryOfDay = contractSalarys / dayOfMonth;
				salaryToMonth  = (countDate + dayWork - dayOff) * salaryOfDay;
				break;
			case "Ngày":
				salaryToMonth = (countDate + dayWork - dayOff) * contractSalarys;
				break;
			case "Giờ":
				int hourOfDays = Integer.parseInt(hourOfDay);
				salaryToMonth = (countDate + dayWork - dayOff) * contractSalarys * hourOfDays;
				break;
		}
		
		LOGGER.info("End Service WorkerService Function setSalaryToCurrentMonthCheckDateStart");
		return salaryToMonth;
	}
	
	public void saveWorker(WorkerDTO workerDTO) {
		LOGGER.info("Begin Service WorkerService Function saveWorker");
		LOGGER.info("Service WorkerService Function saveWorker PARAM:" + workerDTO);
		
		Worker worker = new Worker();

		
		LOGGER.info("End Service WorkerService Function saveWorker");
	}
	
	public Date getFirstDayOfMonthToPayRoll() {
		LOGGER.info("Begin Service WorkerService Function getFirstDayOfMonthToPayRoll");
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH) + 1);
        Date date =  calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date firstDayOfMonth = null;
        try {
			firstDayOfMonth = new SimpleDateFormat("yyyy-MM-dd").parse(format.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        LOGGER.info("End Service WorkerService Function getFirstDayOfMonthToPayRoll");
        return firstDayOfMonth;
	}
	
//	public void savePayRoll(Long workerId, Date firstDayOfMonth, String workTime, String unitSalary, Long contractSalary) {
//		LOGGER.info("Begin Service WorkerService Function savePayRoll");
//		LOGGER.info("Service WorkerService Function savePayRoll PARAM: " + workerId);
//		LOGGER.info("Service WorkerService Function savePayRoll PARAM: " + firstDayOfMonth);
//		LOGGER.info("Service WorkerService Function savePayRoll PARAM: " + workTime);
//		LOGGER.info("Service WorkerService Function savePayRoll PARAM: " + unitSalary);
//		LOGGER.info("Service WorkerService Function savePayRoll PARAM: " + contractSalary);
//		
//		PayRollIdentity payRollIdentity = new PayRollIdentity();
//		PayRoll payRoll = new PayRoll();
//		payRollIdentity.setMonth(firstDayOfMonth);
//		payRollIdentity.setWorkerId(workerId);
//		payRoll.setPayRollIdentity(payRollIdentity);
//		payRoll.setHourOfDay(workTime);
//		payRoll.setUnitSalary(unitSalary);
//		payRoll.setContractSalary(contractSalary);
//		payRoll.setStatus(false);
//		payRollRepository.save(payRoll);
//		
//		LOGGER.info("End Service WorkerService Function savePayRoll");
//	}

	public Date endDateBasedOnStartDate(String startDate){
		LOGGER.info("Begin Service WorkerService Function endDateBasedOnStartDate");
		LOGGER.info("Service WorkerService Fucntion endDateBasedOnStartDate PARAM: " + startDate);
		
		Date endDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(format.parse(startDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		calendar.add(Calendar.DAY_OF_MONTH, 365);
		String newDate = format.format(calendar.getTime());
		try {
			endDate = new SimpleDateFormat("yyyyMMdd").parse(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		LOGGER.info("End Service WorkerService Function endDateBasedOnStartDate");
		return endDate;
	}
	
	public WorkerDTO findWorkerById(Long workerId) {
		LOGGER.info("Begin Service WorkerService Function findWorkerById");
		LOGGER.info("Service WorkerService Function findWorkerById PARAM: " + workerId);
		
		WorkerDTO workerDTO = new WorkerDTO();
		LOGGER.info("End Service WorkerService Function findWorkerById");
		return workerDTO;
	}

	public void updateWorker(WorkerDTO workerDTO) {
		LOGGER.info("Begin Service WorkerService Function updateWorker");
		LOGGER.info("Service WorkerService Function updateWorker PARAM: "+ workerDTO);
			
		Worker worker = workerRepository.findById(Long.valueOf(workerDTO.getId())).get();
		payRollRepository.updatePayRollBasedOnMonthAndWorkerId(workerDTO.getWorkTime(), workerDTO.getUnitSalary(), Long.valueOf(workerDTO.getContractSalary()), getFirstDayOfMonthToPayRollForUpdate(), Long.valueOf(workerDTO.getId()));
		LOGGER.info("End Service WorkerService Function updateWorker");
	}

	public String getFirstDayOfMonthToPayRollForUpdate() {
		LOGGER.info("Begin Service WorkerService Function getFirstDayOfMonthToPayRollForUpdate");
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date date =  calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
        String firstDayOfMonth = format.format(date);
        
        LOGGER.info("End Service WorkerService Function getFirstDayOfMonthToPayRollForUpdate");
        return firstDayOfMonth;
	}

	public void updateDayOffOrDayWorkWorker(String workerId, PayRollDTO payRollDTO) {
		LOGGER.info("Begin Service WorkerService Function updateDayOffOrDayWorkWorker");
		LOGGER.info("Service WorkerService Function updateDayOffOrDayWorkWorker PARAM: " + workerId);
		LOGGER.info("Service WorkerService Function updateDayOffOrDayWorkWorker PARAM: " + payRollDTO);
		
			String currentDateAndMonth = getFirstDayOfMonthToPayRollForUpdate();
			PayRoll payRoll = payRollRepository.findPayRollWorkerByMonthAndWorkerId(currentDateAndMonth, Long.valueOf(workerId));
			
			if(payRoll != null) {
				String dayOff[] = payRollDTO.getDayOff();
				StringJoiner sjOff = new StringJoiner(",");
				for(int i = 0; i < dayOff.length; i++) {
					sjOff.add(dayOff[i]);
				}
				payRoll.setDayOff(sjOff.toString());
				Worker worker = workerRepository.getWorkerById(Long.valueOf(workerId));
				Long totalSalary = this.setSalaryWorker(payRoll, worker);
				payRoll.setTotalSalary(totalSalary);
//				String dayWork[] = payRollDTO.getDayWork();
//				StringJoiner sjWork = new StringJoiner(",");
//				for(int i = 0; i < dayWork.length; i++) {
//					sjWork.add(dayWork[i]);
//				}
//				payRoll.setDayWork(sjWork.toString());
				
				payRollRepository.save(payRoll);
				LOGGER.info("End Service WorkerService Function updateDayOffOrDayWorkWorker");
			}
	}

	public void updateStatusPayRollOfWorker(Long workerId,PayRollDTO payRollDTO) {
		LOGGER.info("Begin Service WorkerService Function updateStatusPayRollOfWorker");
		LOGGER.info("Service WorkerService Function updateStatusPayRollOfWorker PARAM: " + workerId);
		LOGGER.info("Service WorkerService Function updateStatusPayRollOfWorker PARAM: " + payRollDTO);
		
//		String status = payRollDTO.getStatus();
		String month = payRollDTO.getMonth();
		boolean stringToBoolean = true;
		payRollRepository.updateStatusPayRollOfWorker(stringToBoolean, month, workerId);
		
		LOGGER.info("End Service WorkerService Function updateStatusPayRollOfWorker");
	}

	
	public String getMonthToPayRoll() {
		LOGGER.info("Begin Service WorkerService Function getFirstDayOfMonthToPayRoll");
		
        Date date = new Date();
        try {
            String month = DateUtil.convertDateToFormaterString(date);
            if(!StringUtils.isEmpty(month)) {
            	return month;
            }
        } catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
        LOGGER.info("End Service WorkerService Function getFirstDayOfMonthToPayRoll");
        return "";
	}
	
	
	public void savePayRollWorker(Worker worker) {
		LOGGER.info("Begin Service WorkerService Function savePayRoll");
		LOGGER.info("Service WorkerService Function savePayRoll PARAM workerId: " + worker.getId());
		LOGGER.info("Service WorkerService Function savePayRoll PARAM month: " + this.getMonthToPayRoll());
		LOGGER.info("Service WorkerService Function savePayRoll PARAM contractSalary: " + worker.getUnit_salary());
		
		PayRollIdentity payRollIdentity = new PayRollIdentity();
		PayRoll payRoll = new PayRoll();
		payRollIdentity.setMonth(this.getMonthToPayRoll());
		payRollIdentity.setWorkerId(worker.getId());
		payRoll.setPayRollIdentity(payRollIdentity);
		payRoll.setContractSalary(worker.getUnit_salary());
		payRoll.setStatus(false);
		payRoll.setTotalSalary(worker.getUnit_salary());
		payRollRepository.save(payRoll);
		
		LOGGER.info("End Service WorkerService Function savePayRoll");
		
	}

	public Long setSalaryWorker(PayRoll payRoll, Worker worker) {
		String arrayDayOff[] = null;
		if (payRoll.getDayOff() != null)
			arrayDayOff = convertDateStringToArray(payRoll.getDayOff());
		
//		String arrayDayWork[] = convertDateStringToArray(payRoll.getDayWork());
		int dayOff = (arrayDayOff == null) ? 0 : arrayDayOff.length;
		return this.setSalaryWork(payRoll.getPayRollIdentity().getWorkerId(), payRoll.getPayRollIdentity().getMonth(), worker.getCreateDay(), 
				payRoll.getContractSalary(),dayOff, payRoll.getBonus());
		
	}

	private Long setSalaryWork(Long workerId, String month, Date createDay, Long contactSalary, int dayOff, Long bonus) {
		LocalDate localDate = LocalDate.now();
		int dayOfMonth = localDate.getMonth().length(true); //thang co bao nhieu ngay
		Calendar calendar = Calendar.getInstance(); //tao moi the hien cua calendar
		int currentDate = calendar.get(Calendar.DAY_OF_MONTH); //ngay hien tai cua thang hien tai 
		int currentMonth = calendar.get(Calendar.MONTH) + 1; //thang hien tai
		int currentYear = calendar.get(Calendar.YEAR); //nam hien tai
		long salaryToCurrentDay = 0;
		long salaryToEndMonth = 0;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

		int firstDayOfMonth = 1;
		
		int actualDay = dayOfMonth - dayOff;
		
		if(actualDay <= 0) {
			return (long) 0;
		}else {
			bonus =  (bonus != null) ? bonus : 0;
			return (contactSalary/dayOfMonth) * actualDay + bonus;
		}
		
	}

	
	
}

/*
 * 
 * public String setSalaryToMonth(int schedule, String unitSalary, String hourOfDay, String contractSalary, int dayOff, int dayWork) {
LOGGER.info("Begin Servcice WorkerService Function setSalaryToMonth");
LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM schedule: " + schedule);
LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM unitSalary: " + unitSalary);
LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM hourOfDay: " + hourOfDay);
LOGGER.info("Service WorkerService Fucntion setSalaryToMonth PARAM contractSalary: " + contractSalary);
LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM dayOff: " + dayOff);
LOGGER.info("Service WorkerService Function setSalaryToMonth PARAM dayWork: " + dayWork);

int contractSalarys = Integer.parseInt(contractSalary);
int dayOfMonth;
int salaryOfDay;
int salaryToMonth = 0;
switch(unitSalary) {
	case "Tháng":
		dayOfMonth = 30 - ((7-schedule) * 4);
		salaryOfDay = contractSalarys / dayOfMonth;
		salaryToMonth  = (dayOfMonth + dayWork - dayOff) * salaryOfDay;
		break;
	case "Ngày":
		dayOfMonth = 30 - ((7-schedule) * 4);
		salaryToMonth = (dayOfMonth + dayWork - dayOff) * contractSalarys;
		break;
	case "Giờ":
		int hourOfDays = Integer.parseInt(hourOfDay);
		dayOfMonth = 30 - ((7-schedule) * 4);
		salaryToMonth = (dayOfMonth + dayWork - dayOff) * contractSalarys * hourOfDays;
		break;
}
String convertSalaryToMonth = String.valueOf(salaryToMonth);

LOGGER.info("End Service WorkerService Function setSalaryToMonth");
return convertSalaryToMonth;
}
 */


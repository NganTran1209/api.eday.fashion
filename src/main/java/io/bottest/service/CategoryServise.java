package io.bottest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.Category;
import io.bottest.jpa.respository.CategoryRepository;

@Service
public class CategoryServise {

	@Autowired
	private CategoryRepository categoryRepo;

	public List<Category> findAll() {
		return categoryRepo.findAll();
	}
	
	
}

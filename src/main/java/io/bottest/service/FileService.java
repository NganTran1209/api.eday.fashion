package io.bottest.service;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.intercept.RunAsManager;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.CopyLayout;
import io.bottest.dto.ResultTestcaseStatusDTO;
import io.bottest.dto.TestDataDTO;
import io.bottest.dto.UploadFileDTO;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.entity.ProjectProgress;
import io.bottest.jpa.entity.ProjectProgressIdentity;
import io.bottest.jpa.entity.ResultInfo;
import io.bottest.jpa.entity.UploadTestData;
import io.bottest.jpa.respository.ProjectProgressRepository;
import io.bottest.jpa.respository.ProjectRepository;
import io.bottest.jpa.respository.ResultInfoRepository;
import io.bottest.jpa.respository.UploadTestDataRepository;
//import io.bottest.utils.GitSupport;
import io.jsonwebtoken.lang.Collections;

@Service
public class FileService implements FilenameFilter{
	final public static String TMP_DIR_NAME = "tmp";
	final public static String TESTCASES_DIR_NAME = "Testcases";
	final public static String LAYOUTS_DIR_NAME = "Layouts";
	final public static String TESTDATAS_DIR_NAME = "TestDatas";
	final public static String LAYOUTS_IMAGE_DIR_NAME = "images";
	final public static String RESULTS_DIR_NAME = "Results";
	final public static String MANUAL_DIR_NAME = "Manuals";
	final public static String MANUALS_FILES_DIR_NAME = "Files";
	final public static String ISSUE_IMAGES = "IssueImages";

	final public static String TESTSUITE_FILE = "testsuite.json";
	final public static String USERS_FILE = "users.json";
	final public static String PROJECT_FILE = "project.json";
	final public static int MAX_SIZE = 75000;
	
	final public static String GIT = "git";
	final public static String SVN = "svn";
	final public static String GIT_CLONE ="START /REALTIME /W /MIN git clone %1$s";
	final public static String GIT_ADD ="START /REALTIME /W /MIN git add --all";
	final public static String GIT_UPDATE ="START /REALTIME /W /MIN git pull";
	final public static String GIT_DELETE="START /REALTIME /W /MIN git rm -r %1$s";
	final public static String GIT_COMMIT ="START /REALTIME /W /MIN git commit -m \"Bottest commit change %1$s\" ";
	final public static String GIT_PUSH ="START /REALTIME /W /MIN git push ";
	
	final public static String SVN_CLONE ="START /REALTIME /W /MIN svn co %1$s %2$s --username %3$s --password %4$s --non-interactive --trust-server-cert-failures=unknown-ca";
	final public static String SVN_ADD ="START /REALTIME /W /MIN svn add \"%1$s\" --force";
	final public static String SVN_UPDATE ="START /REALTIME /W /MIN svn update";
	final public static String SVN_DELETE="START /REALTIME /W /MIN svn delete \"%1$s\"";
	final public static String SVN_COMMIT = "START /REALTIME /W /MIN svn commit -m \"Bottest Commit change %1$s\" ";
	final public static String SVN_SWITCH = "START /REALTIME /W /MIN svn switch %1$s ";
	
	final public static String PRODUCT_DIR_NAME = "Product";
	final public static String PRODUCT_FILES_DIR_NAME = "Files";

	private final Logger LOGGER = LogManager.getLogger(FileService.class);

	@Value("${resource.path}")
	private String resourcePath;

	//@Value("classpath:svncheckout.sh")
	Resource resourceFile;
	
	//@Value("${dataSampleSql.path}")
	private String pathDataSample;

	@Autowired
	private ProjectRepository projectRepo;
	
	@Autowired
	private UploadTestDataRepository uploadTestdataRepo;
	
	@Autowired
	private ProjectProgressRepository projectProgressRepo;

	@Autowired
	private ResultInfoRepository resultInfoRepo;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	/**
	 * @param projectId
	 * @return
	 * @throws JSONException 
	 */
	

	
	public File[] getProjectTree(String username,String projectId) throws JSONException {
		
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId);
		File testsuiteFolder = path.toFile();
		File[] listfiles = testsuiteFolder.listFiles();
		return listfiles;

	}

	/**
	 * @param projectId
	 * @param testsuiteId
	 * @return
	 */
	public StringBuffer getTestsuiteTree(String username,String projectId, String testsuiteId) {
		LOGGER.info("Begin Service FileService function getTestsuiteTree");
		LOGGER.info("Begin Service FileService function getTestsuiteTree PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function getTestsuiteTree PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function getTestsuiteTree PARAMS testsuiteId: " + testsuiteId);
		StringBuffer result = new StringBuffer();
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuiteId);
		File testsuiteFolder = path.toFile();
		
		if (!testsuiteFolder.exists() || !testsuiteFolder.isDirectory()) {
			
			LOGGER.info("Begin Service FileService function getTestsuiteTree MESSAGE: testsuiteFolder is not exist");
			return result;
		}
		StringBuffer cases = this.readAllTestCaseAndLastRun(username,projectId, testsuiteId);
		StringBuffer layouts = this.readAllLayouts(username, projectId, testsuiteId);
		StringBuffer results = this.readAllResults(username,projectId, testsuiteId);
		StringBuffer casesManual = this.readAllTestCaseAndLastRunManual(username, projectId, testsuiteId);

		result.append("{");
		result.append("\n\"Testcases\":[");
		result.append(cases);
		result.append("],");
		result.append("\n\"TestcasesManual\":[");
		result.append(casesManual);
		result.append("],");
		result.append("\n\"Layouts\":[");
		result.append(layouts);
		result.append("],");
		result.append("\n\"Results\":[");
		result.append(results);
		result.append("]");
		result.append("}");
		
		LOGGER.info("Service FileService function getTestsuiteTree RESULTS result:" + result.toString());
		LOGGER.info("Begin Service FileService function getTestsuiteTree");
		return result;
	}

	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public StringBuffer readAllLayouts(String username,String projectId, String testsuitId) {
		LOGGER.info("Begin Service FileService function readAllLayouts");
		LOGGER.info("Begin Service FileService function readAllLayouts PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function readAllLayouts PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function readAllLayouts PARAMS testsuitId: " + testsuitId);
		StringBuffer sb = new StringBuffer();
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.LAYOUTS_DIR_NAME);

		File layouts =path.toFile();
		if (!layouts.exists() || !layouts.isDirectory()) {
			
			LOGGER.info("End Service FileService function readAllLayouts MESSAGE: layouts is not exist");
			return sb;
		}
		File[] layoutFiles = layouts.listFiles(this);

		if (layoutFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function readAllLayouts MESSAGE: layoutFiles is not exist");
			return sb;
		}
		for (File file : layoutFiles) {
			String testscase = readAJsonFile(file);
			sb.append(testscase + ",");
		}

		LOGGER.info("End Service FileService function readAllLayouts");
		return sb.deleteCharAt(sb.length() - 1);

	}

	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public StringBuffer readAllTestCase(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function readAllTestCase");
		LOGGER.info("Begin Service FileService function readAllTestCase PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function readAllTestCase PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function readAllTestCase PARAMS testsuitId: " + testsuitId);
		StringBuffer sb = new StringBuffer();
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.TESTCASES_DIR_NAME);
				
		File testcases = path.toFile();
		if (!testcases.exists() || !testcases.isDirectory()) {
			
			LOGGER.info("End Service FileService function readAllTestCase MESSAGE: testcases is not exist");
			return sb;
		}
		File[] testCaseFiles = testcases.listFiles(this);

		if (testCaseFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function readAllTestCase MESSAGE: testCaseFiles is not exist");
			return sb;
		}
		for (File fileName : testCaseFiles) {
			String testscase = readAJsonFile( fileName);
			if (testscase != null) {
				sb.append(testscase + ",");
			}
		}

		LOGGER.info("End Service FileService function readAllTestCase" );
		return sb.deleteCharAt(sb.length() - 1);

	}
	
	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public StringBuffer readAllTestCaseManual(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function readAllTestCaseManual");
		LOGGER.info("Begin Service FileService function readAllTestCaseManual PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function readAllTestCaseManual PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function readAllTestCaseManual PARAMS testsuitId: " + testsuitId);
		StringBuffer sb = new StringBuffer();
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.MANUAL_DIR_NAME);
				
		File testcases = path.toFile();
		if (!testcases.exists() || !testcases.isDirectory()) {
			
			LOGGER.info("End Service FileService function readAllTestCaseManual MESSAGE: testcases is not exist");
			return sb;
		}
		File[] testCaseFiles = testcases.listFiles(this);

		if (testCaseFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function readAllTestCaseManual MESSAGE: testCaseFiles is not exist");
			return sb;
		}
		for (File fileName : testCaseFiles) {
			String testscase = readAJsonFile( fileName);
			if (testscase != null) {
				sb.append(testscase + ",");
			}
		}

		LOGGER.info("End Service FileService function readAllTestCaseManual" );
		return sb.deleteCharAt(sb.length() - 1);

	}
	
	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public int countAllTestCase(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function countAllTestCase");
		LOGGER.info("Begin Service FileService function countAllTestCase PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function countAllTestCase PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function countAllTestCase PARAMS testsuitId: " + testsuitId);
		int count = 0;
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.TESTCASES_DIR_NAME);
				
		File testcases = path.toFile();
		if (!testcases.exists() || !testcases.isDirectory()) {
			
			LOGGER.info("End Service FileService function countAllTestCase MESSAGE: testcases is not exist");
			return count;
		}
		File[] testCaseFiles = testcases.listFiles(this);

		if (testCaseFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function countAllTestCase MESSAGE: testCaseFiles is not exist");
			return count;
		}
		count = testCaseFiles.length;

		LOGGER.info("Service FileService function countAllTestCase RESULTS count:" + count);
		LOGGER.info("End Service FileService function countAllTestCase");
		return count;
	}
	
	/**
	 * Read all layout in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public int countAllLayouts(String username,String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function countAllLayouts");
		LOGGER.info("Begin Service FileService function countAllLayouts PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function countAllLayouts PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function countAllLayouts PARAMS testsuitId: " + testsuitId);
		int count = 0;
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.LAYOUTS_DIR_NAME);

		File layouts =path.toFile();
		if (!layouts.exists() || !layouts.isDirectory()) {
			
			LOGGER.info("End Service FileService function countAllLayouts MESSAGE: layouts is not exist");
			return count;
		}
		File[] layoutFiles = layouts.listFiles(this);

		if (layoutFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function countAllLayouts MESSAGE: layoutFiles is not exist");
			return count;
		}
		count = layoutFiles.length;

		LOGGER.info("Service FileService function countAllLayouts RESULTS count:" + count);
		LOGGER.info("End Service FileService function countAllLayouts");
		return count;

	}
	
	/**
	 * Read all testdatas in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public int countAllTestDatas(String username,String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function countAllTestDatas");
		LOGGER.info("Begin Service FileService function countAllTestDatas PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function countAllTestDatas PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function countAllTestDatas PARAMS testsuitId: " + testsuitId);
		int count = 0;
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.TESTDATAS_DIR_NAME);

		File layouts =path.toFile();
		if (!layouts.exists() || !layouts.isDirectory()) {
			
			LOGGER.info("End Service FileService function countAllTestDatas MESSAGE: layouts is not exist");
			return count;
		}
//		File[] layoutFiles = layouts.listFiles(this);
		String[] layoutFilesStr = layouts.list();
		if (layoutFilesStr.length <= 0) {
			
			LOGGER.info("End Service FileService function countAllTestDatas MESSAGE: layoutFilesStr is not exist");
			return count;
		}
		count = layoutFilesStr.length;

		LOGGER.info("Service FileService function countAllTestDatas RESULTS count:" + count);
		LOGGER.info("End Service FileService function countAllTestDatas");
		return count;

	}
	
	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public int countAllResults(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function countAllResults");
		LOGGER.info("Begin Service FileService function countAllResults PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function countAllResults PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function countAllResults PARAMS testsuitId: " + testsuitId);
		int count= 0;
		File[] resultFiles = getResultFileOrderByTime(username,projectId, testsuitId);
		if ((resultFiles == null) || (resultFiles.length <= 0)) {
			
			LOGGER.info("End Service FileService function countAllResults MESSAGE: resultFiles is not exist");
			return count;
		}
		count = resultFiles.length;

		LOGGER.info("Service FileService function countAllResults RESULTS count:" + count);
		LOGGER.info("End Service FileService function countAllResults");
		return count;
	}
	
	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public StringBuffer readAllTestCaseAndLastRun(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRun");
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRun PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRun PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRun PARAMS testsuitId: " + testsuitId);
		StringBuffer sb = new StringBuffer("");
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.TESTCASES_DIR_NAME);
				
		File testcases = path.toFile();
		if (!testcases.exists() || !testcases.isDirectory()) {
			
			LOGGER.info("End Service FileService function readAllTestCaseAndLastRun MESSAGE: testcases is not exist");
			return sb;
		}
		File[] testCaseFiles = testcases.listFiles(this);

		if (testCaseFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function readAllTestCaseAndLastRun MESSAGE: testCaseFiles is not exist");
			return sb;
		}
		for (File fileName : testCaseFiles) {
			String testscase = readAJsonFile( fileName);
			try {
				JSONObject jsonOb = new JSONObject(testscase);
				if (testscase != null) {
					ResultInfo rsInfo = resultInfoRepo.findByProjectIdAndTestSuiteAndTestCaseName(Integer.parseInt(projectId), testsuitId, fileName.getName().split("\\.")[0]);
					if(rsInfo != null) {
						jsonOb.put("TestcaseResult", rsInfo.getTestCaseResult());
						jsonOb.put("ExecuteDate", rsInfo.getExecuteDate());
						jsonOb.put("RunningUser", rsInfo.getRunningUser());
					} else {
						jsonOb.put("TestcaseResult", "");
						jsonOb.put("ExecuteDate", "");
						jsonOb.put("RunningUser", "");
					}
					sb.append(jsonOb.toString() + ",");
					
				}
			} catch (JSONException e) {
				LOGGER.error("Service FileService function readAllTestCaseAndLastRun ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
			
		}

		LOGGER.info("End Service FileService function readAllTestCaseAndLastRun");
		return sb.deleteCharAt(sb.length() - 1);

	}
	
	/**
	 * Read all testcase manual in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public StringBuffer readAllTestCaseAndLastRunManual(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRunManual");
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRunManual PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRunManual PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function readAllTestCaseAndLastRunManual PARAMS testsuitId: " + testsuitId);
		StringBuffer sb = new StringBuffer("");
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuitId,FileService.MANUAL_DIR_NAME);
				
		File testcases = path.toFile();
		if (!testcases.exists() || !testcases.isDirectory()) {
			
			LOGGER.info("End Service FileService function readAllTestCaseAndLastRunManual MESSAGE: testcases is not exist");
			return sb;
		}
		File[] testCaseFiles = testcases.listFiles(this);

		if (testCaseFiles.length <= 0) {
			
			LOGGER.info("End Service FileService function readAllTestCaseAndLastRunManual MESSAGE: testCaseFiles is not exist");
			return sb;
		}
		for (File fileName : testCaseFiles) {
			String testscase = readAJsonFile( fileName);
			try {
				JSONObject jsonOb = new JSONObject(testscase);
				if (testscase != null) {
					ResultInfo rsInfo = resultInfoRepo.findByProjectIdAndTestSuiteAndTestCaseName(Integer.parseInt(projectId), testsuitId, fileName.getName().split("\\.")[0]);
					if(rsInfo != null) {
						jsonOb.put("TestcaseResult", rsInfo.getTestCaseResult());
						jsonOb.put("ExecuteDate", rsInfo.getExecuteDate());
						jsonOb.put("RunningUser", rsInfo.getRunningUser());
					} else {
						jsonOb.put("TestcaseResult", "");
						jsonOb.put("ExecuteDate", "");
						jsonOb.put("RunningUser", "");
					}
					sb.append(jsonOb.toString() + ",");
					
				}
			} catch (JSONException e) {
				LOGGER.error("Service FileService function readAllTestCaseAndLastRunManual ERROR MESSAGE:" + ExceptionUtils.getStackTrace(e));
			}
			
		}

		LOGGER.info("End Service FileService function readAllTestCaseAndLastRunManual");
		return sb.deleteCharAt(sb.length() - 1);

	}

	/**
	 * Read all testcase in testsuite
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public StringBuffer readAllResults(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function readAllResults");
		LOGGER.info("Begin Service FileService function readAllResults PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function readAllResults PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function readAllResults PARAMS testsuitId: " + testsuitId);
		StringBuffer sb = new StringBuffer();
		File[] resultFiles = getResultFileOrderByTime(username,projectId, testsuitId);
		if ((resultFiles == null) || (resultFiles.length <= 0)) {
			
			LOGGER.info("End Service FileService function readAllResults MESSAGE: resultFiles is not exist");
			return sb;
		}
		for (File file : resultFiles) {
			String result = readAJsonFile(file);
			sb.append( result + ",");
		}

		LOGGER.info("End Service FileService function readAllResults");
		return sb.deleteCharAt(sb.length() - 1);
	}

//	/**
//	 * Get last run result of testsuite
//	 * 
//	 * @param projectId
//	 * @param testsuiteId
//	 * @return
//	 */
//	private StatusTestsuiteDTO getLastRunContent(String username,String projectId, String testsuiteId) {
//		ResultInfo resultName = resultInfo.findLastRunOfTestSuite(projectId,testsuiteId);
//		StringBuffer lastRun = new StringBuffer("\"lastRun\":");
////		StringBuffer stDTOStr = new StringBuffer("\"ResultInfor\":");
//		StatusTestsuiteDTO stDTO = new StatusTestsuiteDTO();
////		Date date = new Date();
////		List<Map<String,Object>> resultIf = resultInfo.findTotalGraphic(Integer.parseInt(projectId), date);
////		stDTO.setResultInfo(resultName);
//////		File[] resultFiles = getResultFileOrderByTime(username, projectId, testsuiteId);
//		if (resultName == null) {
//			lastRun.append("{\"TestResult\":{\"Result\": \"NOT YET\"}}");
//			stDTO.setLastRun(lastRun.toString());
//			
//		} else if(resultName.getTestResult().equals("PASS")){
//			lastRun.append("{\"TestResult\":{\"Result\": \"PASS\"}}");
//			stDTO.setLastRun(lastRun.toString());
////			stDTO.setTotalTestcase();
//		}else {
//			lastRun.append("{\"TestResult\":{\"Result\": \"FAILED\"}}");
//			stDTO.setLastRun(lastRun.toString());
//		}
//		
////		//File file = resultFiles[0];
////		Path testResult = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuiteId,FileService.RESULTS_DIR_NAME);
////		String result = readAJsonFile(testResult);
////
////		if (!isJSONValid(result)) {
////			lastRun.append("{\"TestResult\":{\"Result\": \"ABORT\"}}");
////			//lastRun.append(res);
////			
////		} else {
////			lastRun.append(result);
////		}
//		
//		return stDTO;
//
//	}

	private File[] getResultFileOrderByTime(String username, String projectId, String testsuiteId) {
		
		LOGGER.info("Begin Service FileService function getResultFileOrderByTime");
		LOGGER.info("Begin Service FileService function getResultFileOrderByTime PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function getResultFileOrderByTime PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function getResultFileOrderByTime PARAMS testsuitId: " + testsuiteId);
		Path path = Paths.get(this.resourcePath, username.replace("@", "."), projectId, testsuiteId,FileService.RESULTS_DIR_NAME);

		File results = path.toFile();
		if (!results.exists() || !results.isDirectory()) {
			
			LOGGER.info("End Service FileService function getResultFileOrderByTime MESSAGE: results is not exist");
			return null;
		}
		File[] resultFiles = results.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				if (pathname.isFile()) {
					String name = pathname.getName();
					if (name.startsWith("Result_") && name.endsWith(".json")) {
						return true;
					}
				}
				return false;
			}
		});

		Arrays.sort(resultFiles, new Comparator<File>() {
			public int compare(File f1, File f2) {
				return Long.compare(f2.lastModified(), f1.lastModified());
			}
		});

		LOGGER.info("End Service FileService function getResultFileOrderByTime");
		return resultFiles;

	}

	/**
	 * Read a testcase file into a string
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param testcaseName
	 * @return
	 * @throws IOException
	 */
	public void saveLayout(String username, String projectId, String testsuitId, String content, String layoutName) throws IOException {

		LOGGER.info("Begin Service FileService function saveLayout");
		LOGGER.info("Begin Service FileService function saveLayout PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function saveLayout PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function saveLayout PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function saveLayout PARAMS content: " + content);
		LOGGER.info("Begin Service FileService function saveLayout PARAMS layoutName: " + layoutName);
		Path filePath = Paths.get(this.resourcePath, username.replace("@", "."),projectId, testsuitId, FileService.LAYOUTS_DIR_NAME,
				layoutName + ".json");
		Files.write(filePath, content.getBytes());
		this.scmAddNew(username, projectId,filePath.toFile());
		
		LOGGER.info("End Service FileService function saveLayout");
	}
	
	/**
	 * copy a layout file
	 * 
	 * @param projectId
	 * @return
	 * @throws IOException
	 */
	public void copyLayout(String username, String projectId, CopyLayout copyLayoutDTO) throws IOException {
		
		LOGGER.info("Begin Service FileService function copyLayout");
		LOGGER.info("Begin Service FileService function copyLayout PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function copyLayout PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function copyLayout PARAMS copyLayoutDTO: " + new Gson().toJson(copyLayoutDTO, new TypeToken<CopyLayout>() {}.getType()));
		// Copy define layout file
		Path filePathOld = Paths.get(this.resourcePath, username.replace("@", "."), projectId, copyLayoutDTO.getTestsuiteOld(), FileService.LAYOUTS_DIR_NAME, 
				copyLayoutDTO.getLayoutNameOld()+".json");
		String fileContentOld = this.readAJsonFile(filePathOld.toFile());
		JSONObject jsonOb;
		try {
			jsonOb = new JSONObject(fileContentOld);
			jsonOb.put("Name", copyLayoutDTO.getLayoutNameNew());
			this.saveLayout(username, projectId, copyLayoutDTO.getTestsuiteNew(), jsonOb.toString(), copyLayoutDTO.getLayoutNameNew() );
		} catch (JSONException e) {
			LOGGER.error("Service FileService function copyLayout ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
//		fileContentOld.replace("\""+ copyLayoutDTO.getLayoutNameOld() +"\"", "\""+ copyLayoutDTO.getLayoutNameNew() +"\"");
		
		
		List<String> imgs = this.getImage(username, projectId, copyLayoutDTO.getTestsuiteOld(), copyLayoutDTO.getLayoutNameOld());
		
		for (String imageName : imgs) {
			Path filePathImageOld = Paths.get(this.resourcePath,username.replace("@", "."), projectId, copyLayoutDTO.getTestsuiteOld(),FileService.LAYOUTS_DIR_NAME,
					FileService.LAYOUTS_IMAGE_DIR_NAME, imageName); 
			Path filePathImageNew = Paths.get(this.resourcePath,username.replace("@", "."), projectId, copyLayoutDTO.getTestsuiteNew(),FileService.LAYOUTS_DIR_NAME,
					FileService.LAYOUTS_IMAGE_DIR_NAME, imageName.replace(copyLayoutDTO.getLayoutNameOld(), copyLayoutDTO.getLayoutNameNew())); 
			Files.copy(filePathImageOld, filePathImageNew, StandardCopyOption.REPLACE_EXISTING);
			this.scmAddNew(username, projectId, filePathImageNew.toFile());
		}
		
		LOGGER.info("End Service FileService function copyLayout");
	}
	
	/**
	 * copy a testdata file
	 * 
	 * @param projectId
	 * @return
	 * @throws IOException
	 */
	public void copyTestData(String username, String projectId, CopyLayout copyLayoutDTO) throws IOException {
	
		LOGGER.info("Begin Service FileService function copyTestData");
		LOGGER.info("Begin Service FileService function copyTestData PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function copyTestData PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function copyTestData PARAMS copyLayoutDTO: " + new Gson().toJson(copyLayoutDTO, new TypeToken<CopyLayout>() {}.getType()));
		//copy testdata
		Path filePathOld = Paths.get(this.resourcePath,username.replace("@", "."), projectId, copyLayoutDTO.getTestsuiteOld(), FileService.TESTDATAS_DIR_NAME,
				copyLayoutDTO.getLayoutNameOld() + ".json");
		String fileContentOld = this.readAJsonFile(filePathOld.toFile());
		JSONObject jsonOb;
		try {
			jsonOb = new JSONObject(fileContentOld);
			jsonOb.put("layoutName", copyLayoutDTO.getLayoutNameNew());
			this.writeTestData(username, projectId, copyLayoutDTO.getTestsuiteNew(), jsonOb.toString(), copyLayoutDTO.getLayoutNameNew());
			
			LOGGER.info("End Service FileService function copyTestData");
		} catch (JSONException e) {
			LOGGER.error("Service FileService function copyTestData ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
	}
	
//	/**
//	 * write a string to testcase file
//	 * 
//	 * @param projectId
//	 * @param testsuitId
//	 * @param testcaseName
//	 * @return
//	 * @throws IOException
//	 */
//	public void writeTestCase(String username,String projectId, String testsuitId, String content, String testcaseName)
//			throws IOException {
//		String status = "add";
//		String type = "automation";
//		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.TESTCASES_DIR_NAME,
//				testcaseName + ".json");
//		Files.write(filePath, content.getBytes());
//		this.scmAddNew(username, projectId, filePath.toFile());
//		String fileContent = this.readAJsonFile(filePath.toFile());
//		this.writeProjectProgress(username, projectId, testsuitId, fileContent, testcaseName, status, type);
//	}
	
	/**
	 * write a string to testcase file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param testcaseName
	 * @return
	 * @throws IOException
	 */
	public void writeTestCase(String username,String projectId, String testsuitId, ProjectProgress projectProgress, String testcaseName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function writeTestCase");
		LOGGER.info("Begin Service FileService function writeTestCase PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function writeTestCase PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function writeTestCase PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function writeTestCase PARAMS projectProgress: " + new Gson().toJson(projectProgress, new TypeToken<ProjectProgress>() {}.getType()));
		LOGGER.info("Begin Service FileService function writeTestCase PARAMS testcaseName: " + testcaseName);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.TESTCASES_DIR_NAME,
				testcaseName + ".json");
		JSONObject json = new JSONObject();
		try {
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			df.setTimeZone(tz);
			json.put("username", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("Testsuite", projectProgress.getProjectProgressIdentity().getTestsuite());
			json.put("Testcase", testcaseName);
			json.put("Description", projectProgress.getDescription());
			json.put("TestcaseProcedure", projectProgress.getTestcaseProcedure());
			json.put("ExpectedOutput", projectProgress.getExpectedOutput());
			if(!projectProgress.getTestscript().equals("")) {
				JSONArray cvTestscript = new JSONArray(projectProgress.getTestscript());
				json.put("Testscripts", cvTestscript);
			} else {
				json.put("Testscripts", projectProgress.getTestscript());
			}
			
			json.put("CreatedBy", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("UpdatedBy", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("CreatedDate", df.format(projectProgress.getCreateDate()));
			json.put("UpdatedDate", df.format(projectProgress.getUpdateDate()));
			json.put("DeleteFlag", false);
			
			Path folderPath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.TESTCASES_DIR_NAME);
			File directory = new File(String.valueOf(folderPath));

			if(!directory.exists()){
			    directory.mkdir();
			}
			Files.write(filePath, json.toString().getBytes());
			this.scmAddNew(username, projectId, filePath.toFile());

			LOGGER.info("End Service FileService function writeTestCase");
		} catch (JSONException e) {
			LOGGER.error("Service FileService function writeTestCase ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
	}
	
	/**
	 * write a string to testcase manual file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param testcaseName
	 * @return
	 * @throws IOException
	 */
	public void writeTestCaseManual(String username,String projectId, String testsuitId, ProjectProgress projectProgress, String testcaseName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function writeTestCaseManual");
		LOGGER.info("Begin Service FileService function writeTestCaseManual PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function writeTestCaseManual PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function writeTestCaseManual PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function writeTestCaseManual PARAMS projectProgress: " + new Gson().toJson(projectProgress, new TypeToken<ProjectProgress>() {}.getType()));
		LOGGER.info("Begin Service FileService function writeTestCaseManual PARAMS testcaseName: " + testcaseName);
		String userpath = username.replace("@", ".");
		Path filePathManual = Paths.get(this.resourcePath, userpath, projectId, testsuitId, FileService.MANUAL_DIR_NAME);
		
		File f = new File(filePathManual.toFile().toString());
		if (f.exists() && f.isDirectory()) {
			
		} else {
			Path filePathManualFile = Paths.get(this.resourcePath,userpath, projectId, testsuitId, FileService.MANUAL_DIR_NAME, FileService.MANUALS_FILES_DIR_NAME);
			
			Files.createDirectories(Paths.get(filePathManual.toUri()));
			Files.createDirectories(Paths.get(filePathManualFile.toUri()));
		}	

		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.MANUAL_DIR_NAME,
				testcaseName + ".json");
		JSONObject json = new JSONObject();
		try {
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			df.setTimeZone(tz);
			json.put("UpdatedBy", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("Testsuite", projectProgress.getProjectProgressIdentity().getTestsuite());
			json.put("Description", projectProgress.getDescription());
			json.put("CreatedBy", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("ExpectedOutput", projectProgress.getExpectedOutput());
			json.put("TestcaseProcedure", projectProgress.getTestcaseProcedure());
			json.put("Testcase", testcaseName);
			json.put("UpdatedDate", df.format(projectProgress.getUpdateDate()));
			json.put("Testscripts", new ArrayList<String>());
			json.put("CreatedDate", df.format(projectProgress.getCreateDate()));
			json.put("DeleteFlag", false);
			json.put("username", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("AssigneeTo", projectProgress.getAssignTo());
			json.put("PreCondition", projectProgress.getPreCondition());
			json.put("Results", new ArrayList<String>());
			
			Files.write(filePath, json.toString().getBytes());
			this.scmAddNew(username, projectId, filePath.toFile());

			LOGGER.info("End Service FileService function writeTestCaseManual");
		} catch (JSONException e) {
			LOGGER.error("Service FileService function writeTestCaseManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
	}
	
	/**
	 * edit a string to testcase manual file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param testcaseName
	 * @return
	 * @throws IOException
	 */
	public void editTestCaseManual(String username,String projectId, String testsuitId, ProjectProgress projectProgress, String testcaseName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function editTestCaseManual");
		LOGGER.info("Begin Service FileService function editTestCaseManual PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function editTestCaseManual PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function editTestCaseManual PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function editTestCaseManual PARAMS projectProgress: " + new Gson().toJson(projectProgress, new TypeToken<ProjectProgress>() {}.getType()));
		LOGGER.info("Begin Service FileService function editTestCaseManual PARAMS testcaseName: " + testcaseName);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.MANUAL_DIR_NAME,
				testcaseName + ".json");
		try {
			JSONObject json = new JSONObject();
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			df.setTimeZone(tz);
			json.put("UpdatedBy", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("Testsuite", projectProgress.getProjectProgressIdentity().getTestsuite());
			json.put("Description", projectProgress.getDescription());
			json.put("CreatedBy", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("ExpectedOutput", projectProgress.getExpectedOutput());
			json.put("TestcaseProcedure", projectProgress.getTestcaseProcedure());
			json.put("Testcase", testcaseName);
			json.put("UpdatedDate", df.format(projectProgress.getUpdateDate()));
			json.put("Testscripts", new ArrayList<String>());
			json.put("CreatedDate", df.format(projectProgress.getCreateDate()));
			json.put("DeleteFlag", false);
			json.put("username", projectProgress.getProjectProgressIdentity().getProjectId().toString());
			json.put("AssigneeTo", projectProgress.getAssignTo());
			json.put("PreCondition", projectProgress.getPreCondition());
			json.put("Results", new ArrayList<String>());
			
			Files.write(filePath, json.toString().getBytes());
			this.scmAddNew(username, projectId, filePath.toFile());

			LOGGER.info("End Service FileService function editTestCaseManual");
		} catch (JSONException e) {
			LOGGER.error("Service FileService function editTestCaseManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
	}
	
	/**
	 * write a result to testcase manual file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param testcaseName
	 * @return
	 * @throws IOException
	 */
	public void writeResultToManual(String username,String projectId, String testsuitId, ResultInfo resultInfoDTO, String testcaseName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function writeResultToManual");
		LOGGER.info("Begin Service FileService function writeResultToManual PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function writeResultToManual PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function writeResultToManual PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function writeResultToManual PARAMS resultInfoDTO: " + new Gson().toJson(resultInfoDTO, new TypeToken<ResultInfo>() {}.getType()));
		LOGGER.info("Begin Service FileService function writeResultToManual PARAMS testcaseName: " + testcaseName);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.MANUAL_DIR_NAME,
				testcaseName + ".json");
		String fileContent = this.readAJsonFile(filePath.toFile());
		try {
			JSONObject json = new JSONObject(fileContent);
				
			List<ResultInfo> reslMax = resultInfoRepo.getAllResultInProjectIdAndTestSuiteAndTestcaseName(Integer.parseInt(projectId), testsuitId, testcaseName);
			JSONArray jrA = new JSONArray();
			for(int i = 0; i < reslMax.size(); i++) {
				JSONArray lsFile = new JSONArray(reslMax.get(i).getFiles());
				
				ResultInfo results = reslMax.get(i);
				JSONObject jb = new JSONObject(results.toString());
				jb.put("files", lsFile);
				
				jrA.put(jb);
			}
			json.put("Results", jrA);
				
			Files.write(filePath, json.toString().getBytes());
			this.scmAddNew(username, projectId, filePath.toFile());

			LOGGER.info("End Service FileService function writeResultToManual");
		} catch (JSONException e) {
			LOGGER.error("Service FileService function writeResultToManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
	}
	
	/**
	 * delete a result to testcase manual file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param testcaseName
	 * @return
	 * @throws IOException
	 */
	public void deleteResultToManual(String username,String projectId, String testsuitId, Long id, String testcaseName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function deleteResultToManual");
		LOGGER.info("Begin Service FileService function deleteResultToManual PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function deleteResultToManual PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function deleteResultToManual PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function deleteResultToManual PARAMS id: " + id);
		LOGGER.info("Begin Service FileService function deleteResultToManual PARAMS testcaseName: " + testcaseName);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.MANUAL_DIR_NAME,
				testcaseName + ".json");
		String fileContent = this.readAJsonFile(filePath.toFile());
		try {
			JSONObject json = new JSONObject(fileContent);
			
			List<ResultInfo> reslMax = resultInfoRepo.getAllResultInProjectIdAndTestSuiteAndTestcaseName(Integer.parseInt(projectId), testsuitId, testcaseName);
			JSONArray jrA = new JSONArray();
			for(int i = 0; i < reslMax.size(); i++) {
				JSONArray lsFile = new JSONArray(reslMax.get(i).getFiles());
				
				ResultInfo results = reslMax.get(i);
				JSONObject jb = new JSONObject(results.toString());
				jb.put("files", lsFile);
				
				jrA.put(jb);
			}
			json.put("Results", jrA);
			
			Files.write(filePath, json.toString().getBytes());
			this.scmAddNew(username, projectId, filePath.toFile());

			LOGGER.info("End Service FileService function deleteResultToManual");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.error("Service FileService function deleteResultToManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
	}
	
	/**
	 * write a string to testcase file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 * @throws IOException
	 */
	public void writeProjectProgress(String username,String projectId, String testsuitId, String fileContent, String testcaseName, String status, String type)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function writeProjectProgress");
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS fileContent: " + fileContent);
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS testcaseName: " + testcaseName);
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS status: " + status);
		LOGGER.info("Begin Service FileService function writeProjectProgress PARAMS type: " + type);
		JSONObject jsonOb;
		try {
			jsonOb = new JSONObject(fileContent);
			String createDateStr = jsonOb.getString("CreatedDate");
			String updateDateStr = jsonOb.getString("UpdatedDate");
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date createDate = df.parse(createDateStr);
			Date updateDate = df.parse(updateDateStr);
			
			ProjectProgressIdentity proIdentity = new ProjectProgressIdentity();
			proIdentity.setProjectId(Long.parseLong(projectId));
			proIdentity.setTestsuite(testsuitId);
			proIdentity.setTestcase(testcaseName);
			
			ProjectProgress p = new ProjectProgress();
			p.setProjectProgressIdentity(proIdentity);
			p.setUpdateUser(username);
			p.setUpdateDate(updateDate);
			p.setCreateDate(createDate);
			p.setStatus(status);
			p.setType(type);
	
			if(jsonOb.has("PreCondition")) {
				p.setPreCondition(jsonOb.getString("PreCondition"));
				p.setTestcaseProcedure(jsonOb.getString("TestcaseProcedure"));
				p.setExpectedOutput(jsonOb.getString("ExpectedOutput"));
				p.setDescription(jsonOb.getString("Description"));
				p.setAssignTo(jsonOb.getString("AssigneeTo"));
			}
			//set type automation or manual
			
			projectProgressRepo.save(p);
			
			LOGGER.info("Service FileService function writeProjectProgress RESULTS ProjectProgress:" + new Gson().toJson(p, new TypeToken<ProjectProgress>() {}.getType()));
			LOGGER.info("End Service FileService function writeProjectProgress");
		} catch (JSONException e) {
			LOGGER.error("Service FileService function writeProjectProgress ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}  catch (ParseException e) {
			LOGGER.error("Service FileService function writeProjectProgress ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
	}
	
	/**
	 * write a string to testcase file
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @param layoutName
	 * @return
	 * @throws IOException
	 */
	public void writeTestData(String username,String projectId, String testsuitId, String jsonDataText, String layoutName)
			throws IOException {

		LOGGER.info("Begin Service FileService function writeTestData");
		LOGGER.info("Begin Service FileService function writeTestData PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function writeTestData PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function writeTestData PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Begin Service FileService function writeTestData PARAMS layoutName: " + layoutName);
		LOGGER.info("Begin Service FileService function writeTestData PARAMS jsonDataText:" + jsonDataText);
		Path folderPath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.TESTDATAS_DIR_NAME);
		File folder = folderPath.toFile();
		if (!folder.exists()) {
			folder.mkdirs();
		}
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.TESTDATAS_DIR_NAME,
				layoutName + ".json");
		
		Files.write(filePath, jsonDataText.getBytes());
		this.scmAddNew(username, projectId, filePath.toFile());
		
		LOGGER.info("End Service FileService function writeTestData");
	}
	

//	/**
//	 * edit a string to testcase file
//	 * 
//	 * @param projectId
//	 * @param testsuitId
//	 * @param variableTestcaseNew
//	 * @return
//	 * @throws IOException
//	 */
//	public void editNameTestcase(String username,String projectId, String testsuitId, String content, String variableTestcaseOld,
//			String variableTestcaseNew) throws IOException {
//
//		Path filePath = Paths.get(this.resourcePath, username.replace("@", "."),projectId, testsuitId, FileService.TESTCASES_DIR_NAME,
//				variableTestcaseOld + ".json");
//		File file = filePath.toFile();
//		file.delete();
//		Path filePathNew = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.TESTCASES_DIR_NAME,
//				variableTestcaseNew + ".json");
//		Files.write(filePathNew, content.getBytes());
//
//	}

	/**
	 * delete a string to testcase file
	 * 
	 * @param projectId
	 * @param variableTestsuite
	 * @param variableTestcaseNew
	 * @return
	 * @throws IOException
	 */
	public void deleteTestcase(String username,String projectId, String variableTestsuite, String testcaseName) throws IOException {
		
		LOGGER.info("Begin Service FileService function deleteTestcase");
		LOGGER.info("Service FileService function deleteTestcase PARAMS username:" + username);
		LOGGER.info("Service FileService function deleteTestcase PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function deleteTestcase PARAMS variableTestsuite:" + variableTestsuite);
		LOGGER.info("Service FileService function deleteTestcase PARAMS testcaseName:" + testcaseName);
		
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, variableTestsuite, FileService.TESTCASES_DIR_NAME,
				testcaseName + ".json");
		File file = filePath.toFile();
		file.delete();
		scmDelete(username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteTestcase");
	}
	
	/**
	 * delete a string to testcase file
	 * 
	 * @param projectId
	 * @param variableTestsuite
	 * @param variableTestcaseNew
	 * @return
	 * @throws IOException
	 */
	public void deleteTestcaseManual(String username,String projectId, String variableTestsuite, String testcaseName) throws IOException {
		
		LOGGER.info("Begin Service FileService function deleteTestcaseManual");
		LOGGER.info("Service FileService function deleteTestcaseManual PARAMS username:" + username);
		LOGGER.info("Service FileService function deleteTestcaseManual PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function deleteTestcaseManual PARAMS variableTestsuite:" + variableTestsuite);
		LOGGER.info("Service FileService function deleteTestcaseManual PARAMS testcaseName:" + testcaseName);
		
		
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, variableTestsuite, FileService.MANUAL_DIR_NAME,
				testcaseName + ".json");
		File file = filePath.toFile();
		file.delete();
		scmDelete(username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteTestcaseManual");
	}

	/**
	 * @param projectId
	 * @param variableTestsuite
	 * @param layoutName
	 * @throws IOException
	 */
	public void deleteLayout(String username,String projectId, String variableTestsuite, String layoutName) throws IOException {

		LOGGER.info("Begin Service FileService function deleteLayout");
		LOGGER.info("Service FileService function deleteLayout PARAMS username:" + username);
		LOGGER.info("Service FileService function deleteLayout PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function deleteLayout PARAMS variableTestsuite:" + variableTestsuite);
		LOGGER.info("Service FileService function deleteLayout PARAMS layoutName:" + layoutName);
		
		Path filePath = Paths.get(this.resourcePath, username.replace("@", "."), projectId, variableTestsuite, FileService.LAYOUTS_DIR_NAME,
				layoutName + ".json");
		File file = filePath.toFile();
		file.delete();
		scmDelete(username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteLayout");
	}

//	/**
//	 * @param testsuiteId
//	 * @param testSuiteName
//	 * @param descriptions
//	 * @throws IOException
//	 * @throws JSONException
//	 */
//	public void editDescriptionTestSuite(String username,String projectId, String testSuiteName, String descriptions)
//			throws IOException, JSONException {
//
//		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testSuiteName, testSuiteName + ".json");
//		System.out.println(filePath);
//		String fileContent = this.readAJsonFile(filePath.toFile());
//
//		JSONObject jsonOb = new JSONObject(fileContent);
//		jsonOb.put("Description", descriptions);
//		// change description testsuite
//		Files.write(filePath, jsonOb.toString().getBytes());
////		scmCommitChange(username, projectId);
//		this.scmAddNew(username, projectId, filePath.toFile());
//
//	}

	/**
	 * 
	 * @param path
	 * @param fileName
	 * @return
	 */
	public String readFileLayout(String username, String projectId, String testSuiteName, String fileNameLayout) {

		LOGGER.info("Begin Service FileService function readFileLayout");
		LOGGER.info("Service FileService function readFileLayout PARAMS username:" + username);
		LOGGER.info("Service FileService function readFileLayout PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function readFileLayout PARAMS testSuiteName:" + testSuiteName);
		LOGGER.info("Service FileService function readFileLayout PARAMS fileNameLayout:" + fileNameLayout);
		
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testSuiteName, FileService.LAYOUTS_DIR_NAME,
				fileNameLayout + ".json");
		String fileContent = this.readAJsonFile(filePath);
		
		LOGGER.info("End Service FileService function readFileLayout");
		
		return fileContent;
	}
	
	/**
	 * 
	 * @param path
	 * @param fileName
	 * @return
	 */
	public String readFileData(String username, String projectId, String testSuiteName, String fileNameLayout) {
		LOGGER.info("Begin Service FileService function readFileData");
		LOGGER.info("Service FileService function readFileData PARAMS username:" + username);
		LOGGER.info("Service FileService function readFileData PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function readFileData PARAMS testSuiteName:" + testSuiteName);
		LOGGER.info("Service FileService function readFileData PARAMS fileNameLayout:" + fileNameLayout);
		
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testSuiteName, FileService.TESTDATAS_DIR_NAME,
				fileNameLayout + ".json");
		String fileContent = this.readAJsonFile(filePath);
		
		LOGGER.info("End Service FileService function readFileData");
		return fileContent;

	}

	/**
	 * @param projectId
	 * @param testSuiteName
	 * @param fileNameLayout
	 * @return
	 * @throws JSONException
	 */
	public String writeFileTestcase(String username, String projectId, String testSuiteName, String json, String testcaseName)
			 {
		
		LOGGER.info("Begin Service FileService function writeFileTestcase");
		LOGGER.info("Service FileService function writeFileTestcase PARAMS username:" + username);
		LOGGER.info("Service FileService function writeFileTestcase PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function writeFileTestcase PARAMS testSuiteName:" + testSuiteName);
		LOGGER.info("Service FileService function writeFileTestcase PARAMS json:" + json);
		LOGGER.info("Service FileService function writeFileTestcase PARAMS testcaseName:" + testcaseName);
		
		Path filePathTestCase = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testSuiteName, FileService.TESTCASES_DIR_NAME,
				testcaseName + ".json");
		try {
			JSONObject jsonOb = new JSONObject(json);
			Files.write(filePathTestCase, jsonOb.toString().getBytes());
			this.scmAddNew(username, projectId, filePathTestCase.toFile());
		} catch (IOException | JSONException e) {
			LOGGER.error("Service FileService function writeFileTestcase ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}

		LOGGER.info("End Service FileService function writeFileTestcase");
		return json;

	}
	
	/**
	 * @param projectId
	 * @param testSuiteName
	 * @param fileNameLayout
	 * @return
	 * @throws JSONException
	 */
	public List<ResultTestcaseStatusDTO> getResultTestcasePassFails(String username, String projectId, String testSuiteName, String resultTestcaseName, String testcaseName)
			 {
		
		LOGGER.info("Begin Service FileService function getResultTestcasePassFails");
		LOGGER.info("Service FileService function getResultTestcasePassFails PARAMS username:" + username);
		LOGGER.info("Service FileService function getResultTestcasePassFails PARAMS projectId:" + projectId);
		LOGGER.info("Service FileService function getResultTestcasePassFails PARAMS testSuiteName:" + testSuiteName);
		LOGGER.info("Service FileService function getResultTestcasePassFails PARAMS resultTestcaseName:" + resultTestcaseName);
		LOGGER.info("Service FileService function getResultTestcasePassFails PARAMS testcaseName:" + testcaseName);
		List<ResultTestcaseStatusDTO> listResult = new ArrayList<ResultTestcaseStatusDTO>();
		 String replace = testcaseName.replace("[","");
		 String replace1 = replace.replace("]","");
		 List<String> myList = new ArrayList<String>(Arrays.asList(replace1.split(",")));
		 
		 Path filePathTestCase = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testSuiteName, FileService.RESULTS_DIR_NAME,
				 resultTestcaseName + ".json");
			String fileContent = this.readAJsonFile(filePathTestCase.toFile());
				
			JSONObject jsonOb;
			try {
				if(fileContent != null) {
					jsonOb = new JSONObject(fileContent);
					String testcaseStr = jsonOb.getString("Testcases");
					String statusRun = jsonOb.getString("TestResult");
					
					Gson gson = new Gson();
					Object[] object = gson.fromJson(testcaseStr, Object[].class);
					ObjectMapper oMapper = new ObjectMapper();
					
					for(int i = 0; i < object.length; i++) {
						Map<String, Object> map = oMapper.convertValue(object[i], Map.class);
						for(int j = 0; j < myList.size(); j++) {
							if(myList.get(j).equals(map.get("Name"))) {
								ResultTestcaseStatusDTO resultDTO = new ResultTestcaseStatusDTO();
								resultDTO.setName(myList.get(j));
								resultDTO.setResult((String)map.get("Result"));
								resultDTO.setStatusRun(statusRun);
								listResult.add(resultDTO);
							}
						}
						
					}
				}
				
			} catch (JSONException e) {
				LOGGER.error("Service FileService function getResultTestcasePassFails ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
	 
		LOGGER.info("Service FileService function getResultTestcasePassFails RESULTS listResult:" + new Gson().toJson(listResult, new TypeToken<List<ResultTestcaseStatusDTO>>() {}.getType()));
		LOGGER.info("End Service FileService function getResultTestcasePassFails");
		return listResult;
	}


	/**
	 * @param path
	 * @param fileName
	 * @return
	 */
	public String readAJsonFile(File file) {
		
		LOGGER.info("Begin Service FileService function readAJsonFile");
		LOGGER.info("Service FileService function getResultTestcasePassFails PARAMS file:" + file.getAbsolutePath());
		Path filePath = Paths.get(file.getAbsolutePath());
		String fileContent = "";
		if (!file.exists()){
			
			LOGGER.info("End Service FileService function readAJsonFile MESSAGE: file is not exist");
			return fileContent;
		}
		try {
			fileContent = new String(Files.readAllBytes(filePath));

		} catch (IOException e) {
			LOGGER.error("Service FileService function readAJsonFile ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function readAJsonFile");
		return fileContent;

	}
	
	/**
	 * @param path
	 * @param fileName
	 * @return
	 */
	private String readAJsonFile(Path file) {

		LOGGER.info("Begin Service FileService function private readAJsonFile");
		LOGGER.info("Service FileService function private readAJsonFile PARAMS file: " + file);
		String fileContent = null;

		try {
			fileContent = new String(Files.readAllBytes(file));

		} catch (IOException e) {
			LOGGER.error("Service FileService function private readAJsonFile ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("Begin Service FileService function private readAJsonFile");
		return fileContent;

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
	 */
	@Override
	public boolean accept(File dir, String name) {
		if (name.endsWith(".json")) {
			return true;
		}
		return false;
	}

	/**
	 * @param file
	 * @param projectId
	 * @param testsuitId
	 * @param filename
	 */
	private void saveLayoutImageToServer(MultipartFile file, String username, String projectId, String testsuitId, String filename) {
		LOGGER.info("Begin Service FileService function saveLayoutImageToServer");
		LOGGER.info("Service FileService function saveLayoutImageToServer PARAMS username: " + username);
		LOGGER.info("Service FileService function saveLayoutImageToServer PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function saveLayoutImageToServer PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Service FileService function saveLayoutImageToServer PARAMS filename: " + filename);
		try {
			Path path = Paths.get(this.resourcePath, username.replace("@", "."),projectId, testsuitId, FileService.LAYOUTS_DIR_NAME,
					FileService.LAYOUTS_IMAGE_DIR_NAME);
			File imagesDir = path.toFile();
			if (!imagesDir.exists()) {
				imagesDir.mkdirs();
			}
			OutputStream out = null;
			try {
				File imageFile = new File(imagesDir, filename);
				out = new BufferedOutputStream(new FileOutputStream(imageFile));
				out.write(file.getBytes());
				this.scmAddNew(username, projectId, imageFile);
				
				LOGGER.info("End Service FileService function saveLayoutImageToServer");
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("Service FileService function saveLayoutImageToServer ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * @param projectId
	 * @param testsuitId
	 * @param layoutName
	 * @param imageString
	 * @throws IOException
	 */
	public void decodeToImage(String username,String projectId, String testsuitId, String layoutName, String imageString)
		 {
		LOGGER.info("Begin Service FileService function decodeToImage");
		LOGGER.info("Service FileService function decodeToImage PARAMS username: " + username);
		LOGGER.info("Service FileService function decodeToImage PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function decodeToImage PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Service FileService function decodeToImage PARAMS layoutName: " + layoutName);
		LOGGER.info("Service FileService function decodeToImage PARAMS imageString: " + imageString);
		String[] strings = imageString.split(",");
		String extension;
		switch (strings[0]) {// check image's extension
		case "data:image/jpeg;base64":
			extension = "jpeg";
			break;
		case "data:image/png;base64":
			extension = "png";
			break;
		default:// should write cases for more images types
			extension = "jpg";
			break;
		}
		// convert base64 string to binary data
		byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
		Date date = new Date();
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.LAYOUTS_DIR_NAME,
				FileService.LAYOUTS_IMAGE_DIR_NAME, layoutName + "_" + date.getTime() + "." + extension);
		Path folderPath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.LAYOUTS_DIR_NAME,
				FileService.LAYOUTS_IMAGE_DIR_NAME);
		File directory = new File(String.valueOf(folderPath));

		if(!directory.exists()){
		    directory.mkdir();
		}
		File file = new File(path.toString());
		try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
			outputStream.write(data);
		} catch (IOException e) {
			LOGGER.error("Service FileService function decodeToImage ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		this.scmAddNew(username, projectId, file);
		
		LOGGER.info("End Service FileService function decodeToImage");
	}

	/**
	 * @param projectId
	 * @param testsuitId
	 * @param fileName
	 * @throws IOException
	 */
	public void deleteImage(String username,String projectId, String testsuitId, String layoutName, String nameImage)
			throws IOException {
		LOGGER.info("Begin Service FileService function deleteImage");
		LOGGER.info("Service FileService function deleteImage PARAMS username: " + username);
		LOGGER.info("Service FileService function deleteImage PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function deleteImage PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Service FileService function deleteImage PARAMS layoutName: " + layoutName);
		LOGGER.info("Service FileService function deleteImage PARAMS nameImage: " + nameImage);
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.LAYOUTS_DIR_NAME,
				FileService.LAYOUTS_IMAGE_DIR_NAME, nameImage);
		File file = new File(path.toString());
		file.delete();
		scmDelete( username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteImage");
	}
	
	/**
	 * @param projectId
	 * @param testsuitId
	 * @param fileName
	 * @throws IOException
	 */
	public void deleteFileResultManual(String username,String projectId, String testsuitId, String fileName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function deleteFileResultManual");
		LOGGER.info("Service FileService function deleteFileResultManual PARAMS username: " + username);
		LOGGER.info("Service FileService function deleteFileResultManual PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function deleteFileResultManual PARAMS testsuitId: " + testsuitId);
		LOGGER.info("Service FileService function deleteFileResultManual PARAMS fileName: " + fileName);
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuitId, FileService.MANUAL_DIR_NAME,
				FileService.MANUALS_FILES_DIR_NAME, fileName);
		File file = new File(path.toString());
		file.delete();
		scmDelete( username, projectId, file);

		LOGGER.info("End Service FileService function deleteFileResultManual");
	}



	/**
	 * @param file
	 * @param projectId
	 * @param testsuiteId
	 * @param fileName
	 */
	public void uploadFile(MultipartFile file, String username,String projectId, String testsuiteId, String fileName) {
		
		LOGGER.info("Begin Service FileService function uploadFile");
		LOGGER.info("Service FileService function uploadFile PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function uploadFile PARAMS testsuitId: " + testsuiteId);
		LOGGER.info("Service FileService function uploadFile PARAMS fileName: " + fileName);
		// TODO Auto-generated method stub
		saveLayoutImageToServer(file,username, projectId, testsuiteId, fileName);
		//this.scmCommitChange(username, projectId);
		
		LOGGER.info("End Service FileService function uploadFile");
	}

	/**
	 * @param projectId
	 * @param content
	 * @param testSuiteName
	 * @throws IOException
	 */
	public void createTestSuite(String username,String projectId, JSONObject content) throws IOException {
		
		LOGGER.info("Begin Service FileService function createTestSuite");
		LOGGER.info("Service FileService function createTestSuite PARAMS username: " + username);
		LOGGER.info("Service FileService function createTestSuite PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function createTestSuite PARAMS content: " + content);
		String testSuiteName = "";
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		try {
			content.put("username", projectId);
			content.put("CreatedBy", projectId);
			content.put("UpdatedBy", projectId);
			content.put("CreatedDate", dateFormat.format(date));
			content.put("UpdatedDate", dateFormat.format(date));
			content.put("DeleteFlag", 0);
			testSuiteName = content.getString("Testsuite");
		} catch (Exception e) {
			LOGGER.error("Service FileService function createTestSuite ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		String userpath = username.replace("@", ".");
		Path filePathTestSuite = Paths.get(this.resourcePath, userpath, projectId, testSuiteName);
		Path filePathLayout = Paths.get(this.resourcePath, userpath,projectId, testSuiteName, FileService.LAYOUTS_DIR_NAME);
		Path filePathTestCase = Paths.get(this.resourcePath,userpath, projectId, testSuiteName, FileService.TESTCASES_DIR_NAME);
		Path filePathTestData = Paths.get(this.resourcePath, userpath, projectId, testSuiteName, FileService.TESTDATAS_DIR_NAME);
		Path filePathResults = Paths.get(this.resourcePath,userpath, projectId, testSuiteName, FileService.RESULTS_DIR_NAME);
		Path filePath = Paths.get(this.resourcePath,userpath, projectId, testSuiteName, testSuiteName + ".json");
		Path filePathImages = Paths.get(this.resourcePath,userpath, projectId, testSuiteName, FileService.LAYOUTS_DIR_NAME, FileService.LAYOUTS_IMAGE_DIR_NAME);
		Path filePathManual = Paths.get(this.resourcePath, userpath, projectId, testSuiteName, FileService.MANUAL_DIR_NAME);
		Path filePathManualFile = Paths.get(this.resourcePath,userpath, projectId, testSuiteName, FileService.MANUAL_DIR_NAME, FileService.MANUALS_FILES_DIR_NAME);
		
		Files.createDirectories(Paths.get(filePathTestSuite.toUri()));
		Files.createDirectories(Paths.get(filePathLayout.toUri()));
		Files.createDirectories(Paths.get(filePathTestCase.toUri()));
		Files.createDirectories(Paths.get(filePathTestData.toUri()));
		Files.createDirectories(Paths.get(filePathResults.toUri()));
		Files.createDirectories(Paths.get(filePathManual.toUri()));
		Files.createDirectories(Paths.get(filePathManualFile.toUri()));

		Files.createDirectories(Paths.get(filePathImages.toUri()));
		Files.write(filePath, content.toString().getBytes());
		scmAddNew( username, projectId,filePathTestSuite.toFile());
		
		LOGGER.info("End Service FileService function createTestSuite");
	}



	/**
	 * @param projectId
	 * @param testsuiteId
	 * @throws IOException
	 */
	public void deleteTestsuite(String username, String projectId, String testsuiteId) throws IOException {
		
		LOGGER.info("Begin Service FileService function deleteTestsuite");
		LOGGER.info("Service FileService function deleteTestsuite PARAMS username: " + username);
		LOGGER.info("Service FileService function deleteTestsuite PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function deleteTestsuite PARAMS testsuiteId: " + testsuiteId);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId);
		File file = new File(filePath.toString());
		FileUtils.deleteDirectory(file);
		scmDelete(username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteTestsuite");
	}
	
	/**
	 * @param projectId
	 * @param testsuiteId
	 * @throws IOException
	 */
	public String getDescriptionTestsuite(String username, String projectId, String testsuiteId) throws IOException {
		
		LOGGER.info("Begin Service FileService function getDescriptionTestsuite");
		LOGGER.info("Service FileService function getDescriptionTestsuite PARAMS username: " + username);
		LOGGER.info("Service FileService function getDescriptionTestsuite PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function getDescriptionTestsuite PARAMS testsuiteId: " + testsuiteId);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, testsuiteId + ".json");
		
		String fileContent = this.readAJsonFile(filePath.toFile());
		
		JSONObject jsonOb;
		String description = "";
		try {
			if(fileContent != null && fileContent != "") {
				jsonOb = new JSONObject(fileContent);
				description = jsonOb.getString("Description");
			}
			
		} catch (JSONException e) {
			LOGGER.error("Service FileService function getDescriptionTestsuite ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function getDescriptionTestsuite");
		return description;
	}

	/**
	 * @param projectId
	 * @param testsuiteId
	 * @param layoutName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> getImage(String username, String projectId, String testsuiteId, String layoutName) {
		
		LOGGER.info("Begin Service FileService function getImage");
		LOGGER.info("Service FileService function getImage PARAMS username: " + username);
		LOGGER.info("Service FileService function getImage PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function getImage PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function getImage PARAMS layoutName: " + layoutName);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId,FileService.LAYOUTS_DIR_NAME,FileService.LAYOUTS_IMAGE_DIR_NAME);

		
		File images = filePath.toFile();
		String[] imageFiles = images.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.startsWith(layoutName + "_")) {
					return true;
				}
				return false;
			}
		});
		
		LOGGER.info("End Service FileService function getImage");
		return Collections.arrayToList(imageFiles);

	}

	/**
	 * @param projectId
	 * @param testsuiteId
	 * @param fileName
	 * @return
	 */
	public Resource loadFileAsResource(String username, String projectId, String testsuiteId, String fileName) {
		
		LOGGER.info("Begin Service FileService function loadFileAsResource");
		LOGGER.info("Service FileService function loadFileAsResource PARAMS username: " + username);
		LOGGER.info("Service FileService function loadFileAsResource PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function loadFileAsResource PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function loadFileAsResource PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath,username.replace("@", ".") ,projectId, testsuiteId, FileService.LAYOUTS_DIR_NAME,
					FileService.LAYOUTS_IMAGE_DIR_NAME, fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				
				LOGGER.info("End Service FileService function loadFileAsResource");
				return resource;
			} else {
				// throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			// throw new MyFileNotFoundException("File not found " + fileName, ex);
			ex.printStackTrace();
			LOGGER.error("Service FileService function loadFileAsResource ERROR MESSAGE:" + ex.getMessage());
		}
		
		LOGGER.info("End Service FileService function loadFileAsResource");
		return null;
	}
	
	/**
	 * @param projectId
	 * @param testsuiteId
	 * @param fileName
	 * @return
	 */
	public Resource loadFileManualResult(String username, String projectId, String testsuiteId, String fileName) {
		LOGGER.info("Begin Service FileService function loadFileManualResult");
		LOGGER.info("Service FileService function loadFileManualResult PARAMS username: " + username);
		LOGGER.info("Service FileService function loadFileManualResult PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function loadFileManualResult PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function loadFileManualResult PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath,username.replace("@", ".") ,projectId, testsuiteId, FileService.MANUAL_DIR_NAME,
					FileService.MANUALS_FILES_DIR_NAME, fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				
				LOGGER.info("End Service FileService function loadFileManualResult");
				return resource;
			} else {
				// throw new MyFileNotFoundException("File not found " + fileName);
				LOGGER.error("Service FileService function loadFileManualResult ERROR MESSAGE: File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			// throw new MyFileNotFoundException("File not found " + fileName, ex);
			LOGGER.error("Service FileService function loadFileManualResult ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
		}
		
		LOGGER.info("End Service FileService function loadFileManualResult");
		return null;
	}

	/**
	 * @param projectId
	 * @return
	 * @throws IOException
	 */
	public Path createProject(String username, Project project) throws IOException {
		LOGGER.info("Begin Service FileService function createProject");
		LOGGER.info("Service FileService function createProject PARAMS username: " + username);
		LOGGER.info("Service FileService function createProject PARAMS project: " + new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
		Path dir = Paths.get(this.resourcePath, project.getId().toString());
//		scmCreateProject(username, project);
		
		LOGGER.info("End Service FileService function createProject");
		return Files.createDirectories(dir);

	}
	
	public void scmCreateProject(String username, Project project) {
		
		LOGGER.info("Begin Service FileService function scmCreateProject");
		LOGGER.info("Service FileService function scmCreateProject PARAMS username: " + username);
		LOGGER.info("Service FileService function scmCreateProject PARAMS project: " + new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
		StringBuffer cmd = new StringBuffer();
		String newline = System.lineSeparator();
		if (SVN.equalsIgnoreCase(project.getScmType())) {
			cmd.append("START /REALTIME /W /MIN svn mkdir -m \"Create new Project.\" " + project.getScmUrl() +
					" --username " + project.getScmUsername() + " --password " + project.getScmPassword() +
					" --non-interactive --trust-server-cert-failures=unknown-ca");
			cmd.append(newline);
			this.executeSCMCommand( cmd.toString());
		}
//		if (GIT.equalsIgnoreCase(project.getScmType())) {
//
//		    URL url;
//			try {
//		        url = new URL(project.getScmUrl() + "api/v3/user/repos");
//			    String user = project.getScmUsername();
//		  	    String pass = project.getScmPassword();
//		  	    String authStr = user + ":" + pass;
//		  	    String encoding = Base64.getEncoder().encodeToString(authStr.getBytes("utf-8"));
//	    	    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//		  	    connection.setRequestMethod("POST");
//		  	    connection.setDoOutput(true);
//		  	    connection.setRequestProperty("Authorization", "Basic " + encoding);
//		  	    connection.setRequestProperty("Content-Type", "application/json");
//		  	    
//		  	    JSONObject json = new JSONObject(); 
//		  	    json.put("name", project.getId().toString());
//		  	    json.put("description", project.getDescription());
//		  	    json.put("homepage", "https://www.bottest.io/bottest/");
//		        json.put("private", true);
//		        json.put("has_issues", false);
//		        json.put("has_projects", false);
//		        json.put("has_wiki", false);
//		        json.put("auto_init", true);
//		        
//		  	    String urlParams = json.toString();
//		        byte[] postData = urlParams.getBytes("utf-8");
//		        try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
//		            wr.write(postData);
//		        }
//		        InputStream content = connection.getInputStream();
//		        BufferedReader in = new BufferedReader(new InputStreamReader(content));
//	    	    String line;
//	    	    while ((line = in.readLine()) != null) {
//	    	    	LOGGER.info("Begin Service FileService function scmCreateProject " + line);
//	    	    }

//	    	    cmd.append("git config --global user.name \"bottest\" ");
//	    	    cmd.append(newline);
//	    	    cmd.append("git config --global user.email \"bottest@example.com\"");
//	    	    cmd.append(newline);
	  			
//	    	    String urlScm = project.getScmUrl();
//	    	    String domainScm = urlScm.substring(0, 5);
//	    	    if (!"https".equalsIgnoreCase(domainScm)) {
//	    	    	domainScm = "http";
//	    	    }
//	    	    urlScm = urlScm.replace(domainScm + "://", domainScm + "://" + user + ":" + pass + "@");
//	    	    cmd.append("git clone " + urlScm + "git/" + user + "/" + project.getId() + ".git");
//	  			cmd.append(newline);
//	    	    String projectId = String.valueOf(project.getId());
//	    	    GitSupport jgit = new GitSupport(project.getScmUsername(), project.getScmPassword());
//	    	    jgit.gitClone(project.getScmUrl() + "git/" + project.getScmUsername() + "/" + projectId + ".git", projectId, resourcePath, username.replace("@", "."));
//	    	    
//			} catch (MalformedURLException e) {
//				LOGGER.error("End Service FileService function scmCreateProject ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//			} catch (IOException e) {
//				LOGGER.error("End Service FileService function scmCreateProject ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//			} catch (JSONException e) {
//				LOGGER.error("End Service FileService function scmCreateProject ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
//			}
//  	      
//		}
		// Remove this line because used JGIT
//		this.executeSCMCommand( cmd.toString());
		try {
			this.syncProject(username, project);
		} catch (Exception e) {
			LOGGER.error("Service FileService function loadFileManualResult ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function scmCreateProject");
	}

	/**
	 * @param projectId
	 * @param testsuiteId
	 * @param layoutName
	 * @return
	 */
	public Resource loadOneImage(String username,String projectId, String testsuiteId, String layoutName) {
		List<String> images = getImage(username, projectId, testsuiteId, layoutName);
		if (images.isEmpty()) {

			return null;
		} else {

			return loadFileAsResource(username,projectId, testsuiteId, images.get(0));
		}
	}

	public boolean isJSONValid(String jsonInString) {
		
		LOGGER.info("Begin Service FileService function isJSONValid");
		LOGGER.info("Service FileService function isJSONValid PARAMS jsonInString: " + jsonInString);
		try {
			Gson gson = new Gson();
			gson.fromJson(jsonInString, Object.class);
			
			LOGGER.info("End Service FileService function isJSONValid");
			return true;
		} catch (com.google.gson.JsonSyntaxException ex) {
			LOGGER.error("End Service FileService function isJSONValid ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
			return false;
		}
	}

	public String readTestCaseFile(String username,String projectId, String testsuiteName, String testcase) {
		
		LOGGER.info("Begin Service FileService function readTestCaseFile");
		LOGGER.info("Service FileService function readTestCaseFile PARAMS username: " + username);
		LOGGER.info("Service FileService function readTestCaseFile PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function readTestCaseFile PARAMS testsuiteName: " + testsuiteName);
		LOGGER.info("Service FileService function readTestCaseFile PARAMS testcase: " + testcase);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."
				), projectId, testsuiteName, FileService.TESTCASES_DIR_NAME,
				testcase + ".json");
		String fileContent = null;
		try {
			fileContent = new String(Files.readAllBytes(filePath));

		} catch (IOException e) {
			LOGGER.error("End Service FileService function readTestCaseFile ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function readTestCaseFile");
		return fileContent;
	}


	public void syncProject( String username,String projectId) throws Exception {
		
		LOGGER.info("Begin Service FileService function syncProject");
		LOGGER.info("Service FileService function syncProject PARAMS username: " + username);
		LOGGER.info("Service FileService function syncProject PARAMS projectId: " + projectId);
		Optional<Project> project = projectRepo.findById(Long.valueOf(projectId));
		if (project.isPresent()) {
			this.syncProject(username,project.get());

		}
		
		LOGGER.info("End Service FileService function syncProject");
	}

	public String getResultDetail(String username, String projectId, String testsuiteName, String resultname) {
		
		LOGGER.info("Begin Service FileService function getResultDetail");
		LOGGER.info("Service FileService function getResultDetail PARAMS username: " + username);
		LOGGER.info("Service FileService function getResultDetail PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function getResultDetail PARAMS testsuiteName: " + testsuiteName);
		LOGGER.info("Service FileService function getResultDetail PARAMS resultname: " + resultname);
		Path filePath = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteName, FileService.RESULTS_DIR_NAME,
				resultname + ".json");

		LOGGER.info("End Service FileService function getResultDetail");
		return this.readAJsonFile(filePath.toFile());
	}

	public Resource loadResultFileAsResource(String username, String projectId, String testsuiteId, String resultname, String fileName) {
		LOGGER.info("Begin Service FileService function loadResultFileAsResource");
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS username: " + username);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS resultname: " + resultname);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.RESULTS_DIR_NAME,resultname, fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				LOGGER.info("End Service FileService function loadResultFileAsResource");
				return resource;
			} else {
				LOGGER.error("End Service FileService function loadResultFileAsResource ERROR MESSAGE: File not found");
			}
		} catch (MalformedURLException ex) {
			// throw new MyFileNotFoundException("File not found " + fileName, ex);
			LOGGER.error("End Service FileService function loadResultFileAsResource ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
		}
		
		LOGGER.info("End Service FileService function loadResultFileAsResource MESSAGE: Resource not found");
		return null;
	}
	
	public Resource loadResultFileAsResourceFromTestData(String username, String projectId, String testsuiteId, String fileName) {
		LOGGER.info("Begin Service FileService function loadResultFileAsResource");
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS username: " + username);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function loadResultFileAsResource PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME, fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				LOGGER.info("End Service FileService function loadResultFileAsResource");
				return resource;
			} else {
				LOGGER.error("End Service FileService function loadResultFileAsResource ERROR MESSAGE: File not found");
			}
		} catch (MalformedURLException ex) {
			// throw new MyFileNotFoundException("File not found " + fileName, ex);
			LOGGER.error("End Service FileService function loadResultFileAsResource ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
		}
		
		LOGGER.info("End Service FileService function loadResultFileAsResource MESSAGE: Resource not found");
		return null;
	}

	public boolean checkTestCaseIsExist(String username, String projectId, String testsuiteId, String testCaseName) {
		LOGGER.info("Begin Service FileService function checkTestCaseIsExist");
		LOGGER.info("Service FileService function checkTestCaseIsExist PARAMS username: " + username);
		LOGGER.info("Service FileService function checkTestCaseIsExist PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function checkTestCaseIsExist PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function checkTestCaseIsExist PARAMS testCaseName: " + testCaseName);
		
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTCASES_DIR_NAME,testCaseName +".json");
		
		LOGGER.info("End Service FileService function checkTestCaseIsExist");
		return path.toFile().exists();
	}

	public boolean checkLayoutIsExist(String username, String projectId, String testsuiteId, String layoutName) {
		
		LOGGER.info("Begin Service FileService function checkLayoutIsExist");
		LOGGER.info("Service FileService function checkLayoutIsExist PARAMS username: " + username);
		LOGGER.info("Service FileService function checkLayoutIsExist PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function checkLayoutIsExist PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function checkLayoutIsExist PARAMS layoutName: " + layoutName);
		
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.LAYOUTS_DIR_NAME,layoutName +".json");
		
		LOGGER.info("End Service FileService function checkLayoutIsExist");
		return path.toFile().exists();

	}

	public List<TestDataDTO> getTestData(String username, String projectId, String testsuiteId) {
		
		LOGGER.info("Begin Service FileService function getTestData");
		LOGGER.info("Service FileService function getTestData PARAMS username: " + username);
		LOGGER.info("Service FileService function getTestData PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function getTestData PARAMS testsuiteId: " + testsuiteId);
		List<TestDataDTO> result = new ArrayList<>();
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME );
		File dataFolder = path.toFile();
		if (!dataFolder.exists() || !dataFolder.isDirectory()) {
					
					LOGGER.info("End Service FileService function getTestData MESSAGE: testdata is not exist");
					return result;
				}
		File[] datas = dataFolder.listFiles();
		Arrays.sort(datas, new Comparator<File>() {
			public int compare(File f1, File f2) {
				return Long.compare(f2.lastModified(), f1.lastModified());
			}
		});
		for (File file : datas) {
			TestDataDTO data = new TestDataDTO() ;
			data.setFilename(file.getName());
			Date updated = new Date(file.lastModified());
			data.setUpdateDate(updated.toString() );
			data.setLayoutData(file.getName().endsWith(".json"));
			if(file.getName().endsWith(".json")) {
				Path pathcontent = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME,file.getName());
				String fileContent = this.readAJsonFile(pathcontent.toFile());
		
				JSONObject jsonOb;
				try {
					jsonOb = new JSONObject(fileContent);
					data.setDescription(jsonOb.getString("description"));
				} catch (JSONException e) {
					LOGGER.error("End Service FileService function getTestData ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
				}
				
			} else {
				UploadTestData uplTeDa = new UploadTestData();
				uplTeDa = uploadTestdataRepo.findByFileName(file.getName(), Long.parseLong(projectId), testsuiteId);
				if(uplTeDa != null) {
					data.setDescription(uplTeDa.getDescription());
				} else {
					data.setDescription("Test Data");
				}
				
			}
			
			
			result.add(data);
		}
		
		LOGGER.info("End Service FileService function getTestData");
		return result;
	}

	public Resource loadTestDataFileAsResource(String username, String projectId, String testsuiteId, String fileName) {
		LOGGER.info("Begin Service FileService function loadTestDataFileAsResource");
		LOGGER.info("Service FileService function loadTestDataFileAsResource PARAMS username: " + username);
		LOGGER.info("Service FileService function loadTestDataFileAsResource PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function loadTestDataFileAsResource PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function loadTestDataFileAsResource PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME, fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				
				LOGGER.info("End Service FileService function loadTestDataFileAsResource");
				return resource;
			} else {
			}
		} catch (MalformedURLException ex) {
			LOGGER.error("End Service FileService function loadTestDataFileAsResource ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
		}
		
		LOGGER.info("End Service FileService function loadTestDataFileAsResource MESSAGE: Resource is not found");
		return null;
	}


	public int syncProject( String username,Project project) {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				LOGGER.info("Begin Service FileService function syncProject");
				LOGGER.info("Service FileService function syncProject PARAMS username: " + username);
//				LOGGER.info("Service FileService function syncProject PARAMS project: " + new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
				int exitVal = 0;
				Path path = Paths.get(resourcePath, username.replace("@", "."));
				File folderPath = path.toFile();
				if (!folderPath.exists()) {
					folderPath.mkdirs();
				}
				String newLine =  System.lineSeparator();
				StringBuffer cmds = new StringBuffer();
				if (SVN.equalsIgnoreCase(project.getScmType())) {
					//Checkout
					cmds.append("cd \"" + path.toString()+ "\"" + newLine );
					String cli = String.format( FileService.SVN_CLONE, project.getScmUrl(),project.getId(),project.getScmUsername(), project.getScmPassword()) ;
					cmds.append(cli + newLine);
					//exitVal = executeSCMCommand(username,cli, workingDir);
					cmds.append("cd " + project.getId() + newLine);
					
					cmds.append("START /REALTIME /W /MIN svn cleanup " + newLine);
					cmds.append( FileService.SVN_UPDATE + newLine);
					cmds.append("START /REALTIME /W /MIN svn add . --force " + newLine );
					cmds.append(String.format(FileService.SVN_COMMIT, username) + newLine);
					exitVal = executeSCMCommand(cmds.toString(), true);
					
					
				}
//				else if (GIT.equalsIgnoreCase(project.getScmType())) {
//					String ssl = project.getScmUrl().substring(0, 5);
//					String methodWeb = "http";
//					if ("https".equals(ssl)) {
//						methodWeb = "https";
//					}
						
//					cmds.append("cd \"" + path.toString()+ "\"" + newLine );
//					String cli = String.format( FileService.GIT_CLONE, 
//							project.getScmUrl().replace(methodWeb + "://", methodWeb + "://" + project.getScmUsername() +":" + project.getScmPassword() + "@")) + "git/" + project.getScmUsername() + "/" + project.getId() + ".git";
//					cmds.append(cli + newLine);
//					cmds.append("cd " + project.getId() + newLine);
//					cmds.append("START /REALTIME /W /MIN git clean -f" + newLine);
//					cmds.append(FileService.GIT_UPDATE + newLine);
//					cmds.append(FileService.GIT_ADD + newLine);
//					cmds.append(String.format(FileService.GIT_COMMIT, username) + newLine);
//					cmds.append(FileService.GIT_PUSH + "origin master "+ newLine);			
//					exitVal = executeSCMCommand(cmds.toString(), true);
//					String projectId = String.valueOf(project.getId());
//					GitSupport jgit = new GitSupport(project.getScmUsername(), project.getScmPassword());
//					if (jgit.checkExistResponsive(projectId, resourcePath, username.replace("@", "."))) {
//						jgit.gitClone(project.getScmUrl() + "git/" + project.getScmUsername() + "/" + projectId + ".git", projectId, resourcePath, username.replace("@", "."));
//			    	} else {
//			    		jgit.openGitDirectory(projectId, resourcePath, username.replace("@", "."));
//			    	}
//					jgit.gitPull();
//					jgit.gitAdd();
//					jgit.gitCommit("User commit " + username);
//					jgit.gitPush();
//					
//				}

				LOGGER.info("End Service FileService function syncProject");
				
			}
		});
		thread.start();
		return 0;
	}

	private int executeSCMCommand(String username, String cli, File workingDir) {
		LOGGER.info("Begin Service FileService function executeSCMCommand");
		LOGGER.info("Service FileService function executeSCMCommand PARAMS username: " + username);
		LOGGER.info("Service FileService function executeSCMCommand PARAMS cli: " + cli);
		int exitVal=0;
		if(workingDir == null ) {
			Path path = Paths.get(resourcePath, username.replace("@", "."));
			workingDir = path.toFile();
			if (!workingDir.exists()) {
				workingDir.mkdirs();
			}
		}
		Process process;
		try {
			process = Runtime.getRuntime().exec(cli, null, workingDir);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
			}
	
			//exitVal = process.waitFor();

		} catch (IOException e) {
			LOGGER.error("End Service FileService function loadTestDataFileAsResource ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function executeSCMCommand");
		return exitVal;
	}


	private int executeSCMCommand(String cmds, Boolean ...sync ) {
		
		LOGGER.info("Begin Service FileService function executeSCMCommand");
		LOGGER.info("Service FileService function executeSCMCommand PARAMS cmds: " + cmds);
		LOGGER.info("Service FileService function executeSCMCommand PARAMS sync: " + new Gson().toJson(sync, new TypeToken<Boolean[]>() {}.getType()));
		int exitVal=0;
		Path path = Paths.get(resourcePath, TMP_DIR_NAME);
		File workingDir = path.toFile();
		if (!workingDir.exists()) {
			workingDir.mkdir();
		}
		String filename = "tmp_" + Calendar.getInstance().getTimeInMillis() ;
		String os = System.getProperty("os.name").toLowerCase();
		String cmd ="";
		if (os.contains("win")) {
			filename += ".bat";
			cmd ="cmd /C call ";
		}else {
			filename += ".sh";
			cmd = "sh " ;
		}
				
		Path tmpfile = Paths.get(path.toString(),filename);	
		cmd += filename;
		try {
			Files.write(tmpfile, cmds.getBytes());
			Process process = Runtime.getRuntime().exec(cmd, null, path.toFile());
//			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//			String line;
//			while ((line = reader.readLine()) != null) {
//				System.out.println(line);
//			}
			//if (sync.length > 0 && sync[0]) {
//				exitVal = process.waitFor();
			//}
//			System.out.println("Return code:" + exitVal);

			if(!process.waitFor(2, TimeUnit.MINUTES)) {
				process.destroyForcibly();
				process.destroy(); // consider using destroyForcibly instead
				LOGGER.error("Service FileService function executeSCMCommand ERROR MESSAGE: File too large or occurs error");
				LOGGER.error("End Service FileService function executeSCMCommand ERROR MESSAGE: PROCESS TIMEOUT");
				
				exitVal = process.waitFor();
			}

		} catch (IOException | InterruptedException e) {
			LOGGER.error("End Service FileService function executeSCMCommand ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function executeSCMCommand");
//		tmpfile.toFile().delete();
		return exitVal;
	}
	
	/**
	 * Convert absolute file to relate path before call scm
	 * @param username
	 * @param projectId
	 * @param file
	 */
	private void scmAddNew(String username, String projectId, File file) {
		LOGGER.info("Begin Service FileService function scmAddNew");
		LOGGER.info("Service FileService function scmAddNew PARAMS username: " + username);
		LOGGER.info("Service FileService function scmAddNew PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function scmAddNew PARAMS file: " + file.getAbsolutePath());
		String filePath = file.getAbsolutePath();
		Path rootPath = Paths.get(this.resourcePath, username.replace("@", "."), projectId);
		filePath = filePath.replace(rootPath.toString() + File.separator, "");
		this.scmAddNew(username, projectId, filePath);
		LOGGER.info("End Service FileService function scmAddNew");
	}

//	
//	private void scmAddNew(String username, String projectId, String filepath ) {
//		Optional<Project> optProject = projectRepo.findById(Long.valueOf(projectId));
//		if (!optProject.isPresent()) {
//			return ;
//
//		}
//		List<String> cmds = new ArrayList<String>();
//		Project project = optProject.get();
//		if ("svn".equalsIgnoreCase(project.getScmType())) {
//			cmds.add(String.format( FileService.SVN_ADD, filepath));
//			cmds.add(FileService.SVN_COMMIT);
//		}
//		if ("git".equalsIgnoreCase(project.getScmType())) {
//			cmds.add(String.format( FileService.GIT_ADD, filepath));
//			cmds.add(FileService.GIT_COMMIT);
//			cmds.add(FileService.GIT_PUSH);
//		}
//		Path path = Paths.get(resourcePath, username.replace("@", "."),projectId);
//		File workingDir = path.toFile();
//		for (String cli : cmds) {
//			this.executeSCMCommand(username, cli, workingDir);					
//		}
//		
//	}
	
	private void scmAddNew(String username, String projectId, String filepath ) {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				LOGGER.info("Begin Service FileService function scmAddNew");
				LOGGER.info("Service FileService function scmAddNew PARAMS username: " + username);
				LOGGER.info("Service FileService function scmAddNew PARAMS projectId: " + projectId);
				LOGGER.info("Service FileService function scmAddNew PARAMS filepath: " + filepath);
				Optional<Project> optProject = projectRepo.findById(Long.valueOf(projectId));
				if (!optProject.isPresent()) {
					return ;

				}
				Project project = optProject.get();
				String newLine =  System.lineSeparator();
				StringBuffer cmd = new StringBuffer();
				Path path = Paths.get(resourcePath, username.replace("@", "."),projectId);
				if (SVN.equalsIgnoreCase(project.getScmType())) {
					cmd.append("cd " + path.toString() + newLine);
					cmd.append("START /REALTIME /W /MIN svn add . --force "  + newLine);
					cmd.append(String.format(FileService.SVN_COMMIT, username) + newLine);
					
					executeSCMCommand( cmd.toString());	
				}
//				if (GIT.equalsIgnoreCase(project.getScmType())) {
//					cmd.append("cd " + path.toString() + newLine);
//					cmd.append("git config --global user.email \"bottest@bottest.io\"" + newLine);
//					cmd.append("git config --global user.name \"bottest\"" + newLine);
//					cmd.append(FileService.GIT_ADD + newLine);
//					cmd.append(String.format(FileService.GIT_COMMIT, username) + newLine);
//					String urlScm = project.getScmUrl();
//		    	    String domainScm = urlScm.substring(0, 5);
//		    	    if (!"https".equalsIgnoreCase(domainScm)) {
//		    	    	domainScm = "http";
//		    	    }
//		    	    urlScm = urlScm.replace(domainScm + "://", domainScm + "://" + project.getScmUsername() + ":" + project.getScmPassword() + "@");
//					cmd.append(FileService.GIT_PUSH + urlScm + "git/" + project.getScmUsername() + "/" + project.getId() + ".git" + " master " + newLine);
//					GitSupport jgit = new GitSupport(project.getScmUsername(), project.getScmPassword());
//			    	jgit.openGitDirectory(projectId, resourcePath, username.replace("@", "."));
//					jgit.gitPull();
//					jgit.gitAdd();
//					jgit.gitCommit("User commit " + username);
//					jgit.gitPush();
//				}
				// remove because used JGIT
//				this.executeSCMCommand( cmd.toString());	
				LOGGER.info("End Service FileService function scmAddNew");
			}
		});
		thread.start();
	}
	
	private void scmDelete(String username, String projectId, String filepath ) {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				LOGGER.info("Begin Service FileService function scmDelete");
				LOGGER.info("Service FileService function scmDelete PARAMS username: " + username);
				LOGGER.info("Service FileService function scmDelete PARAMS projectId: " + projectId);
				LOGGER.info("Service FileService function scmDelete PARAMS filepath: " + filepath);
				Optional<Project> optProject = projectRepo.findById(Long.valueOf(projectId));
				if (!optProject.isPresent()) {
					return ;

				}
				Project project = optProject.get();
				String newLine =  System.lineSeparator();
				StringBuffer cmd = new StringBuffer();
				Path path = Paths.get(resourcePath, username.replace("@", "."),projectId);
				if (SVN.equalsIgnoreCase(project.getScmType())) {
					cmd.append("cd " + path.toString() + newLine);
					cmd.append(String.format( FileService.SVN_DELETE, filepath)  + newLine);
					cmd.append(String.format(FileService.SVN_COMMIT, username) + newLine);
					executeSCMCommand( cmd.toString());		
				}
//				if (GIT.equalsIgnoreCase(project.getScmType())) {
//					cmd.append("cd " + path.toString() + newLine);
//					cmd.append(String.format( FileService.GIT_DELETE, filepath) + newLine);
//					cmd.append(String.format(FileService.GIT_COMMIT, username) + newLine);
//					cmd.append(FileService.GIT_PUSH + "origin master "+ newLine);
//					GitSupport jgit = new GitSupport(project.getScmUsername(), project.getScmPassword());
//					jgit.openGitDirectory(projectId, resourcePath, username.replace("@", "."));
//					jgit.gitRemoveFromIndexOrTree(filepath);
//					jgit.gitCommit("remove path " + filepath + " User: " + username);
//					jgit.gitPush();
//				}
				
				
				LOGGER.info("End Service FileService function scmDelete");
				
			}
		});
		
		thread.start();
		
	}
	
	/**
	 * Convert absolute file to relate path before call scm
	 * @param username
	 * @param projectId
	 * @param file
	 */
	private void scmDelete(String username, String projectId, File file) {
		LOGGER.info("Begin Service FileService function scmDelete");
		LOGGER.info("Service FileService function scmDelete PARAMS username: " + username);
		LOGGER.info("Service FileService function scmDelete PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function scmDelete PARAMS file: " + file.getAbsolutePath());
		String filePath = file.getAbsolutePath();
		Path rootPath = Paths.get(this.resourcePath, username.replace("@", "."), projectId);
		filePath = filePath.replace(rootPath.toString()+ File.separator, "");
		this.scmDelete(username, projectId, filePath);
		LOGGER.info("End Service FileService function scmDelete");
	}
//	
//	private void scmDelete(String username, String projectId, String filepath ) {
//		Optional<Project> optProject = projectRepo.findById(Long.valueOf(projectId));
//		if (!optProject.isPresent()) {
//			return ;
//
//		}
//		List<String> cmds = new ArrayList<String>();
//		Project project = optProject.get();
//		if ("svn".equalsIgnoreCase(project.getScmType())) {
//			cmds.add(String.format( FileService.SVN_DELETE, filepath));
//			cmds.add(FileService.SVN_COMMIT);
//		}
//		if ("git".equalsIgnoreCase(project.getScmType())) {
//			cmds.add(String.format( FileService.GIT_DELETE, filepath));
//			cmds.add(FileService.GIT_COMMIT);
//			cmds.add(FileService.GIT_PUSH);
//			
//		}
//		Path path = Paths.get(resourcePath, username.replace("@", "."),projectId);
//		File workingDir = path.toFile();
//		for (String cli : cmds) {
//			this.executeSCMCommand(username, cli, workingDir);					
//		}		
//	}
//	
//

	public String uploadTestData(MultipartFile file, String username, String projectId, String testsuiteId, String description) {
		LOGGER.info("Begin Service FileService function uploadTestData");
		LOGGER.info("Service FileService function uploadTestData PARAMS file: " + file.getOriginalFilename());
		LOGGER.info("Service FileService function uploadTestData PARAMS username: " + username);
		LOGGER.info("Service FileService function uploadTestData PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function uploadTestData PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function uploadTestData PARAMS description: " + description);
		String fileName = null;
		try {
			Path path = Paths.get(this.resourcePath, username.replace("@", "."),projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME, file.getOriginalFilename());
			OutputStream out = null;
			UploadTestData uploadTData = new UploadTestData();
			UploadTestData ul = uploadTestdataRepo.findByFileName(file.getOriginalFilename(), Long.parseLong(projectId), testsuiteId);
			
			if(ul != null) {
				fileName = "File was exist!";
				return fileName;
			}
			uploadTData.setFileName(file.getOriginalFilename());
			uploadTData.setDescription(description);
			uploadTData.setProjectId(Long.parseLong(projectId));
			uploadTData.setTestsuiteName(testsuiteId);
			uploadTData.setCreateDate(new Date());
			uploadTData.setType("file");
			uploadTestdataRepo.save(uploadTData);
			try {
				out = new BufferedOutputStream(new FileOutputStream(path.toFile()));
				out.write(file.getBytes());
				this.scmAddNew(username, projectId, path.toFile());
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {
			LOGGER.error("End Service FileService function uploadTestData ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		LOGGER.info("End Service FileService function uploadTestData");
		return fileName;
	}
	
	public void uploadImageToResultManual(MultipartFile file, String username, String projectId, String testsuiteId, String nameFile, String contentType) {
		LOGGER.info("Begin Service FileService function uploadImageToResultManual");
		LOGGER.info("Service FileService function uploadImageToResultManual PARAMS file: " + file.getOriginalFilename());
		LOGGER.info("Service FileService function uploadImageToResultManual PARAMS username: " + username);
		LOGGER.info("Service FileService function uploadImageToResultManual PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function uploadImageToResultManual PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function uploadImageToResultManual PARAMS nameFile: " + nameFile);
		LOGGER.info("Service FileService function uploadImageToResultManual PARAMS contentType: " + contentType);

		try {
			Path path = Paths.get(this.resourcePath, username.replace("@", "."),projectId, testsuiteId, FileService.MANUAL_DIR_NAME, FileService.MANUALS_FILES_DIR_NAME, nameFile+"."+contentType);
			
			Path pathDirectory = Paths.get(this.resourcePath, username.replace("@", "."),projectId, testsuiteId, FileService.MANUAL_DIR_NAME, FileService.MANUALS_FILES_DIR_NAME);
			File directory = new File(String.valueOf(pathDirectory));

			if(!directory.exists()){
			    directory.mkdir();
			}
			OutputStream out = null;
			
			try {
				out = new BufferedOutputStream(new FileOutputStream(path.toFile()));
				out.write(file.getBytes());
				this.scmAddNew(username, projectId, path.toFile());
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {
			LOGGER.error("End Service FileService function uploadTestData ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function uploadImageToResultManual");

	}

	public void deleteTestData(String username, String projectId, String testsuiteId, String filename) {
		
		LOGGER.info("Begin Service FileService function deleteTestData");
		LOGGER.info("Service FileService function deleteTestData PARAMS username: " + username);
		LOGGER.info("Service FileService function deleteTestData PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function deleteTestData PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function deleteTestData PARAMS filename: " + filename);
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME, filename);
		File file = new File(path.toString());
		if(file.getName().endsWith(".json")) {
		} else {
			UploadTestData ul = uploadTestdataRepo.findByFileName(file.getName(), Long.parseLong(projectId), testsuiteId);
			if(ul != null) {
				uploadTestdataRepo.deleteById(ul.getId());
			}
			
		}
		file.delete();
		scmDelete(username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteTestData");
		
	}
	
	public void copyProjectSample(String samplePath, String newPath, String username, String projectId) {
		 File sourceFolder = new File(samplePath);
        
	        //Target directory where files should be copied
		 File destinationFolder = new File(newPath);
	 
	        //Call Copy function
	        try {
				this.copyFolder(sourceFolder, destinationFolder);
				Path filePathTestSuite = Paths.get(this.resourcePath, username.replace("@", "."), projectId, "LoginBottest");
				
				scmAddNew( username, projectId,filePathTestSuite.toFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void copyFolder(File sourceFolder, File destinationFolder) throws IOException
   {
       //Check if sourceFolder is a directory or file
       //If sourceFolder is file; then copy the file directly to new location
       if (sourceFolder.isDirectory()) 
       {
           //Verify if destinationFolder is already present; If not then create it
           if (!destinationFolder.exists()) 
           {
               destinationFolder.mkdir();
               System.out.println("Directory created :: " + destinationFolder);
           }
            
           //Get all files from source directory
           String files[] = sourceFolder.list();
            
           //Iterate over all files and copy them to destinationFolder one by one
           for (String file : files) 
           {
        	   if(!file.equals("dataSample.sql")) {
	               File srcFile = new File(sourceFolder, file);
	               File destFile = new File(destinationFolder, file);
                
	               //Recursive function call
	               copyFolder(srcFile, destFile);
        	   }
           }
       }
       else
       {
           //Copy the file content from one place to another 
           Files.copy(sourceFolder.toPath(), destinationFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
           System.out.println("File copied :: " + destinationFolder);
       }
   }
	
	public void readFileSqlDataSample(String projectId) {
		LOGGER.info("Start readFileSqlDataSample");
//		File file = new File(getClass().getClassLoader().getResource("dataSample.txt").getFile());
		
//		String fileStr = file.toString();
//		String fileDecode;
		try {
//			fileDecode = URLDecoder.decode(fileStr, "UTF-8");
			File fileFinal = new File(pathDataSample);
			
			Scanner sc = new Scanner(fileFinal); 
			int count = 0;
		    while (sc.hasNextLine()) {
		    	count += 1;
		    	String sqlLine = sc.nextLine().replace("1385", projectId);
		    	LOGGER.info("Execute query: " + sqlLine);
		    	jdbcTemplate.execute(sqlLine);
		    }
		    
		    sc.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LOGGER.info("End readFileSqlDataSample");
		
	}
	
	public void deleteTestData(String username, String projectId, String testsuiteId, String filename, Long idTestData) {
		
		LOGGER.info("Begin Service FileService function deleteTestData");
		LOGGER.info("Service FileService function deleteTestData PARAMS username: " + username);
		LOGGER.info("Service FileService function deleteTestData PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function deleteTestData PARAMS testsuiteId: " + testsuiteId);
		LOGGER.info("Service FileService function deleteTestData PARAMS filename: " + filename);
		Path path = Paths.get(this.resourcePath,username.replace("@", "."), projectId, testsuiteId, FileService.TESTDATAS_DIR_NAME, filename);
		File file = new File(path.toString());
		
		uploadTestdataRepo.deleteById(idTestData);
			
		file.delete();
		scmDelete(username, projectId, file);
		
		LOGGER.info("End Service FileService function deleteTestData");
		
	}
	
	public void uploadImageToIssue(MultipartFile file, String username, String projectId, String nameFile, String contentType) {
		LOGGER.info("Begin Service FileService function uploadImageToIssue");
		LOGGER.info("Service FileService function uploadImageToIssue PARAMS file: " + file.getOriginalFilename());
		LOGGER.info("Service FileService function uploadImageToIssue PARAMS username: " + username);
		LOGGER.info("Service FileService function uploadImageToIssue PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function uploadImageToIssue PARAMS nameFile: " + nameFile);
		LOGGER.info("Service FileService function uploadImageToIssue PARAMS contentType: " + contentType);

		try {
			Path path = Paths.get(this.resourcePath,projectId, nameFile+"."+contentType);
			
			Path pathDirectory = Paths.get(this.resourcePath,projectId);
			File directory = new File(String.valueOf(pathDirectory));

			if(!directory.exists()){
			    directory.mkdir();
			}
			OutputStream out = null;
			
			try {
				out = new BufferedOutputStream(new FileOutputStream(path.toFile()));
				out.write(file.getBytes());
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {
			LOGGER.error("End Service FileService function uploadImageToIssue ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function uploadImageToIssue");

	}
	
	/**
	 * @param projectId
	 * @param testsuitId
	 * @param fileName
	 * @throws IOException
	 */
	public void deleteFileIssue(String username,String projectId, String fileName)
			throws IOException {
		
		LOGGER.info("Begin Service FileService function deleteFileIssue");
		LOGGER.info("Service FileService function deleteFileIssue PARAMS username: " + username);
		LOGGER.info("Service FileService function deleteFileIssue PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function deleteFileIssue PARAMS fileName: " + fileName);
		Path path = Paths.get(this.resourcePath, projectId,fileName);
		File file = new File(path.toString());
		file.delete();

		LOGGER.info("End Service FileService function deleteFileIssue");
	}
	
	/**
	 * @param projectId
	 * @param testsuiteId
	 * @param fileName
	 * @return
	 */
	public Resource loadFileIssue(String username, String projectId, String fileName) {
		LOGGER.info("Begin Service FileService function loadFileIssue");
		LOGGER.info("Service FileService function loadFileIssue PARAMS username: " + username);
		LOGGER.info("Service FileService function loadFileIssue PARAMS projectId: " + projectId);
		LOGGER.info("Service FileService function loadFileIssue PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath ,projectId, fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				
				LOGGER.info("End Service FileService function loadFileIssue");
				return resource;
			} else {
				// throw new MyFileNotFoundException("File not found " + fileName);
				LOGGER.error("Service FileService function loadFileIssue ERROR MESSAGE: File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			// throw new MyFileNotFoundException("File not found " + fileName, ex);
			LOGGER.error("Service FileService function loadFileIssue ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
		}
		
		LOGGER.info("End Service FileService function loadFileIssue");
		return null;
	}
	
	public void uploadImage(MultipartFile file, String nameFile, String contentType) {

		try {
			Path path = Paths.get(this.resourcePath,"FileStore", nameFile+"."+contentType);
			
			Path pathDirectory = Paths.get(this.resourcePath,"FileStore");
			File directory = new File(String.valueOf(pathDirectory));

			if(!directory.exists()){
			    directory.mkdir();
			}
			OutputStream out = null;
			
			try {
				out = new BufferedOutputStream(new FileOutputStream(path.toFile()));
				out.write(file.getBytes());
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {
			LOGGER.error("End Service FileService function uploadImageToIssue ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function uploadImageToIssue");

	}
	
	public void deleteFileStore(List<UploadFileDTO> listImages) {
		for(UploadFileDTO image : listImages) {
			String contentType = FilenameUtils.getExtension(image.getName());
			Path path = Paths.get(this.resourcePath,"FileStore", image.getId() + "." + contentType);
			File file = path.toFile();
			file.delete();
		}
	}

	public void moveFileStoreToProject(Long id, String image) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			List<UploadFileDTO> lstFile = mapper.readValue(image, new TypeReference<List<UploadFileDTO>>() {});
			for(UploadFileDTO file: lstFile) {
				String contentType = FilenameUtils.getExtension(file.getName());
				String nameFile = file.getId();
				Path path = Paths.get(this.resourcePath,"FileStore", nameFile + "." + contentType);
				if(Files.exists(path)) {
					Path newPath = Paths.get(this.resourcePath,Long.toString(id), nameFile + "." + contentType);
					Path temp = Files.move(path, newPath); 
				}
				
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void copyFilePublicToPrivate(Project projectOld, Project newProject) {
		
		File source = new File(this.resourcePath + "\\" + Long.toString(projectOld.getId()));
		File dest = new File(this.resourcePath + "\\" + Long.toString(newProject.getId()));
		try {
			if(source.exists()) {
				FileUtils.copyDirectory(source, dest);
			}
		    
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}

	public void uploadImgProduct(MultipartFile file, String nameFile, String contentType) {
		LOGGER.info("Begin Service FileService function uploadImgProduct");
		LOGGER.info("Service FileService function uploadImgProduct PARAMS file: " + file.getOriginalFilename());
		LOGGER.info("Service FileService function uploadImgProduct PARAMS nameFile: " + nameFile);
		LOGGER.info("Service FileService function uploadImgProduct PARAMS contentType: " + contentType);
		String fileNew = null;
		try {
			Path path = Paths.get(this.resourcePath, FileService.PRODUCT_DIR_NAME,  nameFile+"."+contentType);
			
			Path pathDirectory = Paths.get(this.resourcePath, FileService.PRODUCT_DIR_NAME );
			File directory = new File(String.valueOf(pathDirectory));
			Path pathRoot =  Paths.get(this.resourcePath);
			if(! new File(String.valueOf(pathRoot)).exists()) {
				new File(String.valueOf(pathRoot)).mkdir();
			}
			if(!directory.exists()){
			    directory.mkdir();
			}
			OutputStream out = null;
			
			try {
				out = new BufferedOutputStream(new FileOutputStream(path.toFile()));
				out.write(file.getBytes());
				fileNew = path.toFile().toString();
				//this.scmAddNewAvatar(username, path.toFile());
			} finally {
				if (out != null)
					out.close();
			}
		} catch (IOException e) {
			LOGGER.error("End Service FileService function uploadImgProduct ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service FileService function uploadImgProduct");
		
	}

	public Resource loadImg(String fileName) {
		LOGGER.info("Begin Service FileService function loadImg");
		LOGGER.info("Service FileService function loadImg PARAMS fileName: " + fileName);
		try {
			Path path = Paths.get(this.resourcePath,FileService.PRODUCT_DIR_NAME,  fileName);
			Resource resource = new UrlResource(path.toUri());
			if (resource.exists()) {
				
				LOGGER.info("End Service FileService function loadImg");
				return resource;
			} else {
				// throw new MyFileNotFoundException("File not found " + fileName);
				LOGGER.error("Service FileService function loadImg ERROR MESSAGE: File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			// throw new MyFileNotFoundException("File not found " + fileName, ex);
			LOGGER.error("Service FileService function loadImg ERROR MESSAGE: " + ExceptionUtils.getStackTrace(ex));
		}
		
		LOGGER.info("End Service FileService function loadImg");
		return null;
	}

}

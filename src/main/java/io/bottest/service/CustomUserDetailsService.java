package io.bottest.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.BottestUser;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.entity.UserProject;
import io.bottest.jpa.respository.BottestUserRepository;
import io.bottest.jpa.respository.UserRepository;

 
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	private final Logger LOGGER = LogManager.getLogger(CustomUserDetailsService.class);
	
	@Autowired
	UserRepository users;
	
	@Autowired
	ProjectService projectService;

	
	@Override
	public User loadUserByUsername(String loginUserName) {	
		return users.getUserByUsername(loginUserName);

	}

	public boolean checkProjectPermission(Authentication auth, String projectId) {
		
		LOGGER.info("Begin Service customUserDetailsService function checkProjectPermission");
		if (auth == null) {
			LOGGER.info("Service: customUserDetailsService function checkProjectPermission: Authentication is null");
			LOGGER.info("End Service: customUserDetailsService function checkProjectPermission: false");
			return false;
		}
		if (!(auth.getPrincipal() instanceof UserDetails) ) {
			LOGGER.info("Service: customUserDetailsService function checkProjectPermission: Principal is not UserDetails class");
			LOGGER.info("End Service: customUserDetailsService function checkProjectPermission: false");
			return false;
		}
		UserDetails user = (UserDetails) auth.getPrincipal();
		//System.out.println("request user: "+ user.getUsername() + "; projectId=" + projectId );
		List<UserProject> userProjects = projectService.getProjectByUserName(user.getUsername());
		
		if((projectId !=null) && (projectId.length() > 0)) {
			for (UserProject userProject : userProjects) {
				if (projectId.equalsIgnoreCase(userProject.getProjectId().toString())) {
					LOGGER.info("End Service: customUserDetailsService function checkProjectPermission: true");
					return true;
				}
			}
			
			LOGGER.info("Service: customUserDetailsService function checkProjectPermission: Do not exist projectId");
			LOGGER.info("End Service: customUserDetailsService function checkProjectPermission: false");
			return false;
		}else {
			LOGGER.info("Service: customUserDetailsService function checkProjectPermission: Project is null");
			LOGGER.info("End Service: customUserDetailsService function checkProjectPermission: true");
			return true;
		}
	}


public boolean checkAuthen(Authentication auth) {
		
		LOGGER.info("Begin Service customUserDetailsService function checkAuthen");
		if (auth == null) {
			LOGGER.info("Service: customUserDetailsService function checkAuthen: Authentication is null");
			LOGGER.info("End Service: customUserDetailsService function checkAuthen: false");
			return false;
		}else {
			if (!(auth.getPrincipal() instanceof UserDetails) ) {
				LOGGER.info("Service: customUserDetailsService function checkAuthen: Principal is not UserDetails class");
				LOGGER.info("End Service: customUserDetailsService function checkAuthen: false");
				return false;
			}else {
				return true;
		}
		
	}
}

	

}

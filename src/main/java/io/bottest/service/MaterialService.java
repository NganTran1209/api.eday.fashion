package io.bottest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.dto.MaterialDTO;
import io.bottest.jpa.respository.MaterialRepository;

@Service
@Transactional
public class MaterialService {
	
	private final Logger LOGGER = LogManager.getLogger(WorkerService.class);
	
	@Autowired MaterialRepository materialRepository;

	public List<MaterialDTO> getAllMaterialById(Long userId) {
		LOGGER.info("Begin Service MaterialService Function getAllMaterialById");
		LOGGER.info("Service MaterialService Function getAllMaterialById PARAM userId: " + userId);
		
		List<MaterialDTO> listMaterial = new ArrayList<MaterialDTO>();
		List<Map<String, Object>> list = materialRepository.findAllMaterialById(userId);

		if(list != null && ! list.isEmpty()) {
			for(int i = 0; i < list.size(); i ++) {
				MaterialDTO materialDTO = new MaterialDTO();
				materialDTO.setId(list.get(i).get("id") != null ? String.valueOf(list.get(i).get("id")) : "");
				materialDTO.setImage(list.get(i).get("image") != null ? String.valueOf(list.get(i).get("image")) : "");
				materialDTO.setTitle(list.get(i).get("title") != null ? String.valueOf(list.get(i).get("title")) : "");
				materialDTO.setSupplier(list.get(i).get("supplier") != null ? String.valueOf(list.get(i).get("supplier")) : "");
				materialDTO.setPrice(list.get(i).get("price") != null ? String.valueOf(list.get(i).get("price")) : "");
				materialDTO.setAmount(list.get(i).get("amount") != null ? String.valueOf(list.get(i).get("amount")) : "");
				materialDTO.setCategory(list.get(i).get("name") != null ? String.valueOf(list.get(i).get("name")) : "");
				materialDTO.setInputDate(list.get(i).get("input_date") != null ? String.valueOf(list.get(i).get("input_date")) : "");
				listMaterial.add(materialDTO);
			}
			
			LOGGER.info("End Service MaterialService Function getAllMaterialById");
			return listMaterial;
		}
		
		return null;
	}

}

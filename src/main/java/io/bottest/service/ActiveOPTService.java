package io.bottest.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.ActiveOTP;
import io.bottest.jpa.respository.ActiveOPTRepository;
import org.apache.commons.lang.StringUtils;

@Service
@Transactional
public class ActiveOPTService {

	private final Logger LOGGER = LogManager.getLogger(UserService.class);
	
	@Autowired
	private ActiveOPTRepository activeOPTRepository;
 
	//function tao ma code generateOTP khi user forgot password
	public ActiveOTP generateOTP(String email) {
		LOGGER.info("Begin Service ActiveOPTService Function generateOTP");
		LOGGER.info("Service ActiveOPTService Function generateOTP PARAM email: " + email);
		
		ActiveOTP activeOTP = new ActiveOTP();
		activeOTP.setActiveOtpId(null);
		activeOTP.setIsActive(0);
		activeOTP.setBeginTime(new Date());
		activeOTP.setActiveEmail(email);
		
		// plus 30 min
		Calendar calendar = Calendar.getInstance();
		long time = calendar.getTimeInMillis();
		activeOTP.setEndTime(new Date(time + 1800000));
		
		activeOTP.setActiveOtpCode(generateOTPCode());
		activeOTP = activeOPTRepository.save(activeOTP);
		
		LOGGER.info("End Service ActiveOPTService Function generateOTP");
		return activeOTP;
	}
	
	//thuc hien random OTPCode
	public String generateOTPCode() {
		LOGGER.info("Begin Service ActiveOPTService Function generateOTPCode");
		
		String values = "123456789";
		Random random = new Random();
		char[] OTP = new char[6];
		for(int i = 0; i < 6; i++) {
			OTP[i] = values.charAt(random.nextInt(values.length()));
		}
		
		LOGGER.info("End Service ActiveOPTService Function generateOTPCode");
		return String.valueOf(OTP);
	}
	
	public ActiveOTP checkOTPCode(String email, String otpCode, Date timeNow) {
		LOGGER.info("Begin Service ActiveOPTService Function checkOTPCode");
		
		List<ActiveOTP> listOTPCode = activeOPTRepository.findAllByActiveEmailAndBeginTimeLessThanEqualAndEndTimeGreaterThanEqualOrderByEndTimeDesc(email, timeNow, timeNow);
		if(listOTPCode != null && listOTPCode.size() > 0) {
			if(otpCode != null && !StringUtils.EMPTY.equals(otpCode)) {
				if(otpCode.equals(listOTPCode.get(0).getActiveOtpCode())) {
					return listOTPCode.get(0);
				}
			}
		}
		
		LOGGER.info("End Service ActiveOPTService Function generateOTPCode");
		return null;
	}

}

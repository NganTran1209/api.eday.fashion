package io.bottest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.dto.QuyTrinhDTO;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.entity.Task;
import io.bottest.jpa.respository.ProjectRepository;
import io.bottest.jpa.respository.TaskRepository;

@Service
@Transactional
public class QuyTrinhService {
	
	@Autowired 
	TaskRepository taskRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	public List<QuyTrinhDTO> getAllPublicProject(){
		List<Project> projectList = projectRepository.getAllUserProjectByFlagAndByType();
		List<QuyTrinhDTO> quyTrinhDTOList = new ArrayList<>();
		for(Project project : projectList) {
			QuyTrinhDTO quyTrinhDTO = new QuyTrinhDTO();
			quyTrinhDTO.setProject(project);
			List<Task> taskList = taskRepository.findAllByProjectId(project.getId());
			quyTrinhDTO.setTaskList(taskList);
			quyTrinhDTOList.add(quyTrinhDTO);
		}
		
		return quyTrinhDTOList;
	}

}

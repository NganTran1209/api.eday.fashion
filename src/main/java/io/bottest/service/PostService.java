package io.bottest.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.dto.PostDTO;
import io.bottest.jpa.entity.Post;
import io.bottest.jpa.respository.PostRepository;

@Service
@Transactional
public class PostService {

	private final Logger LOGGER = LogManager.getLogger(WorkerService.class);
	
	@Autowired
	private PostRepository postRepository;
	
	public List<PostDTO> getAllPostByUserId(Long userId) {
		LOGGER.info("Begin Service PostService Function getAllPostByUserId");
		LOGGER.info("Service PostService Function getAllPostByUserId PARAM userId: " + userId);
		
		List<Post> list = postRepository.findAllPostOfUserByUserId(userId);
		List<PostDTO> listPostDTO = new ArrayList<PostDTO>();
		
		if(list != null && !list.isEmpty()) {
			for(int i = 0; i < list.size(); i++) {
				PostDTO postDTO = new PostDTO();
				postDTO.setId(list.get(i).getId() != null ? String.valueOf(list.get(i).getId()) : "");
				postDTO.setDayPost(list.get(i).getDayPost() != null ? String.valueOf(list.get(i).getDayPost()) : "");
				postDTO.setTitle(list.get(i).getTitle() != null ? list.get(i).getTitle() : "");
				postDTO.setImage(list.get(i).getImage() != null ? list.get(i).getImage() : "");
				postDTO.setLike(list.get(i).getEmotion() != null ? String.valueOf(list.get(i).getEmotion()) : "");
				postDTO.setComment(list.get(i).getComment() != null ? list.get(i).getComment() : "");
				listPostDTO.add(postDTO);
			}
			
			LOGGER.info("End Service PostService Function getAllPostByUserId");
			return listPostDTO;
		}
		
		return null;
	}

}

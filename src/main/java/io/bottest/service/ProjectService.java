package io.bottest.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.bouncycastle.crypto.tls.AlertDescription;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.config.ExportExcel;
import io.bottest.dto.ExportTestcaseDTO;
import io.bottest.dto.FilterConditionData;
import io.bottest.dto.FilterData;
import io.bottest.dto.FilterTaskDTO;
import io.bottest.dto.ProjectMilestoneDTO;
import io.bottest.dto.ResultInfoDTO;
import io.bottest.dto.ResultTestcaseStatusDTO;
import io.bottest.dto.TestsuiteTestingProgress;
import io.bottest.dto.UploadFileDTO;
import io.bottest.jpa.entity.ExecutingJob;
import io.bottest.jpa.entity.InvitedToProject;
import io.bottest.jpa.entity.Project;
import io.bottest.jpa.entity.ProjectComponent;
import io.bottest.jpa.entity.ProjectMileStoneIdentity;
import io.bottest.jpa.entity.ProjectMilestone;
import io.bottest.jpa.entity.ProjectMilestoneTestcase;
import io.bottest.jpa.entity.ProjectProgress;
import io.bottest.jpa.entity.ProjectProgressIdentity;
import io.bottest.jpa.entity.ProjectSummary;
import io.bottest.jpa.entity.ProjectSummaryIdentity;
import io.bottest.jpa.entity.ResultInfo;
import io.bottest.jpa.entity.ScreenDesign;
import io.bottest.jpa.entity.Task;
import io.bottest.jpa.entity.UpdateTask;
import io.bottest.jpa.entity.UploadTestData;
import io.bottest.jpa.entity.UserProject;
import io.bottest.jpa.respository.ExecutingJobRepository;
import io.bottest.jpa.respository.InvitedReposiry;
import io.bottest.jpa.respository.ProjectComponentRepository;
import io.bottest.jpa.respository.ProjectMilestoneRepository;
import io.bottest.jpa.respository.ProjectMilestoneTestcaseRepository;
import io.bottest.jpa.respository.ProjectProgressRepository;
import io.bottest.jpa.respository.ProjectRepository;
import io.bottest.jpa.respository.ProjectSummaryRepository;
import io.bottest.jpa.respository.ResultInfoRepository;
import io.bottest.jpa.respository.ScreenDesignRepository;
import io.bottest.jpa.respository.TaskRepository;
import io.bottest.jpa.respository.UpdateTaskRepository;
import io.bottest.jpa.respository.UploadTestDataRepository;
import io.bottest.jpa.respository.UserProjectRepository;
import io.bottest.utils.FarmateConstants;
import io.bottest.utils.FunctionUtils;

@Service
@Transactional
public class ProjectService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private final Logger LOGGER = LogManager.getLogger(ProjectService.class);
	
	@Autowired
	UserProjectRepository userProjectRepo;
	
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	FileService fileService;
	
	@Autowired
	ExecutingJobRepository executingJobRepo;
	
	@Autowired
	InvitedReposiry invitedRepo;
	
	@Autowired
	ResultInfoRepository resultInfoRepo;
	
	@Autowired
	ProjectProgressRepository projectProgressRepo;
	
	@Autowired
	ProjectMilestoneRepository projectMilestoneRepo;
	
	@Autowired
	ProjectMilestoneTestcaseRepository projectMilestoneTestcaseRepo;
	
	@Autowired
	ProjectSummaryRepository projectSummaryRepo;
	
	@Autowired
	ScreenDesignRepository screenDesignRepo;
	
	@Autowired
	UploadTestDataRepository uploadTestDataRepo;
	
	@Autowired
	TaskRepository taskRepo;
	
	@Autowired
	UpdateTaskRepository updateTaskRepo;
	
	@Autowired
	ProjectComponentRepository projectComponentRepo;

	public List<ProjectProgress> getAllTestcaseManual(Long projectId, String testsuite) {
		LOGGER.info("Begin Service ProjectService Function getAllTestcaseManual");
		LOGGER.info("Service ProjectService Function getAllTestcaseManual PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function getAllTestcaseManual PARAMS testsuite: " + testsuite);
		
		LOGGER.info("End Service ProjectService Function getAllTestcaseManual");
		return projectProgressRepo.findAllTestcaseManual(projectId, testsuite);
	}
	
	public List<ResultInfo> getStatusTestcase(int projectId, String testsuite, String testcaseNames) {
		LOGGER.info("Begin Service ProjectService Function getStatusTestcase");
		LOGGER.info("Service ProjectService Function getStatusTestcase PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function getStatusTestcase PARAMS testsuite: " + testsuite);
		LOGGER.info("Service ProjectService Function getStatusTestcase PARAMS testcaseNames: " + testcaseNames);
		List<ResultInfo> lsRes = new ArrayList<ResultInfo>();
		Gson gson = new Gson();
		Object[] object = gson.fromJson(testcaseNames, Object[].class);
		ObjectMapper oMapper = new ObjectMapper();
		for(int i = 0; i < object.length; i++) {
			Map<String, Object> map = oMapper.convertValue(object[i], Map.class);
			String testcaseName = (String) map.get("Testcase");
			ResultInfo rsInfo = new ResultInfo();
			rsInfo = resultInfoRepo.findByProjectIdAndTestSuiteAndTestCaseName(projectId, testsuite, testcaseName);
			lsRes.add(rsInfo);
		}
		
		LOGGER.info("End Service ProjectService Function getStatusTestcase");
		return lsRes;
	}
	
	public int getTestcaseTested(int projectId, String testsuite ) {
		LOGGER.info("Begin Service ProjectService Function getTestcaseTested");
		LOGGER.info("Service ProjectService Function getTestcaseTested PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function getTestcaseTested PARAMS testsuite: " + testsuite);
		
		LOGGER.info("End Service ProjectService Function getTestcaseTested");
		return resultInfoRepo.findTestcaseTested(projectId, testsuite);
	}
	
	public int getTestcasePassed(int projectId, String testsuite) {
		LOGGER.info("Begin Service ProjectService Function getTestcasePassed");
		LOGGER.info("Service ProjectService Function getTestcasePassed PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function getTestcasePassed PARAMS testsuite: " + testsuite);
		
		LOGGER.info("End Service ProjectService Function getTestcasePassed");
		return resultInfoRepo.findTestcasePassed(projectId, testsuite);
	}
	public List<UserProject> getProjectByUserName(String username) {

		return removeSameProjectId(userProjectRepo.findByUsername(username));
	}
	
	public int countResults(int projectId, String testsuite) {
		LOGGER.info("Begin Service ProjectService Function countResults");
		LOGGER.info("Service ProjectService Function countResults PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function countResults PARAMS testsuite: " + testsuite);
		
		LOGGER.info("End Service ProjectService Function countResults");
		return resultInfoRepo.countResults(projectId, testsuite);
	}
	
	public int countSchedules(int projectId, String testsuite) {
		LOGGER.info("Begin Service ProjectService Function countSchedules");
		LOGGER.info("Service ProjectService Function countSchedules PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function countSchedules PARAMS testsuite: " + testsuite);
		
		LOGGER.info("End Service ProjectService Function countSchedules");
		return executingJobRepo.countSchedules(projectId, testsuite);
	}
	
	public int countAllTcManual(Long projectId, String testsuite, String type) {
        LOGGER.info("Begin Service ProjectService Function countAllTcManual");
        LOGGER.info("Service ProjectService Function countAllTcManual PARAMS projectId: " + projectId);
		LOGGER.info("Service ProjectService Function countAllTcManual PARAMS testsuite: " + testsuite);
		LOGGER.info("Service ProjectService Function countAllTcManual PARAMS type: " + type);
		
		LOGGER.info("End Service ProjectService Function countAllTcManual");
		return projectProgressRepo.countTcManual(projectId, testsuite, type);
	}

	public List<Map<String, TestsuiteTestingProgress>> getProjectslist(String username) {
		
		List<Map<String, TestsuiteTestingProgress>> lisTDTO = new ArrayList<Map<String,TestsuiteTestingProgress>>();
		List<UserProject> userprojects =  userProjectRepo.findByUsername(username);
		List<Long> ids = new ArrayList<>();	
		//TODO:	
		for(int i = 0; i < userprojects.size(); i++) {
			
			ids.add((long) userprojects.get(i).getProjectId());
			Project project = projectRepo.findById(userprojects.get(i).getProjectId()).get();
			if(project.getDeletedFlag() == 0) {
				Map<String, TestsuiteTestingProgress> listTest = this.getTestsuiteRunStatus(username, userprojects.get(i).getProjectId().toString(), new Date(), project);
				lisTDTO.add(listTest);
			}
			
		}
		return lisTDTO;
	}
	
	public List<Map<String, Object>> getProjectsGraphic(String username) {

		List<Map<String, Object>> lstObjectStr = new ArrayList<Map<String,Object>>();
		List<UserProject> userprojects =  userProjectRepo.findByUsername(username);
		List<Long> ids = new ArrayList<>();	
		//TODO:	
		for(int i = 0; i < userprojects.size(); i++) {
			
			ids.add((long) userprojects.get(i).getProjectId());
			Project project = projectRepo.findById(userprojects.get(i).getProjectId()).get();
			if(project.getDeletedFlag() == 0) {
				List<Map<String, Object>> objectStr = projectProgressRepo.findGraphicTestcaseAutAndMan(userprojects.get(i).getProjectId());
				Map<String, Object> itemGra = new HashMap<String, Object>();
				itemGra.put("ProjectId", userprojects.get(i).getProjectId());
				for(Map<String, Object> info: objectStr) {
					itemGra.put((String) info.get("type"), (BigInteger) info.get("numberText"));
				}
				
				lstObjectStr.add(itemGra);
			}
			
		}
		
		return lstObjectStr;
	}
	
	public List<Map<String, Object>> getOneProjectGraphic(String username, Long projectId) {
		
		List<Map<String, Object>> lstObjectStr = new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> objectStr = projectProgressRepo.findGraphicTestcaseAutAndMan(projectId);
		Map<String, Object> itemGra = new HashMap<String, Object>();
		itemGra.put("ProjectId", projectId);
		for(Map<String, Object> info: objectStr) {
			itemGra.put((String) info.get("type"), (BigInteger) info.get("numberText"));
		}
		lstObjectStr.add(itemGra);
		return lstObjectStr;
	}
	
	private List<UserProject> removeSameProjectId(List<UserProject> userProjects) {
		
		LOGGER.info("Begin Service ProjectService Function removeSameProjectId");
		LOGGER.info("Service ProjectService Function removeSameProjectId PARAMS userProjects:" + new Gson().toJson(userProjects, new TypeToken<List<UserProject>>() {}.getType()));
		Set<Long> projectNames = new HashSet<Long>();
		List<UserProject> result = new ArrayList<UserProject>();
		for (UserProject userProject: userProjects) {
			if (projectNames.contains(userProject.getProjectId())) {
				//userProjects.remove(userProject);
			}else {
				result.add(userProject);
				projectNames.add(userProject.getProjectId());
			}
		}
		
		LOGGER.info("End Service ProjectService Function removeSameProjectId");
		return result;
	}

	public Project saveProject(Project project, String username)  {
		
		LOGGER.info("Begin Service ProjectService Function saveProject");
		LOGGER.info("Service ProjectService Function saveProject PARAMS project:" + new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
		LOGGER.info("Service ProjectService Function saveProject PARAMS username: " + username);
		try {
			projectRepo.save(project);
			
			List<UserProject> userProjects = userProjectRepo.findByProjectIdAndUsername(project.getId(),username);

			if (userProjects.isEmpty()) {
				UserProject userProject = new UserProject();
				userProject.setProjectId(project.getId());
				userProject.setProjectName(project.getName());
				userProject.setUsername(username);
				userProject.setProjectRole(UserProject.PROJECT_ADMIN);
				userProjectRepo.save(userProject);
				fileService.createProject(username,project);
			}else {
				for (UserProject userProject : userProjects) {
					userProject.setProjectName(project.getName());
				}
				userProjectRepo.saveAll(userProjects);
			}

			//fileService.syncProject(project, username);
		} catch ( Exception e) {
			LOGGER.error("Service ProjectService Function saveProject ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return null;
		}
		
		LOGGER.info("Begin Service ProjectService Function saveProject");
		return project;
	}
	
public void saveProjectById(Project project)  {
		projectRepo.save(project);
	}
	

	public Project getProjectById(Long projectId) {
		return projectRepo.findById(projectId).get();
	}

	public List<ExecutingJob> getExecutingJobByUsername(String username) {
		LOGGER.info("Begin Service ProjectService Function getExecutingJobByUsername");
		LOGGER.info("Service ProjectService Function getExecutingJobByUsername PARAMS username:" + username);
		List<UserProject> userProjects = this.getProjectByUserName(username);
		Set<String> projectNames = new HashSet<String>();
		for (UserProject userProject: userProjects) {
			projectNames.add(userProject.getProjectId().toString());
		}
		LOGGER.info("End Service ProjectService Function getExecutingJobByUsername");
		return executingJobRepo.findByStatusInAndProjectIdIn(JenkinService.LIST_RUNNING, projectNames);
		
	}

	public List<UserProject> getUserProjectByProjectId(Long projectId) {
		LOGGER.info("Begin Service ProjectService Function getUserProjectByProjectId");
		LOGGER.info("Service ProjectService Function getUserProjectByProjectId PARAMS projectId:" + projectId);
		
		LOGGER.info("End Service ProjectService Function getUserProjectByProjectId");
		return userProjectRepo.findByProjectIdOrderByProjectRole(projectId);
	}
	
	public List<String> findByProjectIdAndByProjectRole (Long projectId){
		return userProjectRepo.findByProjectIdAndByProjectRole(projectId);
	}
	public List<Map<String, Object>> getAllUserProjectByProjectId(Long projectId) {
		
		return userProjectRepo.getUserProjectByProjectId(projectId);
	}

	public void deleteProjectByUser(String username) {
        LOGGER.info("Begin Service ProjectService Function deleteProjectByUser");
        LOGGER.info("Service ProjectService Function deleteProjectByUser PARAMS username:" + username);
		
		LOGGER.info("End Service ProjectService Function deleteProjectByUser");
		this.userProjectRepo.deleteByUsername(username);
		
	}
	
	public List<ExecutingJob> getScheduleJob(String projectId){
        LOGGER.info("Begin Service ProjectService Function getScheduleJob");
        LOGGER.info("Service ProjectService Function getScheduleJob PARAMS projectId:" + projectId);
		
		LOGGER.info("End Service ProjectService Function getScheduleJob");
		return executingJobRepo.findByTypeAndProjectIdOrderByIdDesc(JenkinService.JOB_SCHEDULE,projectId);
		
	}
	
	public List<ExecutingJob> getRunningJobs(String projectId){
        LOGGER.info("Begin Service ProjectService Function getRunningJobs");
        LOGGER.info("Service ProjectService Function getRunningJobs PARAMS projectId:" + projectId);
		
		LOGGER.info("End Service ProjectService Function getRunningJobs");
		return executingJobRepo.findByTypeAndProjectIdOrderByIdDesc(JenkinService.JOB_RUNNING,projectId);
		
	}
	
	public List<ExecutingJob> getRunningJobInTestsuite(String projectId, String testsuite){
        LOGGER.info("Begin Service ProjectService Function getRunningJobInTestsuite");
        LOGGER.info("Service ProjectService Function getRunningJobInTestsuite PARAMS projectId:" + projectId);
        LOGGER.info("Service ProjectService Function getRunningJobInTestsuite PARAMS testsuite:" + testsuite);
		
		LOGGER.info("End Service ProjectService Function getRunningJobInTestsuite");
		return executingJobRepo.findByTypeAndProjectIdAndTestsuiteOrderByIdDesc(JenkinService.JOB_RUNNING,projectId, testsuite);
		
	}

	public List<ExecutingJob>  getScheduleJob(String projectId, String testsuiteId) {
        LOGGER.info("Begin Service ProjectService Function getScheduleJob");
        LOGGER.info("Service ProjectService Function getScheduleJob PARAMS projectId:" + projectId);
        LOGGER.info("Service ProjectService Function getScheduleJob PARAMS testsuiteId:" + testsuiteId);
		
		LOGGER.info("End Service ProjectService Function getScheduleJob");
		// TODO Auto-generated method stub
		return executingJobRepo.findByTypeAndProjectIdAndTestsuiteOrderByIdDesc(JenkinService.JOB_SCHEDULE, projectId,testsuiteId);
	}

	public Object deleteScheduleJob(ExecutingJob job) {
		LOGGER.info("Begin Service ProjectService Function deleteScheduleJob");
		LOGGER.info("Service ProjectService Function deleteScheduleJob PARAMS jobId:" + job.getId());
			
		executingJobRepo.deleteById(job.getId()); // TODO Auto-generated method stub
		
		LOGGER.info("End Service ProjectService Function deleteScheduleJob");
		return null;
	}
	
	public ExecutingJob getExecutingJobById(Long idJob) {
		LOGGER.info("Begin Service ProjectService Function getExecutingJobById");
		LOGGER.info("Service ProjectService Function getExecutingJobById PARAMS jobId:" + idJob);
		ExecutingJob job = executingJobRepo.findById(idJob).get();
		
		LOGGER.info("End Service ProjectService Function getExecutingJobById");
		return job;
	}
	
	public void saveInvited(InvitedToProject invited) {
	
		InvitedToProject valueInvited = invitedRepo.findByToEmailAndProjectId(invited.getToEmail(),invited.getProjectId()); 
		if(valueInvited == null) {
			invitedRepo.save(invited);	
		}else {
			if( valueInvited.getStatus() == 0 ) {
				valueInvited.setProjectRole(invited.getProjectRole());
				valueInvited.setCreateDate(new Date());
				valueInvited.setContent(invited.getContent());
				invitedRepo.save(valueInvited);
			}
		}
	}
	
	public List<InvitedToProject> getInvitedToProject(){
	
		return invitedRepo.findAll();
	}
	
	public List<InvitedToProject> getInvitedProjectByProjectIdAndStatus(String projectId) {
		
		return invitedRepo.findByProjectIdAndStatus(projectId);
	}
	
	public List<ProjectSummary> getTestsuiteLastRun(String username, String projectId) {
		LOGGER.info("Begin Service ProjectService Function getTestsuiteLastRun");
		LOGGER.info("Service ProjectService Function getTestsuiteLastRun PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function getTestsuiteLastRun PARAMS projectId:" + projectId);
		
		List<ProjectSummary> listProSum = new ArrayList<ProjectSummary>();
		listProSum = projectSummaryRepo.findProjectSummayByProjectIdAndCurDate(projectId);
		for(ProjectSummary p : listProSum) {
			String description = "";
			try {
				description = fileService.getDescriptionTestsuite(username, projectId, p.getProjectSummaryIdentity().getTestsuite());
			} catch (IOException e) {
				LOGGER.error("Service ProjectService Function getTestsuiteLastRun ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
			p.setDescription(description);
		}
		LOGGER.info("End Service ProjectService Function getTestsuiteLastRun");
		return listProSum;
	}
	
	public List<ProjectSummary> getTestsuiteCreatingProgress(Long projectId) throws IOException{
		
		return projectSummaryRepo.findByProjectSummaryIdentity_ProjectId(projectId.toString());
	}

	public Map<String,TestsuiteTestingProgress> getTestsuiteFromFile(String username, String projectId, Date date, Project project) {
		
		LOGGER.info("Begin Service ProjectService Function getTestsuiteCreatingProgress");
		LOGGER.info("Service ProjectService Function getTestsuiteCreatingProgress PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function getTestsuiteCreatingProgress PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getTestsuiteCreatingProgress PARAMS project:" + new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
		
		 Map<String,TestsuiteTestingProgress>  results = new  HashMap<String,TestsuiteTestingProgress> ();
		 try {
				File[] listfiles = fileService.getProjectTree(username, projectId);
				//check folder of project not found
				if (listfiles == null || listfiles.length <= 0) {
					TestsuiteTestingProgress progres = new TestsuiteTestingProgress();
					progres.setProjectName(project.getName());
					progres.setDescription(project.getDescription());
					progres.setColor(project.getColor());
					progres.setProjectId(projectId);
					results.put("ProjectPTS", progres);
					return results;
				}
				//get all folder of project and display in screen
				for (File file : listfiles) {
					JSONObject jsonOb;
					if (file.isDirectory()) {
						File testsuiteFile = new File(file, file.getName() + ".json");
						if (testsuiteFile.exists()) {
							String fileContent = fileService.readAJsonFile(testsuiteFile);
							jsonOb = new JSONObject(fileContent);
							String descriptionTs= jsonOb.getString("Description");
							
							TestsuiteTestingProgress progres = new TestsuiteTestingProgress();
							progres.setProjectId(projectId);
							progres.setExecuteDate(date);
							progres.setTestsuite(file.getName());
							progres.setDescriptionTestsuite(descriptionTs);
							progres.setProjectName(project.getName());
							progres.setDescription(project.getDescription());
							progres.setColor(project.getColor());
							results.put(file.getName(), progres);
						} else {
							TestsuiteTestingProgress progres = new TestsuiteTestingProgress();
							progres.setProjectName(project.getName());
							progres.setDescription(project.getDescription());
							progres.setColor(project.getColor());
							progres.setProjectId(projectId);
							results.put("ProjectPTS", progres);
						}
					} 
				} 
			} catch (JSONException e) {
				LOGGER.error("Service ProjectService Function getTestsuiteCreatingProgress ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
		 LOGGER.info("End Service ProjectService Function getTestsuiteCreatingProgress");
		 return results;
	}
	public Map<String,TestsuiteTestingProgress> getTestsuiteRunStatus(String username, String projectId, Date date, Project project){
		LOGGER.info("Begin Service ProjectService Function getTestsuiteRunStatus");
		LOGGER.info("Service ProjectService Function getTestsuiteRunStatus PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function getTestsuiteRunStatus PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getTestsuiteRunStatus PARAMS project:" + new Gson().toJson(project, new TypeToken<Project>() {}.getType()));
		List<Map<String,Object>> resultIfs = resultInfoRepo.findTotalGraphic(Integer.parseInt(projectId), date);
		
		 Map<String,TestsuiteTestingProgress>  results = new  HashMap<String,TestsuiteTestingProgress> ();
		 if(resultIfs.size() <=0) {
			 //check project not exist in database
			 results = this.getTestsuiteFromFile(username, projectId, date, project);
			 return results;
		 }
		 TestsuiteTestingProgress progress = new TestsuiteTestingProgress();
		 progress.setTestsuite((String)(resultIfs.get(0)).get("testsuite"));
   		 progress.setProjectId(projectId);
		 progress.setExecuteDate(date);
		 progress.setProjectName(project.getName());
		 progress.setColor(project.getColor());
		 progress.setDescription(project.getDescription());
		 results.put(progress.getTestsuite(), progress);
		for (Map<String, Object> info : resultIfs) {
			if (progress.getTestsuite().equals((String)info.get("testsuite")) ) {
			}else {
				progress = new TestsuiteTestingProgress();
				progress.setProjectId(projectId);
				progress.setExecuteDate(date);
				progress.setTestsuite((String)info.get("testsuite"));
				progress.setProjectName(project.getName());
				progress.setColor(project.getColor());
			    progress.setDescription(project.getDescription());
				results.put((String)info.get("testsuite"), progress);
			}
			if ("FAILED".equalsIgnoreCase((String)info.get("status"))) {
				progress.setTestcaseFail(((BigInteger) info.get("numcases")).intValue());
			}else if ("PASS".equalsIgnoreCase((String)info.get("status"))) {
				progress.setTestcasePass(((BigInteger) info.get("numcases")).intValue());
			}else if ("NOT-RUN".equalsIgnoreCase((String)info.get("status"))) {
				progress.setNotRun(((BigInteger) info.get("numcases")).intValue());
			}else if("DEGRADE".equalsIgnoreCase((String)info.get("status"))) {
				progress.setCountDegrade(((BigInteger) info.get("numcases")).intValue());
			}
		}
		
		//get all testsuite in project but not database
		List<String> listTs = new ArrayList<String>(results.keySet());
		try {
			File[] listfiles = fileService.getProjectTree(username, projectId);
			if(listfiles != null) {
				for (File file : listfiles) {
					JSONObject jsonOb;
					if (file.isDirectory()) {
						File testsuiteFile = new File(file, file.getName() + ".json");
						if (testsuiteFile.exists()) {
							String fileContent = fileService.readAJsonFile(testsuiteFile);
							jsonOb = new JSONObject(fileContent);
							String descriptionTs= jsonOb.getString("Description");
							if(listTs.indexOf(file.getName()) == -1) {
								TestsuiteTestingProgress progres = new TestsuiteTestingProgress();
								progres.setProjectId(projectId);
								progres.setExecuteDate(date);
								progres.setTestsuite(file.getName());
								progres.setDescriptionTestsuite(descriptionTs);
								progres.setProjectName(project.getName());
								progres.setDescription(project.getDescription());
								progres.setColor(project.getColor());
								progres.setProjectId(projectId);
								
								results.put(file.getName(), progres);
							}
						}
					}
				}
			}
			
		} catch (JSONException e) {
			LOGGER.error("Service ProjectService Function getTestsuiteRunStatus ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service ProjectService Function getTestsuiteRunStatus");
		return results;
	}
	
	public Map<String, ProjectMilestoneDTO> getMilestoneGraphic(Long projectId){
		LOGGER.info("Begin Service ProjectService Function getMilestoneGraphic");
		LOGGER.info("Service ProjectService Function getMilestoneGraphic PARAMS projectId:" + projectId);
		//Thoa lam
		List<Map<String, Object>> lstMistone = projectMilestoneRepo.findTotalGraphicForMistone(projectId);
		Map<String, ProjectMilestoneDTO> proDTOs = new HashMap<String, ProjectMilestoneDTO>();
		
		if(lstMistone.size() <= 0) {
			return proDTOs;
		}
		
		ProjectMilestoneDTO proMile = new ProjectMilestoneDTO();
		proMile.setProjectId(projectId);
		proMile.setMileStoneName((String)(lstMistone.get(0)).get("mile_stone_name"));
		
		ProjectMilestone prmi = projectMilestoneRepo.findByProjectMileStoneIdentity_ProjectIdAndProjectMileStoneIdentity_MileStoneName(projectId, (String)(lstMistone.get(0)).get("mile_stone_name"));
		proMile.setStartDate(prmi.getStartDate().toString());
		proMile.setEndDate(prmi.getEndDate().toString());
		if(prmi.getStatusMilestone() == null) {
			proMile.setStatusMilestone("Open");
		} else {
			proMile.setStatusMilestone(prmi.getStatusMilestone());
		}
		
		int testcaseTotal = projectProgressRepo.counttestcase(projectId);
		proMile.setTotalTestcase(testcaseTotal);
		proMile.setDescription((String)(lstMistone.get(0)).get("description"));
		proMile.setTestcaseName((String)(lstMistone.get(0)).get("testcaseName"));
		proMile.setStatusMilestone(prmi.getStatusMilestone());
		proDTOs.put((String)(lstMistone.get(0)).get("mile_stone_name"), proMile);
		
		for (Map<String, Object> info : lstMistone) {
			if(proMile.getMileStoneName().equals((String)info.get("mile_stone_name"))) {
				
			} else {
				proMile = new ProjectMilestoneDTO();
				proMile.setProjectId(projectId);
				proMile.setMileStoneName((String)info.get("mile_stone_name"));
				ProjectMilestone prmis = projectMilestoneRepo.findByProjectMileStoneIdentity_ProjectIdAndProjectMileStoneIdentity_MileStoneName(projectId, (String)info.get("mile_stone_name"));
				proMile.setStartDate(prmis.getStartDate().toString());
				proMile.setEndDate(prmis.getEndDate().toString());
				if(prmis.getStatusMilestone() == null) {
					proMile.setStatusMilestone("Open");
				} else {
					proMile.setStatusMilestone(prmis.getStatusMilestone());
				}
				
				int testcaseTotals = projectProgressRepo.counttestcase(projectId);
				proMile.setTotalTestcase(testcaseTotals);
				proMile.setDescription((String)info.get("description"));
				proMile.setTestcaseName((String)info.get("testcaseName"));
				proDTOs.put((String)info.get("mile_stone_name"), proMile);
			}
			if("NOT-RUN".equalsIgnoreCase((String)info.get("testcaseName"))) {
				
			} else {
				if ("FAILED".equalsIgnoreCase((String)info.get("status"))) {
					proMile.setTestcaseFail(((BigInteger) info.get("numberText")).intValue());
				}else if ("PASS".equalsIgnoreCase((String)info.get("status"))) {
					proMile.setTestcasePass(((BigInteger) info.get("numberText")).intValue());
				}else if ("NOT-RUN".equalsIgnoreCase((String)info.get("status"))) {
					proMile.setTestcaseNotRun(((BigInteger) info.get("numberText")).intValue());
				}else if("DEGRADE".equalsIgnoreCase((String)info.get("status"))) {
					proMile.setTestcaseDegrade(((BigInteger) info.get("numberText")).intValue());
				}
			}
		}
		
		LOGGER.info("End Service ProjectService Function getMilestoneGraphic");
		return proDTOs;
	}

	public List<ProjectProgress> getProjectProgressByProjectAndTestSuite(String projectId, String testsuiteId) {
		
		LOGGER.info("Begin Service ProjectService Function getProjectProgressByProjectAndTestSuite");
		LOGGER.info("Service ProjectService Function getProjectProgressByProjectAndTestSuite PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getProjectProgressByProjectAndTestSuite PARAMS testsuiteId:" + testsuiteId);

		LOGGER.info("End Service ProjectService Function getProjectProgressByProjectAndTestSuite");
		return projectProgressRepo.findByProjectProgressIdentity_ProjectIdAndProjectProgressIdentity_Testsuite(Long.valueOf(projectId),testsuiteId);
	}
	
	public List<ProjectProgress> getProjectProgressByStatusAutomation(String projectId, String testsuiteId) {
		
		LOGGER.info("Begin Service ProjectService Function getProjectProgressByStatusAutomation");
		LOGGER.info("Service ProjectService Function getProjectProgressByStatusAutomation PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getProjectProgressByStatusAutomation PARAMS testsuiteId:" + testsuiteId);
		
		LOGGER.info("End Service ProjectService Function getProjectProgressByStatusAutomation");
		return projectProgressRepo.findByProjectProgressStatusAutomation(Long.valueOf(projectId),testsuiteId);
	}
	
	public List<Map<String, Object>> getAllProjectProgressByProjectId(String projectId) {
		
		LOGGER.info("Begin Service ProjectService Function getProjectProgressByStatusAutomation");
		LOGGER.info("Service ProjectService Function getProjectProgressByStatusAutomation PARAMS projectId:" + projectId);
		
		LOGGER.info("End Service ProjectService Function getProjectProgressByStatusAutomation");
		return projectProgressRepo.findAllProjectProgressByProjectId(Long.valueOf(projectId));
	}
	
	public List<Map<String, Object>> getAllPPManualByProjectId(String projectId) {
			
			LOGGER.info("Begin Service ProjectService Function getProjectProgressByStatusAutomation");
			LOGGER.info("Service ProjectService Function getProjectProgressByStatusAutomation PARAMS projectId:" + projectId);
			
			LOGGER.info("End Service ProjectService Function getProjectProgressByStatusAutomation");
			return projectProgressRepo.findAllPPManualByProjectId(Long.valueOf(projectId));
		}
	
	public List<ProjectProgress> getProjectProgressByStatusManual(String projectId, String testsuiteId) {
		
		LOGGER.info("Begin Service ProjectService Function getProjectProgressByStatusManual");
		LOGGER.info("Service ProjectService Function getProjectProgressByStatusManual PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getProjectProgressByStatusManual PARAMS testsuiteId:" + testsuiteId);
		
		LOGGER.info("End Service ProjectService Function getProjectProgressByStatusManual");
		return projectProgressRepo.findByProjectProgressStatusManual(Long.valueOf(projectId),testsuiteId);
	}

	public List<ResultInfo> getResultInfoByName(String name, int projectNumber, String testsuiteId, String resultname) {
		
		LOGGER.info("Begin Service ProjectService Function getResultInfoByName");
		LOGGER.info("Service ProjectService Function getResultInfoByName PARAMS name:" + name);
		LOGGER.info("Service ProjectService Function getResultInfoByName PARAMS projectNumber:" + projectNumber);
		LOGGER.info("Service ProjectService Function getResultInfoByName PARAMS testsuiteId:" + testsuiteId);
		LOGGER.info("Service ProjectService Function getResultInfoByName PARAMS resultname:" + resultname);
		
		LOGGER.info("End Service ProjectService Function getResultInfoByName");
		return this.resultInfoRepo.findByProjectIdAndTestSuiteAndResultName(projectNumber,testsuiteId,resultname);
		
	}

	public List<ResultInfo> getResultInfoByExecuteJobId(String username, int projectNumber, String testsuiteId,
			String jobExecuteId) {
		LOGGER.info("Begin Service ProjectService Function getResultInfoByExecuteJobId");
		LOGGER.info("Service ProjectService Function getResultInfoByExecuteJobId PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function getResultInfoByExecuteJobId PARAMS projectNumber:" + projectNumber);
		LOGGER.info("Service ProjectService Function getResultInfoByExecuteJobId PARAMS testsuiteId:" + testsuiteId);
		
		Optional<ExecutingJob> jobOption = this.executingJobRepo.findById(Long.valueOf(jobExecuteId));
		if (jobOption.isPresent()) {
			ExecutingJob job = jobOption.get();
			
			String resultname= "Result" + "_" + job.getUsername().replace("@",".") + "_"+ job.getRunningDate() ;
			
			LOGGER.info("End Service ProjectService Function getResultInfoByExecuteJobId");
			return this.resultInfoRepo.findByProjectIdAndTestSuiteAndResultName(projectNumber, testsuiteId, resultname);
		}
		
		LOGGER.info("End Service ProjectService Function getResultInfoByExecuteJobId MESSAGE: Results is empty");
		// TODO Auto-generated method stub
		return new ArrayList<ResultInfo>();
	}
	
	public List<ResultInfo> getAllManualResults(int projectId, String testsuite){
		LOGGER.info("Begin Service ProjectService Function getAllManualResults");
		LOGGER.info("Service ProjectService Function getAllManualResults PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getAllManualResults PARAMS testsuite:" + testsuite);
		
		LOGGER.info("End Service ProjectService Function getAllManualResults");
		return resultInfoRepo.findByProjectIdAndTestSuite(projectId, testsuite);
	}
	
	public List<ResultInfo> getAllResultInTestcaseName(int projectId, String testsuite, String testcaseName){
		LOGGER.info("Begin Service ProjectService Function getAllResultInTestcaseName");
		LOGGER.info("Service ProjectService Function getAllResultInTestcaseName PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getAllResultInTestcaseName PARAMS testsuite:" + testsuite);
		LOGGER.info("Service ProjectService Function getAllResultInTestcaseName PARAMS testcaseName:" + testcaseName);
		
		LOGGER.info("End Service ProjectService Function getAllResultInTestcaseName");
		return resultInfoRepo.getAllResultInProjectIdAndTestSuiteAndTestcaseName(projectId, testsuite, testcaseName);
	}

	public void writeTestcasePs(String name, String projectId, String testsuiteId, ProjectProgress projectProgress,
			String testcase) {
		LOGGER.info("Begin Service ProjectService Function writeTestcasePs");
		LOGGER.info("Service ProjectService Function writeTestcasePs PARAMS name:" + name);
		LOGGER.info("Service ProjectService Function writeTestcasePs PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function writeTestcasePs PARAMS testsuiteId:" + testsuiteId);
		LOGGER.info("Service ProjectService Function writeTestcasePs PARAMS projectProgress:" + new Gson().toJson(projectProgress, new TypeToken<ProjectProgress>() {}.getType()));
		LOGGER.info("Service ProjectService Function writeTestcasePs PARAMS testcase:" + testcase);
		int orderId = projectProgressRepo.getMaxOrderId(Long.parseLong(projectId), projectProgress.getType());
		if(projectProgress.getOrderId() == 0) {
			projectProgress.setOrderId(orderId+1);
		}
		projectProgress.setUpdateUser(name);
		projectProgressRepo.save(projectProgress);
		
		try {
			fileService.writeTestCaseManual(name,projectId, testsuiteId, projectProgress, projectProgress.getProjectProgressIdentity().getTestcase());
		} catch (IOException e) {
			LOGGER.error("Service ProjectService Function writeTestcasePs ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service ProjectService Function writeTestcasePs");
		
	}

	public void editTestcasePs(String name, String projectId, String testsuiteId, ProjectProgress projectProgress,
			String testcase) {
		LOGGER.info("Begin Service ProjectService Function editTestcasePs");
		LOGGER.info("Service ProjectService Function editTestcasePs PARAMS name:" + name);
		LOGGER.info("Service ProjectService Function editTestcasePs PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function editTestcasePs PARAMS testsuiteId:" + testsuiteId);
		LOGGER.info("Service ProjectService Function editTestcasePs PARAMS projectProgress:" + new Gson().toJson(projectProgress, new TypeToken<ProjectProgress>() {}.getType()));
		LOGGER.info("Service ProjectService Function editTestcasePs PARAMS testcase:" + testcase);
		
		ProjectProgress pr = projectProgressRepo.findByProjectProgressIdentity_ProjectIdAndProjectProgressIdentity_TestsuiteAndProjectProgressIdentity_Testcase(Long.parseLong(projectId), testsuiteId, projectProgress.getProjectProgressIdentity().getTestcase());
		pr.setDescription(projectProgress.getDescription());
		pr.setAssignTo(projectProgress.getAssignTo());
		pr.setPreCondition(projectProgress.getPreCondition());
		pr.setExpectedOutput(projectProgress.getExpectedOutput());
		pr.setTestcaseProcedure(projectProgress.getTestcaseProcedure());
		pr.setUpdateDate(projectProgress.getUpdateDate());
		pr.setActualTime(projectProgress.getActualTime());
		pr.setCategory(projectProgress.getCategory());
		pr.setEstimatedTime(projectProgress.getEstimatedTime());
		pr.setPlanEndDate(projectProgress.getPlanEndDate());
		pr.setPlanStartDate(projectProgress.getPlanStartDate());
		pr.setStatusIssue(projectProgress.getStatusIssue());
		pr.setMilestone(projectProgress.getMilestone());
		projectProgressRepo.save(pr);
		
		try {
			fileService.editTestCaseManual(name,projectId, testsuiteId, projectProgress, projectProgress.getProjectProgressIdentity().getTestcase());
		} catch (IOException e) {
			LOGGER.error("Service ProjectService Function editTestcasePs ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		LOGGER.info("End Service ProjectService Function editTestcasePs");
	}

	public void deleteTestcaseManual(String name, String projectId, String testsuiteId, String testcaseName) {
		LOGGER.info("Begin Service ProjectService Function deleteTestcaseManual");
		LOGGER.info("Service ProjectService Function deleteTestcaseManual PARAMS name:" + name);
		LOGGER.info("Service ProjectService Function deleteTestcaseManual PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function deleteTestcaseManual PARAMS testsuiteId:" + testsuiteId);
		LOGGER.info("Service ProjectService Function deleteTestcaseManual PARAMS testcaseName:" + testcaseName);
			ProjectProgressIdentity id = new ProjectProgressIdentity();
			id.setProjectId(Long.valueOf(projectId));
			id.setTestsuite(testsuiteId);
			id.setTestcase(testcaseName);
			Optional<ProjectProgress> p = projectProgressRepo.findById(id);
			if (p.isPresent()) {
				p.get().setStatus("delete");
				projectProgressRepo.save(p.get());
				
				//update result info
				this.updateStatusInResultInfo(Integer.parseInt(projectId), testsuiteId, testcaseName);
				
				try {
					fileService.deleteTestcaseManual(name,projectId, testsuiteId, testcaseName);
				} catch (IOException e) {
					LOGGER.error("Service ProjectService Function deleteTestcaseManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
				}
			}
			LOGGER.info("End Service ProjectService Function deleteTestcaseManual");
	}
	
	public void deleteMilestone(Long projectId, String milestoneName) {
		LOGGER.info("Begin Service ProjectService Function deleteMilestone");
		LOGGER.info("Service ProjectService Function deleteMilestone PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function deleteMilestone PARAMS milestoneName:" + milestoneName);
		ProjectMilestone prm = projectMilestoneRepo.findByProjectMileStoneIdentity_ProjectIdAndProjectMileStoneIdentity_MileStoneName(projectId, milestoneName);
		prm.setStatus("delete");
		projectMilestoneRepo.save(prm);
		
		LOGGER.info("End Service ProjectService Function deleteMilestone");
	}
	
	public void deleteTestsuiteWithTestcaseAutomation(String name, String projectId, String testsuiteId) {
		LOGGER.info("Begin Service ProjectService Function deleteTestsuiteWithTestcaseAutomation");
		LOGGER.info("Service ProjectService Function deleteTestsuiteWithTestcaseAutomation PARAMS name:" + name);
		LOGGER.info("Service ProjectService Function deleteTestsuiteWithTestcaseAutomation PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function deleteTestsuiteWithTestcaseAutomation PARAMS testsuiteId:" + testsuiteId);
		List<ProjectProgress> listAuto = this.getAllProjectProgressAutomation(Long.parseLong(projectId), testsuiteId);
		for(ProjectProgress p : listAuto) {
			this.deleteTestcaseAutomation(name, Long.parseLong(projectId), testsuiteId, p.getProjectProgressIdentity().getTestcase());
		}
		
		List<ProjectProgress> listManual = this.getAllTestcaseManual(Long.parseLong(projectId), testsuiteId);
		for(ProjectProgress pp: listManual) {
			this.deleteTestcaseManual(name, projectId, testsuiteId, pp.getProjectProgressIdentity().getTestcase());
		}
		LOGGER.info("End Service ProjectService Function deleteTestsuiteWithTestcaseAutomation");
	}
	
	public List<Map<String, Object>> getDetailsMilestone(Long projectId, String milestoneName) {
		LOGGER.info("Begin Service ProjectService Function getDetailsMilestone");
		LOGGER.info("Service ProjectService Function getDetailsMilestone PARAMS milestoneName:" + milestoneName);
		LOGGER.info("Service ProjectService Function getDetailsMilestone PARAMS projectId:" + projectId);
		
		LOGGER.info("End Service ProjectService Function getDetailsMilestone");
		return projectProgressRepo.getAllMilestoneGraphic(projectId, milestoneName);
	}
	
	public ProjectMilestoneTestcase saveProMsTcToTable(ProjectMilestoneTestcase projectMsTc) {
		LOGGER.info("Begin Service ProjectService Function saveProMsTcToTable");
		LOGGER.info("Service ProjectService Function saveProMsTcToTable PARAMS projectMsTc:" + new Gson().toJson(projectMsTc, new TypeToken<ProjectMilestoneTestcase>() {}.getType()));
		
		LOGGER.info("End Service ProjectService Function saveProMsTcToTable");
		return projectMilestoneTestcaseRepo.save(projectMsTc);
	}
	
	public List<ProjectMilestoneTestcase> getAllDataFromProjectMilestoneTestcase(Long projectId, String milestone){
		LOGGER.info("Begin Service ProjectService Function getAllDataFromProjectMilestoneTestcase");
		LOGGER.info("Service ProjectService Function getAllDataFromProjectMilestoneTestcase PARAMS milestone:" + milestone);
		LOGGER.info("Service ProjectService Function getAllDataFromProjectMilestoneTestcase PARAMS projectId:" + projectId);
		
		
		LOGGER.info("End Service ProjectService Function getAllDataFromProjectMilestoneTestcase");
		return projectMilestoneTestcaseRepo.findByProjectMilestoneTestcaseIdentity_ProjectIdAndProjectMilestoneTestcaseIdentity_MilestoneName(projectId, milestone);
	}
	
	public int deleteDataFromProjectMilestoneTc(Long projectId, String milestoneName) {
		LOGGER.info("Begin Service ProjectService Function deleteDataFromProjectMilestoneTc");
		LOGGER.info("Service ProjectService Function deleteDataFromProjectMilestoneTc PARAMS milestoneName:" + milestoneName);
		LOGGER.info("Service ProjectService Function deleteDataFromProjectMilestoneTc PARAMS projectId:" + projectId);
		
		LOGGER.info("End Service ProjectService Function deleteDataFromProjectMilestoneTc");
		return projectMilestoneTestcaseRepo.deleteDataFromTable(projectId, milestoneName);
	}
	
	public ResultInfo saveTestResultManual(String testsuiteName, int projectId, String testcaseName, String mobileMode, ResultInfoDTO resultInfoDTO, JSONArray jrA) {
		LOGGER.info("Begin Service ProjectService Function saveTestResultManual");
		LOGGER.info("Service ProjectService Function saveTestResultManual PARAMS testsuiteName:" + testsuiteName);
		LOGGER.info("Service ProjectService Function saveTestResultManual PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function saveTestResultManual PARAMS mobileMode:" + mobileMode);
		ResultInfo resultInf = new ResultInfo();
		resultInf.setId(resultInfoDTO.getId());
		resultInf.setResultName("");
		resultInf.setTestSuite(testsuiteName);
		resultInf.setTestResult("");
		resultInf.setRunningUser(resultInfoDTO.getManualRunningUser());
		
		try {
			resultInf.setExecuteDate(FunctionUtils.ISO_DATE.parse(resultInfoDTO.getDatetimeLastRun()));
		} catch (ParseException e) {
			LOGGER.error("Service ProjectService Function saveTestResultManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		
		resultInf.setTestCaseName(testcaseName);
		resultInf.setTestCaseResult(resultInfoDTO.getManualStatus());
		resultInf.setProjectId(projectId);
		resultInf.setMilestone(resultInfoDTO.getMileStoneId());
		resultInf.setOs(resultInfoDTO.getManualOS());
		resultInf.setBrowser(resultInfoDTO.getManualBrowser());
		resultInf.setScreenResolution(resultInfoDTO.getManualResolution());
		resultInf.setLocation(resultInfoDTO.getManualLocation());
		resultInf.setMobileMode(mobileMode);
		resultInf.setDeviceName(resultInfoDTO.getManualDeviceName());
		resultInf.setRunningDescription(resultInfoDTO.getManualDescription());
		resultInf.setExpectedOutput(resultInfoDTO.getManualExpectedOutput());
		resultInf.setActualOutput(resultInfoDTO.getManualActualOutput());
		resultInf.setType("manual");
		
		resultInf.setFiles(jrA.toString());
		
		resultInfoRepo.save(resultInf);
		
		LOGGER.info("End Service ProjectService Function saveTestResultManual");
		return resultInf;
	}
	
	public void saveTestcaseAutomation(String username, String projectId, String testsuiteId, ProjectProgress projectProgress, String testcaseName) {
		LOGGER.info("Begin Service ProjectService Function saveTestcaseAutomation");
		LOGGER.info("Service ProjectService Function saveTestcaseAutomation PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function saveTestcaseAutomation PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function saveTestcaseAutomation PARAMS testsuiteId:" + testsuiteId);
		LOGGER.info("Service ProjectService Function saveTestcaseAutomation PARAMS projectProgress:" + new Gson().toJson(projectProgress, new TypeToken<ProjectProgress>() {}.getType()));
		LOGGER.info("Service ProjectService Function saveTestcaseAutomation PARAMS testcaseName:" + testcaseName);
		int orderId = projectProgressRepo.getMaxOrderId(Long.parseLong(projectId), projectProgress.getType());
		if(projectProgress.getOrderId() == 0) {
			projectProgress.setOrderId(orderId+1);
		}
		
		projectProgressRepo.save(projectProgress);
		//update project summary
		try {
			fileService.writeTestCase(username,projectId, testsuiteId, projectProgress, projectProgress.getProjectProgressIdentity().getTestcase());
		} catch (Exception e) {
			LOGGER.error("Service ProjectService Function saveTestcaseAutomation ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		LOGGER.info("End Service ProjectService Function saveTestcaseAutomation");
	}
	
	public List<Map<String, Object>> getAllProjectProgressAutomations(Long projectId, String testsuiteId) {
		LOGGER.info("Begin Service ProjectService Function getAllProjectProgressAutomation");
		LOGGER.info("Service ProjectService Function getAllProjectProgressAutomation PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getAllProjectProgressAutomation PARAMS testsuiteId:" + testsuiteId);
		
		LOGGER.info("End Service ProjectService Function getAllProjectProgressAutomation");
		return projectProgressRepo.findAllProjectProgressAutomations(projectId, testsuiteId);
	}
	
	public List<ProjectProgress> getAllProjectProgressAutomation(Long projectId, String testsuiteId) {
		LOGGER.info("Begin Service ProjectService Function getAllProjectProgressAutomation");
		LOGGER.info("Service ProjectService Function getAllProjectProgressAutomation PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getAllProjectProgressAutomation PARAMS testsuiteId:" + testsuiteId);
		
		LOGGER.info("End Service ProjectService Function getAllProjectProgressAutomation");
		return projectProgressRepo.findAllProjectProgressAutomation(projectId, testsuiteId);
	}
	
	public void deleteTestcaseAutomation(String username, Long projectId, String testsuite, String testcase) {
		LOGGER.info("Begin Service ProjectService Function deleteTestcaseAutomation");
		LOGGER.info("Service ProjectService Function deleteTestcaseAutomation PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function deleteTestcaseAutomation PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function deleteTestcaseAutomation PARAMS testsuite:" + testsuite);
		LOGGER.info("Service ProjectService Function deleteTestcaseAutomation PARAMS testcase:" + testcase);
		
		ProjectProgress projectProgress = projectProgressRepo.findByProjectIdAndTestsuiteAndTestcase(projectId, testsuite, testcase);
		projectProgress.setStatus("delete");
		projectProgressRepo.save(projectProgress);
		
		//update result info
		this.updateStatusInResultInfo(projectId.intValue(), testsuite, testcase);
		
		try {
			fileService.deleteTestcase(username, String.valueOf(projectId), testsuite, testcase);
		} catch (IOException e) {
			LOGGER.error("Service ProjectService Function deleteTestcaseAutomation ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		LOGGER.info("End Service ProjectService Function deleteTestcaseAutomation");
	}
	
	public void updateStatusInResultInfo(int projectId, String testsuite, String testcaseName) {
		LOGGER.info("Begin Service ProjectService Function updateStatusInResultInfo");
		LOGGER.info("Service ProjectService Function updateStatusInResultInfo PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function updateStatusInResultInfo PARAMS testsuite:" + testsuite);
		LOGGER.info("Service ProjectService Function updateStatusInResultInfo PARAMS testcaseName:" + testcaseName);
		List<ResultInfo> listResult = this.getAllResultInTestcaseName(projectId, testsuite, testcaseName);
		for(ResultInfo rs : listResult) {
			rs.setStatus("delete");
			resultInfoRepo.save(rs);
		}
		LOGGER.info("End Service ProjectService Function updateStatusInResultInfo");
	}
	
	public ProjectProgress getTestscriptFromTestcase(Long projectId, String testsuite, String testcase) {
		LOGGER.info("Begin Service ProjectService Function getTestscriptFromTestcase");
		LOGGER.info("Service ProjectService Function getTestscriptFromTestcase PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getTestscriptFromTestcase PARAMS testsuite:" + testsuite);
		LOGGER.info("Service ProjectService Function getTestscriptFromTestcase PARAMS testcase:" + testcase);
		ProjectProgress projectProgress = projectProgressRepo.findByProjectIdAndTestsuiteAndTestcase(projectId, testsuite, testcase);
		
		LOGGER.info("End Service ProjectService Function getTestscriptFromTestcase");
		return projectProgress;
	}
	
	public void saveTestcaseAndProjectSummary(String username, String projectId, String testsuiteStr) {
		
		LOGGER.info("Begin Service ProjectService Function saveTestcaseAndProjectSummary");
		JSONObject testsuite = null;
		try {
			testsuite = new JSONObject(testsuiteStr);
			String testSuiteName = testsuite.getString("Testsuite");
			
			ProjectSummaryIdentity proSuInd = new ProjectSummaryIdentity();
			proSuInd.setDate(new Date());
			proSuInd.setProjectId(projectId);
			proSuInd.setTestsuite(testSuiteName);
			
			ProjectSummary projectSummary =  new ProjectSummary();
			projectSummary.setProjectSummaryIdentity(proSuInd);
			projectSummary.setPass(0);
			projectSummary.setFail(0);
			projectSummary.setNotRun(0);
			
			projectSummaryRepo.save(projectSummary);
			
			fileService.createTestSuite(username,projectId, testsuite);
		} catch (Exception e) {
			LOGGER.error("Service ProjectService Function saveTestcaseAndProjectSummary ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service ProjectService Function saveTestcaseAndProjectSummary");
	}
	
	public void saveResultManual(String username, String projectId, String testsuiteId, ResultInfo resultInfo, String testcaseName) {
		LOGGER.info("Begin Service ProjectService Function saveResultManual");
		LOGGER.info("Service ProjectService Function saveResultManual PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function saveResultManual PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function saveResultManual PARAMS testsuiteId:" + testsuiteId);
		LOGGER.info("Service ProjectService Function saveResultManual PARAMS testcaseName:" + testcaseName);
		//update project summary
//		this.checkResultManualToProjectSummary(resultInfo.getTestCaseResult(), projectId, testsuiteId, testcaseName);
		
		resultInfoRepo.save(resultInfo);
		try {
			fileService.writeResultToManual(username,projectId, testsuiteId, resultInfo, testcaseName);
		} catch (IOException e) {
			LOGGER.error("Service ProjectService Function saveResultManual ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service ProjectService Function saveResultManual");
	}
	
	public void deleteManualResult(String username,String projectId, String testsuitId, Long id, String testcaseName, ResultInfo resultInfo) {
		
		LOGGER.info("Begin Service ProjectService Function deleteManualResult");
		LOGGER.info("Service ProjectService Function deleteManualResult PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function deleteManualResult PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function deleteManualResult PARAMS testsuitId:" + testsuitId);
		LOGGER.info("Service ProjectService Function deleteManualResult PARAMS id:" + id);
		LOGGER.info("Service ProjectService Function deleteManualResult PARAMS testcaseName:" + testcaseName);
//		this.checkEditResultManualToProjectSummary(id, projectId, testsuitId);
		resultInfoRepo.deleteById(id);
		String nameFiles = resultInfo.getFiles();
		String arr [] = nameFiles.replace("[", "").replace ("]", "").split (",");
		List<String> lstFile = Arrays.asList(arr);
		
		for(String nameFile: lstFile) {
			try {
				fileService.deleteFileResultManual(username,projectId, testsuitId, nameFile.replace("'", ""));
			} catch (IOException e) {
				LOGGER.error("Service ProjectService Function deleteManualResult ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
		}
		
		try {
			fileService.deleteResultToManual(username,projectId, testsuitId, resultInfo.getId(), resultInfo.getTestCaseName());
		} catch (IOException e) {
			LOGGER.error("Service ProjectService Function deleteManualResult ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service ProjectService Function deleteManualResult");
	}
	
	public ProjectSummary getProjectSummary(String projectId, String testsuiteId) {
		LOGGER.info("Begin Service ProjectService Function getProjectSummary");
		LOGGER.info("Service ProjectService Function getProjectSummary PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function getProjectSummary PARAMS testsuiteId:" + testsuiteId);
		
		ProjectSummary projectSummary = projectSummaryRepo.findByDateAndProjectIdAndTestsuite(projectId, testsuiteId);
		
		LOGGER.info("End Service ProjectService Function getProjectSummary");
		return projectSummary;
	}
	
	public Map<String,TestsuiteTestingProgress> getTestsuiteRunStatusByDate(String username,String projectId, Date date){
		
		LOGGER.info("Begin Service ProjectService Function getTestsuiteRunStatusByDate");
		LOGGER.info("Service ProjectService Function getTestsuiteRunStatusByDate PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function getTestsuiteRunStatusByDate PARAMS projectId:" + projectId);
		List<Map<String,Object>> resultIfs = resultInfoRepo.findTotalGraphic(Integer.parseInt(projectId), date);
		 Map<String,TestsuiteTestingProgress>  results = new  HashMap<String,TestsuiteTestingProgress> ();
		 if(resultIfs.size() <=0) {
			 try {
					File[] listfiles = fileService.getProjectTree(username, projectId);
					if(listfiles != null) {
						for (File file : listfiles) {
							JSONObject jsonOb;
							if (file.isDirectory()) {
								File testsuiteFile = new File(file, file.getName() + ".json");
								if (testsuiteFile.exists()) {
									String fileContent = fileService.readAJsonFile(testsuiteFile);
									jsonOb = new JSONObject(fileContent);
									String descriptionTs= jsonOb.getString("Description");
									
									TestsuiteTestingProgress progres = new TestsuiteTestingProgress();
									progres.setProjectId(projectId);
									progres.setExecuteDate(date);
									progres.setTestsuite(file.getName());
									progres.setDescriptionTestsuite(descriptionTs);
									
									results.put(file.getName(), progres);
								}
							}
						}
					}
					
				} catch (JSONException e) {
					LOGGER.error("Service ProjectService Function getTestsuiteRunStatusByDate ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
				}
			 LOGGER.info("End Service ProjectService Function getTestsuiteRunStatusByDate");
			 return results;
		 }
		 TestsuiteTestingProgress progress = new TestsuiteTestingProgress();
		 progress.setTestsuite((String)(resultIfs.get(0)).get("testsuite"));
   		 progress.setProjectId(projectId);
		 progress.setExecuteDate(date);
		 String descriptionTestsuite = "";
		 try {
			 descriptionTestsuite = fileService.getDescriptionTestsuite(username, projectId, (String)(resultIfs.get(0)).get("testsuite"));
		} catch (IOException e) {
			LOGGER.error("Service ProjectService Function getTestsuiteRunStatusByDate ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		 progress.setDescriptionTestsuite(descriptionTestsuite);
		 results.put(progress.getTestsuite(), progress);
		for (Map<String, Object> info : resultIfs) {
			if (progress.getTestsuite().equals((String)info.get("testsuite")) ) {
			}else {
				progress = new TestsuiteTestingProgress();
				progress.setProjectId(projectId);
				progress.setExecuteDate(date);
				progress.setTestsuite((String)info.get("testsuite"));
				String descriptionTestsuites = "";
				 try {
					 descriptionTestsuites = fileService.getDescriptionTestsuite(username, projectId, (String)info.get("testsuite"));
				} catch (IOException e) {
					LOGGER.error("Service ProjectService Function getTestsuiteRunStatusByDate ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
				}
				 progress.setDescriptionTestsuite(descriptionTestsuites);
				results.put((String)info.get("testsuite"), progress);
			}
			if ("FAILED".equalsIgnoreCase((String)info.get("status"))) {
				progress.setTestcaseFail(((BigInteger) info.get("numcases")).intValue());
			}else if ("PASS".equalsIgnoreCase((String)info.get("status"))) {
				progress.setTestcasePass(((BigInteger) info.get("numcases")).intValue());
			}else if ("NOT-RUN".equalsIgnoreCase((String)info.get("status"))) {
				progress.setNotRun(((BigInteger) info.get("numcases")).intValue());
			}else if("DEGRADE".equalsIgnoreCase((String)info.get("status"))) {
				progress.setCountDegrade(((BigInteger) info.get("numcases")).intValue());
			}
		}
		
		List<String> listTs = new ArrayList<String>(results.keySet());
		try {
			File[] listfiles = fileService.getProjectTree(username, projectId);
			if(listfiles != null) {
				for (File file : listfiles) {
					JSONObject jsonOb;
					if (file.isDirectory()) {
						File testsuiteFile = new File(file, file.getName() + ".json");
						if (testsuiteFile.exists()) {
							String fileContent = fileService.readAJsonFile(testsuiteFile);
							jsonOb = new JSONObject(fileContent);
							String descriptionTs= jsonOb.getString("Description");
							if(listTs.indexOf(file.getName()) == -1) {
								TestsuiteTestingProgress progres = new TestsuiteTestingProgress();
								progres.setProjectId(projectId);
								progres.setExecuteDate(date);
								progres.setTestsuite(file.getName());
								progres.setDescriptionTestsuite(descriptionTs);
								
								results.put(file.getName(), progres);
							}
						}
					}
				}
			}
			
		} catch (JSONException e) {
			LOGGER.error("Service ProjectService Function getTestsuiteRunStatusByDate ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("End Service ProjectService Function getTestsuiteRunStatusByDate");
		return results;
	}

	/**
     *  Get All project active
     * @return
     */
	public List<Project> getAllProjectActive() {
		LOGGER.info("Begin Service ProjectService Function getTestsuiteRunStatusByDate");
		List<Project> listProj = new ArrayList<Project>();
		
		// Call Db get all user project active
		listProj = projectRepo.getAllUserProjectByFlag();
		
		LOGGER.info("Service ProjectService Function setFieldInEntity RESULTS listProj:" + new Gson().toJson(listProj, new TypeToken<List<Project>>() {}.getType()));
		LOGGER.info("End Service ProjectService Function getTestsuiteRunStatusByDate");
		return listProj;
	}
	
	public ExecutingJob setFieldInEntity(ExecutingJob job) {
		LOGGER.info("Begin Service ProjectService Function setFieldInEntity");
		ExecutingJob jobNew = new ExecutingJob();
		jobNew.setRunningName(job.getRunningName());
		jobNew.setType(job.getType());
		jobNew.setUsername(job.getUsername());
		jobNew.setProjectId(job.getProjectId());
		jobNew.setTestsuite(job.getTestsuite());
		jobNew.setLsTestcases(job.getLsTestcases());
		jobNew.setRunningDate(job.getRunningDate());
		jobNew.setRunningOS(job.getRunningOS());
		jobNew.setTimeout(job.getTimeout());
		jobNew.setRunningLocation(job.getRunningLocation());
		jobNew.setScreenRes(job.getScreenRes());
		jobNew.setMobileMode(job.getMobileMode());
		jobNew.setMobileRes(job.getMobileRes());
		jobNew.setDeviceName(job.getDeviceName());
		jobNew.setBrowser(job.getBrowser());
		jobNew.setRecordVideo(job.getRecordVideo());
		jobNew.setRunDescription(job.getRunDescription());
		jobNew.setScheduleSpec("");
		
		LOGGER.info("Service ProjectService Function setFieldInEntity RESULTS jobNew:" + new Gson().toJson(jobNew, new TypeToken<ExecutingJob>() {}.getType()));
		LOGGER.info("End Service ProjectService Function setFieldInEntity");
		return jobNew;
	}
	
	public Project saveSampleProject(Project project, String username) {
		projectRepo.save(project);
		
		Project pro = projectRepo.getProjectNew(project.getId());
		if ("svn".equalsIgnoreCase(pro.getScmType())) {
			String url = pro.getScmUrl().trim();
			if (url.endsWith("/")) {
				pro.setScmUrl(url + pro.getName().replaceAll(" ", "_") + "_" + pro.getId());
			}else {
				pro.setScmUrl(url + "/" + pro.getName().replaceAll(" ", "_") + "_" + pro.getId());
			}
		}
		
		projectRepo.save(pro);
		
		try {
			
			List<UserProject> userProjects = userProjectRepo.findByProjectIdAndUsername(pro.getId(),username);

			if (userProjects.isEmpty()) {
				UserProject userProject = new UserProject();
				userProject.setProjectId(pro.getId());
				userProject.setProjectName(pro.getName());
				userProject.setUsername(username);
				userProject.setProjectRole(UserProject.PROJECT_ADMIN);
				userProjectRepo.save(userProject);
				fileService.createProject(username,pro);
			}else {
				for (UserProject userProject : userProjects) {
					userProject.setProjectName(pro.getName());
				}
				userProjectRepo.saveAll(userProjects);
			}

			//fileService.syncProject(project, username);
		} catch ( Exception e) {
			LOGGER.error("Service ProjectService Function saveProject ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			return null;
		}
		
		return pro;
	}
	
	public List<String> getExecuteBy(int projectId) {
		return resultInfoRepo.getNameExecuteBy(projectId);
	}
	
	public List<String> getExecuteByManual(int projectId) {
		return resultInfoRepo.getNameExecuteByManual(projectId);
	}
	
	public String getResultJson(String projectId, String testsuilt, String runningDate) {
		
		return executingJobRepo.findResultByProjectIdAndTestsuiteAndRunningDate(projectId, testsuilt, runningDate);
		
	}
	
	public List<Map<String, Object>> filterTestcaseAutomation(String projectId, FilterConditionData filterTc){
		
		List<Map<String, Object>> lsTc = projectProgressRepo.filterTestcaseAutomation(Long.parseLong(projectId), filterTc);
		return lsTc;
	}
	
	public ResultInfo getResultInfoById(Long id) {
		return resultInfoRepo.findResultInfoById(id);
	}
	
	public ResultInfo getPreviousManualResultInfo(Long projectId, String testsuite, String testcaseName, Long id) {
		return resultInfoRepo.previousManualResultInfo(projectId, testsuite, testcaseName, id);
	}
	
	public ResultInfo getNextManualResultInfo(Long projectId, String testsuite, String testcaseName, Long id) {
		return resultInfoRepo.nextManualResultInfo(projectId, testsuite, testcaseName, id);
	}
	
	public List<ScreenDesign> getListScreenDesign(Long projectId) {
		return screenDesignRepo.findScreenDesignByProjectId(projectId);
	}
	
	public void saveScreenDesign(String username, String projectId, String testsuiteName, ScreenDesign screenDesign) {
		screenDesignRepo.save(screenDesign);
		try {
			fileService.saveLayout(username, projectId, testsuiteName, screenDesign.getLayoutContent(), screenDesign.getLayoutName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ScreenDesign getLayoutByName(String name, String projectId, String testsuiteId, String fileNameLayout) {
		// TODO Auto-generated method stub
		return screenDesignRepo.findLayoutByName(Long.parseLong(projectId), testsuiteId, fileNameLayout);
	}
 	
	public void deleteLayout(String username,String projectId, String testsuiteName, String layoutName, String id) {
		screenDesignRepo.deleteById(Long.parseLong(id));
		try {
			fileService.deleteLayout(username,projectId, testsuiteName, layoutName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<String> getNameCreateByLayout(Long projectId){
		return screenDesignRepo.getNameCreateByLayout(projectId);
	}
	
	public List<ScreenDesign> filterScreenDesign(Long projectId, FilterConditionData filter){
		return screenDesignRepo.filterScreenDesign(projectId, filter);
	}
	
	public List<UploadTestData> getTestData(Long projectId){
		
		return uploadTestDataRepo.findUploadTestDataByProjectId(projectId);
	}
	
	public List<UploadTestData> getTestDataByTestsuite(Long projectId, String testsuiteId){
		
		return uploadTestDataRepo.findUploadTestDataByProjectIdAndTestsuiteName(projectId,testsuiteId);
	}


	public List<ScreenDesign> getListScreenDesignByTestsuiteId(long projectId, String testsuiteId) {
		// TODO Auto-generated method stub
		return screenDesignRepo.findScreenDesignByProjectIdAndTestsuiteName(projectId, testsuiteId);
	}

	public void saveTestDataToDB(String name, String projectId, String testsuiteId, UploadTestData uploadTestdata,
			String layoutName) {
		uploadTestdata.setCreateDate(new Date());
		uploadTestDataRepo.save(uploadTestdata);
		try {
			fileService.writeTestData(name,projectId, testsuiteId, uploadTestdata.getTestdataContent(), layoutName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public UploadTestData getUploadTestDataById(String idTestData) {
		// TODO Auto-generated method stub
		return uploadTestDataRepo.findById(Long.parseLong(idTestData)).get();
	}
	
	public List<UploadTestData> filterUploadTestData(Long projectId, FilterConditionData filter){
		return uploadTestDataRepo.filterUploadTestData(projectId, filter);
	}
	
	public ScreenDesign getScreenDesignById(Long id) {
		return screenDesignRepo.findById(id).get();
	}
	
	public int countAllScreenDesign(Long projectId, String testsuiteName) {
		return screenDesignRepo.countScreenDesgin(projectId, testsuiteName);
	}

	public int countAllTestDatas(long projectId, String testsuiteId) {
		
		return uploadTestDataRepo.countTestData(projectId, testsuiteId);
	}

	public List<ExecutingJob> filterSchedule(String projectId, FilterData filter) {
		// TODO Auto-generated method stub
		return executingJobRepo.findExecutingJobByProjectIdAndTestsuite(projectId, filter.getTestsuiteName());
	}

	public List<ExecutingJob> filterRunning(String projectId, FilterConditionData filter) {
		// TODO Auto-generated method stub
		return executingJobRepo.filterExecutingJob(Long.parseLong(projectId), filter);
	}
	
	public List<Map<String,Object>> findByProjectId(Long projectId){
		return taskRepo.findByProjectId(projectId);
	}
	
	public void saveIssue(String username, String projectId, Task task) {
		LOGGER.info("Begin Service ProjectService Function saveIssue");
		LOGGER.info("Service ProjectService Function saveIssue PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function saveIssue PARAMS projectId:" + projectId);
		
		taskRepo.save(task);
		
	}

	public Task findIssueById(Long id) {
		// TODO Auto-generated method stub
		return taskRepo.findById(id).get();
	}
	
	public Map<String, Object> findIssueId(Long id) {
		// TODO Auto-generated method stub
		return taskRepo.findIssueById(id);
	}
	
	public List<UpdateTask> getTasksHistory(Long projectId, Long taskId) {
		return updateTaskRepo.getUpdateTaskByTaskId(projectId, taskId);
	}
	
	public void deleteIssue(String username,String projectId, Long id, Task task) {
		
		LOGGER.info("Begin Service ProjectService Function deleteIssue");
		LOGGER.info("Service ProjectService Function deleteIssue PARAMS username:" + username);
		LOGGER.info("Service ProjectService Function deleteIssue PARAMS projectId:" + projectId);
		LOGGER.info("Service ProjectService Function deleteIssue PARAMS id:" + id);
		
		taskRepo.deleteById(id);
		
		List<UpdateTask> listHtr = this.getTasksHistory(Long.parseLong(projectId), id);
		for( UpdateTask taskH: listHtr) {
			updateTaskRepo.delete(taskH);
			
		}
		
		this.deleteFileInIssue(username, projectId, task);
		
		LOGGER.info("End Service ProjectService Function deleteIssue");
	}
	
	public void deleteFileInIssue(String username,String projectId, Task task) {
		String nameFiles = task.getFileName();
		String arr [] = nameFiles.replace("[", "").replace ("]", "").split (",");
		List<String> lstFile = Arrays.asList(arr);
		
		for(String nameFile: lstFile) {
			try {
				fileService.deleteFileIssue(username,projectId, nameFile.replace("'", ""));
			} catch (IOException e) {
				LOGGER.error("Service ProjectService Function deleteIssue ERROR MESSAGE: " + ExceptionUtils.getStackTrace(e));
			}
		}
	}
	
	public void saveTaskHistory(UpdateTask updateTask) {
		updateTaskRepo.save(updateTask);
	}
	
	public UpdateTask createHistory(Task taskHtr, String username, String projectId) {
		Task task = taskRepo.findById(taskHtr.getId()).get();
		
		StringBuffer contentTask = new StringBuffer();
		contentTask.append("{");
		
		if(!task.getTracker().equalsIgnoreCase(taskHtr.getTracker())) {
			contentTask.append("\"Tracker\":\" &nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getTracker()+"</span> &nbsp to &nbsp <span style='font-style: italic;'>" +taskHtr.getTracker() +"</span>\",");
		} 
		
		if(!task.getSubject().equalsIgnoreCase(taskHtr.getSubject())) {
			contentTask.append("\"Subject\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getSubject() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getSubject()+"</span>\",");
		}
		
		if(!task.getDescription().isEmpty() && !taskHtr.getDescription().isEmpty()) {
			String descT = taskHtr.getDescription();
			String relaceT = task.getDescription();
			if(!task.getDescription().equalsIgnoreCase(descT)) {
				contentTask.append("\"Description\":\"<div style='margin-left: 40px;'>changed from</div><div style='display: inline-block;margin-left: 137px;font-style: italic;'>"+ relaceT +"</div><div style='margin-left: 40px;'>to</div><div style='display: inline-block;margin-left: 137px;font-style: italic;'>" + descT+"</div>\",");
			}
		} else if(!taskHtr.getDescription().isEmpty()){
			String descT = taskHtr.getDescription().replace("\"", "'");
			contentTask.append("\"Description\":\"" +"<div style='margin-left: 40px;'>set to</div><div style='display: inline-block;margin-left: 137px;font-style: italic;'>" + descT+"</div>\",");
		} else if(!task.getDescription().isEmpty()) {
			String relaceT = task.getDescription().replace("\"", "'");
			contentTask.append("\"Description\":\"" +"<div style='margin-left: 40px;'>set to</div><div style='display: inline-block;margin-left: 137px;font-style: italic; color:gray;'>(" + relaceT+")</div>\",");
		} 
		
		if(!task.getStatus().equalsIgnoreCase(taskHtr.getStatus())) {
			contentTask.append("\"Status\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getStatus() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getStatus()+"</span>\",");
		}
		
		if(!task.getPriority().equalsIgnoreCase( taskHtr.getPriority())) {
			contentTask.append("\"Priority\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getPriority() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getPriority()+"</span>\",");
		}
		
		if(!task.getAssignee().equalsIgnoreCase( taskHtr.getAssignee())) {
			contentTask.append("\"Assignee\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getAssignee() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getAssignee()+"</span>\",");
		}
		
		if(!task.getCategory().equalsIgnoreCase( taskHtr.getCategory())) {
			contentTask.append("\"Category\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getCategory() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getCategory()+"</span>\",");
		}
		
		if(task.getParentTask() != null && taskHtr.getParentTask() != null) {
			if(task.getParentTask().longValue() != taskHtr.getParentTask().longValue()) {
				contentTask.append("\"Parent Task \":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+task.getParentTask() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getParentTask()+"</span>\",");
			}
		} else if( taskHtr.getParentTask() != null ) {
			contentTask.append("\"Parent Task \":\"  &nbsp set to &nbsp  <span style='font-style: italic;'>" + taskHtr.getParentTask()+"</span>\",");
		} else if( task.getParentTask() != null ) {
			contentTask.append("\"Parent Task \":\" &nbsp deleted &nbsp <i style='text-decoration: line-through;color: gray;'>(" + taskHtr.getParentTask()+")</i>\",");
		}
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if(taskHtr.getPlanStartDate() != null) {
			if(task.getPlanStartDate() == null) {
	            String strDateHr = dateFormat.format(taskHtr.getPlanStartDate());  
				contentTask.append("\"Plan Start Date\":\"" +" &nbsp set to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
			} else {
				if(task.getPlanStartDate().compareTo( taskHtr.getPlanStartDate()) != 0) {
					String strDate = dateFormat.format(task.getPlanStartDate());
					String strDateHr = dateFormat.format(taskHtr.getPlanStartDate());
					contentTask.append("\"Plan Start Date\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+ strDate +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
				}
			}
			
		}
		
		if(taskHtr.getPlanEndDate() != null) {
			if(task.getPlanEndDate() == null) {
				String strDateHr = dateFormat.format(taskHtr.getPlanEndDate());
				contentTask.append("\"Plan End Date\":\"" +" &nbsp set to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
			} else {
				if(task.getPlanEndDate().compareTo( taskHtr.getPlanEndDate()) != 0) {
					String strDate = dateFormat.format(task.getPlanEndDate());
					String strDateHr = dateFormat.format(taskHtr.getPlanEndDate());
					contentTask.append("\"Plan End Date\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+ strDate +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
				}
			}
			
		}
		
		if(taskHtr.getActualStartDate() != null) {
			if(task.getActualStartDate() == null) {
				String strDateHr = dateFormat.format(taskHtr.getActualStartDate());
				contentTask.append("\"Actual Start Date\":\"" +" &nbsp set to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
			} else {
				if(task.getActualStartDate().compareTo( taskHtr.getActualStartDate()) != 0) {
					String strDate = dateFormat.format(task.getActualStartDate());
					String strDateHr = dateFormat.format(taskHtr.getActualStartDate());
					contentTask.append("\"Actual Start Date\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+ strDate +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
				}
			}
			
		}
		
		if(taskHtr.getActualEndDate() != null) {
			if(task.getActualEndDate() == null) {
				String strDateHr = dateFormat.format(taskHtr.getActualEndDate());
				contentTask.append("\"Actual End Date\":\"" +" &nbsp set to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
			} else {
				if(task.getActualEndDate().compareTo( taskHtr.getActualEndDate()) != 0) {
					String strDate = dateFormat.format(task.getActualEndDate());
					String strDateHr = dateFormat.format(taskHtr.getActualEndDate());
					contentTask.append("\"Actual End Date\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+ strDate +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + strDateHr +"</span>\",");
				}
			}
			
		}
		
		if(!task.getActualTime().isEmpty() && !taskHtr.getActualTime().isEmpty()) {
			if(!task.getActualTime().equalsIgnoreCase( taskHtr.getActualTime())) {
				contentTask.append("\"Actual Time\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+ task.getActualTime() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getActualTime()+"</span>\",");
			}
		} else if(!taskHtr.getActualTime().isEmpty()){
			contentTask.append("\"Actual Time\":\""+" &nbsp set to &nbsp <span style='font-style: italic;'>" + taskHtr.getActualTime()+"</span>\",");
		} else if(!task.getActualTime().isEmpty()) {
			contentTask.append("\"Actual Time\":\""+" &nbsp deleted &nbsp <i style='text-decoration: line-through;color: gray;'>(" + task.getActualTime()+")</i>\",");
		}
		
		if(!task.getEstimatedTime().isEmpty() && !taskHtr.getEstimatedTime().isEmpty()) {
			if(!task.getEstimatedTime().equalsIgnoreCase( taskHtr.getEstimatedTime())) {
				contentTask.append("\"Estimated Time\":\"&nbsp changed from  &nbsp<span style='font-style: italic;'>"+ task.getEstimatedTime() +"</span> &nbsp to &nbsp <span style='font-style: italic;'>" + taskHtr.getEstimatedTime()+"</span>\",");
			}
			
		} else if(!taskHtr.getEstimatedTime().isEmpty()) {
			contentTask.append("\"Estimated Time\":\"" +" &nbsp set to &nbsp <span style='font-style: italic;'>" + taskHtr.getEstimatedTime()+"</span>\",");
		} else if(!task.getEstimatedTime().isEmpty()) {
			contentTask.append("\"Estimated Time\":\"" +" &nbsp deleted &nbsp <i style='text-decoration: line-through;color: gray;'>(" + task.getEstimatedTime()+")</i>\",");
		}
		
		if(!taskHtr.getNotes().isEmpty()) {
			String nodeT = taskHtr.getNotes().replace("\"", "'");
			contentTask.append("\"Notes\":\"<div style='margin-left: 40px;'>"+nodeT +"</div>\",");
		}
			
		contentTask.append("}");
		
		if(contentTask.length() > 0) {
			UpdateTask updateTask = new UpdateTask();
			updateTask.setContentTask(contentTask.toString());
			updateTask.setTaskId(taskHtr.getId());
			updateTask.setUpdateBy(username);
			updateTask.setUpdateDate(new Date());
			updateTask.setProjectId(Long.parseLong(projectId));
			saveTaskHistory(updateTask);
			return updateTask;
		} else {
			return null;
		}
	}

	public List<Task> getSubTask(Long projectId, Long parentTask) {
		
		return taskRepo.findTaskByProjectIdAndParentTask(projectId, parentTask);
	}
	
	public List<Task> getParent(Long projectId, Long id){
		return taskRepo.findTaskByProjectIdAndId(projectId, id);
	}
	
	public List<Task> getAllTaskByProjectId(Long projectId, String assignee){
		return taskRepo.findTaskByProjectId(projectId, assignee);
	}
	
	public Task previousIssue(Long projectId, Long id, String assignee) {
		return taskRepo.previousIssue(projectId, id, assignee);
	}
	
	public Task nextIssue(Long projectId, Long id, String assignee) {
		return taskRepo.nextIssue(projectId, id, assignee);
	}
	
	public Task getTaskByBugId(Long projectId, Long bugId) {
		return taskRepo.findTaskByProjectIdAndBugId(projectId, bugId);
	}

	public List<Map<String, Object>> filterTask(String projectId, FilterTaskDTO filterTask) {
		// TODO Auto-generated method stub
		return taskRepo.findTask(filterTask, Long.parseLong(projectId));
	}
	
	public List<Map<String, Object>> getForBugId(Long projectId){
		return projectProgressRepo.getForBugId(projectId);
	}

	public void changeStatusTestcase(String name, String projectId, String testsuiteId, String testCaseName) {
		ProjectProgress pr = projectProgressRepo.findByProjectProgressIdentity_ProjectIdAndProjectProgressIdentity_TestsuiteAndProjectProgressIdentity_Testcase(Long.parseLong(projectId), testsuiteId, testCaseName);
		if(pr.getStatusIssue().equalsIgnoreCase("New")) {
			pr.setStatusIssue("In Progress");
		}
		projectProgressRepo.save(pr);
	}
	
	public UserProject checkDataInUserProject(Long id, String projectName, String username) {
		return userProjectRepo.findUserProjectByProjectIdAndProjectNameAndUsername(id, projectName, username);
	}
	
	public UserProject checkRoleUserInProject(Long id, String username) {
		return userProjectRepo.findUserProjectByProjectIdAndUsername(id, username);
	}
	
	public InvitedToProject checkEmailExists(String projectId, String toEmail) {
		return invitedRepo.findByProjectIdAndToEmail(projectId, toEmail);
	}
	
	public void getPPByProjectIdAndTestcase(ProjectProgress p) {
		ProjectProgress pp = projectProgressRepo.findProjectProgressByProjectIdAndTestcase(p.getProjectProgressIdentity().getProjectId(), p.getProjectProgressIdentity().getTestcase());
		
		pp.setOrderId(p.getOrderId());
		projectProgressRepo.save(pp); 
	}
	
	/**
	 * @param projectId
	 * @param testsuiteId
	 * @return
	 */
	public List<ExportTestcaseDTO> getTestsuiteTestcaseAlls(String username,String projectId) {
		
		LOGGER.info("Begin Service FileService function getTestsuiteTestcaseAlls");
		LOGGER.info("Begin Service FileService function getTestsuiteTestcaseAlls PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function getTestsuiteTestcaseAlls PARAMS projectId: " + projectId);
		List<ExportTestcaseDTO> listTestcase = new ArrayList<ExportTestcaseDTO>();
		
		List<String> listTestsuites = projectSummaryRepo.getAllTestsuiteInProject(Long.parseLong(projectId));
		HSSFWorkbook workbook = new HSSFWorkbook();
		for(String testsuiteId: listTestsuites) {
			HSSFSheet sheet = workbook.createSheet(testsuiteId);
			int rownum = 0;
			Cell cell;
	        Row row;
	        
	        ExportExcel ex = new ExportExcel();
	        HSSFCellStyle style = ex.createStyleForTitle(workbook);
	        
	        row = sheet.createRow(rownum);
	 
	        cell = row.createCell(0, CellType.STRING);
	        cell.setCellValue("#");
	        cell.setCellStyle(style);
	        
	        cell = row.createCell(1, CellType.STRING);
	        cell.setCellValue("Testcase");
	        cell.setCellStyle(style);
	        
	        cell = row.createCell(2, CellType.STRING);
	        cell.setCellValue("Testcase Procedure");
	        cell.setCellStyle(style);
	        
	        cell = row.createCell(3, CellType.STRING);
	        cell.setCellValue("Expected Output");
	        cell.setCellStyle(style);
	       
	        cell = row.createCell(4, CellType.STRING);
	        cell.setCellValue("Last Run");
	        cell.setCellStyle(style);
			writeDataToExcel(workbook, username, projectId, testsuiteId, listTestcase, sheet, rownum, row, cell);
		}
		
		String home = System.getProperty("user.home");
        File file = new File(home + "/" + "Project_" + projectId+"_" + "Export.xls");
        file.getParentFile().mkdirs();
        
        FileOutputStream outFile;
		try {
			outFile = new FileOutputStream(file);
			workbook.write(outFile);
	        
		} catch (IOException e) {
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		
		LOGGER.info("Service FileService function getTestsuiteTestcaseAlls RESULTS listTestcase:" + new Gson().toJson(listTestcase, new TypeToken<List<ExportTestcaseDTO>>() {}.getType()));
		LOGGER.info("End Service FileService function getTestsuiteTestcaseAlls");
		return listTestcase;
	}
	
	public void writeDataToExcel(HSSFWorkbook workbook, String username, String projectId, String testsuiteId, List<ExportTestcaseDTO> listTestcase, HSSFSheet sheet, int rownum, Row row, Cell cell) {
		
		List<ExportTestcaseDTO> exportTcs = this.getAllTestCaseAndLastRun(username,projectId, testsuiteId);
		
		for(ExportTestcaseDTO e: exportTcs) {
			listTestcase.add(e);
	        
	        rownum++;
            row = sheet.createRow(rownum);
 
            cell = row.createCell(0, CellType.NUMERIC);
            cell.setCellValue(rownum);
            
            cell = row.createCell(1, CellType.STRING);
            cell.setCellValue(e.getTestcase());
            
            cell = row.createCell(2, CellType.STRING);
            String textTestcaseProcedure = e.getTestcaseProcedure().replaceAll("<br>", "\n");
            cell.setCellValue(textTestcaseProcedure);
            
            cell = row.createCell(3, CellType.STRING);
            String textExpectedOuput = e.getExpectedOutput().replaceAll("<br>", "\n");
            cell.setCellValue(textExpectedOuput);
            
            cell = row.createCell(4, CellType.STRING);
            cell.setCellValue(e.getLastRun());
	        
		}
	}
	
	/**
	 * Get all testcase in testsuite save in exportTestcaseDTO
	 * 
	 * @param projectId
	 * @param testsuitId
	 * @return
	 */
	public List<ExportTestcaseDTO> getAllTestCaseAndLastRun(String username, String projectId, String testsuitId) {
		
		LOGGER.info("Begin Service FileService function getAllTestCaseAndLastRun");
		LOGGER.info("Begin Service FileService function getAllTestCaseAndLastRun PARAMS username: " + username);
		LOGGER.info("Begin Service FileService function getAllTestCaseAndLastRun PARAMS projectId: " + projectId);
		LOGGER.info("Begin Service FileService function getAllTestCaseAndLastRun PARAMS testsuitId: " + testsuitId);
		List<ExportTestcaseDTO> lstExport = new ArrayList<ExportTestcaseDTO>();
		List<Map<String, Object>> lsTestcase = projectProgressRepo.getDataExportExcel(Long.parseLong(projectId), testsuitId);
		
		for(Map<String, Object> info : lsTestcase) {
			ExportTestcaseDTO exportItem = new ExportTestcaseDTO();
			exportItem.setTestsuite(testsuitId);
			exportItem.setTestcase((String) info.get("testcase"));
			exportItem.setExpectedOutput((String) info.get("expectedOutputTc"));
			exportItem.setTestcaseProcedure((String) info.get("testcaseProcedure"));
			
			String testcaseResult = (String) info.get("test_case_result");
			if(testcaseResult == null) {
				testcaseResult = "";
			}
			Date executeDate = (Date) info.get("execute_date");
			String dateStr = "";
			if(executeDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				dateStr = formatter.format(executeDate);
			}
			
			String runningUser = (String) info.get("running_user");
			if(runningUser == null) {
				runningUser = "";
			}
			exportItem.setLastRun(testcaseResult + "--" + dateStr + "--" + runningUser);
			
			lstExport.add(exportItem);
		}

		LOGGER.info("Service FileService function getAllTestCaseAndLastRun RESULTS lstExport:" +  new Gson().toJson(lstExport, new TypeToken<List<ExportTestcaseDTO>>() {}.getType()));
		LOGGER.info("End Service FileService function getAllTestCaseAndLastRun");
		return lstExport;

	}
	
	public UploadTestData getUploadTestData(String projectId, String testsuite, String layout) {
		return uploadTestDataRepo.getUploadTestData(projectId, testsuite, layout);
	}
	
	public List<Map<String, Object>> graphicIssue(Long projectId){
		List<Map<String, Object>> listGraph = taskRepo.graphicDataIssue(projectId);
		List<ProjectComponent> listStatus = projectComponentRepo.getAllStatus(projectId);
		for(ProjectComponent status: listStatus) {
			Map<String, Object> graph = new HashMap<String, Object>();
			graph = projectProgressRepo.graphicTestcaseStatus(projectId, status.getItemName(), status.getColorCode());
			if(((BigInteger) graph.get("count")).intValue() != 0) {
				listGraph.add(graph);
			} 
			
		}
		return listGraph;
	}
	
	public List<ProjectComponent> listStatus(Long projectId){
		List<ProjectComponent> listStatus = projectComponentRepo.getAllStatus(projectId);
		return listStatus;
	}

	public ResultInfo getResultInfo(String projectId, String testsuiteId, String testCaseName) {
		return resultInfoRepo.findResultInfoByProjectIdAndTest(projectId, testsuiteId, testCaseName);
	}

	public void changeStatusTask(Long id) {
		Task task = taskRepo.findByBugId(id);
		if(task != null ) {
			task.setStatus("Closed");
		}
		taskRepo.save(task);
		
	}

	 public Task findByBugId(long id) {
		 return taskRepo.findByBugId(id);
	 }

	public List<Map<String, Object>> graphicIssue(long projectId, String nameMilestone) {
		List<Map<String, Object>> listGraph = taskRepo.graphicDataIssueMilestone(projectId,nameMilestone);
		List<ProjectComponent> listStatus = projectComponentRepo.getAllStatus(projectId);
		for(ProjectComponent status: listStatus) {
			Map<String, Object> graph = new HashMap<String, Object>();
			graph = projectProgressRepo.graphicTestcaseStatusMilestone(projectId, status.getItemName(), status.getColorCode(), nameMilestone);
			if(((BigInteger) graph.get("count")).intValue() != 0) {
				listGraph.add(graph);
			} 
			
		}
		return listGraph;
		
	} 
	
	public List<ProjectMilestone> getAllMilestone(Long projectId){
		return projectMilestoneRepo.getAllMilestone(projectId);
	}
	
	public Project copyPublicProject(Project project, String username) {
		Project projectOld = projectRepo.getProjectNew(project.getId());
		double staSizeOl = projectOld.getStandardSize();
		double staSizeNew = project.getActualSize();
		double timesSize = Math.round((staSizeNew/staSizeOl) * 100)/100;
		List<Task> listTask = taskRepo.findAllByProjectId(project.getId());
		Project newProject = this.setDataToProjectPrivate(projectOld, project, timesSize, username);
		
		boolean require = false;
		if(project.isRequire()) {
			require = true;
		}
		projectRepo.save(newProject);
		this.addProjectToUserProject(newProject, username);
		fileService.copyFilePublicToPrivate(projectOld, newProject);
		this.copyTrackerPublicProject(projectOld, newProject, require);
		this.copyMilestonePublicProject(projectOld, newProject);
		this.copyTaskPublicProject(listTask, newProject, timesSize, username, projectOld);
		return newProject;
	}

	private void addProjectToUserProject(Project newProject, String username) {
		UserProject userProject = new UserProject();
		userProject.setProjectId(newProject.getId());
		userProject.setProjectName(newProject.getName());
		userProject.setProjectRole(UserProject.PROJECT_ADMIN);
		userProject.setUsername(username);
		userProjectRepo.save(userProject);
	}

	public Project setDataToProjectPrivate(Project project, Project data, double timesSize, String username) {
		
		Project newProject = new Project();
		newProject.setName(project.getName());
		newProject.setDescription(project.getDescription());
		newProject.setType(FarmateConstants.PROJECT_TYPE_PRIVATE);
		if(project.getType().equalsIgnoreCase(FarmateConstants.PROJECT_TYPE_PRIVATE)) {
			newProject.setParentProject(Long.toString(project.getId()));
		} 
		newProject.setStatus("Open");
		if(project.getType().equalsIgnoreCase(FarmateConstants.PROJECT_TYPE_PUBLIC)) {
			newProject.setReferProcess(Long.toString(project.getId()));
		} 
		newProject.setImage(project.getImage());
		newProject.setVideo(project.getVideo());
		newProject.setStandardSize(project.getStandardSize());
		newProject.setActualSize(data.getActualSize());
		newProject.setStandardUnit(project.getStandardUnit());
		newProject.setEstimatedProductivity(project.getEstimatedProductivity() * timesSize);
		newProject.setEstimatedPrice(project.getEstimatedPrice() * timesSize);
		newProject.setEstimatedCost(project.getEstimatedCost() * timesSize);
		newProject.setInvestAmmount(data.getInvestAmmount());
		newProject.setEstimatedDuration(project.getEstimatedDuration() * (int)timesSize);
		newProject.setProductType(project.getProductType());
		newProject.setStandardGAP(project.getStandardGAP());
		newProject.setPlanStartDate(data.getPlanStartDate());
		newProject.setProductCode(project.getProductCode());
		newProject.setProductName(project.getProductName());
		if(project.getPlanEndDate() != null) {
			long diff = project.getPlanEndDate().getTime() - project.getPlanStartDate().getTime();
			Date endDateNew = new Date(data.getPlanStartDate().getTime() + diff);
			newProject.setPlanEndDate(endDateNew);
		}
		
		newProject.setCreateDate(new Date());
		newProject.setLocation(project.getLocation());
		newProject.setAddress(data.getAddress());
		newProject.setCreatedBy(username);
		return newProject;
	}
	
	public void copyTaskPublicProject(List<Task> lstTask, Project project, double timesSize, String username, Project projectOld) {
		List<Task> parentList = new ArrayList<Task>();
		for(Task task: lstTask) {
			
			Task taskNew = new Task();
			taskNew.setTracker(task.getTracker());
			taskNew.setSubject(task.getSubject());
			taskNew.setDescription(task.getDescription());
			taskNew.setStatus("New");
			taskNew.setFileName(task.getFileName());
			taskNew.setParentTask(task.getParentTask());
			taskNew.setProjectId(project.getId());
			if(task.getPlanStartDate() != null) {
				long diff = task.getPlanStartDate().getTime() - projectOld.getPlanStartDate().getTime();
				Date startPl = new Date(project.getPlanStartDate().getTime() + diff);
				taskNew.setPlanStartDate(startPl);
			}
			
			if(task.getPlanEndDate() != null) {
				long diff = task.getPlanEndDate().getTime() - projectOld.getPlanStartDate().getTime();
				Date planEndDate = new Date(project.getPlanStartDate().getTime() + diff);
				taskNew.setPlanEndDate(planEndDate);
			}
			
			taskNew.setCreateBy(username);
			taskNew.setCreateDate(new Date());
			String planToolMaterial = task.getPlanToolMaterial();
			if(!planToolMaterial.isEmpty() && planToolMaterial != null) {
				Gson gson = new Gson();
				Object[] object = gson.fromJson(planToolMaterial, Object[].class);
				ObjectMapper oMapper = new ObjectMapper();
				JSONArray arr = new JSONArray();
				for(int i = 0; i < object.length; i++) {
					Map<String, Object> map = oMapper.convertValue(object[i], Map.class);
					double valueQ = Double.parseDouble(((String)map.get("Quantity")))* timesSize;
					double valuePrice = Double.parseDouble(((String)map.get("Price")));
					double valueTotal = valueQ * valuePrice;
					map.put("Quantity", Double.toString(valueQ));
					map.put("Total", Double.toString(valueTotal));
					arr.put(map);
				}
				taskNew.setPlanToolMaterial(arr.toString());
			}
			taskRepo.save(taskNew);
			
			
			Task p = new Task();
			p.setParentTask(task.getId());
			p.setBugId(taskNew.getId());
			parentList.add(p);
			
		}
		
		for(Task itemTak : parentList) {
			taskRepo.updateTaskPrivate(project.getId(), itemTak.getParentTask(), itemTak.getBugId());
		}
	}
	
	public void copyTrackerPublicProject(Project projectOld, Project newProject, boolean require) {
		if(require) {
			ProjectComponent requested = new ProjectComponent("Status", "Requested", "Requested", newProject.getId(), 1, "#e74c3c");
			projectComponentRepo.save(requested);
			ProjectComponent response = new ProjectComponent("Status", "Response", "Response", newProject.getId(), 1, "#e74c3c");
			projectComponentRepo.save(response);
		}
		List<ProjectComponent> lstComponent = projectComponentRepo.getProjectComponentByProjectId(projectOld.getId());
		for(ProjectComponent p : lstComponent) {
			addProjectComponent(p, newProject);
		}
	}
	
	public void addProjectComponent(ProjectComponent p, Project newProject) {
		ProjectComponent pNew = new ProjectComponent();
		pNew.setColorCode(p.getColorCode());
		pNew.setGroupCode(p.getGroupCode());
		pNew.setItemName(p.getItemName());
		pNew.setItemValue(p.getItemValue());
		pNew.setProjectId(newProject.getId());
		pNew.setStatus(p.getStatus());
		projectComponentRepo.save(pNew);
	}
	
	public void copyMilestonePublicProject(Project projectOld, Project newProject) {
		List<ProjectMilestone> lstMilestone = projectMilestoneRepo.getAllMilestone(projectOld.getId());
		for(ProjectMilestone promile: lstMilestone) {
			ProjectMileStoneIdentity proIdenNew = new ProjectMileStoneIdentity(newProject.getId(), promile.getProjectMilestoneIdentity().getMileStoneName());
			ProjectMilestone proMilestone = new ProjectMilestone();
			proMilestone.setProjectMilestoneIdentity(proIdenNew);
			if(promile.getStartDate() != null) {
				long diff = promile.getStartDate().getTime() - projectOld.getPlanStartDate().getTime();
				Date startPl = new Date(newProject.getPlanStartDate().getTime() + diff);
				proMilestone.setStartDate(startPl);
			}
			
			if(promile.getEndDate() != null) {
				long diff = promile.getEndDate().getTime() - projectOld.getPlanStartDate().getTime();
				Date endDate = new Date(newProject.getPlanStartDate().getTime() + diff);
				proMilestone.setEndDate(endDate);
			}
			proMilestone.setDescription(promile.getDescription());
			proMilestone.setStatus(promile.getStatus());
			proMilestone.setStatusMilestone(promile.getStatusMilestone());
			proMilestone.setUpdateDate(promile.getUpdateDate());
			projectMilestoneRepo.save(proMilestone);
			
		}
	}
	
	public List<Project> getAllProject(String username) {
		return projectRepo.findProjectByDeletedFlagAndCreatedBy(username);
	}
	
}

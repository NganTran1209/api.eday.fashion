package io.bottest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.CostRevenueOther;
import io.bottest.jpa.respository.CostRevenueOtherRepository;

@Service
public class CostRevenueOtherService {
	@Autowired
	CostRevenueOtherRepository costRevenueOtherRepo;
	public void save(CostRevenueOther costRevenueOther) {
		costRevenueOtherRepo.save(costRevenueOther);
	}
	
	public List<CostRevenueOther>  listCostRevenueOthers(String username, String status){
		List<CostRevenueOther> listCostRevenueOthers = costRevenueOtherRepo.getCostRevenueOtherByUserAndStatus(username, status);
		return  listCostRevenueOthers;
	}
	
	public void  deleteCostRevenueOther(Long id){
		costRevenueOtherRepo.deleteById(id);
	}
}
